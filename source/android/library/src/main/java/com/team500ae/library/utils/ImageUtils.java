package com.team500ae.library.utils;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.annotation.DrawableRes;
import android.support.annotation.NonNull;
import android.support.annotation.UiThread;
import android.util.Base64;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.etiennelawlor.imagegallery.library.adapters.FullScreenImageGalleryAdapter;
import com.google.gson.JsonElement;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.RequestCreator;
import com.squareup.picasso.Target;
import com.team500ae.library.activities.image.ImageViewerActivity;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;


/**
 * Created by MaiNam on 5/12/2016.
 */
public class ImageUtils {
    private static final String TAG = ImageUtils.class.getSimpleName();

    static int scaleSize = 512;

    public static Bitmap resizeImageForImageView(Bitmap bitmap) {
        Bitmap resizedBitmap = null;
        int originalWidth = bitmap.getWidth();
        int originalHeight = bitmap.getHeight();
        int newWidth = -1;
        int newHeight = -1;
        float multFactor = -1.0F;
        if (originalHeight > originalWidth) {
            newHeight = scaleSize;
            multFactor = (float) originalWidth / (float) originalHeight;
            newWidth = (int) (newHeight * multFactor);
        } else if (originalWidth > originalHeight) {
            newWidth = scaleSize;
            multFactor = (float) originalHeight / (float) originalWidth;
            newHeight = (int) (newWidth * multFactor);
        } else if (originalHeight == originalWidth) {
            newHeight = scaleSize;
            newWidth = scaleSize;
        }
        resizedBitmap = Bitmap.createScaledBitmap(bitmap, newWidth, newHeight, false);
        return resizedBitmap;
    }

    public static String toBase64(Context context, Uri uri) {
        Bitmap bitmap = null;
        try {
            bitmap = MediaStore.Images.Media.getBitmap(context.getContentResolver(), uri);
            bitmap = resizeImageForImageView(bitmap);
            return toBase64(context, bitmap);
        } catch (IOException e) {
            return "";
        } finally {
            if (bitmap != null)
                bitmap.recycle();
        }
    }

    public static String toBase64(Context context, Bitmap bitmap) {
        try {
            bitmap = resizeImageForImageView(bitmap);
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos); //bm is the bitmap object

            byte[] b = baos.toByteArray();
            return Base64.encodeToString(b, Base64.DEFAULT | Base64.NO_WRAP);
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    public static class Size {
        private int height;
        private int width;

        public int getHeight() {
            return height;
        }

        public void setHeight(int height) {
            this.height = height;
        }

        public int getWidth() {
            return width;
        }

        public void setWidth(int width) {
            this.width = width;
        }

        public Size(int width, int height) {
            this.width = width;
            this.height = height;
        }

    }


    @UiThread
    public static void loadImageByGlide(Context context, boolean isResize, int width, int height, @NonNull String url, @DrawableRes int errorResource, @DrawableRes int placeHolder, final ImageView imageView, boolean isCache) {
        try {
            RequestOptions options = new RequestOptions()
                    .centerCrop()
                    .placeholder(placeHolder)
                    .error(errorResource)
                    .priority(Priority.HIGH).dontAnimate().error(errorResource);

            if (isResize) {
                options = options.override(width, height).centerCrop();
            }
            if (isCache) {
                options = options.diskCacheStrategy(DiskCacheStrategy.ALL);
            } else {
                options = options.diskCacheStrategy(DiskCacheStrategy.NONE);
            }
            Glide.with(context).load(url).apply(options).into(imageView);
        } catch (Exception e) {
            e.printStackTrace();
        } catch (OutOfMemoryError e) {
            e.printStackTrace();
        }


    }

    @UiThread
    public static void loadImageByGlide(Context context, boolean isResize, int width, int height, Uri uri, @DrawableRes int errorResource, @DrawableRes int placeHolder, final ImageView imageView, boolean isCache) {
        loadImageByGlide(context, isResize, width, height, uri.toString(), errorResource, placeHolder, imageView, isCache);
    }

    public static void loadImageByPicasso(Context context, JsonElement url, int placeHolderResource, int errorResource, ImageView imageView, boolean clearCache, Size size) {
        try {
            loadImageByPicasso(context, url.getAsString(), placeHolderResource, errorResource, imageView, clearCache, size);
        } catch (Exception e) {
            e.printStackTrace();
        } catch (OutOfMemoryError e) {
            e.printStackTrace();
        }

    }


    public static void loadImageByPicasso(Context context, @DrawableRes int resId, ImageView imageView) {
        try {
            RequestCreator requestCreator = Picasso.with(context).load(resId);
            requestCreator.into(imageView);
        } catch (Exception e) {
            e.printStackTrace();
        } catch (OutOfMemoryError e) {
            e.printStackTrace();
        }
    }

    public static void loadImageByPicasso(Context context, String url, ImageView imageView, boolean clearCache, Size size) {
        try {

            imageView.setImageBitmap(null);

            url = url.trim();
            RequestCreator requestCreator = Picasso.with(context).load(url);
            if (size != null) {
                requestCreator = requestCreator.resize(size.getWidth(), size.getHeight()).centerCrop();
            }
            if (clearCache)
                requestCreator = requestCreator.memoryPolicy(MemoryPolicy.NO_CACHE);
            requestCreator.into(imageView);
        } catch (Exception e) {
            e.printStackTrace();
        } catch (OutOfMemoryError e) {
            e.printStackTrace();
        }
    }


    public static void loadImageByPicasso(Context context, String url, int placeHolderResource, int errorResource, ImageView imageView, boolean clearCache, Size size) {
        try {

            imageView.setImageBitmap(null);

            url = url.trim();
            RequestCreator requestCreator = Picasso.with(context).load(url);
            if (size != null) {
                requestCreator = requestCreator.resize(size.getWidth(), size.getHeight()).centerCrop();
            }
            if (clearCache)
                requestCreator = requestCreator.memoryPolicy(MemoryPolicy.NO_CACHE);

            requestCreator = requestCreator.placeholder(placeHolderResource).error(errorResource);
            requestCreator.into(imageView);
        } catch (Exception e) {
            e.printStackTrace();
        } catch (OutOfMemoryError e) {
            e.printStackTrace();
        }
    }

    public static void loadImageByPicasso(Context context, String url, int placeHolderResource, int errorResource, boolean clearCache, Size size, Target target) {
        try {

            RequestCreator requestCreator = Picasso.with(context).load(url);
            if (size != null) {
                requestCreator = requestCreator.resize(size.getWidth(), size.getHeight()).centerCrop();
            }
            if (clearCache)
                requestCreator = requestCreator.memoryPolicy(MemoryPolicy.NO_CACHE);

            requestCreator = requestCreator.placeholder(placeHolderResource).error(errorResource);
            requestCreator.into(target);
        } catch (Exception e) {
            e.printStackTrace();
        } catch (OutOfMemoryError e) {
            e.printStackTrace();
        }
    }

    public static void showImage(Context context, FullScreenImageGalleryAdapter.FullScreenImageLoader loader, ArrayList<String> listUrl, ArrayList<String> listCaption, int position) {
        try {
            ImageViewerActivity.setFullScreenImageLoader(loader);
            Intent intent = new Intent(context, ImageViewerActivity.class);
            intent.putStringArrayListExtra("images", listUrl);
            intent.putStringArrayListExtra("captions", listCaption);
            intent.putExtra("position", position);
            context.startActivity(intent);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void showImage(Context context, FullScreenImageGalleryAdapter.FullScreenImageLoader loader, ArrayList<String> listUrl, int position) {
        try {
            ImageViewerActivity.setFullScreenImageLoader(loader);
            Intent intent = new Intent(context, ImageViewerActivity.class);
            intent.putStringArrayListExtra("KEY_IMAGES", listUrl);
            intent.putExtra("KEY_POSITION", position);
            context.startActivity(intent);
        } catch (Exception e) {
            e.printStackTrace();

        }
    }

    public static void showImage(Context context, FullScreenImageGalleryAdapter.FullScreenImageLoader loader, String imageUrl, int position) {
        try {
            if(imageUrl!=null) {
                ImageViewerActivity.setFullScreenImageLoader(loader);
                Intent intent = new Intent(context, ImageViewerActivity.class);
                ArrayList<String> listUrl = new ArrayList<>();
                listUrl.add(imageUrl);
                intent.putStringArrayListExtra("KEY_IMAGES", listUrl);
                intent.putExtra("KEY_POSITION", position);
                context.startActivity(intent);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void showImage(Context context, FullScreenImageGalleryAdapter.FullScreenImageLoader loader, String imageUrl, String caption, int position) {
        try {
            ImageViewerActivity.setFullScreenImageLoader(loader);
            Intent intent = new Intent(context, ImageViewerActivity.class);
            ArrayList<String> listUrl = new ArrayList<>();
            ArrayList<String> listCaption = new ArrayList<>();
            listUrl.add(imageUrl);
            listCaption.add(caption);
            intent.putStringArrayListExtra("KEY_IMAGES", listUrl);
            intent.putStringArrayListExtra("captions", listCaption);
            intent.putExtra("KEY_POSITION", position);
            context.startActivity(intent);
        } catch (Exception
                e) {
            e.printStackTrace();
        }
    }


}
