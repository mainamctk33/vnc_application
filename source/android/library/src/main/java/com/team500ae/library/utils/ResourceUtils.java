package com.team500ae.library.utils;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;

/**
 * Created by maina on 9/25/2017.
 */

public class ResourceUtils {
    public static Drawable getDrawableByName(Context context, String name)
    {
        return ContextCompat.getDrawable(context,context.getResources().getIdentifier(name,"drawable",context.getPackageName()));
    }
}
