package com.team500ae.library.constants;

import okhttp3.MediaType;

/**
 * Created by maina on 9/19/2017.
 */

public class ServerConstants {
    public static final int RESPONSE_ERROR = 404;
    public static final MediaType JSON = MediaType.parse("application/json; charset=utf-8");;
    public static final int RESPONSE_OK = 200;
    public static final int REQUEST_TIMEOUT_ERROR = 400;
}
