package com.team500ae.library.constants;


/**
 * Created by MaiNam on 4/21/2016.
 */
public class Constants {
    public static final String SNACKBAR_MESSAGE = "SNACKBAR_MESSAGE";
    public static final String SNACKBAR_BUTTON_RELOAD_NAME = "SNACKBAR_BUTTON_RELOAD_NAME";
    public static final int REQ_REQUEST_PERMISSION = 105;
    public static final String IGNORE_CODE = "IGNORE_CODE";
    public static final int REQUEST_AUTHORIZATION = 106;
    public static final int REQUEST_ACCOUNT_PICKER = 107;
}
