package com.team500ae.vncapplication.services;

import android.app.IntentService;
import android.content.Intent;
import android.content.Context;

import com.google.gson.JsonArray;
import com.team500ae.library.utils.ConvertUtils;
import com.team500ae.library.utils.SharedPreferencesUtil;
import com.team500ae.vncapplication.constants.Constants;
import com.team500ae.vncapplication.data_access.BookmarkInfo;
import com.team500ae.vncapplication.data_access.DataCacheInfo;
import com.team500ae.vncapplication.data_access.news.NewsInfo;
import com.team500ae.vncapplication.data_access.RealmInfo;
import com.team500ae.vncapplication.data_access.news.TagInfo;
import com.team500ae.vncapplication.data_access.news.TypeNewsInfo;
import com.team500ae.vncapplication.data_access.user.UserInfo;
import com.team500ae.vncapplication.models.BaseReturnFunctionEntity;
import com.team500ae.vncapplication.models.ServiceResponseEntity;
import com.team500ae.vncapplication.models.realm_models.news.TypeNewsEntity;

import java.util.ArrayList;
import java.util.Date;

import io.realm.Realm;
import io.realm.exceptions.RealmError;

/**
 * An {@link IntentService} subclass for handling asynchronous task requests in
 * a service on a separate handler thread.
 * <p>
 * TODO: Customize class - update intent actions, extra parameters and static
 * helper methods.
 */
public class ServiceNews extends IntentService {
    // TODO: Rename actions, choose action names that describe tasks that this
    // IntentService can perform, e.g. ACTION_FETCH_NEW_ITEMS
    private static final String ACTION_SYNC_TAG = "com.team500ae.vncapplication.services.action.ACTION_SYNC_TAG";
    private static final String ACTION_SYNC_TYPE_NEWS = "com.team500ae.vncapplication.services.action.ACTION_SYNC_TYPE_NEWS";
    private static final String ACTION_NEWS_BOOKMARK = "com.team500ae.vncapplication.services.action.ACTION_NEWS_BOOKMARK";
    private static final String ACTION_SET_DEFAULT_CATEGORY = "com.team500ae.vncapplication.services.action.ACTION_SET_DEFAULT_CATEGORY";

    public ServiceNews() {
        super("ServiceNews");
    }

    /**
     * Starts this service to perform action Foo with the given parameters. If
     * the service is already performing a task this action will be queued.
     *
     * @see IntentService
     */
    // TODO: Customize helper method
    public static void startActionSyncTypeNews(Context context) {
        Intent intent = new Intent(context, ServiceNews.class);
        intent.setAction(ACTION_SYNC_TYPE_NEWS);
        context.startService(intent);
    }

    public static void startActionBookmark(Context context, int id, boolean bookmark) {
        Intent intent = new Intent(context, ServiceNews.class);
        intent.putExtra("ID", id);
        intent.putExtra("BOOKMARK", bookmark);
        intent.setAction(ACTION_NEWS_BOOKMARK);
        context.startService(intent);
    }

    public static void startActionSyncTag(Context context) {
        Intent intent = new Intent(context, ServiceNews.class);
        intent.setAction(ACTION_SYNC_TAG);
        context.startService(intent);
    }

    public static void startSetDefaultCategory(Context context) {
        Intent intent = new Intent(context, ServiceNews.class);
        intent.setAction(ACTION_SET_DEFAULT_CATEGORY);
        context.startService(intent);
    }


    @Override
    protected void onHandleIntent(Intent intent) {
        if (intent != null) {
            final String action = intent.getAction();
            switch (action) {
                case ACTION_SYNC_TAG:
                    handleActionSyncTag();
                    break;
                case ACTION_SYNC_TYPE_NEWS:
                    handleActionSyncTypeNews();
                    break;
                case ACTION_NEWS_BOOKMARK:
                    boolean bookmark = intent.getBooleanExtra("BOOKMARK", false);
                    int id = intent.getIntExtra("ID", 0);
                    handleActionBookmark(id, bookmark);
                    break;
                case ACTION_SET_DEFAULT_CATEGORY:
                    handleSetDefaulCategory();
                    break;
            }
        }
    }

    /**
     * Handle action Foo in the provided background thread with the provided
     * parameters.
     */
    private void handleActionSyncTypeNews() {
        // TODO: Handle action Foo
        new Thread(new Runnable() {
            @Override
            public void run() {
                Realm realm = null;
                try {
                    realm = RealmInfo.getRealm(getApplicationContext());
                    TypeNewsInfo typeNewsInfo = new TypeNewsInfo();
                    long lastSyncTypeNewsDate = 0;//SharedPreferencesUtil.getLong(getApplicationContext(), Constants.LAST_SYNC_TYPENEWS, 0L);
                    ServiceResponseEntity<JsonArray> result = typeNewsInfo.syncTypeNews(getApplicationContext(), lastSyncTypeNewsDate);
                    if (result != null && result.getStatus() == 0) {
                        if (typeNewsInfo.updateTypeNews(realm, result.getData()).getData())
                            SharedPreferencesUtil.setSharedPreferences(getApplicationContext(), SharedPreferencesUtil.EnumType.Long, Constants.LAST_SYNC_TYPENEWS, new Date().getTime());
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                } catch (RealmError e) {
                    e.printStackTrace();
                } catch (OutOfMemoryError e) {
                    e.printStackTrace();
                } finally {
                    RealmInfo.closeRealm(realm);
                }
            }
        }).start();
    }

    private void handleSetDefaulCategory() {
        // TODO: Handle action Foo
        new Thread(new Runnable() {
            @Override
            public void run() {
                String userId = UserInfo.getCurrentUserId();
                Realm realm = null;
                try {
                    realm = RealmInfo.getRealm(getApplicationContext());
                    TypeNewsInfo typeNewsInfo = new TypeNewsInfo();
                    BaseReturnFunctionEntity<ArrayList<TypeNewsEntity>> currentMyType = typeNewsInfo.getTypeNewsHasSelected(getApplicationContext(), realm, userId);
                    if (currentMyType.isTrue() && currentMyType.getData().size() == 0) {
                        BaseReturnFunctionEntity<ArrayList<TypeNewsEntity>> listType = typeNewsInfo.getListTypeNews(getApplicationContext(), realm);
                        if (listType.isTrue()) {
                            ArrayList<TypeNewsEntity> typeNews = listType.getData();
                            for (int i = 0; i < 5 && i < typeNews.size(); i++) {
                                typeNewsInfo.saveTypeNewsToFavorite(getApplicationContext(), realm, userId, typeNews.get(i).getId());
                            }
                            Intent intent = new Intent(Constants.ACTION_TYPE_NEWS_FOR_USER_CHANGED);
                            getApplicationContext().sendBroadcast(intent);
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                } catch (RealmError e) {
                    e.printStackTrace();
                } catch (OutOfMemoryError e) {
                    e.printStackTrace();
                } finally {
                    RealmInfo.closeRealm(realm);
                }
            }
        }).start();
    }


    /**
     * Handle action Foo in the provided background thread with the provided
     * parameters.
     */
    private void handleActionSyncTag() {
        // TODO: Handle action Foo
        new Thread(new Runnable() {
            @Override
            public void run() {
                Realm realm = null;
                try {
                    realm = RealmInfo.getRealm(getApplicationContext());
                    TagInfo tagInfo = new TagInfo();
                    long lastSyncTypeNewsDate = SharedPreferencesUtil.getLong(getApplicationContext(), Constants.LAST_SYNC_TAG, 0L);
                    ServiceResponseEntity<JsonArray> result = tagInfo.syncTag(getApplicationContext(), lastSyncTypeNewsDate);
                    if (result != null && result.getStatus() == 0) {
                        DataCacheInfo.setData(realm, DataCacheInfo.EnumCacheType.Tag, ConvertUtils.toJson(result.getData()), true);
                        SharedPreferencesUtil.setSharedPreferences(getApplicationContext(), SharedPreferencesUtil.EnumType.Long, Constants.LAST_SYNC_TYPENEWS, new Date().getTime());
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                } catch (RealmError e) {
                    e.printStackTrace();
                } catch (OutOfMemoryError e) {
                    e.printStackTrace();
                } finally {
                    RealmInfo.closeRealm(realm);
                }
            }
        }).start();
    }

    private void handleActionBookmark(int id, boolean bookmark) {
        // TODO: Handle action Foo
        new Thread(new Runnable() {
            @Override
            public void run() {
                Realm realm = null;
                try {
                    realm = RealmInfo.getRealm(getApplicationContext());
                    if (UserInfo.isLogin()) {
                        NewsInfo newsInfo = new NewsInfo();
                        realm = RealmInfo.getRealm(getApplicationContext());
                        new BookmarkInfo().bookmark(getApplicationContext(), realm, id, UserInfo.getCurrentUserId(), BookmarkInfo.BookmarkType.News, bookmark);
                        newsInfo.bookmark(getApplicationContext(), id, bookmark);
                        Intent intent = new Intent(Constants.ACTION_NEWS_BOOKMARK_SUCCESS);
                        getApplicationContext().sendBroadcast(intent);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                } catch (RealmError e) {
                    e.printStackTrace();
                } catch (OutOfMemoryError e) {
                    e.printStackTrace();
                } finally {
                    RealmInfo.closeRealm(realm);
                }
            }
        }).start();
    }

}
