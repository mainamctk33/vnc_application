package com.team500ae.vncapplication.activities.translator;

import com.google.gson.JsonObject;

import java.util.List;

/**
 * Created by thanh.tran on 5/28/2018.
 */
public class TranslatorModel {
    private String headerTitle;
    private JsonObject jsonObject;

    public TranslatorModel(String headerTitle, JsonObject jsonObject) {
        this.headerTitle = headerTitle;
        this.jsonObject = jsonObject;
    }

    public TranslatorModel(JsonObject jsonObject) {
        this.jsonObject = jsonObject;
    }

    public String getHeaderTitle() {
        return headerTitle;
    }

    public JsonObject getJsonObject() {
        return jsonObject;
    }
}
