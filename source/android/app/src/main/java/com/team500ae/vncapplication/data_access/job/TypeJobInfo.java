package com.team500ae.vncapplication.data_access.job;

import android.content.Context;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.team500ae.library.utils.ConvertUtils;
import com.team500ae.vncapplication.R;
import com.team500ae.vncapplication.constants.Constants;
import com.team500ae.vncapplication.data_access.ClientInfo;
import com.team500ae.vncapplication.models.BaseReturnFunctionEntity;
import com.team500ae.vncapplication.models.ServiceResponseEntity;
import com.team500ae.vncapplication.models.realm_models.job.MyTypeJobEntity;
import com.team500ae.vncapplication.models.realm_models.job.TypeJobEntity;

import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmResults;
import io.realm.exceptions.RealmError;

/**
 * Created by SVIP on 11/30/2017.
 */

public class TypeJobInfo {

    public ServiceResponseEntity<JsonArray> getAll(Context context) {
        try {
            ClientInfo.DataResponse dataResponse = new ClientInfo().requestApi(ClientInfo.MethodType.GET, Constants.API_TYPE_JOB_GETALL);
            if (dataResponse.isOK()) {
                ServiceResponseEntity<JsonArray> result = dataResponse.getData(new TypeToken<ServiceResponseEntity<JsonArray>>() {
                }.getType());
                return result;
            }
        } catch (UnknownHostException e) {
            e.printStackTrace();
        } catch (SocketTimeoutException e) {
            e.printStackTrace();
        }
        return null;
    }

    public ServiceResponseEntity<JsonArray> syncType(Context context, long lastSyncDate) {
        try {
            ClientInfo.DataResponse dataResponse = new ClientInfo().requestApi(ClientInfo.MethodType.GET, Constants.API_TYPE_JOB_SYNC + "/" + lastSyncDate);
            if (dataResponse.isOK()) {

                ServiceResponseEntity<JsonArray> result = dataResponse.getData(new TypeToken<ServiceResponseEntity<JsonArray>>() {
                }.getType());
                return result;
            }
        } catch (UnknownHostException e) {
            e.printStackTrace();
        } catch (SocketTimeoutException e) {
            e.printStackTrace();
        }
        return null;
    }

    public BaseReturnFunctionEntity<Boolean> removeTypeJobFromListFavorite(Context context, Realm realm, String userId, int... listTypeJobIds) {
        if (listTypeJobIds == null || listTypeJobIds.length == 0)
            return new BaseReturnFunctionEntity<>(BaseReturnFunctionEntity.StatusFunction.FALSE, context.getString(R.string.select_typejob_to_remove));
        try {
            realm.executeTransaction(realm1 -> {
                for (int id : listTypeJobIds) {
                    MyTypeJobEntity myTypeJobEntity = realm.where(MyTypeJobEntity.class).equalTo("id", userId + id).findFirst();
                    if (myTypeJobEntity != null)
                        myTypeJobEntity.deleteFromRealm();
                }
            });
            return new BaseReturnFunctionEntity<>(BaseReturnFunctionEntity.StatusFunction.TRUE, true);
        } catch (RealmError e) {
            return new BaseReturnFunctionEntity<>(BaseReturnFunctionEntity.StatusFunction.EXCEPTION, e.getMessage());
        } catch (Exception e) {
            return new BaseReturnFunctionEntity<>(BaseReturnFunctionEntity.StatusFunction.EXCEPTION, e.getMessage());
        } catch (OutOfMemoryError e) {
            return new BaseReturnFunctionEntity<>(BaseReturnFunctionEntity.StatusFunction.EXCEPTION, e.getMessage());
        }
    }


    public BaseReturnFunctionEntity<Boolean> saveTypeJobToFavorite(Context context, Realm realm, String userId, int... listTypeJobIds) {
        if (listTypeJobIds == null || listTypeJobIds.length == 0)
            return new BaseReturnFunctionEntity<>(BaseReturnFunctionEntity.StatusFunction.FALSE, context.getString(R.string.select_typejob_to_set_favorite));
        try {
            realm.executeTransaction(realm1 -> {
                for (int id : listTypeJobIds) {
                    TypeJobEntity typeJobEntity = realm.where(TypeJobEntity.class).equalTo("id", id).findFirst();
                    if (typeJobEntity != null) {
                        MyTypeJobEntity myTypeJobEntity = new MyTypeJobEntity();
                        myTypeJobEntity.setId(userId + id);
                        myTypeJobEntity.setTypeJob(typeJobEntity);
                        myTypeJobEntity.setUserId(userId);
                        realm.copyToRealmOrUpdate(myTypeJobEntity);
                    }
                }
            });
            return new BaseReturnFunctionEntity<>(BaseReturnFunctionEntity.StatusFunction.TRUE, true);
        } catch (RealmError e) {
            return new BaseReturnFunctionEntity<>(BaseReturnFunctionEntity.StatusFunction.EXCEPTION, e.getMessage());
        } catch (Exception e) {
            return new BaseReturnFunctionEntity<>(BaseReturnFunctionEntity.StatusFunction.EXCEPTION, e.getMessage());
        } catch (OutOfMemoryError e) {
            return new BaseReturnFunctionEntity<>(BaseReturnFunctionEntity.StatusFunction.EXCEPTION, e.getMessage());
        }
    }

    public BaseReturnFunctionEntity<Boolean> updateTypeJobToFavorite(Context context, Realm realm, String userId, ArrayList<TypeJobEntity> listTypeJobIds) {
        if (listTypeJobIds == null || listTypeJobIds.size() == 0)
            return new BaseReturnFunctionEntity<>(BaseReturnFunctionEntity.StatusFunction.FALSE, context.getString(R.string.select_typejob_to_set_favorite));
        try {
            realm.executeTransaction(realm1 -> {
                for (int i = 0; i < listTypeJobIds.size(); i++) {
                    TypeJobEntity typeJob = listTypeJobIds.get(i);
                    TypeJobEntity typeJobEntity = realm.where(TypeJobEntity.class).equalTo("id", typeJob.getId()).findFirst();
                    if (typeJobEntity != null) {
                        MyTypeJobEntity myTypeJobEntity = new MyTypeJobEntity();
                        myTypeJobEntity.setId(userId + typeJob.getId());
                        myTypeJobEntity.setTypeJob(typeJobEntity);
                        myTypeJobEntity.setUserId(userId);
                        myTypeJobEntity.setPosition(i);
                        realm.copyToRealmOrUpdate(myTypeJobEntity);
                    }
                }
            });
            return new BaseReturnFunctionEntity<>(BaseReturnFunctionEntity.StatusFunction.TRUE, true);
        } catch (RealmError e) {
            return new BaseReturnFunctionEntity<>(BaseReturnFunctionEntity.StatusFunction.EXCEPTION, e.getMessage());
        } catch (Exception e) {
            return new BaseReturnFunctionEntity<>(BaseReturnFunctionEntity.StatusFunction.EXCEPTION, e.getMessage());
        } catch (OutOfMemoryError e) {
            return new BaseReturnFunctionEntity<>(BaseReturnFunctionEntity.StatusFunction.EXCEPTION, e.getMessage());
        }
    }

    public BaseReturnFunctionEntity<ArrayList<TypeJobEntity>> getListTypeJob(Context context, Realm realm) {
        try {
            RealmResults<TypeJobEntity> realmList = realm.where(TypeJobEntity.class).findAll();
            ArrayList<TypeJobEntity> result = new ArrayList<>();
            result.addAll(realmList);
            return new BaseReturnFunctionEntity<>(BaseReturnFunctionEntity.StatusFunction.TRUE, result);
        } catch (RealmError e) {
            return new BaseReturnFunctionEntity<>(BaseReturnFunctionEntity.StatusFunction.EXCEPTION, e.getMessage());
        } catch (Exception e) {
            return new BaseReturnFunctionEntity<>(BaseReturnFunctionEntity.StatusFunction.EXCEPTION, e.getMessage());
        } catch (OutOfMemoryError e) {
            return new BaseReturnFunctionEntity<>(BaseReturnFunctionEntity.StatusFunction.EXCEPTION, e.getMessage());
        }
    }

    public BaseReturnFunctionEntity<ArrayList<TypeJobEntity>> getTypeJobHasSelected(Context context, Realm realm, String userId) {
        try {
            RealmResults<MyTypeJobEntity> realmList = realm.where(MyTypeJobEntity.class).equalTo("userId", userId).findAll();
            ArrayList<TypeJobEntity> result = new ArrayList<>();
            for (MyTypeJobEntity typeJobEntity : realmList.sort("position")) {
                result.add(typeJobEntity.getTypeJob());
            }
            return new BaseReturnFunctionEntity<>(BaseReturnFunctionEntity.StatusFunction.TRUE, result);
        } catch (RealmError e) {
            return new BaseReturnFunctionEntity<>(BaseReturnFunctionEntity.StatusFunction.EXCEPTION, e.getMessage());
        } catch (Exception e) {
            return new BaseReturnFunctionEntity<>(BaseReturnFunctionEntity.StatusFunction.EXCEPTION, e.getMessage());
        } catch (OutOfMemoryError e) {
            return new BaseReturnFunctionEntity<>(BaseReturnFunctionEntity.StatusFunction.EXCEPTION, e.getMessage());
        }
    }

    public BaseReturnFunctionEntity<Boolean> update(Realm realm, JsonArray data) {
        try {
            RealmResults<TypeJobEntity> realmList = realm.where(TypeJobEntity.class).findAll();
            List<Integer> listID = new ArrayList<>();
            realm.executeTransaction(realm1 -> {
                for (int i = 0; i < data.size(); i++) {
                    JsonObject jsonObject = ConvertUtils.toJsonObject(data.get(i));
                    int idData = ConvertUtils.toInt(jsonObject.get("ID"));
                    listID.add(idData);
                    update(realm, jsonObject, false);
                }
            });

            for (int j = 0; j < realmList.size(); j++) {
                int idRealm = realmList.get(j).getId();
                if (!listID.contains(idRealm)) {
                    realm.executeTransaction(new Realm.Transaction() {
                        @Override
                        public void execute(Realm realm) {
                            RealmResults<TypeJobEntity> result = realm.where(TypeJobEntity.class).equalTo("id", idRealm).findAll();
                            result.deleteAllFromRealm();
                        }
                    });

                }
            }
            return new BaseReturnFunctionEntity<>(BaseReturnFunctionEntity.StatusFunction.TRUE, true);
        } catch (
                RealmError e)

        {
            return new BaseReturnFunctionEntity<>(BaseReturnFunctionEntity.StatusFunction.EXCEPTION, e.getMessage());
        } catch (
                Exception e)

        {
            return new BaseReturnFunctionEntity<>(BaseReturnFunctionEntity.StatusFunction.EXCEPTION, e.getMessage());
        } catch (
                OutOfMemoryError e)

        {
            return new BaseReturnFunctionEntity<>(BaseReturnFunctionEntity.StatusFunction.EXCEPTION, e.getMessage());
        }

    }

    public BaseReturnFunctionEntity<Boolean> update(Realm realm, final JsonObject data, boolean needTransacsion) {
        try {
            if (needTransacsion) {
                realm.executeTransaction(realm1 -> {
                    update(realm, data, false);
                });
            } else {
                int id = ConvertUtils.toInt(data.get("ID"));
                TypeJobEntity typeJobEntity = new TypeJobEntity();
                typeJobEntity.setName(ConvertUtils.toString(data.get("Name")));
                typeJobEntity.setName2(ConvertUtils.toString(data.get("Name2")));
                typeJobEntity.setId(id);
                realm.copyToRealmOrUpdate(typeJobEntity);
            }
            return new BaseReturnFunctionEntity<>(BaseReturnFunctionEntity.StatusFunction.EXCEPTION, true);
        } catch (RealmError e) {
            return new BaseReturnFunctionEntity<>(BaseReturnFunctionEntity.StatusFunction.EXCEPTION, e.getMessage());
        } catch (Exception e) {
            return new BaseReturnFunctionEntity<>(BaseReturnFunctionEntity.StatusFunction.EXCEPTION, e.getMessage());
        } catch (OutOfMemoryError e) {
            return new BaseReturnFunctionEntity<>(BaseReturnFunctionEntity.StatusFunction.EXCEPTION, e.getMessage());
        }
    }

}
