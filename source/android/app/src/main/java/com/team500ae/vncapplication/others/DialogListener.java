package com.team500ae.vncapplication.others;
/*
 * Created by HoangDong on 13/06/2017.
 */

public interface DialogListener {
    void onConfirmClicked();
    void onButtonChooseImage();
}
