package com.team500ae.vncapplication.activities.settings;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.team500ae.library.utils.ConvertUtils;
import com.team500ae.vncapplication.R;
import com.team500ae.vncapplication.base.BaseActivity;
import com.team500ae.vncapplication.data_access.question.QuestionInfo;
import com.team500ae.vncapplication.models.QuestionObject;
import com.team500ae.vncapplication.models.ServiceResponseEntity;

import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;

/**
 * Created by Administrator on 19/05/2018.
 */

public class CommonQuestionActivity extends BaseActivity {

    private SearchView svSearch;

    private RecyclerView recyclerView;
    private QuestionAdapter questionAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private ArrayList<QuestionObject> listQuestion;

    public static void open(Context activity) {
        Intent intent = new Intent(activity, CommonQuestionActivity.class);
        activity.startActivity(intent);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        ButterKnife.bind(this);
        super.onCreate(savedInstanceState);
        showBackButton();
        setContentView(R.layout.activity_show_list_question);

        initCollapsingToolbar();

        svSearch = (SearchView) findViewById(R.id.sv_search);
        svSearch.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                if (questionAdapter != null) questionAdapter.getFilter().filter(newText);
                return true;
            }
        });
//        EditText searchEditText = (EditText) svSearch.findViewById(android.support.v7.appcompat.R.id.search_src_text);
//        searchEditText.setTextColor(getResources().getColor(R.color.black));
//        searchEditText.setHintTextColor(getResources().getColor(R.color.black));
//        ImageView searchIcon = svSearch.findViewById(android.support.v7.appcompat.R.id.search_button);
//        searchIcon.setImageDrawable(ContextCompat.getDrawable(getActivity(),R.drawable.ic_menu_find_job));


        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        listQuestion = new ArrayList<>();
        questionAdapter = new QuestionAdapter(this, listQuestion);

        recyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setAdapter(questionAdapter);

//        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(this, 1);
//        recyclerView.setLayoutManager(mLayoutManager);
//        recyclerView.addItemDecoration(new GridSpacingItemDecoration(2, dpToPx(10), true));
//        recyclerView.setItemAnimator(new DefaultItemAnimator());
//        recyclerView.setAdapter(adapter);

        TextView tvBackdropSubTitle = (TextView) findViewById(R.id.tv_backdrop_subtitle);
        tvBackdropSubTitle.setText(getResources().getString(R.string.backdrop_subtitle_common_question));

        loadListQuestion();
        try {
            Glide.with(this).load(R.drawable.bg_landscape_no_blur).into((ImageView) findViewById(R.id.backdrop));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void loadListQuestion() {

        new AsyncTask<Void, Void, JsonArray>() {
            ProgressDialog progDialog = new ProgressDialog(CommonQuestionActivity.this);

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                progDialog.setMessage(getString(R.string.activity_loading_question));
                progDialog.setIndeterminate(false);
                progDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                progDialog.setCancelable(true);
                progDialog.show();
            }

            @Override
            protected JsonArray doInBackground(Void... voids) {
                ServiceResponseEntity<JsonArray> result = QuestionInfo.getListCommonQuestions(getApplication());
                if (result != null && result.getStatus() == 0) {
                    JsonArray jsonArray = result.getData();
                    return jsonArray;
//                    updateListQuestions(jsonArray);
//                    publishProgress(jsonArray);
                }

                return null;
            }

            @Override
            protected void onPostExecute(JsonArray values) {
                if (progDialog.isShowing()) {
                    progDialog.dismiss();
                }
                updateListQuestions(values);
            }
        }.execute();
    }

    private void updateListQuestions(JsonArray jsonArray) {
        QuestionObject questionObject;
        if (jsonArray != null) {
            String type, title, question, answer;

            for (int i = 0; i < jsonArray.size(); i++) {
                JsonObject jsonObject = ConvertUtils.toJsonObject(jsonArray.get(i));

                type = ConvertUtils.toString(jsonObject.get("Type"));
                title = ConvertUtils.toString(jsonObject.get("Title"));
                question = ConvertUtils.toString(jsonObject.get("Content"));
                answer = ConvertUtils.toString(jsonObject.get("Answer"));
                questionObject = new QuestionObject(type, title, question, answer);
                listQuestion.add(questionObject);
            }

        }
        questionAdapter.notifyDataSetChanged();
    }

    /**
     * Initializing collapsing toolbar
     * Will show and hide the toolbar title on scroll
     */
    private void initCollapsingToolbar() {
        final CollapsingToolbarLayout collapsingToolbar =
                (CollapsingToolbarLayout) findViewById(R.id.collapsing_toolbar);
        collapsingToolbar.setTitle(" ");
        AppBarLayout appBarLayout = (AppBarLayout) findViewById(R.id.appbar);
        appBarLayout.setExpanded(true);

        // hiding & showing the title when toolbar expanded & collapsed
        appBarLayout.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
            boolean isShow = false;
            int scrollRange = -1;

            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                scrollRange = appBarLayout.getTotalScrollRange();
                if (scrollRange == -1) {
                    scrollRange = appBarLayout.getTotalScrollRange();
                }
                if (scrollRange + verticalOffset == 0) {
                    collapsingToolbar.setTitle(getString(R.string.pref_info_common_questions_title));
                    isShow = true;
                } else if (isShow) {
                    collapsingToolbar.setTitle(" ");
                    isShow = false;
                }
            }
        });
    }
}