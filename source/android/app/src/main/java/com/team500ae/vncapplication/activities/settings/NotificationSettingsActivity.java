package com.team500ae.vncapplication.activities.settings;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.TextView;

import com.google.gson.JsonObject;
import com.team500ae.library.utils.ConvertUtils;
import com.team500ae.vncapplication.R;
import com.team500ae.vncapplication.base.BaseActivity;
import com.team500ae.vncapplication.data_access.user.UserInfo;
import com.team500ae.vncapplication.models.BaseReturnFunctionEntity;
import com.team500ae.vncapplication.utils.FlagAttributeUtils;

import butterknife.BindView;

/**
 * Created by Administrator on 01/07/2018.
 */

public class NotificationSettingsActivity extends BaseActivity {

    @BindView(R.id.layout_show_news_notif)
    View vNewsNotify;
    @BindView(R.id.layout_show_job_notif)
    View vJobNotify;
    @BindView(R.id.layout_show_tips_notif)
    View vTipsNotify;
    @BindView(R.id.layout_show_friend_notif)
    View vFriendNotify;

    private Switch swtShowNewsNotify, swtShowJobNotify, swtShowTipsNotify, swtShowFriendNotify;


    public static void open(Context activity) {
        Intent intent = new Intent(activity, NotificationSettingsActivity.class);
        activity.startActivity(intent);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        setContentView(R.layout.activity_notification_setting);
        super.onCreate(savedInstanceState);
        showBackButton();

        initLayout();
        int settingAttributeValue = ConvertUtils.toInt(UserInfo.getCurrentUser().get("SettingAttribute"));

        swtShowNewsNotify.setChecked(FlagAttributeUtils.getAttribute(settingAttributeValue, FlagAttributeUtils.SHOW_NEWS_NOTIFICATION));
        swtShowJobNotify.setChecked(FlagAttributeUtils.getAttribute(settingAttributeValue, FlagAttributeUtils.SHOW_JOB_NOTIFICATION));
        swtShowFriendNotify.setChecked(FlagAttributeUtils.getAttribute(settingAttributeValue, FlagAttributeUtils.SHOW_FRIEND_NOTIFICATION));
        swtShowTipsNotify.setChecked(FlagAttributeUtils.getAttribute(settingAttributeValue, FlagAttributeUtils.SHOW_TIPS_NOTIFICATION));

        showNotificationEvent(FlagAttributeUtils.SHOW_JOB_NOTIFICATION, swtShowJobNotify);
        showNotificationEvent(FlagAttributeUtils.SHOW_NEWS_NOTIFICATION, swtShowNewsNotify);
        showNotificationEvent(FlagAttributeUtils.SHOW_FRIEND_NOTIFICATION, swtShowFriendNotify);
        showNotificationEvent(FlagAttributeUtils.SHOW_TIPS_NOTIFICATION, swtShowTipsNotify);
    }

    private void initLayout() {
        TextView tvJob = (TextView) vJobNotify.findViewById(R.id.tv_title);
        tvJob.setText(getResources().getString(R.string.pref_notification_show_job_notif));
        swtShowJobNotify = (Switch) vJobNotify.findViewById(R.id.switch_block);
        TextView tvSubJob=(TextView)vJobNotify.findViewById(R.id.tv_sub_title);
        tvSubJob.setText(getResources().getString(R.string.pref_notification_show_job_notif_sub));

        TextView tvNews = (TextView) vNewsNotify.findViewById(R.id.tv_title);
        tvNews.setText(getResources().getString(R.string.pref_notification_show_news_notif));
        swtShowNewsNotify = (Switch) vNewsNotify.findViewById(R.id.switch_block);
        TextView tvSubNews=(TextView)vNewsNotify.findViewById(R.id.tv_sub_title);
        tvSubNews.setText(getResources().getString(R.string.pref_notification_show_news_notif_sub));

        TextView tvTips = (TextView) vTipsNotify.findViewById(R.id.tv_title);
        tvTips.setText(getResources().getString(R.string.pref_notification_show_tips_notif));
        swtShowTipsNotify = (Switch) vTipsNotify.findViewById(R.id.switch_block);
        TextView tvSubTips=(TextView)vTipsNotify.findViewById(R.id.tv_sub_title);
        tvSubTips.setText(getResources().getString(R.string.pref_notification_show_tips_notif_sub));

        TextView tvFriend = (TextView) vFriendNotify.findViewById(R.id.tv_title);
        tvFriend.setText(getResources().getString(R.string.pref_notification_show_friend_notif));
        swtShowFriendNotify = (Switch) vFriendNotify.findViewById(R.id.switch_block);
        TextView tvSubFriend=(TextView)vFriendNotify.findViewById(R.id.tv_sub_title);
        tvSubFriend.setText(getResources().getString(R.string.pref_notification_show_friend_notif_sub));
    }

    private void showNotificationEvent(int value, Switch swt) {
        swt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int settingAttributeValue = ConvertUtils.toInt(UserInfo.getCurrentUser().get("SettingAttribute"));
                boolean desireStatus = FlagAttributeUtils.getAttribute(settingAttributeValue, value);

                //update switch status
                int desireValue = desireStatus ? (settingAttributeValue ^ value) : (settingAttributeValue | value);

                //call API to update
                new AsyncTask<Void, Void, BaseReturnFunctionEntity<Boolean>>() {
                    ProgressDialog progDialog = new ProgressDialog(NotificationSettingsActivity.this);

                    @Override
                    protected void onPreExecute() {
                        super.onPreExecute();
                        progDialog.setMessage(getString(R.string.activity_update_user_notification_setting));
                        progDialog.setIndeterminate(false);
                        progDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                        progDialog.setCancelable(true);
                        progDialog.show();
                    }

                    @Override
                    protected BaseReturnFunctionEntity<Boolean> doInBackground(Void... voids) {
                        try {
                            Thread.sleep(2000);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        return UserInfo.updateSettingAttribute(desireValue);
                    }

                    @Override
                    protected void onPostExecute(BaseReturnFunctionEntity<Boolean> result) {
                        super.onPostExecute(result);
                        if (progDialog.isShowing()) {
                            progDialog.dismiss();
                        }

                        if (result.isTrue()) {
                            UserInfo.updateLocalSettingAttribute(getActivity(), desireValue);
                            swt.setChecked(!desireStatus);
                            showSnackBar(R.string.activity_update_user_notification_setting_successfully);
                        } else {
                            swt.setChecked(desireStatus);
                            showSnackBar(R.string.activity_update_user_notification_setting_failed + ": " + result.getMessage());
                        }
                    }
                }.execute();
            }
        });
    }

}
