package com.team500ae.vncapplication.models;

/**
 * Created by Administrator on 11/01/2018.
 */

public class JSONUser {
    private String Id;
    private String UserName;
    private String Email;
    private String FullName;
    private int age;
    private String ImageUrl;
    private String Address;
    //    private String PassWord;
    private boolean ConfirmAccount;
    private String Token;
    private String PhoneNumber;
    private String DateOfBirth;
    private String Job;
    private String Sex;
    private String Description;
    private String LastLogin;
    private String PassWord;

    public String getSex() {
        return Sex;
    }

    public JSONUser setSex(String sex) {
        Sex = sex;
        return this;
    }

    public String getJob() {
        return Job;
    }

    public JSONUser setJob(String job) {
        Job = job;
        return this;
    }

    public String getDateOfBirth() {
        return DateOfBirth;
    }

    public JSONUser setDateOfBirth(String dateOfBirth) {
        DateOfBirth = dateOfBirth;
        return this;
    }

    public String getId() {
        return Id;
    }

    public JSONUser setId(String id) {
        Id = id;
        return this;
    }

    public String getPhoneNumber() {
        return PhoneNumber;
    }

    public JSONUser setPhoneNumber(String phoneNumber) {
        PhoneNumber = phoneNumber;
        return this;
    }

    public String getEmail() {
        return Email;
    }

    public JSONUser setEmail(String email) {
        this.Email = email;
        return this;
    }

    public String getFullName() {
        return FullName;
    }

    public JSONUser setFullName(String fullName) {
        this.FullName = fullName;
        return this;
    }

    public String getImageUrl() {
        return (ImageUrl == null || ImageUrl.isEmpty()) ? "abc.jpg" : ImageUrl;
    }

    public JSONUser setImageUrl(String imageUrl) {
        this.ImageUrl = imageUrl;
        return this;
    }

    public String getUserName() {
        return UserName;
    }

    public JSONUser setUserName(String userName) {
        this.UserName = userName;
        return this;
    }

    public String getAddress() {
        return Address;
    }

    public JSONUser setAddress(String address) {
        this.Address = address;
        return this;
    }

    public String getDescription() {
        return Description;
    }

    public JSONUser setDescription(String description) {
        Description = description;
        return this;
    }

    public String getLastLogin() {
        return LastLogin;
    }

    public JSONUser setLastLogin(String lastLogin) {
        LastLogin = lastLogin;
        return this;
    }

    public String getPassWord() {
        return PassWord;
    }

    public void setPassWord(String passWord) {
        PassWord = passWord;
    }
}
