package com.team500ae.vncapplication.activities.location;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;

import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.GoogleApiClient;

import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.team500ae.vncapplication.R;
import com.team500ae.vncapplication.activities.friend.fragments.FriendAroundFragment;
import com.team500ae.vncapplication.activities.location.fragments.LocationSearchFragment;
import com.team500ae.vncapplication.base.BaseActivity;
import com.team500ae.vncapplication.constants.Constants;

import butterknife.OnClick;

/**
 * Created by maina on 1/29/2018.
 */

public class LocationSearchActivity extends BaseActivity implements com.team500ae.library.activities.BaseActivity.OnCallBackBroadcastListener {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        setContentView(R.layout.activity_location_search);
        super.onCreate(savedInstanceState);
        getSupportFragmentManager().beginTransaction().replace(R.id.lnFragment, LocationSearchFragment.getInstants(), "searchLocation").commit();
        registerBroadcastReceiver(
                new ReceiverItem(Constants.ACTION_LOGIN, this),
                new ReceiverItem(Constants.ACTION_LOGOUT, this),
                new ReceiverItem(Constants.ACTION_TYPE_LOCATION_FOR_USER_CHANGED, this));

    }

    public static void open(AppCompatActivity activity) {
        Intent intent = new Intent(activity, LocationSearchActivity.class);
        activity.startActivity(intent);
    }

    @Override
    public void onCallBackBroadcastListener(Context context, String action, Intent intent) {
        switch (action) {
            case Constants.ACTION_LOGIN:
            case Constants.ACTION_LOGOUT:
            case Constants.ACTION_TYPE_LOCATION_FOR_USER_CHANGED:
                getSupportFragmentManager().beginTransaction().replace(R.id.lnFragment, LocationSearchFragment.getInstants(), "fragment").commitAllowingStateLoss();
                break;
        }
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(Constants.ACTION_LOCATION_BACK_TO_MAIN_APP);
        getApplication().sendBroadcast(intent);
        super.onBackPressed();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.lnFragment);
        if (fragment != null && fragment instanceof LocationSearchFragment)
            fragment.onActivityResult(requestCode, resultCode, data);
    }
}
