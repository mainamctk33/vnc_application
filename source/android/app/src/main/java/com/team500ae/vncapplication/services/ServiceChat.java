package com.team500ae.vncapplication.services;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.sendbird.android.SendBird;
import com.team500ae.library.utils.ConvertUtils;
import com.team500ae.vncapplication.data_access.groupchat.ChatInfo;

import org.json.JSONArray;

import java.util.Arrays;
import java.util.List;

public class ServiceChat extends IntentService {
    private static final String ACTION_CREATE_USER = "ACTION_CREATE_USER";
    private static final String ACTION_LEAVE_GROUP = "ACTION_LEAVE_GROUP";
    private static final String ACTION_INVITE_MEMBER = "ACTION_INVITE_MEMBER";
    private static final  String ACTION_UPDATE_GROUP = "ACTION_UPDATE_GROUP";

    public ServiceChat() {
        super("ServiceChat");
    }

    public static void startActionCreateChatAccount(Context context, String username, String fullname, String avatar) {
        Intent intent = new Intent(context, ServiceChat.class);
        intent.setAction(ACTION_CREATE_USER);
        intent.putExtra("USER_NAME", username);
        intent.putExtra("FULL_NAME", fullname);
        intent.putExtra("AVATAR", avatar);
        context.startService(intent);
    }

    public static void startActionCreateChatAccount(Context context, String username) {
        Intent intent = new Intent(context, ServiceChat.class);
        intent.setAction(ACTION_CREATE_USER);
        intent.putExtra("USER_NAME", username);
        intent.putExtra("FULL_NAME", "");
        intent.putExtra("AVATAR", "");
        context.startService(intent);
    }

    public static void startActionLeaveGroup(Context context, String username, String fullname, String channelUrl) {
        Intent intent = new Intent(context, ServiceChat.class);
        intent.setAction(ACTION_LEAVE_GROUP);
        intent.putExtra("USER_NAME", username);
        intent.putExtra("FULL_NAME", fullname);
        intent.putExtra("CHANNEL_URL", channelUrl);
        context.startService(intent);
    }

    public static void startActionUpdateGroup(Context context, String username, String fullname, String channelUrl) {
        Intent intent = new Intent(context, ServiceChat.class);
        intent.setAction(ACTION_UPDATE_GROUP);
        intent.putExtra("USER_NAME", username);
        intent.putExtra("FULL_NAME", fullname);
        intent.putExtra("CHANNEL_URL", channelUrl);
        context.startService(intent);
    }


    public static void startActionInviteMember(Context context, String username, String fullname, List<String> mSelectedUserIds, List<String> mSelectedUserFullname, String channelUrl) {
        Intent intent = new Intent(context, ServiceChat.class);
        intent.setAction(ACTION_INVITE_MEMBER);
        intent.putExtra("USER_NAME", username);
        intent.putExtra("FULL_NAME", fullname);
        intent.putExtra("CHANNEL_URL", channelUrl);
        JsonArray jsonArray = new JsonArray();
        for (int i = 0; i < mSelectedUserIds.size(); i++) {
            JsonObject jsonObject = new JsonObject();
            jsonObject.addProperty("userId", mSelectedUserIds.get(i));
            jsonObject.addProperty("nickname", mSelectedUserFullname.get(i));
            jsonArray.add(jsonObject);
        }
        intent.putExtra("MEMBERS", ConvertUtils.toJson(jsonArray));
        context.startService(intent);

    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        if (intent != null) {
            final String action = intent.getAction();
            String username;
            String fullname;
            String avatar;
            String channelUrl;
            switch (action) {
                case ACTION_LEAVE_GROUP:
                    username = intent.getStringExtra("USER_NAME");
                    fullname = intent.getStringExtra("FULL_NAME");
                    channelUrl = intent.getStringExtra("CHANNEL_URL");
                    handleLeaveGroup(username, fullname, channelUrl);
                    break;
                case ACTION_UPDATE_GROUP:
                    username = intent.getStringExtra("USER_NAME");
                    fullname = intent.getStringExtra("FULL_NAME");
                    channelUrl = intent.getStringExtra("CHANNEL_URL");
                    handleUpdateGroup(username, fullname, channelUrl);
                    break;
                case ACTION_CREATE_USER:
                    username = intent.getStringExtra("USER_NAME");
                    fullname = intent.getStringExtra("FULL_NAME");
                    avatar = intent.getStringExtra("AVATAR");
                    handleCreateUser(username, fullname, avatar);
                    break;
                case ACTION_INVITE_MEMBER:
                    username = intent.getStringExtra("USER_NAME");
                    fullname = intent.getStringExtra("FULL_NAME");
                    channelUrl = intent.getStringExtra("CHANNEL_URL");
                    String members = intent.getStringExtra("MEMBERS");
                    handleInviteMember(username, fullname, channelUrl, members);
                    break;

            }
        }
    }

    private void handleInviteMember(String username, String fullname, String channelUrl, String members) {
        try {
            new Thread(() -> {
                new ChatInfo().inviteMember(getApplicationContext(), username, fullname, channelUrl, members);
            }).start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void handleLeaveGroup(String username, String fullname, String channelUrl) {
        try {
            new Thread(() -> {
                new ChatInfo().leaveGroup(getApplicationContext(), username, fullname, channelUrl);
            }).start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void handleUpdateGroup(String username, String fullname, String channelUrl) {
        try {
            new Thread(() -> {
                new ChatInfo().updateGroup(getApplicationContext(), username, fullname, channelUrl);
            }).start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void handleCreateUser(String username, String fullname, String avatar) {
        try {
            new Thread(() -> {
                new ChatInfo().createChatAccount(getApplicationContext(), username, fullname, avatar);
            }).start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
