package com.team500ae.vncapplication.activities.translator.fragments;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.team500ae.library.adapters.BaseRecyclerAdapter;
import com.team500ae.library.layoutmanager.MyLinearLayoutManager;
import com.team500ae.library.utils.ConvertUtils;
import com.team500ae.library.utils.RecyclerViewUtils;
import com.team500ae.vncapplication.R;
import com.team500ae.vncapplication.activities.translator.TranslatorDetailActivity;
import com.team500ae.vncapplication.base.BaseViewHolder;
import com.team500ae.vncapplication.constants.Constants;
import com.team500ae.vncapplication.data_access.translator.TranslatorInfo;
import com.team500ae.vncapplication.models.ServiceResponseEntity;
import com.team500ae.vncapplication.others.GridSpacingItemDecoration;
import com.team500ae.vncapplication.utils.AppUtils;
import com.team500ae.vncapplication.utils.LanguageUtils;
import com.team500ae.vncapplication.utils.SharedPrefUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by TamPC on 3/24/2018.
 */

public class TranslatorTabFragment extends Fragment {
    private static final String TAG = TranslatorTabFragment.class.getSimpleName();
    private int type;
    private BaseRecyclerAdapter<ViewHolder, JsonObject> adapter;
    private HashMap<Integer, Integer> iconMap;
    private List<Integer> listColors;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        Bundle bundle = getArguments();
        if (bundle != null)
            type = bundle.getInt("type");
        View view = inflater.inflate(R.layout.fragment_tab_translator_fragment, container, false);
        ButterKnife.bind(this, view);

        recyclerView.setLayoutManager(new MyLinearLayoutManager(getContext()));
        //ho tro load next page
        RecyclerViewUtils recyclerViewUtils = new RecyclerViewUtils(new RecyclerViewUtils.RecyclerViewUtilsListener() {
            @Override
            public void onScrollToEnd() {
                if (!isLoading) {
                    //getData(page + 1, size, type, getListener());
                }
            }
        });
        recyclerViewUtils.setUp(recyclerView);
        recyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 3));
        recyclerView.setClipToPadding(false);
        GridSpacingItemDecoration decoration = new GridSpacingItemDecoration(3, getActivity().getResources().getDimensionPixelSize(R.dimen._5dp), false);
        recyclerView.addItemDecoration(decoration);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                page = 1;
                getDataTranslatorOffline();
            }
        });
        Log.d(TAG, "onCreateView: ");
        initMapIcon();
        adapter = new BaseRecyclerAdapter<ViewHolder, JsonObject>(R.layout.item_translator, new BaseRecyclerAdapter.BaseViewHolder<ViewHolder, JsonObject>() {
            @Override
            public ViewHolder getViewHolder(View v) {
                return new ViewHolder(v);
            }

            @Override
            public void bindData(RecyclerView recyclerView, ViewHolder viewHolder, JsonObject data, int position) {
                viewHolder.tvTitle.setText(LanguageUtils.getAppLanguageSetting(getActivity()).equals("en")?ConvertUtils.toString(data.get("Name")):ConvertUtils.toString(data.get("Name2")));
                //viewHolder.tvTitle.setText(ConvertUtils.toString(data.get("Name")));
                viewHolder.imageView.setImageResource(iconMap.get(ConvertUtils.toInt(data.get("ID"))));
                viewHolder.imageView.setBackgroundColor(listColors.get(position));
            }
        }, data, new BaseRecyclerAdapter.OnClickListener() {
            @Override
            public void onClick(View v, int position, Object o) {
                JsonObject jsonObject = (JsonObject) o;
                int id = ConvertUtils.toInt(jsonObject.get("ID"), 0);
                String title = LanguageUtils.getAppLanguageSetting(getActivity()).equals("en")?ConvertUtils.toString(jsonObject.get("Name")):ConvertUtils.toString(jsonObject.get("Name2"));
                TranslatorDetailActivity.open(getContext(), id, title);
            }
        });
        adapter.bindData(recyclerView);
        //getDataTranslatorDetail();
        getDataTranslatorOffline();
        //getData(page, size, type, getListener());
        return view;
    }

    private void getDataTranslatorOffline() {
        String jsonOffline = SharedPrefUtils.getString(getActivity(), Constants.JSON_DATA_TYPE_TRANSLATOR_LIST);
        if (!android.text.TextUtils.isEmpty(jsonOffline)) {
            bindData(ConvertUtils.toJsonArray(jsonOffline));
        }
    }

    private void getDataTranslatorDetail() {
        new AsyncTask<Void, Void, ServiceResponseEntity<JsonArray>>() {
            @Override
            protected void onPreExecute() {
                // showProgress();
                super.onPreExecute();

            }

            @Override
            protected ServiceResponseEntity<JsonArray> doInBackground(Void... voids) {
                ServiceResponseEntity<JsonArray> data;
                try {
                    data = new TranslatorInfo().getAll();
                } catch (Exception ex) {
                    return null;
                }
                return data;
            }


            @Override
            protected void onPostExecute(ServiceResponseEntity<JsonArray> data) {

                if (data != null) {
                    SharedPrefUtils.setString(getActivity(), Constants.JSON_DATA_TRANSLATOR_DETAIL_LIST, data.getData().toString());
                }

            }


        }.execute();
    }

    private void initMapIcon() {
        iconMap = new HashMap<>();
        iconMap.put(1, R.drawable.numbers);
        iconMap.put(2, R.drawable.timeanddate);
        iconMap.put(3, R.drawable.conversation);
        iconMap.put(4, R.drawable.greetings);
        iconMap.put(5, R.drawable.directions);
        iconMap.put(6, R.drawable.direction_word);
        iconMap.put(7, R.drawable.eatingout);
        iconMap.put(8, R.drawable.sight);
        iconMap.put(9, R.drawable.shopping);
        iconMap.put(10, R.drawable.emergency);
        iconMap.put(11, R.drawable.accommodation);
        iconMap.put(12, R.drawable.advanced_conversation);
        iconMap.put(13, R.drawable.health);
        iconMap.put(14, R.drawable.border);
        iconMap.put(15, R.drawable.questions);
        iconMap.put(16, R.drawable.places);
        iconMap.put(17, R.drawable.food);
        iconMap.put(18, R.drawable.vegetables);
        iconMap.put(19, R.drawable.fruits);
        iconMap.put(20, R.drawable.colors);
        iconMap.put(21, R.drawable.romance);
        iconMap.put(22, R.drawable.romance2);
        iconMap.put(23, R.drawable.post);
        iconMap.put(24, R.drawable.phone);
        iconMap.put(25, R.drawable.banking);
        iconMap.put(26, R.drawable.occupations);
        iconMap.put(27, R.drawable.business_talk);
        iconMap.put(28, R.drawable.informal_conversation);
        iconMap.put(29, R.drawable.hobbies);
        iconMap.put(30, R.drawable.wishes);
        iconMap.put(31, R.drawable.feelings);
        iconMap.put(32, R.drawable.body);
        iconMap.put(33, R.drawable.animals);
        iconMap.put(34, R.drawable.family);
        iconMap.put(35, R.drawable.countries);

        iconMap.put(36, R.drawable.numbers);
        iconMap.put(37, R.drawable.timeanddate);
        iconMap.put(38, R.drawable.conversation);
        iconMap.put(39, R.drawable.greetings);
        iconMap.put(40, R.drawable.directions);
        iconMap.put(41, R.drawable.direction_word);
        iconMap.put(42, R.drawable.eatingout);
        iconMap.put(43, R.drawable.sight);
        iconMap.put(44, R.drawable.shopping);
        iconMap.put(45, R.drawable.emergency);
        iconMap.put(46, R.drawable.accommodation);
        iconMap.put(47, R.drawable.advanced_conversation);
        iconMap.put(48, R.drawable.health);
        iconMap.put(49, R.drawable.border);
        iconMap.put(50, R.drawable.questions);
        iconMap.put(51, R.drawable.places);
        iconMap.put(52, R.drawable.food);
        iconMap.put(53, R.drawable.vegetables);
        iconMap.put(54, R.drawable.fruits);
        iconMap.put(55, R.drawable.romance);
        iconMap.put(56, R.drawable.romance2);
        iconMap.put(57, R.drawable.post);
        iconMap.put(58, R.drawable.phone);
        iconMap.put(59, R.drawable.banking);
        iconMap.put(60, R.drawable.occupations);
        iconMap.put(61, R.drawable.business_talk);
        iconMap.put(62, R.drawable.hobbies);
        iconMap.put(63, R.drawable.feelings);
        iconMap.put(64, R.drawable.body);
        iconMap.put(65, R.drawable.animals);
        iconMap.put(66, R.drawable.family);
        iconMap.put(67, R.drawable.countries);
        iconMap.put(68, R.drawable.cuisine);
        iconMap.put(69, R.drawable.dessert);
        iconMap.put(70, R.drawable.drinks);
        iconMap.put(71, R.drawable.slang);

        listColors = new ArrayList<>();
        listColors.add(getResources().getColor(R.color.category_read));
        listColors.add(getResources().getColor(R.color.category_grey_purple));
        listColors.add(getResources().getColor(R.color.category_orange));
        listColors.add(getResources().getColor(R.color.category_green));
        listColors.add(getResources().getColor(R.color.category_dark_primary));
        listColors.add(getResources().getColor(R.color.category_light_primary));
        listColors.add(getResources().getColor(R.color.category_purple));
        listColors.add(getResources().getColor(R.color.category_grey));
        listColors.add(getResources().getColor(R.color.category_light_blue));
        listColors.add(getResources().getColor(R.color.category_blue));
        listColors.add(getResources().getColor(R.color.category_yellow));
        listColors.add(getResources().getColor(R.color.category_light_grey));
        listColors.add(getResources().getColor(R.color.niceBlue2));
        listColors.add(getResources().getColor(R.color.groupChatReadReceiptMe));
        listColors.add(getResources().getColor(R.color.groupChatReadReceiptOther));
        listColors.add(getResources().getColor(R.color.background_color));
        listColors.add(getResources().getColor(R.color.pale_teal));
        listColors.add(getResources().getColor(R.color.dull_teal));
        listColors.add(getResources().getColor(R.color.btn_press));
        listColors.add(getResources().getColor(R.color.category_read));
        listColors.add(getResources().getColor(R.color.category_grey_purple));
        listColors.add(getResources().getColor(R.color.category_orange));
        listColors.add(getResources().getColor(R.color.category_green));
        listColors.add(getResources().getColor(R.color.category_dark_primary));
        listColors.add(getResources().getColor(R.color.category_light_primary));
        listColors.add(getResources().getColor(R.color.category_purple));
        listColors.add(getResources().getColor(R.color.category_grey));
        listColors.add(getResources().getColor(R.color.category_light_blue));
        listColors.add(getResources().getColor(R.color.category_blue));
        listColors.add(getResources().getColor(R.color.category_yellow));
        listColors.add(getResources().getColor(R.color.category_light_grey));
        listColors.add(getResources().getColor(R.color.niceBlue2));
        listColors.add(getResources().getColor(R.color.groupChatReadReceiptMe));
        listColors.add(getResources().getColor(R.color.groupChatReadReceiptOther));
        listColors.add(getResources().getColor(R.color.background_color));
        listColors.add(getResources().getColor(R.color.pale_teal));
        listColors.add(getResources().getColor(R.color.dull_teal));
        listColors.add(getResources().getColor(R.color.btn_press));
    }

    public static TranslatorTabFragment getInstance(int type) {
        TranslatorTabFragment tabFragment = new TranslatorTabFragment();
        Bundle bundle = new Bundle();
        bundle.putInt("type", type);
        tabFragment.setArguments(bundle);
        return tabFragment;
    }


    HashMap<Integer, JsonObject> mapData = new HashMap<>();
    ArrayList<JsonObject> data = new ArrayList<>();

    @BindView(R.id.swipeRefreshLayout)
    SwipeRefreshLayout swipeRefreshLayout;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.loading)
    View loading;
    int page = 1;
    int size = 10;
    boolean isLoading = false;

    public void getData() {
        page = 1;
        //getData(page, size, type, getListener());
    }

    public interface LoadTranslatorListener {
        public void onBeginLoad(Fragment fragment);

        public void onEndLoad(Fragment fragment);

        public JsonArray getData(int page, int size, int type, Fragment translatorTabFragment);
    }

    public LoadTranslatorListener getListener() {
        if (getContext() instanceof LoadTranslatorListener)
            return (LoadTranslatorListener) getContext();
        return null;
    }

    public class ViewHolder extends BaseViewHolder {
        @BindView(R.id.imageView)
        ImageView imageView;
        @BindView(R.id.tvTitle)
        TextView tvTitle;
//        @BindView(R.id.tvSubContent)
//        TextView tvSubContent;
//        @BindView(R.id.tvTime)
//        TextView tvTime;

        public ViewHolder(View itemView) {
            super(itemView);
        }
    }

    void bindData(JsonArray jsonArray) {
        swipeRefreshLayout.setRefreshing(false);
        data.clear();
        for (int i = 0; i < jsonArray.size(); i++) {
            JsonObject jsonObject = ConvertUtils.toJsonObject(jsonArray.get(i));
            int id = ConvertUtils.toInt(jsonObject.get("ID"));
            if (type == ConvertUtils.toInt(jsonObject.get("Category"))) {
                data.add(jsonObject);
                adapter.notifyDataSetChanged();
            }
        }
    }

    public void getData(final int page, final int size, final int type, final LoadTranslatorListener listener) {
        if (!AppUtils.isNetworkAvailable(getActivity())) {
            String jsonOffline = SharedPrefUtils.getString(getActivity(), Constants.JSON_DATA_TYPE_TRANSLATOR_LIST);
            if (!android.text.TextUtils.isEmpty(jsonOffline))
                bindData(ConvertUtils.toJsonArray(jsonOffline));
            else
                Toast.makeText(getActivity(), "Please check your connection network. The first time to sync latest data", Toast.LENGTH_SHORT).show();
        } else {
            new AsyncTask<Void, JsonArray, Void>() {

                @Override
                protected void onPreExecute() {
                    swipeRefreshLayout.setEnabled(false);
                    if (page == 1)
                        swipeRefreshLayout.setRefreshing(true);
                    else
                        loading.setVisibility(View.VISIBLE);
                    if (listener != null)
                        listener.onBeginLoad(TranslatorTabFragment.this);
                    isLoading = true;
                    super.onPreExecute();
                }

                @Override
                protected Void doInBackground(Void... voids) {

                    if (listener != null) {
                        publishProgress(listener.getData(page, size, type, TranslatorTabFragment.this));
                    } else
                        publishProgress(new JsonArray());
                    return null;
                }

                @Override
                protected void onProgressUpdate(JsonArray... values) {
                    if (values.length > 0 && values[0] != null) {
                        if (page == 1) {
                            mapData.clear();
                            data.clear();
                            adapter.notifyDataSetChanged();
                        }
                        bindData(values[0]);
                    }
                    super.onProgressUpdate(values);
                }

                @Override
                protected void onPostExecute(Void aVoid) {
                    swipeRefreshLayout.setEnabled(true);
                    if (page == 1)
                        swipeRefreshLayout.setRefreshing(false);
                    else
                        loading.setVisibility(View.GONE);
                    if (listener != null)
                        listener.onEndLoad(TranslatorTabFragment.this);
                    isLoading = false;
                    super.onPostExecute(aVoid);
                }
            }.execute();
        }

    }
}
