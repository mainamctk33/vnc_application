package com.team500ae.vncapplication.services;

import android.app.IntentService;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.Nullable;

import com.google.gson.JsonArray;
import com.team500ae.library.utils.SharedPreferencesUtil;
import com.team500ae.vncapplication.constants.Constants;
import com.team500ae.vncapplication.data_access.BookmarkInfo;
import com.team500ae.vncapplication.data_access.RealmInfo;
import com.team500ae.vncapplication.data_access.translator.CategoryTranslatorInfo;
import com.team500ae.vncapplication.data_access.translator.TranslatorInfo;
import com.team500ae.vncapplication.data_access.translator.TypeTranslatorInfo;
import com.team500ae.vncapplication.data_access.user.UserInfo;
import com.team500ae.vncapplication.models.ServiceResponseEntity;

import java.util.Date;

import io.realm.Realm;
import io.realm.exceptions.RealmError;

/**
 * Created by TamPC on 3/24/2018.
 */

public class ServiceTranslator extends IntentService {

    private static final String ACTION_SYNC_CATEGORY = "com.team500ae.vncapplication.services.action.ACTION_SYNC_CATEGORY_TRANSLATOR";
    private static final String ACTION_SYNC_TYPE = "com.team500ae.vncapplication.services.action.ACTION_SYNC_TYPE_TRANSLATOR";
    private static final String ACTION_BOOKMARK = "com.team500ae.vncapplication.services.action.ACTION_TRANSLATOR_BOOKMARK";

    public ServiceTranslator() {
        super("ServiceTranslator");
    }

    public static void startActionSyncCategory(Context context) {
        Intent intent = new Intent(context, ServiceTranslator.class);
        intent.setAction(ACTION_SYNC_CATEGORY);
        context.startService(intent);
            }

        public static void startActionSyncType(Context context) {
            Intent intent = new Intent(context, ServiceTranslator.class);
            intent.setAction(ACTION_SYNC_TYPE);
            context.startService(intent);
        }

        public static void startActionBookmark(Context context, int id, boolean bookmark) {
            Intent intent = new Intent(context, ServiceTranslator.class);
            intent.putExtra("ID", id);
            intent.putExtra("BOOKMARK", bookmark);
            intent.setAction(ACTION_BOOKMARK);
            context.startService(intent);
        }

        @Override
        protected void onHandleIntent(Intent intent) {
            if (intent != null) {
                final String action = intent.getAction();
                switch (action) {
                    case ACTION_SYNC_CATEGORY:
                    handleActionSyncCategory();
                    break;
                case ACTION_SYNC_TYPE:
                    handleActionSyncType();
                    break;
                case ACTION_BOOKMARK:
                    boolean bookmark = intent.getBooleanExtra("BOOKMARK", false);
                    int id = intent.getIntExtra("ID", 0);
                    handleActionBookmark(id, bookmark);
                    break;
            }
        }
    }

    private void handleActionSyncCategory() {
        // TODO: Handle action Foo
        new Thread(new Runnable() {
            @Override
            public void run() {
                Realm realm = null;
                try {
                    realm = RealmInfo.getRealm(getApplicationContext());
                    CategoryTranslatorInfo data = new CategoryTranslatorInfo();
                    long lastSyncDate = 0;//SharedPreferencesUtil.getLong(getApplicationContext(), Constants.LAST_SYNC_TYPEJOB, 0L);
                    ServiceResponseEntity<JsonArray> result = data.sync( lastSyncDate);
                    if (result != null && result.getStatus() == 0) {
                        if (data.update(realm, result.getData()).getData())
                            SharedPreferencesUtil.setSharedPreferences(getApplicationContext(), SharedPreferencesUtil.EnumType.Long, Constants.LAST_SYNC_CATEGORYTRANSLATOR, new Date().getTime());
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                } catch (RealmError e) {
                    e.printStackTrace();
                } catch (OutOfMemoryError e) {
                    e.printStackTrace();
                } finally {
                    RealmInfo.closeRealm(realm);
                }
            }
        }).start();
    }
    private void handleActionSyncType() {
        // TODO: Handle action Foo
        new Thread(new Runnable() {
            @Override
            public void run() {
                Realm realm = null;
                try {
                    realm = RealmInfo.getRealm(getApplicationContext());
                    TypeTranslatorInfo data = new TypeTranslatorInfo();
                    long lastSyncDate = 0;
                    ServiceResponseEntity<JsonArray> result = data.sync(lastSyncDate);
                    if (result != null && result.getStatus() == 0) {
                        if (data.update(realm, result.getData()).getData())
                            SharedPreferencesUtil.setSharedPreferences(getApplicationContext(), SharedPreferencesUtil.EnumType.Long, Constants.LAST_SYNC_TYPETRANSLATOR, new Date().getTime());
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                } catch (RealmError e) {
                    e.printStackTrace();
                } catch (OutOfMemoryError e) {
                    e.printStackTrace();
                } finally {
                    RealmInfo.closeRealm(realm);
                }
            }
        }).start();
    }
    private void handleActionBookmark(int id, boolean bookmark) {
        // TODO: Handle action Foo
        new Thread(new Runnable() {
            @Override
            public void run() {
                Realm realm = null;
                try {
                    realm = RealmInfo.getRealm(getApplicationContext());
                    if (UserInfo.isLogin()) {
                        TranslatorInfo jobInfo = new TranslatorInfo();
                        realm = RealmInfo.getRealm(getApplicationContext());
                        new BookmarkInfo().bookmark(getApplicationContext(), realm, id, UserInfo.getCurrentUserId(), BookmarkInfo.BookmarkType.Translator, bookmark);
                        jobInfo.bookmark(id, bookmark);
                        Intent intent = new Intent(Constants.ACTION_JOB_BOOKMARK_SUCCESS);
                        getApplicationContext().sendBroadcast(intent);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                } catch (RealmError e) {
                    e.printStackTrace();
                } catch (OutOfMemoryError e) {
                    e.printStackTrace();
                } finally {
                    RealmInfo.closeRealm(realm);
                }
            }
        }).start();
    }
}
