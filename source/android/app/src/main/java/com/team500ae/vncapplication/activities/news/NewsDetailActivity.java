package com.team500ae.vncapplication.activities.news;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.ImageView;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.team500ae.library.utils.ConvertUtils;
import com.team500ae.vncapplication.R;
import com.team500ae.vncapplication.activities.login.LoginActivity;
import com.team500ae.vncapplication.base.BaseActivity;
import com.team500ae.vncapplication.constants.Constants;
import com.team500ae.vncapplication.data_access.news.NewsInfo;
import com.team500ae.vncapplication.data_access.user.UserInfo;
import com.team500ae.vncapplication.models.ServiceResponseEntity;
import com.team500ae.vncapplication.services.ServiceNews;
import com.team500ae.vncapplication.utils.ShareUtils;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by MaiNam on 11/23/2017.
 */

public class NewsDetailActivity extends BaseActivity implements com.team500ae.library.activities.BaseActivity.OnCallBackBroadcastListener {
    @BindView(R.id.btnShare)
    ImageView btnShare;
    private int id;

    public static void open(Context context, int id) {
        Intent intent = new Intent(context, NewsDetailActivity.class);
        intent.putExtra("NEWS_ID", id);
        context.startActivity(intent);
    }


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        setContentView(R.layout.activity_detail_news);
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);
        showBackButton();
        setTitle(R.string.title_news);
        id = getIntent().getIntExtra("NEWS_ID", 0);
        webView.getSettings().setLayoutAlgorithm(WebSettings.LayoutAlgorithm.SINGLE_COLUMN);
        //webView.loadUrl("http://onlineapi.site/tin-tuc/" + id);
        webView.loadUrl("http://vnc.iduhoc.vn/tin-tuc/" + id);
        listBookmarkID = new ArrayList<>();
        if (!UserInfo.isLogin()) {
            imgBookmark.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.ic_like));
        } else {
            new AsyncTask<Void, Void, ServiceResponseEntity<JsonArray>>() {
                @Override
                protected ServiceResponseEntity<JsonArray> doInBackground(Void... voids) {
                    ServiceResponseEntity<JsonArray> data;
                    try {
                        data = new NewsInfo().getAll(getApplicationContext(), 1, 99999999, -1, "", "");
                    } catch (Exception ex) {
                        return null;
                    }
                    return data;
                }

                @Override
                protected void onPostExecute(ServiceResponseEntity<JsonArray> data) {
                    if (data != null && data.getStatus() == 0) {
                        for (int i = 0; i < data.getData().size(); i++) {
                            JsonObject jsonObject = ConvertUtils.toJsonObject(data.getData().get(i));
                            listBookmarkID.add(ConvertUtils.toInt(jsonObject.get("ID")));
                        }
                    }
                    if (listBookmarkID.contains(id))
                        hasBookmark = true;
                    else
                        hasBookmark = false;
                    imgBookmark.setImageDrawable(ContextCompat.getDrawable(getActivity(), hasBookmark ? R.drawable.ic_like_check : R.drawable.ic_like));
                }
            }.execute();
        }
        //BaseReturnFunctionEntity<Boolean> bookmark = new BookmarkInfo().isBookmark(getActivity(), getRealm(), id, UserInfo.getCurrentUserId(), BookmarkInfo.BookmarkType.News);
        //hasBookmark = bookmark.isTrue() ? bookmark.data : false;
        //imgBookmark.setImageDrawable(ContextCompat.getDrawable(getActivity(), hasBookmark ? R.drawable.ic_like_check : R.drawable.ic_like));
        registerBroadcastReceiver(new ReceiverItem(Constants.ACTION_LOGIN, this));
    }

    boolean hasBookmark;
    ArrayList<Integer> listBookmarkID;
    @BindView(R.id.btnBookmark)
    ImageView imgBookmark;

    @OnClick(R.id.btnBookmark)
    void bookmark() {
//        LoginActivity.openChannel(getActivity());
        if (!UserInfo.isLogin()) {
            LoginActivity.open(getActivity());
            return;
        } else {
            hasBookmark = !hasBookmark;
            ServiceNews.startActionBookmark(getActivity(), id, hasBookmark);
            imgBookmark.setImageDrawable(ContextCompat.getDrawable(getActivity(), hasBookmark ? R.drawable.ic_like_check : R.drawable.ic_like));
        }
    }

    @OnClick(R.id.btnShare)
    void share() {
        ShareUtils.shareFacebook(getActivity(), Constants.SERVER + "/applink/news/" + id, "vnc");
    }

    @BindView(R.id.webView)
    WebView webView;

    @Override
    public void onCallBackBroadcastListener(Context context, String action, Intent intent) {
        switch (action) {
            case Constants.ACTION_LOGIN:
                // BaseReturnFunctionEntity<Boolean> bookmark = new BookmarkInfo().isBookmark(getActivity(), getRealm(), id, UserInfo.getCurrentUserId(), BookmarkInfo.BookmarkType.News);
                // hasBookmark = bookmark.isTrue() ? bookmark.data : false;
                //imgBookmark.setImageDrawable(ContextCompat.getDrawable(getActivity(), hasBookmark ? R.drawable.ic_like_check : R.drawable.ic_like));
                new AsyncTask<Void, Void, ServiceResponseEntity<JsonArray>>() {
                    @Override
                    protected ServiceResponseEntity<JsonArray> doInBackground(Void... voids) {
                        ServiceResponseEntity<JsonArray> data;
                        try {
                            data = new NewsInfo().getAll(getApplicationContext(), 1, 99999999, -1, "", "");
                        } catch (Exception ex) {
                            return null;
                        }
                        return data;
                    }

                    @Override
                    protected void onPostExecute(ServiceResponseEntity<JsonArray> data) {
                        if (data != null && data.getStatus() == 0) {
                            for (int i = 0; i < data.getData().size(); i++) {
                                JsonObject jsonObject = ConvertUtils.toJsonObject(data.getData().get(i));
                                listBookmarkID.add(ConvertUtils.toInt(jsonObject.get("ID")));
                            }
                        }
                        if (listBookmarkID.contains(id))
                            hasBookmark = true;
                        else
                            hasBookmark = false;
                        imgBookmark.setImageDrawable(ContextCompat.getDrawable(getActivity(), hasBookmark ? R.drawable.ic_like_check : R.drawable.ic_like));
                    }
                }.execute();
                break;
        }
    }
}
