package com.team500ae.vncapplication.data_access.upload;

import android.content.Context;
import android.net.Uri;

import com.team500ae.library.model.DataDictionary;

import java.util.ArrayList;

public interface IUploadProvider {
    void upload(Context context, String method, UploadInfo.UploadListener uploadListener, ArrayList<Uri> files, int timeOut);
}