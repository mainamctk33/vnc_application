package com.team500ae.vncapplication.activities.settings;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.view.Gravity;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.team500ae.vncapplication.R;
import com.team500ae.vncapplication.base.BaseActivity;
import com.team500ae.vncapplication.constants.TimeOutConstants;
import com.team500ae.vncapplication.data_access.question.QuestionInfo;
import com.team500ae.vncapplication.models.BaseReturnFunctionEntity;
import com.team500ae.vncapplication.models.QuestionObject;
import com.team500ae.vncapplication.utils.StringUtils;

import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Phuong D. Nguyen on 4/13/2018.
 */

public class AskQuestionActivity extends BaseActivity {

    TextView tvQuestionType_content;
    EditText edtQuestionTitle_content, edtQuestionContent_content;
    private ImageView ivQuestionTypeRightIcon;
    private String type;

    public static void open(Context activity) {
        Intent intent = new Intent(activity, AskQuestionActivity.class);
        activity.startActivity(intent);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        setContentView(R.layout.activity_ask_question);
        ButterKnife.bind(this);
        super.onCreate(savedInstanceState);
        showBackButton();
        initLayout();

        tvQuestionType_content.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showQuestionTypeDialog();
            }
        });
        ivQuestionTypeRightIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showQuestionTypeDialog();
            }
        });
    }

    private void initLayout() {
        View vQuestionTypeInclude = findViewById(R.id.layout_question_type);
        ImageView ivQuestionTypeLeftIcon = (ImageView) vQuestionTypeInclude.findViewById(R.id.image_left);
        ivQuestionTypeLeftIcon.setImageResource(R.drawable.ic_type_question);
        TextView tvQuestionTypeTitle = (TextView) vQuestionTypeInclude.findViewById(R.id.text_title);
        tvQuestionTypeTitle.setText(getResources().getString(R.string.question_type));
        tvQuestionTypeTitle.setTextColor(getResources().getColor(R.color.colorPrimary));
        tvQuestionType_content = (TextView) vQuestionTypeInclude.findViewById(R.id.text_content);
        tvQuestionType_content.setText(getResources().getString(R.string.public_type));
        ivQuestionTypeRightIcon = (ImageView) vQuestionTypeInclude.findViewById(R.id.image_right);
        ivQuestionTypeRightIcon.setImageResource(R.drawable.ic_next);

        View vQuestionTitleInclude = findViewById(R.id.layout_question_title);
        ImageView ivQuestionTitleLeftIcon = (ImageView) vQuestionTitleInclude.findViewById(R.id.image_left);
        ivQuestionTitleLeftIcon.setImageResource(R.drawable.ic_title_question);
        TextView tvQuestionTitleTitle = (TextView) vQuestionTitleInclude.findViewById(R.id.text_title);
        tvQuestionTitleTitle.setText(getResources().getString(R.string.question_title));
        edtQuestionTitle_content = (EditText) vQuestionTitleInclude.findViewById(R.id.text_content);
        ImageView ivQuestionTitleRightIcon = (ImageView) vQuestionTitleInclude.findViewById(R.id.image_right);
        ivQuestionTitleRightIcon.setVisibility(View.GONE);

        View vQuestionInclude = findViewById(R.id.layout_question_content);
        ImageView ivQuestionLeftIcon = (ImageView) vQuestionInclude.findViewById(R.id.image_left);
        ivQuestionLeftIcon.setImageResource(R.drawable.ic_question);
        TextView tvQuestionTitle = (TextView) vQuestionInclude.findViewById(R.id.text_title);
        tvQuestionTitle.setText(getResources().getString(R.string.question_content));
        tvQuestionTitle.setVisibility(View.GONE);
        edtQuestionContent_content = (EditText) vQuestionInclude.findViewById(R.id.text_content);
        edtQuestionContent_content.setGravity(Gravity.LEFT);
        ImageView ivQuestionRightIcon = (ImageView) vQuestionInclude.findViewById(R.id.image_right);
        ivQuestionRightIcon.setVisibility(View.GONE);
    }


    private void showQuestionTypeDialog() {
        final String[] singleChoiceItems = getResources().getStringArray(R.array.list_question_type);
        int itemSelected = 1;
        if (tvQuestionType_content.getText().toString().equals(getResources().getString(R.string.public_type)))
            itemSelected = 0;

        new AlertDialog.Builder(this).setTitle(getResources().getString(R.string.question_type)).setSingleChoiceItems(singleChoiceItems, itemSelected, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int selectedIndex) {
                tvQuestionType_content.setText(singleChoiceItems[selectedIndex]);
            }
        }).setIcon(R.drawable.ic_type_question).setNegativeButton(getResources().getString(R.string.close), null).show();
    }

    @OnClick(R.id.btn_submit)
    void updateAccount() {
        getHandler().postDelayed(new Runnable() {
            @Override
            public void run() {
                sendQuestion();
            }
        }, 500);
    }

    private void sendQuestion() {
        edtQuestionTitle_content.setError(null);
        edtQuestionContent_content.setError(null);

        String title = edtQuestionTitle_content.getText().toString();
        String content = edtQuestionContent_content.getText().toString();
        type = tvQuestionType_content.getText().toString();
        if (type != null && type.equals(getResources().getString(R.string.public_type)))
            type = getResources().getString(R.string.public_type_eng);
        else type = getResources().getString(R.string.private_type_eng);

        if (title == null || title.isEmpty()) {
            edtQuestionTitle_content.setError(getString(R.string.question_require_input_title));
            edtQuestionTitle_content.requestFocus();
            return;
        }

        if (content == null || content.isEmpty()) {
            edtQuestionContent_content.setError(getString(R.string.question_require_input_content));
            edtQuestionContent_content.requestFocus();
            return;
        }

        new AsyncTask<Void, Void, BaseReturnFunctionEntity<Boolean>>() {
            ProgressDialog progDialog = new ProgressDialog(AskQuestionActivity.this);

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                progDialog.setMessage(getString(R.string.activity_question_create));
                progDialog.setIndeterminate(false);
                progDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                progDialog.setCancelable(true);
                progDialog.show();
            }

            @Override
            protected BaseReturnFunctionEntity<Boolean> doInBackground(Void... voids) {
                BaseReturnFunctionEntity<Boolean> result = new BaseReturnFunctionEntity<>(BaseReturnFunctionEntity.StatusFunction.FALSE);
                try {
//                    boolean isExisted = QuestionInfo.isExisted(title, content);
//                    if (isExisted) {
//                        result.setMessage(getString(R.string.question_exist));
//                        return result;
//                    } else {
//                        return QuestionInfo.createQuestion(type, title, content, TimeOutConstants.TIMEOUT_ACCOUNT_REGISTER);
//                    }
                    return QuestionInfo.createQuestion(type, title, content, TimeOutConstants.TIMEOUT_ACCOUNT_REGISTER);
                } catch (Exception e) {
                    e.printStackTrace();
                    result.setMessage(e.getMessage().toString());
                }
                return result;
            }

            @Override
            protected void onPostExecute(BaseReturnFunctionEntity<Boolean> result) {
                super.onPostExecute(result);
                if (progDialog.isShowing()) {
                    progDialog.dismiss();
                }

                if (result.isTrue()) {
                    clearInputInfo();
//                    showSnackBar(R.string.activity_create_question_successful);
                    Toast.makeText(getBaseContext(), R.string.activity_create_question_successful, Toast.LENGTH_SHORT).show();
                } else {
//                    showSnackBar(result.getMessage());
                    Toast.makeText(getBaseContext(), result.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        }.execute();
    }

    private void clearInputInfo() {
        edtQuestionTitle_content.setText("");
        edtQuestionContent_content.setText("");
    }
}
