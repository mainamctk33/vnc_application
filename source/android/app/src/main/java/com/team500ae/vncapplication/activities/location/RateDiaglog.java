package com.team500ae.vncapplication.activities.location;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.Toast;

import com.team500ae.vncapplication.R;
import com.team500ae.vncapplication.others.DialogListener;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class RateDiaglog extends Dialog {


    private float value;
    @BindView(R.id.rtRating)
    public RatingBar rtRating;
    private String comment;
    @BindView(R.id.txtComment)
    public EditText txtComment;

    private DialogListener clickListener;

    public RateDiaglog(Context context, float myRate, String myComment) {
        super(context, android.R.style.Theme_Holo_Dialog);
        this.comment = myComment;
        this.value = myRate;
    }

    public void setOnItemClickListener(DialogListener listener) {
        this.clickListener = listener;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        if (getWindow() != null) {
            getWindow().setDimAmount(0.3f);
            getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        }
        setContentView(R.layout.dialog_rate_location);

        ButterKnife.bind(this);
        rtRating.setRating(value);
        txtComment.setText(comment);
    }

    @OnClick({R.id.btnCancel, R.id.btnOk})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btnCancel:
                dismiss();
                break;
            case R.id.btnOk:
                if (rtRating.getRating() == 0) {
                    Toast.makeText(getContext(), R.string.please_set_rate_value, Toast.LENGTH_LONG).show();
                    return;
                }
                if (clickListener != null)
                    clickListener.onConfirmClicked();
                dismiss();
                break;
        }
    }
}
