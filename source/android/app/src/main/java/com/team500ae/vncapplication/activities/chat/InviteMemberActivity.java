package com.team500ae.vncapplication.activities.chat;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.sendbird.android.GroupChannel;
import com.sendbird.android.SendBird;
import com.sendbird.android.SendBirdException;
import com.sendbird.android.User;
import com.sendbird.android.UserListQuery;
import com.team500ae.library.utils.ConvertUtils;
import com.team500ae.vncapplication.R;
import com.team500ae.vncapplication.activities.chat.adapter.SelectableUserListAdapter;
import com.team500ae.vncapplication.activities.chat.fragments.GroupChatFragment;
import com.team500ae.vncapplication.base.BaseActivity;
import com.team500ae.vncapplication.data_access.user.UserInfo;
import com.team500ae.vncapplication.services.ServiceChat;

import java.util.ArrayList;
import java.util.List;


public class InviteMemberActivity extends BaseActivity {

    private LinearLayoutManager mLayoutManager;
    private RecyclerView mRecyclerView;
    private SelectableUserListAdapter mListAdapter;

    private UserListQuery mUserListQuery;
    private String mChannelUrl;

    private List<String> mSelectedUserIds;
    private List<String> mSelectedUserFullname;
    private JsonArray listMembers;
    ProgressDialog dialog;
    int page = 1;
    int size = 30;
    private ArrayList<JsonObject> listData;
    private boolean finished = false;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        listData = new ArrayList<>();
        setContentView(R.layout.activity_invite_member);
        dialog = ProgressDialog.show(getActivity(), "",
                "Loading. Please wait...", true);

        listMembers = ConvertUtils.toJsonArray(getIntent().getStringExtra("LIST_MEMBERS"), new JsonArray());

        mSelectedUserIds = new ArrayList<>();
        mSelectedUserFullname = new ArrayList<>();

        mRecyclerView = (RecyclerView) findViewById(R.id.recycler_invite_member);
        mListAdapter = new SelectableUserListAdapter(this, false, true);
        mListAdapter.setItemCheckedChangeListener(new SelectableUserListAdapter.OnItemCheckedChangeListener() {
            @Override
            public void OnItemChecked(JsonObject user, boolean checked) {
                if (checked) {
                    mSelectedUserIds.add((ConvertUtils.toString(user.get("UserName"))));
                    mSelectedUserFullname.add((ConvertUtils.toString(user.get("FullName"))));
                } else {
                    int index = mSelectedUserIds.indexOf(ConvertUtils.toString(user.get("UserName")));
                    if (index != -1) {
                        mSelectedUserIds.remove(index);
                        mSelectedUserFullname.remove(index);
                    }
                }
            }
        });

        mChannelUrl = getIntent().getStringExtra(GroupChatFragment.EXTRA_CHANNEL_URL);

        setUpRecyclerView();

        loadData(page, size);
        showBackButton();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_invite_member, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            onBackPressed();
            return true;
        }
        if (id == R.id.button_invite_member) {
            if (mSelectedUserIds.size() > 0) {
                inviteSelectedMembersWithUserIds();
            } else {
                showSnackBar(R.string.select_member_to_add_to_group);
            }

        }

        return super.onOptionsItemSelected(item);
    }

    private void setUpRecyclerView() {
        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setAdapter(mListAdapter);
        mRecyclerView.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));

        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                if (!finished) {
                    if (mLayoutManager.findLastVisibleItemPosition() == mListAdapter.getItemCount() - 1) {
                        loadData(page + 1, size);
                    }
                }
            }
        });
    }

    private void inviteSelectedMembersWithUserIds() {

        // Get channel instance from URL first.
        GroupChannel.getChannel(mChannelUrl, (groupChannel, e) -> {
            if (e != null) {
                // Error!
                return;
            }

            // Then invite the selected members to the channel.
            groupChannel.inviteWithUserIds(mSelectedUserIds, e1 -> {
                if (e1 != null) {
                    // Error!
                    return;
                }
                ServiceChat.startActionInviteMember(getActivity(),UserInfo.getCurrentUserId(), UserInfo.getCurrentUserFullName(), mSelectedUserIds, mSelectedUserFullname, groupChannel.getUrl());
                finish();
            });
        });
    }

    private void loadData(int page, int size) {
        this.page = page;
        new AsyncTask<Void, Void, JsonArray>() {
            @Override
            protected void onPreExecute() {
                try {
                    dialog.show();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                super.onPreExecute();
            }

            @Override
            protected JsonArray doInBackground(Void... voids) {
                JsonArray jsonArray = new UserInfo().searchFriend("", page, size);
                if (jsonArray.size() < size)
                    finished = true;
                if (page == 1) {
                    listData.clear();
                }
                for (int i = 0; i < jsonArray.size(); i++) {
                    JsonObject jsonObject = ConvertUtils.toJsonObject(jsonArray.get(i));
                    boolean isExist = false;
                    String id = ConvertUtils.toString(jsonObject.get("UserName"));
                    for (int j = 0; j < listMembers.size(); j++) {
                        JsonObject jsonObject1 = ConvertUtils.toJsonObject(listMembers.get(j));
                        if (ConvertUtils.toString(jsonObject1.get("mUserId")).equals(id)) {
                            isExist = true;
                            break;
                        }
                    }
                    if (!isExist) {
                        listData.add(jsonObject);
                    }
                }

                return null;
            }

            @Override
            protected void onPostExecute(JsonArray aVoid) {
                try {
                    dialog.hide();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                mListAdapter.setUserList(listData);
                super.onPostExecute(aVoid);
            }
        }.execute();
    }


}
