package com.team500ae.vncapplication.data_access.upload;

import android.content.Context;
import android.net.Uri;

import com.google.gson.JsonArray;
import com.team500ae.library.model.DataDictionary;
import com.team500ae.vncapplication.data_access.ClientInfo;
import com.team500ae.vncapplication.models.ServiceResponseEntity;

import java.util.ArrayList;

/**
 * Created by MaiNam on 1/2/2018.
 */

public class UploadInfo {
    public interface UploadListener {
        void beginUpload();

        void endUpload();

        void onSuccess(JsonArray response);

        void onError(ServiceResponseEntity<JsonArray> response);
    }

    IUploadProvider iUploadProvider;
    UploadListener uploadListener;

    public UploadInfo(IUploadProvider iUploadProvider, UploadListener uploadListener) {
        this.iUploadProvider = iUploadProvider;
        this.uploadListener = uploadListener;
    }

    public void upload(Context context, String method, ArrayList<Uri> files, int timeOut) {
        if (iUploadProvider != null) {
            iUploadProvider.upload(context, method, uploadListener, files, timeOut);
        }
    }
    public void upload(Context context, String method, ArrayList<Uri> files) {
        if (iUploadProvider != null) {
            iUploadProvider.upload(context, method, uploadListener, files, 0);
        }
    }
}
