package com.team500ae.vncapplication.data_access.notification;

import android.content.Context;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.team500ae.vncapplication.constants.Constants;
import com.team500ae.vncapplication.data_access.ClientInfo;
import com.team500ae.vncapplication.models.JSONUser;
import com.team500ae.vncapplication.models.ServiceResponseEntity;

import java.net.UnknownHostException;

public class NotificationInfo {
    public ServiceResponseEntity<Boolean> registerToken(Context context, String username, String deviceId, String token) {
        try {
            JsonObject jsonObject = new JsonObject();
            jsonObject.addProperty("deviceId", deviceId);
            jsonObject.addProperty("token", token);

            ClientInfo.DataResponse dataResponse = new ClientInfo().requestApi(ClientInfo.MethodType.POST, Constants.API_NOTIFICATION_REGISTER_TOKEN+"/"+username, jsonObject);
            if (dataResponse.isOK()) {
                ServiceResponseEntity<Boolean> result = dataResponse.getData(new TypeToken<ServiceResponseEntity<Boolean>>() {
                }.getType());
                return result;
            }
        } catch (UnknownHostException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
    public ServiceResponseEntity<Boolean> removeDeviceToken(Context context,  String deviceId) {
        try {
            ClientInfo.DataResponse dataResponse = new ClientInfo().requestApi(ClientInfo.MethodType.POST, Constants.API_NOTIFICATION_REMOVE_DEVICE+"/"+deviceId);
            if (dataResponse.isOK()) {
                ServiceResponseEntity<Boolean> result = dataResponse.getData(new TypeToken<ServiceResponseEntity<Boolean>>() {
                }.getType());
                return result;
            }
        } catch (UnknownHostException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
