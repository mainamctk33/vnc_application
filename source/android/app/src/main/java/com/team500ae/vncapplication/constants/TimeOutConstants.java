package com.team500ae.vncapplication.constants;

/**
 * Created by Phuong D. Nguyen on 1/11/2018.
 */

public class TimeOutConstants {
    public static final int TIMEOUT_ACCOUNT_CHECKEXIST = 1000;
    public static final int TIMEOUT_ACCOUNT_GETBYID = 100;
    public static final int TIMEOUT_ACCOUNT_REGISTER = 5000;
    public static final int TIMEOUT_ACCOUNT_LOGIN = 2000;
}
