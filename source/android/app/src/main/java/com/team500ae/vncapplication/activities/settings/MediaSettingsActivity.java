package com.team500ae.vncapplication.activities.settings;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.TextView;

import com.google.gson.JsonObject;
import com.team500ae.library.utils.ConvertUtils;
import com.team500ae.vncapplication.R;
import com.team500ae.vncapplication.base.BaseActivity;
import com.team500ae.vncapplication.data_access.user.UserInfo;
import com.team500ae.vncapplication.models.BaseReturnFunctionEntity;
import com.team500ae.vncapplication.utils.FlagAttributeUtils;

import butterknife.BindView;

/**
 * Created by Administrator on 01/07/2018.
 */

public class MediaSettingsActivity extends BaseActivity {

    @BindView(R.id.layout_download_via_mobile_data)
    View vDownloadViaMobileData;
    @BindView(R.id.layout_download_via_wifi)
    View vDownloadViaWifi;

    private Switch swtDownloadViaMobileData, swtDownloadViaWifi;
    private boolean downloadViaMobileData, downloadViaWifi;

    private int settingAttributeValue;

    public static void open(Context activity) {
        Intent intent = new Intent(activity, MediaSettingsActivity.class);
        activity.startActivity(intent);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        setContentView(R.layout.activity_media_settings);
        super.onCreate(savedInstanceState);
        showBackButton();

        initLayout();
        getCurrentSettings();

        swtDownloadViaMobileData.setChecked(downloadViaMobileData);
        swtDownloadViaWifi.setChecked(downloadViaWifi);

        downloadViaMobileDataEvent();
        downloadViaWifiEvent();
    }

    private void initLayout() {
        TextView tvDownloadViaMobileDataTitle = (TextView) vDownloadViaMobileData.findViewById(R.id.tv_title);
        tvDownloadViaMobileDataTitle.setText(getResources().getString(R.string.pref_media_auto_download_over_mobile_network_title));
        TextView tvDownloadViaMobileDataSubTitle = (TextView) vDownloadViaMobileData.findViewById(R.id.tv_sub_title);
        tvDownloadViaMobileDataSubTitle.setText(getResources().getString(R.string.pref_media_auto_download_over_mobile_network_summary));
        swtDownloadViaMobileData = (Switch) vDownloadViaMobileData.findViewById(R.id.switch_block);

        TextView tvDownloadViaWifiTitle = (TextView) vDownloadViaWifi.findViewById(R.id.tv_title);
        tvDownloadViaWifiTitle.setText(getResources().getString(R.string.pref_media_auto_download_when_connect_wifi_title));
        TextView tvDownloadViaWifiSubTitle = (TextView) vDownloadViaWifi.findViewById(R.id.tv_sub_title);
        tvDownloadViaWifiSubTitle.setText(getResources().getString(R.string.pref_media_auto_download_when_connect_wifi_summary));
        swtDownloadViaWifi = (Switch) vDownloadViaWifi.findViewById(R.id.switch_block);
    }

    private void getCurrentSettings() {
        JsonObject currentUser = UserInfo.getCurrentUser();
        settingAttributeValue = ConvertUtils.toInt(currentUser.get("SettingAttribute"));
        downloadViaMobileData = FlagAttributeUtils.getAttribute(settingAttributeValue, FlagAttributeUtils.DOWNLOAD_VIA_MOBILE_NETWORK);
        downloadViaWifi = FlagAttributeUtils.getAttribute(settingAttributeValue, FlagAttributeUtils.DOWNLOAD_VIA_WIFI);
    }

    private void downloadViaMobileDataEvent() {
        swtDownloadViaMobileData.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(MediaSettingsActivity.this);
                builder.setIcon(R.drawable.ic_info);
                builder.setTitle(getResources().getString(R.string.pref_media_auto_download_over_mobile_network_title));
                if (downloadViaMobileData)
                    builder.setMessage(getResources().getString(R.string.notice_disallow_download_via_mobile_network));
                else
                    builder.setMessage(getResources().getString(R.string.notice_allow_download_via_mobile_network));
                builder.setCancelable(false);
                builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        settingAttributeValue = ConvertUtils.toInt(UserInfo.getCurrentUser().get("SettingAttribute"));

                        //update switch status
                        boolean desireStatus = !downloadViaMobileData;
                        int desireValue= desireStatus? (settingAttributeValue | FlagAttributeUtils.DOWNLOAD_VIA_MOBILE_NETWORK):(settingAttributeValue ^ FlagAttributeUtils.DOWNLOAD_VIA_MOBILE_NETWORK);

                        //call API to update
                        new AsyncTask<Void, Void, BaseReturnFunctionEntity<Boolean>>() {
                            ProgressDialog progDialog = new ProgressDialog(MediaSettingsActivity.this);

                            @Override
                            protected void onPreExecute() {
                                super.onPreExecute();
                                progDialog.setMessage(getString(R.string.activity_update_user_media_setting));
                                progDialog.setIndeterminate(false);
                                progDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                                progDialog.setCancelable(true);
                                progDialog.show();
                            }

                            @Override
                            protected BaseReturnFunctionEntity<Boolean> doInBackground(Void... voids) {
                                    return UserInfo.updateSettingAttribute(desireValue);
                            }

                            @Override
                            protected void onPostExecute(BaseReturnFunctionEntity<Boolean> result) {
                                super.onPostExecute(result);
                                if (progDialog.isShowing()) {
                                    progDialog.dismiss();
                                }

                                if (result.isTrue()) {
                                    downloadViaMobileData = desireStatus;
                                    swtDownloadViaMobileData.setChecked(desireStatus);
                                    UserInfo.updateLocalSettingAttribute(getActivity(),desireValue);
                                    showSnackBar(R.string.activity_update_user_media_setting_successfully);
                                } else {
                                    showSnackBar(R.string.activity_update_user_media_setting_failed + ": " + result.getMessage());
                                }
                            }
                        }.execute();
                    }
                });
                builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        //do nothing
                    }
                });
                builder.show();
            }
        });

        swtDownloadViaMobileData.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                //keep current status. Only change status after user click confirm to dialog (see OnClickListener)
                swtDownloadViaMobileData.setChecked(downloadViaMobileData);
            }
        });
    }

    private void downloadViaWifiEvent() {
        swtDownloadViaWifi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(MediaSettingsActivity.this);
                builder.setIcon(R.drawable.ic_info);
                builder.setTitle(getResources().getString(R.string.pref_media_auto_download_when_connect_wifi_title));
                if (downloadViaWifi)
                    builder.setMessage(getResources().getString(R.string.notice_disallow_download_via_wifi));
                else
                    builder.setMessage(getResources().getString(R.string.notice_allow_download_via_wifi));
                builder.setCancelable(false);
                builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        settingAttributeValue = ConvertUtils.toInt(UserInfo.getCurrentUser().get("SettingAttribute"));
                        //update switch status
                        boolean desireStatus = !downloadViaWifi;
                        int desireValue= desireStatus? (settingAttributeValue | FlagAttributeUtils.DOWNLOAD_VIA_WIFI):(settingAttributeValue ^ FlagAttributeUtils.DOWNLOAD_VIA_WIFI);
                        //call API to update
                        new AsyncTask<Void, Void, BaseReturnFunctionEntity<Boolean>>() {
                            ProgressDialog progDialog = new ProgressDialog(MediaSettingsActivity.this);

                            @Override
                            protected void onPreExecute() {
                                super.onPreExecute();
                                progDialog.setMessage(getString(R.string.activity_update_user_media_setting));
                                progDialog.setIndeterminate(false);
                                progDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                                progDialog.setCancelable(true);
                                progDialog.show();
                            }

                            @Override
                            protected BaseReturnFunctionEntity<Boolean> doInBackground(Void... voids) {
                                return UserInfo.updateSettingAttribute(desireValue);
                            }

                            @Override
                            protected void onPostExecute(BaseReturnFunctionEntity<Boolean> result) {
                                super.onPostExecute(result);
                                if (progDialog.isShowing()) {
                                    progDialog.dismiss();
                                }

                                if (result.isTrue()) {
                                    downloadViaWifi = desireStatus;
                                    swtDownloadViaWifi.setChecked(desireStatus);
                                    UserInfo.updateLocalSettingAttribute(getActivity(), desireValue);
                                    showSnackBar(R.string.activity_update_user_media_setting_successfully);
                                } else {
                                    showSnackBar(R.string.activity_update_user_media_setting_failed + ": " + result.getMessage());
                                }
                            }
                        }.execute();
                    }
                });
                builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        //do nothing
                    }
                });
                builder.show();
            }
        });

        swtDownloadViaWifi.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                //keep current status. Only change status after user click confirm to dialog (see OnClickListener)
                swtDownloadViaWifi.setChecked(downloadViaWifi);
            }
        });
    }
}
