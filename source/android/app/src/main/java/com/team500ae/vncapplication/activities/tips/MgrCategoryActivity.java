package com.team500ae.vncapplication.activities.tips;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.View;
import android.widget.TextView;

import com.onurciner.toastox.ToastOXDialog;
import com.team500ae.library.adapters.BaseRecyclerAdapter;
import com.team500ae.library.layoutmanager.MyLinearLayoutManager;
import com.team500ae.library.utils.ConvertUtils;
import com.team500ae.vncapplication.R;
import com.team500ae.vncapplication.base.BaseActivity;
import com.team500ae.vncapplication.base.BaseViewHolder;
import com.team500ae.vncapplication.constants.Constants;
import com.team500ae.vncapplication.data_access.DataCacheInfo;
import com.team500ae.vncapplication.data_access.tips.TypeTipsInfo;
import com.team500ae.vncapplication.data_access.user.UserInfo;
import com.team500ae.vncapplication.models.BaseReturnFunctionEntity;
import com.team500ae.vncapplication.models.realm_models.tips.TypeTipsEntity;
import com.team500ae.vncapplication.utils.LanguageUtils;
import com.team500ae.vncapplication.utils.dragdrop.CallbackItemTouch;
import com.team500ae.vncapplication.utils.dragdrop.MyItemTouchHelperCallback;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.OnClick;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 * Created by phuong.ndp on 12/11/2017.
 */

public class MgrCategoryActivity extends BaseActivity implements CallbackItemTouch {

    public enum TIPS_VIEW_TYPE {
        Small(0), Big(1);
        private final int value;

        TIPS_VIEW_TYPE(int value) {
            this.value = value;
        }

        public int getValue() {
            return value;
        }
    }

    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.tvSmallList)
    TextView tvSmallList;
    @BindView(R.id.tvBigList)
    TextView tvBigList;

    private int typeView;
    private ArrayList<TypeTipsEntity> listData;
    private BaseRecyclerAdapter<ViewHolder, TypeTipsEntity> adapter;

    public static void open(Context context) {
        Intent intent = new Intent(context, MgrCategoryActivity.class);
        context.startActivity(intent);
        BaseActivity.setSlideIn(context, TypeSlideIn.in_right_to_left);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        setContentView(R.layout.activity_mgr_category);
        super.onCreate(savedInstanceState);

        getSupportActionBar().setTitle(R.string.category);

        //get view setting and press button on UI
        typeView = ConvertUtils.toInt(DataCacheInfo.getData(getActivity(), DataCacheInfo.EnumCacheType.Tips_Small_List, ""), TIPS_VIEW_TYPE.Small.getValue());
        if (typeView == TIPS_VIEW_TYPE.Small.getValue()) {
            tvSmallList.setBackgroundResource(R.drawable.btn_primary_color_background);
            tvSmallList.setTextColor(getResources().getColor(R.color.white));
            tvBigList.setBackgroundResource(R.drawable.btn_primary_color_border);
            tvBigList.setTextColor(getResources().getColor(R.color.colorPrimary));
            //tvSmallList.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.btn_press));
            tvSmallList.setSelected(true);
        } else {
            tvSmallList.setBackgroundResource(R.drawable.btn_primary_color_border);
            tvSmallList.setTextColor(getResources().getColor(R.color.colorPrimary));
            tvBigList.setBackgroundResource(R.drawable.btn_primary_color_background);
            tvBigList.setTextColor(getResources().getColor(R.color.white));
            //tvBigList.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.btn_press));
            tvBigList.setSelected(true);
        }

        recyclerView.setLayoutManager(new MyLinearLayoutManager(getActivity()));
        listData = new ArrayList<>();
        adapter = new BaseRecyclerAdapter<ViewHolder, TypeTipsEntity>(R.layout.layout_category_item, new BaseRecyclerAdapter.BaseViewHolder<ViewHolder, TypeTipsEntity>() {
            @Override
            public ViewHolder getViewHolder(View v) {
                return new ViewHolder(v);
            }

            @Override
            public void bindData(RecyclerView recyclerView, ViewHolder viewHolder, TypeTipsEntity data, int position) {
                viewHolder.tvName.setText(LanguageUtils.getAppLanguageSetting(MgrCategoryActivity.this).equals("en") ? data.getName() : data.getName2());
                viewHolder.tvIndex.setText(String.valueOf(position + 1));
            }
        }, listData, new BaseRecyclerAdapter.OnClickListener() {
            @Override
            public void onClick(View v, int position, Object o) {

            }
        });
        adapter.bindData(recyclerView);

        ItemTouchHelper.Callback callback = new MyItemTouchHelperCallback(this);
        //create MyItemTouchHelperCallback
        ItemTouchHelper touchHelper = new ItemTouchHelper(callback);
        // Create ItemTouchHelper and pass with parameter the MyItemTouchHelperCallback
        touchHelper.attachToRecyclerView(recyclerView);
        // Attach ItemTouchHelper to RecyclerView
        showBackButton(true);
    }

    @Override
    protected void onResume() {
        getData();
        super.onResume();
    }

    private void getData() {
        BaseReturnFunctionEntity<ArrayList<TypeTipsEntity>> myListTypeTips = new TypeTipsInfo().getTypeTipsHasSelected(getActivity(), getRealm(), UserInfo.getCurrentUserId());
        listData.clear();
        if (myListTypeTips.isTrue())
            listData.addAll(myListTypeTips.getData());
        adapter.notifyDataSetChanged();
    }

    @OnClick(R.id.tvBigList)
    void bigList() {
        //change button color
        tvSmallList.setBackgroundResource(R.drawable.btn_primary_color_border);
        tvSmallList.setTextColor(getResources().getColor(R.color.colorPrimary));
        tvBigList.setBackgroundResource(R.drawable.btn_primary_color_background);
        tvBigList.setTextColor(getResources().getColor(R.color.white));

        tvBigList.setSelected(true);
        tvSmallList.setSelected(false);
//        tvBigList.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.btn_press));
        //tvSmallList.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.btn_normal));
        DataCacheInfo.setData(getActivity(), DataCacheInfo.EnumCacheType.Tips_Small_List, "", TIPS_VIEW_TYPE.Big.getValue());
        Intent intent = new Intent(Constants.ACTION_TIPS_VIEW_MODE_CHANGE);
        sendBroadcast(intent);
        showSnackBar(getResources().getString(R.string.change_reading_mode_successful));
    }

    @OnClick(R.id.tvSmallList)
    void smallList() {
        //change button color
        tvSmallList.setBackgroundResource(R.drawable.btn_primary_color_background);
        tvSmallList.setTextColor(getResources().getColor(R.color.white));
        tvBigList.setBackgroundResource(R.drawable.btn_primary_color_border);
        tvBigList.setTextColor(getResources().getColor(R.color.colorPrimary));

        tvBigList.setSelected(false);
        tvSmallList.setSelected(true);
//        tvSmallList.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.btn_press));
//        tvBigList.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.btn_normal));
        DataCacheInfo.setData(getActivity(), DataCacheInfo.EnumCacheType.Tips_Small_List, "", TIPS_VIEW_TYPE.Small.getValue());
        Intent intent = new Intent(Constants.ACTION_TIPS_VIEW_MODE_CHANGE);
        sendBroadcast(intent);
        showSnackBar(getResources().getString(R.string.change_reading_mode_successful));
    }

    @OnClick(R.id.btnAddCat)
    void addCat() {
        int numOfNotSelectedItems = getNumberOfNotSeletedItems();
        if (numOfNotSelectedItems == 0) {
            showSnackBar(R.string.select_all_of_items);
        } else {
            ChooseCategoryActivity.open(getActivity());
        }
    }

    int getNumberOfNotSeletedItems() {
        BaseReturnFunctionEntity<ArrayList<TypeTipsEntity>> listTypeTips = new TypeTipsInfo().getListTypeTips(getActivity(), getRealm());
        ArrayList<TypeTipsEntity> listItems = new ArrayList<>();
        if (listTypeTips.isTrue()) {
            listItems.addAll(listTypeTips.getData());
            listTypeTips = new TypeTipsInfo().getTypeTipsHasSelected(getActivity(), getRealm(), UserInfo.getCurrentUserId());
            if (listTypeTips.isTrue()) {
                for (TypeTipsEntity typeTipsEntity : listTypeTips.getData()) {
                    listItems.remove(typeTipsEntity);
                }
            }
        }
        return listItems.size();
    }

    @Override
    public void itemTouchOnMove(int oldPosition, int newPosition) {
        //change position
        listData.add(newPosition, listData.remove(oldPosition));
        //notifies changes in adapter, in this case use the notifyItemMoved
        adapter.notifyItemMoved(oldPosition, newPosition);
    }

    public class ViewHolder extends BaseViewHolder {
        @BindView(R.id.tvName)
        TextView tvName;
        @BindView(R.id.tvIndex)
        TextView tvIndex;

        public ViewHolder(View itemView) {
            super(itemView);
        }

        @OnClick(R.id.btnRemove)
        void remove() {
            if (listData.size() < 2) {
                AlertDialog.Builder builderInner = new AlertDialog.Builder(MgrCategoryActivity.this);
                builderInner.setIcon(R.drawable.ic_info);
                builderInner.setTitle(getResources().getString(R.string.notice));
                builderInner.setMessage(getResources().getString(R.string.category_require_one_item));
                builderInner.setPositiveButton(R.string.select_language_ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                builderInner.show();
                return;
            }

            int index = getAdapterPosition();
            TypeTipsEntity typeTipsEntity = listData.get(index);

            new ToastOXDialog.Build(getActivity()).setTitle(R.string.confirm)
                    .setContent(getResources().getString(R.string.do_you_want_to_delete) + typeTipsEntity.getName() + getResources().getString(R.string.from_display_list)).setPositiveText(R.string.yes)
                    .setPositiveBackgroundColorResource(R.color.colorPrimary)
                    .setPositiveTextColorResource(R.color.white).onPositive(new ToastOXDialog.ButtonCallback() {
                @Override
                public void onClick(@NonNull ToastOXDialog toastOXDialog) {
                    BaseReturnFunctionEntity<Boolean> result = new TypeTipsInfo().removeTypeTipsFromListFavorite(getActivity(), getRealm(), UserInfo.getCurrentUserId(), listData.get(index).getId());
                    if (result.isTrue() && result.getData()) {
                        listData.remove(index);
                        adapter.notifyItemRemoved(index);
//                        adapter.notifyItemChanged(index);
                        Intent intent = new Intent(Constants.ACTION_TYPE_TIPS_FOR_USER_CHANGED);
                        sendBroadcast(intent);
                    }
                }
            }).setNegativeText(R.string.cancel)
                    .setNegativeBackgroundColorResource(R.color.dim)
                    .setNegativeTextColorResource(R.color.white)
                    .onNegative(new ToastOXDialog.ButtonCallback() {
                        @Override
                        public void onClick(@NonNull ToastOXDialog toastOXDialog) {
                        }
                    }).show();
        }
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
}
