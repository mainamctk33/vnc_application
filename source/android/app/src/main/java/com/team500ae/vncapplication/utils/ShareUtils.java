package com.team500ae.vncapplication.utils;

import android.app.Activity;
import android.net.Uri;
import android.support.annotation.NonNull;

import com.facebook.share.model.ShareHashtag;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.widget.ShareDialog;

public class ShareUtils {
    public static void shareFacebook(@NonNull Activity context, @NonNull String deeplink, String hashTag) {
        ShareLinkContent.Builder build = new ShareLinkContent.Builder()
                .setContentUrl(Uri.parse(deeplink));
        if (hashTag != null)
            build.setShareHashtag(new ShareHashtag.Builder().setHashtag("#" + hashTag).build());
        ShareLinkContent content = build.build();
        ShareDialog.show(context, content);
    }

}