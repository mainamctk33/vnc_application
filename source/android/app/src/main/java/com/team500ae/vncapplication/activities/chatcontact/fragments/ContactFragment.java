package com.team500ae.vncapplication.activities.chatcontact.fragments;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.team500ae.library.adapters.BaseRecyclerAdapter;
import com.team500ae.library.layoutmanager.MyLinearLayoutManager;
import com.team500ae.library.utils.AndroidPermissionUtils;
import com.team500ae.library.utils.ConvertUtils;
import com.team500ae.library.utils.ImageUtils;
import com.team500ae.library.utils.RecyclerViewUtils;
import com.team500ae.library.utils.StringUtils;
import com.team500ae.vncapplication.R;
import com.team500ae.vncapplication.activities.chat.GroupChannelActivity;
import com.team500ae.vncapplication.activities.friend.SearchFriendActivity;
import com.team500ae.vncapplication.activities.user.UserProfileActivity;
import com.team500ae.vncapplication.base.BaseActivity;
import com.team500ae.vncapplication.data_access.friend.FriendInfo;
import com.team500ae.vncapplication.models.ServiceResponseEntity;
import com.team500ae.vncapplication.services.ServiceChat;
import com.team500ae.vncapplication.utils.KeyboardUtils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.team500ae.library.activities.BaseActivity.requestPermission;
import static com.team500ae.library.activities.BaseActivity.showSnackBar;

/**
 * Created by MaiNam on 3/21/2018.
 */

public class ContactFragment extends Fragment implements Filterable {
    @BindView(R.id.swipeRefreshLayout)
    SwipeRefreshLayout swipeRefreshLayout;
    ArrayList<JsonObject> data = new ArrayList<>();

    private BaseRecyclerAdapter<ViewHolder, JsonObject> adapter;
    ArrayList<JsonObject> listData;
    @BindView(R.id.loading)
    View loading;
    int page = 1;
    int size = 100;
    boolean isLoading = false;
    @BindView(R.id.txtSearch)
    EditText txtSearch;
    @BindView(R.id.not_found_any_contact)
    View notFound;
    @BindView(R.id.viewData)
    View viewData;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_contact, container, false);
        KeyboardUtils.setupUI(view, getActivity());
        ButterKnife.bind(this, view);
        txtSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (!s.toString().equals("")) {
                    btnClear.setVisibility(View.VISIBLE);
                } else {
                    btnClear.setVisibility(View.GONE);
                }
                getFilter().filter(s.toString());
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        recyclerView.setLayoutManager(new MyLinearLayoutManager(getActivity()));
        RecyclerViewUtils recyclerViewUtils = new RecyclerViewUtils(new RecyclerViewUtils.RecyclerViewUtilsListener() {
            @Override
            public void onScrollToEnd() {
                if (!isLoading) {
                    getData(page + 1, size, txtSearch.getText().toString());
                }
            }
        });
        recyclerViewUtils.setUp(recyclerView);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getFilter().filter(txtSearch.getText().toString());
            }
        });
        listData = new ArrayList<>();
        adapter = new BaseRecyclerAdapter<ViewHolder, JsonObject>(R.layout.item_user_search, new BaseRecyclerAdapter.BaseViewHolder<ViewHolder, JsonObject>() {
            @Override
            public ViewHolder getViewHolder(View v) {
                return new ViewHolder(v);
            }

            @Override
            public void bindData(RecyclerView recyclerView, ViewHolder viewHolder, JsonObject jsonObject, int position) throws IOException {
                if(ConvertUtils.toBoolean(jsonObject.get("ShowAvatar"))) {
                    ImageUtils.loadImageByGlide(getActivity(), false, 0, 0, ConvertUtils.toString(jsonObject.get("Avartar")), R.drawable.img_no_image, R.drawable.img_no_image, viewHolder.imageView, true);
                }
                else
                {
                    viewHolder.imageView.setImageDrawable(ContextCompat.getDrawable(getContext(),R.drawable.default_avatar));
                }
                viewHolder.tvName.setText(ConvertUtils.toString(jsonObject.get("FullName")));
                String userName = ConvertUtils.toString(jsonObject.get("UserName"));
                if (!ConvertUtils.toBoolean(jsonObject.get("createSendBird"))) {
                    ServiceChat.startActionCreateChatAccount(getContext(), ConvertUtils.toString(jsonObject.get("UserName")), ConvertUtils.toString(jsonObject.get("FullName")), ConvertUtils.toString(jsonObject.get("Avartar")));
                    jsonObject.addProperty("createSendBird", true);
                }
                viewHolder.btnSendMessage.setTag(userName);
//                String bod = ConvertUtils.toString(jsonObject.get("Birthday"));
//                if (com.team500ae.vncapplication.utils.StringUtils.isNullOrWhiteSpace(bod)) {
//                    viewHolder.lnAge.setVisibility(View.GONE);
//                } else {
//                    viewHolder.lnAge.setVisibility(View.VISIBLE);
//                    long dou = ConvertUtils.toLong(bod);
//                    viewHolder.tvAge.setText(String.valueOf(TimeUtils.getAge(dou)));
//                }
                String phone = ConvertUtils.toString(jsonObject.get("PhoneNumber"));
                if (!com.team500ae.vncapplication.utils.StringUtils.isNullOrWhiteSpace(phone)) {
                    viewHolder.btnCall.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.ic_call));
                } else {
                    viewHolder.btnCall.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.ic_call_dim));
                }
                viewHolder.btnCall.setTag(phone);
                int gender = ConvertUtils.toInt(jsonObject.get("Gender"));
                if (gender == 0) {
                    viewHolder.imgGender.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.ic_female));
                } else {
                    viewHolder.imgGender.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.ic_male));
                }

                String address = ConvertUtils.toString(jsonObject.get("Address"));
                if ("".equals(address)) {
                    Geocoder geocoder;

                    geocoder = new Geocoder(getActivity(), Locale.getDefault());

                    List<Address> addresses = geocoder.getFromLocation(ConvertUtils.toDouble(jsonObject.get("Lat")), ConvertUtils.toDouble(jsonObject.get("Lon")), 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5
                    address = "";
                    if (addresses.size() != 0) {
                        Address _address = addresses.get(0);
                        if (!com.team500ae.vncapplication.utils.StringUtils.isNullOrWhiteSpace(_address.getLocality())) {
                            address += _address.getLocality();
                        }
                        if (!com.team500ae.vncapplication.utils.StringUtils.isNullOrWhiteSpace(_address.getSubAdminArea())) {
                            if (!address.isEmpty()) {
                                address += ", ";
                            }
                            address += _address.getSubAdminArea();
                        }
                        if (!com.team500ae.vncapplication.utils.StringUtils.isNullOrWhiteSpace(_address.getAdminArea())) {
                            if (!address.isEmpty()) {
                                address += ", ";
                            }
                            address += _address.getAdminArea();
                        }
                        if (!com.team500ae.vncapplication.utils.StringUtils.isNullOrWhiteSpace(_address.getCountryName())) {
                            if (!address.isEmpty()) {
                                address += ", ";
                            }
                            address += _address.getCountryName();
                        }
                        jsonObject.addProperty("Address", address);
                    }
                }
                viewHolder.tvAddress.setText(address);
                if (StringUtils.isEmpty(address)) {
                    viewHolder.tvAddress.setVisibility(View.GONE);
                } else
                    viewHolder.tvAddress.setVisibility(View.VISIBLE);
            }
        }, listData, new BaseRecyclerAdapter.OnClickListener() {
            @Override
            public void onClick(View v, int position, Object o) {
                JsonObject jsonObject = (JsonObject) o;
                UserProfileActivity.open(getActivity(), ConvertUtils.toString(jsonObject.get("UserName")));
            }
        });
        adapter.bindData(recyclerView);
        getFilter().filter("");
        return view;
    }

    public void getData(final int page, final int size, String keyword) {
        this.page = page;
        new AsyncTask<Void, Void, JsonArray>() {

            @Override
            protected void onPreExecute() {
                swipeRefreshLayout.setEnabled(false);
                if (page == 1)
                    swipeRefreshLayout.setRefreshing(true);
                else
                    loading.setVisibility(View.VISIBLE);
                isLoading = true;
                super.onPreExecute();
            }

            @Override
            protected JsonArray doInBackground(Void... voids) {
                try {
                    ServiceResponseEntity<JsonArray> result = new FriendInfo().getAll(getContext(), page, size, keyword);
                    if (page == 1) {
                        listData.clear();
                    }
                    if (result.getStatus() == 0) {
                        return result.getData();
                    }
                }catch (Exception e)
                {

                }

                return new JsonArray();
            }

            @Override
            protected void onPostExecute(JsonArray jsonArray) {
                for (int i = 0; i < jsonArray.size(); i++) {
                    JsonObject jsonObject = ConvertUtils.toJsonObject(jsonArray.get(i));
                    listData.add(jsonObject);
                    adapter.notifyItemInserted(listData.size() - 1);
                }

                swipeRefreshLayout.setEnabled(true);
                if (page == 1)
                    swipeRefreshLayout.setRefreshing(false);
                else
                    loading.setVisibility(View.GONE);
                isLoading = false;
                super.onPostExecute(jsonArray);
            }
        }.execute();

    }

    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;

    @OnClick(R.id.nav_friend_around)
    void friendAround() {
        SearchFriendActivity.open(getContext());
    }

    @BindView(R.id.btnClear)
    View btnClear;

    @OnClick(R.id.btnClear)
    void clear() {
        txtSearch.setText("");
        getFilter().filter("");
        btnClear.setVisibility(View.GONE);
    }


    public static ContactFragment getInstants() {
        ContactFragment contactFragment = new ContactFragment();
        return contactFragment;
    }

    @Override
    public Filter getFilter() {
        isLoading = true;
        swipeRefreshLayout.setEnabled(false);
        swipeRefreshLayout.setRefreshing(true);

        Filter filter = new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                FilterResults results = new FilterResults();
                // Skip the autocomplete query if no constraints are given.
                if (constraint != null) {
                    // Query the autocomplete API for the (constraint) search string.
                    ServiceResponseEntity<JsonArray> result = new FriendInfo().getAll(getContext(), 1, 100, constraint.toString());
                    results.values = result.getData();
                    results.count = result.getData().size();
                }
                return results;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                listData.clear();
                if (results != null && results.count > 0) {
                    JsonArray list = (JsonArray) results.values;
                    for (int i = 0; i < list.size(); i++)
                        listData.add(ConvertUtils.toJsonObject(list.get(i)));
                    // The API returned at least one result, update the data.
                }
                isLoading = false;
                swipeRefreshLayout.setEnabled(true);
                swipeRefreshLayout.setRefreshing(false);
                if (listData.size() == 0) {
                    viewData.setVisibility(View.GONE);
                    notFound.setVisibility(View.VISIBLE);
                } else {
                    viewData.setVisibility(View.VISIBLE);
                    notFound.setVisibility(View.GONE);
                }
                adapter.notifyDataSetChanged();
            }
        };
        return filter;
    }

    public void refresh() {
        getData(1, size, txtSearch.getText().toString());
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.imageView)
        ImageView imageView;
        @BindView(R.id.tvUserName)
        TextView tvName;
        @BindView(R.id.btnSendMessage)
        View btnSendMessage;
        @BindView(R.id.tvAddress)
        TextView tvAddress;
        @BindView(R.id.btnCall)
        ImageView btnCall;
        @BindView(R.id.imgGender)
        ImageView imgGender;
//        @BindView(R.id.lnAge)
//        View lnAge;
        @BindView(R.id.tvAge)
        TextView tvAge;

        void call(View view) {
            requestPermission(getActivity(), new AndroidPermissionUtils.OnCallbackRequestPermission() {
                @SuppressLint("MissingPermission")
                @Override
                public void onSuccess() {
                    String phone = (String) view.getTag();
                    if (phone != null && !phone.trim().isEmpty()) {
                        try {
                            Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + phone));
                            startActivity(intent);
                            return;
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                    showSnackBar(getActivity(), R.string.not_found_phone_number_in_profile);
                }

                @Override
                public void onFailed() {
                    showSnackBar(getActivity(), R.string.you_much_allow_this_app_make_a_call);
                }
            }, AndroidPermissionUtils.TypePermission.CALL_PHONE);
        }


        @OnClick(R.id.btnSendMessage)
        void viewProfile(View view) {
            String userId = (String) view.getTag();
            GroupChannelActivity.open(getContext(), userId);
        }

        @OnClick(R.id.btnCall)
        void callFriend(View view) {
            requestPermission(getActivity(), new AndroidPermissionUtils.OnCallbackRequestPermission() {
                @SuppressLint("MissingPermission")
                @Override
                public void onSuccess() {
                    String phone = (String) view.getTag();
                    if (phone != null && !phone.trim().isEmpty()) {
                        try {
                            Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + phone));
                            startActivity(intent);
                            return;
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                    BaseActivity.showSnackBar(getContext(), R.string.not_found_phone_number_in_profile);
                }

                @Override
                public void onFailed() {
                    showSnackBar(getActivity(), R.string.you_much_allow_this_app_make_a_call);
                }
            }, AndroidPermissionUtils.TypePermission.CALL_PHONE);
        }

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
