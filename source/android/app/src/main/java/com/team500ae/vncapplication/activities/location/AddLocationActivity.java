package com.team500ae.vncapplication.activities.location;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStates;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.google.android.gms.maps.model.LatLng;
import com.google.gson.JsonArray;
import com.team500ae.library.adapters.BaseRecyclerAdapter;
import com.team500ae.library.utils.AndroidPermissionUtils;
import com.team500ae.library.utils.ConvertUtils;
import com.team500ae.library.utils.ImageUtils;
import com.team500ae.library.utils.ViewUtils;
import com.team500ae.vncapplication.R;
import com.team500ae.vncapplication.base.BaseActivity;
import com.team500ae.vncapplication.constants.Constants;
import com.team500ae.vncapplication.data_access.location.LocationInfo;
import com.team500ae.vncapplication.data_access.location.TypeLocationInfo;
import com.team500ae.vncapplication.data_access.upload.BlobUploadProvider;
import com.team500ae.vncapplication.data_access.upload.UploadInfo;
import com.team500ae.vncapplication.models.BaseReturnFunctionEntity;
import com.team500ae.vncapplication.models.ServiceResponseEntity;
import com.team500ae.vncapplication.models.realm_models.location.TypeLocationEntity;
import com.team500ae.vncapplication.utils.FileUtils;
import com.team500ae.vncapplication.utils.GPSTrackerUtils;
import com.team500ae.vncapplication.utils.KeyboardUtils;
import com.team500ae.vncapplication.utils.LanguageUtils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by MaiNam on 3/27/2018.
 */

public class AddLocationActivity extends BaseActivity implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {
    protected static final int REQUEST_CHECK_SETTINGS = 0x1;
    private static final int RQ_SELECT_IMAGE = 0x2;
    String urlHeader = "http://maps.google.com/maps/api/staticmap?zoom=16&size=600x600&markers=";
    private static final int REQ_CODE_PLACE_PICKER = 10006;
    @BindView(R.id.addLocation_layout)
    LinearLayout addLocationLayout;
    private Place place;
    String urlFooter = "&sensor=false";
    private String urlImage;
    private GoogleApiClient mGoogleApiClient;
    private Uri uri;
    private LatLng latlon;
    int type = 3;
    private ArrayList<Integer> typeID;
    private ArrayList<String> typeName;
    List<UriImage> listImages;
    @BindView(R.id.recyclerView1)
    RecyclerView recyclerView1;
    int index = 0;
    private BaseRecyclerAdapter<ViewHolder, UriImage> adapter;
    private Uri tempUri;

    class UriImage {
        Uri uri;

        public UriImage(Uri uri) {
            this.uri = uri;
        }

        public UriImage() {

        }
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        setContentView(R.layout.activity_add_location);
        ButterKnife.bind(this);
        KeyboardUtils.setupUI(addLocationLayout, getActivity());
        super.onCreate(savedInstanceState);
        getSupportActionBar().setTitle(R.string.add_location);

        listImages = new ArrayList<>();
        listImages.add(new UriImage());
        adapter = new BaseRecyclerAdapter<>(R.layout.item_image_selected, new BaseRecyclerAdapter.BaseViewHolder<ViewHolder, UriImage>() {
            @Override
            public ViewHolder getViewHolder(View v) {
                return new ViewHolder(v);
            }

            @Override
            public void bindData(RecyclerView recyclerView, ViewHolder viewHolder, UriImage data, int position) {
                if (data.uri == null) {
                    viewHolder.imageView.setVisibility(View.GONE);
                    viewHolder.imgClose.setVisibility(View.GONE);
                    viewHolder.imgCamera.setVisibility(View.VISIBLE);
                } else {
                    ImageUtils.loadImageByGlide(getActivity(), false, 0, 0, data.uri, R.drawable.bg_map_vn, R.drawable.bg_map_vn, viewHolder.imageView, true);
                    viewHolder.imageView.setVisibility(View.VISIBLE);
                    viewHolder.imgClose.setVisibility(View.VISIBLE);
                    viewHolder.imgCamera.setVisibility(View.GONE);
                }
            }
        }, listImages, new BaseRecyclerAdapter.OnClickListener() {
            @Override
            public void onClick(View v, int position, Object o) {

            }
        });
        recyclerView1.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));
        adapter.bindData(recyclerView1);
        showBackButton();
        ViewUtils.hideKeyboard(getActivity());
        showListType();
        GPSTrackerUtils gpsTrackerUtils = new GPSTrackerUtils(getActivity());
        if (gpsTrackerUtils.canGetLocation()) {
            Log.d(TAG, "canGetLocation");
            latlon = new LatLng(gpsTrackerUtils.getLatitude(), gpsTrackerUtils.getLongitude());
            GPSTrackerUtils.saveCurrentLocation(getActivity(), gpsTrackerUtils.getLongitude(), gpsTrackerUtils.getLatitude());
            bindData(true);
            return;
        } else {
            settingsrequest();
        }
    }

    @BindView(R.id.imgLocation)
    ImageView imgLocation;

    @OnClick(R.id.imgLocation)
    void getLocation() {
        PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();
        try {
            startActivityForResult(builder.build(getActivity()), REQ_CODE_PLACE_PICKER);
        } catch (GooglePlayServicesRepairableException e) {
            showSnackBar("Bạn cần cài đặt google playservice để sử dụng tính năng này");
            e.printStackTrace();
        } catch (GooglePlayServicesNotAvailableException e) {
            showSnackBar("Bạn cần cài đặt google playservice để sử dụng tính năng này");
            e.printStackTrace();
        }
    }

    @BindView(R.id.txtAddress)
    EditText txtAddress;

    @BindView(R.id.txtLocationName)
    EditText txtLocationName;

    @BindView(R.id.txtContent)
    EditText txtContent;

    @BindView(R.id.txtContact)
    EditText txtContact;

    @OnClick(R.id.btnAdd)
    void add() {

        if (place == null && latlon == null) {
            showSnackBar(R.string.please_select_place);
            return;
        }
        if (txtLocationName.getText().toString().trim().equals("")) {
            showSnackBar(R.string.please_input_location_name);
            return;
        }
        if (txtLocationName.getText().toString().trim().equals("")) {
            showSnackBar(R.string.please_input_location_name);
            return;
        }
        if (txtAddress.getText().toString().trim().equals("")) {
            showSnackBar(R.string.please_input_location_address);
            return;
        }
        if (txtContact.getText().toString().trim().equals("")) {
            showSnackBar(R.string.please_input_location_contact);
            return;
        }
        if (txtContent.getText().toString().trim().equals("")) {
            showSnackBar(R.string.please_input_location_content);
            return;
        }
        if (txtLocationType.getSelectedItemPosition() < 0) {
            showSnackBar(R.string.please_select_location_type);
            return;
        }
        final int locationType = typeID.get(txtLocationType.getSelectedItemPosition());

        ArrayList<Uri> listUri = new ArrayList<>();
        for (UriImage uriImage : listImages) {
            if (uriImage.uri != null)
                listUri.add(uriImage.uri);
        }
        if (listUri.size() == 0) {
            showSnackBar(R.string.please_select_image_for_location);
            return;
        }
        new UploadInfo(new BlobUploadProvider() {
        }, new UploadInfo.UploadListener() {
            @Override
            public void beginUpload() {
                try {
                    dialog = ProgressDialog.show(getActivity(), "",
                            getString(R.string.please_wait), true);

                    dialog.show();
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void endUpload() {

            }

            @Override
            public void onSuccess(JsonArray response) {
                if (response.size() == listUri.size()) {
                    addLocation(txtLocationName.getText().toString(), locationType, txtContent.getText().toString(), latlon.latitude, latlon.longitude, txtContact.getText().toString(), ConvertUtils.toJson(response), txtAddress.getText().toString());
                } else {
                    showSnackBar(R.string.msg_upload_image_error);
                }
            }

            @Override
            public void onError(ServiceResponseEntity<JsonArray> response) {
                try {
                    dialog.hide();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }).upload(getActivity(), Constants.UPLOAD_FILE, listUri);
    }

    @BindView(R.id.txtLocationType)
    Spinner txtLocationType;

    void showListType() {
        try {
            BaseReturnFunctionEntity<ArrayList<TypeLocationEntity>> typeLocationEntities = new TypeLocationInfo().getListTypeLocation(getActivity(), getRealm());
            typeID = new ArrayList<>();
            typeName = new ArrayList<>();
            String appLanguageSetting = LanguageUtils.getAppLanguageSetting(this);
            if (typeLocationEntities.isTrue())
                for (TypeLocationEntity type : typeLocationEntities.getData()) {
                    typeID.add(type.getId());
                    typeName.add(appLanguageSetting.equals("en") ? type.getName() : type.getEnName());
                }

            txtLocationType.setAdapter(new ArrayAdapter<String>(getApplicationContext(), R.layout.item_spinner, typeName));
        } catch (Exception ex) {
        }
    }

    private void addLocation(String name, int type, String content, double lat, double lon, String contact, String thumbnail, String address) {
        new AsyncTask<Void, Void, Boolean>() {
            @Override
            protected void onPreExecute() {
                try {
                    dialog.show();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                super.onPreExecute();
            }

            @Override
            protected Boolean doInBackground(Void... voids) {
                LocationInfo locationInfo = new LocationInfo();
                BaseReturnFunctionEntity<Boolean> result = locationInfo.addLocation(name, type, content, lat, lon, contact, thumbnail, address);
                if (result.isTrue())
                    return result.getData();
                return false;
            }

            @Override
            protected void onPostExecute(Boolean aVoid) {
                if (aVoid != null && aVoid) {
                    BaseActivity.showSnackBar(getActivity(), R.string.add_location_success);
                    getHandler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            finish();
                        }
                    }, 1000);
                } else
                    BaseActivity.showSnackBar(getActivity(), R.string.add_location_error);

                try {
                    dialog.hide();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                super.onPostExecute(aVoid);
            }
        }.execute();
    }

    public void settingsrequest() {

        mGoogleApiClient = new GoogleApiClient.Builder(getActivity())
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();
        mGoogleApiClient.connect();

        LocationRequest locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(30 * 1000);
        locationRequest.setFastestInterval(5 * 1000);
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(locationRequest);
        builder.setAlwaysShow(true); //this is the key ingredient

        PendingResult<LocationSettingsResult> result =
                LocationServices.SettingsApi.checkLocationSettings(mGoogleApiClient, builder.build());
        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(LocationSettingsResult result) {
                final Status status = result.getStatus();
                final LocationSettingsStates state = result.getLocationSettingsStates();
                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.SUCCESS:
                        Log.d(TAG, "SUCCESS");
                        try {
                            status.startResolutionForResult(getActivity(), REQUEST_CHECK_SETTINGS);
                        } catch (IntentSender.SendIntentException e) {
                            finish();
                        }
                        break;
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        Log.d(TAG, "RESOLUTION_REQUIRED");
                        try {
                            status.startResolutionForResult(getActivity(), REQUEST_CHECK_SETTINGS);
                        } catch (IntentSender.SendIntentException e) {
                            e.printStackTrace();
                            finish();
                        }
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        finish();
                        break;
                }
            }
        });
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        switch (requestCode) {
            case RQ_SELECT_IMAGE:
                if (resultCode == RESULT_OK) {
                    uri = FileUtils.getSelectedImageFromResult(getActivity(), resultCode, data, tempUri);
                    if (index < listImages.size() && index >= 0) {
                        listImages.set(index, new UriImage(uri));
                        adapter.notifyItemChanged(index);
                    }
                    if (index == listImages.size() - 1) {
                        if (listImages.size() < 4) {
                            listImages.add(new UriImage());
                            adapter.notifyItemInserted(listImages.size() - 1);
                        }
                    }
                }
                break;
            case REQUEST_CHECK_SETTINGS:
                switch (resultCode) {
                    case Activity.RESULT_OK:
                        getHandler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                GPSTrackerUtils gpsTrackerUtils = new GPSTrackerUtils(getActivity());
                                if (gpsTrackerUtils.canGetLocation()) {
                                    GPSTrackerUtils.saveCurrentLocation(getActivity(), gpsTrackerUtils.getLongitude(), gpsTrackerUtils.getLatitude());
                                }
                                bindData(false);
                            }
                        }, 1000);
                        break;
                    case Activity.RESULT_CANCELED:
                        bindData(true);
                        break;
                    default:
                        bindData(true);
                        break;
                }
                break;
            case REQ_CODE_PLACE_PICKER:
                if (resultCode == RESULT_OK) {
                    place = PlacePicker.getPlace(getActivity(), data);
                    if (place.getName() != null) {
                        txtLocationName.setText(place.getName());
                    }
                    if (place.getAddress() != null) {
                        txtAddress.setText(place.getAddress());
                    }
                    if (place.getWebsiteUri() != null) {
                        String contact = "";
                        if (TextUtils.isEmpty(place.getPhoneNumber()))
                            contact += place.getPhoneNumber() + "\n";
                        if (TextUtils.isEmpty(place.getWebsiteUri().toString()))
                            contact += place.getWebsiteUri().toString();
                        txtContact.setText(contact);
                    }
                    latlon = place.getLatLng();
                    urlImage = urlHeader + place.getLatLng().latitude + "," + place.getLatLng().longitude + urlFooter;
                    ImageUtils.loadImageByGlide(getActivity(), false, 0, 150, urlImage, R.drawable.bg_map_vn, R.drawable.bg_map_vn, imgLocation, true);
                }
                break;
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void bindData(boolean withLonLat) {
//        getControl();

        double latitude = GPSTrackerUtils.getCurrentLocationLatitude(getActivity());
        double longitude = GPSTrackerUtils.getCurrentLocationLongitude(getActivity());
        if (!withLonLat) {
            latitude = GPSTrackerUtils.getCurrentLocationLatitude(getActivity());
            longitude = GPSTrackerUtils.getCurrentLocationLongitude(getActivity());
        } else
//            tvPlace.setText(xa + ", " + huyen + ", " + tinh);
            if (latitude != 0 | longitude != 0) {
                urlImage = urlHeader + latitude + "," + longitude + urlFooter;
                ImageUtils.loadImageByGlide(getActivity(), false, 0, 150, urlImage, R.drawable.bg_map_vn, R.drawable.bg_map_vn, imgLocation, true);
            }
    }

    public static void open(Context context) {
        Intent intent = new Intent(context, AddLocationActivity.class);
        context.startActivity(intent);
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.imageView)
        ImageView imageView;
        @BindView(R.id.imgCamera)
        View imgCamera;
        @BindView(R.id.imgClose)
        View imgClose;

        @OnClick(R.id.imgClose)
        void remove() {
            int index = getAdapterPosition();
            listImages.remove(index);
            adapter.notifyItemRemoved(index);
            if (listImages.size() > 0) {
                if (listImages.get(listImages.size() - 1).uri != null) {
                    listImages.add(new UriImage());
                }
            }
        }

        @OnClick(R.id.lnImage)
        void selectImage() {
            index = getAdapterPosition();
            requestPermission(new AndroidPermissionUtils.OnCallbackRequestPermission() {
                @Override
                public void onSuccess() {

                    tempUri = FileUtils.createTempFile(getActivity());
                    Intent pickIntent = new FileUtils().selectOrTakePicture(getActivity(), tempUri);
                    startActivityForResult(pickIntent, RQ_SELECT_IMAGE);
                }

                @Override
                public void onFailed() {

                }
            }, AndroidPermissionUtils.TypePermission.PERMISSION_READ_EXTERNAL_STORAGE);

        }

        public ViewHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);
        }
    }
}
