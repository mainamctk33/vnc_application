package com.team500ae.vncapplication.data_access.location;

import android.content.Context;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.team500ae.library.utils.ConvertUtils;
import com.team500ae.vncapplication.R;
import com.team500ae.vncapplication.constants.Constants;
import com.team500ae.vncapplication.data_access.ClientInfo;
import com.team500ae.vncapplication.models.BaseReturnFunctionEntity;
import com.team500ae.vncapplication.models.ServiceResponseEntity;
import com.team500ae.vncapplication.models.realm_models.location.MyTypeLocationEntity;
import com.team500ae.vncapplication.models.realm_models.location.TypeLocationEntity;

import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmResults;
import io.realm.exceptions.RealmError;

/**
 * Created by maina on 3/30/2018.
 */

public class TypeLocationInfo {
    public ServiceResponseEntity<JsonArray> syncTypeLocation(Context context, long lastSyncDate) {
        try {
            ClientInfo.DataResponse dataResponse = new ClientInfo().requestApi(ClientInfo.MethodType.GET, Constants.API_TYPE_LOCATION_SYNC + "/" + lastSyncDate);
            if (dataResponse.isOK()) {
                ServiceResponseEntity<JsonArray> result = dataResponse.getData(new TypeToken<ServiceResponseEntity<JsonArray>>() {
                }.getType());
                return result;
            }
        } catch (UnknownHostException e) {
            e.printStackTrace();
        } catch (SocketTimeoutException e) {
            e.printStackTrace();
        }
        return null;
    }

    public BaseReturnFunctionEntity<Boolean> removeTypeLocationFromListFavorite(Context context, Realm realm, String userId, int... listTypeLocationIds) {
        if (listTypeLocationIds == null || listTypeLocationIds.length == 0)
            return new BaseReturnFunctionEntity<>(BaseReturnFunctionEntity.StatusFunction.FALSE, context.getString(R.string.select_typelocation_to_remove));
        try {
            realm.executeTransaction(realm1 -> {
                for (int id : listTypeLocationIds) {
                    MyTypeLocationEntity myTypeLocationEntity = realm.where(MyTypeLocationEntity.class).equalTo("id", userId + id).findFirst();
                    if (myTypeLocationEntity != null)
                        myTypeLocationEntity.deleteFromRealm();
                }
            });
            return new BaseReturnFunctionEntity<>(BaseReturnFunctionEntity.StatusFunction.TRUE, true);
        } catch (RealmError e) {
            return new BaseReturnFunctionEntity<>(BaseReturnFunctionEntity.StatusFunction.EXCEPTION, e.getMessage());
        } catch (Exception e) {
            return new BaseReturnFunctionEntity<>(BaseReturnFunctionEntity.StatusFunction.EXCEPTION, e.getMessage());
        } catch (OutOfMemoryError e) {
            return new BaseReturnFunctionEntity<>(BaseReturnFunctionEntity.StatusFunction.EXCEPTION, e.getMessage());
        }
    }


    public BaseReturnFunctionEntity<Boolean> saveTypeLocationToFavorite(Context context, Realm realm, String userId, int... listTypeLocationIds) {
        if (listTypeLocationIds == null || listTypeLocationIds.length == 0)
            return new BaseReturnFunctionEntity<>(BaseReturnFunctionEntity.StatusFunction.FALSE, context.getString(R.string.select_typelocation_to_set_favorite));
        try {
            realm.executeTransaction(realm1 -> {
                for (int id : listTypeLocationIds) {
                    TypeLocationEntity typeLocationEntity = realm.where(TypeLocationEntity.class).equalTo("id", id).findFirst();
                    if (typeLocationEntity != null) {
                        MyTypeLocationEntity myTypeLocationEntity = new MyTypeLocationEntity();
                        myTypeLocationEntity.setId(userId + id);
                        myTypeLocationEntity.setTypeLocation(typeLocationEntity);
                        myTypeLocationEntity.setUserId(userId);
                        realm.copyToRealmOrUpdate(myTypeLocationEntity);
                    }
                }
            });
            return new BaseReturnFunctionEntity<>(BaseReturnFunctionEntity.StatusFunction.TRUE, true);
        } catch (RealmError e) {
            return new BaseReturnFunctionEntity<>(BaseReturnFunctionEntity.StatusFunction.EXCEPTION, e.getMessage());
        } catch (Exception e) {
            return new BaseReturnFunctionEntity<>(BaseReturnFunctionEntity.StatusFunction.EXCEPTION, e.getMessage());
        } catch (OutOfMemoryError e) {
            return new BaseReturnFunctionEntity<>(BaseReturnFunctionEntity.StatusFunction.EXCEPTION, e.getMessage());
        }
    }

    public BaseReturnFunctionEntity<ArrayList<TypeLocationEntity>> getListTypeLocation(Context context, Realm realm) {
        try {
            RealmResults<TypeLocationEntity> realmList = realm.where(TypeLocationEntity.class).findAll();
            ArrayList<TypeLocationEntity> result = new ArrayList<>();
            result.addAll(realmList);
            return new BaseReturnFunctionEntity<>(BaseReturnFunctionEntity.StatusFunction.TRUE, result);
        } catch (RealmError e) {
            return new BaseReturnFunctionEntity<>(BaseReturnFunctionEntity.StatusFunction.EXCEPTION, e.getMessage());
        } catch (Exception e) {
            return new BaseReturnFunctionEntity<>(BaseReturnFunctionEntity.StatusFunction.EXCEPTION, e.getMessage());
        } catch (OutOfMemoryError e) {
            return new BaseReturnFunctionEntity<>(BaseReturnFunctionEntity.StatusFunction.EXCEPTION, e.getMessage());
        }
    }

    public BaseReturnFunctionEntity<ArrayList<TypeLocationEntity>> getTypeLocationHasSelected(Context context, Realm realm, String userId) {
        try {
            RealmResults<MyTypeLocationEntity> realmList = realm.where(MyTypeLocationEntity.class).equalTo("userId", userId).findAll();
            ArrayList<TypeLocationEntity> result = new ArrayList<>();
            for (MyTypeLocationEntity typeLocationEntity : realmList) {
                result.add(typeLocationEntity.getTypeLocation());
            }
            return new BaseReturnFunctionEntity<>(BaseReturnFunctionEntity.StatusFunction.TRUE, result);
        } catch (RealmError e) {
            return new BaseReturnFunctionEntity<>(BaseReturnFunctionEntity.StatusFunction.EXCEPTION, e.getMessage());
        } catch (Exception e) {
            return new BaseReturnFunctionEntity<>(BaseReturnFunctionEntity.StatusFunction.EXCEPTION, e.getMessage());
        } catch (OutOfMemoryError e) {
            return new BaseReturnFunctionEntity<>(BaseReturnFunctionEntity.StatusFunction.EXCEPTION, e.getMessage());
        }
    }

    public BaseReturnFunctionEntity<Boolean> updateTypeLocation(Realm realm, JsonArray data) {
        try {
            RealmResults<TypeLocationEntity> realmList = realm.where(TypeLocationEntity.class).findAll();
            List<Integer> listID = new ArrayList<>();
            realm.executeTransaction(realm1 -> {
                for (int i = 0; i < data.size(); i++) {
                    JsonObject jsonObject = ConvertUtils.toJsonObject(data.get(i));
                    int idData = ConvertUtils.toInt(jsonObject.get("ID"));
                    listID.add(idData);
                    updateTypeLocation(realm, jsonObject, false);
                }
            });

            for (int j = 0; j < realmList.size(); j++) {
                int idRealm = realmList.get(j).getId();
                if (!listID.contains(idRealm)) {
                    realm.executeTransaction(new Realm.Transaction() {
                        @Override
                        public void execute(Realm realm) {
                            RealmResults<TypeLocationEntity> result = realm.where(TypeLocationEntity.class).equalTo("id", idRealm).findAll();
                            result.deleteAllFromRealm();
                        }
                    });

                }
            }
            /*realm.executeTransaction(realm1 -> {
                for (int i = 0; i < data.size(); i++) {
                    JsonObject jsonObject = ConvertUtils.toJsonObject(data.get(i));
                    updateTypeLocation(realm, jsonObject, false);
                }
            });*/
            return new BaseReturnFunctionEntity<>(BaseReturnFunctionEntity.StatusFunction.TRUE, true);
        } catch (RealmError e) {
            return new BaseReturnFunctionEntity<>(BaseReturnFunctionEntity.StatusFunction.EXCEPTION, e.getMessage());
        } catch (Exception e) {
            return new BaseReturnFunctionEntity<>(BaseReturnFunctionEntity.StatusFunction.EXCEPTION, e.getMessage());
        } catch (OutOfMemoryError e) {
            return new BaseReturnFunctionEntity<>(BaseReturnFunctionEntity.StatusFunction.EXCEPTION, e.getMessage());
        }
    }

    public BaseReturnFunctionEntity<Boolean> updateTypeLocation(Realm realm, final JsonObject data, boolean needTransacsion) {
        try {
            if (needTransacsion) {
                realm.executeTransaction(realm1 -> {
                    updateTypeLocation(realm, data, false);
                });
            } else {
                int id = ConvertUtils.toInt(data.get("ID"));
                TypeLocationEntity typeLocationEntity = new TypeLocationEntity();
                typeLocationEntity.setName(ConvertUtils.toString(data.get("Name")));
                typeLocationEntity.setLogo(ConvertUtils.toString(data.get("Logo")));
                typeLocationEntity.setEnName(ConvertUtils.toString(data.get("EnName")));
                typeLocationEntity.setId(id);
                realm.copyToRealmOrUpdate(typeLocationEntity);
            }
            return new BaseReturnFunctionEntity<>(BaseReturnFunctionEntity.StatusFunction.EXCEPTION, true);
        } catch (RealmError e) {
            return new BaseReturnFunctionEntity<>(BaseReturnFunctionEntity.StatusFunction.EXCEPTION, e.getMessage());
        } catch (Exception e) {
            return new BaseReturnFunctionEntity<>(BaseReturnFunctionEntity.StatusFunction.EXCEPTION, e.getMessage());
        } catch (OutOfMemoryError e) {
            return new BaseReturnFunctionEntity<>(BaseReturnFunctionEntity.StatusFunction.EXCEPTION, e.getMessage());
        }
    }
}
