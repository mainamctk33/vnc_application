package com.team500ae.vncapplication.activities.location.fragments;

import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.mahc.custombottomsheetbehavior.BottomSheetBehaviorGoogleMapsLike;
import com.mahc.custombottomsheetbehavior.MergedAppBarLayout;
import com.mahc.custombottomsheetbehavior.MergedAppBarLayoutBehavior;
import com.team500ae.library.utils.ConvertUtils;
import com.team500ae.library.utils.StringUtils;
import com.team500ae.vncapplication.R;
import com.team500ae.vncapplication.activities.location.RateDiaglog;
import com.team500ae.vncapplication.activities.location.adapter.ItemPagerAdapter;
import com.team500ae.vncapplication.activities.login.LoginActivity;
import com.team500ae.vncapplication.base.BaseActivity;
import com.team500ae.vncapplication.data_access.BookmarkInfo;
import com.team500ae.vncapplication.data_access.location.LocationInfo;
import com.team500ae.vncapplication.data_access.user.UserInfo;
import com.team500ae.vncapplication.models.BaseReturnFunctionEntity;
import com.team500ae.vncapplication.others.DialogListener;
import com.team500ae.vncapplication.services.ServiceLocation;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by maina on 4/4/2018.
 */

public class DetailLocationFragment extends Fragment implements OnMapReadyCallback {
    private static final String TAG = DetailLocationFragment.class.getSimpleName();
    private static final float DEFAULT_ZOOM = 1.5f;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    private JsonObject location;
    private GoogleMap googleMap;
    int locationId;
    BaseActivity baseActivity;
    private boolean hasBookmark;
    float myRate = 0;
    private RateDiaglog dialogRate;
    private String myComment;
    private String titleLocation = "";
    private AsyncTask<Void, Void, JsonObject> asyncTaskDetailLocation;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_detail_location, container, false);
        ButterKnife.bind(this, view);
        Bundle bundle = getArguments();
        location = new JsonObject();
        if (bundle != null) {
            String data = bundle.getString("DATA_LOCATION");
            location = ConvertUtils.toJsonObject(data);
            Log.d(TAG, "onCreateView: " + data);
            titleLocation = ConvertUtils.toString(location.get("Title"));
            ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);
            android.support.v7.app.ActionBar supportActionBar = ((AppCompatActivity) getActivity()).getSupportActionBar();
            if (supportActionBar != null) {
                supportActionBar.setDisplayHomeAsUpEnabled(true);
                supportActionBar.setTitle(ConvertUtils.toString(location.get("Title")));
            }
        }
        locationId = ConvertUtils.toInt(location.get("ID"));
        baseActivity = (BaseActivity) getContext();
        BaseReturnFunctionEntity<Boolean> bookmark = new BookmarkInfo().isBookmark(getActivity(), baseActivity.getRealm(), locationId, UserInfo.getCurrentUserId(), BookmarkInfo.BookmarkType.Location);
        hasBookmark = bookmark.isTrue() ? bookmark.data : false;
        imgBookmark.setImageDrawable(ContextCompat.getDrawable(getActivity(), hasBookmark ? R.drawable.ic_bookmark_1 : R.drawable.ic_bookmark_0));
        getDetailLocation(locationId);

        final BottomSheetBehaviorGoogleMapsLike behavior = BottomSheetBehaviorGoogleMapsLike.from(bottomSheet);
        behavior.addBottomSheetCallback(new BottomSheetBehaviorGoogleMapsLike.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {
                switch (newState) {
                    case BottomSheetBehaviorGoogleMapsLike.STATE_COLLAPSED:
                        Log.d("bottomsheet-", "STATE_COLLAPSED");
                        break;
                    case BottomSheetBehaviorGoogleMapsLike.STATE_DRAGGING:
                        Log.d("bottomsheet-", "STATE_DRAGGING");
                        break;
                    case BottomSheetBehaviorGoogleMapsLike.STATE_EXPANDED:
                        Log.d("bottomsheet-", "STATE_EXPANDED");
                        break;
                    case BottomSheetBehaviorGoogleMapsLike.STATE_ANCHOR_POINT:
                        Log.d("bottomsheet-", "STATE_ANCHOR_POINT");
                        break;
                    case BottomSheetBehaviorGoogleMapsLike.STATE_HIDDEN:
                        Log.d("bottomsheet-", "STATE_HIDDEN");
                        break;
                    default:
                        Log.d("bottomsheet-", "STATE_SETTLING");
                        break;
                }
            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {
            }
        });
        MergedAppBarLayoutBehavior mergedAppBarLayoutBehavior = MergedAppBarLayoutBehavior.from(mergedAppBarLayout);
        behavior.setState(BottomSheetBehaviorGoogleMapsLike.STATE_COLLAPSED);
        if (!TextUtils.isEmpty(titleLocation))
            mergedAppBarLayoutBehavior.setToolbarTitle(titleLocation);
        mergedAppBarLayoutBehavior.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                behavior.setState(BottomSheetBehaviorGoogleMapsLike.STATE_COLLAPSED);
            }
        });
        //bindData();
        SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.support_map);
        mapFragment.getMapAsync(this);
        getMyRate();
        return view;
    }

    @BindView(R.id.mergedappbarlayout)
    MergedAppBarLayout mergedAppBarLayout;
    @BindView(R.id.coordinatorlayout)
    CoordinatorLayout coordinatorLayout;
    @BindView(R.id.bottom_sheet)
    View bottomSheet;

    @BindView(R.id.tvName)
    TextView tvName;
    @BindView(R.id.rtRating)
    RatingBar rtRating;
    @BindView(R.id.tvRatingValue)
    TextView tvRatingValue;
    @BindView(R.id.tvTotalPersonRating)
    TextView tvTotalPersonRating;
    @BindView(R.id.pager)
    ViewPager viewPager;
    @BindView(R.id.tvAddress)
    TextView tvAddress;
    @BindView(R.id.tvContent)
    TextView tvContent;
    @BindView(R.id.tvContact)
    TextView tvContact;

    @OnClick(R.id.tvContact)
    void call() {
        if (tvContact.getText() != null && !tvContact.getText().toString().trim().equals("")) {
            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("tel:" + tvContact.getText()));
            startActivity(intent);
        }
    }

    @BindView(R.id.lnContact)
    View lnContact;
    @BindView(R.id.lnContent)
    View lnContent;
    @BindView(R.id.lnAddress)
    View lnAddress;

    @OnClick(R.id.lnDirection)
    void direction() {
        double lat = ConvertUtils.toDouble(location.get("Lat"));
        double lng = ConvertUtils.toDouble(location.get("Lon"));

        Intent intent = new Intent(Intent.ACTION_VIEW,
                Uri.parse("http://maps.google.com/maps?daddr=" + lat + "," + lng));
        startActivity(intent);
    }



    @BindView(R.id.imgBookmark)
    ImageView imgBookmark;

    @OnClick(R.id.btnBookmark)
    void bookmark() {
        if (!UserInfo.isLogin()) {
            LoginActivity.open(getActivity());
            return;
        } else {
            hasBookmark = !hasBookmark;
            ServiceLocation.startActionBookmark(getActivity(), locationId, hasBookmark);
            imgBookmark.setImageDrawable(ContextCompat.getDrawable(getActivity(), hasBookmark ? R.drawable.ic_bookmark_1 : R.drawable.ic_bookmark_0));
        }
    }

    @OnClick(R.id.btnMakeRate)
    void rate() {
        if (UserInfo.isLogin()) {
            dialogRate = new RateDiaglog(getActivity(), myRate, myComment);
            dialogRate.setOnItemClickListener(new DialogListener() {
                @Override
                public void onConfirmClicked() {
                    myRate = dialogRate.rtRating.getRating();
                    myComment = dialogRate.txtComment.getText().toString();
                    ServiceLocation.startActionRate(getContext(), locationId, myRate, myComment);
                    dialogRate.dismiss();
                }

                @Override
                public void onButtonChooseImage() {

                }
            });
            dialogRate.show();
        } else {
            LoginActivity.open(getActivity());
        }
    }

    void getMyRate() {
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... voids) {
                BaseReturnFunctionEntity<JsonObject> result = new LocationInfo().getMyRating(locationId);
                if (result.isTrue()) {
                    myRate = ConvertUtils.toFloat(result.getData().get("Value"));
                    myComment = ConvertUtils.toString(result.getData().get("Comment"));
                }
                return null;
            }
        }.execute();
    }

    public static Fragment getInstants(JsonObject data) {
        DetailLocationFragment detailLocationFragment = new DetailLocationFragment();
        Bundle bundle = new Bundle();
        bundle.putString("DATA_LOCATION", ConvertUtils.toJson(data));
        detailLocationFragment.setArguments(bundle);
        return detailLocationFragment;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.googleMap = googleMap;
        MarkerOptions markerOptions = new MarkerOptions();

        double lat = ConvertUtils.toDouble(location.get("Lat"));
        double lng = ConvertUtils.toDouble(location.get("Lon"));
        String placeName = ConvertUtils.toString(location.get("Title"));
        LatLng latLng = new LatLng(lat, lng);
        markerOptions.position(latLng);
        markerOptions.title(placeName);
        Marker marker = this.googleMap.addMarker(markerOptions);
        this.googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 12.0f));
    }

    public void getDetailLocation(int idLocation) {
        asyncTaskDetailLocation = new AsyncTask<Void, Void, JsonObject>() {
            ProgressDialog progDialog = new ProgressDialog(getActivity());

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                progDialog.setMessage(getString(R.string.loading));
                progDialog.setIndeterminate(false);
                progDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                progDialog.setCancelable(true);
                progDialog.show();
            }

            @Override
            protected JsonObject doInBackground(Void... voids) {
                BaseReturnFunctionEntity<JsonObject> result = new LocationInfo().getDetail(idLocation);
                if (result.isTrue()) {
                    return result.getData();
                }
                return new JsonObject();
            }

            @Override
            protected void onPostExecute(JsonObject jsonObject) {
                if (progDialog.isShowing()) {
                    progDialog.dismiss();
                }
                bindData(jsonObject);
            }
        };
        asyncTaskDetailLocation.execute();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (asyncTaskDetailLocation != null) {
            asyncTaskDetailLocation.cancel(true);
        }
    }

    private void bindData(JsonObject jsonObject) {
        tvName.setText(ConvertUtils.toString(jsonObject.get("Title")));
        String thumbnail = ConvertUtils.toString(jsonObject.get("Thumbnail"));
        JsonArray jsonArray = ConvertUtils.toJsonArray(thumbnail);
        ArrayList<String> items = new ArrayList<>();
        for (int i = 0; i < jsonArray.size(); i++) {
            items.add(ConvertUtils.toString(jsonArray.get(i)));
        }
        if (items.size() != 0) {
            ItemPagerAdapter adapter = new ItemPagerAdapter(getActivity(), items);
            viewPager.setAdapter(adapter);
            viewPager.setVisibility(View.VISIBLE);
        }
        tvTotalPersonRating.setText(ConvertUtils.toString(jsonObject.get("TotalMemberRate")));
        rtRating.setMax(5);
        tvRatingValue.setText(String.valueOf(ConvertUtils.toFloat(jsonObject.get("Rate"))));
        rtRating.setRating(ConvertUtils.toFloat(jsonObject.get("Rate")));
        String data = ConvertUtils.toString(jsonObject.get("Contact"));
        if (StringUtils.isEmpty(data))
            lnContact.setVisibility(View.GONE);
        tvContact.setText(data);
        data = ConvertUtils.toString(jsonObject.get("Content"));
        if (StringUtils.isEmpty(data))
            lnContent.setVisibility(View.GONE);
        tvContent.setText(ConvertUtils.toString(jsonObject.get("Content")));
        data = ConvertUtils.toString(jsonObject.get("Address"));
        if (StringUtils.isEmpty(data))
            lnAddress.setVisibility(View.GONE);
        tvAddress.setText(ConvertUtils.toString(jsonObject.get("Address")));
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }
}
