package com.team500ae.vncapplication.data_access.question;

import android.content.Context;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.team500ae.library.utils.ConvertUtils;
import com.team500ae.vncapplication.constants.Constants;
import com.team500ae.vncapplication.data_access.ClientInfo;
import com.team500ae.vncapplication.models.BaseReturnFunctionEntity;
import com.team500ae.vncapplication.models.ServiceResponseEntity;

import java.net.SocketTimeoutException;
import java.net.UnknownHostException;

/**
 * Created by phuong.ndp on 15/04/2018.
 */

public class QuestionInfo {
    public static ServiceResponseEntity<JsonArray> getListMyQuestions(Context context, String username) {
        try {
//            ClientInfo.DataResponse dataResponse = new ClientInfo().requestApi(ClientInfo.MethodType.GET, Constants.API_QUESTION_GET_MY_QUESTION + "?username=" + username);
            ClientInfo.DataResponse dataResponse = new ClientInfo().requestApi(ClientInfo.MethodType.GET, Constants.API_QUESTION_GET_MY_QUESTION);
            if (dataResponse.isOK()) {
                ServiceResponseEntity<JsonArray> result = dataResponse.getData(new TypeToken<ServiceResponseEntity<JsonArray>>() {
                }.getType());
                return result;
            }
        } catch (UnknownHostException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static ServiceResponseEntity<JsonArray> getListCommonQuestions(Context context) {
        try {
            ClientInfo.DataResponse dataResponse = new ClientInfo().requestApi(ClientInfo.MethodType.GET, Constants.API_QUESTION_GET_COMMON_QUESTION);
            if (dataResponse.isOK()) {
                ServiceResponseEntity<JsonArray> result = dataResponse.getData(new TypeToken<ServiceResponseEntity<JsonArray>>() {
                }.getType());
                return result;
            }
        } catch (UnknownHostException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public BaseReturnFunctionEntity<JsonObject> getQuestionById(Context context, int id) {
        try {
            ClientInfo.DataResponse dataResponse = new ClientInfo().requestApi(ClientInfo.MethodType.GET, Constants.API_QUESTION_GET_BY_ID + "/" + id);
            if (dataResponse.isOK()) {
                ServiceResponseEntity<JsonObject> result = dataResponse.getData(new TypeToken<ServiceResponseEntity<JsonObject>>() {
                }.getType());
                if (result.getStatus() == 0) {
                    return new BaseReturnFunctionEntity<>(BaseReturnFunctionEntity.StatusFunction.TRUE, result.getData());
                }
            }
            return new BaseReturnFunctionEntity<>(BaseReturnFunctionEntity.StatusFunction.FALSE);
        } catch (UnknownHostException e) {
            e.printStackTrace();
            return new BaseReturnFunctionEntity<>(BaseReturnFunctionEntity.StatusFunction.EXCEPTION, e.getMessage());
        } catch (SocketTimeoutException e) {
            e.printStackTrace();
            return new BaseReturnFunctionEntity<>(BaseReturnFunctionEntity.StatusFunction.EXCEPTION, e.getMessage());
        }
    }

    public static BaseReturnFunctionEntity<Boolean> createQuestion(String type, String title, String content, int timeOut) {
        try {
            JsonObject jsonObject = new JsonObject();
            jsonObject.addProperty("Type", type);
            jsonObject.addProperty("Title", title);
            jsonObject.addProperty("Content", content);

            ClientInfo.DataResponse dataResponse = new ClientInfo().requestApi(ClientInfo.MethodType.POST, Constants.API_QUESTION_CREATE, jsonObject);
            if (dataResponse.isOK()) {
                ServiceResponseEntity<Object> result = dataResponse.getData(new TypeToken<ServiceResponseEntity<Object>>() {
                }.getType());
                if (result.getStatus() == 0)
                    return new BaseReturnFunctionEntity<>(BaseReturnFunctionEntity.StatusFunction.TRUE, true);

                return new BaseReturnFunctionEntity<>(BaseReturnFunctionEntity.StatusFunction.FALSE, result.Message);
            }
            return new BaseReturnFunctionEntity<>(BaseReturnFunctionEntity.StatusFunction.FALSE);
        } catch (UnknownHostException e) {
            e.printStackTrace();
            return new BaseReturnFunctionEntity<>(BaseReturnFunctionEntity.StatusFunction.EXCEPTION, e.getMessage());
        } catch (SocketTimeoutException e) {
            e.printStackTrace();
            return new BaseReturnFunctionEntity<>(BaseReturnFunctionEntity.StatusFunction.EXCEPTION, e.getMessage());
        }
    }

    public static boolean isExisted(String title, String content) throws SocketTimeoutException, UnknownHostException {
        try {
            ClientInfo.DataResponse response = new ClientInfo().requestApi(ClientInfo.MethodType.GET, Constants.API_QUESTION_IS_EXISTED + "?title=" + title + "?content=" + content);
            if (response.isOK()) {
                JsonObject jsonObject = response.getData(JsonObject.class);
                boolean result = ConvertUtils.toBoolean(jsonObject.get("IsTrue"));
                return result;
            }
            return true;
        } catch (UnknownHostException e) {
            e.printStackTrace();
            throw e;
        } catch (SocketTimeoutException e) {
            e.printStackTrace();
            throw e;
        } catch (Exception e) {
            String error = e.getMessage().toString();
            return true;
        }
    }
}
