//package com.team500ae.vncapplication.activities.job;
//
//import android.app.AlertDialog;
//import android.app.ProgressDialog;
//import android.content.Context;
//import android.content.DialogInterface;
//import android.content.Intent;
//import android.graphics.Bitmap;
//import android.net.Uri;
//import android.os.AsyncTask;
//import android.os.Build;
//import android.os.Bundle;
//import android.support.v4.app.FragmentActivity;
//import android.text.Html;
//import android.view.View;
//import android.widget.AdapterView;
//import android.widget.ArrayAdapter;
//import android.widget.EditText;
//import android.widget.ImageView;
//import android.widget.Spinner;
//import android.widget.TextView;
//import android.widget.Toast;
//
//import com.google.gson.JsonArray;
//import com.makeramen.roundedimageview.RoundedImageView;
//import com.team500ae.library.utils.AndroidPermissionUtils;
//import com.team500ae.library.utils.ConvertUtils;
//import com.team500ae.library.utils.ImageUtils;
//import com.team500ae.vncapplication.R;
//import com.team500ae.vncapplication.base.BaseActivity;
//import com.team500ae.vncapplication.constants.Constants;
//import com.team500ae.vncapplication.data_access.RealmInfo;
//import com.team500ae.vncapplication.data_access.job.CityJobInfo;
//import com.team500ae.vncapplication.data_access.job.CompanyJobInfo;
//import com.team500ae.vncapplication.data_access.job.CountryJobInfo;
//import com.team500ae.vncapplication.data_access.job.JobInfo;
//import com.team500ae.vncapplication.data_access.job.LevelJobInfo;
//import com.team500ae.vncapplication.data_access.job.TypeJobInfo;
//import com.team500ae.vncapplication.data_access.upload.BlobUploadProvider;
//import com.team500ae.vncapplication.data_access.upload.UploadInfo;
//import com.team500ae.vncapplication.data_access.user.UserInfo;
//import com.team500ae.vncapplication.models.BaseReturnFunctionEntity;
//import com.team500ae.vncapplication.models.ServiceResponseEntity;
//import com.team500ae.vncapplication.models.realm_models.job.CityJobEntity;
//import com.team500ae.vncapplication.models.realm_models.job.CompanyJobEntity;
//import com.team500ae.vncapplication.models.realm_models.job.CountryJobEntity;
//import com.team500ae.vncapplication.models.realm_models.job.LevelJobEntity;
//import com.team500ae.vncapplication.models.realm_models.job.TypeJobEntity;
//import com.team500ae.vncapplication.others.DialogListener;
//import com.team500ae.vncapplication.others.UploadImageDialog;
//import com.team500ae.vncapplication.utils.FileUtils;
//import com.team500ae.vncapplication.utils.KeyboardUtils;
//
//import java.util.ArrayList;
//
//import butterknife.BindView;
//import butterknife.ButterKnife;
//import butterknife.OnClick;
//import io.realm.Realm;
//import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;
//
//public class JobAddActivity extends BaseActivity {
//
//
//    @BindView(R.id.actJobAdd_tvCountry)
//    TextView tvCountry;
//    @BindView(R.id.actJobAdd_tvCity)
//    TextView tvCity;
//    @BindView(R.id.actJobAdd_tvType)
//    TextView tvType;
//    @BindView(R.id.actJobAdd_tvLevel)
//    TextView tvLevel;
//    @BindView(R.id.actJobAdd_tvSalary)
//    TextView tvSalary;
//    @BindView(R.id.actJobAdd_tvCompany)
//    TextView tvCompany;
//    @BindView(R.id.actJobAdd_tvPhone)
//    TextView tvPhone;
//    @BindView(R.id.actJobAdd_tvEmail)
//    TextView tvEmail;
//    private Bitmap bitmap;
//    private Uri selectedImage = Uri.EMPTY;
//    private String thumbnailUrl = "";
//    private int isUploadFile = 0;
//    private Uri tempUri;
//
//    public static void open(FragmentActivity activity) {
//        Intent intent = new Intent(activity, JobAddActivity.class);
//        activity.startActivity(intent);
//    }
//
//    Realm realm;
//
//    @BindView(R.id.imgThumbnail)
//    RoundedImageView imgThumbnail;
//    @BindView(R.id.txtTitle)
//    EditText txtTitle;
//    @BindView(R.id.txtCountry)
//    Spinner txtCountry;
//    @BindView(R.id.txtCity)
//    Spinner txtCity;
//    @BindView(R.id.txtType)
//    Spinner txtType;
//    @BindView(R.id.txtLevel)
//    Spinner txtLevel;
//    @BindView(R.id.txtSalary)
//    EditText txtSalary;
//    @BindView(R.id.txtCompany)
//    Spinner txtCompany;
//
//    @BindView(R.id.txtDescription)
//    EditText txtDescription;
//    @BindView(R.id.txtRequirement)
//    EditText txtRequirement;
//    @BindView(R.id.txtPhone)
//    EditText txtPhone;
//    @BindView(R.id.txtEmail)
//    EditText txtEmail;
//    @BindView(R.id.btnAdd)
//    ImageView btnAdd;
//
//    ServiceResponseEntity<JsonArray> countryList;
//    ArrayList<Integer> countryID;
//    ArrayList<String> countryName;
//
//    ServiceResponseEntity<JsonArray> cityList;
//    ArrayList<Integer> cityID;
//    ArrayList<String> cityName;
//
//
//    ServiceResponseEntity<JsonArray> typeList;
//    ArrayList<Integer> typeID;
//    ArrayList<String> typeName;
//
//    ServiceResponseEntity<JsonArray> levelList;
//    ArrayList<Integer> levelID;
//    ArrayList<String> levelName;
//
//    ServiceResponseEntity<JsonArray> companyList;
//    ArrayList<Integer> companyID;
//    ArrayList<String> companyName;
//
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        setContentView(R.layout.activity_job_add);
//        ButterKnife.bind(this);
//        KeyboardUtils.setupUI(getWindow().getDecorView().getRootView(), this);
//        super.onCreate(savedInstanceState);
//        realm = RealmInfo.getRealm(getApplicationContext());
//        showBackButton();
//        setHintField();
//
//       /* FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
//        fab.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
//                        .setAction("Action", null).show();
//            }
//        });*/
//        loadDataSpinner();
//    }
//
//    public void showBackButton() {
//        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
//        getSupportActionBar().setDisplayShowHomeEnabled(true);
//    }
//
//    private void setHintField() {
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
//            tvCountry.setText(Html.fromHtml(getString(R.string.job_search_country) + Constants.START_COLOR, Html.FROM_HTML_MODE_LEGACY));
//            tvCity.setText(Html.fromHtml(getString(R.string.job_search_city) + Constants.START_COLOR, Html.FROM_HTML_MODE_LEGACY));
//            tvType.setText(Html.fromHtml(getString(R.string.job_search_type) + Constants.START_COLOR, Html.FROM_HTML_MODE_LEGACY));
//            tvLevel.setText(Html.fromHtml(getString(R.string.job_search_level) + Constants.START_COLOR, Html.FROM_HTML_MODE_LEGACY));
//            tvSalary.setText(Html.fromHtml(getString(R.string.job_search_salary) + Constants.START_COLOR, Html.FROM_HTML_MODE_LEGACY));
//            tvCompany.setText(Html.fromHtml(getString(R.string.job_search_company) + Constants.START_COLOR, Html.FROM_HTML_MODE_LEGACY));
//            tvPhone.setText(Html.fromHtml(getString(R.string.job_search_phone) + Constants.START_COLOR, Html.FROM_HTML_MODE_LEGACY));
//            tvEmail.setText(Html.fromHtml(getString(R.string.job_detail_email) + Constants.START_COLOR, Html.FROM_HTML_MODE_LEGACY));
//            txtDescription.setHint(Html.fromHtml(getString(R.string.job_detail_description_hint) + Constants.START_COLOR, Html.FROM_HTML_MODE_LEGACY));
//            txtRequirement.setHint(Html.fromHtml(getString(R.string.job_detail_requirement_hint) + Constants.START_COLOR, Html.FROM_HTML_MODE_LEGACY));
//        } else {
//            tvCountry.setText(Html.fromHtml(getString(R.string.job_search_country) + Constants.START_COLOR));
//            tvCity.setText(Html.fromHtml(getString(R.string.job_search_country) + Constants.START_COLOR));
//            tvCountry.setText(Html.fromHtml(getString(R.string.job_search_city) + Constants.START_COLOR));
//            tvType.setText(Html.fromHtml(getString(R.string.job_search_type) + Constants.START_COLOR));
//            tvLevel.setText(Html.fromHtml(getString(R.string.job_search_level) + Constants.START_COLOR));
//            tvSalary.setText(Html.fromHtml(getString(R.string.job_search_salary) + Constants.START_COLOR));
//            tvCompany.setText(Html.fromHtml(getString(R.string.job_search_company) + Constants.START_COLOR));
//            tvPhone.setText(Html.fromHtml(getString(R.string.job_search_phone) + Constants.START_COLOR));
//            tvEmail.setText(Html.fromHtml(getString(R.string.job_detail_email) + Constants.START_COLOR));
//            txtDescription.setHint(Html.fromHtml(getString(R.string.job_detail_description_hint) + Constants.START_COLOR));
//            txtRequirement.setHint(Html.fromHtml(getString(R.string.job_detail_requirement_hint) + Constants.START_COLOR));
//        }
//    }
//
//    public void loadDataSpinner() {
//        try {
//            BaseReturnFunctionEntity<ArrayList<CompanyJobEntity>> myList = new CompanyJobInfo().getList(getApplicationContext(), realm);
//            companyID = new ArrayList<>();
//            companyName = new ArrayList<>();
//            if (myList.isTrue()) {
//                for (CompanyJobEntity type : myList.getData()) {
//                    companyID.add(type.getId());
//                    companyName.add(type.getName());
//                }
//            }
//            txtCompany.setAdapter(new ArrayAdapter<String>(getApplicationContext(), R.layout.item_spinner, companyName));
//        } catch (Exception ex) {
//        }
//
//        try {
//            BaseReturnFunctionEntity<ArrayList<LevelJobEntity>> myListLevel = new LevelJobInfo().getList(getApplicationContext(), realm);
//            levelName = new ArrayList<>();
//            levelID = new ArrayList<>();
//            if (myListLevel.isTrue()) {
//                //myList2.addAll(myList.getData());
//                for (LevelJobEntity type : myListLevel.getData()) {
//                    levelID.add(type.getId());
//                    levelName.add(type.getName());
//                }
//            }
//            txtLevel.setAdapter(new ArrayAdapter<String>(getApplicationContext(), R.layout.item_spinner, levelName));
//        } catch (Exception ex) {
//        }
//
//        try {
//            BaseReturnFunctionEntity<ArrayList<TypeJobEntity>> myList = new TypeJobInfo().getListTypeJob(getApplicationContext(), realm);
//            typeID = new ArrayList<>();
//            typeName = new ArrayList<>();
//            if (myList.isTrue()) {
//                for (TypeJobEntity type : myList.getData()) {
//                    typeID.add(type.getId());
//                    typeName.add(type.getName());
//                }
//            }
//            txtType.setAdapter(new ArrayAdapter<String>(getApplicationContext(), R.layout.item_spinner, typeName));
//        } catch (Exception ex) {
//        }
//
//        try {
//            BaseReturnFunctionEntity<ArrayList<CountryJobEntity>> myListCountry = new CountryJobInfo().getList(getApplicationContext(), realm);
//            countryName = new ArrayList<>();
//            countryID = new ArrayList<>();
//            if (myListCountry.isTrue()) {
//                for (CountryJobEntity type : myListCountry.getData()) {
//                    countryID.add(type.getId());
//                    countryName.add(type.getName());
//                }
//            }
//            txtCountry.setAdapter(new ArrayAdapter<String>(getApplicationContext(), R.layout.item_spinner, countryName));
//        } catch (Exception ex) {
//        }
//
//        txtCountry.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//            @Override
//            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
//
//
//                try {
//                    int cid = countryID.get(position);
//                    BaseReturnFunctionEntity<ArrayList<CityJobEntity>> myList = new CityJobInfo().getList(getApplicationContext(), realm);
//                    cityID = new ArrayList<>();
//                    cityName = new ArrayList<>();
//                    if (myList.isTrue()) {
//                        for (CityJobEntity type : myList.getData()) {
//                            if (type.getCountry() == cid) {
//                                cityID.add(type.getId());
//                                cityName.add(type.getName());
//                            }
//                        }
//                    }
//                    txtCity.setAdapter(new ArrayAdapter<String>(getApplicationContext(), R.layout.item_spinner, cityName));
//                } catch (Exception ex) {
//                }
//            }
//
//            @Override
//            public void onNothingSelected(AdapterView<?> parent) {
//
//            }
//        });
//    }
//
//    @OnClick(R.id.btnAdd)
//    void btnAdd() {
//        try {
//            btnAdd.setEnabled(false);
//            btnAdd.setImageDrawable(getDrawable(R.drawable.ic_job_save_disable));
//            String title = txtTitle.getText().toString().trim();
//
//            String salary = txtSalary.getText().toString();
//
//            String description = txtDescription.getText().toString().trim();
//            String requirement = txtRequirement.getText().toString().trim();
//            String phone = txtPhone.getText().toString().trim();
//            String email = txtEmail.getText().toString().trim();
//
//            if (title.equals("")
//                    || salary.isEmpty() || description.isEmpty() || requirement.isEmpty() || phone.isEmpty() || email.isEmpty()
//                    || txtCity.getSelectedItemPosition() < 0
//                    || txtType.getSelectedItemPosition() < 0
//                    || txtLevel.getSelectedItemPosition() < 0
//                    || txtCompany.getSelectedItemPosition() < 0
//                    || isUploadFile ==0
//                    ) {
//                Toast.makeText(getApplicationContext(), R.string.please_input_data, Toast.LENGTH_SHORT).show();
//                btnAdd.setEnabled(true);
//                btnAdd.setImageDrawable(getDrawable(R.drawable.ic_job_save));
//            } else {
//                int country = countryID.get(txtCountry.getSelectedItemPosition());
//                int city = cityID.get(txtCity.getSelectedItemPosition());
//                int type = typeID.get(txtType.getSelectedItemPosition());
//                int level = levelID.get(txtLevel.getSelectedItemPosition());
//                int company = companyID.get(txtCompany.getSelectedItemPosition());
//
//                if (isUploadFile == 2) {
//                    new UploadInfo(new BlobUploadProvider() {
//                    }, new UploadInfo.UploadListener() {
//                        //ProgressDialog dialogUpload = new ProgressDialog(getApplicationContext());
//                        ProgressDialog progDialog = new ProgressDialog(JobAddActivity.this);
//                        ProgressDialog dialogUpload;
//                        @Override
//                        public void beginUpload() {
//
//                            progDialog.setMessage(getString(R.string.loading));
//                            progDialog.setIndeterminate(false);
//                            progDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
//                            progDialog.setCancelable(true);
//                            progDialog.show();
//
//                            //start dialog
//                            //dialogUpload.setMessage("Loading..." + selectedImage.getPath());
//                            //dialogUpload.show();
//                            // dialogUpload = ProgressDialog.show(getApplicationContext(), "", "Loading...");
//                            //Toast.makeText(getApplicationContext(), selectedImage.getPath(), Toast.LENGTH_SHORT).show();
//                        }
//
//                        @Override
//                        public void endUpload() {
//                            if (progDialog.isShowing()) {
//                                progDialog.dismiss();
//                            }
//                        }
//
//                        @Override
//                        public void onSuccess(JsonArray response) {
//                            //thong bao upload thanh cong
//                            thumbnailUrl = response.getAsString();
//                            //Toast.makeText(getApplicationContext(), getString(R.string.Job_upload_image_successful) + thumbnailUrl, Toast.LENGTH_SHORT).show();
//                            //asyncTaskAddJob.execute();
//                            new AsyncTask<Void, Void, BaseReturnFunctionEntity<Boolean>>() {
//                                @Override
//                                protected BaseReturnFunctionEntity<Boolean> doInBackground(Void... voids) {
//                                    return new JobInfo().create(getApplicationContext(), title, salary, description,
//                                            requirement, phone, email, UserInfo.getCurrentUserId(), city,
//                                            level, type, "", thumbnailUrl);
//                                }
//
//                                @Override
//                                protected void onPostExecute(BaseReturnFunctionEntity<Boolean> result) {
//                                    super.onPostExecute(result);
//                                    if (result.isTrue()) {
//                                        Toast.makeText(getApplicationContext(), R.string.job_add_successful, Toast.LENGTH_SHORT).show();
//                                        Intent intent = new Intent(Constants.ACTION_JOB_ADD_SUCCESS);
//                                        getApplicationContext().sendBroadcast(intent);
//                                        onBackPressed();
//                                    }
//                                    else {
//                                        Toast.makeText(getApplicationContext(), R.string.job_add_failed + ":" + result.getMessage(), Toast.LENGTH_SHORT).show();
//                                        btnAdd.setEnabled(true);
//                                        btnAdd.setImageDrawable(getDrawable(R.drawable.ic_job_save));
//                                    }
//                                }
//                            }.execute();
//                        }
//
//                        @Override
//                        public void onError(ServiceResponseEntity<JsonArray> response) {
//                            //thong bao upload that bai cmnr
//                            thumbnailUrl = "";
//                            Toast.makeText(getApplicationContext(), getString(R.string.Job_upload_image_failed), Toast.LENGTH_SHORT).show();
//                            btnAdd.setEnabled(true);
//                            btnAdd.setImageDrawable(getDrawable(R.drawable.ic_job_save));
//                        }
//                    }).upload(getActivity(), Constants.UPLOAD_FILE, ConvertUtils.toArrayList(Uri.class, selectedImage));
//                }
//                else
//                    if(isUploadFile==1)
//                    {
//                       // asyncTaskAddJob.execute();
//                        new AsyncTask<Void, Void, BaseReturnFunctionEntity<Boolean>>() {
//                            @Override
//                            protected BaseReturnFunctionEntity<Boolean> doInBackground(Void... voids) {
//                                return new JobInfo().create(getApplicationContext(), title, salary, description,
//                                        requirement, phone, email, UserInfo.getCurrentUserId(), city,
//                                        level, type, "", thumbnailUrl);
//                            }
//
//                            @Override
//                            protected void onPostExecute(BaseReturnFunctionEntity<Boolean> result) {
//                                super.onPostExecute(result);
//                                if (result.isTrue()) {
//                                    Toast.makeText(getApplicationContext(), R.string.job_add_successful, Toast.LENGTH_SHORT).show();
//                                    Intent intent = new Intent(Constants.ACTION_JOB_ADD_SUCCESS);
//                                    getApplicationContext().sendBroadcast(intent);
//                                    onBackPressed();
//                                }
//                                else {
//                                    Toast.makeText(getApplicationContext(), R.string.job_add_failed + ":" + result.getMessage(), Toast.LENGTH_SHORT).show();
//                                    btnAdd.setEnabled(true);
//                                    btnAdd.setImageDrawable(getDrawable(R.drawable.ic_job_save));
//                                }
//                            }
//                        }.execute();
//                    }
//            }
//        } catch (Exception e) {
//            Toast.makeText(getApplicationContext(), R.string.please_input_data, Toast.LENGTH_SHORT).show();
//            btnAdd.setEnabled(true);
//            btnAdd.setImageDrawable(getDrawable(R.drawable.ic_job_save));
//        }
//
//    }
//    @OnClick(R.id.btnClear)
//    void btnClear(){
//        AlertDialog.Builder alert = new AlertDialog.Builder(JobAddActivity.this);
//        alert.setTitle(getString(R.string.job_dialog_notification))
//        .setMessage(getString(R.string.job_dialog_clear))
//        .setPositiveButton("No", new DialogInterface.OnClickListener() {
//            @Override
//            public void onClick(DialogInterface dialog, int which) {
//                dialog.dismiss();
//            }
//        })
//        .setNegativeButton("Yes", new DialogInterface.OnClickListener() {
//            @Override
//            public void onClick(DialogInterface dialog, int which) {
//                txtTitle.setText("");
//                txtCountry.setSelection(0);
//                txtCity.setSelection(0);
//                txtType.setSelection(0);
//                txtLevel.setSelection(0);
//                txtSalary.setText("");
//                txtCompany.setSelection(0);
//                txtDescription.setText("");
//                txtRequirement.setText("");
//                txtPhone.setText("");
//                txtEmail.setText("");
//            }
//        })
//                .setIcon(android.R.drawable.ic_dialog_alert)
//        .show();
//    }
//    @Override
//    protected void attachBaseContext(Context newBase) {
//        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
//    }
//
//    @OnClick(R.id.imgThumbnail)
//    public void onViewClicked(View view) {
//        switch (view.getId()) {
//            case R.id.imgThumbnail:
//                requestPermission(new AndroidPermissionUtils.OnCallbackRequestPermission() {
//                    @Override
//                    public void onSuccess() {
//                        UploadImageDialog uploadImageDialog = new UploadImageDialog(getActivity());
//                        uploadImageDialog.setOnItemClickListener(new DialogListener() {
//                            @Override
//                            public void onConfirmClicked() {
//                                thumbnailUrl = uploadImageDialog.getEditText();
//                                //Toast.makeText(JobAddActivity.this, uploadImageDialog.getEditText(), Toast.LENGTH_SHORT).show();
//                                ImageUtils.loadImageByGlide(getApplicationContext(), true, 200, 200, thumbnailUrl, R.drawable.img_no_image, R.drawable.img_no_image, imgThumbnail, true);
//                                isUploadFile = 1;
//                                btnAdd.setEnabled(true);
//                                btnAdd.setImageDrawable(getDrawable(R.drawable.ic_job_save));
//                            }
//
//                            @Override
//                            public void onButtonChooseImage() {
//                                tempUri = FileUtils.createTempFile(getActivity());
//                                Intent chooseImageIntent = new FileUtils().selectOrTakePicture(getActivity(), tempUri);
//                                startActivityForResult(chooseImageIntent, 1);
//                            }
//                        });
//                        uploadImageDialog.show();
//                    }
//
//                    @Override
//                    public void onFailed() {
//
//                    }
//                }, AndroidPermissionUtils.TypePermission.PERMISSION_READ_EXTERNAL_STORAGE, AndroidPermissionUtils.TypePermission.PERMISSION_WRIRE_EXTERNAL_STORAGE);
//
//
//                break;
//        }
//    }
//
//    @Override
//    public void onActivityResult(int requestCode, int resultCode, Intent data) {
//        switch (requestCode) {
//            case 1:
//                bitmap = FileUtils.getImageFromResult(getActivity(), resultCode, data, tempUri);
//                if (bitmap != null)
//                    imgThumbnail.setImageBitmap(bitmap);
//                selectedImage = FileUtils.getSelectedImageFromResult(getActivity(), resultCode, data, tempUri);
//                isUploadFile = 2;
//                btnAdd.setEnabled(true);
//                btnAdd.setImageDrawable(getDrawable(R.drawable.ic_job_save));
//                break;
//            default:
//                super.onActivityResult(requestCode, resultCode, data);
//                break;
//        }
//    }
//}
