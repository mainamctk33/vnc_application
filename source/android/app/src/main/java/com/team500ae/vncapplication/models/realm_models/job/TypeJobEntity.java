package com.team500ae.vncapplication.models.realm_models.job;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.Required;

/**
 * Created by SVIP on 11/30/2017.
 */

public class TypeJobEntity extends RealmObject {
    @PrimaryKey
    int id;

    int position;

    @Required
    String name;

    String name2;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name != null ? name : "";
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public String getName2() {
        return name2 != null ? name2 : "";
    }

    public void setName2(String name2) {
        this.name2 = name2;
    }
}
