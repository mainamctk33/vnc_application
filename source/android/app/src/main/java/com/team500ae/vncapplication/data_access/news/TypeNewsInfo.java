package com.team500ae.vncapplication.data_access.news;

import android.content.Context;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.team500ae.library.utils.ConvertUtils;
import com.team500ae.vncapplication.R;
import com.team500ae.vncapplication.constants.Constants;
import com.team500ae.vncapplication.data_access.ClientInfo;
import com.team500ae.vncapplication.models.BaseReturnFunctionEntity;
import com.team500ae.vncapplication.models.ServiceResponseEntity;
import com.team500ae.vncapplication.models.realm_models.news.MyTypeNewsEntity;
import com.team500ae.vncapplication.models.realm_models.news.TypeNewsEntity;

import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmResults;
import io.realm.exceptions.RealmError;

/**
 * Created by MaiNam on 11/15/2017.
 */

public class TypeNewsInfo {
    public ServiceResponseEntity<JsonArray> syncTypeNews(Context context, long lastSyncDate) {
        try {
            ClientInfo.DataResponse dataResponse = new ClientInfo().requestApi(ClientInfo.MethodType.GET, Constants.API_TYPE_NEWS_SYNC + "/" + lastSyncDate);
            if (dataResponse.isOK()) {
                ServiceResponseEntity<JsonArray> result = dataResponse.getData(new TypeToken<ServiceResponseEntity<JsonArray>>() {
                }.getType());
                return result;
            }
        } catch (UnknownHostException e) {
            e.printStackTrace();
        } catch (SocketTimeoutException e) {
            e.printStackTrace();
        }
        return null;
    }

    public BaseReturnFunctionEntity<Boolean> removeTypeNewsFromListFavorite(Context context, Realm realm, String userId, int... listTypeNewsIds) {
        if (listTypeNewsIds == null || listTypeNewsIds.length == 0)
            return new BaseReturnFunctionEntity<>(BaseReturnFunctionEntity.StatusFunction.FALSE, context.getString(R.string.select_typenews_to_remove));
        try {
            realm.executeTransaction(realm1 -> {
                for (int id : listTypeNewsIds) {
                    MyTypeNewsEntity myTypeNewsEntity = realm.where(MyTypeNewsEntity.class).equalTo("id", userId + id).findFirst();
                    if (myTypeNewsEntity != null)
                        myTypeNewsEntity.deleteFromRealm();
                }
            });
            return new BaseReturnFunctionEntity<>(BaseReturnFunctionEntity.StatusFunction.TRUE, true);
        } catch (RealmError e) {
            return new BaseReturnFunctionEntity<>(BaseReturnFunctionEntity.StatusFunction.EXCEPTION, e.getMessage());
        } catch (Exception e) {
            return new BaseReturnFunctionEntity<>(BaseReturnFunctionEntity.StatusFunction.EXCEPTION, e.getMessage());
        } catch (OutOfMemoryError e) {
            return new BaseReturnFunctionEntity<>(BaseReturnFunctionEntity.StatusFunction.EXCEPTION, e.getMessage());
        }
    }


    public BaseReturnFunctionEntity<Boolean> saveTypeNewsToFavorite(Context context, Realm realm, String userId, int... listTypeNewsIds) {
        if (listTypeNewsIds == null || listTypeNewsIds.length == 0)
            return new BaseReturnFunctionEntity<>(BaseReturnFunctionEntity.StatusFunction.FALSE, context.getString(R.string.select_typenews_to_set_favorite));
        try {
            realm.executeTransaction(realm1 -> {
                for (int id : listTypeNewsIds) {
                    TypeNewsEntity typeNewsEntity = realm.where(TypeNewsEntity.class).equalTo("id", id).findFirst();
                    if (typeNewsEntity != null) {
                        MyTypeNewsEntity myTypeNewsEntity = new MyTypeNewsEntity();
                        myTypeNewsEntity.setId(userId + id);
                        myTypeNewsEntity.setTypeNews(typeNewsEntity);
                        myTypeNewsEntity.setUserId(userId);
                        realm.copyToRealmOrUpdate(myTypeNewsEntity);
                    }
                }
            });
            return new BaseReturnFunctionEntity<>(BaseReturnFunctionEntity.StatusFunction.TRUE, true);
        } catch (RealmError e) {
            return new BaseReturnFunctionEntity<>(BaseReturnFunctionEntity.StatusFunction.EXCEPTION, e.getMessage());
        } catch (Exception e) {
            return new BaseReturnFunctionEntity<>(BaseReturnFunctionEntity.StatusFunction.EXCEPTION, e.getMessage());
        } catch (OutOfMemoryError e) {
            return new BaseReturnFunctionEntity<>(BaseReturnFunctionEntity.StatusFunction.EXCEPTION, e.getMessage());
        }
    }

    public BaseReturnFunctionEntity<Boolean> updateTypeNewToFavorite(Context context, Realm realm, String userId, ArrayList<TypeNewsEntity> listTypeNewIds) {
        if (listTypeNewIds == null || listTypeNewIds.size() == 0)
            return new BaseReturnFunctionEntity<>(BaseReturnFunctionEntity.StatusFunction.FALSE, context.getString(R.string.select_typenews_to_set_favorite));
        try {
            realm.executeTransaction(realm1 -> {
                for (int i = 0; i < listTypeNewIds.size(); i++) {
                    TypeNewsEntity typeNew = listTypeNewIds.get(i);
                    TypeNewsEntity typeNewEntity = realm.where(TypeNewsEntity.class).equalTo("id", typeNew.getId()).findFirst();
                    if (typeNewEntity != null) {
                        MyTypeNewsEntity myTypeJobEntity = new MyTypeNewsEntity();
                        myTypeJobEntity.setId(userId + typeNew.getId());
                        myTypeJobEntity.setTypeNews(typeNewEntity);
                        myTypeJobEntity.setUserId(userId);
                        myTypeJobEntity.setPosition(i);
                        realm.copyToRealmOrUpdate(myTypeJobEntity);
                    }
                }
            });
            return new BaseReturnFunctionEntity<>(BaseReturnFunctionEntity.StatusFunction.TRUE, true);
        } catch (RealmError e) {
            return new BaseReturnFunctionEntity<>(BaseReturnFunctionEntity.StatusFunction.EXCEPTION, e.getMessage());
        } catch (Exception e) {
            return new BaseReturnFunctionEntity<>(BaseReturnFunctionEntity.StatusFunction.EXCEPTION, e.getMessage());
        } catch (OutOfMemoryError e) {
            return new BaseReturnFunctionEntity<>(BaseReturnFunctionEntity.StatusFunction.EXCEPTION, e.getMessage());
        }
    }

    public BaseReturnFunctionEntity<ArrayList<TypeNewsEntity>> getListTypeNews(Context context, Realm realm) {
        try {
            RealmResults<TypeNewsEntity> realmList = realm.where(TypeNewsEntity.class).findAll();
            ArrayList<TypeNewsEntity> result = new ArrayList<>();
            result.addAll(realmList);
            return new BaseReturnFunctionEntity<>(BaseReturnFunctionEntity.StatusFunction.TRUE, result);
        } catch (RealmError e) {
            return new BaseReturnFunctionEntity<>(BaseReturnFunctionEntity.StatusFunction.EXCEPTION, e.getMessage());
        } catch (Exception e) {
            return new BaseReturnFunctionEntity<>(BaseReturnFunctionEntity.StatusFunction.EXCEPTION, e.getMessage());
        } catch (OutOfMemoryError e) {
            return new BaseReturnFunctionEntity<>(BaseReturnFunctionEntity.StatusFunction.EXCEPTION, e.getMessage());
        }
    }

    public BaseReturnFunctionEntity<ArrayList<TypeNewsEntity>> getTypeNewsHasSelected(Context context, Realm realm, String userId) {
        try {
            RealmResults<MyTypeNewsEntity> realmList = realm.where(MyTypeNewsEntity.class).equalTo("userId", userId).findAll();
            ArrayList<TypeNewsEntity> result = new ArrayList<>();
            for (MyTypeNewsEntity typeNewsEntity : realmList.sort("position")) {
                if (typeNewsEntity.getTypeNews() != null && typeNewsEntity.getTypeNews().getName() != null && !typeNewsEntity.getTypeNews().getName().isEmpty())
                    result.add(typeNewsEntity.getTypeNews());
            }
            return new BaseReturnFunctionEntity<>(BaseReturnFunctionEntity.StatusFunction.TRUE, result);
        } catch (RealmError e) {
            return new BaseReturnFunctionEntity<>(BaseReturnFunctionEntity.StatusFunction.EXCEPTION, e.getMessage());
        } catch (Exception e) {
            return new BaseReturnFunctionEntity<>(BaseReturnFunctionEntity.StatusFunction.EXCEPTION, e.getMessage());
        } catch (OutOfMemoryError e) {
            return new BaseReturnFunctionEntity<>(BaseReturnFunctionEntity.StatusFunction.EXCEPTION, e.getMessage());
        }
    }

    public BaseReturnFunctionEntity<Boolean> updateTypeNews(Realm realm, JsonArray data) {
        try {

            RealmResults<TypeNewsEntity> realmList = realm.where(TypeNewsEntity.class).findAll();
            List<Integer> listID = new ArrayList<>();
            realm.executeTransaction(realm1 -> {
                for (int i = 0; i < data.size(); i++) {
                    JsonObject jsonObject = ConvertUtils.toJsonObject(data.get(i));
                    int idData = ConvertUtils.toInt(jsonObject.get("ID"));
                    listID.add(idData);
                    updateTypeNews(realm, jsonObject, false);
                }
            });

            for (int j = 0; j < realmList.size(); j++) {
                int idRealm = realmList.get(j).getId();
                if (!listID.contains(idRealm)) {
                    realm.executeTransaction(new Realm.Transaction() {
                        @Override
                        public void execute(Realm realm) {
                            RealmResults<TypeNewsEntity> result = realm.where(TypeNewsEntity.class).equalTo("id", idRealm).findAll();
                            result.deleteAllFromRealm();
                        }
                    });

                }
            }
//            realm.executeTransaction(realm1 -> {
//                for (int i = 0; i < data.size(); i++) {
//                    JsonObject jsonObject = ConvertUtils.toJsonObject(data.get(i));
//                    updateTypeNews(realm, jsonObject, false);
//                }
//            });
            return new BaseReturnFunctionEntity<>(BaseReturnFunctionEntity.StatusFunction.TRUE, true);
        } catch (RealmError e) {
            return new BaseReturnFunctionEntity<>(BaseReturnFunctionEntity.StatusFunction.EXCEPTION, e.getMessage());
        } catch (Exception e) {
            return new BaseReturnFunctionEntity<>(BaseReturnFunctionEntity.StatusFunction.EXCEPTION, e.getMessage());
        } catch (OutOfMemoryError e) {
            return new BaseReturnFunctionEntity<>(BaseReturnFunctionEntity.StatusFunction.EXCEPTION, e.getMessage());
        }
    }

    public BaseReturnFunctionEntity<Boolean> updateTypeNews(Realm realm, final JsonObject data, boolean needTransacsion) {
        try {
            if (needTransacsion) {
                realm.executeTransaction(realm1 -> {
                    updateTypeNews(realm, data, false);
                });
            } else {
                int id = ConvertUtils.toInt(data.get("ID"));
                TypeNewsEntity typeNewsEntity = new TypeNewsEntity();
                typeNewsEntity.setName(ConvertUtils.toString(data.get("Name")));
                typeNewsEntity.setName2(ConvertUtils.toString(data.get("Name2")));
                typeNewsEntity.setId(id);
                realm.copyToRealmOrUpdate(typeNewsEntity);
            }
            return new BaseReturnFunctionEntity<>(BaseReturnFunctionEntity.StatusFunction.EXCEPTION, true);
        } catch (RealmError e) {
            return new BaseReturnFunctionEntity<>(BaseReturnFunctionEntity.StatusFunction.EXCEPTION, e.getMessage());
        } catch (Exception e) {
            return new BaseReturnFunctionEntity<>(BaseReturnFunctionEntity.StatusFunction.EXCEPTION, e.getMessage());
        } catch (OutOfMemoryError e) {
            return new BaseReturnFunctionEntity<>(BaseReturnFunctionEntity.StatusFunction.EXCEPTION, e.getMessage());
        }
    }

}
