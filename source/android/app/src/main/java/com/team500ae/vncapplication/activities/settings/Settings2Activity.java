package com.team500ae.vncapplication.activities.settings;

import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.team500ae.vncapplication.R;
import com.team500ae.vncapplication.activities.MainActivity;
import com.team500ae.vncapplication.activities.login.EditAccountActivity;
import com.team500ae.vncapplication.activities.translator.fragments.TranslatorFragment;
import com.team500ae.vncapplication.base.BaseActivity;
import com.team500ae.vncapplication.data_access.user.UserInfo;
import com.team500ae.vncapplication.utils.LanguageUtils;

import butterknife.BindView;
import butterknife.OnClick;

public class Settings2Activity extends BaseActivity {

    @BindView(R.id.layout_account)
    View vAccountLayout;
    @BindView(R.id.layout_notification)
    View vNotificationLayout;
    @BindView(R.id.layout_media)
    View vMediaLayout;
    @BindView(R.id.layout_privacy)
    View vPrivacyLayout;
    @BindView(R.id.layout_language)
    View vLanguageLayout;
    @BindView(R.id.layout_info)
    View vInfoLayout;

    public static void open(Context activity) {
        Intent intent = new Intent(activity, Settings2Activity.class);
        activity.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setContentView(R.layout.activity_settings2);
        super.onCreate(savedInstanceState);

        showBackButton();

        initLayout();
    }

    private void initLayout() {

//        vAccountLayout = findViewById(R.id.layout_account);
        ImageView ivAccountLeftIcon = (ImageView) vAccountLayout.findViewById(R.id.iv_left_icon);
        ivAccountLeftIcon.setImageResource(R.drawable.ic_account);
        TextView tvAccountTitle = (TextView) vAccountLayout.findViewById(R.id.tv_title);
        tvAccountTitle.setText(getResources().getString(R.string.pref_header_account));
        TextView tvAccountSubTitle = (TextView) vAccountLayout.findViewById(R.id.tv_sub_title);
        tvAccountSubTitle.setText(getResources().getString(R.string.settings_account_sub_title));
//        tvAccountSubTitle.setVisibility(View.INVISIBLE);

//        vNotificationLayout = findViewById(R.id.layout_notification);

        ImageView ivNotificationLeftIcon = (ImageView) vNotificationLayout.findViewById(R.id.iv_left_icon);
        ivNotificationLeftIcon.setImageResource(R.drawable.ic_notification);
        TextView tvNotificationTitle = (TextView) vNotificationLayout.findViewById(R.id.tv_title);
        tvNotificationTitle.setText(getResources().getString(R.string.pref_header_notifications));
        TextView tvNotificationSubTitle = (TextView) vNotificationLayout.findViewById(R.id.tv_sub_title);
        tvNotificationSubTitle.setText(getResources().getString(R.string.settings_notif_sub_title));
//        tvNotificationSubTitle.setVisibility(View.INVISIBLE);

//        vMediaLayout = findViewById(R.id.layout_media);

        ImageView ivMediaLeftIcon = (ImageView) vMediaLayout.findViewById(R.id.iv_left_icon);
        ivMediaLeftIcon.setImageResource(R.drawable.ic_multimedia);
        TextView tvMediaTitle = (TextView) vMediaLayout.findViewById(R.id.tv_title);
        tvMediaTitle.setText(getResources().getString(R.string.pref_header_media));
        TextView tvMediaSubTitle = (TextView) vMediaLayout.findViewById(R.id.tv_sub_title);
        tvMediaSubTitle.setText(getResources().getString(R.string.settings_media_sub_title));
//        tvMediaSubTitle.setVisibility(View.INVISIBLE);

//        vPrivacyLayout = findViewById(R.id.layout_privacy);

        ImageView ivPrivacyLeftIcon = (ImageView) vPrivacyLayout.findViewById(R.id.iv_left_icon);
        ivPrivacyLeftIcon.setImageResource(R.drawable.ic_privacy);
        TextView tvPrivacyTitle = (TextView) vPrivacyLayout.findViewById(R.id.tv_title);
        tvPrivacyTitle.setText(getResources().getString(R.string.pref_header_privacy));
        TextView tvPrivacySubTitle = (TextView) vPrivacyLayout.findViewById(R.id.tv_sub_title);
        tvPrivacySubTitle.setText(getResources().getString(R.string.settings_privacy_sub_title));
//        tvPrivacySubTitle.setVisibility(View.INVISIBLE);

//        vLanguageLayout = findViewById(R.id.layout_language);

        ImageView ivLanguageLeftIcon = (ImageView) vLanguageLayout.findViewById(R.id.iv_left_icon);
        ivLanguageLeftIcon.setImageResource(R.drawable.ic_language);
        TextView tvLanguageTitle = (TextView) vLanguageLayout.findViewById(R.id.tv_title);
        tvLanguageTitle.setText(getResources().getString(R.string.pref_header_language));
        TextView tvLanguageSubTitle = (TextView) vLanguageLayout.findViewById(R.id.tv_sub_title);
        tvLanguageSubTitle.setText(getResources().getString(R.string.settings_language_sub_title));
//        tvLanguageSubTitle.setVisibility(View.INVISIBLE);

//        vInfoLayout = findViewById(R.id.layout_info);

        ImageView ivInfoLeftIcon = (ImageView) vInfoLayout.findViewById(R.id.iv_left_icon);
        ivInfoLeftIcon.setImageResource(R.drawable.ic_info);
        TextView tvInfoTitle = (TextView) vInfoLayout.findViewById(R.id.tv_title);
        tvInfoTitle.setText(getResources().getString(R.string.pref_header_info));
        TextView tvInfoSubTitle = (TextView) vInfoLayout.findViewById(R.id.tv_sub_title);
        tvInfoSubTitle.setText(getResources().getString(R.string.settings_info_sub_title));
//        tvInfoSubTitle.setVisibility(View.INVISIBLE);
    }

    @Override
    public void onBackPressed() {
//        detailSettingsLayout.setVisibility(View.GONE);
//        settingsLayout.setVisibility(View.VISIBLE);
        super.onBackPressed();
    }

    @OnClick(R.id.layout_account)
    void accountSettings() {
        if (!UserInfo.isLogin()) {
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            builder.setTitle(getResources().getString(R.string.notice));
            builder.setMessage(getResources().getString(R.string.msg_have_not_login));
            builder.setIcon(R.drawable.ic_message);
            builder.setPositiveButton(getString(android.R.string.ok),
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            // do the work to update the preference ---
                        }
                    });
            builder.create().show();
        } else {
            AccountSettingsActivity.open(getActivity());
        }
    }

    @OnClick(R.id.layout_notification)
    void notificationSettings() {
        if (!UserInfo.isLogin()) {
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            builder.setTitle(getResources().getString(R.string.notice));
            builder.setMessage(getResources().getString(R.string.msg_have_not_login));
            builder.setIcon(R.drawable.ic_message);
            builder.setPositiveButton(getString(android.R.string.ok),
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            // do the work to update the preference ---
                        }
                    });
            builder.create().show();
        } else {
            NotificationSettingsActivity.open(getActivity());
        }
    }

    @OnClick(R.id.layout_media)
    void mediaSettings() {
        if (!UserInfo.isLogin()) {
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            builder.setTitle(getResources().getString(R.string.notice));
            builder.setMessage(getResources().getString(R.string.msg_have_not_login));
            builder.setIcon(R.drawable.ic_message);
            builder.setPositiveButton(getString(android.R.string.ok),
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            // do the work to update the preference ---
                        }
                    });
            builder.create().show();
        } else {
            MediaSettingsActivity.open(getActivity());
        }
//        showSnackBar("This function is under development");
//        detailSettingsLayout.setVisibility(View.VISIBLE);
//        getSupportActionBar().setTitle(R.string.pref_header_media);
//        getFragmentManager().beginTransaction().replace(R.id.detail_settings_layout, new SettingsActivity.MediaPreferenceFragment()).commit();
//        settingsLayout.setVisibility(View.GONE);
    }

    @OnClick(R.id.layout_privacy)
    void privacySettings() {
        if (!UserInfo.isLogin()) {
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            builder.setTitle(getResources().getString(R.string.notice));
            builder.setMessage(getResources().getString(R.string.msg_have_not_login));
            builder.setIcon(R.drawable.ic_message);
            builder.setPositiveButton(getString(android.R.string.ok),
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            // do the work to update the preference ---
                        }
                    });
            builder.create().show();
        } else {
            PrivacySettingsActivity.open(getActivity());
        }
    }

    @OnClick(R.id.layout_info)
    void infoSettings() {
        InfoSettingsActivity.open(getActivity());
//        detailSettingsLayout.setVisibility(View.VISIBLE);
//        getSupportActionBar().setTitle(R.string.pref_header_info);
//        getFragmentManager().beginTransaction().replace(R.id.detail_settings_layout, new SettingsActivity.InfoPreferenceFragment()).commit();
//        settingsLayout.setVisibility(View.GONE);
    }

    @OnClick(R.id.layout_language)
    void languageSettings() {
        AlertDialog.Builder builderSingle = new AlertDialog.Builder(Settings2Activity.this);
        builderSingle.setIcon(R.drawable.ic_language);
        builderSingle.setTitle(getString(R.string.select_language_title));

        final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(Settings2Activity.this, android.R.layout.select_dialog_singlechoice);
        arrayAdapter.add("English");//order number = 0
        arrayAdapter.add("Tiếng Việt");//order number = 1

        String currentLanguage = LanguageUtils.getAppLanguageSetting(getActivity());
//        int selectedLanguage = 0;
        int selectedLanguage = 1;
        if (currentLanguage.equals("vi")) {
            selectedLanguage = 1;
            selectedLanguage = 0;
        }

        builderSingle.setSingleChoiceItems(arrayAdapter, selectedLanguage, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String language = arrayAdapter.getItem(which);
                if (language == null) return;

                //value of strings.xml in 'values folder' and 'values-vi' has been changed to each other because customer want to load Vietnamese as default language
                if (language.toLowerCase().equals("english")) {
                    LanguageUtils.changeLanguage(getActivity(), "vi");
//                    LanguageUtils.changeLanguage(getActivity(), "en");
                } else {
                    LanguageUtils.changeLanguage(getActivity(), "en");
//                    LanguageUtils.changeLanguage(getActivity(), "vi");
                }
                dialog.dismiss();

                AlertDialog.Builder builderInner = new AlertDialog.Builder(Settings2Activity.this);
                builderInner.setMessage(getString(R.string.select_language_notify_after_selected));
                builderInner.setTitle(getString(R.string.select_language_notify_title));
                builderInner.setPositiveButton(R.string.select_language_ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
//                            Intent i = getBaseContext().getPackageManager().getLaunchIntentForPackage(getBaseContext().getPackageName());
//                            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//                            startActivity(i);

                        Intent mStartActivity = new Intent(getBaseContext(), MainActivity.class);
                        int mPendingIntentId = 123456;
                        PendingIntent mPendingIntent = PendingIntent.getActivity(getBaseContext(), mPendingIntentId, mStartActivity, PendingIntent.FLAG_CANCEL_CURRENT);
                        AlarmManager mgr = (AlarmManager) getBaseContext().getSystemService(Context.ALARM_SERVICE);
                        mgr.set(AlarmManager.RTC, System.currentTimeMillis() + 100, mPendingIntent);
                        System.exit(0);
                    }
                });
                builderInner.show();
            }
        });

        builderSingle.show();
    }


}
