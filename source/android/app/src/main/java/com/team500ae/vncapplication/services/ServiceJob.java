package com.team500ae.vncapplication.services;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;

import com.google.gson.JsonArray;
import com.team500ae.library.utils.SharedPreferencesUtil;
import com.team500ae.vncapplication.constants.Constants;
import com.team500ae.vncapplication.data_access.BookmarkInfo;
import com.team500ae.vncapplication.data_access.RealmInfo;
import com.team500ae.vncapplication.data_access.job.CityJobInfo;
import com.team500ae.vncapplication.data_access.job.CompanyJobInfo;
import com.team500ae.vncapplication.data_access.job.CountryJobInfo;
import com.team500ae.vncapplication.data_access.job.JobInfo;
import com.team500ae.vncapplication.data_access.job.LevelJobInfo;
import com.team500ae.vncapplication.data_access.job.SalaryJobInfo;
import com.team500ae.vncapplication.data_access.job.TypeJobInfo;
import com.team500ae.vncapplication.data_access.user.UserInfo;
import com.team500ae.vncapplication.models.BaseReturnFunctionEntity;
import com.team500ae.vncapplication.models.ServiceResponseEntity;
import com.team500ae.vncapplication.models.realm_models.job.TypeJobEntity;

import java.util.ArrayList;
import java.util.Date;

import io.realm.Realm;
import io.realm.exceptions.RealmError;

/**
 * Created by SVIP on 11/30/2017.
 */

public class ServiceJob extends IntentService {
    // TODO: Rename actions, choose action names that describe tasks that this
    // IntentService can perform, e.g. ACTION_FETCH_NEW_ITEMS
    private static final String ACTION_SYNC_TAG = "com.team500ae.vncapplication.services.action.ACTION_SYNC_TAG";
    private static final String ACTION_SYNC_TYPE = "com.team500ae.vncapplication.services.action.ACTION_SYNC_TYPE_JOB";
    private static final String ACTION_SYNC_COMPANY = "com.team500ae.vncapplication.services.action.ACTION_SYNC_COMPANY_JOB";
    private static final String ACTION_SYNC_LEVEL = "com.team500ae.vncapplication.services.action.ACTION_SYNC_LEVEL_JOB";
    private static final String ACTION_SYNC_SALARY = "com.team500ae.vncapplication.services.action.ACTION_SYNC_SALARY_JOB";
    private static final String ACTION_SYNC_COUNTRY = "com.team500ae.vncapplication.services.action.ACTION_SYNC_COUNTRY_JOB";
    private static final String ACTION_SYNC_CITY = "com.team500ae.vncapplication.services.action.ACTION_SYNC_CITY_JOB";
    private static final String ACTION_BOOKMARK = "com.team500ae.vncapplication.services.action.ACTION_JOB_BOOKMARK";
    private static final String ACTION_SET_DEFAULT_CATEGORY = "com.team500ae.vncapplication.services.action.ACTION_SET_DEFAULT_CATEGORY";

    public ServiceJob() {
        super("ServiceJob");
    }

    /**
     * Starts this service to perform action Foo with the given parameters. If
     * the service is already performing a task this action will be queued.
     *
     * @see IntentService
     */
    // TODO: Customize helper method
    public static void startActionSyncType(Context context) {
        Intent intent = new Intent(context, ServiceJob.class);
        intent.setAction(ACTION_SYNC_TYPE);
        context.startService(intent);
    }
    public static void startActionSyncCompany(Context context) {
        Intent intent = new Intent(context, ServiceJob.class);
        intent.setAction(ACTION_SYNC_COMPANY);
        context.startService(intent);
    }
    public static void startActionSyncSalary(Context context) {
        Intent intent = new Intent(context, ServiceJob.class);
        intent.setAction(ACTION_SYNC_SALARY);
        context.startService(intent);
    }
    public static void startActionSyncLevel(Context context) {
        Intent intent = new Intent(context, ServiceJob.class);
        intent.setAction(ACTION_SYNC_LEVEL);
        context.startService(intent);
    }
    public static void startActionSyncCountry(Context context) {
        Intent intent = new Intent(context, ServiceJob.class);
        intent.setAction(ACTION_SYNC_COUNTRY);
        context.startService(intent);
    }
    public static void startActionSyncCity(Context context) {
        Intent intent = new Intent(context, ServiceJob.class);
        intent.setAction(ACTION_SYNC_CITY);
        context.startService(intent);
    }

    public static void startActionBookmark(Context context, int id, boolean bookmark) {
        Intent intent = new Intent(context, ServiceJob.class);
        intent.putExtra("ID", id);
        intent.putExtra("BOOKMARK", bookmark);
        intent.setAction(ACTION_BOOKMARK);
        context.startService(intent);
    }


    public static void startSetDefaultCategory(Context context ){
        Intent intent = new Intent(context, ServiceJob.class);
        intent.setAction(ACTION_SET_DEFAULT_CATEGORY);
        context.startService(intent);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        if (intent != null) {
            final String action = intent.getAction();
            switch (action) {
                case ACTION_SYNC_TYPE:
                    handleActionSyncType();
                    break;
                case ACTION_SYNC_COMPANY:
                    handleActionSyncCompany();
                    break;
                case ACTION_SYNC_SALARY:
                    handleActionSyncSalary();
                    break;
                case ACTION_SYNC_LEVEL:
                    handleActionSyncLevel();
                    break;
                case ACTION_SYNC_COUNTRY:
                    handleActionSyncCountry();
                    break;
                case ACTION_SYNC_CITY:
                    handleActionSyncCity();
                    break;
                case ACTION_BOOKMARK:
                    boolean bookmark = intent.getBooleanExtra("BOOKMARK", false);
                    int id = intent.getIntExtra("ID", 0);
                    handleActionBookmark(id, bookmark);
                    break;
                case ACTION_SET_DEFAULT_CATEGORY:
                    handleSetDefaultCategory();;
                    break;
            }
        }
    }

    /**
     * Handle action Foo in the provided background thread with the provided
     * parameters.
     */
    private void handleActionSyncType() {
        // TODO: Handle action Foo
        new Thread(new Runnable() {
            @Override
            public void run() {
                Realm realm = null;
                try {
                    realm = RealmInfo.getRealm(getApplicationContext());
                    TypeJobInfo data = new TypeJobInfo();
                    long lastSyncTypeDate = 0;//SharedPreferencesUtil.getLong(getApplicationContext(), Constants.LAST_SYNC_TYPEJOB, 0L);
                    ServiceResponseEntity<JsonArray> result = data.syncType(getApplicationContext(), lastSyncTypeDate);
                    if (result != null && result.getStatus() == 0) {

//                        realm.executeTransaction(realm1 -> {
////                            RealmResults<TypeJobEntity> result1 = realm1.where(TypeJobEntity.class).findAll();
////                            result1.deleteAllFromRealm();
////                        });


                        if (data.update(realm, result.getData()).getData())
                            SharedPreferencesUtil.setSharedPreferences(getApplicationContext(), SharedPreferencesUtil.EnumType.Long, Constants.LAST_SYNC_TYPEJOB, new Date().getTime());
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                } catch (RealmError e) {
                    e.printStackTrace();
                } catch (OutOfMemoryError e) {
                    e.printStackTrace();
                } finally {
                    RealmInfo.closeRealm(realm);
                }
            }
        }).start();
    }

    private void handleActionSyncCompany() {
        // TODO: Handle action Foo
        new Thread(new Runnable() {
            @Override
            public void run() {
                Realm realm = null;
                try {
                    realm = RealmInfo.getRealm(getApplicationContext());
                    CompanyJobInfo data = new CompanyJobInfo();
                    long lastSyncDate = 0;//SharedPreferencesUtil.getLong(getApplicationContext(), Constants.LAST_SYNC_TYPEJOB, 0L);
                    ServiceResponseEntity<JsonArray> result = data.sync(getApplicationContext(), lastSyncDate);
                    if (result != null && result.getStatus() == 0) {

                        if (data.update(realm, result.getData()).getData())
                            SharedPreferencesUtil.setSharedPreferences(getApplicationContext(), SharedPreferencesUtil.EnumType.Long, Constants.LAST_SYNC_COMPANYJOB, new Date().getTime());
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                } catch (RealmError e) {
                    e.printStackTrace();
                } catch (OutOfMemoryError e) {
                    e.printStackTrace();
                } finally {
                    RealmInfo.closeRealm(realm);
                }
            }
        }).start();
    }

    private void handleActionSyncLevel() {
        // TODO: Handle action Foo
        new Thread(new Runnable() {
            @Override
            public void run() {
                Realm realm = null;
                try {
                    realm = RealmInfo.getRealm(getApplicationContext());
                    LevelJobInfo data = new LevelJobInfo();
                    long lastSyncDate = 0;//SharedPreferencesUtil.getLong(getApplicationContext(), Constants.LAST_SYNC_TYPEJOB, 0L);
                    ServiceResponseEntity<JsonArray> result = data.sync(getApplicationContext(), lastSyncDate);
                    if (result != null && result.getStatus() == 0) {
                        if (data.update(realm, result.getData()).getData())
                            SharedPreferencesUtil.setSharedPreferences(getApplicationContext(), SharedPreferencesUtil.EnumType.Long, Constants.LAST_SYNC_LEVELJOB, new Date().getTime());
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                } catch (RealmError e) {
                    e.printStackTrace();
                } catch (OutOfMemoryError e) {
                    e.printStackTrace();
                } finally {
                    RealmInfo.closeRealm(realm);
                }
            }
        }).start();
    }
    
    private void handleActionSyncSalary() {
        // TODO: Handle action Foo
        new Thread(new Runnable() {
            @Override
            public void run() {
                Realm realm = null;
                try {
                    realm = RealmInfo.getRealm(getApplicationContext());
                    SalaryJobInfo data = new SalaryJobInfo();
                    long lastSyncDate = 0;//SharedPreferencesUtil.getLong(getApplicationContext(), Constants.LAST_SYNC_TYPEJOB, 0L);
                    ServiceResponseEntity<JsonArray> result = data.sync(getApplicationContext(), lastSyncDate);
                    if (result != null && result.getStatus() == 0) {
                        if (data.update(realm, result.getData()).getData())
                            SharedPreferencesUtil.setSharedPreferences(getApplicationContext(), SharedPreferencesUtil.EnumType.Long, Constants.LAST_SYNC_SALARYJOB, new Date().getTime());
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                } catch (RealmError e) {
                    e.printStackTrace();
                } catch (OutOfMemoryError e) {
                    e.printStackTrace();
                } finally {
                    RealmInfo.closeRealm(realm);
                }
            }
        }).start();
    }
    private void handleActionSyncCountry() {
        // TODO: Handle action Foo
        new Thread(new Runnable() {
            @Override
            public void run() {
                Realm realm = null;
                try {
                    realm = RealmInfo.getRealm(getApplicationContext());
                    CountryJobInfo data = new CountryJobInfo();
                    long lastSyncDate = 0;//SharedPreferencesUtil.getLong(getApplicationContext(), Constants.LAST_SYNC_TYPEJOB, 0L);
                    ServiceResponseEntity<JsonArray> result = data.sync(getApplicationContext(), lastSyncDate);
                    if (result != null && result.getStatus() == 0) {
                        if (data.update(realm, result.getData()).getData())
                            SharedPreferencesUtil.setSharedPreferences(getApplicationContext(), SharedPreferencesUtil.EnumType.Long, Constants.LAST_SYNC_COUNTRYJOB, new Date().getTime());
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                } catch (RealmError e) {
                    e.printStackTrace();
                } catch (OutOfMemoryError e) {
                    e.printStackTrace();
                } finally {
                    RealmInfo.closeRealm(realm);
                }
            }
        }).start();
    }
    private void handleActionSyncCity() {
        // TODO: Handle action Foo
        new Thread(new Runnable() {
            @Override
            public void run() {
                Realm realm = null;
                try {
                    realm = RealmInfo.getRealm(getApplicationContext());
                    CityJobInfo data = new CityJobInfo();
                    long lastSyncDate = 0;//SharedPreferencesUtil.getLong(getApplicationContext(), Constants.LAST_SYNC_TYPEJOB, 0L);
                    ServiceResponseEntity<JsonArray> result = data.sync(getApplicationContext(), lastSyncDate);
                    if (result != null && result.getStatus() == 0) {
                        if (data.update(realm, result.getData()).getData())
                            SharedPreferencesUtil.setSharedPreferences(getApplicationContext(), SharedPreferencesUtil.EnumType.Long, Constants.LAST_SYNC_CITYJOB, new Date().getTime());
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                } catch (RealmError e) {
                    e.printStackTrace();
                } catch (OutOfMemoryError e) {
                    e.printStackTrace();
                } finally {
                    RealmInfo.closeRealm(realm);
                }
            }
        }).start();
    }
    private void handleActionBookmark(int id, boolean bookmark) {
        // TODO: Handle action Foo
        new Thread(new Runnable() {
            @Override
            public void run() {
                Realm realm = null;
                try {
                    realm = RealmInfo.getRealm(getApplicationContext());
                    if (UserInfo.isLogin()) {
                        JobInfo jobInfo = new JobInfo();
                        realm = RealmInfo.getRealm(getApplicationContext());
                        new BookmarkInfo().bookmark(getApplicationContext(), realm, id, UserInfo.getCurrentUserId(), BookmarkInfo.BookmarkType.Job, bookmark);
                        jobInfo.bookmark(getApplicationContext(), id, bookmark);
                        Intent intent = new Intent(Constants.ACTION_JOB_BOOKMARK_SUCCESS);
                        getApplicationContext().sendBroadcast(intent);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                } catch (RealmError e) {
                    e.printStackTrace();
                } catch (OutOfMemoryError e) {
                    e.printStackTrace();
                } finally {
                    RealmInfo.closeRealm(realm);
                }
            }
        }).start();
    }
    private void handleSetDefaultCategory() {
        // TODO: Handle action Foo
        new Thread(new Runnable() {
            @Override
            public void run() {
                String userId = UserInfo.getCurrentUserId();
                Realm realm = null;
                try {
                    realm = RealmInfo.getRealm(getApplicationContext());
                    TypeJobInfo typeInfo = new TypeJobInfo();
                    BaseReturnFunctionEntity<ArrayList<TypeJobEntity>> currentMyType = typeInfo.getTypeJobHasSelected(getApplicationContext(), realm, userId);
                    if (currentMyType.isTrue() && currentMyType.getData().size() == 0) {
                        BaseReturnFunctionEntity<ArrayList<TypeJobEntity>> listType = typeInfo.getListTypeJob(getApplicationContext(), realm);
                        if (listType.isTrue()) {
                            ArrayList<TypeJobEntity> type = listType.getData();
                            for (int i = 0; i < 3 && i < type.size(); i++) {
                                typeInfo.saveTypeJobToFavorite(getApplicationContext(), realm, userId, type.get(i).getId());
                            }
                            Intent intent = new Intent(Constants.ACTION_TYPE_JOB_FOR_USER_CHANGED);
                            getApplicationContext().sendBroadcast(intent);
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                } catch (RealmError e) {
                    e.printStackTrace();
                } catch (OutOfMemoryError e) {
                    e.printStackTrace();
                } finally {
                    RealmInfo.closeRealm(realm);
                }
            }
        }).start();
    }

}
