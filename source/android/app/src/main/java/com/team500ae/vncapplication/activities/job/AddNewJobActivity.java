package com.team500ae.vncapplication.activities.job;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.InputFilter;
import android.text.InputType;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.google.android.gms.maps.model.LatLng;
import com.google.gson.JsonArray;
import com.team500ae.library.utils.AndroidPermissionUtils;
import com.team500ae.library.utils.ConvertUtils;
import com.team500ae.library.utils.ImageUtils;
import com.team500ae.vncapplication.R;
import com.team500ae.vncapplication.base.BaseActivity;
import com.team500ae.vncapplication.constants.Constants;
import com.team500ae.vncapplication.data_access.RealmInfo;
import com.team500ae.vncapplication.data_access.job.CityJobInfo;
import com.team500ae.vncapplication.data_access.job.CompanyJobInfo;
import com.team500ae.vncapplication.data_access.job.CountryJobInfo;
import com.team500ae.vncapplication.data_access.job.JobInfo;
import com.team500ae.vncapplication.data_access.job.LevelJobInfo;
import com.team500ae.vncapplication.data_access.job.TypeJobInfo;
import com.team500ae.vncapplication.data_access.upload.BlobUploadProvider;
import com.team500ae.vncapplication.data_access.upload.UploadInfo;
import com.team500ae.vncapplication.data_access.user.UserInfo;
import com.team500ae.vncapplication.models.BaseReturnFunctionEntity;
import com.team500ae.vncapplication.models.ServiceResponseEntity;
import com.team500ae.vncapplication.models.realm_models.job.CityJobEntity;
import com.team500ae.vncapplication.models.realm_models.job.CompanyJobEntity;
import com.team500ae.vncapplication.models.realm_models.job.CountryJobEntity;
import com.team500ae.vncapplication.models.realm_models.job.LevelJobEntity;
import com.team500ae.vncapplication.models.realm_models.job.TypeJobEntity;
import com.team500ae.vncapplication.others.DialogListener;
import com.team500ae.vncapplication.others.UploadImageDialog;
import com.team500ae.vncapplication.utils.AppUtils;
import com.team500ae.vncapplication.utils.FileUtils;
import com.team500ae.vncapplication.utils.GPSTrackerUtils;
import com.team500ae.vncapplication.utils.KeyboardUtils;
import com.team500ae.vncapplication.utils.LanguageUtils;
import com.team500ae.vncapplication.utils.StringUtils;

import java.io.File;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;
import io.realm.Realm;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;
/**
 * Created by Phuong D. Nguyen on 5/18/2018.
 */

public class AddNewJobActivity extends BaseActivity {

    private EditText edtJobTitleContent, edtCompanyContent, edtSalaryContent, edtJobDescriptionContent, edtJobRequirementContent, edtPhoneNumberContent, edtEmailContent;
    private TextView tvJobCategoryContent, tvJobPositionContent, tvNationContent, tvCityContent;
    private ImageView ivJobCategoryRightIcon, ivJobPositionRightIcon, ivCompanyRightIcon, ivNationRightIcon, ivCityRightIcon;
    private CircleImageView civUserAvatar;
    private Button btnAddNewJob;

    private Bitmap bitmap;
    private Uri selectedImage = Uri.EMPTY;
    private String thumbnailUrl = "";
    private int isUploadFile = 0;
    private Uri tempUri;
    private Realm realm;
    private boolean isAddedJob = false;
    private Place place;

    ArrayList<Integer> listNationId;
    ArrayList<String> listNationName;

    ArrayList<Integer> listCityId;
    ArrayList<String> listCityName;

    ArrayList<Integer> listJobCategoryId;
    ArrayList<String> listJobCategoryName;

    ArrayList<Integer> listJobPositionId;
    ArrayList<String> listJobPositionName;

    ArrayList<Integer> listCompanyId;
    ArrayList<String> listCompanyName;
    private String urlImage;
    String urlHeader = "http://maps.google.com/maps/api/staticmap?zoom=16&size=600x600&markers=";
    private static final int REQ_CODE_PLACE_PICKER = 10006;

    String urlFooter = "&sensor=false";
    private LatLng latlon;
    private EditText edtAddressContent;

    public static void open(FragmentActivity activity) {
        Intent intent = new Intent(activity, AddNewJobActivity.class);
        activity.startActivity(intent);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        setContentView(R.layout.activity_add_job);
        ButterKnife.bind(this);
        KeyboardUtils.setupUI(getWindow().getDecorView().getRootView(), this);
        super.onCreate(savedInstanceState);
        realm = RealmInfo.getRealm(getApplicationContext());
        showBackButton();
        getSupportActionBar().setTitle(R.string.title_activity_job_add);
        isAddedJob = false;

        initLayout();
        loadDataSpinner();

        btnAddNewJob.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addNewJob();
            }
        });

        tvJobCategoryContent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showJobCategoryDialog();
            }
        });
        ivJobCategoryRightIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showJobCategoryDialog();
            }
        });

        tvJobPositionContent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showJobPositionDialog();
            }
        });
        ivJobPositionRightIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showJobPositionDialog();
            }
        });

//        tvCompanyContent.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                showCompanyDialog();
//            }
//        });
//        ivCompanyRightIcon.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                showCompanyDialog();
//            }
//        });

        tvNationContent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showNationDialog();
            }
        });
        ivNationRightIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showNationDialog();
            }
        });

        tvCityContent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showCityDialog();
            }
        });
        ivCityRightIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showCityDialog();
            }
        });

        GPSTrackerUtils gpsTrackerUtils = new GPSTrackerUtils(getActivity());
        if (gpsTrackerUtils.canGetLocation()) {
            Log.d(TAG, "canGetLocation");
            latlon = new LatLng(gpsTrackerUtils.getLatitude(), gpsTrackerUtils.getLongitude());
            GPSTrackerUtils.saveCurrentLocation(getActivity(), gpsTrackerUtils.getLongitude(), gpsTrackerUtils.getLatitude());
            bindData(true);
            return;
        } else {
        }
    }

    @BindView(R.id.imgLocation)
    ImageView imgLocation;

    @OnClick(R.id.imgLocation)
    void getLocation() {
        PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();
        try {
            startActivityForResult(builder.build(getActivity()), REQ_CODE_PLACE_PICKER);
        } catch (GooglePlayServicesRepairableException e) {
            showSnackBar("Bạn cần cài đặt google playservice để sử dụng tính năng này");
            e.printStackTrace();
        } catch (GooglePlayServicesNotAvailableException e) {
            showSnackBar("Bạn cần cài đặt google playservice để sử dụng tính năng này");
            e.printStackTrace();
        }
    }

    private void bindData(boolean withLonLat) {
//        getControl();

        double latitude = GPSTrackerUtils.getCurrentLocationLatitude(getActivity());
        double longitude = GPSTrackerUtils.getCurrentLocationLongitude(getActivity());
        if (!withLonLat) {
            latitude = GPSTrackerUtils.getCurrentLocationLatitude(getActivity());
            longitude = GPSTrackerUtils.getCurrentLocationLongitude(getActivity());
        } else
//            tvPlace.setText(xa + ", " + huyen + ", " + tinh);
            if (latitude != 0 | longitude != 0) {
                urlImage = urlHeader + latitude + "," + longitude + urlFooter;
                ImageUtils.loadImageByGlide(getActivity(), false, 0, 150, urlImage, R.drawable.bg_map_vn, R.drawable.bg_map_vn, imgLocation, true);
            }
    }

    public void showBackButton() {
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }


    private void initLayout() {
        View vJobTitleInclude = findViewById(R.id.layout_job_title);
        ImageView ivJobTitleLeftIcon = (ImageView) vJobTitleInclude.findViewById(R.id.image_left);
        ivJobTitleLeftIcon.setImageResource(R.drawable.ic_job_title);
        TextView tvJobTitleTitle = (TextView) vJobTitleInclude.findViewById(R.id.text_title);
        tvJobTitleTitle.setText(getResources().getString(R.string.job_title));
        tvJobTitleTitle.setTextColor(getResources().getColor(R.color.colorPrimary));
        edtJobTitleContent = (EditText) vJobTitleInclude.findViewById(R.id.text_content);
        edtJobTitleContent.setFilters(new InputFilter[]{new InputFilter.LengthFilter(Constants.MAX_FULL_NAME_LENGTH)});
        ImageView ivJobTitleRightIcon = (ImageView) vJobTitleInclude.findViewById(R.id.image_right);
        ivJobTitleRightIcon.setVisibility(View.GONE);

        View vJobCategoryInclude = findViewById(R.id.layout_job_category);
        ImageView ivJobCategoryLeftIcon = (ImageView) vJobCategoryInclude.findViewById(R.id.image_left);
        ivJobCategoryLeftIcon.setImageResource(R.drawable.ic_job_category);
        TextView tvJobCategoryTitle = (TextView) vJobCategoryInclude.findViewById(R.id.text_title);
        tvJobCategoryTitle.setText(getResources().getString(R.string.job_category));
        tvJobCategoryContent = (TextView) vJobCategoryInclude.findViewById(R.id.text_content);
        ivJobCategoryRightIcon = (ImageView) vJobCategoryInclude.findViewById(R.id.image_right);
        ivJobCategoryRightIcon.setImageResource(R.drawable.ic_next);

        View vJobPositionInclude = findViewById(R.id.layout_job_position);
        ImageView ivJobPositionLeftIcon = (ImageView) vJobPositionInclude.findViewById(R.id.image_left);
        ivJobPositionLeftIcon.setImageResource(R.drawable.ic_job);
        TextView tvJobPositionTitle = (TextView) vJobPositionInclude.findViewById(R.id.text_title);
        tvJobPositionTitle.setText(getResources().getString(R.string.job_position));
        tvJobPositionContent = (TextView) vJobPositionInclude.findViewById(R.id.text_content);
        ivJobPositionRightIcon = (ImageView) vJobPositionInclude.findViewById(R.id.image_right);
        ivJobPositionRightIcon.setImageResource(R.drawable.ic_next);

        View vSalaryInclude = findViewById(R.id.layout_salary);
        ImageView ivSalaryLeftIcon = (ImageView) vSalaryInclude.findViewById(R.id.image_left);
        ivSalaryLeftIcon.setImageResource(R.drawable.ic_salary);
        TextView tvSalaryTitle = (TextView) vSalaryInclude.findViewById(R.id.text_title);
        tvSalaryTitle.setText(getResources().getString(R.string.salary));
        edtSalaryContent = (EditText) vSalaryInclude.findViewById(R.id.text_content);
        edtSalaryContent.setFilters(new InputFilter[]{new InputFilter.LengthFilter(Constants.MAX_PHONE_NUMBER_LENGTH)});
        edtSalaryContent.setInputType(InputType.TYPE_CLASS_NUMBER);
        edtSalaryContent.setFilters(new InputFilter[]{new InputFilter.LengthFilter(Constants.MAX_PHONE_NUMBER_LENGTH)});
        ImageView ivSalaryRightIcon = (ImageView) vSalaryInclude.findViewById(R.id.image_right);
        ivSalaryRightIcon.setVisibility(View.GONE);

//        View vCompanyInclude = findViewById(R.id.layout_company);
//        ImageView ivCompanyLeftIcon = (ImageView) vCompanyInclude.findViewById(R.id.image_left);
//        ivCompanyLeftIcon.setImageResource(R.drawable.ic_company);
//        TextView tvCompanyTitle = (TextView) vCompanyInclude.findViewById(R.id.text_title);
//        tvCompanyTitle.setText(getResources().getString(R.string.company));
//        tvCompanyContent = (TextView) vCompanyInclude.findViewById(R.id.text_content);
//        ivCompanyRightIcon = (ImageView) vCompanyInclude.findViewById(R.id.image_right);
//        ivCompanyRightIcon.setImageResource(R.drawable.ic_next);

        View vCompanyInclude = findViewById(R.id.layout_company);
        ImageView ivCompanyLeftIcon = (ImageView) vCompanyInclude.findViewById(R.id.image_left);
        ivCompanyLeftIcon.setImageResource(R.drawable.ic_company);
        TextView tvCompanyTitle = (TextView) vCompanyInclude.findViewById(R.id.text_title);
        tvCompanyTitle.setText(getResources().getString(R.string.company));
        edtCompanyContent = (EditText) vCompanyInclude.findViewById(R.id.text_content);
        //edtCompanyContent.setFilters(new InputFilter[]{new InputFilter.LengthFilter(Constants.MAX_PHONE_NUMBER_LENGTH)});
        //edtCompanyContent.setInputType(InputType.TYPE_CLASS_NUMBER);
        edtCompanyContent.setFilters(new InputFilter[]{new InputFilter.LengthFilter(Constants.MAX_FULL_NAME_LENGTH)});
        ImageView ivCompanyRightIcon = (ImageView) vCompanyInclude.findViewById(R.id.image_right);
        ivCompanyRightIcon.setVisibility(View.GONE);

        View vNationInclude = findViewById(R.id.layout_nation);
        ImageView ivNationLeftIcon = (ImageView) vNationInclude.findViewById(R.id.image_left);
        ivNationLeftIcon.setImageResource(R.drawable.ic_nation);
        TextView tvNationTitle = (TextView) vNationInclude.findViewById(R.id.text_title);
        tvNationTitle.setText(getResources().getString(R.string.nation));
        tvNationContent = (TextView) vNationInclude.findViewById(R.id.text_content);
        ivNationRightIcon = (ImageView) vNationInclude.findViewById(R.id.image_right);
        ivNationRightIcon.setImageResource(R.drawable.ic_next);

        View vCityInclude = findViewById(R.id.layout_city);
        ImageView ivCityLeftIcon = (ImageView) vCityInclude.findViewById(R.id.image_left);
        ivCityLeftIcon.setImageResource(R.drawable.ic_address);
        TextView tvCityTitle = (TextView) vCityInclude.findViewById(R.id.text_title);
        tvCityTitle.setText(getResources().getString(R.string.city));
        tvCityContent = (TextView) vCityInclude.findViewById(R.id.text_content);
        ivCityRightIcon = (ImageView) vCityInclude.findViewById(R.id.image_right);
        ivCityRightIcon.setImageResource(R.drawable.ic_next);

        View vJobDescriptionInclude = findViewById(R.id.layout_job_description);
        ImageView ivJobDescriptionLeftIcon = (ImageView) vJobDescriptionInclude.findViewById(R.id.image_left);
        ivJobDescriptionLeftIcon.setImageResource(R.drawable.ic_job_description);
        TextView tvJobDescriptionTitle = (TextView) vJobDescriptionInclude.findViewById(R.id.text_title);
        tvJobDescriptionTitle.setVisibility(View.GONE);
        edtJobDescriptionContent = (EditText) vJobDescriptionInclude.findViewById(R.id.text_content);
        edtJobDescriptionContent.setGravity(Gravity.LEFT);
        edtJobDescriptionContent.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() >= 500) {
                    new AlertDialog.Builder(AddNewJobActivity.this).setIcon(R.drawable.ic_app_logo).setTitle(getResources().getString(R.string.notification)).setMessage(getResources().getString(R.string.job_add_max_lenght_text)).setPositiveButton(android.R.string.ok, null).show();
                    edtJobDescriptionContent.requestFocus();
                }
            }
        });
        ImageView ivJobDescriptionRightIcon = (ImageView) vJobDescriptionInclude.findViewById(R.id.image_right);
        ivJobDescriptionRightIcon.setVisibility(View.GONE);

        View vJobRequirementInclude = findViewById(R.id.layout_job_requirement);
        ImageView ivJobRequirementLeftIcon = (ImageView) vJobRequirementInclude.findViewById(R.id.image_left);
        ivJobRequirementLeftIcon.setImageResource(R.drawable.ic_job_requirement);
        TextView tvJobRequirementTitle = (TextView) vJobRequirementInclude.findViewById(R.id.text_title);
        tvJobRequirementTitle.setVisibility(View.GONE);
        edtJobRequirementContent = (EditText) vJobRequirementInclude.findViewById(R.id.text_content);
        edtJobRequirementContent.setGravity(Gravity.LEFT);
        edtJobRequirementContent.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() >= 500) {
                    new AlertDialog.Builder(AddNewJobActivity.this).setIcon(R.drawable.ic_job_requirement                                                                             ).setTitle(getResources().getString(R.string.character_limit_exceeded)).setMessage(getResources().getString(R.string.job_add_max_lenght_text)).setPositiveButton(android.R.string.ok, null).show();
                    edtJobRequirementContent.requestFocus();
                }
            }
        });
        ImageView ivJobRequirementRightIcon = (ImageView) vJobRequirementInclude.findViewById(R.id.image_right);
        ivJobRequirementRightIcon.setVisibility(View.GONE);

        View vAddressInclude = findViewById(R.id.layout_address);
        ImageView ivAddressLeftIcon = (ImageView) vAddressInclude.findViewById(R.id.image_left);
        ivAddressLeftIcon.setImageResource(R.drawable.ic_address);
        TextView tvAddressTitle = (TextView) vAddressInclude.findViewById(R.id.text_title);
        tvAddressTitle.setText(getResources().getString(R.string.address));
        edtAddressContent = (EditText) vAddressInclude.findViewById(R.id.text_content);
        edtAddressContent.setFilters(new InputFilter[]{new InputFilter.LengthFilter(Constants.MAX_ADDRESS_LENGTH)});
        edtAddressContent.setInputType(InputType.TYPE_CLASS_TEXT);
        ImageView ivAddressRightIcon = (ImageView) vAddressInclude.findViewById(R.id.image_right);
        ivAddressRightIcon.setVisibility(View.GONE);

        View vPhoneNumberInclude = findViewById(R.id.layout_phone_number);
        ImageView ivPhoneNumberLeftIcon = (ImageView) vPhoneNumberInclude.findViewById(R.id.image_left);
        ivPhoneNumberLeftIcon.setImageResource(R.drawable.ic_phone_number);
        TextView tvPhoneNumberTitle = (TextView) vPhoneNumberInclude.findViewById(R.id.text_title);
        tvPhoneNumberTitle.setText(getResources().getString(R.string.phone_number));
        edtPhoneNumberContent = (EditText) vPhoneNumberInclude.findViewById(R.id.text_content);
        edtPhoneNumberContent.setFilters(new InputFilter[]{new InputFilter.LengthFilter(Constants.MAX_PHONE_NUMBER_LENGTH)});
        edtPhoneNumberContent.setInputType(InputType.TYPE_CLASS_NUMBER);
        edtPhoneNumberContent.setFilters(new InputFilter[]{new InputFilter.LengthFilter(Constants.MAX_PHONE_NUMBER_LENGTH)});
        ImageView ivPhoneNumberRightIcon = (ImageView) vPhoneNumberInclude.findViewById(R.id.image_right);
        ivPhoneNumberRightIcon.setVisibility(View.GONE);

        View vEmailInclude = findViewById(R.id.layout_email);
        ImageView ivEmailLeftIcon = (ImageView) vEmailInclude.findViewById(R.id.image_left);
        ivEmailLeftIcon.setImageResource(R.drawable.ic_email);
        TextView tvEmailTitle = (TextView) vEmailInclude.findViewById(R.id.text_title);
        tvEmailTitle.setText(getResources().getString(R.string.email));
        tvEmailTitle.setTextColor(getResources().getColor(R.color.colorPrimary));
        edtEmailContent = (EditText) vEmailInclude.findViewById(R.id.text_content);
        edtEmailContent.setFilters(new InputFilter[]{new InputFilter.LengthFilter(Constants.MAX_FULL_NAME_LENGTH)});
        edtEmailContent.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS);
        edtEmailContent.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() >= Constants.MAX_FULL_NAME_LENGTH) {
                    new AlertDialog.Builder(AddNewJobActivity.this).setIcon(R.drawable.ic_email).setTitle(getResources().getString(R.string.character_limit_exceeded)).setMessage(getResources().getString(R.string.notice_number_limit_character) + " " + Constants.MAX_FULL_NAME_LENGTH).setPositiveButton(android.R.string.ok, null).show();
                    edtEmailContent.requestFocus();
                }
            }
        });
        ImageView ivEmailRightIcon = (ImageView) vEmailInclude.findViewById(R.id.image_right);
        ivEmailRightIcon.setVisibility(View.GONE);

        civUserAvatar = (CircleImageView) findViewById(R.id.civ_user_avatar);
        TextView profileTitle = (TextView) findViewById(R.id.text_title);
        profileTitle.setText(getResources().getString(R.string.job_information));
        btnAddNewJob = (Button) findViewById(R.id.btn_add_job);
    }

    private void showJobCategoryDialog() {
        final String[] arrayJobCategory = listJobCategoryName.toArray(new String[0]);
        String currentJobCategory = tvJobCategoryContent.getText().toString();
        int itemSelected = StringUtils.getStringIndex(listJobCategoryName, currentJobCategory);

        new AlertDialog.Builder(this).setTitle(getResources().getString(R.string.job_category)).setSingleChoiceItems(arrayJobCategory, itemSelected, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int selectedIndex) {
                tvJobCategoryContent.setText(arrayJobCategory[selectedIndex]);
            }
        }).setIcon(R.drawable.ic_job_category).setNegativeButton(getResources().getString(R.string.close), null).show();
    }

    private void showJobPositionDialog() {
        final String[] arrayJobPosition = listJobPositionName.toArray(new String[0]);
        String currentJobPosition = tvJobPositionContent.getText().toString();
        int itemSelected = StringUtils.getStringIndex(listJobPositionName, currentJobPosition);

        new AlertDialog.Builder(this).setTitle(getResources().getString(R.string.job_position)).setSingleChoiceItems(arrayJobPosition, itemSelected, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int selectedIndex) {
                tvJobPositionContent.setText(arrayJobPosition[selectedIndex]);
            }
        }).setIcon(R.drawable.ic_job).setNegativeButton(getResources().getString(R.string.close), null).show();
    }

//    private void showCompanyDialog() {
//        final String[] arrayCompany = listCompanyName.toArray(new String[0]);
//        String currentCompany = tvCompanyContent.getText().toString();
//        int itemSelected = StringUtils.getStringIndex(listCompanyName, currentCompany);
//
//        new AlertDialog.Builder(this).setTitle(getResources().getString(R.string.company)).setSingleChoiceItems(arrayCompany, itemSelected, new DialogInterface.OnClickListener() {
//            @Override
//            public void onClick(DialogInterface dialog, int selectedIndex) {
//                tvCompanyContent.setText(arrayCompany[selectedIndex]);
//            }
//        }).setIcon(R.drawable.ic_company).setNegativeButton(getResources().getString(R.string.close), null).show();
//    }

    private void showNationDialog() {
        final String[] arrayNation = listNationName.toArray(new String[0]);
        String currentNation = tvNationContent.getText().toString();
        int itemSelected = StringUtils.getStringIndex(listNationName, currentNation);

        new AlertDialog.Builder(this).setTitle(getResources().getString(R.string.nation)).setSingleChoiceItems(arrayNation, itemSelected, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int selectedIndex) {
                tvNationContent.setText(arrayNation[selectedIndex]);
            }
        }).setIcon(R.drawable.ic_nation).setNegativeButton(getResources().getString(R.string.close), null).show();
    }

    private void showCityDialog() {
        String nation = tvNationContent.getText().toString();
        if (nation == null || nation.isEmpty()) {
            AlertDialog alertDialog = new AlertDialog.Builder(AddNewJobActivity.this).create();
            alertDialog.setTitle("Alert");
            alertDialog.setMessage("Please select nation first");
            alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
            alertDialog.show();
            return;
        }

        int nationId = StringUtils.getIdFromValue(listNationName, listNationId, nation);
        BaseReturnFunctionEntity<ArrayList<CityJobEntity>> myList = new CityJobInfo().getList(getApplicationContext(), realm);
        listCityId = new ArrayList<>();
        listCityName = new ArrayList<>();
        if (myList.isTrue()) {
            for (CityJobEntity type : myList.getData()) {
                if (type.getCountry() == nationId) {
                    listCityId.add(type.getId());
                    listCityName.add(type.getName());
                }
            }
        }

        final String[] arrayCity = listCityName.toArray(new String[0]);
        String currentCity = tvCityContent.getText().toString();
        int itemSelected = StringUtils.getStringIndex(listCityName, currentCity);

        new AlertDialog.Builder(this).setTitle(getResources().getString(R.string.city)).setSingleChoiceItems(arrayCity, itemSelected, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int selectedIndex) {
                tvCityContent.setText(arrayCity[selectedIndex]);
            }
        }).setIcon(R.drawable.ic_address).setNegativeButton(getResources().getString(R.string.close), null).show();
    }

    private void loadDataSpinner() {
        BaseReturnFunctionEntity<ArrayList<CompanyJobEntity>> myListCompany = new CompanyJobInfo().getList(getApplicationContext(), realm);
        listCompanyId = new ArrayList<>();
        listCompanyName = new ArrayList<>();
        if (myListCompany.isTrue()) {
            for (CompanyJobEntity type : myListCompany.getData()) {
                listCompanyId.add(type.getId());
                listCompanyName.add(type.getName());
            }
        }

        BaseReturnFunctionEntity<ArrayList<LevelJobEntity>> myListLevel = new LevelJobInfo().getList(getApplicationContext(), realm);
        listJobPositionName = new ArrayList<>();
        listJobPositionId = new ArrayList<>();
        String appLanguageSetting = LanguageUtils.getAppLanguageSetting(getActivity());
        if (myListLevel.isTrue()) {
            //myList2.addAll(myList.getData());
            for (LevelJobEntity type : myListLevel.getData()) {
                listJobPositionId.add(type.getId());
                listJobPositionName.add(appLanguageSetting.equals("en") ? type.getName() : type.getName2());
            }
        }

        BaseReturnFunctionEntity<ArrayList<TypeJobEntity>> myListJobCategory = new TypeJobInfo().getListTypeJob(getApplicationContext(), realm);
        listJobCategoryId = new ArrayList<>();
        listJobCategoryName = new ArrayList<>();
        String appLanguageSetting2 = LanguageUtils.getAppLanguageSetting(getActivity());
        if (myListJobCategory.isTrue()) {
            for (TypeJobEntity type : myListJobCategory.getData()) {
                listJobCategoryId.add(type.getId());
                listJobCategoryName.add(appLanguageSetting2.equals("en") ? type.getName() : type.getName2());
            }
        }

        BaseReturnFunctionEntity<ArrayList<CountryJobEntity>> myListCountry = new CountryJobInfo().getList(getApplicationContext(), realm);
        listNationName = new ArrayList<>();
        listNationId = new ArrayList<>();
        if (myListCountry.isTrue()) {
            for (CountryJobEntity type : myListCountry.getData()) {
                listNationId.add(type.getId());
                listNationName.add(type.getName());
            }
        }
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @OnClick(R.id.civ_user_avatar)
    public void selectPicture(View view) {
        requestPermission(new AndroidPermissionUtils.OnCallbackRequestPermission() {
            @Override
            public void onSuccess() {
                UploadImageDialog uploadImageDialog = new UploadImageDialog(getActivity());
                uploadImageDialog.setOnItemClickListener(new DialogListener() {
                    @Override
                    public void onConfirmClicked() {
                        thumbnailUrl = uploadImageDialog.getEditText();
                        //Toast.makeText(JobAddActivity.this, uploadImageDialog.getEditText(), Toast.LENGTH_SHORT).show();
                        ImageUtils.loadImageByGlide(getApplicationContext(), true, 200, 200, thumbnailUrl, R.drawable.img_no_image, R.drawable.img_no_image, civUserAvatar, true);
                        isUploadFile = 1;
                    }

                    @Override
                    public void onButtonChooseImage() {
                        tempUri = FileUtils.createTempFile(getActivity());
                        Intent chooseImageIntent = new FileUtils().selectOrTakePicture(getActivity(), tempUri);
                        startActivityForResult(chooseImageIntent, 1);
                    }
                });
                uploadImageDialog.show();
            }

            @Override
            public void onFailed() {

            }
        }, AndroidPermissionUtils.TypePermission.PERMISSION_READ_EXTERNAL_STORAGE, AndroidPermissionUtils.TypePermission.PERMISSION_WRIRE_EXTERNAL_STORAGE);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case 1:
                bitmap = FileUtils.getImageFromResult(getActivity(), resultCode, data, tempUri);
                bitmap = FileUtils.getImageFromResult(getActivity(), resultCode, data, tempUri);
                if (bitmap != null)
                    civUserAvatar.setImageBitmap(bitmap);
                //selectedImage = FileUtils.getSelectedImageFromResult(getActivity(), resultCode, data, tempUri);
                Bitmap bitmap = com.team500ae.vncapplication.utils.FileUtils.getImageFromResult(getActivity(), resultCode, data, tempUri);
                File output = com.team500ae.vncapplication.utils.FileUtils.saveBitmap(getActivity(), bitmap);
                selectedImage = Uri.fromFile(output);
                isUploadFile = 2;
                break;
            case REQ_CODE_PLACE_PICKER:
                if (resultCode == RESULT_OK) {
                    place = PlacePicker.getPlace(getActivity(), data);
                    latlon = place.getLatLng();
                    urlImage = urlHeader + place.getLatLng().latitude + "," + place.getLatLng().longitude + urlFooter;
                    ImageUtils.loadImageByGlide(getActivity(), false, 0, 150, urlImage, R.drawable.bg_map_vn, R.drawable.bg_map_vn, imgLocation, true);
                    edtAddressContent.setText(place.getAddress());
                }
                break;
            default:
                super.onActivityResult(requestCode, resultCode, data);
                break;
        }
    }

    private void addNewJob() {
        try {
            String jobTitle = edtJobTitleContent.getText().toString().trim();
            String salary = edtSalaryContent.getText().toString();
            String company = edtCompanyContent.getText().toString();
            String description = edtJobDescriptionContent.getText().toString().trim();
            String requirement = edtJobRequirementContent.getText().toString().trim();
            String phone = edtPhoneNumberContent.getText().toString().trim();
            String email = edtEmailContent.getText().toString().trim();
            String address = edtAddressContent.getText().toString().trim();
            String nation = tvNationContent.getText().toString().trim();
            int nationId = StringUtils.getIdFromValue(listNationName, listNationId, nation);
            String city = tvCityContent.getText().toString().trim();
            int cityId = StringUtils.getIdFromValue(listCityName, listCityId, city);
            String jobCategory = tvJobCategoryContent.getText().toString().trim();
            int jobCategoryId = StringUtils.getIdFromValue(listJobCategoryName, listJobCategoryId, jobCategory);
            String jobPosition = tvJobPositionContent.getText().toString().trim();
            int jobPositionId = StringUtils.getIdFromValue(listJobPositionName, listJobPositionId, jobPosition);
            //String company = tvCompanyContent.getText().toString().trim();
            //int company = StringUtils.getIdFromValue(listCompanyName, listCompanyId, company);

            if (jobTitle.isEmpty() || salary.isEmpty() || description.isEmpty() || requirement.isEmpty() || phone.isEmpty() || email.isEmpty() || nationId < 0 || cityId < 0 || jobCategoryId < 0 || jobPositionId < 0 || company.isEmpty() || address.isEmpty()) {
                showSnackBar(R.string.please_input_data);
                return;
            }
            if (!AppUtils.validEmail(email)) {
                showSnackBar(R.string.job_add_email_not_valid);
                return;
            }
            if (bitmap == null) {
                showSnackBar(R.string.please_input_image);
                return;
            }
            if (place == null && latlon == null) {
                showSnackBar(R.string.please_select_place);
                return;
            }

            if (isUploadFile == 2) {
                new UploadInfo(new BlobUploadProvider() {
                }, new UploadInfo.UploadListener() {
                    ProgressDialog progDialog = new ProgressDialog(AddNewJobActivity.this);

                    @Override
                    public void beginUpload() {

                        progDialog.setMessage(getString(R.string.loading));
                        progDialog.setIndeterminate(false);
                        progDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                        progDialog.setCancelable(true);
                        progDialog.show();
                    }

                    @Override
                    public void endUpload() {
                        if (progDialog.isShowing()) {
                            progDialog.dismiss();
                        }
                    }

                    @Override
                    public void onSuccess(JsonArray response) {
                        //thong bao upload thanh cong
                        thumbnailUrl = response.getAsString();
                        new AsyncTask<Void, Void, BaseReturnFunctionEntity<Boolean>>() {
                            @Override
                            protected BaseReturnFunctionEntity<Boolean> doInBackground(Void... voids) {
                                return new JobInfo().create(getApplicationContext(), jobTitle, salary, description,
                                        requirement, phone, email, UserInfo.getCurrentUserId(), cityId, nationId,
                                        jobPositionId, jobCategoryId, company, thumbnailUrl, latlon.latitude, latlon.longitude,address );
                            }

                            @Override
                            protected void onPostExecute(BaseReturnFunctionEntity<Boolean> result) {
                                super.onPostExecute(result);
                                if (result.isTrue()) {
                                    android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(getActivity());
                                    builder.setMessage(getResources().getString(R.string.add_job_message));
                                    builder.setCancelable(false);
                                    builder.setPositiveButton(getString(android.R.string.ok),
                                            new DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(DialogInterface dialog, int which) {
                                                    Intent intent = new Intent(Constants.ACTION_JOB_ADD_SUCCESS);
                                                    getApplicationContext().sendBroadcast(intent);
                                                    onBackPressed();
                                                }
                                            });
                                    builder.create().show();
                                    isAddedJob = true;
                                    Toast.makeText(getApplicationContext(), R.string.job_add_successful, Toast.LENGTH_LONG).show();
                                    onBackPressed();
                                } else {
                                    Toast.makeText(getApplicationContext(), R.string.job_add_failed + ":" + result.getMessage(), Toast.LENGTH_SHORT).show();
                                }
                            }
                        }.execute();
                    }

                    @Override
                    public void onError(ServiceResponseEntity<JsonArray> response) {
                        //thong bao upload that bai cmnr
                        thumbnailUrl = "";
                        Toast.makeText(getApplicationContext(), getString(R.string.Job_upload_image_failed), Toast.LENGTH_SHORT).show();
                    }
                }).upload(getActivity(), Constants.UPLOAD_FILE, ConvertUtils.toArrayList(Uri.class, selectedImage));
            } else if (isUploadFile == 1) {
                // asyncTaskAddJob.execute();
                new AsyncTask<Void, Void, BaseReturnFunctionEntity<Boolean>>() {
                    @Override
                    protected BaseReturnFunctionEntity<Boolean> doInBackground(Void... voids) {
                        return new JobInfo().create(getApplicationContext(), jobTitle, salary, description,
                                requirement, phone, email, UserInfo.getCurrentUserId(), cityId, nationId,
                                jobPositionId, jobCategoryId, company, thumbnailUrl, latlon.latitude, latlon.longitude, address);
                    }

                    @Override
                    protected void onPostExecute(BaseReturnFunctionEntity<Boolean> result) {
                        super.onPostExecute(result);
                        if (result.isTrue()) {
                            android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(getActivity());
                            builder.setMessage(getResources().getString(R.string.add_job_message));
                            builder.setCancelable(false);
                            builder.setPositiveButton(getString(android.R.string.ok),
                                    new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            Intent intent = new Intent(Constants.ACTION_JOB_ADD_SUCCESS);
                                            getApplicationContext().sendBroadcast(intent);
                                            onBackPressed();
                                        }
                                    });
                            builder.create().show();
                            isAddedJob = true;
                            onBackPressed();
//                            Toast.makeText(getApplicationContext(), R.string.job_add_successful, Toast.LENGTH_SHORT).show();
//                            Intent intent = new Intent(Constants.ACTION_JOB_ADD_SUCCESS);
//                            getApplicationContext().sendBroadcast(intent);
//                            isAddedJob = true;
//                            onBackPressed();
                        } else {
                            Toast.makeText(getApplicationContext(), R.string.job_add_failed + ":" + result.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }
                }.execute();
            }
        } catch (Exception e) {
            Toast.makeText(getApplicationContext(), R.string.please_input_data, Toast.LENGTH_SHORT).show();
        }
    }

    public boolean checkValueAddLocation() {
        String nation = tvNationContent.getText().toString().trim();
        int nationId = StringUtils.getIdFromValue(listNationName, listNationId, nation);
        String city = tvCityContent.getText().toString().trim();
        int cityId = StringUtils.getIdFromValue(listCityName, listCityId, city);
        String jobCategory = tvJobCategoryContent.getText().toString().trim();
        int jobCategoryId = StringUtils.getIdFromValue(listJobCategoryName, listJobCategoryId, jobCategory);
        String jobPosition = tvJobPositionContent.getText().toString().trim();
        int jobPositionId = StringUtils.getIdFromValue(listJobPositionName, listJobPositionId, jobPosition);
        return bitmap != null || !TextUtils.isEmpty(edtJobTitleContent.getText()) || !TextUtils.isEmpty(edtSalaryContent.getText()) || !TextUtils.isEmpty(edtCompanyContent.getText()) || !TextUtils.isEmpty(edtJobDescriptionContent.getText()) || !TextUtils.isEmpty(edtJobRequirementContent.getText())
                || !TextUtils.isEmpty(edtPhoneNumberContent.getText()) || !TextUtils.isEmpty(edtEmailContent.getText()) || nationId > 0 || cityId > 0 || jobCategoryId > 0 || jobPositionId > 0;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                if (checkValueAddLocation()) {
                    AppUtils.showAlertConfirm(this, getString(R.string.notice), getString(R.string.str_add_new_location_confirm_exit), new com.team500ae.vncapplication.activities.job.interfaces.DialogListener() {
                        @Override
                        public void onConfirmClicked() {
                            AddNewJobActivity.this.finish();
                            slideOut();
                        }
                    });
                } else {
                    this.finish();
                    slideOut();
                }

                break;
        }
        return false;
    }

    @Override
    public void onBackPressed() {
        if (isAddedJob) {
            super.onBackPressed();
        } else if (checkValueAddLocation()) {
            AppUtils.showAlertConfirm(this, getString(R.string.notice), getString(R.string.str_add_new_location_confirm_exit), new com.team500ae.vncapplication.activities.job.interfaces.DialogListener() {
                @Override
                public void onConfirmClicked() {
                    AddNewJobActivity.super.onBackPressed();
                }
            });
        } else
            super.onBackPressed();
    }
}
