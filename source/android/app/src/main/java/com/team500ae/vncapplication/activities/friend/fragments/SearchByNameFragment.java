package com.team500ae.vncapplication.activities.friend.fragments;

import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.gson.ExclusionStrategy;
import com.google.gson.FieldAttributes;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.google.gson.annotations.SerializedName;
import com.google.gson.reflect.TypeToken;
import com.team500ae.library.adapters.BaseRecyclerAdapter;
import com.team500ae.library.layoutmanager.MyLinearLayoutManager;
import com.team500ae.library.utils.AndroidPermissionUtils;
import com.team500ae.library.utils.ConvertUtils;
import com.team500ae.library.utils.ImageUtils;
import com.team500ae.vncapplication.R;
import com.team500ae.vncapplication.activities.chat.GroupChannelActivity;
import com.team500ae.vncapplication.activities.location.DetailLocationActivity;
import com.team500ae.vncapplication.activities.user.UserProfileActivity;
import com.team500ae.vncapplication.base.BaseViewHolder;
import com.team500ae.vncapplication.data_access.DataCacheInfo;
import com.team500ae.vncapplication.utils.KeyboardUtils;
import com.team500ae.vncapplication.utils.StringUtils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.team500ae.library.activities.BaseActivity.requestPermission;
import static com.team500ae.library.activities.BaseActivity.showSnackBar;

public class SearchByNameFragment extends Fragment implements GoogleApiClient.OnConnectionFailedListener, Filterable,
        GoogleApiClient.ConnectionCallbacks, View.OnKeyListener {


    private ArrayList<JsonObject> listObjects;
    private ArrayList<JsonObject> listObjectsHistory;
    private BaseRecyclerAdapter<ViewHolder, JsonObject> adapter;
//    private BaseRecyclerAdapter<ViewHolder, JsonObject> adapterHistory;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_search_friend_by_name, container, false);
        ButterKnife.bind(this, view);
        KeyboardUtils.setupUI(view, getActivity());
        listObjects = new ArrayList<>();
        listObjectsHistory = new ArrayList<>();
        try {
            String text = DataCacheInfo.getData(getContext(), DataCacheInfo.EnumCacheType.History_User);
            listObjectsHistory = new Gson().fromJson(text, new TypeToken<ArrayList<JsonObject>>() {
            }.getType());
            if (listObjectsHistory == null)
                listObjectsHistory = new ArrayList<>();
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (listObjectsHistory == null)
            listObjectsHistory = new ArrayList<>();
        recyclerView.setLayoutManager(new MyLinearLayoutManager(getContext()));
        recyclerView.setHasFixedSize(true);
        txtSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (!s.toString().equals("")) {
                    mClear.setVisibility(View.VISIBLE);
                    adapter.bindData(recyclerView);

                    if (listObjects != null && listObjects.size() > 0)
                        resultSearch.setVisibility(View.VISIBLE);
                    else
                        resultSearch.setVisibility(View.GONE);


                } else {
                    mClear.setVisibility(View.GONE);
//                    adapterHistory.bindData(recyclerView);

                    if (listObjectsHistory != null && listObjectsHistory.size() > 0)
                        resultSearch.setVisibility(View.VISIBLE);
                    else
                        resultSearch.setVisibility(View.GONE);

                }
                getFilter().filter(s.toString());
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        adapter = new BaseRecyclerAdapter<ViewHolder, JsonObject>(R.layout.item_user_search, new BaseRecyclerAdapter.BaseViewHolder<ViewHolder, JsonObject>() {
            @Override
            public ViewHolder getViewHolder(View v) {
                return new ViewHolder(v);
            }

            @Override
            public void bindData(RecyclerView recyclerView, ViewHolder viewHolder, JsonObject jsonObject, int position) throws IOException {
                if(ConvertUtils.toBoolean(jsonObject.get("ShowAvatar"))) {
                    ImageUtils.loadImageByGlide(getActivity(), false, 0, 0, ConvertUtils.toString(jsonObject.get("Avartar")), R.drawable.img_no_image, R.drawable.img_no_image, viewHolder.imageView, true);
                }
                else
                {
                    viewHolder.imageView.setImageDrawable(ContextCompat.getDrawable(getContext(),R.drawable.default_avatar));
                }
                viewHolder.tvName.setText(ConvertUtils.toString(jsonObject.get("FullName")));
                String phone = ConvertUtils.toString(jsonObject.get("PhoneNumber"));
                if (!StringUtils.isNullOrWhiteSpace(phone) && ConvertUtils.toBoolean(jsonObject.get("ShowPhoneNumber"))) {
                    viewHolder.btnCall.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.ic_call));
                } else {
                    viewHolder.btnCall.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.ic_call_dim));
                }
                viewHolder.btnCall.setTag(phone);

                if (ConvertUtils.toBoolean(jsonObject.get("isFriend")) || ConvertUtils.toBoolean(jsonObject.get("ReceiveStrangeMessage")) ) {
                    viewHolder.btnSendMessage.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.ic_mailbox));
                    String userName = ConvertUtils.toString(jsonObject.get("UserName"));
                    viewHolder.btnSendMessage.setTag(userName);
                } else {
                    viewHolder.btnSendMessage.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.ic_mailbox_dim));
                    viewHolder.btnSendMessage.setTag("");
                }
                int gender = ConvertUtils.toInt(jsonObject.get("Gender"));
                if (gender == 0) {
                    viewHolder.imgGender.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.ic_female));
                } else {
                    viewHolder.imgGender.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.ic_male));
                }

                String address = ConvertUtils.toString(jsonObject.get("Address"));
                if ("".equals(address)) {
                    Geocoder geocoder;

                    geocoder = new Geocoder(getContext(), Locale.getDefault());

                    List<Address> addresses = geocoder.getFromLocation(ConvertUtils.toDouble(jsonObject.get("Lat")), ConvertUtils.toDouble(jsonObject.get("Lon")), 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5
                    address = "";
                    if (addresses.size() != 0) {
                        Address _address = addresses.get(0);
                        if (!com.team500ae.vncapplication.utils.StringUtils.isNullOrWhiteSpace(_address.getLocality())) {
                            address += _address.getLocality();
                        }
                        if (!com.team500ae.vncapplication.utils.StringUtils.isNullOrWhiteSpace(_address.getSubAdminArea())) {
                            if (!address.isEmpty()) {
                                address += ", ";
                            }
                            address += _address.getSubAdminArea();
                        }
                        if (!com.team500ae.vncapplication.utils.StringUtils.isNullOrWhiteSpace(_address.getAdminArea())) {
                            if (!address.isEmpty()) {
                                address += ", ";
                            }
                            address += _address.getAdminArea();
                        }
                        if (!com.team500ae.vncapplication.utils.StringUtils.isNullOrWhiteSpace(_address.getCountryName())) {
                            if (!address.isEmpty()) {
                                address += ", ";
                            }
                            address += _address.getCountryName();
                        }
                        jsonObject.addProperty("Address", address);
                    }
                }
                viewHolder.tvAddress.setText(address);
            }
        }, listObjects, new BaseRecyclerAdapter.OnClickListener() {
            @Override
            public void onClick(View v, int position, Object o) {
                viewPlace((JsonObject) o);
            }
        });
//        adapterHistory = new BaseRecyclerAdapter<ViewHolder, JsonObject>(R.layout.item_place, new BaseRecyclerAdapter.BaseViewHolder<ViewHolder, JsonObject>() {
//            @Override
//            public ViewHolder getViewHolder(View v) {
//                return new ViewHolder(v);
//            }
//
//            @Override
//            public void bindData(RecyclerView recyclerView, ViewHolder viewHolder, JsonObject data, int position) {
//                viewHolder.tvName.setText(ConvertUtils.toString(data.get("Title")));
//                if (StringUtils.isEmpty(ConvertUtils.toString(data.get("Address")))) {
//                    viewHolder.tvAddress.setVisibility(View.GONE);
//                } else {
//                    viewHolder.tvAddress.setVisibility(View.VISIBLE);
//                }
//
//                viewHolder.tvAddress.setText(ConvertUtils.toString(data.get("Address")));
//                viewHolder.tvPlaceName.setText(ConvertUtils.toString(data.get("TypeName")));
//                ImageUtils.loadImageByGlide(getContext(), false, 0, 0, ConvertUtils.toString(data.get("TypeLogo")), R.drawable.ic_location_address, R.drawable.ic_location_address, viewHolder.imgPlaceLogo, true);
//
//            }
//        }, listObjectsHistory, new BaseRecyclerAdapter.OnClickListener() {
//            @Override
//            public void onClick(View v, int position, Object o) {
//                txtSearch.clearFocus();
//                viewPlace((JsonObject) o);
//            }
//        });
        if (listObjectsHistory != null && listObjectsHistory.size() > 0)
            resultSearch.setVisibility(View.VISIBLE);
        else
            resultSearch.setVisibility(View.GONE);
//        adapterHistory.bindData(recyclerView);
        view.setFocusableInTouchMode(true);
        view.requestFocus();
        view.setOnKeyListener(this);
        txtSearch.setOnKeyListener(this);
        return view;
    }

    void viewPlace(JsonObject jsonObject) {
        if (!listObjectsHistory.contains(jsonObject)) {
            listObjectsHistory.add(0, jsonObject);
            if (listObjectsHistory.size() > 10)
                listObjectsHistory.remove(listObjectsHistory.size() - 1);
        }
        Gson gson = new GsonBuilder()
                .setExclusionStrategies(new SerializedNameOnlyStrategy())
                .create();
        String json = gson.toJson(listObjectsHistory);
        DataCacheInfo.setData(getContext(), DataCacheInfo.EnumCacheType.History_Place, json);
        DetailLocationActivity.open(getContext(), jsonObject);
    }

    @OnClick({R.id.btnBack, R.id.parent})
    void back() {
        getFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
    }

    @BindView(R.id.btnClear)
    View mClear;

    @OnClick(R.id.btnClear)
    void clear() {
        txtSearch.setText("");
    }

    @BindView(R.id.txtSearch)
    EditText txtSearch;
    @BindView(R.id.resultSearch)
    View resultSearch;
    @BindView(R.id.list_search)
    RecyclerView recyclerView;

    public static Fragment getInstants() {
        return new SearchByNameFragment();
    }

    @Override
    public void onResume() {
        super.onResume();
    }


    @Override
    public void onConnected(@Nullable Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    private ArrayList<JsonObject> getAutocomplete(CharSequence constraint) {

        return new ArrayList<>();
    }

    @Override
    public boolean onKey(View view, int keyCode, KeyEvent keyEvent) {
        if (keyCode == KeyEvent.KEYCODE_BACK && keyEvent.getAction() == KeyEvent.ACTION_UP) {
            getFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
            return true;
        }
        return false;
    }

    public class SerializedNameOnlyStrategy implements ExclusionStrategy {

        @Override
        public boolean shouldSkipField(FieldAttributes f) {
            return f.getAnnotation(SerializedName.class) == null;
        }

        @Override
        public boolean shouldSkipClass(Class<?> clazz) {
            return false;
        }
    }

    @Override
    public Filter getFilter() {
        Filter filter = new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                FilterResults results = new FilterResults();
                // Skip the autocomplete query if no constraints are given.
                if (constraint != null) {
                    // Query the autocomplete API for the (constraint) search string.
                    ArrayList<JsonObject> mResultList = getAutocomplete(constraint);
                    if (mResultList != null) {
                        // The API successfully returned results.
                        results.values = mResultList;
                        results.count = mResultList.size();
                    }
                }
                return results;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                listObjects.clear();
                if (results != null && results.count > 0) {
                    listObjects.addAll((ArrayList<JsonObject>) results.values);
                    // The API returned at least one result, update the data.
                }
                adapter.notifyDataSetChanged();
                if (listObjects != null && listObjects.size() > 0) {
                    resultSearch.setVisibility(View.VISIBLE);
                } else
                    resultSearch.setVisibility(View.GONE);

            }
        };
        return filter;
    }

    public class ViewHolder extends BaseViewHolder {
        @BindView(R.id.imageView)
        ImageView imageView;
        @BindView(R.id.tvUserName)
        TextView tvName;
        @BindView(R.id.btnSendMessage)
        ImageView btnSendMessage;
        @BindView(R.id.tvAddress)
        TextView tvAddress;
        @BindView(R.id.btnCall)
        ImageView btnCall;
        @BindView(R.id.imgGender)
        ImageView imgGender;

        @OnClick(R.id.btnCall)
        void call(View view) {
            requestPermission(getContext(), new AndroidPermissionUtils.OnCallbackRequestPermission() {
                @Override
                public void onSuccess() {
                    String phone = (String) view.getTag();
                    if (phone != null) {
                        try {
                            Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + phone));
                            startActivity(intent);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }

                @Override
                public void onFailed() {
                    showSnackBar(getContext(), R.string.you_much_allow_this_app_make_a_call);
                }
            }, AndroidPermissionUtils.TypePermission.CALL_PHONE);
        }


        @OnClick(R.id.btnSendMessage)
        void viewProfile(View view) {
            String userId = (String) view.getTag();
            GroupChannelActivity.open(getActivity(), userId);
        }

        public ViewHolder(View itemView) {
            super(itemView);
        }
    }
}
