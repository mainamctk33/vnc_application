package com.team500ae.vncapplication;

import android.app.Application;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.multidex.MultiDex;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.messaging.FirebaseMessaging;
import com.sendbird.android.SendBird;
import com.team500ae.vncapplication.data_access.user.UserInfo;
import com.team500ae.vncapplication.services.ServiceJob;
import com.team500ae.vncapplication.services.ServiceLocation;
import com.team500ae.vncapplication.services.ServiceNews;
import com.team500ae.vncapplication.services.ServiceNotification;
import com.team500ae.vncapplication.services.ServiceTips;
import com.team500ae.vncapplication.services.ServiceTranslator;

import io.realm.Realm;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;

/**
 * Created by MaiNam on 11/2/2017.
 */

public class MainApplication extends Application {
    private static final String APP_ID = "9B29F8C3-D031-4053-B756-77CFC6435E67"; // US-1 Demo

    @Override
    public void onCreate() {
        super.onCreate();
        Realm.init(this);
        SendBird.init(APP_ID, getApplicationContext());
        try {
            ServiceNews.startActionSyncTypeNews(getApplicationContext());
            ServiceNews.startActionSyncTag(getApplicationContext());
            ServiceLocation.startActionSyncTypeLocation(getApplicationContext());
            ServiceJob.startActionSyncType(getApplicationContext());
            ServiceJob.startActionSyncCompany(getApplicationContext());
            ServiceJob.startActionSyncSalary(getApplicationContext());
            ServiceJob.startActionSyncLevel(getApplicationContext());
            ServiceJob.startActionSyncCountry(getApplicationContext());
            ServiceJob.startActionSyncCity(getApplicationContext());
            ServiceTranslator.startActionSyncCategory(getApplicationContext());
            ServiceTranslator.startActionSyncType(getApplicationContext());
            ServiceTips.startActionSyncTypeTips(getApplicationContext());
            //  ServiceJob.startActionSyncTag(getApplicationContext());
        }catch (Exception e)
        {
            e.printStackTrace();
        }
        UserInfo.getCurrentUserHaveSaved(getApplicationContext());
        //set font
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath(getString(R.string.font_open_sans_regular))
                .setFontAttrId(R.attr.fontPath)
                .build()
        );
        ServiceNotification.startRegisterTopic(getApplicationContext());
        checkConnect(this);
    }
    void checkConnect(Context mContext) {
        Connectivity.setIsConnected(Connectivity.checkConn(mContext));
    }
    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

    public static boolean start = false;
}
