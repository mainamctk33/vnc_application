package com.team500ae.vncapplication.activities.job.fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;

import com.team500ae.library.adapters.BaseViewPagerAdapter;
import com.team500ae.vncapplication.R;
import com.team500ae.vncapplication.activities.job.AddNewJobActivity;
import com.team500ae.vncapplication.activities.job.JobMgrCategoryActivity;
import com.team500ae.vncapplication.activities.job.SearchJob2Activity;
import com.team500ae.vncapplication.constants.Constants;
import com.team500ae.vncapplication.data_access.RealmInfo;
import com.team500ae.vncapplication.data_access.job.TypeJobInfo;
import com.team500ae.vncapplication.data_access.user.UserInfo;
import com.team500ae.vncapplication.models.BaseReturnFunctionEntity;
import com.team500ae.vncapplication.models.realm_models.job.TypeJobEntity;
import com.team500ae.vncapplication.services.ServiceJob;
import com.team500ae.vncapplication.utils.AppUtils;
import com.team500ae.vncapplication.utils.LanguageUtils;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.realm.Realm;

/**
 * Created by SVIP on 11/30/2017.
 */

public class JobFragment extends Fragment implements ViewPager.OnPageChangeListener {
    Realm realm;
    @BindView(R.id.fragmentJob_flActionButton)
    FloatingActionButton flActionButton;
    private BaseReturnFunctionEntity<ArrayList<TypeJobEntity>> myListType;
    private ArrayList<TypeJobEntity> myType;
    private ArrayList<Fragment> tabFragments;
    private ArrayList<String> listTitle;
    private BaseViewPagerAdapter adapter;
    private TypeJobInfo typeJob;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_job, container, false);
        //alt+insert
        ButterKnife.bind(this, view);
        realm = RealmInfo.getRealm(getContext());

        showFragment();
        //broadcastIntent("test job");
        viewPager.setOffscreenPageLimit(4);
        tabLayout.setupWithViewPager(viewPager);
        adapter = new BaseViewPagerAdapter(getContext(), getChildFragmentManager(), tabFragments, listTitle);
        viewPager.setAdapter(adapter);
        viewPager.addOnPageChangeListener(this);


        return view;
         /* FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                JobAddActivity.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });*/
    }
    public void broadcastIntent(String message) {
        Intent intent = new Intent();
        intent.setAction("com.team500ae.vncapplication.services.NotificationBroadcastReceiver");
        intent.putExtra(Constants.MESSAGE, message);
        getActivity().sendBroadcast(intent);
    }
    public void showFragment() {
        try {
            myType = new ArrayList<>();
            listTitle = new ArrayList<>();
            tabFragments = new ArrayList<>();
            typeJob = new TypeJobInfo();
            myListType = typeJob.getTypeJobHasSelected(getContext(), realm, UserInfo.getCurrentUserId());
            if (myListType.isTrue())
                myType.addAll(myListType.getData());
            if (myType.size() == 0) {
                if (AppUtils.isNetworkAvailable(getActivity()))
                    ServiceJob.startSetDefaultCategory(getContext());
                BaseReturnFunctionEntity<ArrayList<TypeJobEntity>> temp = typeJob.getListTypeJob(getContext(), realm);
                if (temp.isTrue()) {
                    for (int i = 0; i < 3 && i < temp.getData().size(); i++) {
                        myType.add(temp.getData().get(i));
                    }
                }
            }
            TypeJobEntity typeAll = new TypeJobEntity();
            typeAll.setId(-1);
            typeAll.setName(getString(R.string.job_all));
            typeAll.setName2(getString(R.string.job_all));
            myType.add(0, typeAll);

            if (UserInfo.isLogin()) {
                TypeJobEntity bookmark = new TypeJobEntity();
                bookmark.setId(-2);
                bookmark.setName(getString(R.string.has_bookmark));
                bookmark.setName2(getString(R.string.has_bookmark));
                myType.add(0, bookmark);

                TypeJobEntity typeMyJob = new TypeJobEntity();
                typeMyJob.setId(-3);
                typeMyJob.setName(getString(R.string.job_my));
                typeMyJob.setName2(getString(R.string.job_my));
                myType.add(0, typeMyJob);
                flActionButton.setVisibility(View.VISIBLE);
            } else
                flActionButton.setVisibility(View.GONE);

            String appLanguageSetting = LanguageUtils.getAppLanguageSetting(getActivity());
            for (TypeJobEntity type : myType) {
                listTitle.add(appLanguageSetting.equals("en") ? type.getName() : type.getName2());
                tabFragments.add(JobTabFragment.getInstance(type.getId()));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.clear();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @OnClick(R.id.btnSearch)
    void search() {
        SearchJob2Activity.open(getActivity());
//        SearchJobActivity.openChannel(getActivity());
    }

    @OnClick(R.id.btnAddCat)
    void btnAddCatClick() {
        JobMgrCategoryActivity.open(getContext());
    }

    @OnClick(R.id.fragmentJob_flActionButton)
    void fragmentJob_flActionButton() {
        AddNewJobActivity.open(getActivity());
//        JobAddActivity.openChannel(getActivity());
    }

    @Override
    public void onDestroy() {
        RealmInfo.closeRealm(realm);
        super.onDestroy();
    }

    @BindView(R.id.viewPager)
    ViewPager viewPager;
    @BindView(R.id.tabs)
    TabLayout tabLayout;

    public static JobFragment getInstants() {
        JobFragment fragment = new JobFragment();
        return fragment;
    }

    public void notifyOnChangeTypeJob() {
        tabLayout.removeAllTabs();
        showFragment();
    }


    public void notifyBookmarkChange() {
        if (myType != null && myType.size() > 0 && myType.get(1).getId() == -2) {
            ((JobTabFragment) tabFragments.get(1)).getData();
        }
    }

    public void notifyAddChange() {
        if (myType != null && myType.size() > 0 && myType.get(0).getId() == -3) {
            ((JobTabFragment) tabFragments.get(0)).getData();
        }
    }


    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {

        if (position == 0) {
            if (UserInfo.isLogin())
                flActionButton.setVisibility(View.VISIBLE);
        } else {
            flActionButton.setVisibility(View.GONE);
        }
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }
}
