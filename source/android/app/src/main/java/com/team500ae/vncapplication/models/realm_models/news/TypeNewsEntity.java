package com.team500ae.vncapplication.models.realm_models.news;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.Required;

/**
 * Created by MaiNam on 11/15/2017.
 */

public class TypeNewsEntity extends RealmObject {
    @PrimaryKey
    int id;

    @Required
    String name;

    String name2;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name != null ? name : "";
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName2() {
        return name2 != null ? name2 : "";
    }

    public void setName2(String name2) {
        this.name2 = name2;
    }
}
