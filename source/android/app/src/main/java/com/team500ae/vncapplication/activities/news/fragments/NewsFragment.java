package com.team500ae.vncapplication.activities.news.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;

import com.team500ae.library.adapters.BaseViewPagerAdapter;
import com.team500ae.vncapplication.R;
import com.team500ae.vncapplication.activities.news.MgrCategoryActivity;
import com.team500ae.vncapplication.activities.news.SearchNewsActivity;
import com.team500ae.vncapplication.data_access.RealmInfo;
import com.team500ae.vncapplication.data_access.news.TypeNewsInfo;
import com.team500ae.vncapplication.data_access.user.UserInfo;
import com.team500ae.vncapplication.models.BaseReturnFunctionEntity;
import com.team500ae.vncapplication.models.realm_models.news.TypeNewsEntity;
import com.team500ae.vncapplication.services.ServiceNews;
import com.team500ae.vncapplication.utils.AppUtils;
import com.team500ae.vncapplication.utils.LanguageUtils;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.realm.Realm;

/**
 * Created by maina on 11/20/2017.
 */


public class NewsFragment extends Fragment {
    Realm realm;
    private BaseReturnFunctionEntity<ArrayList<TypeNewsEntity>> myListTypeNews;
    private ArrayList<TypeNewsEntity> myTypeNews;
    private ArrayList<Fragment> newsTabFragments;
    private ArrayList<String> listTitle;
    private BaseViewPagerAdapter adapter;
    private TypeNewsInfo typeNews;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_news, container, false);
        ButterKnife.bind(this, view);
        realm = RealmInfo.getRealm(getContext());
        myTypeNews = new ArrayList<>();
        listTitle = new ArrayList<>();
        newsTabFragments = new ArrayList<>();
        showFragment();
        tabLayout.setupWithViewPager(viewPager);
        viewPager.setOffscreenPageLimit(4);
        adapter = new BaseViewPagerAdapter(getContext(), getChildFragmentManager(), newsTabFragments, listTitle);
        viewPager.setAdapter(adapter);
        return view;
    }

    public void showFragment() {
        try {
            typeNews = new TypeNewsInfo();
            myListTypeNews = typeNews.getTypeNewsHasSelected(getContext(), realm, UserInfo.getCurrentUserId());
            if (myListTypeNews.isTrue())
                myTypeNews.addAll(myListTypeNews.getData());
            if (myTypeNews.size() == 0) {
                if (AppUtils.isNetworkAvailable(getActivity())) {
                    ServiceNews.startSetDefaultCategory(getContext());
                }
                BaseReturnFunctionEntity<ArrayList<TypeNewsEntity>> temp = typeNews.getListTypeNews(getContext(), realm);
                if (temp.isTrue()) {
                    for (int i = 0; i < 5 && i < temp.getData().size(); i++) {
                        myTypeNews.add(temp.getData().get(i));
                    }
                }
            }

            //add 'ALL' category. ID = -2 => API will get all tips when type category is -2
            TypeNewsEntity allCategory = new TypeNewsEntity();
            allCategory.setId(-2);
            allCategory.setName(getString(R.string.category_all));
            allCategory.setName2(getString(R.string.category_all));
            myTypeNews.add(0, allCategory);

            if (UserInfo.isLogin()) {
                TypeNewsEntity bookmark = new TypeNewsEntity();
                bookmark.setId(-1);
                bookmark.setName(getString(R.string.has_bookmark));
                bookmark.setName2(getString(R.string.has_bookmark));
                myTypeNews.add(0, bookmark);
            }
            String appLanguageSetting = LanguageUtils.getAppLanguageSetting(getActivity());
            for (TypeNewsEntity typeNews : myTypeNews) {
                listTitle.add(appLanguageSetting.equals("en") ? typeNews.getName() : typeNews.getName2());
                newsTabFragments.add(NewsTabFragment.getInstance(typeNews.getId()));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.clear();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @OnClick(R.id.btnSearch)
    void search() {
        SearchNewsActivity.open(getActivity());
    }

    @OnClick(R.id.btnAddCat)
    void btnAddCatClick() {
        MgrCategoryActivity.open(getContext());
    }

    @Override
    public void onDestroy() {
        RealmInfo.closeRealm(realm);
        super.onDestroy();
    }

    @BindView(R.id.viewPager)
    ViewPager viewPager;
    @BindView(R.id.tabs)
    TabLayout tabLayout;

    public static NewsFragment getInstants() {
        NewsFragment newsFragment = new NewsFragment();
        return newsFragment;
    }

    public void notifyBookmarkChange() {
        if (myTypeNews != null && myTypeNews.size() > 0 && myTypeNews.get(0).getId() == -1) {
            ((NewsTabFragment) newsTabFragments.get(0)).getData();
        }
    }
}
