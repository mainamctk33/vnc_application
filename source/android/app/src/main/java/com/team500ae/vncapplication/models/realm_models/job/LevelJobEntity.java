package com.team500ae.vncapplication.models.realm_models.job;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by SVIP on 12/18/2017.
 */

public class LevelJobEntity extends RealmObject {
    @PrimaryKey
    private int id;
    private String name;
    private String name2;
    public int getId(){
        return  id;
    }
    public void setId(int id){
        this.id = id;
    }
    public String getName(){
        return name;
    }
    public void setName(String name){
        this.name = name;
    }

    public String getName2() {
        return name2 != null ? name2 : "";
    }

    public void setName2(String name2) {
        this.name2 = name2;
    }
}
