package com.team500ae.vncapplication.data_access.file;

import android.content.Context;

import com.team500ae.vncapplication.data_access.BookmarkInfo;
import com.team500ae.vncapplication.models.BaseReturnFunctionEntity;
import com.team500ae.vncapplication.models.realm_models.BookmarkEntity;
import com.team500ae.vncapplication.models.realm_models.file.FileEntity;

import java.util.Date;

import io.realm.Realm;
import io.realm.exceptions.RealmError;

public class FileInfo {
    public boolean isDownloaded(Realm realm, String url) {
        try {
            FileEntity fileEntity = realm.where(FileEntity.class).equalTo("url", url).findFirst();
            if (fileEntity == null)
                return false;
            if (fileEntity.isFinished())
                return true;
            if (new Date().getTime() - fileEntity.getDownloadDate() < 1000 * 60 * 10)
                return true;
            return false;
        } catch (Exception e) {
            return false;
        } catch (RealmError e) {
            return false;
        } catch (OutOfMemoryError e) {
            return false;
        }
    }

    public BaseReturnFunctionEntity<Boolean> addFile(Realm realm, String url, boolean finish,long id) {
        try {
            FileEntity fileEntity = realm.where(FileEntity.class).equalTo("url", url).or().equalTo("id",id).findFirst();
            if (fileEntity != null) {
                realm.executeTransaction((realm2) -> {
                    fileEntity.setDownloadDate(new Date().getTime());
                    fileEntity.setFinished(finish);
                });
            }
            else {
                realm.executeTransaction((realm2) -> {
                    FileEntity  fileEntity2= new FileEntity();
                    fileEntity2.setUrl(url);
                    fileEntity2.setDownloadDate(new Date().getTime());
                    fileEntity2.setFinished(finish);
                    fileEntity2.setId(id);
                    realm2.copyToRealm(fileEntity2);
                });
            }
            return new BaseReturnFunctionEntity<>(BaseReturnFunctionEntity.StatusFunction.TRUE, true);
        } catch (Exception e) {
            return new BaseReturnFunctionEntity<>(BaseReturnFunctionEntity.StatusFunction.EXCEPTION, e.getMessage());
        } catch (RealmError e) {
            return new BaseReturnFunctionEntity<>(BaseReturnFunctionEntity.StatusFunction.EXCEPTION, e.getMessage());
        } catch (OutOfMemoryError e) {
            return new BaseReturnFunctionEntity<>(BaseReturnFunctionEntity.StatusFunction.EXCEPTION, e.getMessage());
        }
    }
}
