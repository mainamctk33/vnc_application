package com.team500ae.vncapplication.services;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.provider.Settings;
import android.support.annotation.Nullable;

import com.google.firebase.messaging.FirebaseMessaging;
import com.team500ae.vncapplication.data_access.notification.NotificationInfo;
import com.team500ae.vncapplication.data_access.tips.TipsInfo;
import com.team500ae.vncapplication.data_access.user.UserInfo;

public class ServiceNotification extends IntentService {
    private static final String ACTION_REGISTER_TOKEN = "ACTION_REGISTER_TOKEN";
    private static final String ACTION_REMOVE_DEVICE = "ACTION_REMOVE_DEVICE";
    private static final String ACTION_REGISTER_TOPIC = "ACTION_REGISTER_TOPIC";

    public ServiceNotification() {
        super("ServiceNotification");
    }

    public static void startRemoveToken(Context context, String token) {
        try {
            String deviceId = Settings.Secure.getString(context.getContentResolver(),
                    Settings.Secure.ANDROID_ID);
            Intent intent = new Intent(context, ServiceNotification.class);
            intent.setAction(ACTION_REMOVE_DEVICE);
            intent.putExtra("DEVICE_ID", deviceId);
            context.startService(intent);

        }catch (Exception e)
        {

        }

    }

    public static void startRegisterToken(Context context, String token) {
        try {
            if (UserInfo.isLogin()) {
                String deviceId = Settings.Secure.getString(context.getContentResolver(),
                        Settings.Secure.ANDROID_ID);
                Intent intent = new Intent(context, ServiceNotification.class);
                intent.setAction(ACTION_REGISTER_TOKEN);
                intent.putExtra("TOKEN", token);
                intent.putExtra("DEVICE_ID", deviceId);
                intent.putExtra("USERNAME", UserInfo.getCurrentUserId());
                context.startService(intent);
            }

        }catch (Exception e)
        {
            e.printStackTrace();
        }
    }
    public static void startRegisterTopic(Context context) {
        try {
            Intent intent = new Intent(context, ServiceNotification.class);
            intent.setAction(ACTION_REGISTER_TOPIC);
            context.startService(intent);

        }catch (Exception e)
        {
            e.printStackTrace();
        }
    }
    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        String action = intent.getAction();
        String device_id = intent.getStringExtra("DEVICE_ID");
        switch (action) {
            case ACTION_REGISTER_TOKEN:
                String token = intent.getStringExtra("TOKEN");
                String userName = intent.getStringExtra("USERNAME");
                handleRegisterToken(userName, device_id, token);
                break;
            case ACTION_REMOVE_DEVICE:
                handleRemoveDevice(device_id);
                break;
            case ACTION_REGISTER_TOPIC:
                handleRegisterTopic();
                break;
        }

    }

    private void handleRegisterTopic() {
        new Thread(()->{
            try {
                Thread.sleep(3000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            FirebaseMessaging.getInstance().subscribeToTopic("news");
            FirebaseMessaging.getInstance().subscribeToTopic("job");
            FirebaseMessaging.getInstance().subscribeToTopic("tip");
        }).start();
    }

    private void handleRemoveDevice(String device_id) {
        new Thread(() -> {
            new NotificationInfo().removeDeviceToken(getApplicationContext(), device_id);

        }).start();
    }

    private void handleRegisterToken(String userName, String device_id, String token) {
        new Thread(() -> {
            new NotificationInfo().registerToken(getApplicationContext(), userName, device_id, token);

        }).start();
    }


}
