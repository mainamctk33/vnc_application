package com.team500ae.vncapplication.utils;

import android.content.Context;
import android.util.Log;

import com.team500ae.library.utils.ConvertUtils;
import com.team500ae.vncapplication.R;

import java.util.Calendar;
import java.util.Date;

/**
 * Created by maina on 11/24/2017.
 */

public class TimeUtils {
    public static int getAge(long timeMilisecond) {
        Date date = new Date(timeMilisecond);
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        int year = calendar.get(Calendar.YEAR);
        calendar.setTime(new Date());
        year = calendar.get(Calendar.YEAR) - year;
        if (year < 0)
            year = 1;
        return year;
    }

    public static String toSocialTime(Context context, long timeMilisecond) {
        long ONE_MINUTE = 60 * 1000;
        long ONE_HOUR = ONE_MINUTE * 60;
        long ONE_DAY = ONE_HOUR * 24;
        long TEN_DAY = ONE_DAY * 10;

        long totalTimeDiff = Calendar.getInstance().getTimeInMillis() - timeMilisecond;

        String time = "";
        if (totalTimeDiff > TEN_DAY) {
            return ConvertUtils.toDateString(timeMilisecond, "dd/MM/yyyy");
        }

        if (totalTimeDiff > ONE_DAY) {
            int day = (int) (totalTimeDiff / ONE_DAY);
            time = day + " " + context.getResources().getString(R.string.day);
        } else if (totalTimeDiff > ONE_HOUR) {
            int hour = (int) (totalTimeDiff / ONE_HOUR);
            time = hour + " " + context.getResources().getString(R.string.hour);
        } else if (totalTimeDiff > ONE_MINUTE) {
            int minute = (int) (totalTimeDiff / ONE_MINUTE);
            time = minute + " " + context.getResources().getString(R.string.minute);
        } else time = context.getResources().getString(R.string.just_now);
        return time;
    }
}
