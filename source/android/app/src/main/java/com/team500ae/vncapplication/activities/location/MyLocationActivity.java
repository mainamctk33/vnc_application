package com.team500ae.vncapplication.activities.location;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v4.widget.SwipeRefreshLayout;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.team500ae.library.adapters.BaseRecyclerAdapter;
import com.team500ae.library.layoutmanager.MyLinearLayoutManager;
import com.team500ae.library.utils.ConvertUtils;
import com.team500ae.library.utils.ImageUtils;
import com.team500ae.library.utils.RecyclerViewUtils;
import com.team500ae.library.utils.StringUtils;
import com.team500ae.vncapplication.R;
import com.team500ae.vncapplication.base.BaseActivity;
import com.team500ae.vncapplication.data_access.location.LocationInfo;
import com.team500ae.vncapplication.data_access.user.UserInfo;
import com.team500ae.vncapplication.models.BaseReturnFunctionEntity;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MyLocationActivity extends BaseActivity implements Filterable {

    private ArrayList<JsonObject> listData;
    private BaseRecyclerAdapter<ViewHolder, JsonObject> adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        listData = new ArrayList<>();
        setContentView(R.layout.activity_my_location);
        super.onCreate(savedInstanceState);
        getSupportActionBar().setTitle(R.string.my_location);

        adapter = new BaseRecyclerAdapter<ViewHolder, JsonObject>(R.layout.item_my_location, new BaseRecyclerAdapter.BaseViewHolder<ViewHolder, JsonObject>() {
            @Override
            public ViewHolder getViewHolder(View v) {
                return new ViewHolder(v);
            }

            @Override
            public void bindData(RecyclerView recyclerView, ViewHolder viewHolder, JsonObject data, int position) {
                viewHolder.tvName.setText(ConvertUtils.toString(data.get("Title")));
                ImageUtils.loadImageByGlide(getActivity(), false, 0, 0, ConvertUtils.toString(data.get("TypeLogo")), R.drawable.ic_location_address, R.drawable.ic_location_address, viewHolder.imgLogo, true);
                viewHolder.tvType.setText(ConvertUtils.toString(data.get("TypeName")));
                String address = ConvertUtils.toString(data.get("Address"));
                viewHolder.tvAddress.setText(address);
                if (StringUtils.isEmpty(address))
                    viewHolder.tvAddress.setVisibility(View.GONE);
                else
                    viewHolder.tvAddress.setVisibility(View.VISIBLE);
                viewHolder.tvCreatedDate.setText(ConvertUtils.toDateString(ConvertUtils.toLong(data.get("CreatedDate")), "dd/MM/yyyy"));
                viewHolder.imgStatus.setImageResource(StringUtils.isEmpty(ConvertUtils.toString(data.get("CreatedDate"))) ? R.drawable.ic_status_0 : R.drawable.ic_status_1);
            }
        }, listData, new BaseRecyclerAdapter.OnClickListener() {
            @Override
            public void onClick(View v, int position, Object o) {
                DetailLocationActivity.open(getActivity(), (JsonObject) o);
            }
        });

        recyclerView.setLayoutManager(new MyLinearLayoutManager(getActivity()));
        adapter.bindData(recyclerView);
        //ho tro load next page
        RecyclerViewUtils recyclerViewUtils = new RecyclerViewUtils(new RecyclerViewUtils.RecyclerViewUtilsListener() {
            @Override
            public void onScrollToEnd() {
                if (!isLoading) {
                    page = page + 1;
                    getData(page, size, txtSearch.getText().toString());
                }
            }
        });
        recyclerViewUtils.setUp(recyclerView);

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                page = 1;
                getData(page, size, txtSearch.getText().toString());
            }
        });
        getData(page, size, txtSearch.getText().toString());
        txtSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (!s.toString().equals("")) {
                    mClear.setVisibility(View.VISIBLE);
                } else {
                    mClear.setVisibility(View.GONE);
                }
                getFilter().filter(s.toString());
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        showBackButton();

    }

    @BindView(R.id.swipeRefreshLayout)
    SwipeRefreshLayout swipeRefreshLayout;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.loading)
    View loading;
    int page = 1;
    int size = 10;
    boolean isLoading = false;
    @BindView(R.id.txtSearch)
    EditText txtSearch;

    @BindView(R.id.btnClear)
    View mClear;

    @OnClick(R.id.btnClear)
    void clear() {
        txtSearch.setText("");
    }

    public void getData() {
        page = 1;
        getData(page, size, txtSearch.getText().toString());
    }

    @OnClick(R.id.btnAddLocation)
    void addLocation() {
        if (UserInfo.isLogin()) {
//            AddLocationActivity.openChannel(getActivity());
            AddNewLocationActivity.open(getActivity());
        }
    }

    public void getData(final int page, final int size, String keyword) {
        new AsyncTask<Void, JsonArray, Void>() {

            @Override
            protected void onPreExecute() {
                swipeRefreshLayout.setEnabled(false);
                if (page == 1)
                    swipeRefreshLayout.setRefreshing(true);
                else
                    loading.setVisibility(View.VISIBLE);
                isLoading = true;
                super.onPreExecute();
            }

            @Override
            protected Void doInBackground(Void... voids) {
                BaseReturnFunctionEntity<JsonArray> result = new LocationInfo().searchMyLocation(page, size, keyword);
                if (result.isTrue())
                    publishProgress(result.getData());
                else
                    publishProgress(new JsonArray());
                return null;
            }

            @Override
            protected void onProgressUpdate(JsonArray... values) {
                if (values.length > 0 && values[0] != null) {
                    if (page == 1) {
                        listData.clear();
                        adapter.notifyDataSetChanged();
                    }
                    bindData(values[0]);
                }
                super.onProgressUpdate(values);
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                swipeRefreshLayout.setEnabled(true);
                if (page == 1)
                    swipeRefreshLayout.setRefreshing(false);
                else
                    loading.setVisibility(View.GONE);
                isLoading = false;
                super.onPostExecute(aVoid);
            }
        }.execute();

    }

    void bindData(JsonArray jsonArray) {
        for (int i = 0; i < jsonArray.size(); i++) {
            JsonObject jsonObject = ConvertUtils.toJsonObject(jsonArray.get(i));
            listData.add(jsonObject);
            adapter.notifyItemInserted(listData.size() - 1);
        }
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tvName)
        TextView tvName;
        @BindView(R.id.imgLogo)
        ImageView imgLogo;
        @BindView(R.id.tvAddress)
        TextView tvAddress;
        @BindView(R.id.tvType)
        TextView tvType;
        @BindView(R.id.tvCreatedDate)
        TextView tvCreatedDate;
        @BindView(R.id.imgStatus)
        ImageView imgStatus;

        public ViewHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);
        }
    }

    public static void open(Context context) {
        Intent intent = new Intent(context, MyLocationActivity.class);
        context.startActivity(intent);
    }

    private ArrayList<JsonObject> getAutocomplete(CharSequence constraint) {
        page = 1;
        BaseReturnFunctionEntity<JsonArray> result = new LocationInfo().searchMyLocation(page, size, constraint.toString());
        if (result.isTrue())
            return ConvertUtils.toArrayList(result.getData());
        return new ArrayList<>();
    }

    @Override
    public Filter getFilter() {
        Filter filter = new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                FilterResults results = new FilterResults();
                // Skip the autocomplete query if no constraints are given.
                if (constraint != null) {
                    // Query the autocomplete API for the (constraint) search string.
                    ArrayList<JsonObject> mResultList = getAutocomplete(constraint);
                    if (mResultList != null) {
                        // The API successfully returned results.
                        results.values = mResultList;
                        results.count = mResultList.size();
                    }
                }
                return results;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                listData.clear();
                if (results != null && results.count > 0) {
                    listData.addAll((ArrayList<JsonObject>) results.values);
                    // The API returned at least one result, update the data.
                }
                adapter.notifyDataSetChanged();
            }
        };
        return filter;
    }
}
