package com.team500ae.vncapplication.activities.translator.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;

import com.team500ae.library.adapters.BaseViewPagerAdapter;
import com.team500ae.vncapplication.R;
import com.team500ae.vncapplication.data_access.RealmInfo;
import com.team500ae.vncapplication.data_access.translator.CategoryTranslatorInfo;
import com.team500ae.vncapplication.models.BaseReturnFunctionEntity;
import com.team500ae.vncapplication.models.realm_models.translator.CategoryTranslatorEntity;
import com.team500ae.vncapplication.utils.LanguageUtils;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.realm.Realm;

/**
 * Created by TamPC on 3/24/2018.
 */

public class TranslatorFragment extends Fragment {
Realm realm;
private BaseReturnFunctionEntity<ArrayList<CategoryTranslatorEntity>> myList;
private ArrayList<CategoryTranslatorEntity> myCategory;
private ArrayList<Fragment> listTabFragments;
private  ArrayList<String> listTitle;
private BaseViewPagerAdapter adapter;
private CategoryTranslatorInfo categoryTranslator;

    @BindView(R.id.viewPager)
    ViewPager viewPager;
    @BindView(R.id.tabs)
    TabLayout tabLayout;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
    View view = inflater.inflate(R.layout.fragment_translator, container,false);
        ButterKnife.bind(this,view);
        realm = RealmInfo.getRealm(getContext());

        myCategory = new ArrayList<>();
        listTitle= new ArrayList<>();
        listTabFragments = new ArrayList<>();
        showFragment();
        tabLayout.setupWithViewPager(viewPager);
        viewPager.setOffscreenPageLimit(4);

        adapter = new BaseViewPagerAdapter(getContext(), getChildFragmentManager(),listTabFragments,listTitle );
        viewPager.setAdapter(adapter);
        //return super.onCreateView(inflater, container, savedInstanceState);
        return view;
    }
    public void showFragment(){
        try{
            categoryTranslator = new CategoryTranslatorInfo();
            myList = categoryTranslator.getAll(realm);
            if(myList.isTrue())
                myCategory.addAll(myList.getData());
//            if (UserInfo.isLogin()) {
//                CategoryTranslatorEntity myTranslator = new CategoryTranslatorEntity();
//                myTranslator.setId(-1);
//                myTranslator.setName(getString(R.string.translator_my));
//                myCategory.add(0, myTranslator);
//            }
            listTitle.add(getString(R.string.favourite));
            listTabFragments.add(new TranslatorMyFavouriteFragment());
            String appLanguageSetting = LanguageUtils.getAppLanguageSetting(getActivity());
            for(CategoryTranslatorEntity c : myCategory){
                listTitle.add(appLanguageSetting.equals("en") ? c.getName() : c.getName2());
                listTabFragments.add(TranslatorTabFragment.getInstance(c.getId()));
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }
    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.clear();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDestroy() {
        RealmInfo.closeRealm(realm);
        super.onDestroy();
    }
    public static TranslatorFragment getInstants() {
        TranslatorFragment fragment = new TranslatorFragment();
        return fragment;
    }
    @OnClick(R.id.btnAddCat)
    void btnAddCatClick() {
        //MgrCategoryActivity.openChannel(getContext());
    }

    @OnClick(R.id.btnSearch)
    void btnSearchClick() {
        //SearchTipsActivity.openChannel(getActivity());
    }

    public void notifyOnChangeTypeTips() {
        tabLayout.removeAllTabs();
        showFragment();
    }

    public void notifyBookmarkChange() {
        if (myCategory != null && myCategory.size() > 0 && myCategory.get(0).getId() == -1) {
            ((TranslatorTabFragment) listTabFragments.get(0)).getData();
        }
    }
}
