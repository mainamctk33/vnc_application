package com.team500ae.vncapplication.models;

/**
 * Created by MaiNam on 11/15/2017.
 */

public class BaseReturnFunctionEntity<T> {

    public StatusFunction status;
    public T data;
    public int total;
    public String message;

    public BaseReturnFunctionEntity()
    {
        status = StatusFunction.TRUE;
        total = 0;
    }
    public BaseReturnFunctionEntity(StatusFunction status, String message, int total)
    {
        this.total = total;
        this.status = status;
        this.message = message;
    }
    public BaseReturnFunctionEntity(StatusFunction status, String message, T data)
    {
        this(status, message, 0);
        this.data = data;
    }
    public BaseReturnFunctionEntity(StatusFunction status, String message, T data, int total)
    {
        this(status, message, total);
        this.data = data;
    }
    public BaseReturnFunctionEntity(StatusFunction status, T data)
    {
        this(status, "", data);
    }
    public BaseReturnFunctionEntity(StatusFunction status, String message)
    {
        this(status,message,null);
    }
    public BaseReturnFunctionEntity(StatusFunction status)
    {
        this(status,null,null);
    }

    public BaseReturnFunctionEntity(StatusFunction status, T data, int total)
    {
        this(status, "", data, total);
    }

    public boolean isTrue()
    {
        return status == StatusFunction.TRUE;
    }

    public enum StatusFunction
    {
        TRUE,
        FALSE,
        EXCEPTION
    }

    public StatusFunction getStatus() {
        return status;
    }

    public void setStatus(StatusFunction status) {
        this.status = status;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
