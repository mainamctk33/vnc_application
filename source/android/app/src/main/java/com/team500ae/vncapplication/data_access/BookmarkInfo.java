package com.team500ae.vncapplication.data_access;

import android.content.Context;

import com.team500ae.vncapplication.models.BaseReturnFunctionEntity;
import com.team500ae.vncapplication.models.realm_models.BookmarkEntity;

import io.realm.Realm;
import io.realm.exceptions.RealmError;

/**
 * Created by maina on 11/24/2017.
 */

public class BookmarkInfo {

    public enum BookmarkType {
        News(0),
        Tip(1),
        Job(2),
        Location(3),
        Translator(4);
        int value;

        public int getValue() {
            return value;
        }

        BookmarkType(int type) {
            this.value = type;
        }
    }

    public BaseReturnFunctionEntity<Boolean> bookmark(Context context, Realm realm, int valueId, String userId, BookmarkType type, boolean bookmark, String obj) {
        try {
            realm.executeTransaction(realm1 -> {
                String bookmarkId = valueId + userId + type.getValue();
                BookmarkEntity bookmarkEntity = new BookmarkEntity();
                bookmarkEntity.setBookmark(bookmark);
                bookmarkEntity.setBookmarkType(type.getValue());
                bookmarkEntity.setBookmarkId(bookmarkId);
                bookmarkEntity.setId(valueId);
                bookmarkEntity.setUserId(userId);
                realm.copyToRealmOrUpdate(bookmarkEntity);
            });
            return new BaseReturnFunctionEntity<>(BaseReturnFunctionEntity.StatusFunction.TRUE, true);
        } catch (Exception e) {
            return new BaseReturnFunctionEntity<>(BaseReturnFunctionEntity.StatusFunction.EXCEPTION, e.getMessage());
        } catch (RealmError e) {
            return new BaseReturnFunctionEntity<>(BaseReturnFunctionEntity.StatusFunction.EXCEPTION, e.getMessage());
        } catch (OutOfMemoryError e) {
            return new BaseReturnFunctionEntity<>(BaseReturnFunctionEntity.StatusFunction.EXCEPTION, e.getMessage());
        }
    }

    public BaseReturnFunctionEntity<Boolean> bookmark(Context context, Realm realm, int valueId, String userId, BookmarkType type, boolean bookmark) {
        return bookmark(context, realm, valueId, userId, type, bookmark, "");
    }

    public BaseReturnFunctionEntity<Boolean> isBookmark(Context context, Realm realm, int valueId, String userId, BookmarkType type) {
        try {
            BaseReturnFunctionEntity<BookmarkEntity> result = getBookmark(realm, valueId, userId, type);
            if (result.isTrue())
                return new BaseReturnFunctionEntity<Boolean>(BaseReturnFunctionEntity.StatusFunction.TRUE, result.getData().isBookmark());
            if (result.getStatus() == BaseReturnFunctionEntity.StatusFunction.FALSE)
                return new BaseReturnFunctionEntity<Boolean>(BaseReturnFunctionEntity.StatusFunction.TRUE, false);
            return new BaseReturnFunctionEntity<>(BaseReturnFunctionEntity.StatusFunction.FALSE, false);
        } catch (Exception e) {
            return new BaseReturnFunctionEntity<>(BaseReturnFunctionEntity.StatusFunction.EXCEPTION, e.getMessage());
        } catch (RealmError e) {
            return new BaseReturnFunctionEntity<>(BaseReturnFunctionEntity.StatusFunction.EXCEPTION, e.getMessage());
        } catch (OutOfMemoryError e) {
            return new BaseReturnFunctionEntity<>(BaseReturnFunctionEntity.StatusFunction.EXCEPTION, e.getMessage());
        }
    }

    public BaseReturnFunctionEntity<BookmarkEntity> getBookmark(Realm realm, int valueId, String userId, BookmarkType type) {
        try {
            String bookmarkId = valueId + userId + type.getValue();
            BookmarkEntity bookmarkEntity = realm.where(BookmarkEntity.class).equalTo("bookmarkId", bookmarkId).findFirst();
            if (bookmarkEntity != null)
                return new BaseReturnFunctionEntity<>(BaseReturnFunctionEntity.StatusFunction.TRUE, bookmarkEntity);
        } catch (RealmError e) {
            e.printStackTrace();
            return new BaseReturnFunctionEntity<BookmarkEntity>(BaseReturnFunctionEntity.StatusFunction.EXCEPTION, e.getMessage());
        } catch (OutOfMemoryError e) {
            e.printStackTrace();
            return new BaseReturnFunctionEntity<BookmarkEntity>(BaseReturnFunctionEntity.StatusFunction.EXCEPTION, e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            return new BaseReturnFunctionEntity<BookmarkEntity>(BaseReturnFunctionEntity.StatusFunction.EXCEPTION, e.getMessage());
        }
        return new BaseReturnFunctionEntity<BookmarkEntity>(BaseReturnFunctionEntity.StatusFunction.FALSE);
    }
}
