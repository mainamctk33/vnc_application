package com.team500ae.vncapplication.data_access.translator;

import android.content.Context;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.team500ae.library.utils.ConvertUtils;
import com.team500ae.vncapplication.constants.Constants;
import com.team500ae.vncapplication.data_access.ClientInfo;
import com.team500ae.vncapplication.models.BaseReturnFunctionEntity;
import com.team500ae.vncapplication.models.ServiceResponseEntity;
import com.team500ae.vncapplication.models.realm_models.translator.TypeTranslatorEntity;

import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.util.ArrayList;

import io.realm.Realm;
import io.realm.RealmResults;
import io.realm.exceptions.RealmError;

/**
 * Created by TamPC on 3/24/2018.
 */

public class TypeTranslatorInfo{
    public ServiceResponseEntity<JsonArray> sync(long lastSyncDate) {
        try {
            ClientInfo.DataResponse dataResponse = new ClientInfo().requestApi(ClientInfo.MethodType.GET, Constants.API_TYPE_TRANSLATOR_SYNC + "/" + lastSyncDate);
            if (dataResponse.isOK()) {
                ServiceResponseEntity<JsonArray> result = dataResponse.getData(new TypeToken<ServiceResponseEntity<JsonArray>>() {
                }.getType());
                return result;
            }
        } catch (UnknownHostException e) {
            e.printStackTrace();
        } catch (SocketTimeoutException e) {
            e.printStackTrace();
        }
        return null;
    }
    public ServiceResponseEntity<JsonArray> getAll( int page, int size, int type) {
        try {
            ClientInfo.DataResponse dataResponse = new ClientInfo().requestApi(ClientInfo.MethodType.GET, Constants.API_TYPE_TRANSLATOR_GETALL + "?type=" + type);
            if (dataResponse.isOK()) {
                ServiceResponseEntity<JsonArray> result = dataResponse.getData(new TypeToken<ServiceResponseEntity<JsonArray>>() {
                }.getType());
                return result;
            }
        } catch (UnknownHostException e) {
            e.printStackTrace();
        } catch (SocketTimeoutException e) {
            e.printStackTrace();
        }
        return null;
    }
    public ServiceResponseEntity<JsonArray> getAll() {
        try {
            ClientInfo.DataResponse dataResponse = new ClientInfo().requestApi(ClientInfo.MethodType.GET, Constants.API_TYPE_TRANSLATOR_GETALL+"?size=99999999");
            if (dataResponse.isOK()) {
                ServiceResponseEntity<JsonArray> result = dataResponse.getData(new TypeToken<ServiceResponseEntity<JsonArray>>() {
                }.getType());
                return result;
            }
        } catch (UnknownHostException e) {
            e.printStackTrace();
        } catch (SocketTimeoutException e) {
            e.printStackTrace();
        }
        return null;
    }
    public BaseReturnFunctionEntity<ArrayList<TypeTranslatorEntity>> getAll(Realm realm) {
        try {
            RealmResults<TypeTranslatorEntity> realmList = realm.where(TypeTranslatorEntity.class).findAll();
            ArrayList<TypeTranslatorEntity> result = new ArrayList<>();
            result.addAll(realmList);
            return new BaseReturnFunctionEntity<>(BaseReturnFunctionEntity.StatusFunction.TRUE, result);
        } catch (RealmError e) {
            return new BaseReturnFunctionEntity<>(BaseReturnFunctionEntity.StatusFunction.EXCEPTION, e.getMessage());
        } catch (Exception e) {
            return new BaseReturnFunctionEntity<>(BaseReturnFunctionEntity.StatusFunction.EXCEPTION, e.getMessage());
        } catch (OutOfMemoryError e) {
            return new BaseReturnFunctionEntity<>(BaseReturnFunctionEntity.StatusFunction.EXCEPTION, e.getMessage());
        }
    }
    public BaseReturnFunctionEntity<ArrayList<TypeTranslatorEntity>> getAllByCategory(Realm realm, int categoryID) {
        try {
            RealmResults<TypeTranslatorEntity> realmList = realm.where(TypeTranslatorEntity.class).findAll();
            ArrayList<TypeTranslatorEntity> result = new ArrayList<>();
            for (TypeTranslatorEntity t : realmList)
            {
                if(t.getCategory() == categoryID)
                    //result.addAll(realmList);
                    result.add(t);
            }

            return new BaseReturnFunctionEntity<>(BaseReturnFunctionEntity.StatusFunction.TRUE, result);
        } catch (RealmError e) {
            return new BaseReturnFunctionEntity<>(BaseReturnFunctionEntity.StatusFunction.EXCEPTION, e.getMessage());
        } catch (Exception e) {
            return new BaseReturnFunctionEntity<>(BaseReturnFunctionEntity.StatusFunction.EXCEPTION, e.getMessage());
        } catch (OutOfMemoryError e) {
            return new BaseReturnFunctionEntity<>(BaseReturnFunctionEntity.StatusFunction.EXCEPTION, e.getMessage());
        }
    }
    public BaseReturnFunctionEntity<Boolean> update(Realm realm, JsonArray data) {
        try {
            realm.executeTransaction(realm1 -> {
                for (int i = 0; i < data.size(); i++) {
                    JsonObject jsonObject = ConvertUtils.toJsonObject(data.get(i));
                    update(realm, jsonObject, false);
                }
            });
            return new BaseReturnFunctionEntity<>(BaseReturnFunctionEntity.StatusFunction.TRUE, true);
        } catch (RealmError e) {
            return new BaseReturnFunctionEntity<>(BaseReturnFunctionEntity.StatusFunction.EXCEPTION, e.getMessage());
        } catch (Exception e) {
            return new BaseReturnFunctionEntity<>(BaseReturnFunctionEntity.StatusFunction.EXCEPTION, e.getMessage());
        } catch (OutOfMemoryError e) {
            return new BaseReturnFunctionEntity<>(BaseReturnFunctionEntity.StatusFunction.EXCEPTION, e.getMessage());
        }
    }
    public BaseReturnFunctionEntity<Boolean> update(Realm realm, final JsonObject data, boolean needTransacsion) {
        try {
            if (needTransacsion) {
                realm.executeTransaction(realm1 -> {
                    update(realm, data, false);
                });
            } else {
                int id = ConvertUtils.toInt(data.get("ID"));
                TypeTranslatorEntity type = new TypeTranslatorEntity();
                type.setName(ConvertUtils.toString(data.get("Name")));
                type.setId(id);
                type.setCategory(ConvertUtils.toInt(data.get("Name")));
                realm.copyToRealmOrUpdate(type);
            }
            return new BaseReturnFunctionEntity<>(BaseReturnFunctionEntity.StatusFunction.EXCEPTION, true);
        } catch (RealmError e) {
            return new BaseReturnFunctionEntity<>(BaseReturnFunctionEntity.StatusFunction.EXCEPTION, e.getMessage());
        } catch (Exception e) {
            return new BaseReturnFunctionEntity<>(BaseReturnFunctionEntity.StatusFunction.EXCEPTION, e.getMessage());
        } catch (OutOfMemoryError e) {
            return new BaseReturnFunctionEntity<>(BaseReturnFunctionEntity.StatusFunction.EXCEPTION, e.getMessage());
        }
    }
}

