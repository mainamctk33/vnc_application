package com.team500ae.vncapplication.models.realm_models.job;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by SVIP on 12/18/2017.
 */

public class CityJobEntity extends RealmObject {
    @PrimaryKey
    private int id;
    private String name;
    private int country;
    public int getId(){
        return  id;
    }
    public void setId(int id){
        this.id = id;
    }
    public String getName(){
        return name;
    }
    public void setName(String name){
        this.name = name;
    }
    public int getCountry(){
        return  country;
    }
    public void setCountry(int country){
        this.country= country;
    }
}
