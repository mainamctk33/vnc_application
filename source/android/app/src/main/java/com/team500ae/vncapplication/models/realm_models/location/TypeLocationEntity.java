package com.team500ae.vncapplication.models.realm_models.location;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.Required;

/**
 * Created by MaiNam on 11/15/2017.
 */

public class TypeLocationEntity extends RealmObject {
    @PrimaryKey
    int id;

    @Required
    String name;

    @Required
    String logo;

    @Required
    String enName;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public String getEnName() {
        return enName != null ? enName : "";
    }

    public void setEnName(String enName) {
        this.enName = enName;
    }


}
