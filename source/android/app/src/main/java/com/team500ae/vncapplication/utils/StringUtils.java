package com.team500ae.vncapplication.utils;

import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.widget.TextView;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.team500ae.vncapplication.R;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

//import com.klinker.android.link_builder.Link;
//import com.klinker.android.link_builder.LinkBuilder;

/**
 * Created by MaiNam on 4/23/2016.
 */
public class StringUtils {
    private static final String TAG = StringUtils.class.getSimpleName();
    private static char[] SPECIAL_CHARACTERS = {' ', '!', '"', '#', '$', '%',
            '*', '+', ',', ':', '<', '=', '>', '?', '@', '[', '\\', ']', '^',
            '`', '|', '~', 'À', 'Á', 'Â', 'Ã', 'È', 'É', 'Ê', 'Ì', 'Í', 'Ò',
            'Ó', 'Ô', 'Õ', 'Ù', 'Ú', 'Ý', 'à', 'á', 'â', 'ã', 'è', 'é', 'ê',
            'ì', 'í', 'ò', 'ó', 'ô', 'õ', 'ù', 'ú', 'ý', 'Ă', 'ă', 'Đ', 'đ',
            'Ĩ', 'ĩ', 'Ũ', 'ũ', 'Ơ', 'ơ', 'Ư', 'ư', 'Ạ', 'ạ', 'Ả', 'ả', 'Ấ',
            'ấ', 'Ầ', 'ầ', 'Ẩ', 'ẩ', 'Ẫ', 'ẫ', 'Ậ', 'ậ', 'Ắ', 'ắ', 'Ằ', 'ằ',
            'Ẳ', 'ẳ', 'Ẵ', 'ẵ', 'Ặ', 'ặ', 'Ẹ', 'ẹ', 'Ẻ', 'ẻ', 'Ẽ', 'ẽ', 'Ế',
            'ế', 'Ề', 'ề', 'Ể', 'ể', 'Ễ', 'ễ', 'Ệ', 'ệ', 'Ỉ', 'ỉ', 'Ị', 'ị',
            'Ọ', 'ọ', 'Ỏ', 'ỏ', 'Ố', 'ố', 'Ồ', 'ồ', 'Ổ', 'ổ', 'Ỗ', 'ỗ', 'Ộ',
            'ộ', 'Ớ', 'ớ', 'Ờ', 'ờ', 'Ở', 'ở', 'Ỡ', 'ỡ', 'Ợ', 'ợ', 'Ụ', 'ụ',
            'Ủ', 'ủ', 'Ứ', 'ứ', 'Ừ', 'ừ', 'Ử', 'ử', 'Ữ', 'ữ', 'Ự', 'ự',};

    private static char[] REPLACEMENTS = {' ', '\0', '\0', '\0', '\0', '%',
            '\0', '_', '\0', '_', '\0', '\0', '\0', '\0', '\0', '\0', '_',
            '\0', '\0', '\0', '\0', '\0', 'A', 'A', 'A', 'A', 'E', 'E', 'E',
            'I', 'I', 'O', 'O', 'O', 'O', 'U', 'U', 'Y', 'a', 'a', 'a', 'a',
            'e', 'e', 'e', 'i', 'i', 'o', 'o', 'o', 'o', 'u', 'u', 'y', 'A',
            'a', 'D', 'd', 'I', 'i', 'U', 'u', 'O', 'o', 'U', 'u', 'A', 'a',
            'A', 'a', 'A', 'a', 'A', 'a', 'A', 'a', 'A', 'a', 'A', 'a', 'A',
            'a', 'A', 'a', 'A', 'a', 'A', 'a', 'A', 'a', 'E', 'e', 'E', 'e',
            'E', 'e', 'E', 'e', 'E', 'e', 'E', 'e', 'E', 'e', 'E', 'e', 'I',
            'i', 'I', 'i', 'O', 'o', 'O', 'o', 'O', 'o', 'O', 'o', 'O', 'o',
            'O', 'o', 'O', 'o', 'O', 'o', 'O', 'o', 'O', 'o', 'O', 'o', 'O',
            'o', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u',
            'U', 'u',};

    public static String toUrlFriendly(String s) {
        int maxLength = Math.min(s.length(), 236);
        char[] buffer = new char[maxLength];
        int n = 0;
        for (int i = 0; i < maxLength; i++) {
            char ch = s.charAt(i);
            buffer[n] = removeAccent(ch);
            // skip not printable characters
            if (buffer[n] > 31) {
                n++;
            }
        }
        // skip trailing slashes
        while (n > 0 && buffer[n - 1] == '/') {
            n--;
        }
        return String.valueOf(buffer, 0, n);
    }

    public static char removeAccent(char ch) {
        int index = Arrays.binarySearch(SPECIAL_CHARACTERS, ch);
        if (index >= 0) {
            ch = REPLACEMENTS[index];
        }
        return ch;
    }

    public static String stripHtml(String html) {
        return Html.fromHtml(html).toString();
    }

    public static String removeAccent(String s) {
        StringBuilder sb = new StringBuilder(s);
        for (int i = 0; i < sb.length(); i++) {
            sb.setCharAt(i, removeAccent(sb.charAt(i)));
        }
        return sb.toString();
    }

    public static String removeAccentLowerCase(String s) {
        StringBuilder sb = new StringBuilder(s);
        for (int i = 0; i < sb.length(); i++) {
            sb.setCharAt(i, removeAccent(sb.charAt(i)));
        }
        return sb.toString().toLowerCase();
    }

    public static class Validate {
        public static final Pattern VALID_EMAIL_ADDRESS_REGEX =
                Pattern.compile("^[A-Z0-9._%+-]{4,}@[A-Z0-9.-]+\\.[A-Z]{2,6}$", Pattern.CASE_INSENSITIVE);

        public static final Pattern VALID_YEAR_REGEX =
                Pattern.compile("^\\d{4}$", Pattern.CASE_INSENSITIVE);

        public static final Pattern VALID_PERSONAL_NAME_REGEX =
                Pattern.compile("[A-Z][a-z]+( [A-Z][a-z]+)?", Pattern.CASE_INSENSITIVE);

        public static final Pattern VALID_PHONE_NUMBER_REGEX =
                Pattern.compile("^(0|\\+\\d{1,3})\\d{6,10}$", Pattern.CASE_INSENSITIVE);
        private static final Pattern VALID_DATE_REGEX = Pattern.compile("^(?:(?:31(\\/|-|\\.)(?:0?[13578]|1[02]))\\1|(?:(?:29|30)(\\/|-|\\.)(?:0?[1,3-9]|1[0-2])\\2))(?:(?:1[6-9]|[2-9]\\d)?\\d{2})$|^(?:29(\\/|-|\\.)0?2\\3(?:(?:(?:1[6-9]|[2-9]\\d)?(?:0[48]|[2468][048]|[13579][26])|(?:(?:16|[2468][048]|[3579][26])00))))$|^(?:0?[1-9]|1\\d|2[0-8])(\\/|-|\\.)(?:(?:0?[1-9])|(?:1[0-2]))\\4(?:(?:1[6-9]|[2-9]\\d)?\\d{2})$", Pattern.CASE_INSENSITIVE);
        public static final Pattern VALID_PHONE_NUMBER_REGEX_2 =
                Pattern.compile("^(\\+|\\d)\\d{9,13}$", Pattern.CASE_INSENSITIVE);

        public static boolean isEmail(CharSequence emailStr) {
            Matcher matcher = VALID_EMAIL_ADDRESS_REGEX.matcher(emailStr);
            return matcher.find();
        }

        public static boolean isPhone(CharSequence emailStr) {
            Matcher matcher = VALID_PHONE_NUMBER_REGEX.matcher(emailStr);
            return matcher.find();
        }

        public static boolean isPhone2(CharSequence phoneNumber) {
            Matcher matcher = VALID_PHONE_NUMBER_REGEX_2.matcher(phoneNumber);
            return matcher.find();
        }

        public static boolean isDate(CharSequence dateStr) {
            Matcher matcher = VALID_DATE_REGEX.matcher(dateStr);
            return matcher.find();
        }


        public static boolean isYear(CharSequence yearStr) {
            Matcher matcher = VALID_YEAR_REGEX.matcher(yearStr);
            return matcher.find();
        }


        public static boolean isPersonalName(CharSequence emailStr) {
            Matcher matcher = VALID_PERSONAL_NAME_REGEX.matcher(emailStr);
            return matcher.find() && emailStr.length() >= 6 && emailStr.length() <= 255;
        }


        public static boolean isPassword(String s) {
            return s.length() >= 6 && s.length() <= 255;
        }
    }

    public static String getMD5(String input) {
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            byte[] messageDigest = md.digest(input.getBytes());
            BigInteger number = new BigInteger(1, messageDigest);
            String hashtext = number.toString(16);
            // Now we need to zero pad it if you actually want the full 32 chars.
            while (hashtext.length() < 32) {
                hashtext = "0" + hashtext;
            }
            return hashtext;
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        }
    }

    public static String join(String split, String[] values) {
        return TextUtils.join(split, values);
    }

    public static String join(String split, List<String> values) {
        return TextUtils.join(split, values);
    }

    public static String join(String split, JsonArray values) {
        try {
            return TextUtils.join(split, values);
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

//    public static void setText(final TextView textView, final String text, int limitChar, String readMore, boolean clickToShowAll) {
//        if (limitChar < text.length() - 1 && limitChar != 0) {
//            if (readMore == null)
//                readMore = "";
//            if (!clickToShowAll) {
//                textView.setText(text.substring(0, limitChar) + readMore);
//            } else {
//                textView.setText(Html.fromHtml(text.substring(0, 10) + " " + readMore + " "));
//                ArrayList<Link> links = new ArrayList<>();
//                links.add(new Link(readMore).setTextColor(textView.getContext().getResources().getColor(R.color.colorPrimary)).setBold(true).setUnderlined(false).setOnClickListener(new Link.OnClickListener() {
//                    @Override
//                    public void onClick(String clickedText) {
//                    }
//                }));
//                LinkBuilder.on(textView)
//                        .addLinks(links)
//                        .build();
//
//                Log.d(TAG, String.valueOf(textView.getText()));
//            }
//        } else {
//            textView.setText(text);
//        }
//    }

    public static String getText(String text, int limitChar, String readMore) {
        if (text.length() < limitChar)
            return text;
        return text.substring(0, limitChar) + " " + readMore;
    }

    public static ArrayList<String> getListNations() {
        try {
            ArrayList<String> listNations = new ArrayList<>();
            String[] locales = Locale.getISOCountries();
            for (String countryCode : locales) {
                Locale obj = new Locale("", countryCode);
                listNations.add(obj.getDisplayCountry());
            }
            return listNations;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static int getIdFromValue(List<String> listValue, List<Integer> listId, String string) {
        try {
            if (listValue == null || listValue.isEmpty() || string == null || string.isEmpty())
                return -1;

            for (int i = 0; i < listValue.size(); i++)
                if (listValue.get(i).toLowerCase().equals(string.toLowerCase()))
                    return listId.get(i);

            return -1;
        } catch (Exception e) {
            e.printStackTrace();
            return -1;
        }
    }

    public static int getIntegerIndex(List<Integer> list, Integer integer) {
        try {
            if (list == null || list.isEmpty())
                return -1;

            for (int i = 0; i < list.size(); i++)
                if (list.get(i) == integer)
                    return i;

            return -1;
        } catch (Exception e) {
            e.printStackTrace();
            return -1;
        }
    }

    public static int getStringIndex(List<String> list, String string) {
        try {
            if (list == null || list.isEmpty() || string == null || string.isEmpty())
                return -1;

            string = string.toLowerCase();
            for (int i = 0; i < list.size(); i++)
                if (list.get(i).toLowerCase().equals(string))
                    return i;

            return -1;
        } catch (Exception e) {
            e.printStackTrace();
            return -1;
        }
    }

    public static boolean isNullOrWhiteSpace(String text) {
        if (text == null)
            return true;
        if (text.trim().isEmpty())
            return true;
        if (text.equals("null"))
            return true;
        return false;
    }

    public static ArrayList<String> getLimitedListString(ArrayList<String> list, int maxItem) {
        if (list == null || list.isEmpty() || list.size() <= maxItem)
            return list;

        int length = list.size();
        for (int idx = length - 1; idx > maxItem - 1; idx--)
            list.remove(idx);
        return list;
    }

    public static ArrayList<JsonObject> getLimitedListJsonObj(ArrayList<JsonObject> list, int maxItem) {
        if (list == null || list.isEmpty() || list.size() <= maxItem)
            return list;

        int length = list.size();
        for (int idx = length - 1; idx > maxItem - 1; idx--)
            list.remove(idx);
        return list;
    }
}
