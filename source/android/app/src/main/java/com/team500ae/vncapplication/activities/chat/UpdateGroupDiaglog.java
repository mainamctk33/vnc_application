package com.team500ae.vncapplication.activities.chat;

import android.app.Dialog;
import android.graphics.Bitmap;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.team500ae.library.utils.ImageUtils;
import com.team500ae.vncapplication.R;
import com.team500ae.vncapplication.others.DialogListener;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class UpdateGroupDiaglog extends Dialog {


    private final String mCover;
    private final String name;
    @BindView(R.id.txtGroupName)
    EditText etUrl;
    @BindView(R.id.ivGroupCover)
    ImageView ivGroupCover;
    private DialogListener clickListener;

    public UpdateGroupDiaglog(FragmentActivity activity, String coverUrl, String name) {
        super(activity, android.R.style.Theme_Holo_Dialog);
        mCover = coverUrl;
        this.name= name;
    }

    public void setOnItemClickListener(DialogListener listener) {
        this.clickListener = listener;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        if (getWindow() != null) {
            getWindow().setDimAmount(0.3f);
            getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        }
        setContentView(R.layout.dialog_update_group_channel);

        ButterKnife.bind(this);
        ImageUtils.loadImageByGlide(getContext(), false, 0, 0, mCover, R.drawable.img_no_image, R.drawable.img_no_image, ivGroupCover, true);
        etUrl.setText(name);
    }

    @OnClick({R.id.btnSelectImage, R.id.btnCancel, R.id.btnOk})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btnCancel:
                dismiss();
                break;
            case R.id.btnOk:
                if (etUrl.getText().toString().isEmpty()) {
                    Toast.makeText(getContext(), R.string.please_input_group_name, Toast.LENGTH_LONG).show();
                    return;
                }
                if (clickListener != null)
                    clickListener.onConfirmClicked();
                dismiss();
                break;
            case R.id.btnSelectImage:
                if (clickListener != null)
                    clickListener.onButtonChooseImage();
                break;

        }
    }

    public void setBitmap(Bitmap bitmap) {
        ivGroupCover.setImageBitmap(bitmap);
    }


    public String getEditText() {
        if (etUrl.getText().length() > 0)
            return etUrl.getText().toString();
        return "";
    }
}
