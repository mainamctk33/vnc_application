package com.team500ae.vncapplication.activities.location;

import com.google.android.gms.location.places.ui.PlaceAutocomplete;

import java.util.ArrayList;

public interface PlaceAutoCompleteInterface {
    public void onPlaceClick(ArrayList<PlaceAutocomplete> mResultList, int position);
}