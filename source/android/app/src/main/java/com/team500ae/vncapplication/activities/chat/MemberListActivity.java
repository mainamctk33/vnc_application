package com.team500ae.vncapplication.activities.chat;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;

import com.sendbird.android.GroupChannel;
import com.sendbird.android.Member;
import com.sendbird.android.SendBird;
import com.sendbird.android.SendBirdException;
import com.sendbird.android.User;
import com.team500ae.vncapplication.R;
import com.team500ae.vncapplication.activities.chat.adapter.UserListAdapter;
import com.team500ae.vncapplication.activities.chat.fragments.GroupChatFragment;
import com.team500ae.vncapplication.activities.user.UserProfileActivity;
import com.team500ae.vncapplication.base.BaseActivity;
import com.team500ae.vncapplication.data_access.user.UserInfo;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;


public class MemberListActivity extends BaseActivity {

    private UserListAdapter mListAdapter;
    private RecyclerView mRecyclerView;
    private LinearLayoutManager mLayoutManager;
    private String mChannelUrl;
    private GroupChannel mChannel;
    @BindView(R.id.menu)
    View menu;
    private Member currentMember;

    @OnClick(R.id.menu)
    void menuClick() {
        menu.setVisibility(View.GONE);
    }

    @BindView(R.id.btnBlock)
    View btnBlock;
    @BindView(R.id.btnUnBlock)
    View btnUnblock;

    @OnClick(R.id.btnUnBlock)
    void unBlock() {
        menu.setVisibility(View.GONE);
        if (currentMember != null) {
            SendBird.unblockUser(currentMember, new SendBird.UserUnblockHandler() {
                @Override
                public void onUnblocked(SendBirdException e) {
                    if (e != null) {
                        return;
                    }
                    mListAdapter.notifyDataSetChanged();
                }
            });
        }
    }

    @OnClick(R.id.btnBlock)
    void block() {
        menu.setVisibility(View.GONE);
        if (currentMember != null) {
            SendBird.blockUser(currentMember, new SendBird.UserBlockHandler() {
                @Override
                public void onBlocked(User user, SendBirdException e) {
                    if (e != null) {
                        return;
                    }
                    mListAdapter.notifyDataSetChanged();
                }
            });
        }
    }

    @OnClick(R.id.btnUserProfile)
    void viewProfile()
    {
        menu.setVisibility(View.GONE);
        UserProfileActivity.open(getActivity(),currentMember.getUserId());
    }
    @OnClick(R.id.btnChat)
    void sendMessage()
    {
        menu.setVisibility(View.GONE);
        GroupChannelActivity.open(getActivity(),currentMember.getUserId());
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        setContentView(R.layout.activity_member_list);
        super.onCreate(savedInstanceState);

        mChannelUrl = getIntent().getStringExtra(GroupChatFragment.EXTRA_CHANNEL_URL);
        if (mChannelUrl == null) {
            // Theoretically shouldn't happen
            finish();
        }

        mChannelUrl = getIntent().getStringExtra(GroupChatFragment.EXTRA_CHANNEL_URL);
        mRecyclerView = (RecyclerView) findViewById(R.id.recycler_member_list);
        mListAdapter = new UserListAdapter(this, true, new UserListAdapter.onMenuClickListener() {
            @Override
            public void onClick(View view, Member member) {
                menu.setVisibility(View.VISIBLE);
                currentMember = member;
                if (member.isBlockedByMe()) {
                    btnUnblock.setVisibility(View.VISIBLE);
                    btnBlock.setVisibility(View.GONE);
                } else {
                    btnUnblock.setVisibility(View.GONE);
                    btnBlock.setVisibility(View.VISIBLE);
                }
            }
        });


        setUpRecyclerView();

        getChannelFromUrl(mChannelUrl);
        showBackButton();
    }

    private void setUpRecyclerView() {
        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setAdapter(mListAdapter);
        mRecyclerView.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home) {
            onBackPressed();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void getChannelFromUrl(String url) {
        GroupChannel.getChannel(url, new GroupChannel.GroupChannelGetHandler() {
            @Override
            public void onResult(GroupChannel groupChannel, SendBirdException e) {
                if (e != null) {
                    // Error!
                    return;
                }

                mChannel = groupChannel;

                refreshChannel();
            }
        });
    }

    private void refreshChannel() {
        mChannel.refresh(new GroupChannel.GroupChannelRefreshHandler() {
            @Override
            public void onResult(SendBirdException e) {
                if (e != null) {
                    // Error!
                    return;
                }

                setUserList(mChannel.getMembers());
            }
        });
    }

    private void setUserList(List<Member> userList) {
        mListAdapter.setUserList(userList);
    }

}
