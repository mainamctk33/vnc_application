package com.team500ae.vncapplication.activities.job;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.gson.JsonArray;
import com.team500ae.vncapplication.R;
import com.team500ae.vncapplication.activities.job.fragments.JobTabFragment;
import com.team500ae.vncapplication.base.BaseActivity;
import com.team500ae.vncapplication.data_access.RealmInfo;
import com.team500ae.vncapplication.data_access.job.CityJobInfo;
import com.team500ae.vncapplication.data_access.job.CompanyJobInfo;
import com.team500ae.vncapplication.data_access.job.CountryJobInfo;
import com.team500ae.vncapplication.data_access.job.JobInfo;
import com.team500ae.vncapplication.data_access.job.LevelJobInfo;
import com.team500ae.vncapplication.data_access.job.SalaryJobInfo;
import com.team500ae.vncapplication.data_access.job.TypeJobInfo;
import com.team500ae.vncapplication.models.BaseReturnFunctionEntity;
import com.team500ae.vncapplication.models.ServiceResponseEntity;
import com.team500ae.vncapplication.models.realm_models.job.CityJobEntity;
import com.team500ae.vncapplication.models.realm_models.job.CompanyJobEntity;
import com.team500ae.vncapplication.models.realm_models.job.CountryJobEntity;
import com.team500ae.vncapplication.models.realm_models.job.LevelJobEntity;
import com.team500ae.vncapplication.models.realm_models.job.SalaryJobEntity;
import com.team500ae.vncapplication.models.realm_models.job.TypeJobEntity;

import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.OnClick;
import io.realm.Realm;

/**
 * Created by SVIP on 12/7/2017.
 */

public class SearchJobActivity extends BaseActivity implements JobTabFragment.LoadListener {

    public static void open(FragmentActivity activity) {
        Intent intent = new Intent(activity, SearchJobActivity.class);
        activity.startActivity(intent);
    }

    Realm realm;
    @BindView(R.id.txtTitle)
    TextView txtTitle;
    @BindView(R.id.rdRecent)
    RadioButton rdRecent;
    @BindView(R.id.rdHighestSalary)
    RadioButton rdHighestSalary;
    @BindView(R.id.spCity)
    Spinner spCity;
    @BindView(R.id.spCountry)
    Spinner spCountry;
    @BindView(R.id.spType)
    Spinner spType;
    @BindView(R.id.spLevel)
    Spinner spLevel;
    @BindView(R.id.spSalary)
    Spinner spSalary;
    @BindView(R.id.txtCompany)
    EditText txtCompany;
    @BindView(R.id.layout_search_main)
    ScrollView layout_search_main;
    @BindView(R.id.layoutFragment)
    LinearLayout layoutFragment;

    ServiceResponseEntity<JsonArray> countryList;
    ArrayList<Integer> countryID;
    ArrayList<String> countryName;

    ServiceResponseEntity<JsonArray> cityList;
    ArrayList<Integer> cityID;
    ArrayList<String> cityName;


    ServiceResponseEntity<JsonArray> typeList;
    ArrayList<Integer> typeID;
    ArrayList<String> typeName;

    ServiceResponseEntity<JsonArray> levelList;
    ArrayList<Integer> levelID;
    ArrayList<String> levelName;

    ServiceResponseEntity<JsonArray> salaryList;
    ArrayList<Integer> salaryID;
    ArrayList<String> salaryName;
    ArrayList<Integer> salaryFrom;
    ArrayList<Integer> salaryTo;

    ServiceResponseEntity<JsonArray> companyList;
    ArrayList<Integer> companyID;
    ArrayList<String> companyName;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        setContentView(R.layout.activity_search_job);
        super.onCreate(savedInstanceState);
        realm = RealmInfo.getRealm(getApplicationContext());
        showBackButton();

//       try {
//           BaseReturnFunctionEntity<ArrayList<CompanyJobEntity>> myList = new CompanyJobInfo().getList(getApplicationContext(), realm);
//           companyID = new ArrayList<>();
//           companyName = new ArrayList<>();
//           companyID.add(-1);
//           companyName.add(getString(R.string.job_search_all));
//           if (myList.isTrue()) {
//               //myList2.addAll(myList.getData());
//               for (CompanyJobEntity type : myList.getData()) {
//                   companyID.add(type.getId());
//                   companyName.add(type.getName());
//                   //tabFragments.add(JobTabFragment.getInstance(type.getId()));
//               }
//           }
//       }catch (Exception ex){}

        try {
            BaseReturnFunctionEntity<ArrayList<LevelJobEntity>> myListLevel = new LevelJobInfo().getList(getApplicationContext(), realm);
            levelName = new ArrayList<>();
            levelID = new ArrayList<>();
            levelID.add(-1);
            levelName.add(getString(R.string.job_search_all));
            if (myListLevel.isTrue()) {
                //myList2.addAll(myList.getData());
                for (LevelJobEntity type : myListLevel.getData()) {
                    levelID.add(type.getId());
                    levelName.add(type.getName());
                    //tabFragments.add(JobTabFragment.getInstance(type.getId()));
                }
            }
            spLevel.setAdapter(new ArrayAdapter<String>(SearchJobActivity.this, R.layout.item_spinner, levelName));
        } catch (Exception ex) {
        }

        try {
            BaseReturnFunctionEntity<ArrayList<SalaryJobEntity>> myList = new SalaryJobInfo().getList(getApplicationContext(), realm);
            salaryID = new ArrayList<>();
            salaryName = new ArrayList<>();
            salaryFrom = new ArrayList<>();
            salaryTo = new ArrayList<>();
            salaryID.add(-1);
            salaryName.add(getString(R.string.job_search_all));
            if (myList.isTrue()) {
                //myList2.addAll(myList.getData());
                for (SalaryJobEntity type : myList.getData()) {
                    salaryID.add(type.getId());
                    salaryName.add(type.getName());
                    salaryFrom.add(type.getFrom());
                    salaryTo.add(type.getTo());
                    //tabFragments.add(JobTabFragment.getInstance(type.getId()));
                }
            }
            spSalary.setAdapter(new ArrayAdapter<String>(SearchJobActivity.this, R.layout.item_spinner, salaryName));
        } catch (Exception ex) {
        }

        try {
            BaseReturnFunctionEntity<ArrayList<TypeJobEntity>> myList = new TypeJobInfo().getListTypeJob(getApplicationContext(), realm);
            typeID = new ArrayList<>();
            typeName = new ArrayList<>();
            typeID.add(-1);
            typeName.add(getString(R.string.job_search_all));
            if (myList.isTrue()) {
                //myList2.addAll(myList.getData());
                for (TypeJobEntity type : myList.getData()) {
                    typeID.add(type.getId());
                    typeName.add(type.getName());
                    //tabFragments.add(JobTabFragment.getInstance(type.getId()));
                }
            }
            spType.setAdapter(new ArrayAdapter<String>(SearchJobActivity.this, R.layout.item_spinner, typeName));
        } catch (Exception ex) {
        }

        try {
            BaseReturnFunctionEntity<ArrayList<CountryJobEntity>> myListCountry = new CountryJobInfo().getList(getApplicationContext(), realm);
            countryName = new ArrayList<>();
            countryID = new ArrayList<>();
            countryID.add(-1);
            countryName.add(getString(R.string.job_search_all));
            if (myListCountry.isTrue()) {
                //myList2.addAll(myList.getData());
                for (CountryJobEntity type : myListCountry.getData()) {
                    countryID.add(type.getId());
                    countryName.add(type.getName());
                    //tabFragments.add(JobTabFragment.getInstance(type.getId()));
                }
            }
            spCountry.setAdapter(new ArrayAdapter<String>(SearchJobActivity.this, R.layout.item_spinner, countryName));
        } catch (Exception ex) {
        }

        spCountry.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {


                try {
                    int cid = countryID.get(position);
                    BaseReturnFunctionEntity<ArrayList<CityJobEntity>> myList = new CityJobInfo().getList(getApplicationContext(), realm);
                    cityID = new ArrayList<>();
                    cityName = new ArrayList<>();
                    cityID.add(-1);
                    cityName.add(getString(R.string.job_search_all));
                    if (myList.isTrue()) {
                        //myList2.addAll(myList.getData());
                        for (CityJobEntity type : myList.getData()) {
                            if (type.getCountry() == cid) {
                                cityID.add(type.getId());
                                cityName.add(type.getName());
                            }
                        }
                    }
                    spCity.setAdapter(new ArrayAdapter<String>(SearchJobActivity.this, R.layout.item_spinner, cityName));
                } catch (Exception ex) {
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }

    public void showBackButton() {
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }


    @Override
    public void onBeginLoad(Fragment fragment) {

    }

    @Override
    public void onEndLoad(Fragment fragment) {

    }

    @Override
    public void onDestroy() {
        RealmInfo.closeRealm(realm);
        super.onDestroy();
    }


    JobTabFragment tabFragment;

    @OnClick(R.id.tvApply)
    void tvApply() {
        try {
            layout_search_main.setVisibility(View.GONE);
            layoutFragment.setVisibility(View.VISIBLE);
            String keyword = txtTitle.getText().toString();
            tabFragment = (JobTabFragment) getSupportFragmentManager().findFragmentByTag("job");
            if (tabFragment == null) {
                tabFragment = JobTabFragment.getInstance(0);
                getSupportFragmentManager().beginTransaction().replace(R.id.layoutFragment, tabFragment, "job").addToBackStack(null).commit();
            } else {
                tabFragment.getData();
            }
        } catch (Exception e) {
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home) {
            onBackPressed();


            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onBackPressed() {
        if (layout_search_main.getVisibility() == View.VISIBLE) {
            layoutFragment.setVisibility(View.VISIBLE);
            layout_search_main.setVisibility(View.GONE);
        } else {
            layoutFragment.setVisibility(View.GONE);
            layout_search_main.setVisibility(View.VISIBLE);
        }
        super.onBackPressed();

    }

    @Override
    public JsonArray getData(int page, int size, int type, Fragment tabFragment) {
        try {
            String titleData = "";
            int countryData = -1;
            int cityData = -1;
            int typeData = -1;
            int levelData = -1;
            int salaryFromData = 0;
            int salaryToData = 999999999;
            String companyData = "";
            int sortData = -1;

            titleData = txtTitle.getText().toString().trim();
            countryData = countryID.get(spCountry.getSelectedItemPosition());
            cityData = cityID.get(spCity.getSelectedItemPosition());
            typeData = typeID.get(spType.getSelectedItemPosition());
            levelData = levelID.get(spLevel.getSelectedItemPosition());
            salaryFromData = salaryFrom.get(spSalary.getSelectedItemPosition());
            salaryToData = salaryTo.get(spSalary.getSelectedItemPosition());
            companyData = txtCompany.getText().toString().trim();
            if (rdRecent.isChecked())
                sortData = 0;
            if (rdHighestSalary.isChecked())
                sortData = 1;

            ServiceResponseEntity<JsonArray> result = new JobInfo().searchWithCity(getActivity(), page, size, URLEncoder.encode(titleData,"UTF-8"), countryData, cityData, typeData, levelData, salaryFromData, salaryToData, companyData, sortData);
            if (result != null && result.getStatus() == 0)
                return result.getData();
            return new JsonArray();
        } catch (Exception ex) {
            return new JsonArray();
        }
    }

    @OnClick(R.id.tvClear)
    void tvClear() {
        txtTitle.setText("");
        txtCompany.setText("");
        rdRecent.setChecked(true);
        //rdHighestSalary.setChecked(true);

        spCountry.setSelection(0);
        spCity.setSelection(0);
        spType.setSelection(0);
        spLevel.setSelection(0);
        spSalary.setSelection(0);
    }

}
