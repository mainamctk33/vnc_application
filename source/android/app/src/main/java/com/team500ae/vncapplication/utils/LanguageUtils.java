package com.team500ae.vncapplication.utils;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.support.annotation.NonNull;
import android.util.Log;

import com.team500ae.vncapplication.constants.Constants;

import java.util.Locale;

/**
 * Created by phuong.ndp on 25/02/2018.
 */

public class LanguageUtils {

    public static void changeLanguage(@NonNull Activity context, String language) {
        String languageToLoad = language;
        Locale locale = new Locale(languageToLoad);
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        context.getResources().updateConfiguration(config, context.getResources().getDisplayMetrics());
        LanguageUtils.saveAppLanguageSetting(context, language);
    }

    public static void saveAppLanguageSetting(@NonNull Activity context, String language) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(Constants.VNC_SHARED_PREFERENCES_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(Constants.SHARED_PREFERENCES_LANGUAE, language);
        editor.commit();
    }

    public static String getAppLanguageSetting(@NonNull Activity context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(Constants.VNC_SHARED_PREFERENCES_NAME, Context.MODE_PRIVATE);
        String language = sharedPreferences.getString(Constants.SHARED_PREFERENCES_LANGUAE, "vi");
        Log.d("test",language);
        return language;
    }
}
