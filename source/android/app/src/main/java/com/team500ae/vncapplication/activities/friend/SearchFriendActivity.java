package com.team500ae.vncapplication.activities.friend;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStates;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.team500ae.library.utils.AndroidPermissionUtils;
import com.team500ae.vncapplication.R;
import com.team500ae.vncapplication.activities.friend.fragments.FriendAroundFragment;
import com.team500ae.vncapplication.activities.location.fragments.LocationSearchFragment;
import com.team500ae.vncapplication.base.BaseActivity;
import com.team500ae.vncapplication.constants.Constants;
import com.team500ae.vncapplication.utils.GPSTrackerUtils;

/**
 * Created by maina on 1/16/2018.
 */

public class SearchFriendActivity extends BaseActivity {
    public static void open(Context context) {
        Intent intent = new Intent(context, SearchFriendActivity.class);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_friend);
        getSupportFragmentManager().beginTransaction().replace(R.id.lnFragment, FriendAroundFragment.getInstants(), "searchFriend1").commit();
    }

    @Override
    public void onBackPressed() {
        Intent intent=new Intent(Constants.ACTION_LOCATION_BACK_TO_MAIN_APP);
        getApplication().sendBroadcast(intent);
        super.onBackPressed();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.lnFragment);
        if (fragment != null && fragment instanceof FriendAroundFragment)
            fragment.onActivityResult(requestCode, resultCode, data);
    }
}
