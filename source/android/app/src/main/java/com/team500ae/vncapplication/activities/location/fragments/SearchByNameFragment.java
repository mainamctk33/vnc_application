package com.team500ae.vncapplication.activities.location.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.gson.ExclusionStrategy;
import com.google.gson.FieldAttributes;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.annotations.SerializedName;
import com.google.gson.reflect.TypeToken;
import com.team500ae.library.adapters.BaseRecyclerAdapter;
import com.team500ae.library.layoutmanager.MyLinearLayoutManager;
import com.team500ae.library.utils.ConvertUtils;
import com.team500ae.library.utils.ImageUtils;
import com.team500ae.library.utils.StringUtils;
import com.team500ae.vncapplication.R;
import com.team500ae.vncapplication.activities.location.DetailLocationActivity;
import com.team500ae.vncapplication.base.BaseViewHolder;
import com.team500ae.vncapplication.data_access.DataCacheInfo;
import com.team500ae.vncapplication.data_access.location.LocationInfo;
import com.team500ae.vncapplication.models.BaseReturnFunctionEntity;
import com.team500ae.vncapplication.utils.KeyboardUtils;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by maina on 2/4/2018.
 */


public class SearchByNameFragment extends Fragment implements Filterable {


    private ArrayList<JsonObject> listObjects;
    private ArrayList<JsonObject> listObjectsHistory;
    private BaseRecyclerAdapter<ViewHolder, JsonObject> adapter;
    private BaseRecyclerAdapter<ViewHolder, JsonObject> adapterHistory;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_search_location_by_name, container, false);
        KeyboardUtils.setupUI(view, getActivity());
        ButterKnife.bind(this, view);
        KeyboardUtils.setupUI(view, getActivity());
        listObjects = new ArrayList<>();
        listObjectsHistory = new ArrayList<>();
        try {
            String text = DataCacheInfo.getData(getContext(), DataCacheInfo.EnumCacheType.History_Place);
            listObjectsHistory = new Gson().fromJson(text, new TypeToken<ArrayList<JsonObject>>() {
            }.getType());
            if (listObjectsHistory == null)
                listObjectsHistory = new ArrayList<>();
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (listObjectsHistory == null)
            listObjectsHistory = new ArrayList<>();
        recyclerView.setLayoutManager(new MyLinearLayoutManager(getContext()));
        recyclerView.setHasFixedSize(true);
        txtSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (!s.toString().equals("")) {
                    mClear.setVisibility(View.VISIBLE);
                    adapter.bindData(recyclerView);

                    if (listObjects != null && listObjects.size() > 0)
                        resultSearch.setVisibility(View.VISIBLE);
                    else
                        resultSearch.setVisibility(View.GONE);
                } else {
                    mClear.setVisibility(View.GONE);
                    adapterHistory.bindData(recyclerView);

                    if (listObjectsHistory != null && listObjectsHistory.size() > 0) {
                        resultSearch.setVisibility(View.VISIBLE);
                    } else {
                        resultSearch.setVisibility(View.GONE);
                    }
                }
                getFilter().filter(s.toString());
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        adapter = new BaseRecyclerAdapter<ViewHolder, JsonObject>(R.layout.item_place, new BaseRecyclerAdapter.BaseViewHolder<ViewHolder, JsonObject>() {
            @Override
            public ViewHolder getViewHolder(View v) {
                return new ViewHolder(v);
            }

            @Override
            public void bindData(RecyclerView recyclerView, ViewHolder viewHolder, JsonObject data, int position) {
                viewHolder.tvName.setText(ConvertUtils.toString(data.get("Title")));
                if (StringUtils.isEmpty(ConvertUtils.toString(data.get("Address")))) {
                    viewHolder.tvAddress.setVisibility(View.GONE);
                } else {
                    viewHolder.tvAddress.setVisibility(View.VISIBLE);
                }

                viewHolder.tvAddress.setText(ConvertUtils.toString(data.get("Address")));
                viewHolder.tvPlaceName.setText(ConvertUtils.toString(data.get("TypeName")));
                ImageUtils.loadImageByGlide(getContext(), false, 0, 0, ConvertUtils.toString(data.get("TypeLogo")), R.drawable.ic_location_address, R.drawable.ic_location_address, viewHolder.imgPlaceLogo, true);
            }
        }, listObjects, new BaseRecyclerAdapter.OnClickListener() {
            @Override
            public void onClick(View v, int position, Object o) {
                viewPlace((JsonObject) o);
            }
        });
        adapterHistory = new BaseRecyclerAdapter<ViewHolder, JsonObject>(R.layout.item_place, new BaseRecyclerAdapter.BaseViewHolder<ViewHolder, JsonObject>() {
            @Override
            public ViewHolder getViewHolder(View v) {
                return new ViewHolder(v);
            }

            @Override
            public void bindData(RecyclerView recyclerView, ViewHolder viewHolder, JsonObject data, int position) {
                viewHolder.tvName.setText(ConvertUtils.toString(data.get("Title")));
                if (StringUtils.isEmpty(ConvertUtils.toString(data.get("Address")))) {
                    viewHolder.tvAddress.setVisibility(View.GONE);
                } else {
                    viewHolder.tvAddress.setVisibility(View.VISIBLE);
                }

                viewHolder.tvAddress.setText(ConvertUtils.toString(data.get("Address")));
                viewHolder.tvPlaceName.setText(ConvertUtils.toString(data.get("TypeName")));
                ImageUtils.loadImageByGlide(getContext(), false, 0, 0, ConvertUtils.toString(data.get("TypeLogo")), R.drawable.ic_location_address, R.drawable.ic_location_address, viewHolder.imgPlaceLogo, true);

            }
        }, listObjectsHistory, new BaseRecyclerAdapter.OnClickListener() {
            @Override
            public void onClick(View v, int position, Object o) {
                txtSearch.clearFocus();
                viewPlace((JsonObject) o);
            }
        });
        if (listObjectsHistory != null && listObjectsHistory.size() > 0)
            resultSearch.setVisibility(View.VISIBLE);
        else
            resultSearch.setVisibility(View.GONE);
        adapterHistory.bindData(recyclerView);
        return view;
    }

    void viewPlace(JsonObject jsonObject) {
        if (listObjectsHistory.size() > 0) {
            int idItemClick = ConvertUtils.toInt(jsonObject.get("ID"));
            for (int i = 0; i < listObjectsHistory.size(); i++) {
                JsonObject jsonHistory = listObjectsHistory.get(i);
                int id = ConvertUtils.toInt(jsonHistory.get("ID"));
                if (idItemClick == id) {
                    listObjectsHistory.remove(i);
                }
            }
        }
        listObjectsHistory.add(0, jsonObject);
        if (listObjectsHistory.size() > 10)
            listObjectsHistory.remove(listObjectsHistory.size() - 1);
        Gson gson = new GsonBuilder()
                .setExclusionStrategies(new SerializedNameOnlyStrategy())
                .create();
        String json = gson.toJson(listObjectsHistory);
        DataCacheInfo.setData(getContext(), DataCacheInfo.EnumCacheType.History_Place, json);
        DetailLocationActivity.open(getContext(), jsonObject);
        adapterHistory.notifyDataSetChanged();
    }

    @OnClick({R.id.btnBack, R.id.parent})
    void back() {
        getFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
    }

    @BindView(R.id.btnClear)
    View mClear;

    @OnClick(R.id.btnClear)
    void clear() {
        txtSearch.setText("");
    }

    @BindView(R.id.txtSearch)
    EditText txtSearch;
    @BindView(R.id.resultSearch)
    View resultSearch;
    @BindView(R.id.list_search)
    RecyclerView recyclerView;

    public static Fragment getInstants() {
        return new SearchByNameFragment();
    }

    @Override
    public void onResume() {
        super.onResume();
    }


    private ArrayList<JsonObject> getAutocomplete(CharSequence constraint) {
        BaseReturnFunctionEntity<JsonArray> result = new LocationInfo().search(constraint.toString(), 1, 30);
        if (result.isTrue())
            return ConvertUtils.toArrayList(result.getData());
        return new ArrayList<>();
    }

    public class SerializedNameOnlyStrategy implements ExclusionStrategy {

        @Override
        public boolean shouldSkipField(FieldAttributes f) {
            return f.getAnnotation(SerializedName.class) == null;
        }

        @Override
        public boolean shouldSkipClass(Class<?> clazz) {
            return false;
        }
    }

    @Override
    public Filter getFilter() {
        Filter filter = new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                if (constraint == null || constraint.toString().isEmpty())
                    return null;

                FilterResults results = new FilterResults();
                // Skip the autocomplete query if no constraints are given.
                if (constraint != null) {
                    // Query the autocomplete API for the (constraint) search string.
                    ArrayList<JsonObject> mResultList = getAutocomplete(constraint);
                    if (mResultList != null) {
                        // The API successfully returned results.
                        results.values = mResultList;
                        results.count = mResultList.size();
                    }
                }
                return results;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                listObjects.clear();
                if (results != null && results.count > 0) {
                    listObjects.addAll((ArrayList<JsonObject>) results.values);
                    // The API returned at least one result, update the data.
                }
                adapter.notifyDataSetChanged();
                if (listObjects != null && listObjects.size() > 0) {
                    resultSearch.setVisibility(View.VISIBLE);
                } else {
                    resultSearch.setVisibility(View.GONE);
//                    Toast.makeText(getContext(), getResources().getString(R.string.no_results_found), Toast.LENGTH_LONG).show();
                }

            }
        };
        return filter;
    }

    public class ViewHolder extends BaseViewHolder {
        @BindView(R.id.tvName)
        TextView tvName;
        @BindView(R.id.tvAddress)
        TextView tvAddress;
        @BindView(R.id.tvPlaceName)
        TextView tvPlaceName;
        @BindView(R.id.imgPlaceLogo)
        ImageView imgPlaceLogo;

        public ViewHolder(View itemView) {
            super(itemView);
        }
    }
}
