package com.team500ae.vncapplication.activities.location.fragments;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.UiThread;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStates;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.location.places.GeoDataClient;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.PlaceDetectionClient;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.team500ae.library.adapters.BaseRecyclerAdapter;
import com.team500ae.library.layoutmanager.MyLinearLayoutManager;
import com.team500ae.library.utils.AndroidPermissionUtils;
import com.team500ae.library.utils.ConvertUtils;
import com.team500ae.library.utils.ImageUtils;
import com.team500ae.library.utils.RecyclerViewUtils;
import com.team500ae.library.utils.StringUtils;
import com.team500ae.vncapplication.R;
import com.team500ae.vncapplication.activities.location.AddLocationActivity;
import com.team500ae.vncapplication.activities.location.AddNewLocationActivity;
import com.team500ae.vncapplication.activities.location.DetailLocationActivity;
import com.team500ae.vncapplication.activities.login.LoginActivity;
import com.team500ae.vncapplication.base.BaseActivity;
import com.team500ae.vncapplication.data_access.location.LocationInfo;
import com.team500ae.vncapplication.data_access.user.UserInfo;
import com.team500ae.vncapplication.models.BaseReturnFunctionEntity;
import com.team500ae.vncapplication.services.ServiceLocation;
import com.team500ae.vncapplication.utils.GPSTrackerUtils;
import com.team500ae.vncapplication.utils.KeyboardUtils;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by maina on 1/29/2018.
 */

public class LocationSearchByTypeFragment extends Fragment implements OnMapReadyCallback, GoogleMap.OnMarkerClickListener, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {
    private static final float DEFAULT_ZOOM = 15f;
    protected static final int REQUEST_CHECK_SETTINGS = 0x1;
    private final static int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
    private Place place;
    private FusedLocationProviderClient mFusedLocationProviderClient;
    private GeoDataClient mGeoDataClient;
    private PlaceDetectionClient mPlaceDetectionClient;
    private GoogleMap mMap;
    private Location mLastKnownLocation;
    private GoogleApiClient mGoogleApiClient;
    private BaseRecyclerAdapter<ViewHolder, JsonObject> adapter;
    private ArrayList<JsonObject> listData;
    private int type;
    private String name;
    @BindView(R.id.loading)
    View loading;
    int page = 1;
    int size = 10;
    boolean isLoading = false;
    private double lat;
    private double lon;


    @BindView(R.id.coordinatorlayout)
    CoordinatorLayout coordinatorLayout;
    @BindView(R.id.tvTypeName)
    TextView tvTypeName;
    private View bottomSheet;
    private View mapView;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        listData = new ArrayList<>();

        View view = inflater.inflate(R.layout.fragment_location_search_by_type, container, false);
        ButterKnife.bind(this, view);
        KeyboardUtils.setupUI(view, getActivity());
        SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);
        mapView = mapFragment.getView();
        mapFragment.getMapAsync(this);
        mGoogleApiClient = new GoogleApiClient.Builder(getActivity())
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();
        mGoogleApiClient.connect();

// Construct a GeoDataClient.
        mGeoDataClient = Places.getGeoDataClient(getContext(), null);

        // Construct a PlaceDetectionClient.
        mPlaceDetectionClient = Places.getPlaceDetectionClient(getContext(), null);

        // Construct a FusedLocationProviderClient.
        mFusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(getContext());
        recyclerView.setLayoutManager(new MyLinearLayoutManager(getContext()));
        adapter = new BaseRecyclerAdapter<ViewHolder, JsonObject>(R.layout.item_place_2, new BaseRecyclerAdapter.BaseViewHolder<ViewHolder, JsonObject>() {
            @Override
            public ViewHolder getViewHolder(View v) {
                return new ViewHolder(v);
            }

            @Override
            public void bindData(RecyclerView recyclerView, ViewHolder viewHolder, JsonObject data, int position) {
                viewHolder.tvName.setText(ConvertUtils.toString(data.get("Title")));
                if (StringUtils.isEmpty(ConvertUtils.toString(data.get("Address")))) {
                    viewHolder.tvAddress.setVisibility(View.GONE);
                } else {
                    viewHolder.tvAddress.setVisibility(View.VISIBLE);
                }

                viewHolder.tvAddress.setText(ConvertUtils.toString(data.get("Address")));
                ImageUtils.loadImageByGlide(getContext(), false, 0, 0, ConvertUtils.toString(data.get("TypeLogo")), R.drawable.ic_location_address, R.drawable.ic_location_address, viewHolder.imgPlaceLogo, true);
            }
        }, listData, new BaseRecyclerAdapter.OnClickListener() {
            @Override
            public void onClick(View v, int position, Object o) {
                DetailLocationActivity.open(getContext(), (JsonObject) o);
            }
        });
        adapter.bindData(recyclerView);

        bottomSheet = coordinatorLayout.findViewById(R.id.bottom_sheet);
        BottomSheetBehavior behavior = BottomSheetBehavior.from(bottomSheet);
        behavior.setPeekHeight(getResources().getDimensionPixelSize(R.dimen.list_location_height));
        behavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {
                if (behavior.getState() == BottomSheetBehavior.STATE_EXPANDED) {
                    bottomSheet.setPadding(0, getResources().getDimensionPixelOffset(R.dimen.bottom_sheet_padding_top), 0, 0);
                    btnAddLocation.setVisibility(View.GONE);
                } else {
                    bottomSheet.setPadding(0, 0, 0, 0);
                    btnAddLocation.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {
                // React to dragging events
            }
        });

        Bundle bundle = getArguments();
        if (bundle != null) {
            type = bundle.getInt("TYPE");
            name = bundle.getString("NAME");
        }
        tvTypeName.setText((getResources().getString(R.string.list) + " " + name.trim()).toUpperCase());

        RecyclerViewUtils recyclerViewUtils = new RecyclerViewUtils(new RecyclerViewUtils.RecyclerViewUtilsListener() {
            @Override
            public void onScrollToEnd() {
                if (!isLoading) {
                    page = page + 1;
                    getData(lat, lon, page, size);
                }
            }
        });
        recyclerViewUtils.setUp(recyclerView);
        return view;
    }

    public void getData(double lat, double lon, final int page, final int size) {
        new AsyncTask<Void, JsonArray, Void>() {

            @Override
            protected void onPreExecute() {
                loading.setVisibility(View.VISIBLE);
                isLoading = true;
                super.onPreExecute();
            }

            @Override
            protected Void doInBackground(Void... voids) {
                BaseReturnFunctionEntity<JsonArray> result = new LocationInfo().near(type, lat, lon, page, size);
                if (result.isTrue())
                    publishProgress(result.getData());
                else
                    publishProgress(new JsonArray());
                return null;
            }

            @Override
            protected void onProgressUpdate(JsonArray... values) {
                if (values.length > 0 && values[0] != null) {
                    if (page == 1) {
                        listData.clear();
                        adapter.notifyDataSetChanged();
                    }
                    bindData(values[0]);
                    showLocation(page, values[0]);
                }
                super.onProgressUpdate(values);
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                loading.setVisibility(View.GONE);
                isLoading = false;
                super.onPostExecute(aVoid);
            }
        }.execute();
    }

    void bindData(JsonArray jsonArray) {
        for (int i = 0; i < jsonArray.size(); i++) {
            JsonObject jsonObject = ConvertUtils.toJsonObject(jsonArray.get(i));
            listData.add(jsonObject);
            adapter.notifyItemInserted(listData.size() - 1);
        }
    }

    @Override
    public void onMapReady(GoogleMap map) {
        mMap = map;
        mMap.setOnMarkerClickListener(this);

        // Do other setup activities here too, as described elsewhere in this tutorial.

        // Turn on the My Location layer and the related control on the map.
        View locationButton = ((View) mapView.findViewById(Integer.parseInt("1")).getParent()).findViewById(Integer.parseInt("2"));
        // and next place it, on bottom right (as Google Maps app)
        RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams)
                locationButton.getLayoutParams();
        // position on right bottom
        layoutParams.setMargins(0, getResources().getDimensionPixelOffset(R.dimen.marginTopMyLocation), 30, 0);

        updateLocationUI();

        // Get the current location of the device and set the position of the map.
        BaseActivity.requestPermission(getContext(), new AndroidPermissionUtils.OnCallbackRequestPermission() {

            @Override
            public void onSuccess() {
                LocationManager manager = (LocationManager) getContext().getSystemService(Context.LOCATION_SERVICE);
                if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                    settingsRequest();
                } else {
                    getDeviceLocation();
                }
            }

            @Override
            public void onFailed() {

            }
        }, AndroidPermissionUtils.TypePermission.PERMISSION_ACCESS_COARSE_LOCATION, AndroidPermissionUtils.TypePermission.PERMISSION_ACCESS_COARSE_LOCATION);

    }

    public void settingsRequest() {
        LocationRequest locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(30 * 1000);
        locationRequest.setFastestInterval(5 * 1000);
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(locationRequest);
        builder.setAlwaysShow(true); //this is the key ingredient
        PendingResult<LocationSettingsResult> result =
                LocationServices.SettingsApi.checkLocationSettings(mGoogleApiClient, builder.build());
        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(LocationSettingsResult result) {
                final Status status = result.getStatus();
                final LocationSettingsStates state = result.getLocationSettingsStates();
                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.SUCCESS:
                        try {
                            status.startResolutionForResult(getActivity(), REQUEST_CHECK_SETTINGS);
                        } catch (IntentSender.SendIntentException e) {
                        }
                        break;
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        try {
                            status.startResolutionForResult(getActivity(), REQUEST_CHECK_SETTINGS);
                        } catch (IntentSender.SendIntentException e) {
                            e.printStackTrace();
                        }
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        break;
                }
            }
        });
    }

    private void getDeviceLocation() {
        /*
         * Get the best and most recent location of the device, which may be null in rare
         * cases when a location is not available.
         */
        try {
            Task locationResult = mFusedLocationProviderClient.getLastLocation();
            locationResult.addOnCompleteListener(getActivity(), new OnCompleteListener() {
                @Override
                public void onComplete(@NonNull Task task) {
                    if (task.isSuccessful()) {
                        // Set the map's camera position to the current location of the device.
                        mLastKnownLocation = (Location) task.getResult();
                        if (mLastKnownLocation != null) {
                            ServiceLocation.startActionUpdateLocation(getContext(), mLastKnownLocation.getLatitude(), mLastKnownLocation.getLongitude());
                            GPSTrackerUtils.saveCurrentLocation(getContext(), mLastKnownLocation.getLatitude(), mLastKnownLocation.getLongitude());
                            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(
                                    new LatLng(mLastKnownLocation.getLatitude(),
                                            mLastKnownLocation.getLongitude()), DEFAULT_ZOOM));
                            getLocation(mLastKnownLocation.getLatitude(), mLastKnownLocation.getLongitude());

                        }
                    } else {
                        mMap.getUiSettings().setMyLocationButtonEnabled(false);
                    }
                }
            });
        } catch (SecurityException e) {
        }
    }


    public void getLocation(double lat, double lon) {
        this.lat = lat;
        this.lon = lon;
        getData(lat, lon, page, size);
    }

    void showLocation(int page, JsonArray jsonArray) {
        if (page == 1)
            mMap.clear();
        JsonObject jsonObject;
        for (int i = 0; i < jsonArray.size(); i++) {
            MarkerOptions markerOptions = new MarkerOptions();
            jsonObject = ConvertUtils.toJsonObject(jsonArray.get(i));
            double lat = ConvertUtils.toDouble(jsonObject.get("Lat"));
            double lng = ConvertUtils.toDouble(jsonObject.get("Lon"));
            String placeName = ConvertUtils.toString(jsonObject.get("Title"));
            LatLng latLng = new LatLng(lat, lng);
            markerOptions.position(latLng);
            markerOptions.title(placeName);
            Marker marker = mMap.addMarker(markerOptions);
            marker.setTag(jsonObject);
            loadImageByGlide(getContext(), ConvertUtils.toString(jsonObject.get("TypeLogo")), marker);
//            mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
        }
    }

    @UiThread
    public void loadImageByGlide(Context context, @NonNull String url, Marker marker) {
        try {
            RequestOptions options = new RequestOptions()
                    .centerCrop()
                    .priority(Priority.HIGH).dontAnimate().override(40, 40).centerCrop();

            options = options.diskCacheStrategy(DiskCacheStrategy.ALL);
            Glide.with(context).load(url).apply(options).into(new SimpleTarget<Drawable>() {
                @Override
                public void onResourceReady(Drawable resource, Transition<? super Drawable> transition) {
                    try {
                        BitmapDescriptor markerIcon = getMarkerIconFromDrawable(resource);
                        marker.setIcon(markerIcon);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        } catch (OutOfMemoryError e) {
            e.printStackTrace();
        }


    }

    private BitmapDescriptor getMarkerIconFromDrawable(Drawable drawable) {
        Canvas canvas = new Canvas();
        Bitmap bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        canvas.setBitmap(bitmap);
        drawable.setBounds(0, 0, drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight());
        drawable.draw(canvas);
        return BitmapDescriptorFactory.fromBitmap(bitmap);
    }

    private void updateLocationUI() {
        if (mMap == null) {
            return;
        }
        BaseActivity.requestPermission(getContext(), new AndroidPermissionUtils.OnCallbackRequestPermission() {
            @SuppressLint("MissingPermission")
            @Override
            public void onSuccess() {
                mMap.setMyLocationEnabled(true);
                mMap.getUiSettings().setMyLocationButtonEnabled(true);
            }

            @SuppressLint("MissingPermission")
            @Override
            public void onFailed() {
                mMap.setMyLocationEnabled(false);
                mMap.getUiSettings().setMyLocationButtonEnabled(false);
                mLastKnownLocation = null;
            }
        }, AndroidPermissionUtils.TypePermission.PERMISSION_ACCESS_COARSE_LOCATION, AndroidPermissionUtils.TypePermission.PERMISSION_ACCESS_FINE_LOCATION);
    }

    @OnClick(R.id.btnBack)
    void menuClick() {
        getActivity().onBackPressed();
    }

    @BindView(R.id.btnAddLocation)
    FloatingActionButton btnAddLocation;

    @OnClick(R.id.btnAddLocation)
    void addLocation() {
        if (UserInfo.isLogin()) {
//            AddLocationActivity.openChannel(getContext());
            AddNewLocationActivity.open(getContext());
        } else {
            LoginActivity.open(getContext());
        }
    }

    @OnClick({R.id.txtSearch})
    void search() {
        getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.lnFragment, SearchByNameFragment.getInstants(), "searchFriend").addToBackStack(null).commit();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    public static Fragment getInstants(int type, String name) {
        LocationSearchByTypeFragment fragment = new LocationSearchByTypeFragment();
        Bundle bunder = new Bundle();
        bunder.putInt("TYPE", type);
        bunder.putString("NAME", name);
        fragment.setArguments(bunder);
        return fragment;
    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        try {
            JsonObject jsonObject = (JsonObject) marker.getTag();
            DetailLocationActivity.open(getContext(), jsonObject);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case REQUEST_CHECK_SETTINGS:
                switch (resultCode) {
                    case Activity.RESULT_OK:
                        getDeviceLocation();
                        break;
                }
        }
    }

    @BindView(R.id.recyclerListTypeLocation)
    RecyclerView recyclerView;

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tvName)
        TextView tvName;
        @BindView(R.id.tvAddress)
        TextView tvAddress;
        @BindView(R.id.imgPlaceLogo)
        ImageView imgPlaceLogo;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
