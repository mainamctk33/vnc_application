package com.team500ae.vncapplication.activities.location;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.google.gson.JsonObject;
import com.team500ae.library.utils.ConvertUtils;
import com.team500ae.vncapplication.R;
import com.team500ae.vncapplication.activities.location.fragments.DetailLocationFragment;
import com.team500ae.vncapplication.base.BaseActivity;

public class DetailLocationActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_location);
        String detailLocation = getIntent().getStringExtra("DETAIL_LOCATION");
        JsonObject jsonObject = ConvertUtils.toJsonObject(detailLocation);
        getSupportFragmentManager().beginTransaction().replace(R.id.linearLayout, DetailLocationFragment.getInstants(jsonObject),"detail_location").commit();
        showBackButton();
    }
    public static void open(Context context, JsonObject jsonObject)
    {
        Intent intent  = new Intent(context, DetailLocationActivity.class);
        intent.putExtra("DETAIL_LOCATION", ConvertUtils.toJson(jsonObject));
        context.startActivity(intent);
    }
}
