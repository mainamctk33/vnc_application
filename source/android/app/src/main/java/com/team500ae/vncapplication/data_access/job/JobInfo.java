package com.team500ae.vncapplication.data_access.job;

import android.content.Context;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.team500ae.vncapplication.constants.Constants;
import com.team500ae.vncapplication.data_access.ClientInfo;
import com.team500ae.vncapplication.models.BaseReturnFunctionEntity;
import com.team500ae.vncapplication.models.ServiceResponseEntity;

import java.net.SocketTimeoutException;
import java.net.UnknownHostException;

/**
 * Created by SVIP on 11/30/2017.
 */

public class JobInfo  {
    public ServiceResponseEntity<JsonArray> getAll(Context context, int page, int size, int type) {
        try {
            ClientInfo.DataResponse dataResponse = new ClientInfo().requestApi(ClientInfo.MethodType.GET, Constants.API_JOB_GETALL + "?type=" + type + "&page=" + page + "&size=" + size);
            if (dataResponse.isOK()) {
                ServiceResponseEntity<JsonArray> result = dataResponse.getData(new TypeToken<ServiceResponseEntity<JsonArray>>() {
                }.getType());
                return result;
            }
        } catch (UnknownHostException e) {
            e.printStackTrace();
        } catch (SocketTimeoutException e) {
            e.printStackTrace();
        }
        return null;
    }

    public ServiceResponseEntity<JsonArray> getAllWithCity(Context context, int page, int size, int type) {
        try {
            if(type==-1) size =99999;
            //TODO: thanhtam.tr can sua lai cai nay
            ClientInfo.DataResponse dataResponse = new ClientInfo().requestApi(ClientInfo.MethodType.GET, Constants.API_JOB_GETALLWITHCITY + "?type=" + type + "&page=" + page + "&size=" + size);
            //ClientInfo.DataResponse dataResponse = new ClientInfo().requestApi(ClientInfo.MethodType.GET, Constants.API_JOB_GETALL + "?type=" + type + "&page=" + page + "&size=" + size);
            if (dataResponse.isOK()) {
                ServiceResponseEntity<JsonArray> result = dataResponse.getData(new TypeToken<ServiceResponseEntity<JsonArray>>() {
                }.getType());
                return result;
            }
        } catch (UnknownHostException e) {
            e.printStackTrace();
        } catch (SocketTimeoutException e) {
            e.printStackTrace();
        }
        return null;
    }

    public ServiceResponseEntity<JsonArray> searchWithCity(Context context, int page, int size, String title, int country, int city , int type, int level, int salaryFrom, int salaryTo, String company, int sort) {
        try {
            if(type==-1) size =99999;
            ClientInfo.DataResponse dataResponse = new ClientInfo().requestApi(ClientInfo.MethodType.GET, Constants.API_JOB_SEARCHWITHCITY + "?page=" + page + "&size=" + size
                    +"&title="+ title
                    +"&country=" + country +"&city=" + city+"&type=" + type
                    +"&level=" + level +"&salaryFrom=" + salaryFrom +"&salaryTo=" + salaryTo
                    +"&company=" + company +"&sort=" + sort);
            if (dataResponse.isOK()) {
                ServiceResponseEntity<JsonArray> result = dataResponse.getData(new TypeToken<ServiceResponseEntity<JsonArray>>() {
                }.getType());
                return result;
            }
        } catch (UnknownHostException e) {
            e.printStackTrace();
        } catch (SocketTimeoutException e) {
            e.printStackTrace();
        }
        return null;
    }

    public BaseReturnFunctionEntity<Boolean> bookmark(Context applicationContext, int id, boolean bookmark) {
        try {
            JsonObject jsonObject = new JsonObject();
            jsonObject.addProperty("bookmark", bookmark);
            ClientInfo.DataResponse dataResponse = new ClientInfo().requestApi(ClientInfo.MethodType.PUT, Constants.API_JOB_BOOKMARK + "/" + id, jsonObject);
            if (dataResponse.isOK()) {
                ServiceResponseEntity<Object> result = dataResponse.getData(new TypeToken<ServiceResponseEntity<Object>>() {
                }.getType());
                if (result.getStatus() == 0)
                    return new BaseReturnFunctionEntity<>(BaseReturnFunctionEntity.StatusFunction.TRUE, true);
            }
            return new BaseReturnFunctionEntity<>(BaseReturnFunctionEntity.StatusFunction.FALSE);
        } catch (UnknownHostException e) {
            e.printStackTrace();
            return new BaseReturnFunctionEntity<>(BaseReturnFunctionEntity.StatusFunction.EXCEPTION, e.getMessage());
        } catch (SocketTimeoutException e) {
            e.printStackTrace();
            return new BaseReturnFunctionEntity<>(BaseReturnFunctionEntity.StatusFunction.EXCEPTION, e.getMessage());
        }
    }


    public BaseReturnFunctionEntity<JsonObject> getById(Context context, int id) {
        try {
            ClientInfo.DataResponse dataResponse = new ClientInfo().requestApi(ClientInfo.MethodType.GET, Constants.API_JOB_GET + "/" + id);
            if (dataResponse.isOK()) {
                ServiceResponseEntity<JsonObject> result = dataResponse.getData(new TypeToken<ServiceResponseEntity<JsonObject>>() {
                }.getType());
                if (result.getStatus() == 0) {
                    return new BaseReturnFunctionEntity<>(BaseReturnFunctionEntity.StatusFunction.TRUE, result.getData());
                }
            }
            return new BaseReturnFunctionEntity<>(BaseReturnFunctionEntity.StatusFunction.FALSE);
        } catch (UnknownHostException e) {
            e.printStackTrace();
            return new BaseReturnFunctionEntity<>(BaseReturnFunctionEntity.StatusFunction.EXCEPTION, e.getMessage());
        } catch (SocketTimeoutException e) {
            e.printStackTrace();
            return new BaseReturnFunctionEntity<>(BaseReturnFunctionEntity.StatusFunction.EXCEPTION, e.getMessage());
        }
    }



        public BaseReturnFunctionEntity<JsonObject> getById2(Context context, int id) {
        try {
            //ClientInfo.DataResponse dataResponse = new ClientInfo().requestApi(ClientInfo.MethodType.GET, Constants.API_JOB_GET + "/" + id);
            ClientInfo.DataResponse dataResponse = new ClientInfo().requestApi(ClientInfo.MethodType.GET, Constants.API_NEWS_GET + "/" + 10);
            //ClientInfo.DataResponse dataResponse = new ClientInfo().requestApi(ClientInfo.MethodType.GET,  "http://onlineapi.site/job/get/10");
            if (dataResponse.isOK()) {
                ServiceResponseEntity<JsonObject> result = dataResponse.getData(new TypeToken<ServiceResponseEntity<JsonObject>>() {
                }.getType());
                if (result.getStatus() == 0) {
                    return new BaseReturnFunctionEntity<>(BaseReturnFunctionEntity.StatusFunction.TRUE, result.getData());
                }
            }
            return new BaseReturnFunctionEntity<>(BaseReturnFunctionEntity.StatusFunction.FALSE);
        } catch (UnknownHostException e) {
            e.printStackTrace();
            return new BaseReturnFunctionEntity<>(BaseReturnFunctionEntity.StatusFunction.EXCEPTION, e.getMessage());
        } catch (SocketTimeoutException e) {
            e.printStackTrace();
            return new BaseReturnFunctionEntity<>(BaseReturnFunctionEntity.StatusFunction.EXCEPTION, e.getMessage());
        }

    }
    public BaseReturnFunctionEntity<Boolean> create(Context applicationContext, String title, String salary, String description,
                                                    String requirement, String phone, String email, String createdBy, int city,
                                                    int country, int level, int type, String company, String thumbnail, double lat, double lon, String address) {
        try {
            JsonObject jsonObject = new JsonObject();
            jsonObject.addProperty("Title", title);
            jsonObject.addProperty("Salary", salary);
            jsonObject.addProperty("Description", description);
            jsonObject.addProperty("Requirement", requirement);
            jsonObject.addProperty("Phone", phone);
            jsonObject.addProperty("Email", email);
            jsonObject.addProperty("Address", address);
            jsonObject.addProperty("CreatedBy", createdBy);
            jsonObject.addProperty("City", city);
            jsonObject.addProperty("Level", level);
            jsonObject.addProperty("Type", type);
            jsonObject.addProperty("Company", company);
            jsonObject.addProperty("Thumbnail", thumbnail);
            jsonObject.addProperty("Lat", lat);
            jsonObject.addProperty("Long", lon);


            ClientInfo.DataResponse dataResponse = new ClientInfo().requestApi(ClientInfo.MethodType.POST, Constants.API_JOB_CREATE , jsonObject);
            if (dataResponse.isOK()) {
                ServiceResponseEntity<Object> result = dataResponse.getData(new TypeToken<ServiceResponseEntity<Object>>() {
                }.getType());
                if (result.getStatus() == 0)
                    return new BaseReturnFunctionEntity<>(BaseReturnFunctionEntity.StatusFunction.TRUE, true);
            }
            return new BaseReturnFunctionEntity<>(BaseReturnFunctionEntity.StatusFunction.FALSE);
        } catch (UnknownHostException e) {
            e.printStackTrace();
            return new BaseReturnFunctionEntity<>(BaseReturnFunctionEntity.StatusFunction.EXCEPTION, e.getMessage());
        } catch (SocketTimeoutException e) {
            e.printStackTrace();
            return new BaseReturnFunctionEntity<>(BaseReturnFunctionEntity.StatusFunction.EXCEPTION, e.getMessage());
        }
    }
    public BaseReturnFunctionEntity<Boolean> delete(Context applicationContext, int id) {
        try {

            ClientInfo.DataResponse dataResponse = new ClientInfo().requestApi(ClientInfo.MethodType.DELETE, Constants.API_JOB_DELETE + "/" + id,new JsonObject());
            if (dataResponse.isOK()) {
                ServiceResponseEntity<Object> result = dataResponse.getData(new TypeToken<ServiceResponseEntity<Object>>() {
                }.getType());
                if (result.getStatus() == 0)
                    return new BaseReturnFunctionEntity<>(BaseReturnFunctionEntity.StatusFunction.TRUE, true);
            }
            return new BaseReturnFunctionEntity<>(BaseReturnFunctionEntity.StatusFunction.FALSE);
        } catch (UnknownHostException e) {
            e.printStackTrace();
            return new BaseReturnFunctionEntity<>(BaseReturnFunctionEntity.StatusFunction.EXCEPTION, e.getMessage());
        } catch (SocketTimeoutException e) {
            e.printStackTrace();
            return new BaseReturnFunctionEntity<>(BaseReturnFunctionEntity.StatusFunction.EXCEPTION, e.getMessage());
        }
    }
}
