package com.team500ae.vncapplication.data_access.upload;

import android.content.Context;
import android.net.Uri;
import android.os.AsyncTask;

import com.google.gson.JsonArray;
import com.google.gson.reflect.TypeToken;
import com.team500ae.library.model.DataDictionary;
import com.team500ae.vncapplication.constants.Constants;
import com.team500ae.vncapplication.data_access.ClientInfo;
import com.team500ae.vncapplication.models.BaseReturnFunctionEntity;
import com.team500ae.vncapplication.models.ServiceResponseEntity;

import java.util.ArrayList;

public class BlobUploadProvider implements IUploadProvider {

    @Override
    public void upload(final Context context, final String method, final UploadInfo.UploadListener uploadListener, final ArrayList<Uri> files, final int timeOut) {
         new AsyncTask<Void, Void, ServiceResponseEntity<JsonArray>>()
        {
            @Override
            protected void onPreExecute() {
                try {
                    if (uploadListener != null)
                        uploadListener.beginUpload();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                super.onPreExecute();
            }

            @Override
            protected ServiceResponseEntity<JsonArray> doInBackground(Void... voids) {
                try {
                    ClientInfo.DataResponse dataResponse= new ClientInfo().postFile(context, method,files);
                    if(dataResponse.isOK())
                        return dataResponse.getData(new TypeToken<ServiceResponseEntity<JsonArray>>(){}.getType());
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return null;
            }

            @Override
            protected void onPostExecute(ServiceResponseEntity<JsonArray> aVoid) {
                try {
                    if (uploadListener != null)
                        uploadListener.endUpload();

                    if (aVoid != null && aVoid.getStatus()==0)
                        if (uploadListener != null)
                            uploadListener.onSuccess(aVoid.getData());
                        else
                            uploadListener.onError(aVoid);
                    else uploadListener.onError(aVoid);
                } catch (OutOfMemoryError e) {
                    e.getMessage();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                super.onPostExecute(aVoid);
            }

        }.execute();

    }
}