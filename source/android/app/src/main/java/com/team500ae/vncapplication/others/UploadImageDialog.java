package com.team500ae.vncapplication.others;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.media.tv.TvView;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.EditText;
import android.widget.TextView;

import com.team500ae.vncapplication.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.realm.Case;

public class UploadImageDialog extends Dialog {


    @BindView(R.id.dialogUploadImage_etUrl)
    EditText etUrl;
    @BindView(R.id.dialogUploadImage_tvCancel)
    TextView tvCancel;
    @BindView(R.id.dialogUploadImage_tvOk)
    TextView tvOk;
    private DialogListener clickListener;

    public void setOnItemClickListener(DialogListener listener) {
        this.clickListener = listener;
    }

    public UploadImageDialog(Context context) {
        super(context, android.R.style.Theme_Holo_Dialog);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        if (getWindow() != null) {
            getWindow().setDimAmount(0.3f);
            getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        }
        setContentView(R.layout.dialog_upload_image);

        ButterKnife.bind(this);
        tvOk.setText(android.R.string.yes);
        tvCancel.setText(android.R.string.no);
    }

    @OnClick({R.id.dialogUploadImage_tvButtonChooseImage, R.id.dialogUploadImage_tvCancel, R.id.dialogUploadImage_tvOk})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.dialogUploadImage_tvCancel:
                dismiss();
                break;
            case R.id.dialogUploadImage_tvOk:
                if (clickListener != null)
                    clickListener.onConfirmClicked();
                dismiss();
                break;
            case R.id.dialogUploadImage_tvButtonChooseImage:
                if (clickListener != null)
                    clickListener.onButtonChooseImage();
                dismiss();
                break;

        }
    }

    public String getEditText() {
        if (etUrl.getText().length() > 0)
            return etUrl.getText().toString();
        return "";
    }
}
