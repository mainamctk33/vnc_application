package com.team500ae.vncapplication.models.realm_models;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by maina on 11/24/2017.
 */

public class BookmarkEntity extends RealmObject {
    @PrimaryKey
    String bookmarkId;
    int id;
    String data;
    String userId;
    int bookmarkType;
    boolean bookmark;

    public String getBookmarkId() {
        return bookmarkId;
    }

    public void setBookmarkId(String bookmarkId) {
        this.bookmarkId = bookmarkId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public int getBookmarkType() {
        return bookmarkType;
    }

    public void setBookmarkType(int bookmarkType) {
        this.bookmarkType = bookmarkType;
    }

    public boolean isBookmark() {
        return bookmark;
    }

    public void setBookmark(boolean bookmark) {
        this.bookmark = bookmark;
    }
}
