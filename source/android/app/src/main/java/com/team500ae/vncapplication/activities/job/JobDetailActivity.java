package com.team500ae.vncapplication.activities.job;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.JsonObject;
import com.onurciner.toastox.ToastOXDialog;
import com.team500ae.library.utils.ConvertUtils;
import com.team500ae.library.utils.ImageUtils;
import com.team500ae.vncapplication.R;
import com.team500ae.vncapplication.activities.login.LoginActivity;
import com.team500ae.vncapplication.base.BaseActivity;
import com.team500ae.vncapplication.constants.Constants;
import com.team500ae.vncapplication.data_access.BookmarkInfo;
import com.team500ae.vncapplication.data_access.job.JobInfo;
import com.team500ae.vncapplication.data_access.user.UserInfo;
import com.team500ae.vncapplication.models.BaseReturnFunctionEntity;
import com.team500ae.vncapplication.services.ServiceJob;
import com.team500ae.vncapplication.utils.ShareUtils;

import butterknife.BindView;
import butterknife.OnClick;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 * Created by SVIP on 12/5/2017.
 */

public class JobDetailActivity extends BaseActivity implements com.team500ae.library.activities.BaseActivity.OnCallBackBroadcastListener {
    private int id;
    private String createdBy;
    private String title;
    private Menu mMenu;


    @BindView(R.id.tvTitle)
    TextView tvTitle;
    @BindView(R.id.tvSalary)
    TextView tvSalary;
    @BindView(R.id.tvLevel)
    TextView tvLevel;
    @BindView(R.id.tvType)
    TextView tvType;
    @BindView(R.id.tvAddress)
    TextView tvAddress;
    @BindView(R.id.tvCompany)
    TextView tvCompany;
    @BindView(R.id.tvDescription)
    TextView tvDescription;
    @BindView(R.id.tvRequirement)
    TextView tvRequirement;
    @BindView(R.id.tvPhone)
    TextView tvPhone;
    @BindView(R.id.tvEmail)
    TextView tvEmail;
    @BindView(R.id.imgThumbnail)
    ImageView imgThumbnail;
    @BindView(R.id.btnBookmark)
    ImageView imgBookmark;

    public static void open(Context context, int id) {
        Intent intent = new Intent(context, JobDetailActivity.class);
        intent.putExtra("ID", id);
        // intent.putExtra("TITLE", title);
        context.startActivity(intent);
    }

    public static void open(Context context, int id, String title) {
        Intent intent = new Intent(context, JobDetailActivity.class);
        intent.putExtra("ID", id);
        intent.putExtra("TITLE", title);
        context.startActivity(intent);
    }


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        setContentView(R.layout.activity_detail_job);
        super.onCreate(savedInstanceState);
        getSupportActionBar().setTitle(R.string.job_detail_title);

        showBackButton();
        id = getIntent().getIntExtra("ID", 0);
        //title = getIntent().getStringExtra("TITLE");
        setTitle(getString(R.string.job_detail_title));

        new AsyncTask<Void, Void, BaseReturnFunctionEntity<JsonObject>>() {
            ProgressDialog progDialog = new ProgressDialog(JobDetailActivity.this);

            @Override
            protected void onPreExecute() {
                // showProgress();
                super.onPreExecute();
                progDialog.setMessage(getString(R.string.loading));
                progDialog.setIndeterminate(false);
                progDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                progDialog.setCancelable(true);
                progDialog.show();

            }

            @Override
            protected BaseReturnFunctionEntity<JsonObject> doInBackground(Void... voids) {
                BaseReturnFunctionEntity<JsonObject> data;
                try {
                    data = new JobInfo().getById(getActivity(), id);
                } catch (Exception ex) {
                    return null;
                }
                return data;
            }

            @Override
            protected void onPostExecute(BaseReturnFunctionEntity<JsonObject> data) {
                //super.onPostExecute(data);
                if (progDialog.isShowing()) {
                    progDialog.dismiss();
                }
                if (data != null && data.isTrue()) {

                    JsonObject o = data.getData();
                    JsonObject o2 = ConvertUtils.toJsonObject(o.get("Job"));
                    createdBy = ConvertUtils.toString(o2.get("CreatedBy"));
                    if (UserInfo.getCurrentUserId().equals(createdBy)) {
                        menuJobDelete.setVisible(true);
                    }

                    tvTitle.setText(ConvertUtils.toString(o2.get("Title")));
                    tvSalary.setText(ConvertUtils.toString(o2.get("Salary")));
                    tvLevel.setText(ConvertUtils.toString(o.get("Level")));
                    tvType.setText(ConvertUtils.toString(o.get("Type")));
                    tvAddress.setText(ConvertUtils.toString(o.get("City")));
                    tvCompany.setText(ConvertUtils.toString(o.get("Company")));
                    tvDescription.setText(ConvertUtils.toString(o2.get("Description")));
                    tvRequirement.setText(ConvertUtils.toString(o2.get("Requirement")));
                    //String phoneNumber = ConvertUtils.toString(o2.get("Phone"))";
                    tvPhone.setText(ConvertUtils.toString(o2.get("Phone")));
                   /* if(phoneNumber.length()>0)
                    {
                        tvPhone.setTextColor(getResources().getColor(R.color.blue_50));
                        tvPhone.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                             str
                            }
                        });
                    }*/
                    tvEmail.setText(ConvertUtils.toString(o2.get("Email")));
                    ImageUtils.loadImageByGlide(getApplicationContext(), true, 300, 300, ConvertUtils.toString(o2.get("Thumbnail")), R.drawable.img_no_image, R.drawable.img_no_image, imgThumbnail, true);
//                    Glide.with(getActivity()).load(ConvertUtils.toString(o2.get("Thumbnail"))).apply(new RequestOptions().placeholder(R.drawable.img_no_image)).into(imgThumbnail);
                }


                //hideProgress();
            }


        }.execute();


        BaseReturnFunctionEntity<Boolean> bookmark = new BookmarkInfo().isBookmark(getActivity(), getRealm(), id, UserInfo.getCurrentUserId(), BookmarkInfo.BookmarkType.Job);
        hasBookmark = bookmark.isTrue() ? bookmark.data : false;
        imgBookmark.setImageDrawable(ContextCompat.getDrawable(getActivity(), hasBookmark ? R.drawable.ic_like_check : R.drawable.ic_like));
        registerBroadcastReceiver(new ReceiverItem(Constants.ACTION_LOGIN, this));
    }

    boolean hasBookmark;

    @OnClick(R.id.btnBookmark)
    void bookmark() {
        if (!UserInfo.isLogin()) {
            LoginActivity.open(getActivity());
            return;
        } else {
            hasBookmark = !hasBookmark;
            ServiceJob.startActionBookmark(getActivity(), id, hasBookmark);
            imgBookmark.setImageDrawable(ContextCompat.getDrawable(getActivity(), hasBookmark ? R.drawable.ic_like_check : R.drawable.ic_like));
        }
    }

    @OnClick(R.id.llShare)
    void share() {
        ShareUtils.shareFacebook(getActivity(), Constants.SERVER + "/applink/job/", "vnc");
    }


    @Override
    public void onCallBackBroadcastListener(Context context, String action, Intent intent) {
        switch (action) {
            case Constants.ACTION_LOGIN:
                BaseReturnFunctionEntity<Boolean> bookmark = new BookmarkInfo().isBookmark(getActivity(), getRealm(), id, UserInfo.getCurrentUserId(), BookmarkInfo.BookmarkType.Job);
                hasBookmark = bookmark.isTrue() ? bookmark.data : false;
                imgBookmark.setImageDrawable(ContextCompat.getDrawable(getActivity(), hasBookmark ? R.drawable.ic_like_check : R.drawable.ic_like));
                break;
        }
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    MenuItem menuJobDelete;

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        mMenu = menu;
        getMenuInflater().inflate(R.menu.menu_job_detail, menu);
        menuJobDelete = menu.findItem(R.id.action_job_delete);
        menuJobDelete.setVisible(false);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_job_delete:
                new ToastOXDialog.Build(getActivity()).setTitle(R.string.confirm).setContent(getString(R.string.job_dialog_delete))
                        .setPositiveText(R.string.yes).setPositiveBackgroundColorResource(R.color.btnyes).setPositiveTextColorResource(R.color.black).onPositive(new ToastOXDialog.ButtonCallback() {
                    @Override
                    public void onClick(@NonNull ToastOXDialog toastOXDialog) {
                        new AsyncTask<Void, Void, BaseReturnFunctionEntity<Boolean>>() {
                            ProgressDialog progDialog = new ProgressDialog(JobDetailActivity.this);

                            @Override
                            protected void onPreExecute() {
                                // showProgress();
                                super.onPreExecute();
                                progDialog.setMessage(getString(R.string.loading));
                                progDialog.setIndeterminate(false);
                                progDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                                progDialog.setCancelable(true);
                                progDialog.show();

                            }

                            @Override
                            protected BaseReturnFunctionEntity<Boolean> doInBackground(Void... voids) {
                                BaseReturnFunctionEntity<Boolean> data;
                                try {
                                    data = new JobInfo().delete(getActivity(), id);
                                } catch (Exception ex) {
                                    return null;
                                }
                                return data;
                            }

                            @Override
                            protected void onPostExecute(BaseReturnFunctionEntity<Boolean> data) {
                                //super.onPostExecute(data);
                                if (progDialog.isShowing()) {
                                    progDialog.dismiss();
                                }
                                if (data != null && data.isTrue()) {
                                    Toast.makeText(JobDetailActivity.this, "Xóa thành công", Toast.LENGTH_SHORT).show();
                                    Intent intent = new Intent(Constants.ACTION_JOB_ADD_SUCCESS);
                                    getApplicationContext().sendBroadcast(intent);
                                    onBackPressed();
                                } else
                                    Toast.makeText(JobDetailActivity.this, "Xóa thất bại", Toast.LENGTH_SHORT).show();
                            }


                        }.execute();
                    }
                }).setNegativeText(R.string.cancel)
                        .setNegativeBackgroundColorResource(R.color.dim)
                        .setNegativeTextColorResource(R.color.white).show();
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
