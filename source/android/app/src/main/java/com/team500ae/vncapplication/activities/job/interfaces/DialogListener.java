package com.team500ae.vncapplication.activities.job.interfaces;
/*
 * Created by HoangDong on 30/11/2017.
 */

public interface DialogListener {
    void onConfirmClicked();
}
