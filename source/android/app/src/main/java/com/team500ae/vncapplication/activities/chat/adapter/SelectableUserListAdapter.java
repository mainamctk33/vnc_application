package com.team500ae.vncapplication.activities.chat.adapter;

import android.app.IntentService;
import android.app.Service;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.plus.model.people.Person;
import com.google.gson.JsonObject;
import com.team500ae.library.utils.ConvertUtils;
import com.team500ae.vncapplication.R;
import com.team500ae.vncapplication.activities.chat.utils.ImageUtils;
import com.team500ae.vncapplication.services.ServiceChat;

import java.util.ArrayList;
import java.util.List;

/**
 * Populates a RecyclerView with a list of users, each with a checkbox.
 */

public class SelectableUserListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private ArrayList<JsonObject> mUsers;
    private Context mContext;
    private static List<String> mSelectedUserIds;
    private boolean mIsBlockedList;
    private boolean mShowCheckBox;

    private SelectableUserHolder mSelectableUserHolder;

    // For the adapter to track which users have been selected
    private OnItemCheckedChangeListener mCheckedChangeListener;

    public interface OnItemCheckedChangeListener {
        void OnItemChecked(JsonObject user, boolean checked);
    }

    public SelectableUserListAdapter(Context context, boolean isBlockedList, boolean showCheckBox) {
        mContext = context;
        mUsers = new ArrayList<>();
        mSelectedUserIds = new ArrayList<>();
        mIsBlockedList = isBlockedList;
        mShowCheckBox = showCheckBox;
    }

    public void setItemCheckedChangeListener(OnItemCheckedChangeListener listener) {
        mCheckedChangeListener = listener;
    }

    public void setUserList(ArrayList<JsonObject> users) {
        mUsers = users;
        notifyDataSetChanged();
    }

    public void setShowCheckBox(boolean showCheckBox) {
        mShowCheckBox = showCheckBox;
        if (mSelectableUserHolder != null) {
            mSelectableUserHolder.setShowCheckBox(showCheckBox);
        }
        notifyDataSetChanged();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_selectable_user, parent, false);
        mSelectableUserHolder = new SelectableUserHolder(view, mIsBlockedList, mShowCheckBox);
        return mSelectableUserHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ((SelectableUserHolder) holder).bind(
                mContext,
                mUsers.get(position),
                isSelected(mUsers.get(position)),
                mCheckedChangeListener);
    }

    @Override
    public int getItemCount() {
        return mUsers.size();
    }

    public boolean isSelected(JsonObject user) {
        return mSelectedUserIds.contains(ConvertUtils.toString(user.get("UserName")));
    }

    public void addLast(JsonObject user) {
        mUsers.add(user);
        notifyDataSetChanged();
    }

    private class SelectableUserHolder extends RecyclerView.ViewHolder {
        private TextView nameText;
        private ImageView profileImage;
        private ImageView blockedImage;
        private CheckBox checkbox;

        private boolean mIsBlockedList;
        private boolean mShowCheckBox;

        public SelectableUserHolder(View itemView, boolean isBlockedList, boolean hideCheckBox) {
            super(itemView);

            this.setIsRecyclable(false);
            mIsBlockedList = isBlockedList;
            mShowCheckBox = hideCheckBox;

            nameText = (TextView) itemView.findViewById(R.id.text_selectable_user_list_nickname);
            profileImage = (ImageView) itemView.findViewById(R.id.image_selectable_user_list_profile);
            blockedImage = (ImageView) itemView.findViewById(R.id.image_user_list_blocked);
            checkbox = (CheckBox) itemView.findViewById(R.id.checkbox_selectable_user_list);
        }

        public void setShowCheckBox(boolean showCheckBox) {
            mShowCheckBox = showCheckBox;
        }

        private void bind(final Context context, final JsonObject user, boolean isSelected, final OnItemCheckedChangeListener listener) {
            if (!ConvertUtils.toBoolean(user.get("createSendBird"))) {
                ServiceChat.startActionCreateChatAccount(mContext, ConvertUtils.toString(user.get("UserName")), ConvertUtils.toString(user.get("FullName")), ConvertUtils.toString(user.get("Avatar")));
                user.addProperty("createSendBird", true);
            }
            nameText.setText(ConvertUtils.toString(user.get("FullName")));
            ImageUtils.displayRoundImageFromUrl(context, ConvertUtils.toString(user.get("Avartar")), profileImage, R.drawable.default_avatar);

            if (mIsBlockedList) {
                blockedImage.setVisibility(View.VISIBLE);
            } else {
                blockedImage.setVisibility(View.GONE);
            }

            if (mShowCheckBox) {
                checkbox.setVisibility(View.VISIBLE);
            } else {
                checkbox.setVisibility(View.GONE);
            }

            if (isSelected) {
                checkbox.setChecked(true);
            } else {
                checkbox.setChecked(false);
            }

            if (mShowCheckBox) {
                itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (mShowCheckBox) {
                            checkbox.setChecked(!checkbox.isChecked());
                        }
                    }
                });
            }

            checkbox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    listener.OnItemChecked(user, isChecked);

                    if (isChecked) {
                        mSelectedUserIds.add(ConvertUtils.toString(user.get("UserName")));
                    } else {
                        mSelectedUserIds.remove(ConvertUtils.toString(user.get("UserName")));
                    }
                }
            });
        }
    }
}
