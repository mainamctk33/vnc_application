package com.team500ae.vncapplication.data_access.news;

import android.content.Context;

import com.google.gson.JsonArray;
import com.google.gson.reflect.TypeToken;
import com.team500ae.vncapplication.constants.Constants;
import com.team500ae.vncapplication.data_access.ClientInfo;
import com.team500ae.vncapplication.models.ServiceResponseEntity;

import java.net.SocketTimeoutException;
import java.net.UnknownHostException;

/**
 * Created by MaiNam on 12/1/2017.
 */

public class TagInfo
{
    public ServiceResponseEntity<JsonArray> syncTag(Context context, long lastSyncDate) {
    try {
        ClientInfo.DataResponse dataResponse = new ClientInfo().requestApi(ClientInfo.MethodType.GET, Constants.API_TAG_SYNC+ "/" + lastSyncDate);
        if (dataResponse.isOK()) {
            ServiceResponseEntity<JsonArray> result = dataResponse.getData(new TypeToken<ServiceResponseEntity<JsonArray>>(){}.getType());
            return  result;
        }
    } catch (UnknownHostException e) {
        e.printStackTrace();
    } catch (SocketTimeoutException e) {
        e.printStackTrace();
    }
    return null;
}

}
