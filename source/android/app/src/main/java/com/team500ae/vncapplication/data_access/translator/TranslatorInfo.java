package com.team500ae.vncapplication.data_access.translator;

import android.content.Context;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.team500ae.vncapplication.constants.Constants;
import com.team500ae.vncapplication.data_access.ClientInfo;
import com.team500ae.vncapplication.models.BaseReturnFunctionEntity;
import com.team500ae.vncapplication.models.ServiceResponseEntity;

import java.net.SocketTimeoutException;
import java.net.UnknownHostException;

/**
 * Created by TamPC on 3/24/2018.
 */

public class TranslatorInfo {
    public ServiceResponseEntity<JsonArray> getAll(int page, int size, int type) {
        try {
            ClientInfo.DataResponse dataResponse = new ClientInfo().requestApi(ClientInfo.MethodType.GET, Constants.API_TRANSLATOR_GETALL + "?type=" + type);
            if (dataResponse.isOK()) {
                ServiceResponseEntity<JsonArray> result = dataResponse.getData(new TypeToken<ServiceResponseEntity<JsonArray>>() {
                }.getType());
                return result;
            }
        } catch (UnknownHostException e) {
            e.printStackTrace();
        } catch (SocketTimeoutException e) {
            e.printStackTrace();
        }
        return null;
    }
    public ServiceResponseEntity<JsonArray> getAll() {
        try {
            ClientInfo.DataResponse dataResponse = new ClientInfo().requestApi(ClientInfo.MethodType.GET, Constants.API_TRANSLATOR_GETALL+"?size=99999999" );
            if (dataResponse.isOK()) {
                ServiceResponseEntity<JsonArray> result = dataResponse.getData(new TypeToken<ServiceResponseEntity<JsonArray>>() {
                }.getType());
                return result;
            }
        } catch (UnknownHostException e) {
            e.printStackTrace();
        } catch (SocketTimeoutException e) {
            e.printStackTrace();
        }
        return null;
    }
    public ServiceResponseEntity<JsonArray> getAllNew() {
        try {
            ClientInfo.DataResponse dataResponse = new ClientInfo().requestApi(ClientInfo.MethodType.GET, Constants.API_TRANSLATOR_GETALLNEW);
            if (dataResponse.isOK()) {
                ServiceResponseEntity<JsonArray> result = dataResponse.getData(new TypeToken<ServiceResponseEntity<JsonArray>>() {
                }.getType());
                return result;
            }
        } catch (UnknownHostException e) {
            e.printStackTrace();
        } catch (SocketTimeoutException e) {
            e.printStackTrace();
        }
        return null;
    }
    public BaseReturnFunctionEntity<Boolean> bookmark(int id, boolean bookmark) {
        try {
            JsonObject jsonObject = new JsonObject();
            jsonObject.addProperty("bookmark", bookmark);
            ClientInfo.DataResponse dataResponse = new ClientInfo().requestApi(ClientInfo.MethodType.PUT, Constants.API_TRANSLATOR_BOOKMARK + "/" + id, jsonObject);
            if (dataResponse.isOK()) {
                ServiceResponseEntity<Object> result = dataResponse.getData(new TypeToken<ServiceResponseEntity<Object>>() {
                }.getType());
                if (result.getStatus() == 0)
                    return new BaseReturnFunctionEntity<>(BaseReturnFunctionEntity.StatusFunction.TRUE, true);
            }
            return new BaseReturnFunctionEntity<>(BaseReturnFunctionEntity.StatusFunction.FALSE);
        } catch (UnknownHostException e) {
            e.printStackTrace();
            return new BaseReturnFunctionEntity<>(BaseReturnFunctionEntity.StatusFunction.EXCEPTION, e.getMessage());
        } catch (SocketTimeoutException e) {
            e.printStackTrace();
            return new BaseReturnFunctionEntity<>(BaseReturnFunctionEntity.StatusFunction.EXCEPTION, e.getMessage());
        }
    }
    public BaseReturnFunctionEntity<JsonObject> getById(int id) {
        try {
            ClientInfo.DataResponse dataResponse = new ClientInfo().requestApi(ClientInfo.MethodType.GET, Constants.API_TRANSLATOR_GET + "/" + id);
            if (dataResponse.isOK()) {
                ServiceResponseEntity<JsonObject> result = dataResponse.getData(new TypeToken<ServiceResponseEntity<JsonObject>>() {
                }.getType());
                if (result.getStatus() == 0) {
                    return new BaseReturnFunctionEntity<>(BaseReturnFunctionEntity.StatusFunction.TRUE, result.getData());
                }
            }
            return new BaseReturnFunctionEntity<>(BaseReturnFunctionEntity.StatusFunction.FALSE);
        } catch (UnknownHostException e) {
            e.printStackTrace();
            return new BaseReturnFunctionEntity<>(BaseReturnFunctionEntity.StatusFunction.EXCEPTION, e.getMessage());
        } catch (SocketTimeoutException e) {
            e.printStackTrace();
            return new BaseReturnFunctionEntity<>(BaseReturnFunctionEntity.StatusFunction.EXCEPTION, e.getMessage());
        }
    }
}
