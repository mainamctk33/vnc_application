package com.team500ae.vncapplication.activities.chat.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.sendbird.android.BaseChannel;
import com.sendbird.android.BaseMessage;
import com.sendbird.android.GroupChannel;
import com.sendbird.android.GroupChannelListQuery;
import com.sendbird.android.SendBird;
import com.sendbird.android.SendBirdException;
import com.sendbird.android.User;
import com.team500ae.vncapplication.R;
import com.team500ae.vncapplication.activities.chat.CreateGroupChannelActivity;
import com.team500ae.vncapplication.activities.chat.GroupChannelActivity;
import com.team500ae.vncapplication.activities.chat.adapter.GroupChannelListAdapter;
import com.team500ae.vncapplication.base.BaseActivity;
import com.team500ae.vncapplication.data_access.user.UserInfo;
import com.team500ae.vncapplication.services.ServiceChat;
import com.team500ae.vncapplication.utils.KeyboardUtils;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static android.app.Activity.RESULT_OK;

import com.onurciner.toastox.ToastOXDialog;

public class GroupChannelListFragment extends Fragment {

    private static final String CHANNEL_HANDLER_ID = "CHANNEL_HANDLER_GROUP_CHANNEL_LIST";
    public static final String EXTRA_GROUP_CHANNEL_URL = "GROUP_CHANNEL_URL";
    public static final int INTENT_REQUEST_NEW_GROUP_CHANNEL = 302;
    private static final String TAG = GroupChannelListFragment.class.getSimpleName();

    private LinearLayoutManager mLayoutManager;
    private GroupChannelListAdapter mChannelListAdapter;
    private GroupChannelListQuery mChannelListQuery;
    public GroupChannel currentChannel;

    @BindView(R.id.lnLeaveGroup)
    View lnLeaveGroup;

    public static GroupChannelListFragment newInstance() {
        GroupChannelListFragment fragment = new GroupChannelListFragment();
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mChannelListAdapter = new GroupChannelListAdapter(getActivity());
        mChannelListAdapter.load();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mChannelListAdapter.save();
    }

    @BindView(R.id.group_item_menu)
    View group_item_menu;

    @OnClick(R.id.group_item_menu)
    void hideMenuChatItem() {
        group_item_menu.setVisibility(View.GONE);
    }
    @BindView(R.id.lnBlock)
    View lnBlock;
    @OnClick(R.id.btnBlock)
    void block() {
        group_item_menu.setVisibility(View.GONE);
        new ToastOXDialog.Build(getActivity())
                .setTitle(R.string.confirm)
                .setContent(R.string.msg_do_you_want_to_block_this_user)
                .setPositiveText(R.string.yes)
                .setPositiveBackgroundColorResource(R.color.btnyes)
                .setPositiveTextColorResource(R.color.black)
                .onPositive(new ToastOXDialog.ButtonCallback() {
                    @Override
                    public void onClick(@NonNull ToastOXDialog toastOXDialog) {
                        User user = null;
                        for(User user1: currentChannel.getMembers())
                        {
                            if(!user1.getUserId().equals(UserInfo.getCurrentUserId()))
                            {
                                user=user1;
                                break;
                            }
                        }
                        if(user!=null) {
                            SendBird.blockUser(user, new SendBird.UserBlockHandler() {
                                @Override
                                public void onBlocked(User user, SendBirdException e) {
                                    mChannelListAdapter.remove(currentChannel);
                                    return;
                                }
                            });
                        }
                    }
                })
                .setNegativeText(R.string.cancel)
                .setNegativeBackgroundColorResource(R.color.dim)
                .setNegativeTextColorResource(R.color.white).show();
    }
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        Log.d("LIFECYCLE", "GroupChannelListFragment onCreateView()");

        View rootView = inflater.inflate(R.layout.fragment_group_channel_list, container, false);
        ButterKnife.bind(this, rootView);
        group_item_menu.setVisibility(View.GONE);
        KeyboardUtils.setupUI(rootView, getActivity());
        setRetainInstance(true);

        // Change action bar title
//        ((GroupChannelActivity) getActivity()).setActionBarTitle(getResources().getString(R.string.all_group_channels));


        mSwipeRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                mSwipeRefresh.setRefreshing(true);
                refreshChannelList(15);
            }
        });
        setUpRecyclerView();
        setUpChannelListAdapter();
        return rootView;
    }

    @OnClick(R.id.txtSearch)
    void search() {
        getFragmentManager().beginTransaction().replace(R.id.lnFragment, SearchGroupChatFragment.getInstance(getContext()), "fragmentSearch").addToBackStack("null").commit();
    }

    @BindView(R.id.recycler_group_channel_list)
    public RecyclerView mRecyclerView;
    @BindView(R.id.swipe_layout_group_channel_list)
    public SwipeRefreshLayout mSwipeRefresh;

    @OnClick(R.id.fab_group_channel_list)
    void createGroup() {
        Intent intent = new Intent(getContext(), CreateGroupChannelActivity.class);
        startActivity(intent);
    }

    @Override
    public void onResume() {
        Log.d("LIFECYCLE", "GroupChannelListFragment onResume()");

        refreshChannelList(15);

        SendBird.addChannelHandler(CHANNEL_HANDLER_ID, new SendBird.ChannelHandler() {
            @Override
            public void onMessageReceived(BaseChannel baseChannel, BaseMessage baseMessage) {
                Log.d(TAG, "onMessageReceived: ");
                mChannelListAdapter.clearMap();
                mChannelListAdapter.updateOrInsert(baseChannel);
            }

            @Override
            public void onTypingStatusUpdated(GroupChannel channel) {
                mChannelListAdapter.notifyDataSetChanged();
            }
        });

        super.onResume();
    }

    @Override
    public void onPause() {
        Log.d("LIFECYCLE", "GroupChannelListFragment onPause()");

        SendBird.removeChannelHandler(CHANNEL_HANDLER_ID);
        super.onPause();
    }

    @Override
    public void onDetach() {
        Log.d("LIFECYCLE", "GroupChannelListFragment onDetach()");
        super.onDetach();
    }


    // Sets up recycler view
    private void setUpRecyclerView() {
        mLayoutManager = new LinearLayoutManager(getContext());
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setAdapter(mChannelListAdapter);
        mRecyclerView.addItemDecoration(new DividerItemDecoration(getContext(), DividerItemDecoration.VERTICAL));

        // If user scrolls to bottom of the list, loads more channels.
        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                if (mLayoutManager.findLastVisibleItemPosition() == mChannelListAdapter.getItemCount() - 1) {
                    loadNextChannelList();
                }
            }
        });
    }

    @BindView(R.id.btnTurnOffNotification)
    View btnTurnOffNotification;
    @BindView(R.id.btnTurnOnNotification)
    View btnTurnOnNotification;

    @OnClick({R.id.btnTurnOffNotification, R.id.btnTurnOnNotification})
    void turnOnOffNotification() {
        group_item_menu.setVisibility(View.GONE);
        currentChannel.setPushPreference(currentChannel.isPushEnabled() ? false : true, new GroupChannel.GroupChannelSetPushPreferenceHandler() {
            @Override
            public void onResult(SendBirdException e) {
            }
        });
    }
    @OnClick(R.id.btnMarkAsRead)
    void markAsRead() {
        Log.d(TAG, "markAsRead: ");
        group_item_menu.setVisibility(View.GONE);
        currentChannel.markAsRead();
    }
    @OnClick(R.id.btnLeaveGroup)
    void leaveGroup() {
        group_item_menu.setVisibility(View.GONE);
        new ToastOXDialog.Build(getActivity())
                .setTitle(R.string.confirm)
                .setContent(R.string.msg_do_you_want_to_leave_this_group)
                .setPositiveText(R.string.yes)
                .setPositiveBackgroundColorResource(R.color.btnyes)
                .setPositiveTextColorResource(R.color.black)
                .onPositive(toastOXDialog -> currentChannel.leave(e -> {
                    if (e == null) {
                        ServiceChat.startActionLeaveGroup(getContext(),UserInfo.getCurrentUserId(),UserInfo.getCurrentUserFullName(),currentChannel.getUrl());
                        mChannelListAdapter.remove(currentChannel);
                    }
                }))
                .setNegativeText(R.string.cancel)
                .setNegativeBackgroundColorResource(R.color.dim)
                .setNegativeTextColorResource(R.color.white).show();
    }

    @OnClick(R.id.btnDelete)
    void deleteGroup() {
        group_item_menu.setVisibility(View.GONE);
        new ToastOXDialog.Build(getActivity())
                .setTitle(R.string.confirm)
                .setContent(R.string.msg_do_you_want_to_delete_this_group)
                .setPositiveText(R.string.yes)
                .setPositiveBackgroundColorResource(R.color.btnyes)
                .setPositiveTextColorResource(R.color.black)
                .onPositive(toastOXDialog -> currentChannel.leave(e -> {
                    if (e == null) {
                        ServiceChat.startActionLeaveGroup(getContext(),UserInfo.getCurrentUserId(),UserInfo.getCurrentUserFullName(),currentChannel.getUrl());
                        mChannelListAdapter.remove(currentChannel);
                    }
                }))
                .setNegativeText(R.string.cancel)
                .setNegativeBackgroundColorResource(R.color.dim)
                .setNegativeTextColorResource(R.color.white).show();
    }

    // Sets up channel list adapter
    private void setUpChannelListAdapter() {
        mChannelListAdapter.setOnItemClickListener(channel -> enterGroupChannel(channel));
        mChannelListAdapter.setOnItemLongClickListener(channel -> {
            currentChannel = channel;
            btnTurnOffNotification.setVisibility(View.GONE);
            btnTurnOnNotification.setVisibility(View.GONE);

            if (currentChannel != null) {
                if (currentChannel.isPushEnabled())
                    btnTurnOffNotification.setVisibility(View.VISIBLE);
                else
                    btnTurnOnNotification.setVisibility(View.VISIBLE);
            }

            if (currentChannel.getMembers().size() == 2) {
                lnBlock.setVisibility(View.VISIBLE);
            } else {
                lnBlock.setVisibility(View.GONE);
            }
            if(currentChannel.getMembers().size()<=2)
            {
                lnLeaveGroup.setVisibility(View.GONE);
            }else
            {
                lnLeaveGroup.setVisibility(View.VISIBLE);
            }
            group_item_menu.setVisibility(View.VISIBLE);
        });
    }


    /**
     * Entersa a Group Channel. Upon entering, a GroupChatFragment will be inflated
     * to display messages within the channel.
     *
     * @param channel The Group Channel to enter.
     */
    void enterGroupChannel(GroupChannel channel) {
        final String channelUrl = channel.getUrl();

        enterGroupChannel(channelUrl);
    }

    /**
     * Enters a Group Channel with a URL.
     *
     * @param channelUrl The URL of the channel to enter.
     */
    void enterGroupChannel(String channelUrl) {
        GroupChannelActivity.openChannel((AppCompatActivity) getContext(), channelUrl);
//        GroupChatFragment fragment = GroupChatFragment.newInstance(channelUrl);
//        getFragmentManager().beginTransaction()
//                .replace(R.id.container_group_channel, fragment)
//                .addToBackStack(null)
//                .commit();
    }

    /**
     * Creates a new query to get the list of the user's Group Channels,
     * then replaces the existing dataset.
     *
     * @param numChannels The number of channels to load.
     */
    private void refreshChannelList(int numChannels) {
        mChannelListQuery = GroupChannel.createMyGroupChannelListQuery();
        mChannelListQuery.setLimit(numChannels);

        mChannelListQuery.next(new GroupChannelListQuery.GroupChannelListQueryResultHandler() {
            @Override
            public void onResult(List<GroupChannel> list, SendBirdException e) {
                if (e != null) {
                    // Error!
                    e.printStackTrace();
                    return;
                }

                mChannelListAdapter.clearMap();
                mChannelListAdapter.setGroupChannelList(list);
            }
        });

        if (mSwipeRefresh.isRefreshing()) {
            mSwipeRefresh.setRefreshing(false);
        }
    }

    /**
     * Loads the next channels from the current query instance.
     */
    private void loadNextChannelList() {
        mChannelListQuery.next(new GroupChannelListQuery.GroupChannelListQueryResultHandler() {
            @Override
            public void onResult(List<GroupChannel> list, SendBirdException e) {
                if (e != null) {
                    // Error!
                    e.printStackTrace();
                    return;
                }

                for (GroupChannel channel : list) {
                    mChannelListAdapter.addLast(channel);
                }
            }
        });
    }
}
