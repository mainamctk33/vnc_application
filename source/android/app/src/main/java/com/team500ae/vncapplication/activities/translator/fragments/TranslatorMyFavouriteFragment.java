package com.team500ae.vncapplication.activities.translator.fragments;


import android.os.AsyncTask;
import android.os.Bundle;
import android.speech.tts.TextToSpeech;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.team500ae.library.adapters.BaseRecyclerAdapter;
import com.team500ae.library.layoutmanager.MyLinearLayoutManager;
import com.team500ae.library.utils.ConvertUtils;
import com.team500ae.library.utils.RecyclerViewUtils;
import com.team500ae.vncapplication.R;
import com.team500ae.vncapplication.activities.login.LoginActivity;
import com.team500ae.vncapplication.base.BaseActivity;
import com.team500ae.vncapplication.base.BaseViewHolder;
import com.team500ae.vncapplication.constants.Constants;
import com.team500ae.vncapplication.data_access.BookmarkInfo;
import com.team500ae.vncapplication.data_access.translator.TranslatorInfo;
import com.team500ae.vncapplication.data_access.user.UserInfo;
import com.team500ae.vncapplication.models.BaseReturnFunctionEntity;
import com.team500ae.vncapplication.models.ServiceResponseEntity;
import com.team500ae.vncapplication.services.ServiceTranslator;
import com.team500ae.vncapplication.utils.SharedPrefUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class TranslatorMyFavouriteFragment extends Fragment {
    @BindView(R.id.loading)
    LinearLayout loading;
    @BindView(R.id.fragTranslatorMyFavourite_rvList)
    RecyclerView rvList;
    Unbinder unbinder;
    View viewMain;
    TextToSpeech textToSpeech;
    int result;
    boolean isLoading = false;
    @BindView(R.id.swipeRefreshLayout)
    SwipeRefreshLayout swipeRefreshLayout;
    TextToSpeech textToSpeechUS, textToSpeechGerman;
    private int selectPos = -1;
    int page = 1;
    int size = 10;
    private boolean isViewShown = false;
    HashMap<Integer, JsonObject> mapData = new HashMap<>();
    private BaseRecyclerAdapter<ViewHolder, JsonObject> adapter;
    ArrayList<JsonObject> mData = new ArrayList<>();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_translator_my_favourite, container, false);
        unbinder = ButterKnife.bind(this, view);
        initTextToSpeech();
        init();
        if (!isViewShown) {
            getDataTranslatorFavouriteOffline();
        }
        return view;
    }

    private void initTextToSpeech() {
        textToSpeechUS = new TextToSpeech(getActivity(), new TextToSpeech.OnInitListener() {
            @Override
            public void onInit(int status) {
                if (status == TextToSpeech.SUCCESS) {
                    textToSpeechUS.setLanguage(Locale.US);
                    textToSpeechUS.setSpeechRate(0.6f);
                } else {
                    Toast.makeText(getActivity(), "Feature not supported in your device", Toast.LENGTH_SHORT).show();
                }
            }
        }, "com.google.android.tts");
        textToSpeechGerman = new TextToSpeech(getActivity(), new TextToSpeech.OnInitListener() {
            @Override
            public void onInit(int status) {
                if (status == TextToSpeech.SUCCESS) {
                    textToSpeechGerman.setLanguage(Locale.GERMAN);
                    textToSpeechGerman.setSpeechRate(0.6f);
                } else {
                    Toast.makeText(getActivity(), "Feature not supported in your device", Toast.LENGTH_SHORT).show();
                }
            }
        }, "com.google.android.tts");

    }

    @Override
    public void onDestroyView() {
        if (textToSpeechUS != null) {
            textToSpeechUS.stop();
        }
        if (textToSpeechGerman != null) {
            textToSpeechGerman.stop();
        }
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }


    private void init() {
        rvList.setLayoutManager(new MyLinearLayoutManager(getActivity()));
        //ho tro load next page
        RecyclerViewUtils rvListUtils = new RecyclerViewUtils(new RecyclerViewUtils.RecyclerViewUtilsListener() {
            @Override
            public void onScrollToEnd() {
                if (!isLoading) {
                    //getData(page + 1, size, type, getListener());
                }
            }
        });
        rvListUtils.setUp(rvList);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                page = 1;
                getDataTranslatorFavouriteOffline();
            }
        });
        rvList.setLayoutManager(new LinearLayoutManager(getActivity()));
        adapter = new BaseRecyclerAdapter<ViewHolder, JsonObject>(R.layout.item_translator_detail, new BaseRecyclerAdapter.BaseViewHolder<ViewHolder, JsonObject>() {


            @Override
            public ViewHolder getViewHolder(View v) {
                return new ViewHolder(v);
            }

            @Override
            public void bindData(RecyclerView recyclerView, ViewHolder viewHolder, JsonObject data, int position) {
                try {
                    int categoryId = ConvertUtils.toInt(data.get("Category"));
                    JSONObject jsonObject = ConvertUtils.toJSONObject(data);
                    JSONObject translator = jsonObject.getJSONObject("Translator");
                    viewHolder.tvTitle.setText(translator.getString("ContentVN"));
                    viewHolder.tvDescription.setText(translator.getString("ContentEN"));
                    viewHolder.tvTypeNameTranslator.setVisibility(View.VISIBLE);
                    if (selectPos == position) {
                        viewHolder.llDescription.setVisibility(View.VISIBLE);
                        viewHolder.llMain.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.colorPrimary));
                    } else {
                        viewHolder.llMain.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.white));
                        viewHolder.llDescription.setVisibility(View.GONE);
                    }
                    if (categoryId == 1)
                        viewHolder.tvTypeNameTranslator.setText("English");
                    else if (categoryId == 2)
                        viewHolder.tvTypeNameTranslator.setText("Germany");
                    BaseReturnFunctionEntity<Boolean> bookmark = new BookmarkInfo().isBookmark(getActivity(), ((BaseActivity) getActivity()).getRealm(), translator.getInt("ID"), UserInfo.getCurrentUserId(), BookmarkInfo.BookmarkType.Translator);
                    viewHolder.imageView.setSelected(bookmark.isTrue() ? bookmark.data : false);
                    viewHolder.llDescription.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (categoryId == 1)
                                textToSpeechUS.speak(viewHolder.tvDescription.getText().toString(), TextToSpeech.QUEUE_FLUSH, null);
                            else if (categoryId == 2)
                                textToSpeechGerman.speak(viewHolder.tvDescription.getText().toString(), TextToSpeech.QUEUE_FLUSH, null);
                        }
                    });
                    viewHolder.imageView.setOnClickListener(v -> {
                        if (!UserInfo.isLogin()) {
                            LoginActivity.open(getActivity());
                            return;
                        } else {
                            viewHolder.imageView.setSelected(!viewHolder.imageView.isSelected());
                            try {
                                ServiceTranslator.startActionBookmark(getActivity(), translator.getInt("ID"), viewHolder.imageView.isSelected());
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    });
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }


        }, mData, new BaseRecyclerAdapter.OnClickListener() {
            @Override
            public void onClick(View v, int position, Object o) {
                if (textToSpeechUS != null) {
                    textToSpeechUS.stop();
                }
                if (textToSpeechGerman != null) {
                    textToSpeechGerman.stop();
                }
                selectPos = position;
                adapter.notifyDataSetChanged();
            }
        });
        adapter.bindData(rvList);
        //getData(page, size, type, getListener());
    }

    private void getDataTranslatorDetail() {
        new AsyncTask<Void, Void, ServiceResponseEntity<JsonArray>>() {
            @Override
            protected void onPreExecute() {
                // showProgress();
                super.onPreExecute();

            }

            @Override
            protected ServiceResponseEntity<JsonArray> doInBackground(Void... voids) {
                ServiceResponseEntity<JsonArray> data;
                try {
                    data = new TranslatorInfo().getAllNew();
                } catch (Exception ex) {
                    return null;
                }
                return data;
            }


            @Override
            protected void onPostExecute(ServiceResponseEntity<JsonArray> data) {

                if (data != null) {
                    SharedPrefUtils.setString(getActivity(), Constants.JSON_DATA_TRANSLATOR_DETAIL_LIST, data.getData().toString());
                }

            }


        }.execute();
    }

    private void getDataTranslatorFavouriteOffline() {
        String jsonOffline = SharedPrefUtils.getString(getActivity(), Constants.JSON_DATA_TRANSLATOR_DETAIL_LIST);
        if (!android.text.TextUtils.isEmpty(jsonOffline)) {
            bindData(ConvertUtils.toJsonArray(jsonOffline));
        }
    }

    public class ViewHolder extends BaseViewHolder {
        @BindView(R.id.imgFavorite)
        ImageView imageView;
        @BindView(R.id.tvTitle)
        TextView tvTitle;
        @BindView(R.id.tvNameTypeTranslator)
        TextView tvTypeNameTranslator;
        @BindView(R.id.tvDescription)
        TextView tvDescription;
        @BindView(R.id.llDescription)
        LinearLayout llDescription;
        @BindView(R.id.llMain)
        LinearLayout llMain;

        public ViewHolder(View itemView) {
            super(itemView);
        }
    }

    public interface LoadTranslatorListener {
        public void onBeginLoad(Fragment fragment);

        public void onEndLoad(Fragment fragment);

        public JsonArray getData(int page, int size, int type, Fragment translatorTabFragment);
    }

    public TranslatorMyFavouriteFragment.LoadTranslatorListener getListener() {
        if (getContext() instanceof TranslatorMyFavouriteFragment.LoadTranslatorListener)
            return (TranslatorMyFavouriteFragment.LoadTranslatorListener) getContext();
        return null;
    }

    public void getData(final int page, final int size, final int type, final LoadTranslatorListener listener) {
        new AsyncTask<Void, JsonArray, Void>() {

            @Override
            protected void onPreExecute() {
                swipeRefreshLayout.setEnabled(false);
                if (page == 1)
                    swipeRefreshLayout.setRefreshing(true);
                else
                    loading.setVisibility(View.VISIBLE);
                if (listener != null)
                    listener.onBeginLoad(TranslatorMyFavouriteFragment.this);
                isLoading = true;
                super.onPreExecute();
            }

            @Override
            protected Void doInBackground(Void... voids) {

                if (listener != null) {
                    publishProgress(listener.getData(page, size, type, TranslatorMyFavouriteFragment.this));
                } else
                    publishProgress(new JsonArray());
                return null;
            }

            @Override
            protected void onProgressUpdate(JsonArray... values) {
                if (values.length > 0 && values[0] != null) {
                    if (page == 1) {
                        mapData.clear();
                        mData.clear();
                        adapter.notifyDataSetChanged();
                    }

                    bindData(values[0]);
                }
                super.onProgressUpdate(values);
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                swipeRefreshLayout.setEnabled(true);
                if (page == 1)
                    swipeRefreshLayout.setRefreshing(false);
                else
                    loading.setVisibility(View.GONE);
                if (listener != null)
                    listener.onEndLoad(TranslatorMyFavouriteFragment.this);
                isLoading = false;
                super.onPostExecute(aVoid);
            }
        }.execute();
    }

    void bindData(JsonArray jsonArray) {
        swipeRefreshLayout.setRefreshing(false);
        mData.clear();
        for (int i = 0; i < jsonArray.size(); i++) {
            try {
                JsonObject jsonObject = ConvertUtils.toJsonObject(jsonArray.get(i));
                JSONObject data = ConvertUtils.toJSONObject(jsonObject);
                JSONObject translator = data.getJSONObject("Translator");
                BaseReturnFunctionEntity<Boolean> bookmark = new BookmarkInfo().isBookmark(getActivity(), ((BaseActivity) getActivity()).getRealm(), translator.getInt("ID"), UserInfo.getCurrentUserId(), BookmarkInfo.BookmarkType.Translator);
                if (bookmark.getData()) {
                    mData.add(jsonObject);
                    adapter.notifyDataSetChanged();
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (textToSpeechUS != null) {
            textToSpeechUS.stop();
        }
        if (textToSpeechGerman != null) {
            textToSpeechGerman.stop();
        }
        if (getView() != null) {
            isViewShown = true;
            getDataTranslatorFavouriteOffline();
        } else {
            isViewShown = false;
        }
    }
}
