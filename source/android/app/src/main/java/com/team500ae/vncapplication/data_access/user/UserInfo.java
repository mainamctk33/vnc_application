package com.team500ae.vncapplication.data_access.user;

import android.content.Context;
import android.util.Log;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.reflect.TypeToken;
import com.sendbird.android.SendBird;
import com.sendbird.android.SendBirdException;
import com.sendbird.android.User;
import com.team500ae.library.utils.ConvertUtils;
import com.team500ae.vncapplication.constants.Constants;
import com.team500ae.vncapplication.data_access.ClientInfo;
import com.team500ae.vncapplication.data_access.DataCacheInfo;
import com.team500ae.vncapplication.models.BaseReturnFunctionEntity;
import com.team500ae.vncapplication.models.ServiceResponseEntity;
import com.team500ae.vncapplication.services.ServiceNews;
import com.team500ae.vncapplication.services.ServiceNotification;
import com.team500ae.vncapplication.utils.FlagAttributeUtils;

import java.net.SocketTimeoutException;
import java.net.UnknownHostException;

/**
 * Created by MaiNam on 11/15/2017.
 */

public class UserInfo {
    public static void registerToken(Context context, String token) {
        if (UserInfo.isLogin()) {
            ServiceNotification.startRegisterToken(context, token);
            SendBird.registerPushTokenForCurrentUser(token, (pushTokenRegistrationStatus, e) -> {
            });
        }
    }

    public JsonArray searchFriend(String query, int page, int size) {
        try {
            ClientInfo.DataResponse dataResponse = new ClientInfo().requestApi(ClientInfo.MethodType.GET, Constants.API_USER_SEARCH_FRIEND + "?size=" + size + "&page=" + page + "&query=" + query);
            if (dataResponse.isOK()) {
                ServiceResponseEntity<JsonArray> result = dataResponse.getData(new TypeToken<ServiceResponseEntity<JsonArray>>() {
                }.getType());
                if (result.getStatus() == 0)
                    return result.getData();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new JsonArray();
    }

    public ServiceResponseEntity<JsonObject> updateLocation(Context applicationContext, double lat, double lon) {
        try {
            int settingAttributeValue = ConvertUtils.toInt(UserInfo.getCurrentUser().get("SettingAttribute"));
            boolean isShareLocation = FlagAttributeUtils.getAttribute(settingAttributeValue, FlagAttributeUtils.SHARE_YOUR_LOCATION);

            if (isShareLocation) {
                JsonObject jsonObject = new JsonObject();
                jsonObject.addProperty("latitude", lat);
                jsonObject.addProperty("longitude", lon);
                ClientInfo.DataResponse dataResponse = new ClientInfo().requestApi(ClientInfo.MethodType.PUT, Constants.API_USER_UPDATE_LOCATION, jsonObject);
                if (dataResponse.isOK()) {
                    return dataResponse.getData(new TypeToken<ServiceResponseEntity<JsonObject>>() {
                    }.getType());
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new ServiceResponseEntity<>();
    }

    public BaseReturnFunctionEntity<JsonObject> search(int page, int size, String query, double time) {
        try {
            ClientInfo.DataResponse dataResponse = new ClientInfo().requestApi(ClientInfo.MethodType.GET, Constants.API_USER_SEARCH + "?size=" + size + "&page=" + page + "&query=" + query + "&time=" + time);
            if (dataResponse.isOK()) {
                ServiceResponseEntity<JsonObject> result = dataResponse.getData(new TypeToken<ServiceResponseEntity<JsonObject>>() {
                }.getType());
                if (result.getStatus() == 0)
                    return new BaseReturnFunctionEntity<>(BaseReturnFunctionEntity.StatusFunction.TRUE, result.getData());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new BaseReturnFunctionEntity<>(BaseReturnFunctionEntity.StatusFunction.FALSE);
    }


    public enum SocialType {
        fb,
        gg
    }

    private static final String TAG = UserInfo.class.getSimpleName();
    static JsonObject currentUser;

    public synchronized static void setCurrentUserFromCache(Context mContext, JsonObject account) {
        Log.d(TAG, "setCurrentUserFromCache: ");
        UserInfo.currentUser = account;
        try {
            String token = DataCacheInfo.getData(mContext, DataCacheInfo.EnumCacheType.TokenFCM);
            if (token != null)
                ServiceNotification.startRegisterToken(mContext, token);

            if (UserInfo.currentUser != null) {
                DataCacheInfo.setData(mContext, DataCacheInfo.EnumCacheType.CurrentUser, "", UserInfo.currentUser);
                String userId = getCurrentUserId();
                String userAvatar = getCurrentUserAvartar();
                if (userId != null && !userId.equals("")) {
                    connectToSendBird(mContext, userId, UserInfo.getCurrentUserFullName(), userAvatar);
                }
            } else {
                DataCacheInfo.setData(mContext, DataCacheInfo.EnumCacheType.CurrentUser, "", null);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public synchronized static void setCurrentUser(Context mContext, JsonObject account) {
        Log.d(TAG, "setCurrentUser: ");
        UserInfo.currentUser = account;
        String token = DataCacheInfo.getData(mContext, DataCacheInfo.EnumCacheType.TokenFCM);
        if (token != null)
            ServiceNotification.startRegisterToken(mContext, token);


        try {
            if (UserInfo.currentUser != null) {
                DataCacheInfo.setData(mContext, DataCacheInfo.EnumCacheType.CurrentUser, "", UserInfo.currentUser);
                String userId = getCurrentUserId();
                String userNickname = getCurrentUserFullName();
                String userAvatar = getCurrentUserAvartar();
                if (userId != null && !userId.equals("")) {
                    connectToSendBird(mContext, userId, userNickname, userAvatar);
                }
                ServiceNews.startSetDefaultCategory(mContext);
            } else {
                DataCacheInfo.setData(mContext, DataCacheInfo.EnumCacheType.CurrentUser, "", null);
                SendBird.disconnect(new SendBird.DisconnectHandler() {
                    @Override
                    public void onDisconnected() {

                    }
                });
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public synchronized static void logOut(Context mContext) {
        Log.d(TAG, "logOut: ");
        if (getCurrentUser() == null)
            return;

        setCurrentUser(mContext, null);
        DataCacheInfo.setData(mContext, DataCacheInfo.EnumCacheType.CurrentUser, "", null);
    }

    public synchronized static JsonObject getCurrentUserHaveSaved(Context mContext) {
        try {
            if (currentUser == null) {
                String _currentUser = DataCacheInfo.getData(mContext, DataCacheInfo.EnumCacheType.CurrentUser, "");
                Log.d(TAG, "getCurrentUserHaveSaved _currentUser: " + _currentUser);
                if (!_currentUser.equals("")) {
                    setCurrentUserFromCache(mContext, ConvertUtils.toJsonObject(_currentUser));
                }
            }
            return currentUser;
        } catch (Exception e) {
        }
        return null;
    }

    public static String getCurrentUserId() {
        try {
            if (!UserInfo.isLogin()) return "";
            return ConvertUtils.toString(UserInfo.getCurrentUser().get("UserName"));

        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static String getCurrentUserFullName() {
        try {
            if (!UserInfo.isLogin()) return "";
            return ConvertUtils.toString(UserInfo.getCurrentUser().get("FullName"));

        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    //return email
    public static String getCurrentUserName() {
        try {
            if (!UserInfo.isLogin()) return "";
            String username = ConvertUtils.toString(UserInfo.getCurrentUser().get("UserName"));
            if (username != null && username.contains("@"))
                return username;
            return "Vietnamese Connecting Application";
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static String getCurrentUserAvartar() {
        try {
            if (!UserInfo.isLogin()) return "";
            return ConvertUtils.toString(UserInfo.getCurrentUser().get("Avartar"));

        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static JsonObject getCurrentUser() {
        return currentUser;
    }

    public static boolean isLogin() {
        return currentUser != null && !currentUser.toString().equals("{}");
    }

    public BaseReturnFunctionEntity<Boolean> loginSocial(Context context, SocialType socialType, String socialId, String fullname, int gender, String avatar) {
        try {
            JsonObject jsonObject = new JsonObject();
            jsonObject.addProperty("socialType", socialType.toString());
            jsonObject.addProperty("socialId", socialId);
            jsonObject.addProperty("fullname", fullname);
            jsonObject.addProperty("avatar", avatar);
            jsonObject.addProperty("gender", gender);
            ClientInfo.DataResponse dataResponse = new ClientInfo().requestApi(ClientInfo.MethodType.POST, Constants.API_USER_LOGIN_SOCIAL, jsonObject);
            if (dataResponse.isOK()) {
                ServiceResponseEntity<JsonObject> result = dataResponse.getData(new TypeToken<ServiceResponseEntity<JsonObject>>() {
                }.getType());
                if (result.getStatus() == 0) {
                    setCurrentUser(context, result.getData());
                    return new BaseReturnFunctionEntity<>(BaseReturnFunctionEntity.StatusFunction.TRUE, true);
                }
            }
            return new BaseReturnFunctionEntity<>(BaseReturnFunctionEntity.StatusFunction.FALSE);
        } catch (UnknownHostException e) {
            e.printStackTrace();
            return new BaseReturnFunctionEntity<>(BaseReturnFunctionEntity.StatusFunction.EXCEPTION, e.getMessage());
        } catch (SocketTimeoutException e) {
            e.printStackTrace();
            return new BaseReturnFunctionEntity<>(BaseReturnFunctionEntity.StatusFunction.EXCEPTION, e.getMessage());
        }
    }


    private static void connectToSendBird(Context context, final String userId, String nickname, String avatar) {
        // Show the loading indicator
        SendBird.connect(userId, new SendBird.ConnectHandler() {
            @Override
            public void onConnected(User user, SendBirdException e) {
                // Callback received; hide the progress bar.
                if (e != null) {
                    // Error!
                    return;
                }
//                String token = DataCacheInfo.getData(context, DataCacheInfo.EnumCacheType.TokenFCM);
//                registerToken(token);
                // Update the user's nickname
                updateCurrentUserInfo(nickname, avatar);
//                updateCurrentUserPushToken();
            }
        });
    }

    private static void updateCurrentUserInfo(final String userNickname, String imageUrl) {
        SendBird.updateCurrentUserInfo(userNickname, imageUrl, new SendBird.UserInfoUpdateHandler() {
            @Override
            public void onUpdated(SendBirdException e) {
                if (e != null) {
                    // Error!
                    return;
                }
            }
        });
    }

    public static String getLoginToken() {
        try {
            if (!UserInfo.isLogin()) return "";
            return ConvertUtils.toString(UserInfo.getCurrentUser().get("LoginToken"));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static boolean isExistedAccount(String email, int timeOut) throws SocketTimeoutException {
        try {
            ClientInfo.DataResponse response = new ClientInfo().requestApi(ClientInfo.MethodType.GET, Constants.API_ACCOUNT_CHECK_EXISTED + "?username=" + email);
            if (response.isOK()) {
                JsonObject jsonObject = response.getData(JsonObject.class);
                boolean result = ConvertUtils.toBoolean(jsonObject.get("IsTrue"));
                return result;
            }
            return true;
        } catch (SocketTimeoutException e) {
            throw e;
        } catch (Exception e) {
            String error = e.getMessage().toString();
            return true;
        }
    }

    public static BaseReturnFunctionEntity<Boolean> loginEmail(Context context, String email, String password, int timeOut) throws SocketTimeoutException {
        try {
            if (email != null)
                email = email.toLowerCase();

            JsonObject jsonObject = new JsonObject();
            jsonObject.addProperty("username", email);
            jsonObject.addProperty("password", password);//id 4=user

            ClientInfo.DataResponse response = new ClientInfo().requestApi(ClientInfo.MethodType.POST, Constants.API_ACCOUNT_LOGIN_VIA_EMAIL, jsonObject);
            if (response.isOK()) {
                ServiceResponseEntity<JsonObject> result = response.getData(new TypeToken<ServiceResponseEntity<JsonObject>>() {
                }.getType());

                if (result != null && result.getStatus() == 0) {
                    //status 0 ~ success
                    setCurrentUser(context, result.getData());
                    return new BaseReturnFunctionEntity<>(BaseReturnFunctionEntity.StatusFunction.TRUE, true);
                }else {
                    JsonParser jsonParser=new JsonParser();
                    JsonObject obj=(JsonObject)jsonParser.parse(response.getBody());
                    return new BaseReturnFunctionEntity<>(BaseReturnFunctionEntity.StatusFunction.FALSE, obj.get("Message").toString());
                }
            }
            return new BaseReturnFunctionEntity<>(BaseReturnFunctionEntity.StatusFunction.FALSE);
        } catch (UnknownHostException e) {
            e.printStackTrace();
            return new BaseReturnFunctionEntity<>(BaseReturnFunctionEntity.StatusFunction.EXCEPTION, e.getMessage());
        } catch (SocketTimeoutException e) {
            e.printStackTrace();
            return new BaseReturnFunctionEntity<>(BaseReturnFunctionEntity.StatusFunction.EXCEPTION, e.getMessage());
        }
    }

    public static BaseReturnFunctionEntity<Boolean> createAccount(String fullname, String email, String password, String nationality, long birthday, String gender, String currentLivingCity, int typeJob, String job, String phonenumber, String introduction, String avatarUrl, int isActived, double latAddress, double lonAddress, int timeOut) {
        try {
            if (email != null)
                email = email.toLowerCase();

            JsonObject jsonObject = new JsonObject();
            jsonObject.addProperty("UserName", email);
            jsonObject.addProperty("Permission", 4);//id 4=user
            jsonObject.addProperty("Class", 1);//1 ~ temporary value
            jsonObject.addProperty("Lat", 0f);
            jsonObject.addProperty("Lon", 0f);
            jsonObject.addProperty("FullName", fullname);
            jsonObject.addProperty("Avartar", avatarUrl);
            jsonObject.addProperty("SocialId", Constants.EMPTY);
            jsonObject.addProperty("LatestLogin", 0);
            jsonObject.addProperty("LoginToken", Constants.EMPTY);
            jsonObject.addProperty("Gender", UserInfo.getGender(gender));
            jsonObject.addProperty("Password", password);
            jsonObject.addProperty("Nationality", nationality);
            jsonObject.addProperty("Birthday", birthday);
            jsonObject.addProperty("CurrentLivingCity", currentLivingCity);
            jsonObject.addProperty("TypeJob", typeJob);
            jsonObject.addProperty("Job", job);
            jsonObject.addProperty("PhoneNumber", phonenumber);
            jsonObject.addProperty("Introduction", introduction);
            jsonObject.addProperty("IsActived", isActived);
            jsonObject.addProperty("LatAddress", latAddress);
            jsonObject.addProperty("LonAddress", lonAddress);
            jsonObject.addProperty("SettingAttribute", 0);//download media file via 4G and wifi is turned off. See FlagAttributeUtils

            ClientInfo.DataResponse dataResponse = new ClientInfo().requestApi(ClientInfo.MethodType.POST, Constants.API_ACCOUNT_CREATE, jsonObject);
            if (dataResponse.isOK()) {
                ServiceResponseEntity<Object> result = dataResponse.getData(new TypeToken<ServiceResponseEntity<Object>>() {
                }.getType());
                if (result.getStatus() == 0)
                    return new BaseReturnFunctionEntity<>(BaseReturnFunctionEntity.StatusFunction.TRUE, true);
            }
            return new BaseReturnFunctionEntity<>(BaseReturnFunctionEntity.StatusFunction.FALSE);
        } catch (UnknownHostException e) {
            e.printStackTrace();
            return new BaseReturnFunctionEntity<>(BaseReturnFunctionEntity.StatusFunction.EXCEPTION, e.getMessage());
        } catch (SocketTimeoutException e) {
            e.printStackTrace();
            return new BaseReturnFunctionEntity<>(BaseReturnFunctionEntity.StatusFunction.EXCEPTION, e.getMessage());
        }
    }

    public static BaseReturnFunctionEntity<Boolean> updateAccount(Context context, String fullname, String nationality, long birthday, String gender, String currentLivingCity, int typeJob, String job, String phonenumber, String introduction, String avatarUrl, double latAddress, double lonAddress, int timeOut) {
        try {
            String username = ConvertUtils.toString(currentUser.get("UserName"));
            JsonObject jsonObject = new JsonObject();
            jsonObject.addProperty("UserName", username);
            jsonObject.addProperty("FullName", fullname);
            jsonObject.addProperty("Avartar", avatarUrl);
            jsonObject.addProperty("Gender", UserInfo.getGender(gender));
            jsonObject.addProperty("Nationality", nationality);
            jsonObject.addProperty("Birthday", birthday);
            jsonObject.addProperty("CurrentLivingCity", currentLivingCity);
            jsonObject.addProperty("TypeJob", typeJob);
            jsonObject.addProperty("Job", job);
            jsonObject.addProperty("PhoneNumber", phonenumber);
            jsonObject.addProperty("Introduction", introduction);
            jsonObject.addProperty("LatAddress", latAddress);
            jsonObject.addProperty("LonAddress", lonAddress);

            ClientInfo.DataResponse dataResponse = new ClientInfo().requestApi(ClientInfo.MethodType.PUT, Constants.API_ACCOUNT_UPDATE, jsonObject);
            if (dataResponse.isOK()) {
                ServiceResponseEntity<JsonObject> result = dataResponse.getData(new TypeToken<ServiceResponseEntity<JsonObject>>() {
                }.getType());
                if (result != null && result.getStatus() == 0) {
                    setCurrentUser(context, result.getData());
                    return new BaseReturnFunctionEntity<>(BaseReturnFunctionEntity.StatusFunction.TRUE, true);
                }
            }
            return new BaseReturnFunctionEntity<>(BaseReturnFunctionEntity.StatusFunction.FALSE);
        } catch (UnknownHostException e) {
            e.printStackTrace();
            return new BaseReturnFunctionEntity<>(BaseReturnFunctionEntity.StatusFunction.EXCEPTION, e.getMessage());
        } catch (SocketTimeoutException e) {
            e.printStackTrace();
            return new BaseReturnFunctionEntity<>(BaseReturnFunctionEntity.StatusFunction.EXCEPTION, e.getMessage());
        }
    }

    public static JsonObject getAccountInfo(String email) {
        try {
            if (email == null || email.isEmpty())
                return null;

            ClientInfo.DataResponse response = new ClientInfo().requestApi(ClientInfo.MethodType.GET, Constants.API_ACCOUNT_GET_INFO + "?username=" + email);
            if (response.isOK()) {
                ServiceResponseEntity<JsonObject> result = response.getData(new TypeToken<ServiceResponseEntity<JsonObject>>() {
                }.getType());

                if (result != null && result.getStatus() == 0) {
                    //status 0 ~ success
                    return result.getData();
                }
            }
            return null;
        } catch (UnknownHostException e) {
            e.printStackTrace();
            return null;
        } catch (SocketTimeoutException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static BaseReturnFunctionEntity<Boolean> changePassword(Context context, String newPassword, int timeOut) {
        try {
            JsonObject jsonObject = new JsonObject();
            jsonObject.addProperty("Password", newPassword);

            ClientInfo.DataResponse dataResponse = new ClientInfo().requestApi(ClientInfo.MethodType.PUT, Constants.API_ACCOUNT_UPDATE_PASSWORD, jsonObject);
            if (dataResponse.isOK()) {
                ServiceResponseEntity<JsonObject> result = dataResponse.getData(new TypeToken<ServiceResponseEntity<JsonObject>>() {
                }.getType());
                if (result != null && result.getStatus() == 0) {
                    setCurrentUser(context, result.getData());
                    return new BaseReturnFunctionEntity<>(BaseReturnFunctionEntity.StatusFunction.TRUE, true);
                } else
                    return new BaseReturnFunctionEntity<>(BaseReturnFunctionEntity.StatusFunction.FALSE, result.Message.toString());
            }
            return new BaseReturnFunctionEntity<>(BaseReturnFunctionEntity.StatusFunction.FALSE);
        } catch (UnknownHostException e) {
            e.printStackTrace();
            return new BaseReturnFunctionEntity<>(BaseReturnFunctionEntity.StatusFunction.EXCEPTION, e.getMessage());
        } catch (SocketTimeoutException e) {
            e.printStackTrace();
            return new BaseReturnFunctionEntity<>(BaseReturnFunctionEntity.StatusFunction.EXCEPTION, e.getMessage());
        }
    }

    public static BaseReturnFunctionEntity<Boolean> shareOnlineStatusSetting(boolean isShared) {
        try {
            JsonObject jsonObject = new JsonObject();
            jsonObject.addProperty("isShared", isShared);

            ClientInfo.DataResponse dataResponse = new ClientInfo().requestApi(ClientInfo.MethodType.PUT, Constants.API_ACCOUNT_SHARE_ONLINE_STATUS, jsonObject);
            if (dataResponse.isOK()) {
                JsonObject jObject = dataResponse.getData(JsonObject.class);
                boolean result = ConvertUtils.toBoolean(jObject.get("IsTrue"));
                if (result)
                    return new BaseReturnFunctionEntity<>(BaseReturnFunctionEntity.StatusFunction.TRUE, true);
                else
                    return new BaseReturnFunctionEntity<>(BaseReturnFunctionEntity.StatusFunction.FALSE, ConvertUtils.toBoolean(jObject.get("Message")));
            }
            return new BaseReturnFunctionEntity<>(BaseReturnFunctionEntity.StatusFunction.FALSE);
        } catch (UnknownHostException e) {
            e.printStackTrace();
            return new BaseReturnFunctionEntity<>(BaseReturnFunctionEntity.StatusFunction.EXCEPTION, e.getMessage());
        } catch (SocketTimeoutException e) {
            e.printStackTrace();
            return new BaseReturnFunctionEntity<>(BaseReturnFunctionEntity.StatusFunction.EXCEPTION, e.getMessage());
        }
    }

    public static boolean updateLocalShareOnlineStatusValue(Context context, boolean isShared) {
        try {
            UserInfo.currentUser.addProperty("ShareOnlineStatus", isShared);
            UserInfo.setCurrentUser(context, UserInfo.getCurrentUser());
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public static BaseReturnFunctionEntity<Boolean> showAvatarSetting(boolean isShared) {
        try {
            JsonObject jsonObject = new JsonObject();
            jsonObject.addProperty("isShown", isShared);

            ClientInfo.DataResponse dataResponse = new ClientInfo().requestApi(ClientInfo.MethodType.PUT, Constants.API_ACCOUNT_SHOW_AVATAR_SETTING, jsonObject);
            if (dataResponse.isOK()) {
                JsonObject jObject = dataResponse.getData(JsonObject.class);
                boolean result = ConvertUtils.toBoolean(jObject.get("IsTrue"));
                if (result)
                    return new BaseReturnFunctionEntity<>(BaseReturnFunctionEntity.StatusFunction.TRUE, true);
                else
                    return new BaseReturnFunctionEntity<>(BaseReturnFunctionEntity.StatusFunction.FALSE, ConvertUtils.toBoolean(jObject.get("Message")));
            }
            return new BaseReturnFunctionEntity<>(BaseReturnFunctionEntity.StatusFunction.FALSE);
        } catch (UnknownHostException e) {
            e.printStackTrace();
            return new BaseReturnFunctionEntity<>(BaseReturnFunctionEntity.StatusFunction.EXCEPTION, e.getMessage());
        } catch (SocketTimeoutException e) {
            e.printStackTrace();
            return new BaseReturnFunctionEntity<>(BaseReturnFunctionEntity.StatusFunction.EXCEPTION, e.getMessage());
        }
    }

    public static boolean updateLocalShowAvatarSettingValue(Context context, boolean isShared) {
        try {
            UserInfo.currentUser.addProperty("ShowAvatar", isShared);
            UserInfo.setCurrentUser(context, UserInfo.getCurrentUser());
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public static BaseReturnFunctionEntity<Boolean> receiveStrangeMessageSetting(boolean isShared) {
        try {
            JsonObject jsonObject = new JsonObject();
            jsonObject.addProperty("isReceived", isShared);

            ClientInfo.DataResponse dataResponse = new ClientInfo().requestApi(ClientInfo.MethodType.PUT, Constants.API_ACCOUNT_RECEIVE_STRANGE_MESSAGE, jsonObject);
            if (dataResponse.isOK()) {
                JsonObject jObject = dataResponse.getData(JsonObject.class);
                boolean result = ConvertUtils.toBoolean(jObject.get("IsTrue"));
                if (result)
                    return new BaseReturnFunctionEntity<>(BaseReturnFunctionEntity.StatusFunction.TRUE, true);
                else
                    return new BaseReturnFunctionEntity<>(BaseReturnFunctionEntity.StatusFunction.FALSE, ConvertUtils.toBoolean(jObject.get("Message")));
            }
            return new BaseReturnFunctionEntity<>(BaseReturnFunctionEntity.StatusFunction.FALSE);
        } catch (UnknownHostException e) {
            e.printStackTrace();
            return new BaseReturnFunctionEntity<>(BaseReturnFunctionEntity.StatusFunction.EXCEPTION, e.getMessage());
        } catch (SocketTimeoutException e) {
            e.printStackTrace();
            return new BaseReturnFunctionEntity<>(BaseReturnFunctionEntity.StatusFunction.EXCEPTION, e.getMessage());
        }
    }

    public static boolean updateLocalReceiveStrangeMessageSettingValue(Context context, boolean isShared) {
        try {
            UserInfo.currentUser.addProperty("ReceiveStrangeMessage", isShared);
            UserInfo.setCurrentUser(context, UserInfo.getCurrentUser());
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public static BaseReturnFunctionEntity<Boolean> showYourPhoneNumberSetting(boolean isShared) {
        try {
            JsonObject jsonObject = new JsonObject();
            jsonObject.addProperty("isShown", isShared);

            ClientInfo.DataResponse dataResponse = new ClientInfo().requestApi(ClientInfo.MethodType.PUT, Constants.API_ACCOUNT_SHOW_PHONE_NUMBER, jsonObject);
            if (dataResponse.isOK()) {
                JsonObject jObject = dataResponse.getData(JsonObject.class);
                boolean result = ConvertUtils.toBoolean(jObject.get("IsTrue"));
                if (result)
                    return new BaseReturnFunctionEntity<>(BaseReturnFunctionEntity.StatusFunction.TRUE, true);
                else
                    return new BaseReturnFunctionEntity<>(BaseReturnFunctionEntity.StatusFunction.FALSE, ConvertUtils.toBoolean(jObject.get("Message")));
            }
            return new BaseReturnFunctionEntity<>(BaseReturnFunctionEntity.StatusFunction.FALSE);
        } catch (UnknownHostException e) {
            e.printStackTrace();
            return new BaseReturnFunctionEntity<>(BaseReturnFunctionEntity.StatusFunction.EXCEPTION, e.getMessage());
        } catch (SocketTimeoutException e) {
            e.printStackTrace();
            return new BaseReturnFunctionEntity<>(BaseReturnFunctionEntity.StatusFunction.EXCEPTION, e.getMessage());
        }
    }

    public static boolean updateLocalShowYourPhoneNumberSettingValue(Context context, boolean isShared) {
        try {
            UserInfo.currentUser.addProperty("ShowPhoneNumber", isShared);
            UserInfo.setCurrentUser(context, UserInfo.getCurrentUser());
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public static BaseReturnFunctionEntity<Boolean> updateSettingAttribute(int settingValue) {
        try {
            JsonObject jsonObject = new JsonObject();
            jsonObject.addProperty("settingValue", settingValue);

            ClientInfo.DataResponse dataResponse = new ClientInfo().requestApi(ClientInfo.MethodType.PUT, Constants.API_ACCOUNT_UPDATE_SETTING_ATTRIBUTE, jsonObject);
            if (dataResponse.isOK()) {
                JsonObject jObject = dataResponse.getData(JsonObject.class);
                boolean result = ConvertUtils.toBoolean(jObject.get("IsTrue"));
                if (result)
                    return new BaseReturnFunctionEntity<>(BaseReturnFunctionEntity.StatusFunction.TRUE, true);
                else
                    return new BaseReturnFunctionEntity<>(BaseReturnFunctionEntity.StatusFunction.FALSE, ConvertUtils.toBoolean(jObject.get("Message")));
            }
            return new BaseReturnFunctionEntity<>(BaseReturnFunctionEntity.StatusFunction.FALSE);
        } catch (UnknownHostException e) {
            e.printStackTrace();
            return new BaseReturnFunctionEntity<>(BaseReturnFunctionEntity.StatusFunction.EXCEPTION, e.getMessage());
        } catch (SocketTimeoutException e) {
            e.printStackTrace();
            return new BaseReturnFunctionEntity<>(BaseReturnFunctionEntity.StatusFunction.EXCEPTION, e.getMessage());
        }
    }

    public static boolean updateLocalSettingAttribute(Context context, int settingValue) {
        try {
            UserInfo.currentUser.addProperty("SettingAttribute", settingValue);
            UserInfo.setCurrentUser(context, UserInfo.getCurrentUser());
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public static BaseReturnFunctionEntity<Boolean> sentEmail(String email) {
        try {
            JsonObject jsonObject = new JsonObject();
            jsonObject.addProperty("email", email);

            ClientInfo.DataResponse dataResponse = new ClientInfo().requestApi(ClientInfo.MethodType.POST, Constants.API_ACCOUNT_FORGOT_PASSWORD, jsonObject);
            if (dataResponse.isOK()) {
                ServiceResponseEntity<Boolean> result = dataResponse.getData(new TypeToken<ServiceResponseEntity<Boolean>>() {
                }.getType());

                if (result.getStatus() == 0) {
                    return new BaseReturnFunctionEntity<>(BaseReturnFunctionEntity.StatusFunction.TRUE, true);
                }else {
                    JsonParser jsonParser=new JsonParser();
                    JsonObject obj=(JsonObject)jsonParser.parse(dataResponse.getBody());
                    return new BaseReturnFunctionEntity<>(BaseReturnFunctionEntity.StatusFunction.FALSE, obj.get("Message").toString());
                }
            }
            return new BaseReturnFunctionEntity<>(BaseReturnFunctionEntity.StatusFunction.FALSE);
        } catch (UnknownHostException e) {
            e.printStackTrace();
            return new BaseReturnFunctionEntity<>(BaseReturnFunctionEntity.StatusFunction.EXCEPTION, e.getMessage());
        } catch (SocketTimeoutException e) {
            e.printStackTrace();
            return new BaseReturnFunctionEntity<>(BaseReturnFunctionEntity.StatusFunction.EXCEPTION, e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            return new BaseReturnFunctionEntity<>(BaseReturnFunctionEntity.StatusFunction.EXCEPTION, e.getMessage());
        }
    }

    // male=1, female=0
    private static int getGender(String gender) {
        if (gender != null && !gender.isEmpty()) {
            if (gender.toLowerCase().equals("male") || gender.toLowerCase().equals("nam"))
                return 1;
        }
        return 0;
    }

    public static BaseReturnFunctionEntity<Boolean> unfriend(String friendId) {
        try {
            if (friendId != null)
                friendId = friendId.toLowerCase();

            JsonObject jsonObject = new JsonObject();
            jsonObject.addProperty("id", friendId);

            ClientInfo.DataResponse dataResponse = new ClientInfo().requestApi(ClientInfo.MethodType.PUT, Constants.API_FRIEND_UNFRIEND, jsonObject);
            if (dataResponse.isOK()) {
                ServiceResponseEntity<Object> result = dataResponse.getData(new TypeToken<ServiceResponseEntity<Object>>() {
                }.getType());
                if (result.getStatus() == 0)
                    return new BaseReturnFunctionEntity<>(BaseReturnFunctionEntity.StatusFunction.TRUE, true);
            }
            return new BaseReturnFunctionEntity<>(BaseReturnFunctionEntity.StatusFunction.FALSE);
        } catch (UnknownHostException e) {
            e.printStackTrace();
            return new BaseReturnFunctionEntity<>(BaseReturnFunctionEntity.StatusFunction.EXCEPTION, e.getMessage());
        } catch (SocketTimeoutException e) {
            e.printStackTrace();
            return new BaseReturnFunctionEntity<>(BaseReturnFunctionEntity.StatusFunction.EXCEPTION, e.getMessage());
        }
    }

    public static boolean isFriend(String friendId) throws SocketTimeoutException {
        try {
            if (friendId != null)
                friendId = friendId.toLowerCase();

            ClientInfo.DataResponse response = new ClientInfo().requestApi(ClientInfo.MethodType.GET, Constants.API_FRIEND_IS_FRIEND + "?id=" + friendId);
            if (response.isOK()) {
                JsonObject jsonObject = response.getData(JsonObject.class);
                boolean result = ConvertUtils.toBoolean(jsonObject.get("IsTrue"));
                return result;
            }
            return false;
        } catch (SocketTimeoutException e) {
            throw e;
        } catch (Exception e) {
            return false;
        }
    }

}
