package com.team500ae.vncapplication.activities.job;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.onurciner.toastox.ToastOXDialog;
import com.team500ae.library.utils.ConvertUtils;
import com.team500ae.library.utils.ImageUtils;
import com.team500ae.vncapplication.R;
import com.team500ae.vncapplication.activities.login.LoginActivity;
import com.team500ae.vncapplication.base.BaseActivity;
import com.team500ae.vncapplication.constants.Constants;
import com.team500ae.vncapplication.data_access.BookmarkInfo;
import com.team500ae.vncapplication.data_access.job.JobInfo;
import com.team500ae.vncapplication.data_access.user.UserInfo;
import com.team500ae.vncapplication.models.BaseReturnFunctionEntity;
import com.team500ae.vncapplication.models.ServiceResponseEntity;
import com.team500ae.vncapplication.services.ServiceJob;
import com.team500ae.vncapplication.utils.ShareUtils;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

import static com.facebook.FacebookSdk.getApplicationContext;

/**
 * Created by Phuong D. Nguyen on 5/17/2018.
 */

public class DetailJobActivity extends BaseActivity implements com.team500ae.library.activities.BaseActivity.OnCallBackBroadcastListener {

    private TextView tvJobTitleContent, tvSalaryContent, tvJobDescriptionContent, tvJobRequirementContent, tvPhoneNumberContent, tvEmailContent;
    private TextView tvJobCategoryContent, tvJobPositionContent, tvCompanyContent, tvNationContent, tvCityContent;
    private CircleImageView civUserAvatar;
    private Menu mMenu;

    @BindView(R.id.btnBookmark)
    ImageView imgBookmark;

    private int id;
    private String createdBy;
    boolean hasBookmark;
    ArrayList<Integer> listBookmarkID;

    @BindView(R.id.lnLocation)
    View lnLocation;
    private String urlImage;
    String urlHeader = "http://maps.google.com/maps/api/staticmap?zoom=16&size=600x600&markers=";
    String urlFooter = "&sensor=false";
    private double lat;
    private double lon;
    private TextView tvAddressContent;

    @OnClick(R.id.lnLocation)
    void direction()
    {
        Intent intent = new Intent(Intent.ACTION_VIEW,
                Uri.parse("http://maps.google.com/maps?daddr=" + lat + "," + lon));
        startActivity(intent);
    }
    @BindView(R.id.imgLocation)
    ImageView imgLocation;

    public static void open(Context context, int id) {
        Intent intent = new Intent(context, DetailJobActivity.class);
        intent.putExtra("ID", id);
        // intent.putExtra("TITLE", title);
        context.startActivity(intent);
    }

    public static void open(Context context, int id, String title) {
        Intent intent = new Intent(context, DetailJobActivity.class);
        intent.putExtra("ID", id);
        intent.putExtra("TITLE", title);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        setContentView(R.layout.activity_job_detail);
        ButterKnife.bind(this);
        super.onCreate(savedInstanceState);

        getSupportActionBar().setTitle(R.string.job_detail_title);
        showBackButton();
        initLayout();

        id = getIntent().getIntExtra("ID", 0);
        //title = getIntent().getStringExtra("TITLE");
        setTitle(getString(R.string.job_detail_title));

        new AsyncTask<Void, Void, BaseReturnFunctionEntity<JsonObject>>() {
            ProgressDialog progDialog = new ProgressDialog(DetailJobActivity.this);

            @Override
            protected void onPreExecute() {
                // showProgress();
                super.onPreExecute();
                progDialog.setMessage(getString(R.string.loading));
                progDialog.setIndeterminate(false);
                progDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                progDialog.setCancelable(true);
                progDialog.show();

            }

            @Override
            protected BaseReturnFunctionEntity<JsonObject> doInBackground(Void... voids) {
                BaseReturnFunctionEntity<JsonObject> data;
                try {
                    data = new JobInfo().getById(getActivity(), id);
                } catch (Exception ex) {
                    return null;
                }
                return data;
            }

            @Override
            protected void onPostExecute(BaseReturnFunctionEntity<JsonObject> data) {
                //super.onPostExecute(data);
                try {
                    if (progDialog.isShowing()) {
                        progDialog.dismiss();
                    }
                }catch (Exception e)
                {
                    e.printStackTrace();
                }
                if (data != null && data.isTrue()) {

                    JsonObject o = data.getData();
                    JsonObject o2 = ConvertUtils.toJsonObject(o.get("Job"));
                    createdBy = ConvertUtils.toString(o2.get("CreatedBy"));
                    if (UserInfo.getCurrentUserId().equals(createdBy)) {
                        menuJobDelete.setVisible(true);
                    }

                    tvJobTitleContent.setText(ConvertUtils.toString(o2.get("Title")));
                    tvSalaryContent.setText(ConvertUtils.toString(o2.get("Salary")));
                    tvJobPositionContent.setText(ConvertUtils.toString(o.get("Level")));
                    tvJobCategoryContent.setText(ConvertUtils.toString(o.get("Type")));
                    tvCityContent.setText(ConvertUtils.toString(o.get("City")));
                    tvNationContent.setText(ConvertUtils.toString(o.get("Country")));
                    tvCompanyContent.setText(ConvertUtils.toString(o2.get("Company")));
                    tvJobDescriptionContent.setText(ConvertUtils.toString(o2.get("Description")));
                    tvJobRequirementContent.setText(ConvertUtils.toString(o2.get("Requirement")));
                    //String phoneNumber = ConvertUtils.toString(o2.get("Phone"))";
                    tvPhoneNumberContent.setText(ConvertUtils.toString(o2.get("Phone")));
                   /* if(phoneNumber.length()>0)
                    {
                        tvPhone.setTextColor(getResources().getColor(R.color.blue_50));
                        tvPhone.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                             str
                            }
                        });
                    }*/
                    tvEmailContent.setText(ConvertUtils.toString(o2.get("Email")));
                    ImageUtils.loadImageByGlide(getApplicationContext(), true, 300, 300, ConvertUtils.toString(o2.get("Thumbnail")), R.drawable.img_no_image, R.drawable.img_no_image, civUserAvatar, true);
                    lat = ConvertUtils.toDouble(o2.get("Lat"));
                    lon = ConvertUtils.toDouble(o2.get("Long"));
                    if(lat!=0&&lon!=0) {
                        urlImage = urlHeader + lat + "," + lon + urlFooter;
                        ImageUtils.loadImageByGlide(getActivity(), false, 0, 150, urlImage, R.drawable.bg_map_vn, R.drawable.bg_map_vn, imgLocation, true);
                    }
                    else
                    {
                        lnLocation.setVisibility(View.GONE);
                    }
                    tvAddressContent.setText(ConvertUtils.toString(o2.get("Address")));

//                    Glide.with(getActivity()).load(ConvertUtils.toString(o2.get("Thumbnail"))).apply(new RequestOptions().placeholder(R.drawable.img_no_image)).into(imgThumbnail);
                }
                //hideProgress();
            }


        }.execute();

        listBookmarkID = new ArrayList<>();
        if (!UserInfo.isLogin()) {
            imgBookmark.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.ic_like));
        }else {
            new AsyncTask<Void, Void, ServiceResponseEntity<JsonArray>>() {
                @Override
                protected ServiceResponseEntity<JsonArray> doInBackground(Void... voids) {
                    ServiceResponseEntity<JsonArray> data;
                    try {
                        data = new JobInfo().getAllWithCity(getApplicationContext(), 1, 99999999, -2);
                    } catch (Exception ex) {
                        return null;
                    }
                    return data;
                }

                @Override
                protected void onPostExecute(ServiceResponseEntity<JsonArray> data) {
                    if (data != null && data.getStatus() == 0) {
                        for (int i = 0; i < data.getData().size(); i++) {
                            JsonObject jsonObject = ConvertUtils.toJsonObject(data.getData().get(i));
                            JsonObject jsonObject2 = ConvertUtils.toJsonObject(jsonObject.get("Job"));
                            listBookmarkID.add(ConvertUtils.toInt(jsonObject2.get("ID")));
                        }

                    }
                    if (listBookmarkID.contains(id))
                        hasBookmark = true;
                    else
                        hasBookmark = false;
                    imgBookmark.setImageDrawable(ContextCompat.getDrawable(getActivity(), hasBookmark ? R.drawable.ic_like_check : R.drawable.ic_like));
                }


            }.execute();
        }
        //BaseReturnFunctionEntity<Boolean> bookmark = new BookmarkInfo().isBookmark(getActivity(), getRealm(), id, UserInfo.getCurrentUserId(), BookmarkInfo.BookmarkType.Job);
        //hasBookmark = bookmark.isTrue() ? bookmark.data : false;
        //imgBookmark.setImageDrawable(ContextCompat.getDrawable(getActivity(), hasBookmark ? R.drawable.ic_like_check : R.drawable.ic_like));
        registerBroadcastReceiver(new ReceiverItem(Constants.ACTION_LOGIN, this));
    }

    private void initLayout() {
        View vJobTitleInclude = findViewById(R.id.layout_job_title);
        ImageView ivJobTitleLeftIcon = (ImageView) vJobTitleInclude.findViewById(R.id.image_left);
        ivJobTitleLeftIcon.setImageResource(R.drawable.ic_job_title);
        TextView tvJobTitleTitle = (TextView) vJobTitleInclude.findViewById(R.id.text_title);
        tvJobTitleTitle.setText(getResources().getString(R.string.job_title));
        tvJobTitleTitle.setTextColor(getResources().getColor(R.color.colorPrimary));
        tvJobTitleContent = (TextView) vJobTitleInclude.findViewById(R.id.text_content);
        tvJobTitleContent.setTextIsSelectable(true);
        ImageView ivJobTitleRightIcon = (ImageView) vJobTitleInclude.findViewById(R.id.image_right);
        ivJobTitleRightIcon.setVisibility(View.GONE);

        View vJobCategoryInclude = findViewById(R.id.layout_job_category);
        ImageView ivJobCategoryLeftIcon = (ImageView) vJobCategoryInclude.findViewById(R.id.image_left);
        ivJobCategoryLeftIcon.setImageResource(R.drawable.ic_job_category);
        TextView tvJobCategoryTitle = (TextView) vJobCategoryInclude.findViewById(R.id.text_title);
        tvJobCategoryTitle.setText(getResources().getString(R.string.job_category));
        tvJobCategoryContent = (TextView) vJobCategoryInclude.findViewById(R.id.text_content);
        tvJobCategoryContent.setTextIsSelectable(true);
        ImageView ivJobCategoryRightIcon = (ImageView) vJobCategoryInclude.findViewById(R.id.image_right);
        ivJobCategoryRightIcon.setVisibility(View.GONE);

        View vJobPositionInclude = findViewById(R.id.layout_job_position);
        ImageView ivJobPositionLeftIcon = (ImageView) vJobPositionInclude.findViewById(R.id.image_left);
        ivJobPositionLeftIcon.setImageResource(R.drawable.ic_job);
        TextView tvJobPositionTitle = (TextView) vJobPositionInclude.findViewById(R.id.text_title);
        tvJobPositionTitle.setText(getResources().getString(R.string.job_position));
        tvJobPositionContent = (TextView) vJobPositionInclude.findViewById(R.id.text_content);
        tvJobPositionContent.setTextIsSelectable(true);
        ImageView ivJobPositionRightIcon = (ImageView) vJobPositionInclude.findViewById(R.id.image_right);
        ivJobPositionRightIcon.setVisibility(View.GONE);

        View vSalaryInclude = findViewById(R.id.layout_salary);
        ImageView ivSalaryLeftIcon = (ImageView) vSalaryInclude.findViewById(R.id.image_left);
        ivSalaryLeftIcon.setImageResource(R.drawable.ic_salary);
        TextView tvSalaryTitle = (TextView) vSalaryInclude.findViewById(R.id.text_title);
        tvSalaryTitle.setText(getResources().getString(R.string.salary));
        tvSalaryContent = (TextView) vSalaryInclude.findViewById(R.id.text_content);
        tvSalaryContent.setTextIsSelectable(true);
        ImageView ivSalaryRightIcon = (ImageView) vSalaryInclude.findViewById(R.id.image_right);
        ivSalaryRightIcon.setVisibility(View.GONE);

        View vCompanyInclude = findViewById(R.id.layout_company);
        ImageView ivCompanyLeftIcon = (ImageView) vCompanyInclude.findViewById(R.id.image_left);
        ivCompanyLeftIcon.setImageResource(R.drawable.ic_company);
        TextView tvCompanyTitle = (TextView) vCompanyInclude.findViewById(R.id.text_title);
        tvCompanyTitle.setText(getResources().getString(R.string.company));
        tvCompanyContent = (TextView) vCompanyInclude.findViewById(R.id.text_content);
        tvCompanyContent.setTextIsSelectable(true);
        ImageView ivCompanyRightIcon = (ImageView) vCompanyInclude.findViewById(R.id.image_right);
        ivCompanyRightIcon.setVisibility(View.GONE);

        View vNationInclude = findViewById(R.id.layout_nation);
        ImageView ivNationLeftIcon = (ImageView) vNationInclude.findViewById(R.id.image_left);
        ivNationLeftIcon.setImageResource(R.drawable.ic_nation);
        TextView tvNationTitle = (TextView) vNationInclude.findViewById(R.id.text_title);
        tvNationTitle.setText(getResources().getString(R.string.nation));
        tvNationContent = (TextView) vNationInclude.findViewById(R.id.text_content);
        tvNationContent.setTextIsSelectable(true);
        ImageView ivNationRightIcon = (ImageView) vNationInclude.findViewById(R.id.image_right);
        ivNationRightIcon.setVisibility(View.GONE);

        View vCityInclude = findViewById(R.id.layout_city);
        ImageView ivCityLeftIcon = (ImageView) vCityInclude.findViewById(R.id.image_left);
        ivCityLeftIcon.setImageResource(R.drawable.ic_address);
        TextView tvCityTitle = (TextView) vCityInclude.findViewById(R.id.text_title);
        tvCityTitle.setText(getResources().getString(R.string.city));
        tvCityContent = (TextView) vCityInclude.findViewById(R.id.text_content);
        tvCityTitle.setTextIsSelectable(true);
        ImageView ivCityRightIcon = (ImageView) vCityInclude.findViewById(R.id.image_right);
        ivCityRightIcon.setVisibility(View.GONE);

        View vJobDescriptionInclude = findViewById(R.id.layout_job_description);
        ImageView ivJobDescriptionLeftIcon = (ImageView) vJobDescriptionInclude.findViewById(R.id.image_left);
        ivJobDescriptionLeftIcon.setImageResource(R.drawable.ic_job_description);
        TextView tvJobDescriptionTitle = (TextView) vJobDescriptionInclude.findViewById(R.id.text_title);
        tvJobDescriptionTitle.setVisibility(View.GONE);
        tvJobDescriptionContent = (TextView) vJobDescriptionInclude.findViewById(R.id.text_content);
        tvJobDescriptionTitle.setTextIsSelectable(true);
        tvJobDescriptionContent.setGravity(Gravity.LEFT);
        ImageView ivJobDescriptionRightIcon = (ImageView) vJobDescriptionInclude.findViewById(R.id.image_right);
        ivJobDescriptionRightIcon.setVisibility(View.GONE);

        View vJobRequirementInclude = findViewById(R.id.layout_job_requirement);
        ImageView ivJobRequirementLeftIcon = (ImageView) vJobRequirementInclude.findViewById(R.id.image_left);
        ivJobRequirementLeftIcon.setImageResource(R.drawable.ic_job_requirement);
        TextView tvJobRequirementTitle = (TextView) vJobRequirementInclude.findViewById(R.id.text_title);
        tvJobRequirementTitle.setVisibility(View.GONE);
        tvJobRequirementContent = (TextView) vJobRequirementInclude.findViewById(R.id.text_content);
        tvJobRequirementContent.setGravity(Gravity.LEFT);
        tvJobRequirementContent.setTextIsSelectable(true);
        ImageView ivJobRequirementRightIcon = (ImageView) vJobRequirementInclude.findViewById(R.id.image_right);
        ivJobRequirementRightIcon.setVisibility(View.GONE);

        View vPhoneNumberInclude = findViewById(R.id.layout_phone_number);
        ImageView ivPhoneNumberLeftIcon = (ImageView) vPhoneNumberInclude.findViewById(R.id.image_left);
        ivPhoneNumberLeftIcon.setImageResource(R.drawable.ic_phone_number);
        TextView tvPhoneNumberTitle = (TextView) vPhoneNumberInclude.findViewById(R.id.text_title);
        tvPhoneNumberTitle.setText(getResources().getString(R.string.phone_number));
        tvPhoneNumberContent = (TextView) vPhoneNumberInclude.findViewById(R.id.text_content);
        tvPhoneNumberContent.setTextIsSelectable(true);
        ImageView ivPhoneNumberRightIcon = (ImageView) vPhoneNumberInclude.findViewById(R.id.image_right);
        ivPhoneNumberRightIcon.setVisibility(View.GONE);

        View vAddressInclude = findViewById(R.id.layout_address);
        ImageView ivAddressLeftIcon = (ImageView) vAddressInclude.findViewById(R.id.image_left);
        ivAddressLeftIcon.setImageResource(R.drawable.ic_address);
        TextView tvAddressTitle = (TextView) vAddressInclude.findViewById(R.id.text_title);
        tvAddressTitle.setText(getResources().getString(R.string.address));
        tvAddressContent = (TextView) vAddressInclude.findViewById(R.id.text_content);
        tvAddressContent.setTextIsSelectable(true);
        ImageView ivAddressRightIcon = (ImageView) vAddressInclude.findViewById(R.id.image_right);
        ivAddressRightIcon.setVisibility(View.GONE);

        View vEmailInclude = findViewById(R.id.layout_email);
        ImageView ivEmailLeftIcon = (ImageView) vEmailInclude.findViewById(R.id.image_left);
        ivEmailLeftIcon.setImageResource(R.drawable.ic_email);
        TextView tvEmailTitle = (TextView) vEmailInclude.findViewById(R.id.text_title);
        tvEmailTitle.setText(getResources().getString(R.string.email));
        tvEmailTitle.setTextColor(getResources().getColor(R.color.colorPrimary));
        tvEmailContent = (TextView) vEmailInclude.findViewById(R.id.text_content);
        tvEmailContent.setTextIsSelectable(true);
        ImageView ivEmailRightIcon = (ImageView) vEmailInclude.findViewById(R.id.image_right);
        ivEmailRightIcon.setVisibility(View.GONE);

        civUserAvatar = (CircleImageView) findViewById(R.id.civ_user_avatar);
        TextView profileTitle = (TextView) findViewById(R.id.text_title);
        profileTitle.setText(getResources().getString(R.string.job_information));
    }

    @OnClick(R.id.btnBookmark)
    void bookmark() {
        if (!UserInfo.isLogin()) {
            LoginActivity.open(getActivity());
            return;
        } else {
            hasBookmark = !hasBookmark;
            ServiceJob.startActionBookmark(getActivity(), id, hasBookmark);
            imgBookmark.setImageDrawable(ContextCompat.getDrawable(getActivity(), hasBookmark ? R.drawable.ic_like_check : R.drawable.ic_like));
        }
    }

    @OnClick(R.id.llShare)
    void share() {
        ShareUtils.shareFacebook(getActivity(), Constants.SERVER + "/applink/job/" + id, "iDuhoc");
    }


    @Override
    public void onCallBackBroadcastListener(Context context, String action, Intent intent) {
        switch (action) {
            case Constants.ACTION_LOGIN:
                listBookmarkID = new ArrayList<>();
                new AsyncTask<Void, Void, ServiceResponseEntity<JsonArray>>() {
                    @Override
                    protected ServiceResponseEntity<JsonArray> doInBackground(Void... voids) {
                        ServiceResponseEntity<JsonArray> data;
                        try {
                            data = new JobInfo().getAllWithCity(getApplicationContext(), 1, 99999999, -2);
                        } catch (Exception ex) {
                            return null;
                        }
                        return data;
                    }

                    @Override
                    protected void onPostExecute(ServiceResponseEntity<JsonArray> data) {
                        if (data != null && data.getStatus() == 0) {
                            for (int i=0; i< data.getData().size();i++)
                            {
                                JsonObject jsonObject = ConvertUtils.toJsonObject(data.getData().get(i));
                                JsonObject jsonObject2 = ConvertUtils.toJsonObject(jsonObject.get("Job"));
                                listBookmarkID.add(ConvertUtils.toInt(jsonObject2.get("ID")));
                            }
                        }
                        if(listBookmarkID.contains(id))
                            hasBookmark=true;
                        else
                            hasBookmark=false;
                        imgBookmark.setImageDrawable(ContextCompat.getDrawable(getActivity(), hasBookmark ? R.drawable.ic_like_check : R.drawable.ic_like));
                    }
                }.execute();
                break;
        }
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    MenuItem menuJobDelete;

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        mMenu = menu;
        getMenuInflater().inflate(R.menu.menu_job_detail, menu);
        menuJobDelete = menu.findItem(R.id.action_job_delete);
        menuJobDelete.setVisible(false);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_job_delete:
                new ToastOXDialog.Build(getActivity()).setTitle(R.string.confirm).setContent(getString(R.string.job_dialog_delete))
                        .setPositiveText(R.string.yes).setPositiveBackgroundColorResource(R.color.btnyes).setPositiveTextColorResource(R.color.black).onPositive(new ToastOXDialog.ButtonCallback() {
                    @Override
                    public void onClick(@NonNull ToastOXDialog toastOXDialog) {
                        new AsyncTask<Void, Void, BaseReturnFunctionEntity<Boolean>>() {
                            ProgressDialog progDialog = new ProgressDialog(DetailJobActivity.this);

                            @Override
                            protected void onPreExecute() {
                                // showProgress();
                                super.onPreExecute();
                                progDialog.setMessage(getString(R.string.loading));
                                progDialog.setIndeterminate(false);
                                progDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                                progDialog.setCancelable(true);
                                progDialog.show();

                            }

                            @Override
                            protected BaseReturnFunctionEntity<Boolean> doInBackground(Void... voids) {
                                BaseReturnFunctionEntity<Boolean> data;
                                try {
                                    data = new JobInfo().delete(getActivity(), id);
                                } catch (Exception ex) {
                                    return null;
                                }
                                return data;
                            }

                            @Override
                            protected void onPostExecute(BaseReturnFunctionEntity<Boolean> data) {
                                //super.onPostExecute(data);
                                if (progDialog.isShowing()) {
                                    progDialog.dismiss();
                                }
                                if (data != null && data.isTrue()) {
                                    Toast.makeText(DetailJobActivity.this, "Xóa thành công", Toast.LENGTH_SHORT).show();
                                    Intent intent = new Intent(Constants.ACTION_JOB_ADD_SUCCESS);
                                    getApplicationContext().sendBroadcast(intent);
                                    onBackPressed();
                                } else
                                    Toast.makeText(DetailJobActivity.this, "Xóa thất bại", Toast.LENGTH_SHORT).show();
                            }


                        }.execute();
                    }
                }).setNegativeText(R.string.cancel)
                        .setNegativeBackgroundColorResource(R.color.dim)
                        .setNegativeTextColorResource(R.color.white).show();
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
