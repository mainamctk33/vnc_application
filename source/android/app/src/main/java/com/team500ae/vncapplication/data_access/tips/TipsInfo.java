package com.team500ae.vncapplication.data_access.tips;

import android.content.Context;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.team500ae.vncapplication.constants.Constants;
import com.team500ae.vncapplication.data_access.ClientInfo;
import com.team500ae.vncapplication.models.BaseReturnFunctionEntity;
import com.team500ae.vncapplication.models.ServiceResponseEntity;

import java.net.SocketTimeoutException;
import java.net.UnknownHostException;

/**
 * Created by phuong.ndp on 26/11/2017.
 * 171204-1036pm
 */

public class TipsInfo {
    public ServiceResponseEntity<JsonArray> getAll(Context context, int page, int size, int type, String keyword) {
        try {
            ClientInfo.DataResponse dataResponse = new ClientInfo().requestApi(ClientInfo.MethodType.GET, Constants.API_TIPS_GETALL + "?type=" + type + "&page=" + page + "&size=" + size+"&keyword="+keyword);
            if (dataResponse.isOK()) {
                ServiceResponseEntity<JsonArray> result = dataResponse.getData(new TypeToken<ServiceResponseEntity<JsonArray>>() {
                }.getType());
                return result;
            }
        } catch (UnknownHostException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public BaseReturnFunctionEntity<Boolean> bookmark(Context applicationContext, int id, boolean bookmark) {
        try {
            JsonObject jsonObject = new JsonObject();
            jsonObject.addProperty("bookmark", bookmark);
            ClientInfo.DataResponse dataResponse = new ClientInfo().requestApi(ClientInfo.MethodType.PUT, Constants.API_TIPS_BOOKMARK + "/" + id, jsonObject);
            if (dataResponse.isOK()) {
                ServiceResponseEntity<Object> result = dataResponse.getData(new TypeToken<ServiceResponseEntity<Object>>() {
                }.getType());
                if (result.getStatus() == 0)
                    return new BaseReturnFunctionEntity<>(BaseReturnFunctionEntity.StatusFunction.TRUE, true);
            }
            return new BaseReturnFunctionEntity<>(BaseReturnFunctionEntity.StatusFunction.FALSE);
        } catch (UnknownHostException e) {
            e.printStackTrace();
            return new BaseReturnFunctionEntity<>(BaseReturnFunctionEntity.StatusFunction.EXCEPTION, e.getMessage());
        } catch (SocketTimeoutException e) {
            e.printStackTrace();
            return new BaseReturnFunctionEntity<>(BaseReturnFunctionEntity.StatusFunction.EXCEPTION, e.getMessage());
        }
    }

    public BaseReturnFunctionEntity<JsonObject> getById(Context context, int id) {
        try {
            ClientInfo.DataResponse dataResponse = new ClientInfo().requestApi(ClientInfo.MethodType.GET, Constants.API_TIPS_GET_BY_ID + "/" + id);
            if (dataResponse.isOK()) {
                ServiceResponseEntity<JsonObject> result = dataResponse.getData(new TypeToken<ServiceResponseEntity<JsonObject>>() {
                }.getType());
                if (result.getStatus() == 0) {
                    return new BaseReturnFunctionEntity<>(BaseReturnFunctionEntity.StatusFunction.TRUE, result.getData());
                }
            }
            return new BaseReturnFunctionEntity<>(BaseReturnFunctionEntity.StatusFunction.FALSE);
        } catch (UnknownHostException e) {
            e.printStackTrace();
            return new BaseReturnFunctionEntity<>(BaseReturnFunctionEntity.StatusFunction.EXCEPTION, e.getMessage());
        } catch (SocketTimeoutException e) {
            e.printStackTrace();
            return new BaseReturnFunctionEntity<>(BaseReturnFunctionEntity.StatusFunction.EXCEPTION, e.getMessage());
        }
    }
}
