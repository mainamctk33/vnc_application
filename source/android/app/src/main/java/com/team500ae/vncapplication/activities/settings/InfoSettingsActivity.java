package com.team500ae.vncapplication.activities.settings;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.team500ae.vncapplication.BuildConfig;
import com.team500ae.vncapplication.R;
import com.team500ae.vncapplication.activities.login.ChangePasswordActivity;
import com.team500ae.vncapplication.activities.login.EditAccountActivity;
import com.team500ae.vncapplication.base.BaseActivity;
import com.team500ae.vncapplication.data_access.user.UserInfo;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by Administrator on 07/06/2018.
 */

public class InfoSettingsActivity extends BaseActivity {

    @BindView(R.id.layout_error_report)
    View vErrorReport;
    @BindView(R.id.layout_iduhoc_service)
    View vIduhocService;
    @BindView(R.id.layout_app_version)
    View vAppVersion;

    public static void open(Context activity) {
        Intent intent = new Intent(activity, InfoSettingsActivity.class);
        activity.startActivity(intent);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        setContentView(R.layout.activity_info_settings);
        super.onCreate(savedInstanceState);
        showBackButton();

        initLayout();
    }

    private void initLayout() {
        ImageView ivErrorReportLeftIcon = (ImageView) vErrorReport.findViewById(R.id.iv_left_icon);
        ivErrorReportLeftIcon.setImageResource(R.drawable.ic_flag);
        TextView tvErrorReportTitle = (TextView) vErrorReport.findViewById(R.id.tv_title);
        tvErrorReportTitle.setText(getResources().getString(R.string.pref_info_error_reports_title));
        TextView tvErrorReportSubTitle = (TextView) vErrorReport.findViewById(R.id.tv_sub_title);
        tvErrorReportSubTitle.setText(getResources().getString(R.string.pref_info_error_reports_summary));

        ImageView ivIduhocServiceLeftIcon = (ImageView) vIduhocService.findViewById(R.id.iv_left_icon);
        ivIduhocServiceLeftIcon.setImageResource(R.drawable.ic_calender);
        TextView tvIduhocServiceTitle = (TextView) vIduhocService.findViewById(R.id.tv_title);
        tvIduhocServiceTitle.setText(getResources().getString(R.string.pref_info_iduhoc_service_title));
        TextView tvIduhocServiceSubTitle = (TextView) vIduhocService.findViewById(R.id.tv_sub_title);
        tvIduhocServiceSubTitle.setText(getResources().getString(R.string.pref_info_iduhoc_service_summary));

        ImageView ivAppVersionLeftIcon = (ImageView) vAppVersion.findViewById(R.id.iv_left_icon);
        ivAppVersionLeftIcon.setImageResource(R.drawable.ic_phone_number);
        TextView tvAppVersionTitle = (TextView) vAppVersion.findViewById(R.id.tv_title);
        tvAppVersionTitle.setText(getResources().getString(R.string.pref_info_app_version_title));
        TextView tvAppVersionSubTitle = (TextView) vAppVersion.findViewById(R.id.tv_sub_title);
        tvAppVersionSubTitle.setText(getResources().getString(R.string.pref_info_app_version_summary));

//        int versionCode = BuildConfig.VERSION_CODE;
        String versionName = BuildConfig.VERSION_NAME;
//        appVersion.setSummary(versionName);
        tvAppVersionSubTitle.setText(versionName);
    }

    @OnClick(R.id.layout_error_report)
    void errorReport() {
        String appPackageName = getActivity().getPackageName(); // getPackageName() from Context or Activity object
        try {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
        } catch (android.content.ActivityNotFoundException anfe) {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
        }
    }

    @OnClick(R.id.layout_iduhoc_service)
    void iDuhocService() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage("VĂN PHÒNG HÀ NỘI\n" +
                "Địa chỉ: số 15/ ngách 26/ phố Nghĩa Đô (ngõ 100 Hoàng Quốc Việt cũ), quận Cầu Giấy, Hà Nội\n" +
                "Điện thoại: 0903 405 535 (Mr. Kiên)\n" +
                "\n" +
                "CHI NHÁNH HẢI PHÒNG\n" +
                "Địa chỉ: số 7, ngõ 275 Lê Lợi, quận Ngô Quyền, Hải Phòng\n" +
                "Điện thoại: 0904 884 774 (Mr.Việt)\n" +
                "\n" +
                "Email: kienkhuc@gmail.com\n" +
                "Website: http://iduhoc.vn\n" +
                "Facebook: https://www.facebook.com/kienkhuc.ideutsch");
        builder.setTitle(getResources().getString(R.string.pref_info_iduhoc_service_title));
        builder.setIcon(R.drawable.iduhoc_logo_114x60);
        builder.setPositiveButton(getString(android.R.string.ok),
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // do the work to update the preference ---
                    }
                });
        builder.create().show();
    }
}
