package com.team500ae.vncapplication.models.realm_models.location;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by maina on 11/16/2017.
 */

public class MyTypeLocationEntity extends RealmObject {
    @PrimaryKey
    String id;
    String userId;
    TypeLocationEntity typeLocation;
    long updatedDate;

    public long getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(long updatedDate) {
        this.updatedDate = updatedDate;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public TypeLocationEntity getTypeLocation() {
        return typeLocation;
    }

    public void setTypeLocation(TypeLocationEntity typeLocation) {
        this.typeLocation = typeLocation;
    }
}
