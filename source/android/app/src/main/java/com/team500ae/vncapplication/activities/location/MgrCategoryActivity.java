package com.team500ae.vncapplication.activities.location;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.onurciner.toastox.ToastOXDialog;
import com.team500ae.library.adapters.BaseRecyclerAdapter;
import com.team500ae.library.layoutmanager.MyLinearLayoutManager;
import com.team500ae.vncapplication.R;
import com.team500ae.vncapplication.base.BaseActivity;
import com.team500ae.vncapplication.base.BaseViewHolder;
import com.team500ae.vncapplication.constants.Constants;
import com.team500ae.vncapplication.data_access.location.TypeLocationInfo;
import com.team500ae.vncapplication.data_access.user.UserInfo;
import com.team500ae.vncapplication.models.BaseReturnFunctionEntity;
import com.team500ae.vncapplication.models.realm_models.location.TypeLocationEntity;
import com.team500ae.vncapplication.utils.LanguageUtils;
import com.team500ae.vncapplication.utils.dragdrop.CallbackItemTouch;
import com.team500ae.vncapplication.utils.dragdrop.MyItemTouchHelperCallback;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.OnClick;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 * Created by maina on 11/23/2017.
 */

public class MgrCategoryActivity extends BaseActivity implements CallbackItemTouch {

    @BindView(R.id.block_reading_mode)
    LinearLayout readingModeLayout;

    private BaseRecyclerAdapter<ViewHolder, TypeLocationEntity> adapter;
    private ArrayList<TypeLocationEntity> listData;
    private int typeView;

    public static void open(Context context) {
        Intent intent = new Intent(context, MgrCategoryActivity.class);
        context.startActivity(intent);
        BaseActivity.setSlideIn(context, TypeSlideIn.in_right_to_left);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
//        setContentView(R.layout.activity_mgr_category2);
        setContentView(R.layout.activity_mgr_category);
        super.onCreate(savedInstanceState);
        getSupportActionBar().setTitle(R.string.activity_location_type);

        readingModeLayout.setVisibility(View.GONE);

        recyclerView.setLayoutManager(new MyLinearLayoutManager(getActivity()));
        listData = new ArrayList<>();
//        adapter = new BaseRecyclerAdapter<ViewHolder, TypeLocationEntity>(R.layout.item_my_type_location, new BaseRecyclerAdapter.BaseViewHolder<ViewHolder, TypeLocationEntity>() {
        adapter = new BaseRecyclerAdapter<ViewHolder, TypeLocationEntity>(R.layout.layout_category_item, new BaseRecyclerAdapter.BaseViewHolder<ViewHolder, TypeLocationEntity>() {
            @Override
            public ViewHolder getViewHolder(View v) {
                return new ViewHolder(v);
            }

            @Override
            public void bindData(RecyclerView recyclerView, ViewHolder viewHolder, TypeLocationEntity data, int position) {
                viewHolder.tvName.setText(LanguageUtils.getAppLanguageSetting(MgrCategoryActivity.this).equals("en") ? data.getName() : data.getEnName());
                viewHolder.tvIndex.setText(String.valueOf(position + 1));
            }
        }, listData, new BaseRecyclerAdapter.OnClickListener() {
            @Override
            public void onClick(View v, int position, Object o) {

            }
        });
        adapter.bindData(recyclerView);

        ItemTouchHelper.Callback callback = new MyItemTouchHelperCallback(this);// create MyItemTouchHelperCallback
        ItemTouchHelper touchHelper = new ItemTouchHelper(callback); // Create ItemTouchHelper and pass with parameter the MyItemTouchHelperCallback
        touchHelper.attachToRecyclerView(recyclerView); // Attach ItemTouchHelper to RecyclerView
        showBackButton(true);
    }

    @Override
    protected void onResume() {
        getData();
        super.onResume();
    }

    void getData() {
        BaseReturnFunctionEntity<ArrayList<TypeLocationEntity>> myListTypeLocation = new TypeLocationInfo().getTypeLocationHasSelected(getActivity(), getRealm(), UserInfo.getCurrentUserId());
        listData.clear();
        if (myListTypeLocation.isTrue())
            listData.addAll(myListTypeLocation.getData());
        adapter.notifyDataSetChanged();
    }

    @OnClick(R.id.btnAddCat)
    void addCat() {
        int numOfNotSelectedItems = getNumberOfNotSeletedItems();
        if (numOfNotSelectedItems == 0) {
            showSnackBar(R.string.select_all_of_items);
        } else {
            ChooseCategoryActivity.open(getActivity());
        }
    }

    int getNumberOfNotSeletedItems() {
        BaseReturnFunctionEntity<ArrayList<TypeLocationEntity>> listTypeLocation = new TypeLocationInfo().getListTypeLocation(getActivity(), getRealm());
        ArrayList<TypeLocationEntity> listItems = new ArrayList<>();
        if (listTypeLocation.isTrue()) {
            listItems.addAll(listTypeLocation.getData());
            listTypeLocation = new TypeLocationInfo().getTypeLocationHasSelected(getActivity(), getRealm(), UserInfo.getCurrentUserId());
            if (listTypeLocation.isTrue()) {
                for (TypeLocationEntity typeLocationEntity : listTypeLocation.getData()) {
                    listItems.remove(typeLocationEntity);
                }
            }
        }
        return listItems.size();
    }

    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;

    public class ViewHolder extends BaseViewHolder {
        @BindView(R.id.tvName)
        TextView tvName;
        @BindView(R.id.tvIndex)
        TextView tvIndex;

        @OnClick(R.id.btnRemove)
        void remove() {
            if (listData.size() < 2) {
                AlertDialog.Builder builderInner = new AlertDialog.Builder(MgrCategoryActivity.this);
                builderInner.setIcon(R.drawable.ic_info);
                builderInner.setTitle(getResources().getString(R.string.notice));
                builderInner.setMessage(getResources().getString(R.string.category_require_one_item));
                builderInner.setPositiveButton(R.string.select_language_ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                builderInner.show();
                return;
            }

            int index = getAdapterPosition();
            TypeLocationEntity typeLocationEntity = listData.get(index);
            String appLanguageSetting = LanguageUtils.getAppLanguageSetting(MgrCategoryActivity.this);
            new ToastOXDialog.Build(getActivity())
                    .setTitle(R.string.confirm)
                    .setContent(getResources().getString(R.string.do_you_want_to_delete) + (appLanguageSetting.equals("en") ? typeLocationEntity.getName() : typeLocationEntity.getEnName()) + getResources().getString(R.string.from_display_list))
                    .setPositiveText(R.string.yes)
                    .setPositiveBackgroundColorResource(R.color.colorPrimary)
                    .setPositiveTextColorResource(R.color.white)
                    .onPositive(new ToastOXDialog.ButtonCallback() {
                        @Override
                        public void onClick(@NonNull ToastOXDialog toastOXDialog) {
                            BaseReturnFunctionEntity<Boolean> result = new TypeLocationInfo().removeTypeLocationFromListFavorite(getActivity(), getRealm(), UserInfo.getCurrentUserId(), listData.get(index).getId());
                            if (result.isTrue() && result.getData()) {
                                listData.remove(index);
                                adapter.notifyItemRemoved(index);
                                Intent intent = new Intent(Constants.ACTION_TYPE_LOCATION_FOR_USER_CHANGED);
                                sendBroadcast(intent);
                            }
                        }
                    })
                    .setNegativeText(R.string.cancel)
                    .setNegativeBackgroundColorResource(R.color.dim)
                    .setNegativeTextColorResource(R.color.white)
                    .onNegative(new ToastOXDialog.ButtonCallback() {
                        @Override
                        public void onClick(@NonNull ToastOXDialog toastOXDialog) {
                        }
                    }).show();
        }

        public ViewHolder(View itemView) {
            super(itemView);
        }
    }

    @Override
    public void itemTouchOnMove(int oldPosition, int newPosition) {
        listData.add(newPosition, listData.remove(oldPosition));// change position
        adapter.notifyItemMoved(oldPosition, newPosition); //notifies changes in adapter, in this case use the notifyItemMoved
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
}
