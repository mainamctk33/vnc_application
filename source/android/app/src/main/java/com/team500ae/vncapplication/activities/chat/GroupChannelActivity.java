package com.team500ae.vncapplication.activities.chat;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;

import com.sendbird.android.GroupChannel;
import com.sendbird.android.SendBird;
import com.sendbird.android.SendBirdException;
import com.sendbird.android.User;
import com.team500ae.library.utils.ConvertUtils;
import com.team500ae.vncapplication.Connectivity;
import com.team500ae.vncapplication.R;
import com.team500ae.vncapplication.activities.chat.fragments.GroupChatFragment;
import com.team500ae.vncapplication.base.BaseActivity;
import com.team500ae.vncapplication.data_access.user.UserInfo;
import com.team500ae.vncapplication.services.ServiceChat;
import com.team500ae.vncapplication.utils.AppUtils;

import java.util.ArrayList;

public class GroupChannelActivity extends BaseActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        setContentView(R.layout.activity_group_channel);
        super.onCreate(savedInstanceState);
        showBackButton();

        String channelUrl = getIntent().getStringExtra("groupChannelUrl");
        if (channelUrl != null) {
            // If started from notification
            Fragment fragment = GroupChatFragment.newInstance(channelUrl);
            FragmentManager manager = getSupportFragmentManager();
            manager.beginTransaction()
                    .replace(R.id.container_group_channel, fragment)
                    .commit();
        }
    }

    public static void open(Context context, String userId) {
        if (!UserInfo.isLogin()) {
            showSnackBar(context, R.string.please_login_to_chat_with_this_user);
            return;
        }
        if (!AppUtils.isNetworkAvailable(context)) {
            showSnackBar(context, R.string.please_check_internet_connection);
            return;
        }
        if (SendBird.getConnectionState() != SendBird.ConnectionState.OPEN) {
            SendBird.connect(userId, new SendBird.ConnectHandler() {
                @Override
                public void onConnected(User user, SendBirdException e) {
                    // Callback received; hide the progress bar.
                    if (e != null) {
                        // Error!
                        return;
                    } else {
                        createGroupChat(context, UserInfo.getCurrentUserId());
                    }
                }
            });
        } else {
            createGroupChat(context, UserInfo.getCurrentUserId());
        }


    }

    static void createGroupChat(Context context, String userId) {
        ServiceChat.startActionCreateChatAccount(context, userId);

        ArrayList<String> mSelectedIds = new ArrayList<>();
        mSelectedIds.add(userId);
        mSelectedIds.add(UserInfo.getCurrentUserId());

        GroupChannel.createChannelWithUserIds(mSelectedIds, true, "", "", UserInfo.getCurrentUserId(), UserInfo.getCurrentUserId(), new GroupChannel.GroupChannelCreateHandler() {
            @Override
            public void onResult(GroupChannel groupChannel, SendBirdException e) {
                if (e != null) {
                    // Error!
                    return;
                }
                GroupChannelActivity.openChannel(context, groupChannel.getUrl());
            }
        });
    }

    public static void openChannel(Context activity, String channelUrl) {
        Intent intent = new Intent(activity, GroupChannelActivity.class);
        intent.putExtra("groupChannelUrl", channelUrl);
        activity.startActivity(intent);
    }

    public interface onBackPressedListener {
        boolean onBack();
    }

    private onBackPressedListener mOnBackPressedListener;

    public void setOnBackPressedListener(onBackPressedListener listener) {
        mOnBackPressedListener = listener;
    }

    @Override
    public void onBackPressed() {
        if (mOnBackPressedListener != null && mOnBackPressedListener.onBack()) {
            return;
        }
        super.onBackPressed();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home) {
            onBackPressed();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void setActionBarTitle(String title) {
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(title);
        }
    }
}
