package com.team500ae.vncapplication.services;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.Nullable;

public class ServiceFile extends IntentService {
    public ServiceFile() {

        super("ServiceFile");

    }


    public static void startActionCreateChatAccount(Context context, String username, String fullname, String avatar) {
        Intent intent = new Intent(context, ServiceChat.class);
//        intent.setAction(ACTION_CREATE_USER);
        intent.putExtra("USER_NAME", username);
        intent.putExtra("FULL_NAME", fullname);
        intent.putExtra("AVATAR", avatar);
        context.startService(intent);
    }
    @Override
    protected void onHandleIntent(@Nullable Intent intent) {

    }
}
