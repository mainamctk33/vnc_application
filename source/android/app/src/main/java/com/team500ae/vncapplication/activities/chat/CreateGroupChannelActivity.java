package com.team500ae.vncapplication.activities.chat;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.sendbird.android.GroupChannel;
import com.sendbird.android.SendBirdException;
import com.sendbird.android.UserListQuery;
import com.team500ae.library.utils.AndroidPermissionUtils;
import com.team500ae.library.utils.ConvertUtils;
import com.team500ae.vncapplication.R;
import com.team500ae.vncapplication.activities.chat.adapter.SelectableUserListAdapter;
import com.team500ae.vncapplication.base.BaseActivity;
import com.team500ae.vncapplication.constants.Constants;
import com.team500ae.vncapplication.data_access.upload.BlobUploadProvider;
import com.team500ae.vncapplication.data_access.upload.UploadInfo;
import com.team500ae.vncapplication.data_access.user.UserInfo;
import com.team500ae.vncapplication.models.ServiceResponseEntity;
import com.team500ae.vncapplication.others.DialogListener;
import com.team500ae.vncapplication.utils.FileUtils;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

public class CreateGroupChannelActivity extends BaseActivity {
    public static final String EXTRA_NEW_CHANNEL_URL = "EXTRA_NEW_CHANNEL_URL";
    private static final int REQ_SELECT_FILE = 10001;

    private List<String> mSelectedIds;
    @BindView(R.id.recycler_select_user)
    RecyclerView mRecyclerView;
    private LinearLayoutManager mLayoutManager;
    private SelectableUserListAdapter mListAdapter;
    private UserListQuery mUserListQuery;
    private CreateGroupDiaglog dialogGroupInfo;
    private Uri uri;
    ProgressDialog dialog;
    int page = 1;
    int size = 30;
    private ArrayList<JsonObject> listData;
    private boolean finished = false;
    private Uri tempUri;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        setContentView(R.layout.activity_create_group_channel);

        super.onCreate(savedInstanceState);
        listData = new ArrayList<>();
        dialog = ProgressDialog.show(getActivity(), "",
                "Loading. Please wait...", true);
        showBackButton();


        mSelectedIds = new ArrayList<>();


        mListAdapter = new SelectableUserListAdapter(getActivity(), false, true);
        mListAdapter.setItemCheckedChangeListener(new SelectableUserListAdapter.OnItemCheckedChangeListener() {
            @Override
            public void OnItemChecked(JsonObject user, boolean checked) {
                if (checked) {
                    onUserSelected(true, ConvertUtils.toString(user.get("UserName")));
                } else {
                    onUserSelected(false, ConvertUtils.toString(user.get("UserName")));
                }
            }
        });
        setUpRecyclerView();
        loadData(page, size);
    }

    private void loadData(int page, int size) {
        this.page = page;
        new AsyncTask<Void, Void, JsonArray>() {
            @Override
            protected void onPreExecute() {
                try {
                    dialog.show();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                super.onPreExecute();
            }

            @Override
            protected JsonArray doInBackground(Void... voids) {
                JsonArray jsonArray = new UserInfo().searchFriend("", page, size);
                if (jsonArray.size() < size)
                    finished = true;
                if (page == 1) {
                    listData.clear();
                }
                for (int i = 0; i < jsonArray.size(); i++) {
                    JsonObject jsonObject = ConvertUtils.toJsonObject(jsonArray.get(i));
                    listData.add(jsonObject);
                }

                return null;
            }

            @Override
            protected void onPostExecute(JsonArray aVoid) {
                try {
                    dialog.hide();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                mListAdapter.setUserList(listData);
                super.onPostExecute(aVoid);
            }
        }.execute();
    }


    private void createGroup(String url) {
        GroupChannel.createChannelWithUserIds(mSelectedIds, true, dialogGroupInfo.getEditText(), url, UserInfo.getCurrentUserId(), UserInfo.getCurrentUserId(), new GroupChannel.GroupChannelCreateHandler() {
            @Override
            public void onResult(GroupChannel groupChannel, SendBirdException e) {
                if (e != null) {
                    // Error!
                    return;
                }
                GroupChannelActivity.openChannel(getActivity(), groupChannel.getUrl());
                finish();
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case REQ_SELECT_FILE:
                if (RESULT_OK == resultCode) {
                    Bitmap bitmap = FileUtils.getImageFromResult(getActivity(), resultCode, data, tempUri);
                    dialogGroupInfo.setBitmap(bitmap);
                    File output = FileUtils.saveBitmap(getActivity(),bitmap);
                    uri = Uri.fromFile(output);

                }
                break;
            default:
                super.onActivityResult(requestCode, resultCode, data);
                break;
        }
    }

    private void setUpRecyclerView() {
        mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setAdapter(mListAdapter);
        mRecyclerView.addItemDecoration(new DividerItemDecoration(getActivity(), DividerItemDecoration.VERTICAL));
        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                if (!finished)
                    if (mLayoutManager.findLastVisibleItemPosition() == mListAdapter.getItemCount() - 1) {
                        loadData(page + 1, size);
                    }
            }
        });
    }


    public void onUserSelected(boolean selected, String userId) {
        if (selected) {
            mSelectedIds.add(userId);
        } else {
            mSelectedIds.remove(userId);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_create_group_chat, menu);
        return super.onCreateOptionsMenu(menu);
    }

    boolean isSelectedImage = false;

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_create:
                if (mSelectedIds.size() > 0) {
                    if (mSelectedIds.size() == 1) {
                        GroupChannelActivity.open(getActivity(), mSelectedIds.get(0));
                        this.finish();
                    } else {
                        isSelectedImage = false;
                        requestPermission(new AndroidPermissionUtils.OnCallbackRequestPermission() {
                            @Override
                            public void onSuccess() {
                                dialogGroupInfo = new CreateGroupDiaglog(getActivity());
                                dialogGroupInfo.setOnItemClickListener(new DialogListener() {
                                    @Override
                                    public void onConfirmClicked() {
                                        String groupName = dialogGroupInfo.getEditText();
                                        if (isSelectedImage) {
                                            new UploadInfo(new BlobUploadProvider() {
                                            }, new UploadInfo.UploadListener() {
                                                ProgressDialog progDialog = new ProgressDialog(getActivity());
                                                ProgressDialog dialogUpload;

                                                @Override
                                                public void beginUpload() {
                                                    progDialog.setMessage(getString(R.string.loading));
                                                    progDialog.setIndeterminate(false);
                                                    progDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                                                    progDialog.setCancelable(true);
                                                    progDialog.show();

                                                }

                                                @Override
                                                public void endUpload() {
                                                    if (progDialog.isShowing()) {
                                                        progDialog.dismiss();
                                                    }

                                                }

                                                @Override
                                                public void onSuccess(JsonArray response) {
                                                    if (response == null || response.size() == 0) {
                                                        createGroup("");
                                                    } else {
                                                        createGroup(ConvertUtils.toString(response.get(0)));
                                                    }

                                                }

                                                @Override
                                                public void onError(ServiceResponseEntity<JsonArray> response) {
                                                    createGroup("");
                                                }
                                            }).upload(getActivity(), Constants.UPLOAD_FILE, ConvertUtils.toArrayList(Uri.class, uri));
                                        } else {
                                            createGroup(dialogGroupInfo.getEditText());
                                        }
                                    }

                                    @Override
                                    public void onButtonChooseImage() {
                                        isSelectedImage = true;
                                        tempUri = FileUtils.createTempFile(getActivity());
                                        Intent chooseImageIntent = new FileUtils().selectOrTakePicture(getActivity(), tempUri);
                                        startActivityForResult(chooseImageIntent, REQ_SELECT_FILE);

                                    }
                                });
                                dialogGroupInfo.show();
                            }

                            @Override
                            public void onFailed() {

                            }
                        }, AndroidPermissionUtils.TypePermission.PERMISSION_READ_EXTERNAL_STORAGE, AndroidPermissionUtils.TypePermission.PERMISSION_WRIRE_EXTERNAL_STORAGE);
                    }
                } else {
                    showSnackBar(R.string.select_member_to_create_a_group);
                }
                break;
        }
        return super.onOptionsItemSelected(item);
    }

}
