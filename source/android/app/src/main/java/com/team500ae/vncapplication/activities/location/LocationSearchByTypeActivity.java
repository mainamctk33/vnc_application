package com.team500ae.vncapplication.activities.location;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.team500ae.vncapplication.R;
import com.team500ae.vncapplication.activities.location.fragments.LocationSearchByTypeFragment;
import com.team500ae.vncapplication.base.BaseActivity;

public class LocationSearchByTypeActivity extends BaseActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_location_search_by_type);
        String name = getIntent().getStringExtra("NAME");
        getSupportActionBar().setTitle(name);
        int type = getIntent().getIntExtra("TYPE",0);
        getSupportFragmentManager().beginTransaction().replace(R.id.linearLayout, LocationSearchByTypeFragment.getInstants(type,name), "location_search").commit();
        showBackButton();
    }

    public static void open(Context context, int type, String name) {
        Intent intent = new Intent(context, LocationSearchByTypeActivity.class);
        intent.putExtra("TYPE", type);
        intent.putExtra("NAME", name);
        context.startActivity(intent);
    }
}
