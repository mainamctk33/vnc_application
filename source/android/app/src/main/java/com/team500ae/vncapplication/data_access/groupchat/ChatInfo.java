package com.team500ae.vncapplication.data_access.groupchat;

import android.content.Context;

import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.team500ae.library.utils.ConvertUtils;
import com.team500ae.vncapplication.constants.Constants;
import com.team500ae.vncapplication.data_access.ClientInfo;
import com.team500ae.vncapplication.models.ServiceResponseEntity;

import java.net.UnknownHostException;

public class ChatInfo {
    public ServiceResponseEntity<Boolean> createChatAccount(Context context, String username, String fullname, String avatar) {
        try {
            JsonObject jsonObject = new JsonObject();
            jsonObject.addProperty("username", username);
            jsonObject.addProperty("fullname", fullname);
            jsonObject.addProperty("avatar", avatar);

            ClientInfo.DataResponse dataResponse = new ClientInfo().requestApi(ClientInfo.MethodType.POST, Constants.API_CHAT_CREATE_ACCOUNT, jsonObject);
            if (dataResponse.isOK()) {
                ServiceResponseEntity<Boolean> result = dataResponse.getData(new TypeToken<ServiceResponseEntity<Boolean>>() {
                }.getType());
                return result;
            }
        } catch (UnknownHostException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public ServiceResponseEntity<Boolean> leaveGroup(Context context, String username, String fullname, String channelUrl) {
        try {
            JsonObject jsonObject = new JsonObject();
            jsonObject.addProperty("username", username);
            jsonObject.addProperty("fullname", fullname);
            jsonObject.addProperty("channelUrl", channelUrl);

            ClientInfo.DataResponse dataResponse = new ClientInfo().requestApi(ClientInfo.MethodType.POST, Constants.API_GROUP_CHAT_LEAVE, jsonObject);
            if (dataResponse.isOK()) {
                ServiceResponseEntity<Boolean> result = dataResponse.getData(new TypeToken<ServiceResponseEntity<Boolean>>() {
                }.getType());
                return result;
            }
        } catch (UnknownHostException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;

    }

    public ServiceResponseEntity<Boolean> inviteMember(Context context, String username, String fullname, String channelUrl, String members) {
        try {
            JsonObject jsonObject = new JsonObject();
            jsonObject.addProperty("username", username);
            jsonObject.addProperty("fullname", fullname);
            jsonObject.addProperty("channelUrl", channelUrl);
            jsonObject.add("members", ConvertUtils.toJsonArray(members));

            ClientInfo.DataResponse dataResponse = new ClientInfo().requestApi(ClientInfo.MethodType.POST, Constants.API_GROUP_CHAT_INVITE_MEMBER, jsonObject);
            if (dataResponse.isOK()) {
                ServiceResponseEntity<Boolean> result = dataResponse.getData(new TypeToken<ServiceResponseEntity<Boolean>>() {
                }.getType());
                return result;
            }
        } catch (UnknownHostException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public ServiceResponseEntity<Boolean>  updateGroup(Context applicationContext, String username, String fullname, String channelUrl) {
        try {
            JsonObject jsonObject = new JsonObject();
            jsonObject.addProperty("username", username);
            jsonObject.addProperty("fullname", fullname);
            jsonObject.addProperty("channelUrl", channelUrl);

            ClientInfo.DataResponse dataResponse = new ClientInfo().requestApi(ClientInfo.MethodType.POST, Constants.API_GROUP_CHAT_UPDATE, jsonObject);
            if (dataResponse.isOK()) {
                ServiceResponseEntity<Boolean> result = dataResponse.getData(new TypeToken<ServiceResponseEntity<Boolean>>() {
                }.getType());
                return result;
            }
        } catch (UnknownHostException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
