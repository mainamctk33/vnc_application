package com.team500ae.vncapplication.services;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;

import com.google.gson.JsonArray;
import com.team500ae.library.utils.SharedPreferencesUtil;
import com.team500ae.vncapplication.constants.Constants;
import com.team500ae.vncapplication.data_access.BookmarkInfo;
import com.team500ae.vncapplication.data_access.RealmInfo;
import com.team500ae.vncapplication.data_access.tips.TipsInfo;
import com.team500ae.vncapplication.data_access.tips.TypeTipsInfo;
import com.team500ae.vncapplication.data_access.user.UserInfo;
import com.team500ae.vncapplication.models.BaseReturnFunctionEntity;
import com.team500ae.vncapplication.models.ServiceResponseEntity;
import com.team500ae.vncapplication.models.realm_models.tips.TypeTipsEntity;

import java.util.ArrayList;
import java.util.Date;

import io.realm.Realm;
import io.realm.exceptions.RealmError;

/**
 * Created by phuong.ndp on 26/11/2017.
 * done
 */

public class ServiceTips extends IntentService {
    // TODO: Rename actions, choose action names that describe tasks that this
    // IntentService can perform, e.g. ACTION_FETCH_TIPS_ITEMS
    private static final String ACTION_SYNC_TYPE_TIPS = "com.team500ae.vncapplication.services.action.ACTION_SYNC_TYPE_TIPS";
    private static final String ACTION_TIPS_BOOKMARK = "com.team500ae.vncapplication.services.action.ACTION_TIPS_BOOKMARK";
    private static final String ACTION_SET_DEFAULT_CATEGORY = "com.team500ae.vncapplication.services.action.ACTION_SET_DEFAULT_CATEGORY";

    public ServiceTips() {
        super("ServiceTips");
    }

    /**
     * Starts this service to perform action Foo with the given parameters. If
     * the service is already performing a task this action will be queued.
     *
     * @see IntentService
     */
    // TODO: Customize helper method
    public static void startActionSyncTypeTips(Context context) {
        Intent intent = new Intent(context, ServiceTips.class);
        intent.setAction(ACTION_SYNC_TYPE_TIPS);
        context.startService(intent);
    }

    public static void startActionBookmark(Context context, int id, boolean bookmark) {
        Intent intent = new Intent(context, ServiceTips.class);
        intent.putExtra("ID", id);
        intent.putExtra("BOOKMARK", bookmark);
        intent.setAction(ACTION_TIPS_BOOKMARK);
        context.startService(intent);
    }

    public static void startSetDefaultCategory(Context context) {
        Intent intent = new Intent(context, ServiceTips.class);
        intent.setAction(ACTION_SET_DEFAULT_CATEGORY);
        context.startService(intent);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        if (intent != null) {
            final String action = intent.getAction();
            switch (action) {
                case ACTION_SYNC_TYPE_TIPS:
                    handleActionSyncTypeTips();
                    break;
                case ACTION_TIPS_BOOKMARK:
                    boolean bookmark = intent.getBooleanExtra("BOOKMARK", false);
                    int id = intent.getIntExtra("ID", 0);
                    handleActionBookmark(id, bookmark);
                    break;
                case ACTION_SET_DEFAULT_CATEGORY:
                    handleSetDefaultCategory();
                    break;
            }
        }
    }

    /**
     * Handle action Foo in the provided background thread with the provided
     * parameters.
     */
    private void handleActionSyncTypeTips() {
        // TODO: Handle action Foo
        new Thread(new Runnable() {
            @Override
            public void run() {
                Realm realm = null;
                try {
                    realm = RealmInfo.getRealm(getApplicationContext());
                    TypeTipsInfo typeTipsInfo = new TypeTipsInfo();
                    long lastSyncTypeTipsDate = 0;//SharedPreferencesUtil.getLong(getApplicationContext(), Constants.LAST_SYNC_TYPETIPS, 0L);
                    ServiceResponseEntity<JsonArray> result = typeTipsInfo.syncTypeTips(getApplicationContext(), lastSyncTypeTipsDate);
                    if (result != null && result.getStatus() == 0) {
                        if (typeTipsInfo.updateTypeTips(realm, result.getData()).getData()) {
                            SharedPreferencesUtil.setSharedPreferences(getApplicationContext(), SharedPreferencesUtil.EnumType.Long, Constants.LAST_SYNC_TYPETIPS, new Date().getTime());
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                } catch (RealmError e) {
                    e.printStackTrace();
                } catch (OutOfMemoryError e) {
                    e.printStackTrace();
                } finally {
                    RealmInfo.closeRealm(realm);
                }
            }
        }).start();
    }

    private void handleSetDefaultCategory() {
        // TODO: Handle action Foo
        new Thread(new Runnable() {
            @Override
            public void run() {
                String userId = UserInfo.getCurrentUserId();
                Realm realm = null;
                try {
                    realm = RealmInfo.getRealm(getApplicationContext());
                    TypeTipsInfo typeTipsInfo = new TypeTipsInfo();
                    BaseReturnFunctionEntity<ArrayList<TypeTipsEntity>> currentMyType = typeTipsInfo.getTypeTipsHasSelected(getApplicationContext(), realm, userId);
                    if (currentMyType.isTrue() && currentMyType.getData().size() == 0) {
                        BaseReturnFunctionEntity<ArrayList<TypeTipsEntity>> listType = typeTipsInfo.getListTypeTips(getApplicationContext(), realm);
                        if (listType.isTrue()) {
                            ArrayList<TypeTipsEntity> typeTips = listType.getData();
                            for (int i = 0; i < 5 && i < typeTips.size(); i++) {
                                typeTipsInfo.saveTypeTipsToFavorite(getApplicationContext(), realm, userId, typeTips.get(i).getId());
                            }
                            Intent intent = new Intent(Constants.ACTION_TYPE_TIPS_FOR_USER_CHANGED);
                            getApplicationContext().sendBroadcast(intent);
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                } catch (RealmError e) {
                    e.printStackTrace();
                } catch (OutOfMemoryError e) {
                    e.printStackTrace();
                } finally {
                    RealmInfo.closeRealm(realm);
                }
            }
        }).start();
    }

    private void handleActionBookmark(int id, boolean bookmark) {
        // TODO: Handle action Foo
        new Thread(new Runnable() {
            @Override
            public void run() {
                Realm realm = null;
                try {
                    realm = RealmInfo.getRealm(getApplicationContext());
                    if (UserInfo.isLogin()) {
                        TipsInfo tipsInfo = new TipsInfo();
                        realm = RealmInfo.getRealm(getApplicationContext());
                        new BookmarkInfo().bookmark(getApplicationContext(), realm, id, UserInfo.getCurrentUserId(), BookmarkInfo.BookmarkType.Tip, bookmark);
                        tipsInfo.bookmark(getApplicationContext(), id, bookmark);
                        Intent intent=new Intent(Constants.ACTION_TIPS_BOOKMARK_SUCCESS);
                        getApplication().sendBroadcast(intent);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                } catch (RealmError e) {
                    e.printStackTrace();
                } catch (OutOfMemoryError e) {
                    e.printStackTrace();
                } finally {
                    RealmInfo.closeRealm(realm);
                }
            }
        }).start();
    }
}
