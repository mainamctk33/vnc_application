//package com.team500ae.vncapplication.activities.job;
//
//import android.app.ProgressDialog;
//import android.content.DialogInterface;
//import android.content.Intent;
//import android.graphics.Bitmap;
//import android.net.Uri;
//import android.os.AsyncTask;
//import android.os.Bundle;
//import android.support.annotation.Nullable;
//import android.support.v4.app.FragmentActivity;
//import android.support.v7.app.AlertDialog;
//import android.text.Editable;
//import android.text.InputFilter;
//import android.text.InputType;
//import android.text.TextWatcher;
//import android.view.Gravity;
//import android.view.View;
//import android.widget.Button;
//import android.widget.EditText;
//import android.widget.ImageView;
//import android.widget.TextView;
//
//import com.google.gson.JsonArray;
//import com.team500ae.library.utils.AndroidPermissionUtils;
//import com.team500ae.library.utils.ConvertUtils;
//import com.team500ae.library.utils.ImageUtils;
//import com.team500ae.vncapplication.R;
//import com.team500ae.vncapplication.base.BaseActivity;
//import com.team500ae.vncapplication.constants.Constants;
//import com.team500ae.vncapplication.data_access.RealmInfo;
//import com.team500ae.vncapplication.data_access.job.CityJobInfo;
//import com.team500ae.vncapplication.data_access.job.CompanyJobInfo;
//import com.team500ae.vncapplication.data_access.job.CountryJobInfo;
//import com.team500ae.vncapplication.data_access.job.JobInfo;
//import com.team500ae.vncapplication.data_access.job.LevelJobInfo;
//import com.team500ae.vncapplication.data_access.job.TypeJobInfo;
//import com.team500ae.vncapplication.data_access.upload.BlobUploadProvider;
//import com.team500ae.vncapplication.data_access.upload.UploadInfo;
//import com.team500ae.vncapplication.data_access.user.UserInfo;
//import com.team500ae.vncapplication.models.BaseReturnFunctionEntity;
//import com.team500ae.vncapplication.models.ServiceResponseEntity;
//import com.team500ae.vncapplication.models.realm_models.job.CityJobEntity;
//import com.team500ae.vncapplication.models.realm_models.job.CompanyJobEntity;
//import com.team500ae.vncapplication.models.realm_models.job.CountryJobEntity;
//import com.team500ae.vncapplication.models.realm_models.job.LevelJobEntity;
//import com.team500ae.vncapplication.models.realm_models.job.TypeJobEntity;
//import com.team500ae.vncapplication.others.DialogListener;
//import com.team500ae.vncapplication.others.UploadImageDialog;
//import com.team500ae.vncapplication.utils.FileUtils;
//import com.team500ae.vncapplication.utils.StringUtils;
//
//import java.util.ArrayList;
//
//import butterknife.BindView;
//import butterknife.ButterKnife;
//import butterknife.OnClick;
//import de.hdodenhof.circleimageview.CircleImageView;
//import io.realm.Realm;
//
///**
// * Created by Phuong D. Nguyen on 5/8/2018.
// */
//
//public class AddJobActivity extends BaseActivity {
//    private EditText edtJobTitleContent, edtSalaryContent, edtJobDescriptionContent, edtJobRequirementContent, edtPhoneNumberContent, edtEmailContent;
//    private TextView tvJobCategoryContent, tvJobPositionContent, tvCompanyContent, tvNationContent, tvCityContent;
//    private ImageView ivJobCategoryRightIcon, ivJobPositionRightIcon, ivCompanyRightIcon, ivNationRightIcon, ivCityRightIcon;
//    private CircleImageView civUserAvatar;
//    @BindView(R.id.btn_add_job)
//    Button btnAdd;
//
//    private Realm realm;
//    private Bitmap bitmap;
//    private Uri selectedImage = Uri.EMPTY;
//    private String thumbnailUrl = "";
//    private int isUploadFile = 0;
//    private Uri tempUri;
//
//    ServiceResponseEntity<JsonArray> countryList;
//    ArrayList<Integer> listNationId;
//    ArrayList<String> listNationName;
//
//    ServiceResponseEntity<JsonArray> cityList;
//    ArrayList<Integer> listCityId;
//    ArrayList<String> listCityName;
//
//    ServiceResponseEntity<JsonArray> typeList;
//    ArrayList<Integer> listJobCategoryId;
//    ArrayList<String> listJobCategoryName;
//
//    ServiceResponseEntity<JsonArray> levelList;
//    ArrayList<Integer> listJobPositionId;
//    ArrayList<String> listJobPositionName;
//
//    ServiceResponseEntity<JsonArray> companyList;
//    ArrayList<Integer> listCompanyId;
//    ArrayList<String> listCompanyName;
//
//    public static void open(FragmentActivity activity) {
//        Intent intent = new Intent(activity, AddJobActivity.class);
//        activity.startActivity(intent);
//    }
//
//    @Override
//    protected void onCreate(@Nullable Bundle savedInstanceState) {
//        setContentView(R.layout.activity_add_job);
//        ButterKnife.bind(this);
//        super.onCreate(savedInstanceState);
//        realm = RealmInfo.getRealm(getApplicationContext());
//        showBackButton();
//        getSupportActionBar().setTitle(R.string.add_job);
//
//        initLayout();
//        loadDataSpinner();
//
//        tvJobCategoryContent.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                showJobCategoryDialog();
//            }
//        });
//        ivJobCategoryRightIcon.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                showJobCategoryDialog();
//            }
//        });
//
//        tvJobPositionContent.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                showJobPositionDialog();
//            }
//        });
//        ivJobPositionRightIcon.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                showJobPositionDialog();
//            }
//        });
//
//        tvCompanyContent.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                showCompanyDialog();
//            }
//        });
//        ivCompanyRightIcon.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                showCompanyDialog();
//            }
//        });
//
//        tvNationContent.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                showNationDialog();
//            }
//        });
//        ivNationRightIcon.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                showNationDialog();
//            }
//        });
//
//        tvCityContent.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                showCityDialog();
//            }
//        });
//        ivCityRightIcon.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                showCityDialog();
//            }
//        });
//    }
//
//    private void initLayout() {
//        View vJobTitleInclude = findViewById(R.id.layout_job_title);
//        ImageView ivJobTitleLeftIcon = (ImageView) vJobTitleInclude.findViewById(R.id.image_left);
//        ivJobTitleLeftIcon.setImageResource(R.drawable.ic_job_description);
//        TextView tvJobTitleTitle = (TextView) vJobTitleInclude.findViewById(R.id.text_title);
//        tvJobTitleTitle.setText(getResources().getString(R.string.job_title));
//        tvJobTitleTitle.setTextColor(getResources().getColor(R.color.colorPrimary));
//        edtJobTitleContent = (EditText) vJobTitleInclude.findViewById(R.id.text_content);
//        edtJobTitleContent.setFilters(new InputFilter[]{new InputFilter.LengthFilter(Constants.MAX_FULL_NAME_LENGTH)});
//        ImageView ivJobTitleRightIcon = (ImageView) vJobTitleInclude.findViewById(R.id.image_right);
//        ivJobTitleRightIcon.setVisibility(View.GONE);
//
//        View vJobCategoryInclude = findViewById(R.id.layout_job_category);
//        ImageView ivJobCategoryLeftIcon = (ImageView) vJobCategoryInclude.findViewById(R.id.image_left);
//        ivJobCategoryLeftIcon.setImageResource(R.drawable.ic_job_type);
//        TextView tvJobCategoryTitle = (TextView) vJobCategoryInclude.findViewById(R.id.text_title);
//        tvJobCategoryTitle.setText(getResources().getString(R.string.job_category));
//        tvJobCategoryContent = (TextView) vJobCategoryInclude.findViewById(R.id.text_content);
//        ivJobCategoryRightIcon = (ImageView) vJobCategoryInclude.findViewById(R.id.image_right);
//        ivJobCategoryRightIcon.setImageResource(R.drawable.ic_next);
//
//        View vJobPositionInclude = findViewById(R.id.layout_job_position);
//        ImageView ivJobPositionLeftIcon = (ImageView) vJobPositionInclude.findViewById(R.id.image_left);
//        ivJobPositionLeftIcon.setImageResource(R.drawable.ic_job_level);
//        TextView tvJobPositionTitle = (TextView) vJobPositionInclude.findViewById(R.id.text_title);
//        tvJobPositionTitle.setText(getResources().getString(R.string.job_position));
//        tvJobPositionContent = (TextView) vJobPositionInclude.findViewById(R.id.text_content);
//        ivJobPositionRightIcon = (ImageView) vJobPositionInclude.findViewById(R.id.image_right);
//        ivJobPositionRightIcon.setImageResource(R.drawable.ic_next);
//
//        View vSalaryInclude = findViewById(R.id.layout_salary);
//        ImageView ivSalaryLeftIcon = (ImageView) vSalaryInclude.findViewById(R.id.image_left);
//        ivSalaryLeftIcon.setImageResource(R.drawable.ic_job_salary);
//        TextView tvSalaryTitle = (TextView) vSalaryInclude.findViewById(R.id.text_title);
//        tvSalaryTitle.setText(getResources().getString(R.string.salary));
//        edtSalaryContent = (EditText) vSalaryInclude.findViewById(R.id.text_content);
//        edtSalaryContent.setFilters(new InputFilter[]{new InputFilter.LengthFilter(Constants.MAX_PHONE_NUMBER_LENGTH)});
//        edtSalaryContent.setInputType(InputType.TYPE_CLASS_NUMBER);
//        edtSalaryContent.setFilters(new InputFilter[]{new InputFilter.LengthFilter(Constants.MAX_PHONE_NUMBER_LENGTH)});
//        ImageView ivSalaryRightIcon = (ImageView) vSalaryInclude.findViewById(R.id.image_right);
//        ivSalaryRightIcon.setVisibility(View.GONE);
//
//        View vCompanyInclude = findViewById(R.id.layout_company);
//        ImageView ivCompanyLeftIcon = (ImageView) vCompanyInclude.findViewById(R.id.image_left);
//        ivCompanyLeftIcon.setImageResource(R.drawable.ic_job_company);
//        TextView tvCompanyTitle = (TextView) vCompanyInclude.findViewById(R.id.text_title);
//        tvCompanyTitle.setText(getResources().getString(R.string.company));
//        tvCompanyContent = (TextView) vCompanyInclude.findViewById(R.id.text_content);
//        ivCompanyRightIcon = (ImageView) vCompanyInclude.findViewById(R.id.image_right);
//        ivCompanyRightIcon.setImageResource(R.drawable.ic_next);
//
//        View vNationInclude = findViewById(R.id.layout_nation);
//        ImageView ivNationLeftIcon = (ImageView) vNationInclude.findViewById(R.id.image_left);
//        ivNationLeftIcon.setImageResource(R.drawable.ic_job_country);
//        TextView tvNationTitle = (TextView) vNationInclude.findViewById(R.id.text_title);
//        tvNationTitle.setText(getResources().getString(R.string.nation));
//        tvNationContent = (TextView) vNationInclude.findViewById(R.id.text_content);
//        ivNationRightIcon = (ImageView) vNationInclude.findViewById(R.id.image_right);
//        ivNationRightIcon.setImageResource(R.drawable.ic_next);
//
//        View vCityInclude = findViewById(R.id.layout_city);
//        ImageView ivCityLeftIcon = (ImageView) vCityInclude.findViewById(R.id.image_left);
//        ivCityLeftIcon.setImageResource(R.drawable.ic_job_address);
//        TextView tvCityTitle = (TextView) vCityInclude.findViewById(R.id.text_title);
//        tvCityTitle.setText(getResources().getString(R.string.city));
//        tvCityContent = (TextView) vCityInclude.findViewById(R.id.text_content);
//        ivCityRightIcon = (ImageView) vCityInclude.findViewById(R.id.image_right);
//        ivCityRightIcon.setImageResource(R.drawable.ic_next);
//
//        View vJobDescriptionInclude = findViewById(R.id.layout_job_description);
//        ImageView ivJobDescriptionLeftIcon = (ImageView) vJobDescriptionInclude.findViewById(R.id.image_left);
//        ivJobDescriptionLeftIcon.setImageResource(R.drawable.ic_job_description);
//        TextView tvJobDescriptionTitle = (TextView) vJobDescriptionInclude.findViewById(R.id.text_title);
//        tvJobDescriptionTitle.setVisibility(View.GONE);
//        edtJobDescriptionContent = (EditText) vJobDescriptionInclude.findViewById(R.id.text_content);
//        edtJobDescriptionContent.setGravity(Gravity.LEFT);
//        ImageView ivJobDescriptionRightIcon = (ImageView) vJobDescriptionInclude.findViewById(R.id.image_right);
//        ivJobDescriptionRightIcon.setVisibility(View.GONE);
//
//        View vJobRequirementInclude = findViewById(R.id.layout_job_requirement);
//        ImageView ivJobRequirementLeftIcon = (ImageView) vJobRequirementInclude.findViewById(R.id.image_left);
//        ivJobRequirementLeftIcon.setImageResource(R.drawable.ic_job_requirement);
//        TextView tvJobRequirementTitle = (TextView) vJobRequirementInclude.findViewById(R.id.text_title);
//        tvJobRequirementTitle.setVisibility(View.GONE);
//        edtJobRequirementContent = (EditText) vJobRequirementInclude.findViewById(R.id.text_content);
//        edtJobRequirementContent.setGravity(Gravity.LEFT);
//        ImageView ivJobRequirementRightIcon = (ImageView) vJobRequirementInclude.findViewById(R.id.image_right);
//        ivJobRequirementRightIcon.setVisibility(View.GONE);
//
//        View vPhoneNumberInclude = findViewById(R.id.layout_phone_number);
//        ImageView ivPhoneNumberLeftIcon = (ImageView) vPhoneNumberInclude.findViewById(R.id.image_left);
//        ivPhoneNumberLeftIcon.setImageResource(R.drawable.ic_job_phone);
//        TextView tvPhoneNumberTitle = (TextView) vPhoneNumberInclude.findViewById(R.id.text_title);
//        tvPhoneNumberTitle.setText(getResources().getString(R.string.phone_number));
//        edtPhoneNumberContent = (EditText) vPhoneNumberInclude.findViewById(R.id.text_content);
//        edtPhoneNumberContent.setFilters(new InputFilter[]{new InputFilter.LengthFilter(Constants.MAX_PHONE_NUMBER_LENGTH)});
//        edtPhoneNumberContent.setInputType(InputType.TYPE_CLASS_NUMBER);
//        edtPhoneNumberContent.setFilters(new InputFilter[]{new InputFilter.LengthFilter(Constants.MAX_PHONE_NUMBER_LENGTH)});
//        ImageView ivPhoneNumberRightIcon = (ImageView) vPhoneNumberInclude.findViewById(R.id.image_right);
//        ivPhoneNumberRightIcon.setVisibility(View.GONE);
//
//        View vEmailInclude = findViewById(R.id.layout_email);
//        ImageView ivEmailLeftIcon = (ImageView) vEmailInclude.findViewById(R.id.image_left);
//        ivEmailLeftIcon.setImageResource(R.drawable.ic_job_email);
//        TextView tvEmailTitle = (TextView) vEmailInclude.findViewById(R.id.text_title);
//        tvEmailTitle.setText(getResources().getString(R.string.email));
//        tvEmailTitle.setTextColor(getResources().getColor(R.color.colorPrimary));
//        edtEmailContent = (EditText) vEmailInclude.findViewById(R.id.text_content);
//        edtEmailContent.setFilters(new InputFilter[]{new InputFilter.LengthFilter(Constants.MAX_FULL_NAME_LENGTH)});
//        edtEmailContent.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS);
//        edtEmailContent.addTextChangedListener(new TextWatcher() {
//            @Override
//            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
//                if (s.length() >= Constants.MAX_FULL_NAME_LENGTH) {
//                    new AlertDialog.Builder(AddJobActivity.this).setIcon(R.drawable.ic_job_email).setTitle(getResources().getString(R.string.character_limit_exceeded)).setMessage(getResources().getString(R.string.notice_number_limit_character) + " " + Constants.MAX_FULL_NAME_LENGTH).setPositiveButton(android.R.string.ok, null).show();
//                    edtEmailContent.requestFocus();
//                }
//            }
//
//            @Override
//            public void onTextChanged(CharSequence s, int start, int before, int count) {
//            }
//
//            @Override
//            public void afterTextChanged(Editable s) {
//            }
//        });
//        ImageView ivEmailRightIcon = (ImageView) vEmailInclude.findViewById(R.id.image_right);
//        ivEmailRightIcon.setVisibility(View.GONE);
//
//        civUserAvatar = (CircleImageView) findViewById(R.id.civ_user_avatar);
//        TextView profileTitle = (TextView) findViewById(R.id.text_title);
//        profileTitle.setText(getResources().getString(R.string.job_information));
//    }
//
//    private void showJobCategoryDialog() {
//        final String[] arrayJobCategory = listJobCategoryName.toArray(new String[0]);
//        String currentJobCategory = tvJobCategoryContent.getText().toString();
//        int itemSelected = StringUtils.getStringIndex(listJobCategoryName, currentJobCategory);
//
//        new AlertDialog.Builder(this).setTitle(getResources().getString(R.string.job_category)).setSingleChoiceItems(arrayJobCategory, itemSelected, new DialogInterface.OnClickListener() {
//            @Override
//            public void onClick(DialogInterface dialog, int selectedIndex) {
//                tvJobCategoryContent.setText(arrayJobCategory[selectedIndex]);
//            }
//        }).setIcon(R.drawable.ic_job_type).setNegativeButton(getResources().getString(R.string.close), null).show();
//    }
//
//    private void showJobPositionDialog() {
//        final String[] arrayJobPosition = listJobPositionName.toArray(new String[0]);
//        String currentJobPosition = tvJobPositionContent.getText().toString();
//        int itemSelected = StringUtils.getStringIndex(listJobPositionName, currentJobPosition);
//
//        new AlertDialog.Builder(this).setTitle(getResources().getString(R.string.job_position)).setSingleChoiceItems(arrayJobPosition, itemSelected, new DialogInterface.OnClickListener() {
//            @Override
//            public void onClick(DialogInterface dialog, int selectedIndex) {
//                tvJobPositionContent.setText(arrayJobPosition[selectedIndex]);
//            }
//        }).setIcon(R.drawable.ic_job_level).setNegativeButton(getResources().getString(R.string.close), null).show();
//    }
//
//    private void showCompanyDialog() {
//        final String[] arrayCompany = listCompanyName.toArray(new String[0]);
//        String currentCompany = tvCompanyContent.getText().toString();
//        int itemSelected = StringUtils.getStringIndex(listCompanyName, currentCompany);
//
//        new AlertDialog.Builder(this).setTitle(getResources().getString(R.string.company)).setSingleChoiceItems(arrayCompany, itemSelected, new DialogInterface.OnClickListener() {
//            @Override
//            public void onClick(DialogInterface dialog, int selectedIndex) {
//                tvCompanyContent.setText(arrayCompany[selectedIndex]);
//            }
//        }).setIcon(R.drawable.ic_job_company).setNegativeButton(getResources().getString(R.string.close), null).show();
//    }
//
//    private void showNationDialog() {
//        final String[] arrayNation = listNationName.toArray(new String[0]);
//        String currentNation = tvNationContent.getText().toString();
//        int itemSelected = StringUtils.getStringIndex(listNationName, currentNation);
//
//        new AlertDialog.Builder(this).setTitle(getResources().getString(R.string.nation)).setSingleChoiceItems(arrayNation, itemSelected, new DialogInterface.OnClickListener() {
//            @Override
//            public void onClick(DialogInterface dialog, int selectedIndex) {
//                tvNationContent.setText(arrayNation[selectedIndex]);
//            }
//        }).setIcon(R.drawable.ic_job_country).setNegativeButton(getResources().getString(R.string.close), null).show();
//    }
//
//    private void showCityDialog() {
//        String nation = tvNationContent.getText().toString();
//        if (nation == null || nation.isEmpty()) {
//            AlertDialog alertDialog = new AlertDialog.Builder(AddJobActivity.this).create();
//            alertDialog.setTitle("Alert");
//            alertDialog.setMessage("Please select nation first");
//            alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
//                    new DialogInterface.OnClickListener() {
//                        public void onClick(DialogInterface dialog, int which) {
//                            dialog.dismiss();
//                        }
//                    });
//            alertDialog.show();
//            return;
//        }
//
//        int nationId = StringUtils.getIdFromValue(listNationName, listNationId, nation);
//        BaseReturnFunctionEntity<ArrayList<CityJobEntity>> myList = new CityJobInfo().getList(getApplicationContext(), realm);
//        listCityId = new ArrayList<>();
//        listCityName = new ArrayList<>();
//        if (myList.isTrue()) {
//            for (CityJobEntity type : myList.getData()) {
//                if (type.getCountry() == nationId) {
//                    listCityId.add(type.getId());
//                    listCityName.add(type.getName());
//                }
//            }
//        }
//
//        final String[] arrayCity = listCityName.toArray(new String[0]);
//        String currentCity = tvCityContent.getText().toString();
//        int itemSelected = StringUtils.getStringIndex(listCityName, currentCity);
//
//        new AlertDialog.Builder(this).setTitle(getResources().getString(R.string.city)).setSingleChoiceItems(arrayCity, itemSelected, new DialogInterface.OnClickListener() {
//            @Override
//            public void onClick(DialogInterface dialog, int selectedIndex) {
//                tvCityContent.setText(arrayCity[selectedIndex]);
//            }
//        }).setIcon(R.drawable.ic_job_address).setNegativeButton(getResources().getString(R.string.close), null).show();
//    }
//
//    public void showBackButton() {
//        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
//        getSupportActionBar().setDisplayShowHomeEnabled(true);
//    }
//
//
//    public void loadDataSpinner() {
//        BaseReturnFunctionEntity<ArrayList<CompanyJobEntity>> myListCompany = new CompanyJobInfo().getList(getApplicationContext(), realm);
//        listCompanyId = new ArrayList<>();
//        listCompanyName = new ArrayList<>();
//        if (myListCompany.isTrue()) {
//            for (CompanyJobEntity type : myListCompany.getData()) {
//                listCompanyId.add(type.getId());
//                listCompanyName.add(type.getName());
//            }
//        }
//
//        BaseReturnFunctionEntity<ArrayList<LevelJobEntity>> myListLevel = new LevelJobInfo().getList(getApplicationContext(), realm);
//        listJobPositionName = new ArrayList<>();
//        listJobPositionId = new ArrayList<>();
//        if (myListLevel.isTrue()) {
//            //myList2.addAll(myList.getData());
//            for (LevelJobEntity type : myListLevel.getData()) {
//                listJobPositionId.add(type.getId());
//                listJobPositionName.add(type.getName());
//            }
//        }
//
//        BaseReturnFunctionEntity<ArrayList<TypeJobEntity>> myListJobCategory = new TypeJobInfo().getListTypeJob(getApplicationContext(), realm);
//        listJobCategoryId = new ArrayList<>();
//        listJobCategoryName = new ArrayList<>();
//        if (myListJobCategory.isTrue()) {
//            for (TypeJobEntity type : myListJobCategory.getData()) {
//                listJobCategoryId.add(type.getId());
//                listJobCategoryName.add(type.getName());
//            }
//        }
//
//        BaseReturnFunctionEntity<ArrayList<CountryJobEntity>> myListCountry = new CountryJobInfo().getList(getApplicationContext(), realm);
//        listNationName = new ArrayList<>();
//        listNationId = new ArrayList<>();
//        if (myListCountry.isTrue()) {
//            for (CountryJobEntity type : myListCountry.getData()) {
//                listNationId.add(type.getId());
//                listNationName.add(type.getName());
//            }
//        }
//
//    }
//
//    @OnClick(R.id.btn_add_job)
//    void addJob() {
//        edtJobTitleContent.setError(null);
//        edtSalaryContent.setError(null);
//        edtJobDescriptionContent.setError(null);
//        edtJobRequirementContent.setError(null);
//        edtPhoneNumberContent.setError(null);
//        edtEmailContent.setError(null);
//        tvCompanyContent.setError(null);
//        tvJobCategoryContent.setError(null);
//        tvJobPositionContent.setError(null);
//        tvCityContent.setError(null);
//        tvCompanyContent.setError(null);
//
//        String jobTitle = edtJobTitleContent.getText().toString().trim();
//        String salary = edtSalaryContent.getText().toString();
//        String description = edtJobDescriptionContent.getText().toString().trim();
//        String requirement = edtJobRequirementContent.getText().toString().trim();
//        String phone = edtPhoneNumberContent.getText().toString().trim();
//        String email = edtEmailContent.getText().toString().trim();
//        String nation = tvNationContent.getText().toString().trim();
//        int nationId = StringUtils.getIdFromValue(listNationName, listNationId, nation);
//        String city = tvCityContent.getText().toString().trim();
//        int cityId = StringUtils.getIdFromValue(listCityName, listCityId, city);
//        String jobCategory = tvJobCategoryContent.getText().toString().trim();
//        int jobCategoryId = StringUtils.getIdFromValue(listJobCategoryName, listJobCategoryId, jobCategory);
//        String jobPosition = tvJobPositionContent.getText().toString().trim();
//        int jobPositionId = StringUtils.getIdFromValue(listJobPositionName, listJobPositionId, jobPosition);
//        String company = tvCompanyContent.getText().toString().trim();
//        int companyId = StringUtils.getIdFromValue(listCompanyName, listCompanyId, company);
//
//        if (jobTitle.isEmpty() || salary.isEmpty() || description.isEmpty() || requirement.isEmpty() || phone.isEmpty() || email.isEmpty() || nationId < 0 || cityId < 0 || jobCategoryId < 0 || jobPositionId < 0 || companyId < 0) {
//            showSnackBar(R.string.please_input_data);
//            return;
//        }
//
//        if (isUploadFile == 2) {
//            new UploadInfo(new BlobUploadProvider() {
//            }, new UploadInfo.UploadListener() {
//                //ProgressDialog dialogUpload = new ProgressDialog(getApplicationContext());
//                ProgressDialog progDialog = new ProgressDialog(AddJobActivity.this);
//                ProgressDialog dialogUpload;
//
//                @Override
//                public void beginUpload() {
//                    progDialog.setMessage(getString(R.string.loading));
//                    progDialog.setIndeterminate(false);
//                    progDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
//                    progDialog.setCancelable(true);
//                    progDialog.show();
//                }
//
//                @Override
//                public void endUpload() {
//                    if (progDialog.isShowing()) {
//                        progDialog.dismiss();
//                    }
//                }
//
//                @Override
//                public void onSuccess(JsonArray response) {
//                    //thong bao upload thanh cong
//                    thumbnailUrl = response.getAsString();
//                    //Toast.makeText(getApplicationContext(), getString(R.string.Job_upload_image_successful) + thumbnailUrl, Toast.LENGTH_SHORT).show();
//                    //asyncTaskAddJob.execute();
//                    new AsyncTask<Void, Void, BaseReturnFunctionEntity<Boolean>>() {
//                        @Override
//                        protected BaseReturnFunctionEntity<Boolean> doInBackground(Void... voids) {
//                            return new JobInfo().create(getApplicationContext(), jobTitle, salary, description,
//                                    requirement, phone, email, UserInfo.getCurrentUserId(), cityId,
//                                    jobPositionId, jobCategoryId, "", thumbnailUrl);
//                        }
//
//                        @Override
//                        protected void onPostExecute(BaseReturnFunctionEntity<Boolean> result) {
//                            super.onPostExecute(result);
//                            if (result.isTrue()) {
//                                showSnackBar(getResources().getString(R.string.job_add_successful));
//                                Intent intent = new Intent(Constants.ACTION_JOB_ADD_SUCCESS);
//                                getApplicationContext().sendBroadcast(intent);
//                                onBackPressed();
//                            } else {
//                                showSnackBar(getResources().getString(R.string.job_add_failed) + ":" + result.getMessage());
//                            }
//                        }
//                    }.execute();
//                }
//
//                @Override
//                public void onError(ServiceResponseEntity<JsonArray> response) {
//                    //thong bao upload that bai cmnr
//                    thumbnailUrl = "";
//                    showSnackBar(getString(R.string.Job_upload_image_failed));
//                }
//            }).upload(getActivity(), Constants.UPLOAD_FILE, ConvertUtils.toArrayList(Uri.class, selectedImage));
//        } else if (isUploadFile == 1) {
//            // asyncTaskAddJob.execute();
//            new AsyncTask<Void, Void, BaseReturnFunctionEntity<Boolean>>() {
//                @Override
//                protected BaseReturnFunctionEntity<Boolean> doInBackground(Void... voids) {
//                    return new JobInfo().create(getApplicationContext(), jobTitle, salary, description,
//                            requirement, phone, email, UserInfo.getCurrentUserId(), cityId,
//                            jobPositionId, jobCategoryId, "", thumbnailUrl);
//                }
//
//                @Override
//                protected void onPostExecute(BaseReturnFunctionEntity<Boolean> result) {
//                    super.onPostExecute(result);
//                    if (result.isTrue()) {
//                        showSnackBar(getResources().getString(R.string.job_add_successful));
//                        Intent intent = new Intent(Constants.ACTION_JOB_ADD_SUCCESS);
//                        getApplicationContext().sendBroadcast(intent);
//                        onBackPressed();
//                    } else {
//                        showSnackBar(getResources().getString(R.string.job_add_failed) + ":" + result.getMessage());
//                    }
//                }
//            }.execute();
//        }
//    }
//
//    @OnClick(R.id.civ_user_avatar)
//    public void onViewClicked(View view) {
//        switch (view.getId()) {
//            case R.id.civ_user_avatar:
//                requestPermission(new AndroidPermissionUtils.OnCallbackRequestPermission() {
//                    @Override
//                    public void onSuccess() {
//                        UploadImageDialog uploadImageDialog = new UploadImageDialog(getActivity());
//                        uploadImageDialog.setOnItemClickListener(new DialogListener() {
//                            @Override
//                            public void onConfirmClicked() {
//                                thumbnailUrl = uploadImageDialog.getEditText();
//                                //Toast.makeText(JobAddActivity.this, uploadImageDialog.getEditText(), Toast.LENGTH_SHORT).show();
//                                ImageUtils.loadImageByGlide(getApplicationContext(), true, 200, 200, thumbnailUrl, R.drawable.img_no_image, R.drawable.img_no_image, civUserAvatar, true);
//                                isUploadFile = 1;
////                                btnAdd.setEnabled(true);
////                                btnAdd.setImageDrawable(getDrawable(R.drawable.ic_job_save));
//                            }
//
//                            @Override
//                            public void onButtonChooseImage() {
//                                tempUri = FileUtils.createTempFile(getActivity());
//                                Intent chooseImageIntent = new FileUtils().selectOrTakePicture(getActivity(), tempUri);
//                                startActivityForResult(chooseImageIntent, 1);
//                            }
//                        });
//                        uploadImageDialog.show();
//                    }
//
//                    @Override
//                    public void onFailed() {
//
//                    }
//                }, AndroidPermissionUtils.TypePermission.PERMISSION_READ_EXTERNAL_STORAGE, AndroidPermissionUtils.TypePermission.PERMISSION_WRIRE_EXTERNAL_STORAGE);
//                break;
//        }
//    }
//
//    @Override
//    public void onActivityResult(int requestCode, int resultCode, Intent data) {
//        switch (requestCode) {
//            case 1:
//                bitmap = FileUtils.getImageFromResult(getActivity(), resultCode, data, tempUri);
//                if (bitmap != null)
//                    civUserAvatar.setImageBitmap(bitmap);
//                selectedImage = FileUtils.getSelectedImageFromResult(getActivity(), resultCode, data, tempUri);
//                isUploadFile = 2;
////                btnAdd.setEnabled(true);
////                btnAdd.setImageDrawable(getDrawable(R.drawable.ic_job_save));
//                break;
//            default:
//                super.onActivityResult(requestCode, resultCode, data);
//                break;
//        }
//    }
//}
