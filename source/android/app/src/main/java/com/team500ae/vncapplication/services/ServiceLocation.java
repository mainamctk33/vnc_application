package com.team500ae.vncapplication.services;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;

import com.google.gson.JsonArray;
import com.team500ae.library.utils.SharedPreferencesUtil;
import com.team500ae.vncapplication.constants.Constants;
import com.team500ae.vncapplication.data_access.BookmarkInfo;
import com.team500ae.vncapplication.data_access.RealmInfo;
import com.team500ae.vncapplication.data_access.location.LocationInfo;
import com.team500ae.vncapplication.data_access.location.TypeLocationInfo;
import com.team500ae.vncapplication.data_access.user.UserInfo;
import com.team500ae.vncapplication.models.BaseReturnFunctionEntity;
import com.team500ae.vncapplication.models.ServiceResponseEntity;
import com.team500ae.vncapplication.models.realm_models.location.TypeLocationEntity;

import java.util.ArrayList;
import java.util.Date;

import io.realm.Realm;
import io.realm.exceptions.RealmError;

/**
 * An {@link IntentService} subclass for handling asynchronous task requests in
 * a service on a separate handler thread.
 * <p>
 * TODO: Customize class - update intent actions, extra parameters and static
 * helper methods.
 */
public class ServiceLocation extends IntentService {
    // TODO: Rename actions, choose action names that describe tasks that this
    // IntentService can perform, e.g. ACTION_FETCH_NEW_ITEMS
    private static final String ACTION_UPDATE_LOCATION = "com.team500ae.vncapplication.services.action.ACTION_UPDATE_LOCATION";
    private static final String ACTION_SYNC_TYPE_LOCATION = "com.team500ae.vncapplication.services.action.ACTION_SYNC_TYPE_LOCATION";
    private static final String ACTION_SET_DEFAULT_CATEGORY = "com.team500ae.vncapplication.services.action.ACTION_SET_DEFAULT_CATEGORY";
    private static final String ACTION_LOCATION_BOOKMARK = "com.team500ae.vncapplication.services.action.ACTION_LOCATION_BOOKMARK";
    private static final String ACTION_LOCATION_RATE = "com.team500ae.vncapplication.services.action.ACTION_LOCATION_RATE";

    public ServiceLocation() {
        super("ServiceLocation");
    }

    /**
     * Starts this service to perform action Foo with the given parameters. If
     * the service is already performing a task this action will be queued.
     *
     * @see IntentService
     */
    // TODO: Customize helper method
    public static void startActionUpdateLocation(Context context, double lat, double lon) {
        try {
            Intent intent = new Intent(context, ServiceLocation.class);
            intent.setAction(ACTION_UPDATE_LOCATION);
            intent.putExtra("lat", lat);
            intent.putExtra("lon", lon);
            context.startService(intent);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void startActionSyncTypeLocation(Context context) {
        Intent intent = new Intent(context, ServiceLocation.class);
        intent.setAction(ACTION_SYNC_TYPE_LOCATION);
        context.startService(intent);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        if (intent != null) {
            final String action = intent.getAction();
            switch (action) {
                case ACTION_UPDATE_LOCATION:
                    if (UserInfo.isLogin()) {
                        double lat = intent.getDoubleExtra("lat", 0);
                        double lon = intent.getDoubleExtra("lon", 0);
                        handleActionUpdateLocation(lat, lon);
                    }
                    break;
                case ACTION_SYNC_TYPE_LOCATION:
                    handleActionSyncTypeLocation();
                    break;
                case ACTION_SET_DEFAULT_CATEGORY:
                    handleSetDefaulCategory();
                    break;
                case ACTION_LOCATION_BOOKMARK:
                    boolean bookmark = intent.getBooleanExtra("BOOKMARK", false);
                    int id = intent.getIntExtra("ID", 0);
                    handleActionBookmark(id, bookmark);
                    break;
                case ACTION_LOCATION_RATE:
                    handleActionRate(intent.getIntExtra("ID", 0), intent.getFloatExtra("MYRATE", 0),intent.getStringExtra("MYCOMMENT"));
                    break;
            }
        }
    }

    private void handleActionRate(int id, float myrate, String mycomment) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    BaseReturnFunctionEntity<Boolean> result = new LocationInfo().rate(id, myrate,mycomment);
                } catch (Exception e) {
                    e.printStackTrace();
                } catch (OutOfMemoryError e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }

    private void handleActionBookmark(int id, boolean bookmark) {
        // TODO: Handle action Foo
        new Thread(new Runnable() {
            @Override
            public void run() {
                Realm realm = null;
                try {
                    realm = RealmInfo.getRealm(getApplicationContext());
                    if (UserInfo.isLogin()) {
                        BookmarkInfo newsInfo = new BookmarkInfo();
                        realm = RealmInfo.getRealm(getApplicationContext());
                        newsInfo.bookmark(getApplicationContext(), realm, id, UserInfo.getCurrentUserId(), BookmarkInfo.BookmarkType.Location, bookmark);
                        BaseReturnFunctionEntity<Boolean> result = new LocationInfo().bookmark(id, bookmark);
                        if (result.isTrue()) {
                            Intent intent = new Intent(Constants.ACTION_LOCATION_BOOKMARK_SUCCESS);
                            getApplicationContext().sendBroadcast(intent);
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                } catch (RealmError e) {
                    e.printStackTrace();
                } catch (OutOfMemoryError e) {
                    e.printStackTrace();
                } finally {
                    RealmInfo.closeRealm(realm);
                }
            }
        }).start();
    }

    private void handleSetDefaulCategory() {
        // TODO: Handle action Foo
        new Thread(new Runnable() {
            @Override
            public void run() {
                String userId = UserInfo.getCurrentUserId();
                Realm realm = null;
                try {
                    realm = RealmInfo.getRealm(getApplicationContext());
                    TypeLocationInfo typeLocationInfo = new TypeLocationInfo();
                    BaseReturnFunctionEntity<ArrayList<TypeLocationEntity>> currentMyType = typeLocationInfo.getTypeLocationHasSelected(getApplicationContext(), realm, userId);
                    if (currentMyType.isTrue() && currentMyType.getData().size() == 0) {
                        BaseReturnFunctionEntity<ArrayList<TypeLocationEntity>> listType = typeLocationInfo.getListTypeLocation(getApplicationContext(), realm);
                        if (listType.isTrue()) {
                            ArrayList<TypeLocationEntity> typeLocation = listType.getData();
                            for (int i = 0; i < 5 && i < typeLocation.size(); i++) {
                                typeLocationInfo.saveTypeLocationToFavorite(getApplicationContext(), realm, userId, typeLocation.get(i).getId());
                            }
                            Intent intent = new Intent(Constants.ACTION_TYPE_LOCATION_FOR_USER_CHANGED);
                            getApplicationContext().sendBroadcast(intent);
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                } catch (RealmError e) {
                    e.printStackTrace();
                } catch (OutOfMemoryError e) {
                    e.printStackTrace();
                } finally {
                    RealmInfo.closeRealm(realm);
                }
            }
        }).start();
    }

    private void handleActionSyncTypeLocation() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                Realm realm = null;
                try {
                    realm = RealmInfo.getRealm(getApplicationContext());
                    TypeLocationInfo typeLocationInfo = new TypeLocationInfo();
                    long lastSyncTypeLocationDate = 0;//SharedPreferencesUtil.getLong(getApplicationContext(), Constants.LAST_SYNC_TYPENEWS, 0L);
                    ServiceResponseEntity<JsonArray> result = typeLocationInfo.syncTypeLocation(getApplicationContext(), lastSyncTypeLocationDate);
                    if (result != null && result.getStatus() == 0) {
                        if (typeLocationInfo.updateTypeLocation(realm, result.getData()).getData())
                            SharedPreferencesUtil.setSharedPreferences(getApplicationContext(), SharedPreferencesUtil.EnumType.Long, Constants.LAST_SYNC_TYPELOCATION, new Date().getTime());
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                } catch (RealmError e) {
                    e.printStackTrace();
                } catch (OutOfMemoryError e) {
                    e.printStackTrace();
                } finally {
                    RealmInfo.closeRealm(realm);
                }
            }
        }).start();
    }

    /**
     * Handle action Foo in the provided background thread with the provided
     * parameters.
     *
     * @param lat
     * @param lon
     */
    private void handleActionUpdateLocation(double lat, double lon) {
        // TODO: Handle action Foo
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    new UserInfo().updateLocation(getApplicationContext(), lat, lon);
                } catch (Exception e) {
                    e.printStackTrace();
                } catch (OutOfMemoryError e) {
                    e.printStackTrace();
                } finally {
                }
            }
        }).start();
    }

    public static void startSetDefaultCategory(Context context) {
        Intent intent = new Intent(context, ServiceLocation.class);
        intent.setAction(ACTION_SET_DEFAULT_CATEGORY);
        context.startService(intent);
    }

    public static void startActionBookmark(Context context, int id, boolean bookmark) {
        Intent intent = new Intent(context, ServiceLocation.class);
        intent.putExtra("ID", id);
        intent.putExtra("BOOKMARK", bookmark);
        intent.setAction(ACTION_LOCATION_BOOKMARK);
        context.startService(intent);
    }

    public static void startActionRate(Context context, int id, float myRate, String myComment) {
        Intent intent = new Intent(context, ServiceLocation.class);
        intent.putExtra("ID", id);
        intent.putExtra("MYRATE", myRate);
        intent.putExtra("MYCOMMENT", myComment);
        intent.setAction(ACTION_LOCATION_RATE);
        context.startService(intent);
    }
}
