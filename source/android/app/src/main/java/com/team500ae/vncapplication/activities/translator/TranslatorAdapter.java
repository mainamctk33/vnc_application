package com.team500ae.vncapplication.activities.translator;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.team500ae.vncapplication.R;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class TranslatorAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private static final int TYPE_HEADER = 0;
    private static final int TYPE_ITEM = 1;
    private List<TranslatorModel> mData;

    public TranslatorAdapter(List<TranslatorModel> itemObjects) {
        this.mData = itemObjects;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == TYPE_HEADER) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_header_translator, parent, false);
            return new HeaderViewHolder(view);
        } else if (viewType == TYPE_ITEM) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_translator, parent, false);
            return new ItemViewHolder(view);
        }
        throw new RuntimeException("No match for " + viewType + ".");
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        TranslatorModel item = mData.get(position);
        if (holder instanceof HeaderViewHolder) {
            HeaderViewHolder viewHeader = (HeaderViewHolder) holder;
            viewHeader.headerTitle.setText(item.getHeaderTitle());
        } else if (holder instanceof ItemViewHolder) {
            ItemViewHolder viewMain = (ItemViewHolder) holder;
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (isPositionHeader(position))
            return TYPE_HEADER;
        return TYPE_ITEM;
    }

    private boolean isPositionHeader(int position) {

        return position == 0 || position == 10;
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }


    public class HeaderViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.viewHeaderTranslator_tvTitle)
        TextView headerTitle;

        public HeaderViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }


    public class ItemViewHolder extends RecyclerView.ViewHolder {
        public ItemViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}