package com.team500ae.vncapplication.constants;

import android.content.Context;
import android.content.Intent;

/**
 * Created by MaiNam on 11/15/2017.
 */

public class Constants {
    public static final String APP_VERSION = "1.x.x";
    public static final String EMPTY = "";

    public static final int MAX_HISTORY_SEARCH_ITEM = 8;

    //public static final String SERVER = "http://onlineapi.site";
    public static final String SERVER = "http://vnc.iduhoc.vn";
    public static final long DELAY_LOGIN_FACEBOOK = 2000;
    public static final String LAST_SYNC_TYPENEWS = "LAST_SYNC_TYPENEWS";
    public static final String LAST_SYNC_TYPELOCATION = "LAST_SYNC_TYPELOCATION";
    public static final String REALM_FILE = "15112017.realm";
    public static final String CURRENT_DATABASE = "15112017.realm";
    public static final String API_TYPE_NEWS_SYNC = "typenews/sync";
    public static final String API_TAG_SYNC = "tag/sync";
    public static final String API_NEWS_GETALL = "news/getall";
    public static final String API_NEWS_BOOKMARK = "news/bookmark";
    public static boolean IS_SET_DEFAULT_CATEGORY_NEWS = false;
    public static boolean IS_SET_DEFAULT_CATEGORY_JOBS = false;
    public static boolean IS_SET_DEFAULT_CATEGORY_TIPS = false;
    public static boolean IS_SET_LOCATION_FOR_USER_CHANGED = false;

    //Tips
    public static final String LAST_SYNC_TYPETIPS = "LAST_SYNC_TYPETIPS";
    public static final String API_TYPE_TIPS_SYNC = "typetips/sync";
    public static final String API_TIPS_GETALL = "tips/getall";
    public static final String API_TIPS_GET_BY_ID = "tips/get";
    public static final String API_TIPS_BOOKMARK = "tips/bookmark";
    public static final String ACTION_TIPS_VIEW_MODE_CHANGE = "ACTION_TIPS_VIEW_MODE_CHANGE";
    public static final String ACTION_TIPS_BOOKMARK_SUCCESS = "ACTION_TIPS_BOOKMARK_SUCCESS";
    public static final String ACTION_TYPE_TIPS_FOR_USER_CHANGED = "ACTION_TYPE_TIPS_FOR_USER_CHANGED";
    public static final String ACTION_LOCATION_BACK_TO_MAIN_APP = "ACTION_LOCATION_BACK_TO_MAIN_APP";

    //Question
    public static final String API_QUESTION_GET_MY_QUESTION = "question/GetMyQuestion";
    public static final String API_QUESTION_GET_COMMON_QUESTION = "question/GetCommonQuestion";
    public static final String API_QUESTION_GET_BY_ID = "question/get";
    public static final String API_QUESTION_CREATE = "question/create";
    public static final String API_QUESTION_IS_EXISTED = "question/IsExisted";

    //account
    public static final String API_ACCOUNT_CHECK_EXISTED = "user/isexisted";
    public static final String API_ACCOUNT_LOGIN_VIA_EMAIL = "user/LoginEmail";
    public static final String API_ACCOUNT_CREATE = "user/create";
    public static final String API_ACCOUNT_UPDATE = "user/UpdateAccount";
    public static final String API_ACCOUNT_GET_INFO = "user/GetUserInfo";
    public static final String API_ACCOUNT_UPDATE_PASSWORD = "user/ChangePassword";
    public static final String API_ACCOUNT_SHARE_ONLINE_STATUS = "user/ShareOnlineStatusSetting";
    public static final String API_ACCOUNT_SHOW_AVATAR_SETTING = "user/ShowAvatarSetting";
    public static final String API_ACCOUNT_RECEIVE_STRANGE_MESSAGE = "user/ReceiveStrangeMessageSetting";
    public static final String API_ACCOUNT_SHOW_PHONE_NUMBER = "user/ShowYourPhoneNumberSetting";
    public static final String API_ACCOUNT_UPDATE_SETTING_ATTRIBUTE = "user/UpdateSettingAttribute";
    public static final int MAX_FULL_NAME_LENGTH = 50;
    public static final int MAX_PHONE_NUMBER_LENGTH = 14;
    public static final int MAX_ADDRESS_LENGTH = 255;

    public static final String API_NEWS_GET = "news/get";
    public static final String API_USER_LOGIN_SOCIAL = "user/loginSocial";
    public static final String ACTION_NEWS_VIEW_MODE_CHANGE = "ACTION_NEWS_VIEW_MODE_CHANGE";
    public static final String ACTION_NEWS_BOOKMARK_SUCCESS = "ACTION_NEWS_BOOKMARK_SUCCESS";
    public static final String ACTION_TYPE_NEWS_FOR_USER_CHANGED = "ACTION_TYPE_NEWS_FOR_USER_CHANGED";


    public static final String ACTION_JOB_VIEW_MODE_CHANGE = "ACTION_JOB_VIEW_MODE_CHANGE";
    public static final String ACTION_JOB_BOOKMARK_SUCCESS = "ACTION_JOB_BOOKMARK_SUCCESS";
    public static final String ACTION_JOB_ADD_SUCCESS = "ACTION_JOB_ADD_SUCCESS";
    public static final String ACTION_TYPE_JOB_FOR_USER_CHANGED = "ACTION_TYPE_JOB_FOR_USER_CHANGED";
    public static final String ID_SHARE_JOB = "id_share_job";

    public static final String ACTION_UPDATE_ACCOUNT = "ACTION_UPDATE_ACCOUNT";
    public static final String ACTION_LOGIN = "ACTION_LOGIN";
    public static final String ACTION_LOGOUT = "ACTION_LOGOUT";
    public static final long DELAY_SPLASH_SCREEN = 500;
    //JOB
    public static final String API_TYPE_JOB_SYNC = "typejob/sync";
    public static final String API_COUNTRY_JOB_SYNC = "countryjob/sync";
    public static final String API_CITY_JOB_SYNC = "cityjob/sync";
    public static final String API_LEVEL_JOB_SYNC = "leveljob/sync";
    public static final String API_SALARY_JOB_SYNC = "salaryjob/sync";
    public static final String API_COMPANY_JOB_SYNC = "companyjob/sync";
    public static final String API_TYPE_JOB_GETALL = "typejob/getall";
    public static final String API_LEVEL_JOB_GETALL = "leveljob/getall";
    public static final String API_JOB_GETALL = "job/getall";
    public static final String API_JOB_GETALLWITHCITY = "job/getallwithcity";
    public static final String API_JOB_SEARCHWITHCITY = "job/searchwithcity";
    public static final String API_JOB_BOOKMARK = "job/bookmark";
    public static final String API_JOB_CREATE = "job/create";
    public static final String API_JOB_DELETE = "job/delete";
    public static final String START_COLOR = "<font color='#F15B22'> * </font>";
    public static final String API_JOB_GET = "job/get";
    public static final String LAST_SYNC_TYPEJOB = "LAST_SYNC_TYPEJOB";
    public static final String LAST_SYNC_COUNTRYJOB = "LAST_SYNC_COUNTRYJOB";
    public static final String LAST_SYNC_CITYJOB = "LAST_SYNC_CITYJOB";
    public static final String LAST_SYNC_LEVELJOB = "LAST_SYNC_LEVELJOB";
    public static final String LAST_SYNC_SALARYJOB = "LAST_SYNC_SALARYJOB";
    public static final String LAST_SYNC_COMPANYJOB = "LAST_SYNC_COMPANYJOB";
    public static final String LAST_SYNC_TAG = "LAST_SYNC_TAG";
    public static final String API_COUNTRY_JOB_GETALL = "countryjob/getall";
    public static final String API_CITY_JOB_GETALL = "cityjob/getall";
    public static final String API_CITY_JOB_GETALLWITHCOUNTRY = "cityjob/getallwithcountry";
    public static final String API_SALARY_JOB_GETALL = "salaryjob/getall";
    public static final String API_COMPANY_JOB_GETALL = "companyjob/getall";

    //TRANSLATOR
    public static final String API_CATEGORY_TRANSLATOR_SYNC = "categorytranslator/getall";
    public static final String API_CATEGORY_TRANSLATOR_GETALL = "categorytranslator/getall";
    public static final String API_TYPE_TRANSLATOR_SYNC = "typetranslator/sync";
    public static final String API_TYPE_TRANSLATOR_GETALL = "typetranslator/getall";
    public static final String API_TYPE_TRANSLATOR_GETALLBYCATEGORY = "typetranslator/getallbycategory";
    public static final String LAST_SYNC_CATEGORYTRANSLATOR = "LAST_SYNC_CATEGORYTRANSLATOR";
    public static final String LAST_SYNC_TYPETRANSLATOR = "LAST_SYNC_TYPETRANSLATOR";
    public static final String API_TRANSLATOR_GETALL = "translator/getall";
    public static final String API_TRANSLATOR_GETALLNEW = "translator/getallnew";
    public static final String API_TRANSLATOR_BOOKMARK = "translator/bookmark";
    public static final String API_TRANSLATOR_GET = "translator/get";

    public static final String UPLOAD_FILE = "filemanager/upload";
    public static final String API_GROUP_CHAT_LEAVE = "chat/LeaveGroup";
    public static final String API_GROUP_CHAT_UPDATE = "chat/UpdateGroup";
    public static final String API_GROUP_CHAT_INVITE_MEMBER = "chat/InviteMember";
    public static final String API_USER_UPDATE_LOCATION = "user/updatelocation";

    public static final String VNC_SHARED_PREFERENCES_NAME = "com.team500ae.vncapplication.SharedPreferences";
    public static final String SHARED_PREFERENCES_LANGUAE = "vnc_language";
    public static final String API_USER_SEARCH_FRIEND = "/friend/search";
    public static final String API_FRIEND_SEARCH = "/friend/search";
    public static final String API_FRIEND_GET_LIST_REQUEST_FRIEND = "/friend/GetListFriendRequest";
    public static final String API_FRIEND_SEND_FRIEND_REQUEST = "/friend/SendFriendRequest";
    public static final String API_FRIEND_CANCEL_FRIEND_REQUEST = "/friend/CancelFriendRequest";
    public static final String API_FRIEND_DELETE_FRIEND_REQUEST = "/friend/DeleteFriendRequest";
    public static final String API_FRIEND_ACCEPT_FRIEND_REQUEST = "/friend/AcceptFriendRequest";
    public static final String API_FRIEND_GET_FRIEND_STATUS = "/friend/GetFriendStatus";
    public static final String API_FRIEND_UNFRIEND = "/friend/Unfriend";
    public static final String API_FRIEND_IS_FRIEND = "/friend/IsFriend";
    public static final String API_TYPE_LOCATION_SYNC = "typelocation/sync";
    public static final String ACTION_TYPE_LOCATION_FOR_USER_CHANGED = "ACTION_TYPE_LOCATION_FOR_USER_CHANGED";
    public static final String API_LOCATION_CREATE = "/location/create";
    public static final String API_LOCATION_SEARCH_BY_NAME = "/location/search_by_name";
    public static final String API_LOCATION_GET_DETAIL = "/location/get";
    public static final java.lang.String API_LOCATION_LOCATION_NEAR = "/location/near";
    public static final String API_LOCATION_SEARCH_BOOKMARK = "/location/search_location_bookmark";
    public static final String API_LOCATION_SEARCH_MY_LOCATION = "/location/search_my_location";
    public static final String API_LOCATION_BOOKMARK = "location/bookmark";
    public static final String ACTION_LOCATION_BOOKMARK_SUCCESS = "ACTION_LOCATION_BOOKMARK_SUCCESS";
    public static final String ACTION_LOCATION_RATE_SUCCESS = "ACTION_LOCATION_RATE_SUCCESS";
    public static final String API_LOCATION_MY_RATING = "location/myrate";
    public static final String API_LOCATION_RATE = "location/rate";
    public static final String API_LOCATION_IS_BOOKMARK = "location/isbookmark";
    public static final String API_USER_SEARCH = "user/search";

    public static final String STATUS_FRIEND = "Friend";
    public static final String STATUS_SENT_REQUEST = "Sent Request";
    public static final String STATUS_RECEIVED_REQUEST = "Received Request";
    public static final String STATUS_NOT_FRIEND = "Not Friend";
    public static final String STATUS_GET_FAIL = "Get Fail";
    public static final String MAP_URL_HEADER = "http://maps.google.com/maps/api/staticmap?zoom=16&size=600x600&markers=";
    public static final String MAP_URL_FOOTER = "&sensor=false";
    public static final String JSON_DATA_TYPE_TRANSLATOR_LIST = "type_translator_list";
    public static final String JSON_DATA_TRANSLATOR_DETAIL_LIST = "translator_detail_list";
    public static final String JSON_DATA_TRANSLATOR_TYPE_LIST = "translator_type_list";
    public static final String FILTER_GENDER = "FilterGender";
    public static final String FILTER_AGE = "FilterAge";
    public static final String ONLY_FRIEND = "SearchOnlyFriend";
    public static final String MAX_DISTANCE = "MaxDistance";
    public static final String LIST_MODEL = "list_model";
    public static final String CODE = "code";
    public static final String API_ACCOUNT_FORGOT_PASSWORD = "user/forgotpassword";
    public static final String MESSAGE = "message";
    public static final String API_NOTIFICATION_REGISTER_TOKEN = "notification/RegisterToken";
    public static final String API_NOTIFICATION_REMOVE_DEVICE = "notification/RemoveDevice";
    public static final String ACTION_RELOAD_LIST_CONTACT = "ACTION_RELOAD_LIST_CONTACT";
    public static final String API_CHAT_CREATE_ACCOUNT = "chat/CreateAccount";
}
