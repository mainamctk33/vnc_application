package com.team500ae.vncapplication.models.realm_models.job;


import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by SVIP on 11/30/2017.
 */

public class MyTypeJobEntity extends RealmObject {
    @PrimaryKey
    String id;
    String userId;
    int position;
    TypeJobEntity typeJob;
    long updatedDate;

    public long getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(long updatedDate) {
        this.updatedDate = updatedDate;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public TypeJobEntity getTypeJob() {
        return typeJob;
    }

    public void setTypeJob(TypeJobEntity typeJob) {
        this.typeJob = typeJob;
    }
}
