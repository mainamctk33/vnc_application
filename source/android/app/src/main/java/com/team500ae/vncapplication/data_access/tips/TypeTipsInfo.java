package com.team500ae.vncapplication.data_access.tips;

import android.content.Context;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.team500ae.library.utils.ConvertUtils;
import com.team500ae.vncapplication.R;
import com.team500ae.vncapplication.constants.Constants;
import com.team500ae.vncapplication.data_access.ClientInfo;
import com.team500ae.vncapplication.models.BaseReturnFunctionEntity;
import com.team500ae.vncapplication.models.ServiceResponseEntity;
import com.team500ae.vncapplication.models.realm_models.tips.MyTypeTipsEntity;
import com.team500ae.vncapplication.models.realm_models.tips.TypeTipsEntity;

import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmResults;
import io.realm.exceptions.RealmError;

/**
 * Created by phuong.ndp on 26/11/2017.
 * done
 */

public class TypeTipsInfo {
    public ServiceResponseEntity<JsonArray> syncTypeTips(Context context, long lastSyncDate) {
        try {
            ClientInfo.DataResponse dataResponse = new ClientInfo().requestApi(ClientInfo.MethodType.GET, Constants.API_TYPE_TIPS_SYNC + "/" + lastSyncDate);
            if (dataResponse.isOK()) {
                ServiceResponseEntity<JsonArray> result = dataResponse.getData(new TypeToken<ServiceResponseEntity<JsonArray>>() {
                }.getType());
                return result;
            }
        } catch (UnknownHostException e) {
            e.printStackTrace();
        } catch (SocketTimeoutException e) {
            e.printStackTrace();
        }
        return null;
    }

    public BaseReturnFunctionEntity<Boolean> removeTypeTipsFromListFavorite(Context context, Realm realm, String userId, int... listTypeTipsIds) {
        if (listTypeTipsIds == null || listTypeTipsIds.length == 0)
            return new BaseReturnFunctionEntity<>(BaseReturnFunctionEntity.StatusFunction.FALSE, context.getString(R.string.select_typenews_to_remove));

        try {
            realm.executeTransaction(realm1 -> {
                for (int id : listTypeTipsIds) {
                    MyTypeTipsEntity myTypeTipsEntity = realm.where(MyTypeTipsEntity.class).equalTo("id", userId + id).findFirst();
                    if (myTypeTipsEntity != null)
                        myTypeTipsEntity.deleteFromRealm();
                }
            });
            return new BaseReturnFunctionEntity<>(BaseReturnFunctionEntity.StatusFunction.TRUE, true);
        } catch (RealmError e) {
            return new BaseReturnFunctionEntity<>(BaseReturnFunctionEntity.StatusFunction.EXCEPTION, e.getMessage());
        } catch (Exception e) {
            return new BaseReturnFunctionEntity<>(BaseReturnFunctionEntity.StatusFunction.EXCEPTION, e.getMessage());
        } catch (OutOfMemoryError e) {
            return new BaseReturnFunctionEntity<>(BaseReturnFunctionEntity.StatusFunction.EXCEPTION, e.getMessage());
        }
    }

    public BaseReturnFunctionEntity<Boolean> saveTypeTipsToFavorite(Context context, Realm realm, String userId, int... listTypeTipsIds) {
        if (listTypeTipsIds == null || listTypeTipsIds.length == 0)
            return new BaseReturnFunctionEntity<>(BaseReturnFunctionEntity.StatusFunction.FALSE, context.getString(R.string.select_typetips_to_set_favorite));

        try {
            realm.executeTransaction(realm1 -> {
                for (int id : listTypeTipsIds) {
                    TypeTipsEntity typeTipsEntity = realm.where(TypeTipsEntity.class).equalTo("id", id).findFirst();
                    if (typeTipsEntity != null) {
                        MyTypeTipsEntity myTypeTipsEntity = new MyTypeTipsEntity();
                        myTypeTipsEntity.setId(userId + id);
                        myTypeTipsEntity.setTypeTips(typeTipsEntity);
                        myTypeTipsEntity.setUserId(userId);
                        realm.copyToRealmOrUpdate(myTypeTipsEntity);
                    }
                }
            });

            return new BaseReturnFunctionEntity<>(BaseReturnFunctionEntity.StatusFunction.TRUE, true);
        } catch (RealmError e) {
            return new BaseReturnFunctionEntity<>(BaseReturnFunctionEntity.StatusFunction.EXCEPTION, e.getMessage());
        } catch (Exception e) {
            return new BaseReturnFunctionEntity<>(BaseReturnFunctionEntity.StatusFunction.EXCEPTION, e.getMessage());
        } catch (OutOfMemoryError e) {
            return new BaseReturnFunctionEntity<>(BaseReturnFunctionEntity.StatusFunction.EXCEPTION, e.getMessage());
        }
    }

    public BaseReturnFunctionEntity<ArrayList<TypeTipsEntity>> getListTypeTips(Context context, Realm realm) {
        try {
            RealmResults<TypeTipsEntity> realmList = realm.where(TypeTipsEntity.class).findAll();
            ArrayList<TypeTipsEntity> result = new ArrayList<>();
            result.addAll(realmList);
            return new BaseReturnFunctionEntity<>(BaseReturnFunctionEntity.StatusFunction.TRUE, result);
        } catch (RealmError e) {
            return new BaseReturnFunctionEntity<>(BaseReturnFunctionEntity.StatusFunction.EXCEPTION, e.getMessage());
        } catch (Exception e) {
            return new BaseReturnFunctionEntity<>(BaseReturnFunctionEntity.StatusFunction.EXCEPTION, e.getMessage());
        } catch (OutOfMemoryError e) {
            return new BaseReturnFunctionEntity<>(BaseReturnFunctionEntity.StatusFunction.EXCEPTION, e.getMessage());
        }
    }

    public BaseReturnFunctionEntity<ArrayList<TypeTipsEntity>> getTypeTipsHasSelected(Context context, Realm realm, String userId) {
        try {
            RealmResults<MyTypeTipsEntity> realmList = realm.where(MyTypeTipsEntity.class).equalTo("userId", userId).findAll();
            ArrayList<TypeTipsEntity> result = new ArrayList<>();
            for (MyTypeTipsEntity typeTipsEntity : realmList) {
                result.add(typeTipsEntity.getTypeTips());
            }
            return new BaseReturnFunctionEntity<>(BaseReturnFunctionEntity.StatusFunction.TRUE, result);
        } catch (RealmError e) {
            return new BaseReturnFunctionEntity<>(BaseReturnFunctionEntity.StatusFunction.EXCEPTION, e.getMessage());
        } catch (Exception e) {
            return new BaseReturnFunctionEntity<>(BaseReturnFunctionEntity.StatusFunction.EXCEPTION, e.getMessage());
        } catch (OutOfMemoryError e) {
            return new BaseReturnFunctionEntity<>(BaseReturnFunctionEntity.StatusFunction.EXCEPTION, e.getMessage());
        }
    }

    public BaseReturnFunctionEntity<Boolean> updateTypeTips(Realm realm, JsonArray data) {
        try {

            RealmResults<TypeTipsEntity> realmList = realm.where(TypeTipsEntity.class).findAll();
            List<Integer> listID = new ArrayList<>();
            realm.executeTransaction(realm1 -> {
                for (int i = 0; i < data.size(); i++) {
                    JsonObject jsonObject = ConvertUtils.toJsonObject(data.get(i));
                    int idData = ConvertUtils.toInt(jsonObject.get("ID"));
                    listID.add(idData);
                    updateTypeTips(realm, jsonObject, false);
                }
            });

            for (int j = 0; j < realmList.size(); j++) {
                int idRealm = realmList.get(j).getId();
                if (!listID.contains(idRealm)) {
                    realm.executeTransaction(new Realm.Transaction() {
                        @Override
                        public void execute(Realm realm) {
                            RealmResults<TypeTipsEntity> result = realm.where(TypeTipsEntity.class).equalTo("id", idRealm).findAll();
                            result.deleteAllFromRealm();
                        }
                    });

                }
            }
//            realm.executeTransaction(realm1 -> {
//                for (int i = 0; i < data.size(); i++) {
//                    JsonObject jsonObject = ConvertUtils.toJsonObject(data.get(i));
//                    updateTypeTips(realm, jsonObject, false);
//                }
//            });
            return new BaseReturnFunctionEntity<>(BaseReturnFunctionEntity.StatusFunction.TRUE, true);
        } catch (RealmError e) {
            return new BaseReturnFunctionEntity<>(BaseReturnFunctionEntity.StatusFunction.EXCEPTION, e.getMessage());
        } catch (Exception e) {
            return new BaseReturnFunctionEntity<>(BaseReturnFunctionEntity.StatusFunction.EXCEPTION, e.getMessage());
        } catch (OutOfMemoryError e) {
            return new BaseReturnFunctionEntity<>(BaseReturnFunctionEntity.StatusFunction.EXCEPTION, e.getMessage());
        }
    }

    public BaseReturnFunctionEntity<Boolean> updateTypeTips(Realm realm, final JsonObject data, boolean needTransaction) {
        try {
            if (needTransaction) {
                realm.executeTransaction(realm1 -> {
                    updateTypeTips(realm, data, false);
                });
            } else {
                int id = ConvertUtils.toInt(data.get("ID"));
                TypeTipsEntity typeTipsEntity = new TypeTipsEntity();
                typeTipsEntity.setName(ConvertUtils.toString(data.get("Name")));
                typeTipsEntity.setName2(ConvertUtils.toString(data.get("Name2")));
                typeTipsEntity.setId(id);
                realm.copyToRealmOrUpdate(typeTipsEntity);
            }
            return new BaseReturnFunctionEntity<>(BaseReturnFunctionEntity.StatusFunction.TRUE, true);
        } catch (RealmError e) {
            return new BaseReturnFunctionEntity<>(BaseReturnFunctionEntity.StatusFunction.EXCEPTION, e.getMessage());
        } catch (Exception e) {
            return new BaseReturnFunctionEntity<>(BaseReturnFunctionEntity.StatusFunction.EXCEPTION, e.getMessage());
        } catch (OutOfMemoryError e) {
            return new BaseReturnFunctionEntity<>(BaseReturnFunctionEntity.StatusFunction.EXCEPTION, e.getMessage());
        }
    }

}
