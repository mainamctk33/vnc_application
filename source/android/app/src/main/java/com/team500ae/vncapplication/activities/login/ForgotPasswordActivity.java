package com.team500ae.vncapplication.activities.login;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.InputFilter;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.team500ae.vncapplication.R;
import com.team500ae.vncapplication.base.BaseActivity;
import com.team500ae.vncapplication.constants.Constants;
import com.team500ae.vncapplication.data_access.user.UserInfo;
import com.team500ae.vncapplication.models.BaseReturnFunctionEntity;
import com.team500ae.vncapplication.utils.KeyboardUtils;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Administrator on 22/12/2017.
 */

public class ForgotPasswordActivity extends BaseActivity {

    @BindView(R.id.edt_email)
    EditText edtEmail;
    @BindView(R.id.forgotPassword_layout)
    LinearLayout forgotPasswordLayout;

    @OnClick(R.id.btn_submit_forgot)
    void submit() {
        try {
            dialog = ProgressDialog.show(getActivity(), "",
                    getString(R.string.please_wait), true);

            dialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
        String email = edtEmail.getText().toString();

        new AsyncTask<Void, Void, BaseReturnFunctionEntity<Boolean>>() {
            @Override
            protected void onPreExecute() {
                try {

                    dialog.show();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                super.onPreExecute();
            }

            @Override
            protected BaseReturnFunctionEntity<Boolean> doInBackground(Void... voids) {
                BaseReturnFunctionEntity<Boolean> result = new UserInfo().sentEmail(email);
                return result;
            }

            @Override
            protected void onPostExecute(BaseReturnFunctionEntity<Boolean> result) {
                if (result != null && result.status == BaseReturnFunctionEntity.StatusFunction.TRUE) {
                    showSnackBar(R.string.send_success);
                } else
                    showSnackBar(result.getMessage());

                try {
                    dialog.hide();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                super.onPostExecute(result);
            }
        }.execute();


    }

    public static void open(Context activity) {
        Intent intent = new Intent(activity, ForgotPasswordActivity.class);
        activity.startActivity(intent);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        //LanguageUtils.changeLanguage(this, LanguageUtils.getAppLanguageSetting(this));
        setContentView(R.layout.activity_forgot_password);
        ButterKnife.bind(this);
        KeyboardUtils.setupUI(forgotPasswordLayout, getActivity());
        super.onCreate(savedInstanceState);
        showBackButton();
        getSupportActionBar().setTitle(R.string.forgot_password);

        edtEmail.setFilters(new InputFilter[]{new InputFilter.LengthFilter(Constants.MAX_FULL_NAME_LENGTH)});
    }

    @OnClick(R.id.btn_back)
    void back() {
        super.onBackPressed();
    }
}
