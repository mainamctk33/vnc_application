package com.team500ae.vncapplication.activities.chatcontact;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;

import com.team500ae.library.adapters.BaseViewPagerAdapter;
import com.team500ae.library.utils.ConvertUtils;
import com.team500ae.vncapplication.R;
import com.team500ae.vncapplication.activities.chat.fragments.GroupChannelListFragment;
import com.team500ae.vncapplication.activities.chat.fragments.SearchGroupChatFragment;
import com.team500ae.vncapplication.activities.chatcontact.fragments.ContactFragment;
import com.team500ae.vncapplication.activities.chatcontact.fragments.FriendRequestFragment;
import com.team500ae.vncapplication.base.BaseActivity;
import com.team500ae.vncapplication.constants.Constants;

import java.util.ArrayList;

import butterknife.BindView;

public class ChatContactActivity extends BaseActivity implements com.team500ae.library.activities.BaseActivity.OnCallBackBroadcastListener {
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.viewPager)
    ViewPager viewPager;

    @BindView(R.id.tabs)
    TabLayout tabLayout;

    BaseViewPagerAdapter adapter;
    private ArrayList<String> listTitle;
    private ContactFragment contactFragment;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        setContentView(R.layout.activity_chat_contact);
        super.onCreate(savedInstanceState);
        setSupportActionBar(toolbar);
        tabLayout.setupWithViewPager(viewPager);
        viewPager.setOffscreenPageLimit(4);
        listTitle = new ArrayList<>();
        ArrayList<Fragment> arrayList = ConvertUtils.toArrayList(Fragment.class,
                GroupChannelListFragment.newInstance()
                , contactFragment = ContactFragment.getInstants()
                , FriendRequestFragment.getInstants()
        );
        listTitle.add(getResources().getString(R.string.message));
        listTitle.add(getResources().getString(R.string.contact));
        listTitle.add(getResources().getString(R.string.friend_request));
        adapter = new BaseViewPagerAdapter(getActivity(), getSupportFragmentManager(), arrayList, listTitle);
        viewPager.setAdapter(adapter);
        int tab = getIntent().getIntExtra("TAB", 0);
        viewPager.setCurrentItem(tab);
        viewPager.addOnPageChangeListener(new android.support.v4.view.ViewPager.OnPageChangeListener() {
            public void onPageScrollStateChanged(int state) {
            }

            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            public void onPageSelected(int position) {
                getHandler().postDelayed(() -> {
                    hideKeyboard();
                }, 1000);
                // Check if this is the page you want.
            }
        });
        showBackButton();
        registerBroadcastReceiver(new ReceiverItem(Constants.ACTION_RELOAD_LIST_CONTACT, this));

    }

    public static void open(Context context) {
        context.startActivity(new Intent(context, ChatContactActivity.class));
    }

    @Override
    public void onCallBackBroadcastListener(Context context, String action, Intent intent) {
        switch (action) {
            case Constants.ACTION_RELOAD_LIST_CONTACT:
                if (contactFragment != null)
                    contactFragment.refresh();
                break;
        }
    }

    @Override
    public void onBackPressed() {
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.lnFragment);
        if (fragment instanceof SearchGroupChatFragment) {
            finish();
        } else
            super.onBackPressed();
    }
}
