package com.team500ae.vncapplication.activities.tips.fragments;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.team500ae.library.adapters.BaseRecyclerAdapter;
import com.team500ae.library.layoutmanager.MyLinearLayoutManager;
import com.team500ae.library.utils.ConvertUtils;
import com.team500ae.library.utils.ImageUtils;
import com.team500ae.library.utils.RecyclerViewUtils;
import com.team500ae.vncapplication.R;
import com.team500ae.vncapplication.activities.tips.MgrCategoryActivity;
import com.team500ae.vncapplication.activities.tips.TipsDetailActivity;
import com.team500ae.vncapplication.base.BaseViewHolder;
import com.team500ae.vncapplication.data_access.DataCacheInfo;
import com.team500ae.vncapplication.utils.AppUtils;

import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by phuong.ndp on 11/27/2017.
 * This class get list of tips in each 'tip category' and show. When user click on each tip >> call TipsDetailActivity.openChannel(getContext(), id);
 */
public class TipsTabFragment extends Fragment {
    private int typeView = 1;
    private static final String TAG = TipsTabFragment.class.getSimpleName();
    private int type;
    private BaseRecyclerAdapter<ViewHolder, JsonObject> adapter;
    private int page = 1;
    private int size = 10;
    private boolean isLoading = false;

    @BindView(R.id.swipeRefreshLayout)
    SwipeRefreshLayout swipeRefreshLayout;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.loading)
    View loading;
    @BindView(R.id.layout_no_found_result)
    LinearLayout noFoundResultLayout;

    HashMap<Integer, JsonObject> mapData = new HashMap<>();
    ArrayList<JsonObject> data = new ArrayList<>();

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        typeView = ConvertUtils.toInt(DataCacheInfo.getData(getContext(), DataCacheInfo.EnumCacheType.Tips_Small_List, ""), MgrCategoryActivity.TIPS_VIEW_TYPE.Small.getValue());
        Toast.makeText(getContext(), "typeView: " + typeView, Toast.LENGTH_SHORT);
        Bundle bundle = getArguments();
        if (bundle != null) type = bundle.getInt("type");
        View view = inflater.inflate(R.layout.fragment_tab_tips_fragment, container, false);
        ButterKnife.bind(this, view);

        recyclerView.setLayoutManager(new MyLinearLayoutManager(getContext()));
        //Create listener to handle event when user scroll to end of current view > Support loading next page
        RecyclerViewUtils recyclerViewUtils = new RecyclerViewUtils(new RecyclerViewUtils.RecyclerViewUtilsListener() {
            @Override
            public void onScrollToEnd() {
                if (!isLoading) getData(page + 1, size, type, getListener());
            }
        });
        recyclerViewUtils.setUp(recyclerView);

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                page = 1;
                getData(page, size, type, getListener());
            }
        });
        //item_tips> layout for each brief tip: image, title, time, sub content
        adapter = new BaseRecyclerAdapter<ViewHolder, JsonObject>(typeView == MgrCategoryActivity.TIPS_VIEW_TYPE.Small.getValue() ? R.layout.item_tips : R.layout.item_tips_large
                , new BaseRecyclerAdapter.BaseViewHolder<ViewHolder, JsonObject>() {
            @Override
            public ViewHolder getViewHolder(View v) {
                return new ViewHolder(v);
            }

            @Override
            public void bindData(RecyclerView recyclerView, ViewHolder viewHolder, JsonObject data, int position) {
                //set content for each tip in list of tips when user selects a 'tip category'
                viewHolder.tvTitle.setText(ConvertUtils.toString(data.get("Title")));
                viewHolder.tvTime.setText(com.team500ae.vncapplication.utils.TimeUtils.toSocialTime(getContext(), ConvertUtils.toLong(data.get("CreatedDate"))));
                if (data.get("SubContent") != null)
                    viewHolder.tvSubContent.setText(ConvertUtils.toString(data.get("SubContent")));
                int widthLarge = AppUtils.getScreenWidth();
                int heightLarge = viewHolder.imageView.getContext().getResources().getDimensionPixelSize(R.dimen._200dp);
                int width = viewHolder.imageView.getContext().getResources().getDimensionPixelSize(R.dimen.news_thumbnail_width);
                int height = viewHolder.imageView.getContext().getResources().getDimensionPixelSize(R.dimen.news_thumbnail_height);
                if (typeView == MgrCategoryActivity.TIPS_VIEW_TYPE.Small.getValue())
                    Glide.with(viewHolder.imageView).load(ConvertUtils.toString(data.get("Thumbnail"))).apply(new RequestOptions().override(width, height).placeholder(R.drawable.img_no_image)).into(viewHolder.imageView);
                else
                    Glide.with(viewHolder.imageView).load(ConvertUtils.toString(data.get("Thumbnail"))).apply(new RequestOptions().override(widthLarge, heightLarge).placeholder(R.drawable.img_no_image)).into(viewHolder.imageView);
                //ImageUtils.loadImageByGlide(getContext(), true, 200, 200, ConvertUtils.toString(data.get("Thumbnail")), R.drawable.img_no_image, R.drawable.img_no_image, viewHolder.imageView, true);
            }
        }, data, new BaseRecyclerAdapter.OnClickListener() {
            @Override
            public void onClick(View v, int position, Object o) {
                //handle event user clicks on each tip > show detail tip
                JsonObject jsonObject = (JsonObject) o;
                int id = ConvertUtils.toInt(jsonObject.get("ID"), 0);
                TipsDetailActivity.open(getContext(), id);
            }
        });
        adapter.bindData(recyclerView);
        getData(page, size, type, getListener());
        return view;
    }

    public static TipsTabFragment getInstance(int type) {
        TipsTabFragment tipsTabFragment = new TipsTabFragment();
        Bundle bundle = new Bundle();
        bundle.putInt("type", type);
        tipsTabFragment.setArguments(bundle);
        return tipsTabFragment;
    }

    public interface LoadTipsListener {
        public void onBeginLoad(Fragment fragment);

        public void onEndLoad(Fragment fragment);

        public JsonArray getData(int page, int size, int type, Fragment tipsTabFragment);
    }

    public LoadTipsListener getListener() {
        if (getContext() instanceof LoadTipsListener) return (LoadTipsListener) getContext();
        return null;
    }

    public void getData() {
        page = 1;
        getData(page, size, type, getListener());
    }

    public void getData(final int page, final int size, final int type, final LoadTipsListener listener) {
        new AsyncTask<Void, JsonArray, Void>() {
            @Override
            protected void onPreExecute() {
                swipeRefreshLayout.setEnabled(false);
                if (page == 1) swipeRefreshLayout.setRefreshing(true);
                else loading.setVisibility(View.VISIBLE);
                if (listener != null) listener.onBeginLoad(TipsTabFragment.this);
                isLoading = true;
                super.onPreExecute();
            }

            @Override
            protected Void doInBackground(Void... voids) {
                if (listener != null)
                    publishProgress(listener.getData(page, size, type, TipsTabFragment.this));
                else publishProgress(new JsonArray());
                return null;
            }

            @Override
            protected void onProgressUpdate(JsonArray... values) {
                if (values.length > 0 && values[0] != null) {
                    if (page == 1) {
                        mapData.clear();
                        data.clear();
                        adapter.notifyDataSetChanged();
                    }
                    bindData(values[0]);
                }
                super.onProgressUpdate(values);
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                swipeRefreshLayout.setEnabled(true);
                if (page == 1) swipeRefreshLayout.setRefreshing(false);
                else loading.setVisibility(View.GONE);
                if (listener != null) listener.onEndLoad(TipsTabFragment.this);
                isLoading = false;
                super.onPostExecute(aVoid);
            }
        }.execute();
    }

    void bindData(JsonArray jsonArray) {
        for (int i = 0; i < jsonArray.size(); i++) {
            JsonObject jsonObject = ConvertUtils.toJsonObject(jsonArray.get(i));
            int id = ConvertUtils.toInt(jsonObject.get("ID"));
            if (mapData.containsKey(id)) {
                int index = data.indexOf(mapData.get(id));
                mapData.put(id, jsonObject);
                if (index != -1) {
                    data.set(index, jsonObject);
//                    adapter.notifyDataSetChanged();
                    adapter.notifyItemChanged(index);
                }
            } else {
                mapData.put(id, jsonObject);
                data.add(jsonObject);
//                adapter.notifyDataSetChanged();
                adapter.notifyItemInserted(data.size() - 1);
            }
        }

        if (data.size() == 0) {
            noFoundResultLayout.setVisibility(View.VISIBLE);
            recyclerView.setVisibility(View.GONE);
        } else {
            noFoundResultLayout.setVisibility(View.GONE);
            recyclerView.setVisibility(View.VISIBLE);
        }
    }

    public class ViewHolder extends BaseViewHolder {
        @BindView(R.id.imageView)
        ImageView imageView;
        @BindView(R.id.tvTitle)
        TextView tvTitle;
        @BindView(R.id.tvSubContent)
        TextView tvSubContent;
        @BindView(R.id.tvTime)
        TextView tvTime;

        public ViewHolder(View itemView) {
            super(itemView);
        }
    }
}