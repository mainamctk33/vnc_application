package com.team500ae.vncapplication.activities.job.fragments;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.team500ae.library.adapters.BaseRecyclerAdapter;
import com.team500ae.library.layoutmanager.MyLinearLayoutManager;
import com.team500ae.library.utils.ConvertUtils;
import com.team500ae.library.utils.ImageUtils;
import com.team500ae.library.utils.RecyclerViewUtils;
import com.team500ae.vncapplication.R;
import com.team500ae.vncapplication.activities.job.DetailJobActivity;
import com.team500ae.vncapplication.activities.job.JobDetailActivity;
import com.team500ae.vncapplication.base.BaseViewHolder;
import com.team500ae.vncapplication.data_access.DataCacheInfo;
import com.team500ae.vncapplication.utils.AppUtils;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by SVIP on 11/30/2017.
 */

public class JobTabFragment extends Fragment {
    private int typeView = 1;
    private static final String TAG = JobTabFragment.class.getSimpleName();
    @BindView(R.id.layout_no_found_result)
    LinearLayout noFoundResultLayout;
    private int type;
    private BaseRecyclerAdapter<ViewHolder, JsonObject> adapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        typeView = ConvertUtils.toInt(DataCacheInfo.getData(getContext(), DataCacheInfo.EnumCacheType.Job_Small_List, ""), 0);
        Bundle bundle = getArguments();
        if (bundle != null)
            type = bundle.getInt("type");
        View view = inflater.inflate(R.layout.fragment_tab_job_fragment, container, false);
        ButterKnife.bind(this, view);

        recyclerView.setLayoutManager(new MyLinearLayoutManager(getContext()));

        RecyclerViewUtils recyclerViewUtils = new RecyclerViewUtils(new RecyclerViewUtils.RecyclerViewUtilsListener() {
            @Override
            public void onScrollToEnd() {
                if (!isLoading) {
                    getData(page + 1, size, type, getListener());
                }
            }
        });
        recyclerViewUtils.setUp(recyclerView);

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                page = 1;
                getData(page, size, type, getListener());
            }
        });
        Log.d(TAG, "onCreateView: ");
        adapter = new BaseRecyclerAdapter<ViewHolder, JsonObject>(typeView == 0 ? R.layout.item_job : R.layout.item_job_large, new BaseRecyclerAdapter.BaseViewHolder<ViewHolder, JsonObject>() {
            @Override
            public ViewHolder getViewHolder(View v) {
                return new ViewHolder(v);
            }

            @Override
            public void bindData(RecyclerView recyclerView, ViewHolder viewHolder, JsonObject data, int position) {
                JsonObject jsonObject = ConvertUtils.toJsonObject(data.get("Job"));
                viewHolder.tvTitle.setText(ConvertUtils.toString(jsonObject.get("Title")));
                viewHolder.tvDescription.setText(ConvertUtils.toString(jsonObject.get("Description")));
                long dateLong = ConvertUtils.toLong(jsonObject.get("CreatedDate"));
                long approveDate = ConvertUtils.toLong(jsonObject.get("ApproveDate"));
                if (approveDate == 0 || approveDate < 3661000)
                    viewHolder.tvTime.setText(getString(R.string.job_approval));
                else
                    viewHolder.tvTime.setText(com.team500ae.vncapplication.utils.TimeUtils.toSocialTime(getContext(), dateLong));
                viewHolder.tvLocationAndSalary.setText(ConvertUtils.toString(data.get("City")) + " - " + ConvertUtils.toString(jsonObject.get("Salary")));
                //ImageUtils.loadImageByGlide(getContext(), true, 200, 200, ConvertUtils.toString(jsonObject.get("Thumbnail")), R.drawable.img_no_image, R.drawable.img_no_image, viewHolder.imageView, true);
                int widthLarge = AppUtils.getScreenWidth();
                int heightLarge = viewHolder.imageView.getContext().getResources().getDimensionPixelSize(R.dimen._200dp);
                int width = viewHolder.imageView.getContext().getResources().getDimensionPixelSize(R.dimen.news_thumbnail_width);
                int height = viewHolder.imageView.getContext().getResources().getDimensionPixelSize(R.dimen.news_thumbnail_height);
                if (typeView == 0)
                    Glide.with(viewHolder.imageView).load(ConvertUtils.toString(jsonObject.get("Thumbnail"))).apply(new RequestOptions().override(width, height).placeholder(R.drawable.img_no_image)).into(viewHolder.imageView);
                else
                    Glide.with(viewHolder.imageView).load(ConvertUtils.toString(jsonObject.get("Thumbnail"))).apply(new RequestOptions().override(widthLarge, heightLarge).placeholder(R.drawable.img_no_image)).into(viewHolder.imageView);
                /*viewHolder.tvTitle.setText(ConvertUtils.toString(data.get("Title")));
                viewHolder.tvDescription.setText(ConvertUtils.toString(data.get("Description")));
                viewHolder.tvLocationAndSalary.setText(ConvertUtils.toString(data.get("City"))+ " - " + ConvertUtils.toString(data.get("Salary")));
                ImageUtils.loadImageByGlide(getContext(), true, 200, 200, ConvertUtils.toString(data.get("Thumbnail")), R.drawable.img_no_image, R.drawable.img_no_image, viewHolder.imageView, true);*/

            }
        }, data, new BaseRecyclerAdapter.OnClickListener() {
            @Override
            public void onClick(View v, int position, Object o) {
                JsonObject jsonObject = (JsonObject) o;
                JsonObject jsonObject2 = ConvertUtils.toJsonObject(jsonObject.get("Job"));
                int id = ConvertUtils.toInt(jsonObject2.get("ID"), 0);
//                JobDetailActivity.openChannel(getContext(), id);
                DetailJobActivity.open(getContext(), id);
                //String title = ConvertUtils.toString(jsonObject2.get("Title"));
                //JobDetailActivity.openChannel(getContext(), id, title);
            }
        });
        adapter.bindData(recyclerView);
        getData(page, size, type, getListener());
        return view;
    }

    public static JobTabFragment getInstance(int type) {
        JobTabFragment fragment = new JobTabFragment();
        Bundle bundle = new Bundle();
        bundle.putInt("type", type);
        fragment.setArguments(bundle);
        return fragment;
    }


    HashMap<Integer, JsonObject> mapData = new HashMap<>();
    ArrayList<JsonObject> data = new ArrayList<>();

    @BindView(R.id.swipeRefreshLayout)
    SwipeRefreshLayout swipeRefreshLayout;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.loading)
    View loading;
    int page = 1;
    int size = 10;
    boolean isLoading = false;

    public void getData() {
        page = 1;
        getData(page, size, type, getListener());
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    public interface LoadListener {
        public void onBeginLoad(Fragment fragment);

        public void onEndLoad(Fragment fragment);

        public JsonArray getData(int page, int size, int type, Fragment fragment);
    }

    public LoadListener getListener() {
        if (getContext() instanceof LoadListener)
            return (LoadListener) getContext();
        return null;
    }

    public class ViewHolder extends BaseViewHolder {
        @BindView(R.id.imageView)
        ImageView imageView;
        @BindView(R.id.tvTitle)
        TextView tvTitle;
        @BindView(R.id.tvLocationAndSalary)
        TextView tvLocationAndSalary;
        @BindView(R.id.tvTime)
        TextView tvTime;
        @BindView(R.id.tvDescription)
        TextView tvDescription;

        public ViewHolder(View itemView) {
            super(itemView);
        }
    }

    void bindData(JsonArray jsonArray) {
        //day la ham de tranh duplicate data
        //thanhtam.tr bua that. ko check doan nay nen chi binding duoc 1 phan tu len thoi
        //Vi no ko get duoc object job kia nen max cang luon
        for (int i = 0; i < jsonArray.size(); i++) {
            JsonObject jsonObject = ConvertUtils.toJsonObject(jsonArray.get(i));
            JsonObject jsonObject1 = ConvertUtils.toJsonObject(jsonObject.get("Job"));
            int id = ConvertUtils.toInt(jsonObject1.get("ID"));
            if (mapData.containsKey(id)) {
                int index = data.indexOf(mapData.get(id));
                mapData.put(id, jsonObject);
                if (index != -1) {
                    data.set(index, jsonObject);
                    adapter.notifyItemChanged(index);
                }
            } else {
                mapData.put(id, jsonObject);
                data.add(jsonObject);
                adapter.notifyItemInserted(data.size() - 1);
            }
        }
        if (data.size() == 0) {
            noFoundResultLayout.setVisibility(View.VISIBLE);
            recyclerView.setVisibility(View.GONE);
        } else {
            noFoundResultLayout.setVisibility(View.GONE);
            recyclerView.setVisibility(View.VISIBLE);
        }
    }

    public void getData(final int page, final int size, final int type, final LoadListener listener) {
        new AsyncTask<Void, JsonArray, Void>() {

            @Override
            protected void onPreExecute() {
                swipeRefreshLayout.setEnabled(false);
                if (page == 1)
                    swipeRefreshLayout.setRefreshing(true);
                else
                    loading.setVisibility(View.VISIBLE);
                if (listener != null)
                    listener.onBeginLoad(JobTabFragment.this);
                isLoading = true;
                super.onPreExecute();
            }

            @Override
            protected Void doInBackground(Void... voids) {

                if (listener != null) {
                    publishProgress(listener.getData(page, size, type, JobTabFragment.this));
                } else
                    publishProgress(new JsonArray());
                return null;
            }

            @Override
            protected void onProgressUpdate(JsonArray... values) {
                if (values.length > 0 && values[0] != null) {
                    if (page == 1) {
                        mapData.clear();
                        data.clear();
                        adapter.notifyDataSetChanged();
                    }

                    bindData(values[0]);
                }
                super.onProgressUpdate(values);
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                swipeRefreshLayout.setEnabled(true);
                if (page == 1)
                    swipeRefreshLayout.setRefreshing(false);
                else
                    loading.setVisibility(View.GONE);
                if (listener != null)
                    listener.onEndLoad(JobTabFragment.this);
                isLoading = false;
                super.onPostExecute(aVoid);
            }
        }.execute();

    }
}
