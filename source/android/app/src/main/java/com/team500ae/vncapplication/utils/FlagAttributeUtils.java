package com.team500ae.vncapplication.utils;

/**
 * Created by phuong.ndp on 01/07/2018.
 */

public class FlagAttributeUtils {
    //FEDCBA ~ Ex: 110100
    //A: DOWNLOAD_VIA_MOBILE_NETWORK
    //B: DOWNLOAD_VIA_WIFI
    private static final int MEDIA_SETTINGS_NONE_DEFAULT = 0;
    public static final int DOWNLOAD_VIA_MOBILE_NETWORK = 1;  //Thường đặt là 1<<0
    public static final int DOWNLOAD_VIA_WIFI = 2; // 1<<1
    public static final int SHOW_NEWS_NOTIFICATION = 4; // 1<<2
    public static final int SHOW_JOB_NOTIFICATION = 8; // 1<<3
    public static final int SHOW_FRIEND_NOTIFICATION = 16; // 1<<4
    public static final int SHOW_TIPS_NOTIFICATION = 32; // 1<<5
    public static final int SHARE_YOUR_LOCATION = 64; // 1<<6

    //Toggle flag: use XOR
    public static int changeAttribute(int flagSet, int flag) {
        return flagSet ^ flag;
    }

    public static boolean getAttribute(int flagSet, int flag) {
        return (flagSet | flag) == flagSet;
    }
}
