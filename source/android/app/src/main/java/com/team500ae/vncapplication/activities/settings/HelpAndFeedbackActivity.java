package com.team500ae.vncapplication.activities.settings;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.team500ae.vncapplication.R;
import com.team500ae.vncapplication.base.BaseActivity;
import com.team500ae.vncapplication.data_access.user.UserInfo;

import butterknife.ButterKnife;

/**
 * Created by Administrator on 19/05/2018.
 */

public class HelpAndFeedbackActivity extends BaseActivity {

    private View vAskQuestionLayout, vMyQuestionLayout, vCommonQuestionLayout;

    public static void open(Context activity) {
        Intent intent = new Intent(activity, HelpAndFeedbackActivity.class);
        activity.startActivity(intent);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        setContentView(R.layout.activity_help_and_feedback);
        ButterKnife.bind(this);
        super.onCreate(savedInstanceState);
        showBackButton();

        initLayout();

        vAskQuestionLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (UserInfo.isLogin())
                    AskQuestionActivity.open(getActivity());
                else
                    showSnackBar(getString(R.string.msg_have_not_login));
            }
        });

        vMyQuestionLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (UserInfo.isLogin())
                    MyQuestionActivity.open(getActivity());
                else
                    showSnackBar(getString(R.string.msg_have_not_login));
            }
        });

        vCommonQuestionLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CommonQuestionActivity.open(getActivity());
            }
        });
    }

    private void initLayout() {
        vAskQuestionLayout = findViewById(R.id.layout_ask_question);
        ImageView ivAskQuestionLeftIcon = (ImageView) vAskQuestionLayout.findViewById(R.id.iv_left_icon);
        ivAskQuestionLeftIcon.setImageResource(R.drawable.ic_send_question);
        TextView tvAskQuestionTitle = (TextView) vAskQuestionLayout.findViewById(R.id.tv_title);
        tvAskQuestionTitle.setText(getResources().getString(R.string.pref_info_ask_questions_title));
        tvAskQuestionTitle.setTextColor(getResources().getColor(R.color.colorPrimary));
        TextView tvAskQuestionSubTitle = (TextView) vAskQuestionLayout.findViewById(R.id.tv_sub_title);
        tvAskQuestionSubTitle.setText(getResources().getString(R.string.pref_info_ask_questions_summary));

        vMyQuestionLayout = findViewById(R.id.layout_my_question);
        ImageView ivMyQuestionLeftIcon = (ImageView) vMyQuestionLayout.findViewById(R.id.iv_left_icon);
        ivMyQuestionLeftIcon.setImageResource(R.drawable.ic_my_question);
        TextView tvMyQuestionTitle = (TextView) vMyQuestionLayout.findViewById(R.id.tv_title);
        tvMyQuestionTitle.setText(getResources().getString(R.string.pref_info_my_questions_title));
        TextView tvMyQuestionSubTitle = (TextView) vMyQuestionLayout.findViewById(R.id.tv_sub_title);
        tvMyQuestionSubTitle.setText(getResources().getString(R.string.pref_info_my_questions_summary));

        vCommonQuestionLayout = findViewById(R.id.layout_common_question);
        ImageView ivCommonQuestionLeftIcon = (ImageView) vCommonQuestionLayout.findViewById(R.id.iv_left_icon);
        ivCommonQuestionLeftIcon.setImageResource(R.drawable.ic_common_question);
        TextView tvCommonQuestionTitle = (TextView) vCommonQuestionLayout.findViewById(R.id.tv_title);
        tvCommonQuestionTitle.setText(getResources().getString(R.string.pref_info_common_questions_title));
        TextView tvCommonQuestionSubTitle = (TextView) vCommonQuestionLayout.findViewById(R.id.tv_sub_title);
        tvCommonQuestionSubTitle.setText(getResources().getString(R.string.pref_info_common_questions_summary));
    }
}
