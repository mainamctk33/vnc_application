package com.team500ae.vncapplication.activities.job.selectiondialog;


import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


import com.team500ae.vncapplication.R;
import com.team500ae.vncapplication.activities.job.SelectionModel;
import com.team500ae.vncapplication.activities.job.interfaces.ItemListener;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SelectionAdapter extends RecyclerView.Adapter<SelectionAdapter.SelectionSingleViewHolder> {

    private List<SelectionModel> data;
    private ItemListener listener;

    public SelectionAdapter(List<SelectionModel> data) {
        this.data = data;
    }

    public void setOnItemListener(ItemListener listener) {
        this.listener = listener;
    }

    @Override
    public SelectionSingleViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_selection, parent, false);
        return new SelectionSingleViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final SelectionSingleViewHolder holder, int position) {
        SelectionModel item = data.get(position);
        holder.tvTitle.setText(item.getValue());
        holder.itemView.setOnClickListener(view -> {
            if (listener != null) {
                listener.onItemClicked(holder.getAdapterPosition());
            }
        });
    }

    @Override
    public int getItemCount() {
        return data != null ? data.size() : 0;
    }

    static class SelectionSingleViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.itemSelection_tvTitle)
        TextView tvTitle;

        SelectionSingleViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}

