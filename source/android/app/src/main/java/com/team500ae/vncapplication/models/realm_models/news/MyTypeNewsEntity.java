package com.team500ae.vncapplication.models.realm_models.news;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by maina on 11/16/2017.
 */

public class MyTypeNewsEntity extends RealmObject {
    @PrimaryKey
    String id;
    String userId;
    TypeNewsEntity typeNews;
    long updatedDate;
    int position;

    public long getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(long updatedDate) {
        this.updatedDate = updatedDate;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public TypeNewsEntity getTypeNews() {
        return typeNews;
    }

    public void setTypeNews(TypeNewsEntity typeNews) {
        this.typeNews = typeNews;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }
}
