package com.team500ae.vncapplication.activities.tips;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.team500ae.library.adapters.BaseRecyclerAdapter;
import com.team500ae.library.layoutmanager.MyLinearLayoutManager;
import com.team500ae.vncapplication.R;
import com.team500ae.vncapplication.base.BaseActivity;
import com.team500ae.vncapplication.base.BaseViewHolder;
import com.team500ae.vncapplication.constants.Constants;
import com.team500ae.vncapplication.data_access.tips.TypeTipsInfo;
import com.team500ae.vncapplication.data_access.user.UserInfo;
import com.team500ae.vncapplication.models.BaseReturnFunctionEntity;
import com.team500ae.vncapplication.models.realm_models.tips.TypeTipsEntity;
import com.team500ae.vncapplication.utils.LanguageUtils;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by phuong.ndp on 11/27/2017.
 * done
 */

public class ChooseCategoryActivity extends BaseActivity {
    private BaseRecyclerAdapter<ViewHolder, TypeTipsEntity> adapter;
    private ArrayList<TypeTipsEntity> listData;
    private ArrayList<Integer> listSelect = new ArrayList<>();
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;

    public static void open(AppCompatActivity activity) {
        Intent intent = new Intent(activity, ChooseCategoryActivity.class);
        activity.startActivity(intent);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        setContentView(R.layout.activity_choose_category);
        super.onCreate(savedInstanceState);
        recyclerView.setLayoutManager(new MyLinearLayoutManager(getActivity()));
        getSupportActionBar().setTitle(R.string.add_category);
        listData = new ArrayList<>();
        adapter = new BaseRecyclerAdapter<ViewHolder, TypeTipsEntity>(R.layout.item_type_tips, new BaseRecyclerAdapter.BaseViewHolder<ViewHolder, TypeTipsEntity>() {
            @Override
            public ViewHolder getViewHolder(View v) {
                return new ViewHolder(v);
            }

            @Override
            public void bindData(RecyclerView recyclerView, ViewHolder viewHolder, TypeTipsEntity data, int position) {
                viewHolder.tvName.setText(LanguageUtils.getAppLanguageSetting(ChooseCategoryActivity.this).equals("en") ? data.getName() : data.getName2());
                if (listSelect.contains(data.getId())) {
                    viewHolder.tvSave.setText(getResources().getString(R.string.selected));
                    viewHolder.llSaveState.setSelected(true);
                    viewHolder.imgSave.setVisibility(View.VISIBLE);
                    viewHolder.tvSave.setTextColor(ContextCompat.getColor(viewHolder.tvName.getContext(), android.R.color.white));
                } else {
                    viewHolder.tvSave.setText(getResources().getString(R.string.not_selected));
                    viewHolder.llSaveState.setSelected(false);
                    viewHolder.imgSave.setVisibility(View.GONE);
                    viewHolder.tvSave.setTextColor(ContextCompat.getColor(viewHolder.tvName.getContext(), R.color.grey_500));
                }
            }
        }, listData, new BaseRecyclerAdapter.OnClickListener() {
            @Override
            public void onClick(View v, int position, Object o) {

            }
        });
        adapter.bindData(recyclerView);
        getData();
        showBackButton(true);
    }

    private void getData() {
        BaseReturnFunctionEntity<ArrayList<TypeTipsEntity>> listTypeTips = new TypeTipsInfo().getListTypeTips(getActivity(), getRealm());
        listData.clear();
        if (listTypeTips.isTrue()) {
            listData.addAll(listTypeTips.getData());
            listTypeTips = new TypeTipsInfo().getTypeTipsHasSelected(getActivity(), getRealm(), UserInfo.getCurrentUserId());
            if (listTypeTips.isTrue()) {
                for (TypeTipsEntity typeTipsEntity : listTypeTips.getData()) {
                    listData.remove(typeTipsEntity);
                }
            }
        }
        adapter.notifyDataSetChanged();
    }

    public class ViewHolder extends BaseViewHolder {
        @BindView(R.id.tvName)
        TextView tvName;
        @BindView(R.id.tvSave)
        TextView tvSave;
        @BindView(R.id.imgSave)
        ImageView imgSave;
        @BindView(R.id.llSaveState)
        LinearLayout llSaveState;

        @OnClick(R.id.llSaveState)
        void save() {
            try {
                int index = getAdapterPosition();
                int id = listData.get(index).getId();
                if (listSelect.contains(id)) {
                    BaseReturnFunctionEntity<Boolean> result = new TypeTipsInfo().removeTypeTipsFromListFavorite(getActivity(), getRealm(), UserInfo.getCurrentUserId(), listData.get(index).getId());
                    if (result.isTrue() && result.getData()) {
                        listSelect.remove(Integer.valueOf(id));
                        adapter.notifyItemChanged(index);
                    }
                } else {
                    BaseReturnFunctionEntity<Boolean> result = new TypeTipsInfo().saveTypeTipsToFavorite(getActivity(), getRealm(), UserInfo.getCurrentUserId(), listData.get(index).getId());
                    if (result.isTrue() && result.getData()) {
                        listSelect.add(id);
                        adapter.notifyItemChanged(index);
                    }
                }
                Intent intent = new Intent(Constants.ACTION_TYPE_TIPS_FOR_USER_CHANGED);
                sendBroadcast(intent);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        public ViewHolder(View itemView) {
            super(itemView);
        }
    }
}
