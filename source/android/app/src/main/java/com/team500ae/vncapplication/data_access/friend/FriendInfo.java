package com.team500ae.vncapplication.data_access.friend;

import android.content.Context;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.team500ae.library.utils.ConvertUtils;
import com.team500ae.vncapplication.constants.Constants;
import com.team500ae.vncapplication.data_access.ClientInfo;
import com.team500ae.vncapplication.models.BaseReturnFunctionEntity;
import com.team500ae.vncapplication.models.ServiceResponseEntity;

import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.util.ArrayList;

/**
 * Created by maina on 3/24/2018.
 */

public class FriendInfo {
    public ServiceResponseEntity<JsonArray> getAll(Context context, int page, int size, String keyword) {
        try {
            ClientInfo.DataResponse dataResponse = new ClientInfo().requestApi(ClientInfo.MethodType.GET, Constants.API_FRIEND_SEARCH + "?page=" + page + "&size=" + size + "&keyword=" + keyword);
            if (dataResponse.isOK()) {
                ServiceResponseEntity<JsonArray> result = dataResponse.getData(new TypeToken<ServiceResponseEntity<JsonArray>>() {
                }.getType());
                return result;
            }
        } catch (UnknownHostException e) {
            e.printStackTrace();
        } catch (SocketTimeoutException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static ServiceResponseEntity<JsonArray> getListFriendRequest() {
        try {
            ClientInfo.DataResponse dataResponse = new ClientInfo().requestApi(ClientInfo.MethodType.GET, Constants.API_FRIEND_GET_LIST_REQUEST_FRIEND);
            if (dataResponse.isOK()) {
                ServiceResponseEntity<JsonArray> result = dataResponse.getData(new TypeToken<ServiceResponseEntity<JsonArray>>() {
                }.getType());
                return result;
            }
        } catch (UnknownHostException e) {
            e.printStackTrace();
        } catch (SocketTimeoutException e) {
            e.printStackTrace();
        }
        return null;
    }


    public static String getFriendStatus(String friendId) {
        try {
            if (friendId != null)
                friendId = friendId.toLowerCase();

            ClientInfo.DataResponse response = new ClientInfo().requestApi(ClientInfo.MethodType.GET, Constants.API_FRIEND_GET_FRIEND_STATUS + "?toUsername=" + friendId);
            if (response.isOK()) {
                JsonObject jsonObject = response.getData(JsonObject.class);
                String message = ConvertUtils.toString(jsonObject.get("Message"));
                return message;
            }
            return Constants.STATUS_GET_FAIL;
        } catch (UnknownHostException e) {
            e.printStackTrace();
            return null;
        } catch (SocketTimeoutException e) {
            e.printStackTrace();
            return null;
        } catch (Exception e) {
            return "";
        }
    }

    public static BaseReturnFunctionEntity<Boolean> sendFriendRequest(String toUsername) {
        try {
            if (toUsername != null)
                toUsername = toUsername.toLowerCase();

            JsonObject jsonObject = new JsonObject();
            jsonObject.addProperty("toUsername", toUsername);

            ClientInfo.DataResponse dataResponse = new ClientInfo().requestApi(ClientInfo.MethodType.PUT, Constants.API_FRIEND_SEND_FRIEND_REQUEST, jsonObject);
            if (dataResponse.isOK()) {
                ServiceResponseEntity<Object> result = dataResponse.getData(new TypeToken<ServiceResponseEntity<Object>>() {
                }.getType());
                if (result.getStatus() == 0)
                    return new BaseReturnFunctionEntity<>(BaseReturnFunctionEntity.StatusFunction.TRUE, true);
            }
            return new BaseReturnFunctionEntity<>(BaseReturnFunctionEntity.StatusFunction.FALSE);
        } catch (UnknownHostException e) {
            e.printStackTrace();
            return new BaseReturnFunctionEntity<>(BaseReturnFunctionEntity.StatusFunction.EXCEPTION, e.getMessage());
        } catch (SocketTimeoutException e) {
            e.printStackTrace();
            return new BaseReturnFunctionEntity<>(BaseReturnFunctionEntity.StatusFunction.EXCEPTION, e.getMessage());
        }
    }

    public static BaseReturnFunctionEntity<Boolean> unFriend(String toUsername) {
        try {
            if (toUsername != null)
                toUsername = toUsername.toLowerCase();

            JsonObject jsonObject = new JsonObject();
            jsonObject.addProperty("toUsername", toUsername);

            ClientInfo.DataResponse dataResponse = new ClientInfo().requestApi(ClientInfo.MethodType.PUT, Constants.API_FRIEND_UNFRIEND, jsonObject);
            if (dataResponse.isOK()) {
                ServiceResponseEntity<Object> result = dataResponse.getData(new TypeToken<ServiceResponseEntity<Object>>() {
                }.getType());
                if (result.getStatus() == 0)
                    return new BaseReturnFunctionEntity<>(BaseReturnFunctionEntity.StatusFunction.TRUE, true);
            }
            return new BaseReturnFunctionEntity<>(BaseReturnFunctionEntity.StatusFunction.FALSE);
        } catch (UnknownHostException e) {
            e.printStackTrace();
            return new BaseReturnFunctionEntity<>(BaseReturnFunctionEntity.StatusFunction.EXCEPTION, e.getMessage());
        } catch (SocketTimeoutException e) {
            e.printStackTrace();
            return new BaseReturnFunctionEntity<>(BaseReturnFunctionEntity.StatusFunction.EXCEPTION, e.getMessage());
        }
    }

    public static BaseReturnFunctionEntity<Boolean> cancelFriendRequest(String toUsername) {
        try {
            if (toUsername != null)
                toUsername = toUsername.toLowerCase();

            JsonObject jsonObject = new JsonObject();
            jsonObject.addProperty("toUsername", toUsername);

            ClientInfo.DataResponse dataResponse = new ClientInfo().requestApi(ClientInfo.MethodType.PUT, Constants.API_FRIEND_CANCEL_FRIEND_REQUEST, jsonObject);
            if (dataResponse.isOK()) {
                ServiceResponseEntity<Object> result = dataResponse.getData(new TypeToken<ServiceResponseEntity<Object>>() {
                }.getType());
                if (result.getStatus() == 0)
                    return new BaseReturnFunctionEntity<>(BaseReturnFunctionEntity.StatusFunction.TRUE, true);
            }
            return new BaseReturnFunctionEntity<>(BaseReturnFunctionEntity.StatusFunction.FALSE);
        } catch (UnknownHostException e) {
            e.printStackTrace();
            return new BaseReturnFunctionEntity<>(BaseReturnFunctionEntity.StatusFunction.EXCEPTION, e.getMessage());
        } catch (SocketTimeoutException e) {
            e.printStackTrace();
            return new BaseReturnFunctionEntity<>(BaseReturnFunctionEntity.StatusFunction.EXCEPTION, e.getMessage());
        }
    }

    public static BaseReturnFunctionEntity<Boolean> deleteFriendRequest(String requester) {
        try {
            if (requester != null)
                requester = requester.toLowerCase();

            JsonObject jsonObject = new JsonObject();
            jsonObject.addProperty("requester", requester);

            ClientInfo.DataResponse dataResponse = new ClientInfo().requestApi(ClientInfo.MethodType.PUT, Constants.API_FRIEND_DELETE_FRIEND_REQUEST, jsonObject);
            if (dataResponse.isOK()) {
                ServiceResponseEntity<Object> result = dataResponse.getData(new TypeToken<ServiceResponseEntity<Object>>() {
                }.getType());
                if (result.getStatus() == 0)
                    return new BaseReturnFunctionEntity<>(BaseReturnFunctionEntity.StatusFunction.TRUE, true);
            }
            return new BaseReturnFunctionEntity<>(BaseReturnFunctionEntity.StatusFunction.FALSE);
        } catch (UnknownHostException e) {
            e.printStackTrace();
            return new BaseReturnFunctionEntity<>(BaseReturnFunctionEntity.StatusFunction.EXCEPTION, e.getMessage());
        } catch (SocketTimeoutException e) {
            e.printStackTrace();
            return new BaseReturnFunctionEntity<>(BaseReturnFunctionEntity.StatusFunction.EXCEPTION, e.getMessage());
        }
    }

    public static BaseReturnFunctionEntity<Boolean> acceptFriendRequest(String toUsername) {
        try {
            if (toUsername != null)
                toUsername = toUsername.toLowerCase();

            JsonObject jsonObject = new JsonObject();
            jsonObject.addProperty("toUsername", toUsername);

            ClientInfo.DataResponse dataResponse = new ClientInfo().requestApi(ClientInfo.MethodType.PUT, Constants.API_FRIEND_ACCEPT_FRIEND_REQUEST, jsonObject);
            if (dataResponse.isOK()) {
                ServiceResponseEntity<Object> result = dataResponse.getData(new TypeToken<ServiceResponseEntity<Object>>() {
                }.getType());
                if (result.getStatus() == 0)
                    return new BaseReturnFunctionEntity<>(BaseReturnFunctionEntity.StatusFunction.TRUE, true);
            }
            return new BaseReturnFunctionEntity<>(BaseReturnFunctionEntity.StatusFunction.FALSE);
        } catch (UnknownHostException e) {
            e.printStackTrace();
            return new BaseReturnFunctionEntity<>(BaseReturnFunctionEntity.StatusFunction.EXCEPTION, e.getMessage());
        } catch (SocketTimeoutException e) {
            e.printStackTrace();
            return new BaseReturnFunctionEntity<>(BaseReturnFunctionEntity.StatusFunction.EXCEPTION, e.getMessage());
        }
    }

    public ServiceResponseEntity<JsonArray> getAllRequest(Context context, int page, int size, String keyword) {
        try {
            ClientInfo.DataResponse dataResponse = new ClientInfo().requestApi(ClientInfo.MethodType.GET, Constants.API_FRIEND_GET_LIST_REQUEST_FRIEND + "?page=" + page + "&size=" + size + "&keyword=" + keyword);
            if (dataResponse.isOK()) {
                ServiceResponseEntity<JsonArray> result = dataResponse.getData(new TypeToken<ServiceResponseEntity<JsonArray>>() {
                }.getType());
                return result;
            }
        } catch (UnknownHostException e) {
            e.printStackTrace();
        } catch (SocketTimeoutException e) {
            e.printStackTrace();
        }
        return null;
    }
}
