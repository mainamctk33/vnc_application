package com.team500ae.vncapplication.activities.login;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.InputFilter;
import android.text.InputType;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.team500ae.library.utils.ConvertUtils;
import com.team500ae.vncapplication.R;
import com.team500ae.vncapplication.base.BaseActivity;
import com.team500ae.vncapplication.constants.Constants;
import com.team500ae.vncapplication.constants.TimeOutConstants;
import com.team500ae.vncapplication.data_access.user.UserInfo;
import com.team500ae.vncapplication.models.BaseReturnFunctionEntity;
import com.team500ae.vncapplication.utils.KeyboardUtils;
import com.team500ae.vncapplication.utils.SecurityUtils;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ChangePasswordActivity extends BaseActivity {

    @BindView(R.id.btn_change_password)
    TextView btnChangePassword;
    @BindView(R.id.changePassword_layout)
    LinearLayout changePasswordLayout;

    private EditText edtOldPasswordContent, edtNewPasswordContent, edtConfirmPasswordContent;

    public static void open(Context activity) {
        Intent intent = new Intent(activity, ChangePasswordActivity.class);
        activity.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setContentView(R.layout.activity_change_password);
        ButterKnife.bind(this);
        KeyboardUtils.setupUI(changePasswordLayout, getActivity());
        super.onCreate(savedInstanceState);

        getSupportActionBar().setTitle(R.string.pref_account_change_password_title);
        showBackButton();

        initLayout();
    }

    private void initLayout() {

        View vOldPasswordInclude = findViewById(R.id.layout_old_password);
        ImageView ivOldPasswordLeftIcon = (ImageView) vOldPasswordInclude.findViewById(R.id.image_left);
        ivOldPasswordLeftIcon.setImageResource(R.drawable.ic_password);
        TextView tvOldPasswordTitle = (TextView) vOldPasswordInclude.findViewById(R.id.text_title);
        tvOldPasswordTitle.setText(getResources().getString(R.string.old_passowrd));
        edtOldPasswordContent = (EditText) vOldPasswordInclude.findViewById(R.id.text_content);
        edtOldPasswordContent.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
        edtOldPasswordContent.setFilters(new InputFilter[]{new InputFilter.LengthFilter(Constants.MAX_FULL_NAME_LENGTH)});
        ImageView ivOldPasswordRightIcon = (ImageView) vOldPasswordInclude.findViewById(R.id.image_right);
        ivOldPasswordRightIcon.setVisibility(View.GONE);

        View vNewPasswordInclude = findViewById(R.id.layout_new_password);
        ImageView ivNewPasswordLeftIcon = (ImageView) vNewPasswordInclude.findViewById(R.id.image_left);
        ivNewPasswordLeftIcon.setImageResource(R.drawable.ic_password);
        TextView tvNewPasswordTitle = (TextView) vNewPasswordInclude.findViewById(R.id.text_title);
        tvNewPasswordTitle.setText(getResources().getString(R.string.new_passowrd));
        edtNewPasswordContent = (EditText) vNewPasswordInclude.findViewById(R.id.text_content);
        edtNewPasswordContent.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
        edtNewPasswordContent.setFilters(new InputFilter[]{new InputFilter.LengthFilter(Constants.MAX_FULL_NAME_LENGTH)});
        ImageView ivNewPasswordRightIcon = (ImageView) vNewPasswordInclude.findViewById(R.id.image_right);
        ivNewPasswordRightIcon.setVisibility(View.GONE);

        View vConfirmPasswordInclude = findViewById(R.id.layout_confirm_password);
        ImageView ivConfirmPasswordLeftIcon = (ImageView) vConfirmPasswordInclude.findViewById(R.id.image_left);
        ivConfirmPasswordLeftIcon.setImageResource(R.drawable.ic_confirm_password);
        TextView tvConfirmPasswordTitle = (TextView) vConfirmPasswordInclude.findViewById(R.id.text_title);
        tvConfirmPasswordTitle.setText(getResources().getString(R.string.confirmPassword));
        edtConfirmPasswordContent = (EditText) vConfirmPasswordInclude.findViewById(R.id.text_content);
        edtConfirmPasswordContent.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
        edtConfirmPasswordContent.setFilters(new InputFilter[]{new InputFilter.LengthFilter(Constants.MAX_FULL_NAME_LENGTH)});
        ImageView ivConfirmPasswordRightIcon = (ImageView) vConfirmPasswordInclude.findViewById(R.id.image_right);
        ivConfirmPasswordRightIcon.setVisibility(View.GONE);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @OnClick(R.id.btn_change_password)
    void changePassword() {
        edtOldPasswordContent.setError(null);
        edtNewPasswordContent.setError(null);
        edtConfirmPasswordContent.setError(null);

        String currentPassword = edtOldPasswordContent.getText().toString();
        String rawPassword = edtNewPasswordContent.getText().toString();
        String confirmPassword = edtConfirmPasswordContent.getText().toString();

        if (rawPassword.length() < 4) {
            edtNewPasswordContent.setError(getString(R.string.activity_register_error_password_length));
            edtNewPasswordContent.requestFocus();
            return;
        }

        if (!rawPassword.equals(confirmPassword)) {
            edtConfirmPasswordContent.setError(getString(R.string.activity_register_error_password_no_match));
            edtConfirmPasswordContent.requestFocus();
            return;
        }

        String currentPasswordEncrypt = SecurityUtils.encryptMD5(currentPassword);
        if (!currentPasswordEncrypt.equals(ConvertUtils.toString(UserInfo.getCurrentUser().get("Password")))) {
            edtOldPasswordContent.setError(getString(R.string.activity_update_account_wrong_password));
            edtOldPasswordContent.requestFocus();
            return;
        }

        //encrypt password by MD5
        String password = SecurityUtils.encryptMD5(rawPassword);
        new AsyncTask<Void, Void, BaseReturnFunctionEntity<Boolean>>() {
            ProgressDialog progDialog = new ProgressDialog(ChangePasswordActivity.this);

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                progDialog.setMessage(getString(R.string.activity_change_password_changing));
                progDialog.setIndeterminate(false);
                progDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                progDialog.setCancelable(true);
                progDialog.show();
            }

            @Override
            protected BaseReturnFunctionEntity<Boolean> doInBackground(Void... voids) {
                return UserInfo.changePassword(getActivity(), password, TimeOutConstants.TIMEOUT_ACCOUNT_REGISTER);
            }

            @Override
            protected void onPostExecute(BaseReturnFunctionEntity<Boolean> result) {
                super.onPostExecute(result);
                try {
                    if (progDialog.isShowing()) {
                        progDialog.dismiss();
                    }
                }catch (Exception e)
                {
                    e.printStackTrace();
                }


                if (result.isTrue()) {
                    onBackPressed();
                    showSnackBar(R.string.activity_change_password_successful);
                    Toast.makeText(getBaseContext(), R.string.activity_change_password_successful, Toast.LENGTH_SHORT).show();
                } else {
                    showSnackBar(R.string.activity_change_password_failed);
                    Toast.makeText(getBaseContext(), R.string.activity_change_password_failed, Toast.LENGTH_SHORT).show();
                }
            }
        }.execute();
    }
}
