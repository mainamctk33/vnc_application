package com.team500ae.vncapplication.activities.friend.fragments;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.UiThread;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStates;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.location.places.GeoDataClient;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.PlaceDetectionClient;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.team500ae.library.adapters.BaseRecyclerAdapter;
import com.team500ae.library.layoutmanager.MyLinearLayoutManager;
import com.team500ae.library.utils.AndroidPermissionUtils;
import com.team500ae.library.utils.ConvertUtils;
import com.team500ae.library.utils.ImageUtils;
import com.team500ae.library.utils.SharedPreferencesUtil;
import com.team500ae.vncapplication.R;
import com.team500ae.vncapplication.activities.chat.GroupChannelActivity;
import com.team500ae.vncapplication.activities.friend.AdvanceSearchActivity;
import com.team500ae.vncapplication.activities.friend.SearchByNameActivity;
import com.team500ae.vncapplication.activities.user.UserProfileActivity;
import com.team500ae.vncapplication.base.BaseActivity;
import com.team500ae.vncapplication.base.BaseViewHolder;
import com.team500ae.vncapplication.constants.Constants;
import com.team500ae.vncapplication.data_access.ClientInfo;
import com.team500ae.vncapplication.data_access.DataCacheInfo;
import com.team500ae.vncapplication.data_access.friend.FriendInfo;
import com.team500ae.vncapplication.data_access.user.UserInfo;
import com.team500ae.vncapplication.services.ServiceLocation;
import com.team500ae.vncapplication.utils.GPSTrackerUtils;
import com.team500ae.vncapplication.utils.KeyboardUtils;
import com.team500ae.vncapplication.utils.StringUtils;

import java.io.IOException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.team500ae.library.activities.BaseActivity.requestPermission;
import static com.team500ae.library.activities.BaseActivity.showSnackBar;

public class FriendAroundFragment extends Fragment implements OnMapReadyCallback, GoogleMap.OnMarkerClickListener, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {
    public static String TAG = FriendAroundFragment.class.getSimpleName();

    private static final float DEFAULT_ZOOM = 12f;
    protected static final int REQUEST_CHECK_SETTINGS = 0x1;
    private static final int REQ_CODE_PLACE_PICKER = 10006;
    private static final int REQ_CHANGE_SEARCH_OPTION = 10007;
    private FusedLocationProviderClient mFusedLocationProviderClient;
    private GeoDataClient mGeoDataClient;
    private PlaceDetectionClient mPlaceDetectionClient;
    private GoogleMap mMap;
    private LatLng mLastKnownLocation;
    private GoogleApiClient mGoogleApiClient;
    private ArrayList<JsonObject> listObject;
    private BaseRecyclerAdapter<ViewHolder, JsonObject> adapter;
    private View mapView;
    private Place place;
    Marker locationMarker = null;
    int gender = 0;
    int range_age = 0;
    boolean only_friend = false;
    private String maxDistance;
    private int page = 1;
    private int size = 50;
    private View bottomSheet;
    private SupportMapFragment mapFragment;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_friend_around, container, false);
        ButterKnife.bind(this, view);
        KeyboardUtils.setupUI(view, getActivity());
        gender = SharedPreferencesUtil.getInt(getContext(), Constants.FILTER_GENDER, 0);
        range_age = SharedPreferencesUtil.getInt(getContext(), Constants.FILTER_AGE, 0);
        maxDistance = SharedPreferencesUtil.getString(getContext(), Constants.MAX_DISTANCE, "");
        if (maxDistance.equals(""))
            maxDistance = "0";
        if (UserInfo.isLogin()) {
            only_friend = SharedPreferencesUtil.getBoolean(getContext(), Constants.ONLY_FRIEND, false);
        } else {
            only_friend = false;
        }
        listObject = new ArrayList<>();
        recyclerView.setLayoutManager(new MyLinearLayoutManager(getActivity()));
        adapter = new BaseRecyclerAdapter<ViewHolder, JsonObject>(R.layout.item_search_friend, new BaseRecyclerAdapter.BaseViewHolder<ViewHolder, JsonObject>() {
            @Override
            public ViewHolder getViewHolder(View v) {
                return new ViewHolder(v);
            }

            @Override
            public void bindData(RecyclerView recyclerView, ViewHolder viewHolder, JsonObject data, int position) throws IOException {
                JsonObject jsonObject = ConvertUtils.toJsonObject(data.get("User"));

                //show avatar or not
                if (ConvertUtils.toBoolean(jsonObject.get("ShowAvatar"))) {
                    ImageUtils.loadImageByGlide(getActivity(), false, 0, 0, ConvertUtils.toString(jsonObject.get("Avartar")), R.drawable.img_no_image, R.drawable.img_no_image, viewHolder.imageView, true);
                } else {
                    viewHolder.imageView.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.default_avatar));
                }
//                viewHolder.tvName.setText(ConvertUtils.toString(jsonObject.get("FullName"))+"|"+ConvertUtils.toBoolean(jsonObject.get("ShowAvatar")));
                viewHolder.tvName.setText(ConvertUtils.toString(jsonObject.get("FullName")));
//                String bod = ConvertUtils.toString(jsonObject.get("Birthday"));
//                if (StringUtils.isNullOrWhiteSpace(bod)) {
//                    viewHolder.lnAge.setVisibility(View.GONE);
//                } else {
//                    viewHolder.lnAge.setVisibility(View.VISIBLE);
//                    long dou = ConvertUtils.toLong(bod);
//                    viewHolder.tvAge.setText(String.valueOf(TimeUtils.getAge(dou)));
//                }
                String phone = ConvertUtils.toString(jsonObject.get("PhoneNumber"));
                if (!StringUtils.isNullOrWhiteSpace(phone) && ConvertUtils.toBoolean(jsonObject.get("ShowPhoneNumber"))) {
                    viewHolder.btnCall.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.ic_call));
                } else {
                    viewHolder.btnCall.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.ic_call_dim));
                }
                viewHolder.btnCall.setTag(phone);
                if (ConvertUtils.toBoolean(jsonObject.get("isFriend")) || ConvertUtils.toBoolean(jsonObject.get("ReceiveStrangeMessage"))) {
                    viewHolder.btnMailBox.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.ic_mailbox));
                    String userName = ConvertUtils.toString(jsonObject.get("UserName"));
                    viewHolder.btnMailBox.setTag(userName);
                } else {
                    viewHolder.btnMailBox.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.ic_mailbox_dim));
                    viewHolder.btnMailBox.setTag("");
                }
                int gender = ConvertUtils.toInt(jsonObject.get("Gender"));
                if (gender == 0) {
                    viewHolder.imgGender.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.ic_female));
                } else {
                    viewHolder.imgGender.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.ic_male));
                }

                double distance = ConvertUtils.toDouble(data.get("Distance")) / 1000;
                String _distance = ConvertUtils.toString(data.get("_Distance"));
                if (StringUtils.isNullOrWhiteSpace(_distance)) {
                    DecimalFormat df = new DecimalFormat("#.##");

                    String t = "km";
                    if (distance < 1) {
                        t = "m";
                        distance *= 1000;
                        df = new DecimalFormat("#");
                    }
                    _distance = String.valueOf(df.format(distance)) + " " + t;
                    data.addProperty("_Distance", _distance);
                    viewHolder.tvDistance.setText(Html.fromHtml(_distance));
                } else {
                    viewHolder.tvDistance.setText(Html.fromHtml(_distance));
                }
                String address = ConvertUtils.toString(jsonObject.get("Address"));
                if ("".equals(address)) {
                    Geocoder geocoder;

                    geocoder = new Geocoder(getContext(), Locale.getDefault());

                    List<Address> addresses = geocoder.getFromLocation(ConvertUtils.toDouble(jsonObject.get("Lat")), ConvertUtils.toDouble(jsonObject.get("Lon")), 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5
                    address = "";
                    if (addresses.size() != 0) {
                        Address _address = addresses.get(0);
                        if (!StringUtils.isNullOrWhiteSpace(_address.getLocality())) {
                            address += _address.getLocality();
                        }
                        if (!StringUtils.isNullOrWhiteSpace(_address.getSubAdminArea())) {
                            if (!address.isEmpty()) {
                                address += ", ";
                            }
                            address += _address.getSubAdminArea();
                        }
                        if (!StringUtils.isNullOrWhiteSpace(_address.getAdminArea())) {
                            if (!address.isEmpty()) {
                                address += ", ";
                            }
                            address += _address.getAdminArea();
                        }
                        if (!StringUtils.isNullOrWhiteSpace(_address.getCountryName())) {
                            if (!address.isEmpty()) {
                                address += ", ";
                            }
                            address += _address.getCountryName();
                        }
                        jsonObject.addProperty("Address", address);
                    }
                }
                if (!("".equals(address))) {
                    viewHolder.tvAddress.setVisibility(View.VISIBLE);
                    viewHolder.tvAddress.setText(address);
                } else {
                    viewHolder.tvAddress.setVisibility(View.GONE);
                }

            }
        }, listObject, new BaseRecyclerAdapter.OnClickListener() {
            @Override
            public void onClick(View v, int position, Object o) {
                JsonObject jsonObject = (JsonObject) o;
                jsonObject = ConvertUtils.toJsonObject(jsonObject.get("User"));
                UserProfileActivity.open(getContext(), ConvertUtils.toString(jsonObject.get("UserName")));
            }
        });
        adapter.bindData(recyclerView);

        mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);
        mapView = mapFragment.getView();

        mapFragment.getMapAsync(this);
        mGoogleApiClient = new GoogleApiClient.Builder(getActivity())
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();
        mGoogleApiClient.connect();


        // Construct a GeoDataClient.
        mGeoDataClient = Places.getGeoDataClient(getContext(), null);

        // Construct a PlaceDetectionClient.
        mPlaceDetectionClient = Places.getPlaceDetectionClient(getContext(), null);

        // Construct a FusedLocationProviderClient.
        mFusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(getContext());


        bottomSheet = coordinatorLayout.findViewById(R.id.bottom_sheet);
        final BottomSheetBehavior behavior = BottomSheetBehavior.from(bottomSheet);
        behavior.setState(BottomSheetBehavior.STATE_EXPANDED);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                behavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
            }
        }, 1200);
        behavior.setPeekHeight(getResources().getDimensionPixelSize(R.dimen.list_user_height));
        behavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {
                if (behavior.getState() == BottomSheetBehavior.STATE_COLLAPSED) {
                    tvList.setVisibility(View.VISIBLE);
                } else {
                    tvList.setVisibility(View.GONE);
                }
            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {
                tvList.setVisibility(View.VISIBLE);
            }
        });
        return view;
    }

    @BindView(R.id.tvList)
    View tvList;
    @BindView(R.id.lnMap)
    View lnMap;

    @BindView(R.id.recyclerUser)
    RecyclerView recyclerView;
    //    @BindView(R.id.mergedappbarlayout)
//    MergedAppBarLayout mergedAppBarLayout;
    @BindView(R.id.coordinatorlayout)
    CoordinatorLayout coordinatorLayout;

    @OnClick(R.id.btnOption)
    void option() {
//        Intent intent = new Intent(getContext(), SearchOptionActivity.class);
        Intent intent = new Intent(getContext(), AdvanceSearchActivity.class);
        startActivityForResult(intent, REQ_CHANGE_SEARCH_OPTION);
    }


    //    @OnClick(R.id.btnSelectLocation)
    void selectLocation() {
        PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();
        try {
            startActivityForResult(builder.build(getActivity()), REQ_CODE_PLACE_PICKER);
        } catch (GooglePlayServicesRepairableException e) {
            BaseActivity.showSnackBar(getContext(), "Bạn cần cài đặt google playservice để sử dụng tính năng này");
            e.printStackTrace();
        } catch (GooglePlayServicesNotAvailableException e) {
            BaseActivity.showSnackBar(getContext(), "Bạn cần cài đặt google playservice để sử dụng tính năng này");
            e.printStackTrace();
        }
    }

    void showBottomSheet(boolean show) {
        if (bottomSheet != null)
            if (show) {
                bottomSheet.setVisibility(View.VISIBLE);
                lnMap.setPadding(0, 0, 0, getResources().getDimensionPixelOffset(R.dimen._200dp));
            } else {
                bottomSheet.setVisibility(View.GONE);
                lnMap.setPadding(0, 0, 0, 0);
            }
    }

    @Override
    public void onMapReady(GoogleMap map) {
        mMap = map;
        mMap.setOnMarkerClickListener(this);
        View locationButton = ((View) mapView.findViewById(Integer.parseInt("1")).getParent()).findViewById(Integer.parseInt("2"));
        // and next place it, on bottom right (as Google Maps app)
        RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams)
                locationButton.getLayoutParams();
        // position on right bottom
        layoutParams.setMargins(0, getResources().getDimensionPixelOffset(R.dimen.marginTopMyLocation), 30, 0);

        JsonArray jsonArray = ConvertUtils.toJsonArray(DataCacheInfo.getData(getContext(), DataCacheInfo.EnumCacheType.NearUser));
        showPlace(jsonArray);
        showBottomSheet(jsonArray.size() > 0);

        // Do other setup activities here too, as described elsewhere in this tutorial.

        // Turn on the My Location layer and the related control on the map.
        updateLocationUI();

        // Get the current location of the device and set the position of the map.
        BaseActivity.requestPermission(getContext(), new AndroidPermissionUtils.OnCallbackRequestPermission() {

            @Override
            public void onSuccess() {
                LocationManager manager = (LocationManager) getContext().getSystemService(Context.LOCATION_SERVICE);
                if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                    settingsRequest();
                    updateLocationUI();
                } else {
                    getDeviceLocation();
                }
            }

            @Override
            public void onFailed() {

            }
        }, AndroidPermissionUtils.TypePermission.PERMISSION_ACCESS_COARSE_LOCATION, AndroidPermissionUtils.TypePermission.PERMISSION_ACCESS_COARSE_LOCATION);

    }

    public void settingsRequest() {
        LocationRequest locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(30 * 1000);
        locationRequest.setFastestInterval(5 * 1000);
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(locationRequest);
        builder.setAlwaysShow(true); //this is the key ingredient
        PendingResult<LocationSettingsResult> result =
                LocationServices.SettingsApi.checkLocationSettings(mGoogleApiClient, builder.build());
        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(LocationSettingsResult result) {
                final Status status = result.getStatus();
                final LocationSettingsStates state = result.getLocationSettingsStates();
                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.SUCCESS:
                        try {
                            status.startResolutionForResult(getActivity(), REQUEST_CHECK_SETTINGS);
                        } catch (IntentSender.SendIntentException e) {
                        }
                        break;
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        try {
                            status.startResolutionForResult(getActivity(), REQUEST_CHECK_SETTINGS);
                        } catch (IntentSender.SendIntentException e) {
                            e.printStackTrace();
                        }
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        break;
                }
            }
        });
    }

    private void getDeviceLocation() {
        /*
         * Get the best and most recent location of the device, which may be null in rare
         * cases when a location is not available.
         */
        try {
            Task locationResult = mFusedLocationProviderClient.getLastLocation();
            locationResult.addOnCompleteListener(getActivity(), new OnCompleteListener() {
                @Override
                public void onComplete(@NonNull Task task) {
                    if (task.isSuccessful()) {
                        // Set the map's camera position to the current location of the device.
                        Location location = (Location) task.getResult();
                        if (location != null) {
                            mLastKnownLocation = new LatLng(location.getLatitude(), location.getLongitude());
                            if (mLastKnownLocation != null) {
                                GPSTrackerUtils.saveCurrentLocation(getContext(), mLastKnownLocation.latitude, mLastKnownLocation.longitude);
                                ServiceLocation.startActionUpdateLocation(getContext(), mLastKnownLocation.latitude, mLastKnownLocation.longitude);
                                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(
                                        mLastKnownLocation, DEFAULT_ZOOM));
                                loadListUser(mLastKnownLocation.latitude, mLastKnownLocation.longitude, 1, size);
                                showLocationMarker(mLastKnownLocation.latitude, mLastKnownLocation.longitude);

                            }
                        }
                    } else {
                        mMap.getUiSettings().setMyLocationButtonEnabled(false);
                    }
                }
            });
        } catch (SecurityException e) {
        }
    }

    private void showLocationMarker(double lat, double lon) {
        if (locationMarker != null)
            locationMarker.remove();
        MarkerOptions markerOptions = new MarkerOptions();
        LatLng latLng = new LatLng(lat, lon);
        markerOptions.position(latLng);
        locationMarker = mMap.addMarker(markerOptions);
        BitmapDescriptor markerIcon = getMarkerIconFromDrawable(Objects.requireNonNull(ContextCompat.getDrawable(getActivity(), R.drawable.ic_location_picked)));
        locationMarker.setIcon(markerIcon);
    }

    private void loadListUser(double lat, double lon, int page, int size) {
        this.page = page;
        new AsyncTask<Void, Void, ClientInfo.DataResponse>() {
            @Override
            protected ClientInfo.DataResponse doInBackground(Void... voids) {
                try {
                    return new ClientInfo().requestApi(ClientInfo.MethodType.GET, getUrl(lat, lon, 1, 50, range_age, gender, maxDistance, only_friend));
                } catch (UnknownHostException e) {
                    e.printStackTrace();
                } catch (SocketTimeoutException e) {
                    e.printStackTrace();
                }
                return null;
            }

            @Override
            protected void onPostExecute(ClientInfo.DataResponse dataResponse) {
                if (dataResponse != null && dataResponse.isOK()) {
                    JsonObject jsonObject = dataResponse.getData(JsonObject.class);
                    if (ConvertUtils.toBoolean(jsonObject.get("IsTrue"))) {
                        JsonArray jsonArray = ConvertUtils.toJsonArray(jsonObject.get("Data"));
                        DataCacheInfo.setData(getContext(), DataCacheInfo.EnumCacheType.NearUser, jsonArray);
                        if (page == 1) {
                            try {
                                mMap.clear();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            listObject.clear();
                            adapter.notifyDataSetChanged();
                        }
                        showLocationMarker(lat, lon);
                        showPlace(jsonArray);
                        showBottomSheet(jsonArray.size() != 0);
                        if (jsonArray.size() == 0)
                            showSnackBar(getContext(), getResources().getString(R.string.search_result_is_empty));
                    } else {
                        if (page == 1) {
                            mMap.clear();
                            listObject.clear();
                            adapter.notifyDataSetChanged();
                        }
                        showPlace(new JsonArray());
                    }
                }
                super.onPostExecute(dataResponse);
            }
        }.execute();
    }

    private String getUrl(double latitude, double longitude, int page, int size, int range_age, int gender, String max_distance, boolean only_friend) {
        StringBuilder googlePlacesUrl = new StringBuilder("/user/FindFriendAround?");
        googlePlacesUrl.append("&latitude=" + latitude);
        googlePlacesUrl.append("&longitude=" + longitude);
        googlePlacesUrl.append("&range_age=" + range_age);
        googlePlacesUrl.append("&gender=" + gender);
        googlePlacesUrl.append("&max_distance=" + max_distance);
        googlePlacesUrl.append("&only_friend=" + (only_friend ? 1 : 0));
        googlePlacesUrl.append("&page=" + page);
        googlePlacesUrl.append("&size=" + size);
        return (googlePlacesUrl.toString());
    }

    private void showPlace(JsonArray jsonArray) {
        JsonObject jsonObject;
        for (int i = 0; i < jsonArray.size(); i++) {
            try {
                MarkerOptions markerOptions = new MarkerOptions();
                jsonObject = ConvertUtils.toJsonObject(jsonArray.get(i));
                listObject.add(jsonObject);
                adapter.notifyItemInserted(listObject.size() - 1);
                JsonObject jsonObject2 = ConvertUtils.toJsonObject(jsonObject.get("User"));
                double lat = ConvertUtils.toDouble(jsonObject2.get("Lat"));
                double lng = ConvertUtils.toDouble(jsonObject2.get("Lon"));
                String fullname = ConvertUtils.toString(jsonObject2.get("FullName"));
                LatLng latLng = new LatLng(lat, lng);
                markerOptions.position(latLng);
                markerOptions.title(fullname);
                Marker marker = mMap.addMarker(markerOptions);
                marker.setTag(jsonObject);
                if (ConvertUtils.toBoolean(jsonObject2.get("ShowAvartar"))) {
                    String avatar = ConvertUtils.toString(jsonObject2.get("Avartar"));
                    if (avatar != null && !avatar.isEmpty()) {
                        loadImageByGlide(getContext(), avatar, marker);
                    } else {
                        BitmapDescriptor markerIcon = getMarkerIconFromDrawable(ContextCompat.getDrawable(getContext(), R.drawable.ic_friend_around));
                        marker.setIcon(markerIcon);
                    }
                } else {
                    BitmapDescriptor markerIcon = getMarkerIconFromDrawable(ContextCompat.getDrawable(getContext(), R.drawable.ic_friend_around));
                    marker.setIcon(markerIcon);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        }

    }

    @UiThread
    public void loadImageByGlide(Context context, @NonNull String url, Marker marker) {
        try {
            RequestOptions options = new RequestOptions()
                    .centerCrop()
                    .priority(Priority.HIGH).dontAnimate().override(50, 50).centerCrop();

            options = options.diskCacheStrategy(DiskCacheStrategy.ALL);
            Glide.with(context).load(url).apply(options).into(new SimpleTarget<Drawable>() {
                @Override
                public void onResourceReady(Drawable resource, Transition<? super Drawable> transition) {
                    try {
                        BitmapDescriptor markerIcon = getMarkerIconFromDrawable(resource);
                        marker.setIcon(markerIcon);
                    } catch (Exception e) {
                        e.printStackTrace();
                        try {
                            BitmapDescriptor markerIcon = getMarkerIconFromDrawable(ContextCompat.getDrawable(getContext(), R.drawable.ic_menu_friend_around));
                            marker.setIcon(markerIcon);
                        } catch (Exception e2) {

                        }
                    }
                }

                @Override
                public void onLoadFailed(@Nullable Drawable errorDrawable) {
                    try {
                        BitmapDescriptor markerIcon = getMarkerIconFromDrawable(ContextCompat.getDrawable(getContext(), R.drawable.ic_menu_friend_around));
                        marker.setIcon(markerIcon);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    super.onLoadFailed(errorDrawable);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        } catch (OutOfMemoryError e) {
            e.printStackTrace();
        }


    }

    private BitmapDescriptor getMarkerIconFromDrawable(Drawable drawable) {
        Canvas canvas1 = new Canvas();
        Bitmap bitmap1 = Bitmap.createBitmap(drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        canvas1.setBitmap(bitmap1);
        drawable.setBounds(0, 0, drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight());
        drawable.draw(canvas1);

        Bitmap output = Bitmap.createBitmap(bitmap1.getWidth(), bitmap1
                .getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(output);
        final Rect rect = new Rect(0, 0, bitmap1.getWidth(), bitmap1.getHeight());

        final Paint paint = new Paint();
        final int color = 0xff424242;
        paint.setAntiAlias(true);
        paint.setColor(color);
        final RectF rectF = new RectF(rect);

        canvas.drawRoundRect(rectF, 25, 25, paint);
        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
        canvas.drawBitmap(bitmap1, rect, rect, paint);

        return BitmapDescriptorFactory.fromBitmap(output);
    }

    private void updateLocationUI() {
        if (mMap == null) {
            return;
        }
        BaseActivity.requestPermission(getContext(), new AndroidPermissionUtils.OnCallbackRequestPermission() {
            @SuppressLint("MissingPermission")
            @Override
            public void onSuccess() {
                mMap.setMyLocationEnabled(true);
                mMap.getUiSettings().setMyLocationButtonEnabled(true);
            }

            @SuppressLint("MissingPermission")
            @Override
            public void onFailed() {
                mMap.setMyLocationEnabled(true);
                mMap.getUiSettings().setMyLocationButtonEnabled(false);
                mLastKnownLocation = null;
            }
        }, AndroidPermissionUtils.TypePermission.PERMISSION_ACCESS_COARSE_LOCATION, AndroidPermissionUtils.TypePermission.PERMISSION_ACCESS_FINE_LOCATION);
    }

    @OnClick(R.id.btnBack)
    void menuClick() {
        getActivity().finish();
    }

    void chatWith(String userId) {
        GroupChannelActivity.open(getActivity(), userId);
    }

    @OnClick({R.id.txtSearch})
    void search() {
        SearchByNameActivity.open(getContext());
//        getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.lnFragment, SearchByNameFragment.getInstants(), "searchFriend").addToBackStack(null).commit();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    public static Fragment getInstants() {
        return new FriendAroundFragment();
    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        BaseActivity.getHandler(getContext()).postDelayed(new Runnable() {
            @Override
            public void run() {
                JsonObject jsonObject = (JsonObject) marker.getTag();
                if (jsonObject != null) {
                    jsonObject = ConvertUtils.toJsonObject(jsonObject.get("User"));
                    chatWith(ConvertUtils.toString(jsonObject.get("UserName")));
                }
            }
        }, 2000);
        return false;
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case REQUEST_CHECK_SETTINGS:
                switch (resultCode) {
                    case Activity.RESULT_OK:
                        getDeviceLocation();
                        break;
                }
                break;
            case REQ_CHANGE_SEARCH_OPTION:
                if (mLastKnownLocation != null) {
                    gender = SharedPreferencesUtil.getInt(getContext(), Constants.FILTER_GENDER, 0);
                    range_age = SharedPreferencesUtil.getInt(getContext(), Constants.FILTER_AGE, 0);
                    maxDistance = SharedPreferencesUtil.getString(getContext(), Constants.MAX_DISTANCE, "");
                    if (maxDistance.equals(""))
                        maxDistance = "0";
                    if (!UserInfo.isLogin())
                        only_friend = false;
                    else
                        only_friend = SharedPreferencesUtil.getBoolean(getContext(), Constants.ONLY_FRIEND, false);
                    loadListUser(mLastKnownLocation.latitude, mLastKnownLocation.longitude, 1, 0);
                }
                break;
            case REQ_CODE_PLACE_PICKER:
                if (resultCode == Activity.RESULT_OK) {
                    mMap.clear();
                    place = PlacePicker.getPlace(getActivity(), data);
                    mLastKnownLocation = place.getLatLng();
                    showLocationMarker(mLastKnownLocation.latitude, mLastKnownLocation.longitude);
                    mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(mLastKnownLocation, DEFAULT_ZOOM));
                    listObject.clear();
                    adapter.notifyDataSetChanged();
                    loadListUser(mLastKnownLocation.latitude, mLastKnownLocation.longitude, 1, size);
                }
                break;

        }
    }

    public class ViewHolder extends BaseViewHolder {
        @BindView(R.id.imageView)
        ImageView imageView;
        @BindView(R.id.tvUserName)
        TextView tvName;
        @BindView(R.id.btnMailBox)
        ImageView btnMailBox;
        @BindView(R.id.tvDistance)
        TextView tvDistance;
        @BindView(R.id.tvAddress)
        TextView tvAddress;
        @BindView(R.id.btnCall)
        ImageView btnCall;
        @BindView(R.id.imgGender)
        ImageView imgGender;
        @BindView(R.id.lnAge)
        View lnAge;
        @BindView(R.id.tvAge)
        TextView tvAge;

        @OnClick(R.id.btnDirection)
        void direction(View view) {
            int index = getAdapterPosition();
            JsonObject jsonObject = listObject.get(index);
            jsonObject = ConvertUtils.toJsonObject(jsonObject.get("User"));
            double lat = ConvertUtils.toDouble(jsonObject.get("Lat"));
            double lon = ConvertUtils.toDouble(jsonObject.get("Lon"));
            Intent intent = new Intent(Intent.ACTION_VIEW,
                    Uri.parse("http://maps.google.com/maps?daddr=" + lat + "," + lon));
            startActivity(intent);

        }

        @OnClick(R.id.btnCall)
        void call(View view) {
            requestPermission(getActivity(), new AndroidPermissionUtils.OnCallbackRequestPermission() {
                @SuppressLint("MissingPermission")
                @Override
                public void onSuccess() {
                    String phone = (String) view.getTag();
                    if (phone != null && !phone.trim().isEmpty()) {
                        try {
                            Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + phone));
                            startActivity(intent);
                            return;
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                    BaseActivity.showSnackBar(getContext(), R.string.not_found_phone_number_in_profile);
                }

                @Override
                public void onFailed() {
                    showSnackBar(getActivity(), R.string.you_much_allow_this_app_make_a_call);
                }
            }, AndroidPermissionUtils.TypePermission.CALL_PHONE);
        }


        @OnClick(R.id.btnMailBox)
        void viewProfile(View view) {
            if ("".equals(view.getTag())) {
                showSnackBar(getActivity(), R.string.add_friend_before_chat);
                return;
            }
            String userId = (String) view.getTag();
            GroupChannelActivity.open(getContext(), userId);

//            String userId = (String) view.getTag();
//            GroupChannelActivity.open(getContext(), userId);
        }

        public ViewHolder(View itemView) {
            super(itemView);
        }
    }

}
