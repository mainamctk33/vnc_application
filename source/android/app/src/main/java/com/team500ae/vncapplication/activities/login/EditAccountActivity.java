package com.team500ae.vncapplication.activities.login;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.InputFilter;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStates;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.google.android.gms.maps.model.LatLng;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.team500ae.library.utils.AndroidPermissionUtils;
import com.team500ae.library.utils.ConvertUtils;
import com.team500ae.library.utils.ImageUtils;
import com.team500ae.vncapplication.R;
import com.team500ae.vncapplication.base.BaseActivity;
import com.team500ae.vncapplication.constants.Constants;
import com.team500ae.vncapplication.constants.TimeOutConstants;
import com.team500ae.vncapplication.data_access.RealmInfo;
import com.team500ae.vncapplication.data_access.job.TypeJobInfo;
import com.team500ae.vncapplication.data_access.upload.BlobUploadProvider;
import com.team500ae.vncapplication.data_access.upload.UploadInfo;
import com.team500ae.vncapplication.data_access.user.UserInfo;
import com.team500ae.vncapplication.models.BaseReturnFunctionEntity;
import com.team500ae.vncapplication.models.ServiceResponseEntity;
import com.team500ae.vncapplication.models.realm_models.job.TypeJobEntity;
import com.team500ae.vncapplication.others.DialogListener;
import com.team500ae.vncapplication.others.UploadImageDialog;
import com.team500ae.vncapplication.utils.FileUtils;
import com.team500ae.vncapplication.utils.GPSTrackerUtils;
import com.team500ae.vncapplication.utils.KeyboardUtils;
import com.team500ae.vncapplication.utils.SecurityUtils;
import com.team500ae.vncapplication.utils.StringUtils;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;
import io.realm.Realm;

/**
 * phuong.ndp 04/03/2018
 */

public class EditAccountActivity extends BaseActivity implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {

    @BindView(R.id.layout_password)
    View vPasswordInclude;
    @BindView(R.id.editProfile_layout)
    LinearLayout editProfileLayout;

    private String TAG = EditAccountActivity.class.toString();
    private final int SELECT_AVATAR_ACTION = 1;
    private boolean isSelectNewAvatar = false;
    private static final int REQ_CODE_PLACE_PICKER = 10006;
    protected static final int REQUEST_CHECK_SETTINGS = 0x1;

    private EditText edtFullnameContent, edtPasswordContent, edtConfirmPasswordContent, edtPhoneNumberContent, edtAddressContent, edtJobContent, edtIntroductionContent;
    private TextView tvBirthdayContent, tvGenderContent, tvJobCategoryContent, tvEmailContent, tvNationContent;
    private ImageView ivGenderRightIcon, ivJobCategoryRightIcon, ivNationRightIcon;
    private CircleImageView civUserAvatar;
    private long birthdayMiliseconds = 0;

    private Realm realm;
    private Calendar myCalendar;
    private String avatarUrl = "";
    private Bitmap bitmap;
    private Uri selectedAvatarImage = Uri.EMPTY;
    private String gender = Constants.EMPTY;
    private int typeJobId;
    private GoogleApiClient mGoogleApiClient;
    private String urlLocationImage;
    private Place place;
    private LatLng latlon;
    private double latitude, longtitude;

    private ArrayList<Integer> listTypeJobID;
    private ArrayList<String> listTypeJobName;
    private List<String> listNations;
    private Uri tempUri;

    public static void open(Context activity) {
        Intent intent = new Intent(activity, EditAccountActivity.class);
        activity.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
//        setContentView(R.layout.activity_edit_account);
        setContentView(R.layout.activity_edit_profile);
        ButterKnife.bind(this);
        KeyboardUtils.setupUI(editProfileLayout, getActivity());
        super.onCreate(savedInstanceState);
        realm = RealmInfo.getRealm(getApplicationContext());
        showBackButton();
        getSupportActionBar().setTitle(R.string.pref_account_edit_profile_title);

        initLayout();
        myCalendar = Calendar.getInstance();
        loadTypeJob();
        listNations = StringUtils.getListNations();

//        edtBirthday.setFocusable(false);
//        edtBirthday.setClickable(true);
//
//        listNations = StringUtils.getListNations();
//        ArrayAdapter<String> nationAdapter = new ArrayAdapter<String>(this, R.layout.spinner_item, listNations);
//        spNation.setAdapter(nationAdapter);
//        nationAdapter.notifyDataSetChanged();

        loadOldAccountInfo();

        tvGenderContent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showGenderTypeDialog();
            }
        });
        ivGenderRightIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showGenderTypeDialog();
            }
        });
        tvBirthdayContent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectBirthday();
            }
        });
        tvJobCategoryContent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showJobCategoryDialog();
            }
        });
        ivJobCategoryRightIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showJobCategoryDialog();
            }
        });
        tvNationContent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showNationDialog();
            }
        });
        ivNationRightIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showNationDialog();
            }
        });

        //show current location
//        GPSTrackerUtils gpsTrackerUtils = new GPSTrackerUtils(getActivity());
//        if (gpsTrackerUtils.canGetLocation()) {
//            Log.d(TAG, "canGetLocation");
//            latlon = new LatLng(gpsTrackerUtils.getLatitude(), gpsTrackerUtils.getLongitude());
//            GPSTrackerUtils.saveCurrentLocation(getActivity(), gpsTrackerUtils.getLongitude(), gpsTrackerUtils.getLatitude());
//            bindData(true);
//            return;
//        } else {
//            settingsrequest();
//        }
    }

    public void showBackButton() {
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            // Show the Up button in the action bar.
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setDisplayShowHomeEnabled(true);
        }
    }

    private void loadTypeJob() {
        BaseReturnFunctionEntity<ArrayList<TypeJobEntity>> myList = new TypeJobInfo().getListTypeJob(getApplicationContext(), realm);
        listTypeJobID = new ArrayList<>();
        listTypeJobName = new ArrayList<>();
        if (myList.isTrue()) {
            for (TypeJobEntity type : myList.getData()) {
                listTypeJobID.add(type.getId());
                listTypeJobName.add(type.getName());
            }
        }

//        ArrayAdapter<String> jobCategoryAdapter = new ArrayAdapter<String>(this, R.layout.spinner_item, listTypeJobName);
//        spJobCategory.setAdapter(jobCategoryAdapter);
//        jobCategoryAdapter.notifyDataSetChanged();
    }


    private void initLayout() {
        View vFullnameInclude = findViewById(R.id.layout_full_name);
        ImageView ivFullnameLeftIcon = (ImageView) vFullnameInclude.findViewById(R.id.image_left);
        ivFullnameLeftIcon.setImageResource(R.drawable.ic_full_name);
        TextView tvFullnameTitle = (TextView) vFullnameInclude.findViewById(R.id.text_title);
        tvFullnameTitle.setText(getResources().getString(R.string.fullName));
        tvFullnameTitle.setTextColor(getResources().getColor(R.color.colorPrimary));
        edtFullnameContent = (EditText) vFullnameInclude.findViewById(R.id.text_content);
        edtFullnameContent.setHint(getResources().getString(R.string.only_input_50_characters));
        edtFullnameContent.setFilters(new InputFilter[]{new InputFilter.LengthFilter(Constants.MAX_FULL_NAME_LENGTH)});
        edtFullnameContent.setFilters(new InputFilter[]{new InputFilter.LengthFilter(Constants.MAX_FULL_NAME_LENGTH)});
        edtFullnameContent.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                if (s.length() >= Constants.MAX_FULL_NAME_LENGTH) {
                    new AlertDialog.Builder(EditAccountActivity.this).setTitle(getResources().getString(R.string.character_limit_exceeded)).setMessage(getResources().getString(R.string.notice_number_limit_character) + " " + Constants.MAX_FULL_NAME_LENGTH).setIcon(R.drawable.ic_full_name).setPositiveButton(android.R.string.ok, null).show();
                }
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        ImageView ivFullnameRightIcon = (ImageView) vFullnameInclude.findViewById(R.id.image_right);
        ivFullnameRightIcon.setVisibility(View.GONE);

        View vEmailInclude = findViewById(R.id.layout_email);
        ImageView ivEmailLeftIcon = (ImageView) vEmailInclude.findViewById(R.id.image_left);
        ivEmailLeftIcon.setImageResource(R.drawable.ic_email);
        TextView tvEmailTitle = (TextView) vEmailInclude.findViewById(R.id.text_title);
        tvEmailTitle.setText(getResources().getString(R.string.email));
        tvEmailTitle.setTextColor(getResources().getColor(R.color.colorPrimary));
        tvEmailContent = (TextView) vEmailInclude.findViewById(R.id.text_content);
        tvEmailContent.setHint(getResources().getString(R.string.only_input_50_characters));
        ImageView ivEmailRightIcon = (ImageView) vEmailInclude.findViewById(R.id.image_right);
        ivEmailRightIcon.setVisibility(View.GONE);

        View vBirthdayInclude = findViewById(R.id.layout_birthday);
        ImageView ivBirthdayLeftIcon = (ImageView) vBirthdayInclude.findViewById(R.id.image_left);
        ivBirthdayLeftIcon.setImageResource(R.drawable.ic_birthday);
        TextView tvBirthdayTitle = (TextView) vBirthdayInclude.findViewById(R.id.text_title);
        tvBirthdayTitle.setText(getResources().getString(R.string.birthday));
        tvBirthdayContent = (TextView) vBirthdayInclude.findViewById(R.id.text_content);
        tvBirthdayContent.setText("19/11/1991");
        ImageView ivBirthdayRightIcon = (ImageView) vBirthdayInclude.findViewById(R.id.image_right);
        ivBirthdayRightIcon.setVisibility(View.GONE);

        View vGenderInclude = findViewById(R.id.layout_gender);
        ImageView ivGenderLeftIcon = (ImageView) vGenderInclude.findViewById(R.id.image_left);
        ivGenderLeftIcon.setImageResource(R.drawable.ic_gender);
        TextView tvGenderTitle = (TextView) vGenderInclude.findViewById(R.id.text_title);
        tvGenderTitle.setText(getResources().getString(R.string.gender));
        tvGenderContent = (TextView) vGenderInclude.findViewById(R.id.text_content);
        tvGenderContent.setText(getResources().getString(R.string.common_gender_male));
        ivGenderRightIcon = (ImageView) vGenderInclude.findViewById(R.id.image_right);
        ivGenderRightIcon.setImageResource(R.drawable.ic_next);

        View vPhoneNumberInclude = findViewById(R.id.layout_phone_number);
        ImageView ivPhoneNumberLeftIcon = (ImageView) vPhoneNumberInclude.findViewById(R.id.image_left);
        ivPhoneNumberLeftIcon.setImageResource(R.drawable.ic_phone_number);
        TextView tvPhoneNumberTitle = (TextView) vPhoneNumberInclude.findViewById(R.id.text_title);
        tvPhoneNumberTitle.setText(getResources().getString(R.string.phone_number));
        edtPhoneNumberContent = (EditText) vPhoneNumberInclude.findViewById(R.id.text_content);
        edtPhoneNumberContent.setFilters(new InputFilter[]{new InputFilter.LengthFilter(Constants.MAX_PHONE_NUMBER_LENGTH)});

        ImageView ivPhoneNumberRightIcon = (ImageView) vPhoneNumberInclude.findViewById(R.id.image_right);
        ivPhoneNumberRightIcon.setVisibility(View.GONE);

        View vLocationLocationInclude = findViewById(R.id.layout_location_location);
        ImageView ivLocationLocationLeftIcon = (ImageView) vLocationLocationInclude.findViewById(R.id.image_left);
        ivLocationLocationLeftIcon.setImageResource(R.drawable.ic_address);
        TextView tvLocationLocationTitle = (TextView) vLocationLocationInclude.findViewById(R.id.text_title);
        tvLocationLocationTitle.setText(getResources().getString(R.string.location_location));
        TextView edtLocationLocationContent = (TextView) vLocationLocationInclude.findViewById(R.id.text_content);
        edtLocationLocationContent.setVisibility(View.GONE);
        ImageView ivLocationLocationRightIcon = (ImageView) vLocationLocationInclude.findViewById(R.id.image_right);
        ivLocationLocationRightIcon.setVisibility(View.GONE);

        View vNationInclude = findViewById(R.id.layout_nation);
        ImageView ivNationLeftIcon = (ImageView) vNationInclude.findViewById(R.id.image_left);
        ivNationLeftIcon.setImageResource(R.drawable.ic_nation);
        TextView tvNationTitle = (TextView) vNationInclude.findViewById(R.id.text_title);
        tvNationTitle.setText(getResources().getString(R.string.nation));
        tvNationContent = (TextView) vNationInclude.findViewById(R.id.text_content);
        tvNationContent.setText("Vietnam");
        ivNationRightIcon = (ImageView) vNationInclude.findViewById(R.id.image_right);
        ivNationRightIcon.setImageResource(R.drawable.ic_next);

        View vAddressInclude = findViewById(R.id.layout_address);
        ImageView ivAddressLeftIcon = (ImageView) vAddressInclude.findViewById(R.id.image_left);
        ivAddressLeftIcon.setImageResource(R.drawable.ic_company);
        TextView tvAddressTitle = (TextView) vAddressInclude.findViewById(R.id.text_title);
        tvAddressTitle.setText(getResources().getString(R.string.address));
        edtAddressContent = (EditText) vAddressInclude.findViewById(R.id.text_content);
        ImageView ivAddressRightIcon = (ImageView) vAddressInclude.findViewById(R.id.image_right);
        ivAddressRightIcon.setVisibility(View.GONE);

        View vJobCategoryInclude = findViewById(R.id.layout_job_category);
        ImageView ivJobCategoryLeftIcon = (ImageView) vJobCategoryInclude.findViewById(R.id.image_left);
        ivJobCategoryLeftIcon.setImageResource(R.drawable.ic_job_category);
        TextView tvJobCategoryTitle = (TextView) vJobCategoryInclude.findViewById(R.id.text_title);
        tvJobCategoryTitle.setText(getResources().getString(R.string.job_category));
        tvJobCategoryContent = (TextView) vJobCategoryInclude.findViewById(R.id.text_content);
        ivJobCategoryRightIcon = (ImageView) vJobCategoryInclude.findViewById(R.id.image_right);
        ivJobCategoryRightIcon.setImageResource(R.drawable.ic_next);

        View vJobInclude = findViewById(R.id.layout_job);
        ImageView ivJobLeftIcon = (ImageView) vJobInclude.findViewById(R.id.image_left);
        ivJobLeftIcon.setImageResource(R.drawable.ic_job);
        TextView tvJobTitle = (TextView) vJobInclude.findViewById(R.id.text_title);
        tvJobTitle.setText(getResources().getString(R.string.job));
        edtJobContent = (EditText) vJobInclude.findViewById(R.id.text_content);
        ImageView ivJobRightIcon = (ImageView) vJobInclude.findViewById(R.id.image_right);
        ivJobRightIcon.setVisibility(View.GONE);

        View vIntroductionInclude = findViewById(R.id.layout_introduction);
        ImageView ivIntroductionLeftIcon = (ImageView) vIntroductionInclude.findViewById(R.id.image_left);
        ivIntroductionLeftIcon.setImageResource(R.drawable.ic_introduction);
        TextView tvIntroductionTitle = (TextView) vIntroductionInclude.findViewById(R.id.text_title);
        tvIntroductionTitle.setText(getResources().getString(R.string.introduction));
        tvIntroductionTitle.setVisibility(View.GONE);
        edtIntroductionContent = (EditText) vIntroductionInclude.findViewById(R.id.text_content);
        edtIntroductionContent.setGravity(Gravity.LEFT);
        ImageView ivIntroductionRightIcon = (ImageView) vIntroductionInclude.findViewById(R.id.image_right);
        ivIntroductionRightIcon.setVisibility(View.GONE);

//        vPasswordInclude = findViewById(R.id.layout_password);
        ImageView ivPasswordLeftIcon = (ImageView) vPasswordInclude.findViewById(R.id.image_left);
        ivPasswordLeftIcon.setImageResource(R.drawable.ic_confirm_password);
        TextView tvPasswordTitle = (TextView) vPasswordInclude.findViewById(R.id.text_title);
        tvPasswordTitle.setText(getResources().getString(R.string.passowrd));
        tvPasswordTitle.setTextColor(getResources().getColor(R.color.colorPrimary));
        edtPasswordContent = (EditText) vPasswordInclude.findViewById(R.id.text_content);
        edtPasswordContent.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
        edtPasswordContent.setFilters(new InputFilter[]{new InputFilter.LengthFilter(Constants.MAX_FULL_NAME_LENGTH)});

        ImageView ivPasswordRightIcon = (ImageView) vPasswordInclude.findViewById(R.id.image_right);
        ivPasswordRightIcon.setVisibility(View.GONE);

        civUserAvatar = (CircleImageView) findViewById(R.id.civ_user_avatar);
    }

    private void loadOldAccountInfo() {
        JsonObject currentUser = UserInfo.getCurrentUser();
        avatarUrl = ConvertUtils.toString(currentUser.get("Avartar"));
        ImageUtils.loadImageByGlide(getApplicationContext(), true, 140, 140, avatarUrl, R.drawable.default_avatar, R.drawable.default_avatar, civUserAvatar, true);

        edtFullnameContent.setText(ConvertUtils.toString(currentUser.get("FullName")));
        String birthday = ConvertUtils.toDateString(ConvertUtils.toLong(currentUser.get("Birthday")), "dd/mm/yyyy");
        tvBirthdayContent.setText(birthday);
        if(birthday!=null && birthday.equals("01/00/1970")) {
            tvBirthdayContent.setText("");
            tvBirthdayContent.setHint(getResources().getString(R.string.input_birthday));
        }

        String email = ConvertUtils.toString(currentUser.get("UserName")).toLowerCase();
        if (email.contains(UserInfo.SocialType.fb.toString())) {
            tvEmailContent.setText(getResources().getString(R.string.hidden_content));
            tvEmailContent.setTextColor(getResources().getColor(R.color.grey_600));
            vPasswordInclude.setVisibility(View.GONE);
        } else if (email.contains(UserInfo.SocialType.gg.toString())) {
            tvEmailContent.setText(email.substring(2));//cut prefix: gg
            vPasswordInclude.setVisibility(View.GONE);
        } else
            tvEmailContent.setText(email);

        if (ConvertUtils.toInt(currentUser.get("Gender")) == 1)
            tvGenderContent.setText(getResources().getString(R.string.common_gender_male));
        else
            tvGenderContent.setText(getResources().getString(R.string.common_gender_female));

        edtAddressContent.setText(ConvertUtils.toString(currentUser.get("CurrentLivingCity")));
        tvNationContent.setText(ConvertUtils.toString(currentUser.get("Nationality")));
        edtJobContent.setText(ConvertUtils.toString(currentUser.get("Job")));
        edtPhoneNumberContent.setText(ConvertUtils.toString(currentUser.get("PhoneNumber")));
        edtIntroductionContent.setText(ConvertUtils.toString(currentUser.get("Introduction")));

        //select user's nation on spinner
//        String nation = ConvertUtils.toString(currentUser.get("Nationality"));
//        int nationIndex = StringUtils.getIdFromValue(listNations, nation);
//        if (nationIndex < 0) nationIndex = 0;
//        spNation.setSelection(nationIndex);

        //select job category on spinner
        int jobCategoryId = ConvertUtils.toInt(currentUser.get("TypeJob"));
        int jobCategoryIndex = StringUtils.getIntegerIndex(listTypeJobID, jobCategoryId);
        if (jobCategoryIndex > 0)
            tvJobCategoryContent.setText(listTypeJobName.get(jobCategoryIndex));
//        if (jobCategoryIndex < 0) jobCategoryIndex = 0;
//        spJobCategory.setSelection(jobCategoryIndex);
//        tvJobCategoryContent.setText(listTypeJobName.get(jobCategoryIndex));

        //show location on map
        String latAddress = ConvertUtils.toString(currentUser.get("LatAddress"));
        String lonAddress = ConvertUtils.toString(currentUser.get("LonAddress"));
        latitude = ConvertUtils.toDouble(currentUser.get("LatAddress"));
        longtitude = ConvertUtils.toDouble(currentUser.get("LonAddress"));
        if (latAddress == null || latAddress.isEmpty() || lonAddress == null || lonAddress.isEmpty()) {
            ImageUtils.loadImageByGlide(getActivity(), false, 0, 150, "", R.drawable.bg_map_vn, R.drawable.bg_map_vn, imgLocation, true);
        } else {
            urlLocationImage = Constants.MAP_URL_HEADER + latAddress + "," + lonAddress + Constants.MAP_URL_FOOTER;
            ImageUtils.loadImageByGlide(getActivity(), false, 0, 150, urlLocationImage, R.drawable.bg_map_vn, R.drawable.bg_map_vn, imgLocation, true);
        }
    }

    @OnClick(R.id.civ_user_avatar)
    void selectAvatar(View view) {
        requestPermission(new AndroidPermissionUtils.OnCallbackRequestPermission() {
            @Override
            public void onSuccess() {
                UploadImageDialog uploadImageDialog = new UploadImageDialog(getActivity());
                uploadImageDialog.setOnItemClickListener(new DialogListener() {
                    @Override
                    public void onConfirmClicked() {
                        avatarUrl = uploadImageDialog.getEditText();
                        Toast.makeText(EditAccountActivity.this, uploadImageDialog.getEditText(), Toast.LENGTH_SHORT).show();
                        showSnackBar("avatarUrl: " + avatarUrl);
                        ImageUtils.loadImageByGlide(getApplicationContext(), true, 200, 200, avatarUrl, R.drawable.img_no_image, R.drawable.img_no_image, civUserAvatar, true);
                    }

                    @Override
                    public void onButtonChooseImage() {
                        tempUri = FileUtils.createTempFile(getActivity());
                        Intent chooseImageIntent = new FileUtils().selectOrTakePicture(getActivity(), tempUri);
                        startActivityForResult(chooseImageIntent, SELECT_AVATAR_ACTION);
                    }
                });
                uploadImageDialog.show();
            }

            @Override
            public void onFailed() {
                Log.e(TAG, "onFailedonFailedonFailedonFailed");
            }
        }, AndroidPermissionUtils.TypePermission.PERMISSION_READ_EXTERNAL_STORAGE, AndroidPermissionUtils.TypePermission.PERMISSION_WRIRE_EXTERNAL_STORAGE);
    }

    //    @OnClick(R.id.birthday)
    void selectBirthday() {
        DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                // TODO Auto-generated method stub
                Calendar calendar = Calendar.getInstance();
                calendar.set(Calendar.YEAR, year);
                calendar.set(Calendar.MONTH, monthOfYear);
                calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                if (calendar.getTimeInMillis() > new Date().getTime()) {
                    showSnackBar(R.string.activity_register_error_birthday_greater_than_current_date);
                } else {
                    myCalendar.set(Calendar.YEAR, year);
                    myCalendar.set(Calendar.MONTH, monthOfYear);
                    myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                    String myFormat = "dd/MM/yyyy"; //In which you need put here
                    SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
                    tvBirthdayContent.setText(sdf.format(myCalendar.getTime()));
                }
            }

        };
        new DatePickerDialog(EditAccountActivity.this, date, myCalendar
                .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                myCalendar.get(Calendar.DAY_OF_MONTH)).show();
    }

    private void showGenderTypeDialog() {
        final String[] singleChoiceItems = getResources().getStringArray(R.array.gender_type);
        int itemSelected = 1;
        if (tvGenderContent.getText().toString().equals(getResources().getString(R.string.common_gender_male)))
            itemSelected = 0;

        new AlertDialog.Builder(this).setTitle(getResources().getString(R.string.gender)).setSingleChoiceItems(singleChoiceItems, itemSelected, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int selectedIndex) {
                tvGenderContent.setText(singleChoiceItems[selectedIndex]);
            }
        }).setIcon(R.drawable.ic_gender).setNegativeButton(getResources().getString(R.string.close), null).show();
    }

    private void showJobCategoryDialog() {
        final String[] arrayJobCategory = listTypeJobName.toArray(new String[0]);
        String currentJobCategory = tvJobCategoryContent.getText().toString();
        int itemSelected = StringUtils.getStringIndex(listTypeJobName, currentJobCategory);

        new AlertDialog.Builder(this).setTitle(getResources().getString(R.string.job_category)).setSingleChoiceItems(arrayJobCategory, itemSelected, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int selectedIndex) {
                tvJobCategoryContent.setText(arrayJobCategory[selectedIndex]);
            }
        }).setIcon(R.drawable.ic_job_category).setNegativeButton(getResources().getString(R.string.close), null).show();
    }

    private void showNationDialog() {
        final String[] arrayNations = listNations.toArray(new String[0]);
        String currentNation = tvNationContent.getText().toString();
        int itemSelected = StringUtils.getStringIndex(listNations, currentNation);

        new AlertDialog.Builder(this).setTitle(getResources().getString(R.string.nation)).setSingleChoiceItems(arrayNations, itemSelected, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int selectedIndex) {
                tvNationContent.setText(arrayNations[selectedIndex]);
            }
        }).setIcon(R.drawable.ic_nation).setNegativeButton(getResources().getString(R.string.close), null).show();
    }

    @OnClick(R.id.btn_update)
    void updateAccount() {
        getHandler().postDelayed(new Runnable() {
            @Override
            public void run() {
                saveData();
            }
        }, 500);
    }

    private void saveData() {
        edtFullnameContent.setError(null);
        edtAddressContent.setError(null);
        edtJobContent.setError(null);
        edtPhoneNumberContent.setError(null);

        String fullname = edtFullnameContent.getText().toString();
//        String nationality = spNation.getSelectedItem().toString();
        String nationality = tvNationContent.getText().toString();
        String birthday = tvBirthdayContent.getText().toString();
        gender = tvGenderContent.getText().toString();
        if (gender != null && gender.equals(getResources().getString(R.string.common_gender_male)))
            gender = getResources().getString(R.string.common_gender_male);
        else gender = getResources().getString(R.string.common_gender_female);
//        if (rbtnMale.isChecked())
//            gender = getResources().getString(R.string.common_gender_male);

        String currentLivingCity = edtAddressContent.getText().toString();
        //todo: get typeJobId from typeJob
//        String jobCategory = spJobCategory.getSelectedItem().toString();
        String jobCategory = tvJobCategoryContent.getText().toString();
        int typeJobId = StringUtils.getIdFromValue(listTypeJobName, listTypeJobID, jobCategory);
        if (typeJobId < 0) typeJobId = 0;

        String email = tvEmailContent.getText().toString();
        String job = edtJobContent.getText().toString();
        String phonenumber = edtPhoneNumberContent.getText().toString();
        String introduction = edtIntroductionContent.getText().toString();
        String rawPassword = edtPasswordContent.getText().toString();

        if (!StringUtils.Validate.isPersonalName(fullname)) {
            edtFullnameContent.setError(getString(R.string.activity_register_error_fullname_error));
            edtFullnameContent.requestFocus();
            return;
        }

        if (!birthday.isEmpty()) {
            if (!StringUtils.Validate.isDate(birthday)) {
                tvBirthdayContent.setError(getString(R.string.activity_register_error_birthday));
                tvBirthdayContent.requestFocus();
                return;
            }
        }

        if (!phonenumber.isEmpty() && !StringUtils.Validate.isPhone2(phonenumber)) {
            edtPhoneNumberContent.setError(getString(R.string.activity_register_error_phone_empty));
            edtPhoneNumberContent.requestFocus();
            return;
        }

        String socialId = ConvertUtils.toString(UserInfo.getCurrentUser().get("SocialId"));
        if (socialId == null || socialId.isEmpty()) {
            if (rawPassword == null || rawPassword.isEmpty()) {
                edtPasswordContent.setError(getString(R.string.activity_update_account_input_password));
                edtPasswordContent.requestFocus();
                return;
            }

            //encrypt password by MD5
            final String password = SecurityUtils.encryptMD5(rawPassword);
            if (!password.equals(ConvertUtils.toString(UserInfo.getCurrentUser().get("Password")))) {
                edtPasswordContent.setError(getString(R.string.activity_update_account_wrong_password));
                edtPasswordContent.requestFocus();
                return;
            }
        }

        try {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/mm/yyyy");
            Date date = simpleDateFormat.parse(birthday);
            birthdayMiliseconds = date.getTime();
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (isSelectNewAvatar) {
            new UploadInfo(new BlobUploadProvider() {
            }, new UploadInfo.UploadListener() {
                ProgressDialog progDialog = new ProgressDialog(EditAccountActivity.this);

                @Override
                public void beginUpload() {
                    progDialog.setMessage(getString(R.string.activity_update_account_info));
                    progDialog.setIndeterminate(false);
                    progDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                    progDialog.setCancelable(true);
                    progDialog.show();
                }

                @Override
                public void endUpload() {
                    try {
                        if (progDialog.isShowing()) {
                            progDialog.dismiss();
                        }
                    }catch (Exception e)
                    {
                        e.printStackTrace();
                    }

                }

                @Override
                public void onSuccess(JsonArray response) {
                    //After uploading avatar > call API to create user in DB
                    avatarUrl = response.getAsString();
                    new AsyncTask<Void, Void, BaseReturnFunctionEntity<Boolean>>() {
                        @Override
                        protected BaseReturnFunctionEntity<Boolean> doInBackground(Void... voids) {
                            int jobCategoryId = StringUtils.getIdFromValue(listTypeJobName, listTypeJobID, jobCategory);
                            if (jobCategoryId < 0) jobCategoryId = 0;
                            return UserInfo.updateAccount(getActivity(), fullname, nationality, birthdayMiliseconds, gender, currentLivingCity, jobCategoryId, job, phonenumber, introduction, avatarUrl, latitude, latitude, TimeOutConstants.TIMEOUT_ACCOUNT_REGISTER);
                        }

                        @Override
                        protected void onPostExecute(BaseReturnFunctionEntity<Boolean> result) {
                            super.onPostExecute(result);
                            if (result.isTrue()) {
                                showSnackBar(R.string.activity_update_account_successful);
                                hideProgress();

                                //send broadcast to update info on navigation draw
                                Intent intent = new Intent(Constants.ACTION_UPDATE_ACCOUNT);
                                sendBroadcast(intent);
                            } else {
                                showSnackBar(R.string.activity_update_account_failed);
                                hideProgress();
                            }
                        }
                    }.execute();
                }

                @Override
                public void onError(ServiceResponseEntity<JsonArray> response) {
                    //upload avatar failed
                    avatarUrl = "";
                    showSnackBar(R.string.activity_register_upload_image_failed);
                }
            }).upload(getActivity(), Constants.UPLOAD_FILE, ConvertUtils.toArrayList(Uri.class, selectedAvatarImage));
        } else {
            new AsyncTask<Void, Void, BaseReturnFunctionEntity<Boolean>>() {
                ProgressDialog progDialog = new ProgressDialog(EditAccountActivity.this);

                @Override
                protected void onPreExecute() {
                    super.onPreExecute();
                    try {
                        progDialog.setMessage(getString(R.string.activity_update_account_info));
                        progDialog.setIndeterminate(false);
                        progDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                        progDialog.setCancelable(true);
                        progDialog.show();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }

                @Override
                protected BaseReturnFunctionEntity<Boolean> doInBackground(Void... voids) {
                    int jobCategoryId = StringUtils.getIdFromValue(listTypeJobName, listTypeJobID, jobCategory);
                    if (jobCategoryId < 0) jobCategoryId = 0;
                    return UserInfo.updateAccount(getActivity(), fullname, nationality, birthdayMiliseconds, gender, currentLivingCity, jobCategoryId, job, phonenumber, introduction, avatarUrl, latitude, longtitude, TimeOutConstants.TIMEOUT_ACCOUNT_REGISTER);
                }

                @Override
                protected void onPostExecute(BaseReturnFunctionEntity<Boolean> result) {
                    super.onPostExecute(result);
                    try {
                        if (progDialog.isShowing()) {
                            progDialog.dismiss();
                        }
                    }catch (Exception e)
                    {
                        e.printStackTrace();
                    }


                    if (result.isTrue()) {
                        showSnackBar(R.string.activity_update_account_successful);

                        //send broadcast to update info on navigation draw
                        Intent intent = new Intent(Constants.ACTION_UPDATE_ACCOUNT);
                        sendBroadcast(intent);
                    } else {
                        showSnackBar(R.string.activity_update_account_failed);
                    }
                }
            }.execute();
        }
    }

    @BindView(R.id.imgLocation)
    ImageView imgLocation;

    @OnClick(R.id.imgLocation)
    void getLocation() {
        PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();
        try {
            startActivityForResult(builder.build(getActivity()), REQ_CODE_PLACE_PICKER);
        } catch (GooglePlayServicesRepairableException e) {
            showSnackBar("Bạn cần cài đặt google playservice để sử dụng tính năng này");
            e.printStackTrace();
        } catch (GooglePlayServicesNotAvailableException e) {
            showSnackBar("Bạn cần cài đặt google playservice để sử dụng tính năng này");
            e.printStackTrace();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case REQ_CODE_PLACE_PICKER:
                if (resultCode == RESULT_OK) {
                    place = PlacePicker.getPlace(getActivity(), data);
//                    if (edtLocationNameContent.getText().toString().trim().equals("") && place.getName() != null) {
//                        edtLocationNameContent.setText(place.getName());
//                    }
//                    if (edtAddressContent.getText().toString().trim().equals("") && place.getAddress() != null) {
//                        edtAddressContent.setText(place.getAddress());
//                    }
                    edtAddressContent.setText(place.getAddress());
                    latlon = place.getLatLng();
                    latitude = latlon.latitude;
                    longtitude = latlon.longitude;
                    urlLocationImage = Constants.MAP_URL_HEADER + latitude + "," + longtitude + Constants.MAP_URL_FOOTER;
                    ImageUtils.loadImageByGlide(getActivity(), false, 0, 150, urlLocationImage, R.drawable.bg_map_vn, R.drawable.bg_map_vn, imgLocation, true);
                }
                break;
            case SELECT_AVATAR_ACTION:
                bitmap = FileUtils.getImageFromResult(getActivity(), resultCode, data, tempUri);
                if (bitmap != null)
                    civUserAvatar.setImageBitmap(bitmap);
                selectedAvatarImage = FileUtils.getSelectedImageFromResult(getActivity(), resultCode, data, tempUri);
                isSelectNewAvatar = true;
                break;
            default:
                super.onActivityResult(requestCode, resultCode, data);
                break;
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void bindData(boolean withLonLat) {
        double latitude = GPSTrackerUtils.getCurrentLocationLatitude(getActivity());
        double longitude = GPSTrackerUtils.getCurrentLocationLongitude(getActivity());
        if (!withLonLat) {
            latitude = GPSTrackerUtils.getCurrentLocationLatitude(getActivity());
            longitude = GPSTrackerUtils.getCurrentLocationLongitude(getActivity());
        } else
//            tvPlace.setText(xa + ", " + huyen + ", " + tinh);
            if (latitude != 0 | longitude != 0) {
                urlLocationImage = Constants.MAP_URL_HEADER + latitude + "," + longitude + Constants.MAP_URL_FOOTER;
                ImageUtils.loadImageByGlide(getActivity(), false, 0, 150, urlLocationImage, R.drawable.bg_map_vn, R.drawable.bg_map_vn, imgLocation, true);
            }
    }

    public void settingsrequest() {
        mGoogleApiClient = new GoogleApiClient.Builder(getActivity())
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();
        mGoogleApiClient.connect();

        LocationRequest locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(30 * 1000);
        locationRequest.setFastestInterval(5 * 1000);
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(locationRequest);
        builder.setAlwaysShow(true); //this is the key ingredient

        PendingResult<LocationSettingsResult> result =
                LocationServices.SettingsApi.checkLocationSettings(mGoogleApiClient, builder.build());
        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(LocationSettingsResult result) {
                final Status status = result.getStatus();
                final LocationSettingsStates state = result.getLocationSettingsStates();
                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.SUCCESS:
                        Log.d(TAG, "SUCCESS");
                        try {
                            status.startResolutionForResult(getActivity(), REQUEST_CHECK_SETTINGS);
                        } catch (IntentSender.SendIntentException e) {
                            finish();
                        }
                        break;
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        Log.d(TAG, "RESOLUTION_REQUIRED");
                        try {
                            status.startResolutionForResult(getActivity(), REQUEST_CHECK_SETTINGS);
                        } catch (IntentSender.SendIntentException e) {
                            e.printStackTrace();
                            finish();
                        }
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        finish();
                        break;
                }
            }
        });
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }
}

