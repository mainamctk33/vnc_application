//package com.team500ae.vncapplication.activities.settings;
//
//import android.annotation.TargetApi;
//import android.app.AlarmManager;
//import android.app.AlertDialog;
//import android.app.PendingIntent;
//import android.content.Context;
//import android.content.DialogInterface;
//import android.content.Intent;
//import android.content.pm.PackageInfo;
//import android.content.res.Configuration;
//import android.media.Ringtone;
//import android.media.RingtoneManager;
//import android.net.Uri;
//import android.os.Build;
//import android.os.Bundle;
//import android.preference.ListPreference;
//import android.preference.Preference;
//import android.preference.PreferenceActivity;
//import android.preference.PreferenceFragment;
//import android.preference.PreferenceManager;
//import android.preference.RingtonePreference;
//import android.support.v7.app.ActionBar;
//import android.text.TextUtils;
//import android.util.Log;
//import android.view.MenuItem;
//import android.widget.ArrayAdapter;
//
//import com.team500ae.vncapplication.BuildConfig;
//import com.team500ae.vncapplication.R;
//import com.team500ae.vncapplication.activities.MainActivity;
//import com.team500ae.vncapplication.activities.login.ChangePasswordActivity;
//import com.team500ae.vncapplication.activities.login.EditAccountActivity;
//import com.team500ae.vncapplication.activities.login.LoginActivity;
//import com.team500ae.vncapplication.constants.Constants;
//import com.team500ae.vncapplication.data_access.user.UserInfo;
//import com.team500ae.vncapplication.utils.LanguageUtils;
//
//import java.util.List;
//
///**
// * A {@link PreferenceActivity} that presents a set of application settings. On
// * handset devices, settings are presented as a single list. On tablets,
// * settings are split by category, with category headers shown to the left of
// * the list of settings.
// * <p>
// * See <a href="http://developer.android.com/design/patterns/settings.html">
// * Android Design: Settings</a> for design guidelines and the <a
// * href="http://developer.android.com/guide/topics/ui/settings.html">Settings
// * API Guide</a> for more information on developing a Settings UI.
// */
//public class SettingsActivity extends AppCompatPreferenceActivity {
//    public String TAG = SettingsActivity.class.toString();
//
//    public AppCompatPreferenceActivity getActivity() {
//        return this;
//    }
//
//
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setupActionBar();
//    }
//
//    /**
//     * Set up the {@link android.app.ActionBar}, if the API is available.
//     */
//    private void setupActionBar() {
//        ActionBar actionBar = getSupportActionBar();
//        if (actionBar != null) {
//            // Show the Up button in the action bar.
//            actionBar.setDisplayHomeAsUpEnabled(true);
//        }
//    }
//
//    @Override
//    public void onBackPressed() {
//        super.onBackPressed();
//    }
//
//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        switch (item.getItemId()) {
//            case android.R.id.home:
//                // ps: handle events users click on back button on action bar > back to previous screen or back to main app
//                // this takes the user 'back', as if they pressed the left-facing triangle icon on the main android toolbar.
//                // if this doesn't work as desired, another possibility is to call `finish()` here.
//                getActivity().onBackPressed();
//                return true;
//            default:
//                return super.onOptionsItemSelected(item);
//        }
//    }
//
//    /**
//     * A preference value change listener that updates the preference's summary
//     * to reflect its new value.
//     */
//    private static Preference.OnPreferenceChangeListener sBindPreferenceSummaryToValueListener = new Preference.OnPreferenceChangeListener() {
//        @Override
//        public boolean onPreferenceChange(Preference preference, Object value) {
//            String stringValue = value.toString();
//
//            if (preference instanceof ListPreference) {
//                // For list preferences, look up the correct display value in
//                // the preference's 'entries' list.
//                ListPreference listPreference = (ListPreference) preference;
//                int index = listPreference.findIndexOfValue(stringValue);
//
//                // Set the summary to reflect the new value.
//                preference.setSummary(
//                        index >= 0
//                                ? listPreference.getEntries()[index]
//                                : null);
//
//            } else if (preference instanceof RingtonePreference) {
//                // For ringtone preferences, look up the correct display value
//                // using RingtoneManager.
//                if (TextUtils.isEmpty(stringValue)) {
//                    // Empty values correspond to 'silent' (no ringtone).
//                    preference.setSummary(R.string.pref_ringtone_silent);
//
//                } else {
//                    Ringtone ringtone = RingtoneManager.getRingtone(
//                            preference.getContext(), Uri.parse(stringValue));
//
//                    if (ringtone == null) {
//                        // Clear the summary if there was a lookup error.
//                        preference.setSummary(null);
//                    } else {
//                        // Set the summary to reflect the new ringtone display
//                        // name.
//                        String name = ringtone.getTitle(preference.getContext());
//                        preference.setSummary(name);
//                    }
//                }
//
//            } else {
//                // For all other preferences, set the summary to the value's
//                // simple string representation.
//                preference.setSummary(stringValue);
//            }
//            return true;
//        }
//    };
//
//    /**
//     * Helper method to determine if the device has an extra-large screen. For
//     * example, 10" tablets are extra-large.
//     */
//    private static boolean isXLargeTablet(Context context) {
//        return (context.getResources().getConfiguration().screenLayout
//                & Configuration.SCREENLAYOUT_SIZE_MASK) >= Configuration.SCREENLAYOUT_SIZE_XLARGE;
//    }
//
//    /**
//     * Binds a preference's summary to its value. More specifically, when the
//     * preference's value is changed, its summary (line of text below the
//     * preference title) is updated to reflect the value. The summary is also
//     * immediately updated upon calling this method. The exact display format is
//     * dependent on the type of preference.
//     *
//     * @see #sBindPreferenceSummaryToValueListener
//     */
//    private static void bindPreferenceSummaryToValue(Preference preference) {
//        // Set the listener to watch for value changes.
//        preference.setOnPreferenceChangeListener(sBindPreferenceSummaryToValueListener);
//
//        // Trigger the listener immediately with the preference's
//        // current value.
//        sBindPreferenceSummaryToValueListener.onPreferenceChange(preference,
//                PreferenceManager
//                        .getDefaultSharedPreferences(preference.getContext())
//                        .getString(preference.getKey(), ""));
//    }
//
//
//    /**
//     * {@inheritDoc}
//     */
//    @Override
//    public boolean onIsMultiPane() {
//        return isXLargeTablet(this);
//    }
//
//    /**
//     * {@inheritDoc}
//     */
//    @Override
//    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
//    public void onBuildHeaders(List<PreferenceActivity.Header> target) {
//        loadHeadersFromResource(R.xml.pref_headers, target);
//    }
//
//    /**
//     * This method stops fragment injection in malicious applications.
//     * Make sure to deny any unknown fragments here.
//     */
//    protected boolean isValidFragment(String fragmentName) {
//        return PreferenceFragment.class.getName().equals(fragmentName)
//                || AccountPreferenceFragment.class.getName().equals(fragmentName)
//                || NotificationPreferenceFragment.class.getName().equals(fragmentName)
//                || MediaPreferenceFragment.class.getName().equals(fragmentName)
//                || PrivacyPreferenceFragment.class.getName().equals(fragmentName)
//                || InfoPreferenceFragment.class.getName().equals(fragmentName)
//                || LanguagePreferenceFragment.class.getName().equals(fragmentName);
//    }
//
//    /**
//     * This fragment shows general preferences only. It is used when the
//     * activity is showing a two-pane settings UI.
//     */
//    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
//    public static class AccountPreferenceFragment extends PreferenceFragment {
//        @Override
//        public void onCreate(Bundle savedInstanceState) {
//            super.onCreate(savedInstanceState);
//            addPreferencesFromResource(R.xml.pref_account);
//            setHasOptionsMenu(true);
//
//            Preference editProfilePreference = findPreference("account_edit_profile");
//            editProfilePreference.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
//                @Override
//                public boolean onPreferenceClick(Preference preference) {
//                    if (!UserInfo.isLogin()) {
//                        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
//                        builder.setTitle(getResources().getString(R.string.pref_account_edit_profile_title));
//                        builder.setMessage(getResources().getString(R.string.msg_have_not_login));
//                        builder.setIcon(R.drawable.ic_message);
//                        builder.setPositiveButton(getString(android.R.string.ok),
//                                new DialogInterface.OnClickListener() {
//                                    @Override
//                                    public void onClick(DialogInterface dialog, int which) {
//                                        // do the work to update the preference ---
//                                    }
//                                });
//                        builder.create().show();
//                        return true;
//                    }
//
//                    EditAccountActivity.open(getActivity());
//                    return true;
//                }
//            });
//
//            Preference changePasswordProfilePreference = findPreference("account_change_password");
//            changePasswordProfilePreference.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
//                @Override
//                public boolean onPreferenceClick(Preference preference) {
//                    if (!UserInfo.isLogin()) {
//                        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
//                        builder.setTitle(getResources().getString(R.string.pref_account_change_password_title));
//                        builder.setMessage(getResources().getString(R.string.msg_have_not_login));
//                        builder.setIcon(R.drawable.ic_message);
//                        builder.setPositiveButton(getString(android.R.string.ok),
//                                new DialogInterface.OnClickListener() {
//                                    @Override
//                                    public void onClick(DialogInterface dialog, int which) {
//                                        // do the work to update the preference ---
//                                    }
//                                });
//                        builder.create().show();
//                        return true;
//                    }
//
//                    ChangePasswordActivity.open(getActivity());
//                    return true;
//                }
//            });
//
//            Preference logOutPreference = findPreference("account_log_out");
//            logOutPreference.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
//                @Override
//                public boolean onPreferenceClick(Preference preference) {
//                    if (!UserInfo.isLogin()) {
//                        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
//                        builder.setTitle(getResources().getString(R.string.pref_account_log_out_title));
//                        builder.setMessage(getResources().getString(R.string.msg_have_not_login));
//                        builder.setIcon(R.drawable.ic_message);
//                        builder.setPositiveButton(getString(android.R.string.ok),
//                                new DialogInterface.OnClickListener() {
//                                    @Override
//                                    public void onClick(DialogInterface dialog, int which) {
//                                        // do the work to update the preference ---
//                                    }
//                                });
//                        builder.create().show();
//                        return true;
//                    }else {
//                        UserInfo.logOut(getActivity());
//                        Intent intent = new Intent(Constants.ACTION_LOGOUT);
//
//                        LoginActivity.open(getActivity());
//                        return true;
//                    }
//                }
//            });
//
//            // Bind the summaries of EditText/List/Dialog/Ringtone preferences
//            // to their values. When their values change, their summaries are
//            // updated to reflect the new value, per the Android Design
//            // guidelines.
////            bindPreferenceSummaryToValue(findPreference("example_text"));
////            bindPreferenceSummaryToValue(findPreference("example_list"));
//        }
//
//        @Override
//        public boolean onOptionsItemSelected(MenuItem item) {
//            int id = item.getItemId();
//            if (id == android.R.id.home) {
//                startActivity(new Intent(getActivity(), SettingsActivity.class));
//                return true;
//            }
//            return super.onOptionsItemSelected(item);
//        }
//    }
//
//    /**
//     * This fragment shows notification preferences only. It is used when the
//     * activity is showing a two-pane settings UI.
//     */
//    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
//    public static class NotificationPreferenceFragment extends PreferenceFragment {
//        @Override
//        public void onCreate(Bundle savedInstanceState) {
//            super.onCreate(savedInstanceState);
//            addPreferencesFromResource(R.xml.pref_notification);
//            setHasOptionsMenu(true);
//
//            // Bind the summaries of EditText/List/Dialog/Ringtone preferences
//            // to their values. When their values change, their summaries are
//            // updated to reflect the new value, per the Android Design
//            // guidelines.
//            bindPreferenceSummaryToValue(findPreference("notifications_new_message_ringtone"));
//        }
//
//        @Override
//        public boolean onOptionsItemSelected(MenuItem item) {
//            int id = item.getItemId();
//            if (id == android.R.id.home) {
//                startActivity(new Intent(getActivity(), SettingsActivity.class));
//                return true;
//            }
//            return super.onOptionsItemSelected(item);
//        }
//    }
//
//    /**
//     * This fragment shows data and sync preferences only. It is used when the
//     * activity is showing a two-pane settings UI.
//     */
//    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
//    public static class MediaPreferenceFragment extends PreferenceFragment {
//        @Override
//        public void onCreate(Bundle savedInstanceState) {
//            super.onCreate(savedInstanceState);
//            addPreferencesFromResource(R.xml.pref_media);
//            setHasOptionsMenu(true);
//
//            // Bind the summaries of EditText/List/Dialog/Ringtone preferences
//            // to their values. When their values change, their summaries are
//            // updated to reflect the new value, per the Android Design
//            // guidelines.
////            bindPreferenceSummaryToValue(findPreference("sync_frequency"));
//        }
//
//        @Override
//        public boolean onOptionsItemSelected(MenuItem item) {
//            int id = item.getItemId();
//            if (id == android.R.id.home) {
//                startActivity(new Intent(getActivity(), SettingsActivity.class));
//                return true;
//            }
//            return super.onOptionsItemSelected(item);
//        }
//    }
//
//    /**
//     * This fragment shows data and sync preferences only. It is used when the
//     * activity is showing a two-pane settings UI.
//     */
//    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
//    public static class PrivacyPreferenceFragment extends PreferenceFragment {
//        @Override
//        public void onCreate(Bundle savedInstanceState) {
//            super.onCreate(savedInstanceState);
//            addPreferencesFromResource(R.xml.pref_privacy);
//            setHasOptionsMenu(true);
//
//            // Bind the summaries of EditText/List/Dialog/Ringtone preferences
//            // to their values. When their values change, their summaries are
//            // updated to reflect the new value, per the Android Design
//            // guidelines.
////            bindPreferenceSummaryToValue(findPreference("sync_frequency"));
//        }
//
//        @Override
//        public boolean onOptionsItemSelected(MenuItem item) {
//            int id = item.getItemId();
//            if (id == android.R.id.home) {
//                startActivity(new Intent(getActivity(), SettingsActivity.class));
//                return true;
//            }
//            return super.onOptionsItemSelected(item);
//        }
//    }
//
//    /**
//     * This fragment shows data and sync preferences only. It is used when the
//     * activity is showing a two-pane settings UI.
//     */
//    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
//    public static class LanguagePreferenceFragment extends PreferenceFragment {
//        @Override
//        public void onCreate(Bundle savedInstanceState) {
//            super.onCreate(savedInstanceState);
//            addPreferencesFromResource(R.xml.pref_language);
//            setHasOptionsMenu(true);
//
//            // Bind the summaries of EditText/List/Dialog/Ringtone preferences
//            // to their values. When their values change, their summaries are
//            // updated to reflect the new value, per the Android Design
//            // guidelines.
//            bindPreferenceSummaryToValue(findPreference("change_language"));
//        }
//
//        @Override
//        public boolean onOptionsItemSelected(MenuItem item) {
//            int id = item.getItemId();
//            if (id == android.R.id.home) {
//                startActivity(new Intent(getActivity(), SettingsActivity.class));
//                return true;
//            }
//            return super.onOptionsItemSelected(item);
//        }
//    }
//
//    /**
//     * This fragment shows data and sync preferences only. It is used when the
//     * activity is showing a two-pane settings UI.
//     */
//    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
//    public static class InfoPreferenceFragment extends PreferenceFragment {
//        @Override
//        public void onCreate(Bundle savedInstanceState) {
//            super.onCreate(savedInstanceState);
//            addPreferencesFromResource(R.xml.pref_info);
//            setHasOptionsMenu(true);
//
////            Preference infoAskQuestions = findPreference("info_ask_questions");
////            infoAskQuestions.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
////                @Override
////                public boolean onPreferenceClick(Preference preference) {
////                    if (!UserInfo.isLogin()) {
////                        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
////                        builder.setTitle(getResources().getString(R.string.pref_info_ask_questions_title));
////                        builder.setMessage(getResources().getString(R.string.msg_have_not_login));
////                        builder.setIcon(R.drawable.ic_message);
////                        builder.setPositiveButton(getString(android.R.string.ok),
////                                new DialogInterface.OnClickListener() {
////                                    @Override
////                                    public void onClick(DialogInterface dialog, int which) {
////                                        // do the work to update the preference ---
////                                    }
////                                });
////                        builder.create().show();
////                        return true;
////                    }
////
////                    AskQuestionActivity.openChannel(getActivity());
////                    return true;
////                }
////            });
////
////            Preference infoShowQuestions = findPreference("info_my_questions");
////            infoShowQuestions.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
////                @Override
////                public boolean onPreferenceClick(Preference preference) {
////                    MyQuestionActivity.openChannel(getActivity());
////                    return true;
////                }
////            });
//
//            Preference infoIduhocService = findPreference("info_iduhoc_service");
//            infoIduhocService.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
//                @Override
//                public boolean onPreferenceClick(Preference preference) {
//                    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
//                    builder.setMessage("VĂN PHÒNG HÀ NỘI\n" +
//                            "Địa chỉ: số 15/ ngách 26/ phố Nghĩa Đô (ngõ 100 Hoàng Quốc Việt cũ), quận Cầu Giấy, Hà Nội\n" +
//                            "Điện thoại: 0903 405 535 (Mr. Kiên)\n" +
//                            "\n" +
//                            "CHI NHÁNH HẢI PHÒNG\n" +
//                            "Địa chỉ: số 7, ngõ 275 Lê Lợi, quận Ngô Quyền, Hải Phòng\n" +
//                            "Điện thoại: 0904 884 774 (Mr.Việt)\n" +
//                            "\n" +
//                            "Email: kienkhuc@gmail.com\n" +
//                            "Website: http://iduhoc.vn\n" +
//                            "Facebook: https://www.facebook.com/kienkhuc.ideutsch");
//                    builder.setTitle(getResources().getString(R.string.pref_info_iduhoc_service_title));
//                    builder.setIcon(R.drawable.iduhoc_logo_114x60);
//                    builder.setPositiveButton(getString(android.R.string.ok),
//                            new DialogInterface.OnClickListener() {
//                                @Override
//                                public void onClick(DialogInterface dialog, int which) {
//                                    // do the work to update the preference ---
//                                }
//                            });
//                    builder.create().show();
//                    return true;
//                }
//            });
//
//            //openChannel app on google play
//            Preference infoErrorReport = findPreference("info_error_reports");
//            infoErrorReport.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
//                @Override
//                public boolean onPreferenceClick(Preference preference) {
//                    String appPackageName = getActivity().getPackageName(); // getPackageName() from Context or Activity object
//                    try {
//                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
//                    } catch (android.content.ActivityNotFoundException anfe) {
//                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
//                    }
//                    return true;
//                }
//            });
//
//            Preference appVersion = (Preference) findPreference("info_app_version");
//            if (appVersion != null) {
//                int versionCode = BuildConfig.VERSION_CODE;
//                String versionName = BuildConfig.VERSION_NAME;
//                SettingsActivity settingsActivity = new SettingsActivity();
////                appVersion.setSummary(settingsActivity.getAppVersion());
//                appVersion.setSummary(versionName);
//            }
//
//            // Bind the summaries of EditText/List/Dialog/Ringtone preferences
//            // to their values. When their values change, their summaries are
//            // updated to reflect the new value, per the Android Design
//            // guidelines.
////            bindPreferenceSummaryToValue(findPreference("sync_frequency"));
//        }
//
//        @Override
//        public boolean onOptionsItemSelected(MenuItem item) {
//            int id = item.getItemId();
//            if (id == android.R.id.home) {
//                startActivity(new Intent(getActivity(), SettingsActivity.class));
//                return true;
//            }
//            return super.onOptionsItemSelected(item);
//        }
//
//    }
//
//    public String getAppVersion() {
//        try {
//            PackageInfo packageInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
//            String version = packageInfo.versionName;
//            return version;
//        } catch (Exception e) {
//            Log.d(TAG, "getAppVersion: " + e.getMessage());
//            return Constants.APP_VERSION;
//        }
//    }
//
//    //handle events when user click on preference headers
//    @Override
//    public void onHeaderClick(Header header, int position) {
//        super.onHeaderClick(header, position);
//        if (header.id == R.id.settings_language) {
//            AlertDialog.Builder builderSingle = new AlertDialog.Builder(SettingsActivity.this);
//            builderSingle.setIcon(R.drawable.ic_language_black_24dp);
//            builderSingle.setTitle(getString(R.string.select_language_title));
//
//            final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(SettingsActivity.this, android.R.layout.select_dialog_singlechoice);
//            arrayAdapter.add("English");//order number = 0
//            arrayAdapter.add("Tiếng Việt");//order number = 1
//
//            String currentLanguage = LanguageUtils.getAppLanguageSetting(getActivity());
//            int selectedLanguage = 0;
//            if (currentLanguage.equals("vi"))
//                selectedLanguage = 1;
//
//            builderSingle.setSingleChoiceItems(arrayAdapter, selectedLanguage, new DialogInterface.OnClickListener() {
//                @Override
//                public void onClick(DialogInterface dialog, int which) {
//                    String language = arrayAdapter.getItem(which);
//                    if (language == null) return;
//                    if (language.toLowerCase().equals("english")) {
//                        LanguageUtils.changeLanguage(getActivity(), "en");
//                    } else {
//                        LanguageUtils.changeLanguage(getActivity(), "vi");
//                    }
//                    dialog.dismiss();
//
//                    AlertDialog.Builder builderInner = new AlertDialog.Builder(SettingsActivity.this);
//                    builderInner.setMessage(getString(R.string.select_language_notify_after_selected));
//                    builderInner.setTitle(getString(R.string.select_language_notify_title));
//                    builderInner.setPositiveButton(R.string.select_language_ok, new DialogInterface.OnClickListener() {
//                        @Override
//                        public void onClick(DialogInterface dialog, int which) {
//                            dialog.dismiss();
////                            Intent i = getBaseContext().getPackageManager().getLaunchIntentForPackage(getBaseContext().getPackageName());
////                            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
////                            startActivity(i);
//
//                            Intent mStartActivity = new Intent(getBaseContext(), MainActivity.class);
//                            int mPendingIntentId = 123456;
//                            PendingIntent mPendingIntent = PendingIntent.getActivity(getBaseContext(), mPendingIntentId, mStartActivity, PendingIntent.FLAG_CANCEL_CURRENT);
//                            AlarmManager mgr = (AlarmManager) getBaseContext().getSystemService(Context.ALARM_SERVICE);
//                            mgr.set(AlarmManager.RTC, System.currentTimeMillis() + 100, mPendingIntent);
//                            System.exit(0);
//                        }
//                    });
//                    builderInner.show();
//                }
//            });
//
//            builderSingle.show();
//        }
//    }
//
//}
