package com.team500ae.vncapplication.activities.settings;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.TextView;

import com.google.gson.JsonObject;
import com.team500ae.library.utils.ConvertUtils;
import com.team500ae.vncapplication.R;
import com.team500ae.vncapplication.base.BaseActivity;
import com.team500ae.vncapplication.data_access.user.UserInfo;
import com.team500ae.vncapplication.models.BaseReturnFunctionEntity;
import com.team500ae.vncapplication.utils.FlagAttributeUtils;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by phuong.ndp on 23/06/2018.
 */

public class PrivacySettingsActivity extends BaseActivity {

    @BindView(R.id.layout_show_avatar)
    View vShowAvatar;
    @BindView(R.id.layout_receive_mess_from_stranger)
    View vReceiveMessFromStranger;
    @BindView(R.id.layout_show_your_phone_number)
    View vShowYourPhoneNumber;
    @BindView(R.id.layout_share_your_location)
    View vShareYourLocation;

    //    private Switch swtShareOnlineStatus;
    private Switch swtShowAvatar, swtReceiveMessFromStranger, swtShowYourPhoneNumber, swtShareYourLocation;
    private boolean showAvatar, receiveMessFromStranger, showYourPhoneNumber, shareYourLocation;

    public static void open(Context activity) {
        Intent intent = new Intent(activity, PrivacySettingsActivity.class);
        activity.startActivity(intent);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        setContentView(R.layout.activity_privacy_settings);
        super.onCreate(savedInstanceState);
        showBackButton();

        initLayout();
        int settingAttributeValue = ConvertUtils.toInt(UserInfo.getCurrentUser().get("SettingAttribute"));
        getCurrentSettings();

//        swtShareOnlineStatus.setChecked(shareOnlineStatus);
        swtShowAvatar.setChecked(showAvatar);
        swtReceiveMessFromStranger.setChecked(receiveMessFromStranger);
        swtShowYourPhoneNumber.setChecked(showYourPhoneNumber);
        swtShareYourLocation.setChecked(FlagAttributeUtils.getAttribute(settingAttributeValue, FlagAttributeUtils.SHARE_YOUR_LOCATION));

//        shareOnlineStatusEvent();
        showAvatarEvent();
        receiveMessFromStrangerEvent();
        showYourPhoneNumberEvent();
        showNotificationEvent(FlagAttributeUtils.SHARE_YOUR_LOCATION, swtShareYourLocation);
    }

    private void initLayout() {
        TextView tvShowYourPhotoTitle = (TextView) vShowAvatar.findViewById(R.id.tv_title);
        tvShowYourPhotoTitle.setText(getResources().getString(R.string.pref_privacy_show_your_photo_title));
        TextView tvShowYourPhotoSubTitle = (TextView) vShowAvatar.findViewById(R.id.tv_sub_title);
        tvShowYourPhotoSubTitle.setText(getResources().getString(R.string.pref_privacy_show_your_photo_summary));
        swtShowAvatar = (Switch) vShowAvatar.findViewById(R.id.switch_block);

        TextView tvReceiveMessFromStrangerTitle = (TextView) vReceiveMessFromStranger.findViewById(R.id.tv_title);
        tvReceiveMessFromStrangerTitle.setText(getResources().getString(R.string.pref_privacy_receive_message_from_stranger_title));
        TextView tvReceiveMessFromStrangerSubTitle = (TextView) vReceiveMessFromStranger.findViewById(R.id.tv_sub_title);
        tvReceiveMessFromStrangerSubTitle.setText(getResources().getString(R.string.pref_privacy_receive_message_from_stranger_summary));
        swtReceiveMessFromStranger = (Switch) vReceiveMessFromStranger.findViewById(R.id.switch_block);

        TextView tvShowYourPhoneNumberTitle = (TextView) vShowYourPhoneNumber.findViewById(R.id.tv_title);
        tvShowYourPhoneNumberTitle.setText(getResources().getString(R.string.pref_privacy_show_your_phone_number_title));
        TextView tvShowYourPhoneNumberSubTitle = (TextView) vShowYourPhoneNumber.findViewById(R.id.tv_sub_title);
        tvShowYourPhoneNumberSubTitle.setText(getResources().getString(R.string.pref_privacy_show_your_phone_number_summary));
        swtShowYourPhoneNumber = (Switch) vShowYourPhoneNumber.findViewById(R.id.switch_block);

        TextView tvShareYourLocationTitle = (TextView) vShareYourLocation.findViewById(R.id.tv_title);
        tvShareYourLocationTitle.setText(getResources().getString(R.string.pref_privacy_share_your_location_title));
        TextView tvShareYourLocationSubTitle = (TextView) vShareYourLocation.findViewById(R.id.tv_sub_title);
        tvShareYourLocationSubTitle.setText(getResources().getString(R.string.pref_privacy_share_your_location_summary));
        swtShareYourLocation = (Switch) vShareYourLocation.findViewById(R.id.switch_block);
    }

    private void getCurrentSettings() {
        JsonObject currentUser = UserInfo.getCurrentUser();
        showAvatar = ConvertUtils.toBoolean(currentUser.get("ShowAvatar"));
        receiveMessFromStranger = ConvertUtils.toBoolean(currentUser.get("ReceiveStrangeMessage"));
        showYourPhoneNumber = ConvertUtils.toBoolean(currentUser.get("ShowPhoneNumber"));
    }

//    private void shareOnlineStatusEvent() {
//        swtShareOnlineStatus.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                AlertDialog.Builder builder = new AlertDialog.Builder(PrivacySettingsActivity.this);
//                builder.setIcon(R.drawable.ic_info);
//                builder.setTitle(getResources().getString(R.string.pref_privacy_share_online_status_title));
//                if (shareOnlineStatus)
//                    builder.setMessage(getResources().getString(R.string.notice_disallow_share_online_status));
//                else
//                    builder.setMessage(getResources().getString(R.string.notice_allow_share_online_status));
//                builder.setCancelable(false);
//                builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
//                    @Override
//                    public void onClick(DialogInterface dialog, int which) {
//                        //update switch status
//                        boolean desireStatus=!shareOnlineStatus;
//
//                        //call API to update
//                        new AsyncTask<Void, Void, BaseReturnFunctionEntity<Boolean>>() {
//                            ProgressDialog progDialog = new ProgressDialog(PrivacySettingsActivity.this);
//
//                            @Override
//                            protected void onPreExecute() {
//                                super.onPreExecute();
//                                progDialog.setMessage(getString(R.string.activity_update_user_privacy_setting));
//                                progDialog.setIndeterminate(false);
//                                progDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
//                                progDialog.setCancelable(true);
//                                progDialog.show();
//                            }
//
//                            @Override
//                            protected BaseReturnFunctionEntity<Boolean> doInBackground(Void... voids) {
//                                return UserInfo.shareOnlineStatusSetting(desireStatus);
//                            }
//
//                            @Override
//                            protected void onPostExecute(BaseReturnFunctionEntity<Boolean> result) {
//                                super.onPostExecute(result);
//                                if (progDialog.isShowing()) {
//                                    progDialog.dismiss();
//                                }
//
//                                if (result.isTrue()) {
//                                    shareOnlineStatus = desireStatus;
//                                    swtShareOnlineStatus.setChecked(desireStatus);
//                                    UserInfo.updateLocalShareOnlineStatusValue(getActivity(),desireStatus);
//                                    showSnackBar(R.string.activity_update_user_privacy_setting_successfully);
//                                } else {
//                                    showSnackBar(R.string.activity_update_user_privacy_setting_failed + ": " + result.getMessage());
//                                }
//                            }
//                        }.execute();
//                    }
//                });
//                builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
//                    @Override
//                    public void onClick(DialogInterface dialog, int which) {
//                        //do nothing
//                    }
//                });
//                builder.show();
//            }
//        });
//
//        swtShareOnlineStatus.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
//            @Override
//            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
//                //keep current status. Only change status after user click confirm to dialog (see OnClickListener)
//                swtShareOnlineStatus.setChecked(shareOnlineStatus);
//            }
//        });
//    }

    private void showAvatarEvent() {
        swtShowAvatar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(PrivacySettingsActivity.this);
                builder.setIcon(R.drawable.ic_info);
                builder.setTitle(getResources().getString(R.string.pref_privacy_show_your_photo_title));
                if (showAvatar)
                    builder.setMessage(getResources().getString(R.string.notice_disallow_show_your_photo));
                else
                    builder.setMessage(getResources().getString(R.string.notice_allow_show_your_photo));
                builder.setCancelable(false);
                builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        //update switch status
                        boolean desireStatus = !showAvatar;

                        //call API to update
                        new AsyncTask<Void, Void, BaseReturnFunctionEntity<Boolean>>() {
                            ProgressDialog progDialog = new ProgressDialog(PrivacySettingsActivity.this);

                            @Override
                            protected void onPreExecute() {
                                super.onPreExecute();
                                progDialog.setMessage(getString(R.string.activity_update_user_privacy_setting));
                                progDialog.setIndeterminate(false);
                                progDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                                progDialog.setCancelable(true);
                                progDialog.show();
                            }

                            @Override
                            protected BaseReturnFunctionEntity<Boolean> doInBackground(Void... voids) {
                                return UserInfo.showAvatarSetting(desireStatus);
                            }

                            @Override
                            protected void onPostExecute(BaseReturnFunctionEntity<Boolean> result) {
                                super.onPostExecute(result);
                                if (progDialog.isShowing()) {
                                    progDialog.dismiss();
                                }

                                if (result.isTrue()) {
                                    showAvatar = desireStatus;
                                    swtShowAvatar.setChecked(desireStatus);
                                    UserInfo.updateLocalShowAvatarSettingValue(getActivity(), desireStatus);
                                    showSnackBar(R.string.activity_update_user_privacy_setting_successfully);
                                } else {
                                    showSnackBar(R.string.activity_update_user_privacy_setting_failed + ": " + result.getMessage());
                                }
                            }
                        }.execute();
                    }
                });
                builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        //do nothing
                    }
                });
                builder.show();
            }
        });

        swtShowAvatar.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                //keep current status. Only change status after user click confirm to dialog (see OnClickListener)
                swtShowAvatar.setChecked(showAvatar);
            }
        });
    }

    private void receiveMessFromStrangerEvent() {
        swtReceiveMessFromStranger.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(PrivacySettingsActivity.this);
                builder.setIcon(R.drawable.ic_info);
                builder.setTitle(getResources().getString(R.string.pref_privacy_receive_message_from_stranger_title));
                if (receiveMessFromStranger)
                    builder.setMessage(getResources().getString(R.string.notice_disallow_receive_strange_message));
                else
                    builder.setMessage(getResources().getString(R.string.notice_allow_receive_strange_message));
                builder.setCancelable(false);
                builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        //update switch status
                        boolean desireStatus = !receiveMessFromStranger;

                        //call API to update
                        new AsyncTask<Void, Void, BaseReturnFunctionEntity<Boolean>>() {
                            ProgressDialog progDialog = new ProgressDialog(PrivacySettingsActivity.this);

                            @Override
                            protected void onPreExecute() {
                                super.onPreExecute();
                                progDialog.setMessage(getString(R.string.activity_update_user_privacy_setting));
                                progDialog.setIndeterminate(false);
                                progDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                                progDialog.setCancelable(true);
                                progDialog.show();
                            }

                            @Override
                            protected BaseReturnFunctionEntity<Boolean> doInBackground(Void... voids) {
                                return UserInfo.receiveStrangeMessageSetting(desireStatus);
                            }

                            @Override
                            protected void onPostExecute(BaseReturnFunctionEntity<Boolean> result) {
                                super.onPostExecute(result);
                                if (progDialog.isShowing()) {
                                    progDialog.dismiss();
                                }

                                if (result.isTrue()) {
                                    receiveMessFromStranger = desireStatus;
                                    swtReceiveMessFromStranger.setChecked(desireStatus);
                                    UserInfo.updateLocalReceiveStrangeMessageSettingValue(getActivity(), desireStatus);
                                    showSnackBar(R.string.activity_update_user_privacy_setting_successfully);
                                } else {
                                    showSnackBar(R.string.activity_update_user_privacy_setting_failed + ": " + result.getMessage());
                                }
                            }
                        }.execute();
                    }
                });
                builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        //do nothing
                    }
                });
                builder.show();
            }
        });

        swtReceiveMessFromStranger.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                //keep current status. Only change status after user click confirm to dialog (see OnClickListener)
                swtReceiveMessFromStranger.setChecked(receiveMessFromStranger);
            }
        });
    }

    private void showYourPhoneNumberEvent() {
        swtShowYourPhoneNumber.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(PrivacySettingsActivity.this);
                builder.setIcon(R.drawable.ic_info);
                builder.setTitle(getResources().getString(R.string.pref_privacy_show_your_phone_number_title));
                if (showYourPhoneNumber)
                    builder.setMessage(getResources().getString(R.string.notice_disallow_show_your_phone_number));
                else
                    builder.setMessage(getResources().getString(R.string.notice_allow_show_your_phone_number));
                builder.setCancelable(false);
                builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        //update switch status
                        boolean desireStatus = !showYourPhoneNumber;

                        //call API to update
                        new AsyncTask<Void, Void, BaseReturnFunctionEntity<Boolean>>() {
                            ProgressDialog progDialog = new ProgressDialog(PrivacySettingsActivity.this);

                            @Override
                            protected void onPreExecute() {
                                super.onPreExecute();
                                progDialog.setMessage(getString(R.string.activity_update_user_privacy_setting));
                                progDialog.setIndeterminate(false);
                                progDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                                progDialog.setCancelable(true);
                                progDialog.show();
                            }

                            @Override
                            protected BaseReturnFunctionEntity<Boolean> doInBackground(Void... voids) {
                                return UserInfo.showYourPhoneNumberSetting(desireStatus);
                            }

                            @Override
                            protected void onPostExecute(BaseReturnFunctionEntity<Boolean> result) {
                                super.onPostExecute(result);
                                if (progDialog.isShowing()) {
                                    progDialog.dismiss();
                                }

                                if (result.isTrue()) {
                                    showYourPhoneNumber = desireStatus;
                                    swtShowYourPhoneNumber.setChecked(desireStatus);
                                    UserInfo.updateLocalShowYourPhoneNumberSettingValue(getActivity(), desireStatus);
                                    showSnackBar(R.string.activity_update_user_privacy_setting_successfully);
                                } else {
                                    showSnackBar(R.string.activity_update_user_privacy_setting_failed + ": " + result.getMessage());
                                }
                            }
                        }.execute();
                    }
                });
                builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        //do nothing
                    }
                });
                builder.show();
            }
        });

        swtShowYourPhoneNumber.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                //keep current status. Only change status after user click confirm to dialog (see OnClickListener)
                swtShowYourPhoneNumber.setChecked(showYourPhoneNumber);
            }
        });
    }
    private void showNotificationEvent(int value, Switch swt) {
        swt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int settingAttributeValue = ConvertUtils.toInt(UserInfo.getCurrentUser().get("SettingAttribute"));
                boolean desireStatus = FlagAttributeUtils.getAttribute(settingAttributeValue, value);

                //update switch status
                int desireValue = desireStatus ? (settingAttributeValue ^ value) : (settingAttributeValue | value);

                //call API to update
                new AsyncTask<Void, Void, BaseReturnFunctionEntity<Boolean>>() {
                    ProgressDialog progDialog = new ProgressDialog(PrivacySettingsActivity.this);

                    @Override
                    protected void onPreExecute() {
                        super.onPreExecute();
                        progDialog.setMessage(getString(R.string.activity_update_user_notification_setting));
                        progDialog.setIndeterminate(false);
                        progDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                        progDialog.setCancelable(true);
                        progDialog.show();
                    }

                    @Override
                    protected BaseReturnFunctionEntity<Boolean> doInBackground(Void... voids) {
                        try {
                            Thread.sleep(2000);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        return UserInfo.updateSettingAttribute(desireValue);
                    }

                    @Override
                    protected void onPostExecute(BaseReturnFunctionEntity<Boolean> result) {
                        super.onPostExecute(result);
                        if (progDialog.isShowing()) {
                            progDialog.dismiss();
                        }

                        if (result.isTrue()) {
                            UserInfo.updateLocalSettingAttribute(getActivity(), desireValue);
                            swt.setChecked(!desireStatus);
                            showSnackBar(R.string.activity_update_user_notification_setting_successfully);
                        } else {
                            swt.setChecked(desireStatus);
                            showSnackBar(R.string.activity_update_user_notification_setting_failed + ": " + result.getMessage());
                        }
                    }
                }.execute();
            }
        });
    }
}
