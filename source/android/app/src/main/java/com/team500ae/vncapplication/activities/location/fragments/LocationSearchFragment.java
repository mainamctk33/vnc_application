package com.team500ae.vncapplication.activities.location.fragments;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.UiThread;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStates;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.location.places.GeoDataClient;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.PlaceDetectionClient;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.team500ae.library.adapters.BaseRecyclerAdapter;
import com.team500ae.library.utils.AndroidPermissionUtils;
import com.team500ae.library.utils.ConvertUtils;
import com.team500ae.library.utils.ImageUtils;
import com.team500ae.vncapplication.R;
import com.team500ae.vncapplication.activities.location.AddNewLocationActivity;
import com.team500ae.vncapplication.activities.location.DetailLocationActivity;
import com.team500ae.vncapplication.activities.location.ListBookmarkLocationActivity;
import com.team500ae.vncapplication.activities.location.MgrCategoryActivity;
import com.team500ae.vncapplication.activities.location.MyLocationActivity;
import com.team500ae.vncapplication.activities.login.LoginActivity;
import com.team500ae.vncapplication.base.BaseActivity;
import com.team500ae.vncapplication.constants.Constants;
import com.team500ae.vncapplication.data_access.RealmInfo;
import com.team500ae.vncapplication.data_access.location.LocationInfo;
import com.team500ae.vncapplication.data_access.location.TypeLocationInfo;
import com.team500ae.vncapplication.data_access.user.UserInfo;
import com.team500ae.vncapplication.models.BaseReturnFunctionEntity;
import com.team500ae.vncapplication.models.realm_models.location.TypeLocationEntity;
import com.team500ae.vncapplication.services.ServiceLocation;
import com.team500ae.vncapplication.utils.GPSTrackerUtils;
import com.team500ae.vncapplication.utils.LanguageUtils;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.realm.Realm;

/**
 * Created by maina on 1/29/2018.
 */

public class LocationSearchFragment extends Fragment implements OnMapReadyCallback, GoogleMap.OnMarkerClickListener, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {
    private static final float DEFAULT_ZOOM = 15f;
    protected static final int REQUEST_CHECK_SETTINGS = 0x1;
    private final static int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
    private Place place;
    private FusedLocationProviderClient mFusedLocationProviderClient;
    private GeoDataClient mGeoDataClient;
    private PlaceDetectionClient mPlaceDetectionClient;
    private GoogleMap mMap;
    private Location mLastKnownLocation;
    private GoogleApiClient mGoogleApiClient;
    private BaseRecyclerAdapter<ViewHolderType, TypeLocationEntity> adapter;
    private ArrayList<TypeLocationEntity> myTypeLocation;
    private BaseReturnFunctionEntity<ArrayList<TypeLocationEntity>> myListTypeLocation;
    private TypeLocationInfo typeLocationInfo;
    private Realm realm;
    private View bottomSheet;
    private View mapView;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        myTypeLocation = new ArrayList<>();
        realm = RealmInfo.getRealm(getContext());

        View view = inflater.inflate(R.layout.fragment_location_search, container, false);

        ButterKnife.bind(this, view);
        try {
            ((BaseActivity) getContext()).getSupportActionBar().hide();
        } catch (Exception e) {
            e.printStackTrace();
        }

        SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);
        mapView = mapFragment.getView();

        mapFragment.getMapAsync(this);
        mGoogleApiClient = new GoogleApiClient.Builder(getActivity())
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();
        mGoogleApiClient.connect();


// Construct a GeoDataClient.
        mGeoDataClient = Places.getGeoDataClient(getContext(), null);

        // Construct a PlaceDetectionClient.
        mPlaceDetectionClient = Places.getPlaceDetectionClient(getContext(), null);

        // Construct a FusedLocationProviderClient.
        mFusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(getContext());
        //Show list of location types on the bottom sheet
        recyclerViewTypeLocation.setLayoutManager(new GridLayoutManager(getContext(), 3));
        adapter = new BaseRecyclerAdapter<ViewHolderType, TypeLocationEntity>(R.layout.item_type_location, new BaseRecyclerAdapter.BaseViewHolder<ViewHolderType, TypeLocationEntity>() {
            @Override
            public ViewHolderType getViewHolder(View v) {
                return new ViewHolderType(v);
            }

            @Override
            public void bindData(RecyclerView recyclerView, ViewHolderType viewHolder, TypeLocationEntity data, int position) {
                if (data != null) {
                    viewHolder.tvName.setVisibility(View.VISIBLE);
                    viewHolder.imageView.setVisibility(View.VISIBLE);
                    viewHolder.btnAdd.setVisibility(View.GONE);

                    viewHolder.tvName.setText(LanguageUtils.getAppLanguageSetting(getActivity()).equals("en") ? data.getName() : data.getEnName());
                    if (data.getId() == -1) {
                        viewHolder.imageView.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.ic_bookmark));
                    } else if (data.getId() == -2) {
                        viewHolder.imageView.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.ic_my_location));
                    } else
                        ImageUtils.loadImageByGlide(getContext(), false, 0, 0, data.getLogo(), R.drawable.img_no_image, R.drawable.img_no_image, viewHolder.imageView, true);
                } else {
                    viewHolder.tvName.setVisibility(View.GONE);
                    viewHolder.imageView.setVisibility(View.GONE);
                    viewHolder.btnAdd.setVisibility(View.VISIBLE);
                }
            }
        }, myTypeLocation, new BaseRecyclerAdapter.OnClickListener() {
            @Override
            public void onClick(View v, int position, Object o) {
                if (UserInfo.isLogin()) {
                    if (position == 0) {
                        ListBookmarkLocationActivity.open(getContext());
                        return;
                    }
                    if (position == 1) {
                        MyLocationActivity.open(getContext());
                        return;
                    }
                }
                if (o == null) {
                    MgrCategoryActivity.open(getActivity());
                    return;
                }
                TypeLocationEntity jsonObject = (TypeLocationEntity) o;
                try {
                    getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.lnFragment, LocationSearchByTypeFragment.getInstants(jsonObject.getId(), jsonObject.getName()), "search_location_by_type").addToBackStack(null).commit();
                } catch (Exception e) {

                }
            }
        });
        adapter.bindData(recyclerViewTypeLocation);
        showTypeLocation();
//        View bottomSheet = coordinatorLayout.findViewById(R.id.bottom_sheet);
//        final BottomSheetBehaviorGoogleMapsLike behavior = BottomSheetBehaviorGoogleMapsLike.from(bottomSheet);
//        behavior.setState(BottomSheetBehaviorGoogleMapsLike.STATE_COLLAPSED);
//

        bottomSheet = coordinatorLayout.findViewById(R.id.bottom_sheet);
        BottomSheetBehavior behavior = BottomSheetBehavior.from(bottomSheet);
        behavior.setPeekHeight(getResources().getDimensionPixelSize(R.dimen.list_location_height));
        behavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {
                if (behavior.getState() == BottomSheetBehavior.STATE_EXPANDED) {
                    bottomSheet.setPadding(0, getResources().getDimensionPixelOffset(R.dimen.bottom_sheet_padding_top), 0, 0);
                    btnAddLocation.setVisibility(View.GONE);
                } else {
                    bottomSheet.setPadding(0, 0, 0, 0);
                    btnAddLocation.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {
                // React to dragging events
            }
        });

        return view;
    }

    void showTypeLocation() {
        try {
            typeLocationInfo = new TypeLocationInfo();
            myListTypeLocation = typeLocationInfo.getTypeLocationHasSelected(getContext(), realm, UserInfo.getCurrentUserId());
            if (myListTypeLocation.isTrue())
                myTypeLocation.addAll(myListTypeLocation.getData());
            if (myTypeLocation.size() == 0) {
                ServiceLocation.startSetDefaultCategory(getContext());
                BaseReturnFunctionEntity<ArrayList<TypeLocationEntity>> temp = typeLocationInfo.getListTypeLocation(getContext(), realm);
                if (temp.isTrue()) {
                    for (int i = 0; i < 5 && i < temp.getData().size(); i++) {
                        myTypeLocation.add(temp.getData().get(i));
                    }
                }
            }
            if (UserInfo.isLogin()) {
                TypeLocationEntity bookmark = new TypeLocationEntity();
                bookmark.setId(-1);
                bookmark.setName(getString(R.string.has_bookmark));
                bookmark.setEnName(getString(R.string.has_bookmark));
                myTypeLocation.add(0, bookmark);

                bookmark = new TypeLocationEntity();
                bookmark.setId(-2);
                bookmark.setName(getString(R.string.location_created));
                bookmark.setEnName(getString(R.string.location_created));
                myTypeLocation.add(1, bookmark);


                myTypeLocation.add(null);

            }
            adapter.notifyDataSetChanged();

        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    @Override
    public void onMapReady(GoogleMap map) {
        mMap = map;
        mMap.setOnMarkerClickListener(this);
        View locationButton = ((View) mapView.findViewById(Integer.parseInt("1")).getParent()).findViewById(Integer.parseInt("2"));
        // and next place it, on bottom right (as Google Maps app)
        RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams)
                locationButton.getLayoutParams();
        // position on right bottom
        layoutParams.setMargins(0, getResources().getDimensionPixelOffset(R.dimen.marginTopMyLocation), 30, 0);

        // Do other setup activities here too, as described elsewhere in this tutorial.

        // Turn on the My Location layer and the related control on the map.
        updateLocationUI();

        // Get the current location of the device and set the position of the map.
        BaseActivity.requestPermission(getContext(), new AndroidPermissionUtils.OnCallbackRequestPermission() {

            @Override
            public void onSuccess() {
                try {
                    LocationManager manager = (LocationManager) getContext().getSystemService(Context.LOCATION_SERVICE);
                    if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                        settingsRequest();
                    } else {
                        getDeviceLocation();
                    }
                } catch (Exception e) {

                }
            }

            @Override
            public void onFailed() {

            }
        }, AndroidPermissionUtils.TypePermission.PERMISSION_ACCESS_COARSE_LOCATION, AndroidPermissionUtils.TypePermission.PERMISSION_ACCESS_COARSE_LOCATION);

    }

    public void settingsRequest() {
        LocationRequest locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(30 * 1000);
        locationRequest.setFastestInterval(5 * 1000);
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(locationRequest);
        builder.setAlwaysShow(true); //this is the key ingredient
        PendingResult<LocationSettingsResult> result =
                LocationServices.SettingsApi.checkLocationSettings(mGoogleApiClient, builder.build());
        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(LocationSettingsResult result) {
                try {
                    final Status status = result.getStatus();
                    final LocationSettingsStates state = result.getLocationSettingsStates();
                    switch (status.getStatusCode()) {
                        case LocationSettingsStatusCodes.SUCCESS:
                            try {
                                status.startResolutionForResult(getActivity(), REQUEST_CHECK_SETTINGS);
                            } catch (IntentSender.SendIntentException e) {
                            }
                            break;
                        case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                            try {
                                status.startResolutionForResult(getActivity(), REQUEST_CHECK_SETTINGS);
                            } catch (IntentSender.SendIntentException e) {
                                e.printStackTrace();
                            }
                            break;
                        case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                            break;
                    }

                } catch (Exception e) {

                }

            }
        });
    }

    private void getDeviceLocation() {
        /*
         * Get the best and most recent location of the device, which may be null in rare
         * cases when a location is not available.
         */
        try {
            Task locationResult = mFusedLocationProviderClient.getLastLocation();
            locationResult.addOnCompleteListener(getActivity(), new OnCompleteListener() {
                @Override
                public void onComplete(@NonNull Task task) {
                    if (task.isSuccessful()) {
                        // Set the map's camera position to the current location of the device.
                        mLastKnownLocation = (Location) task.getResult();
                        if (mLastKnownLocation != null) {
                            ServiceLocation.startActionUpdateLocation(getContext(), mLastKnownLocation.getLatitude(), mLastKnownLocation.getLongitude());
                            GPSTrackerUtils.saveCurrentLocation(getContext(), mLastKnownLocation.getLatitude(), mLastKnownLocation.getLongitude());
                            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(
                                    new LatLng(mLastKnownLocation.getLatitude(),
                                            mLastKnownLocation.getLongitude()), DEFAULT_ZOOM));
                            getLocation(mLastKnownLocation.getLatitude(), mLastKnownLocation.getLongitude());
                        }
                    } else {
                        mMap.getUiSettings().setMyLocationButtonEnabled(false);
                    }
                }
            });
        } catch (SecurityException e) {
        }
    }


    public void getLocation(double lat, double lon) {
        new AsyncTask<Void, Void, JsonArray>() {
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
            }

            @Override
            protected JsonArray doInBackground(Void... voids) {
                BaseReturnFunctionEntity<JsonArray> result = new LocationInfo().near(0, lat, lon, 1, 100);
                if (result.isTrue()) {
                    return result.getData();
                }
                return new JsonArray();
            }

            @Override
            protected void onPostExecute(JsonArray aVoid) {
                showLocation(aVoid);
                super.onPostExecute(aVoid);
            }
        }.execute();
    }

    //show added locations after getting data from iDuhoc
    void showLocation(JsonArray jsonArray) {
        mMap.clear();
        JsonObject jsonObject;
        for (int i = 0; i < jsonArray.size(); i++) {
            MarkerOptions markerOptions = new MarkerOptions();
            jsonObject = ConvertUtils.toJsonObject(jsonArray.get(i));
            double lat = ConvertUtils.toDouble(jsonObject.get("Lat"));
            double lng = ConvertUtils.toDouble(jsonObject.get("Lon"));
            String placeName = ConvertUtils.toString(jsonObject.get("Title"));
            LatLng latLng = new LatLng(lat, lng);
            markerOptions.position(latLng);
            markerOptions.title(placeName);
            markerOptions.visible(true);
            Marker marker = mMap.addMarker(markerOptions);
            marker.showInfoWindow();
            marker.setTag(jsonObject);
            loadImageByGlide(getContext(), ConvertUtils.toString(jsonObject.get("TypeLogo")), marker);
//            mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
        }
    }

    @UiThread
    public void loadImageByGlide(Context context, @NonNull String url, Marker marker) {
        try {
            RequestOptions options = new RequestOptions()
                    .centerCrop()
                    .priority(Priority.HIGH).dontAnimate().override(40, 40).centerCrop();

            options = options.diskCacheStrategy(DiskCacheStrategy.ALL);
            Glide.with(context).load(url).apply(options).into(new SimpleTarget<Drawable>() {
                @Override
                public void onResourceReady(Drawable resource, Transition<? super Drawable> transition) {
                    try {
//                        BitmapDescriptor markerIcon = getMarkerIconFromDrawable(resource);
                        BitmapDescriptor markerIcon = getMarkerIconWithLabel(url, marker.getTitle());
                        marker.setIcon(markerIcon);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        } catch (OutOfMemoryError e) {
            e.printStackTrace();
        }
    }

    private BitmapDescriptor getMarkerIconWithLabel(@NonNull String url, String title) {
        LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        LinearLayout layout = (LinearLayout) inflater.inflate(R.layout.item_location_icon_on_map, null, false);

        //load icon
        ImageView ivLocationIcon = (ImageView) layout.findViewById(R.id.imageView);
        RequestOptions options = new RequestOptions().centerCrop().priority(Priority.HIGH).dontAnimate().override(40, 40).centerCrop();
        options = options.diskCacheStrategy(DiskCacheStrategy.ALL);
        Glide.with(getContext()).load(url).apply(options).into(ivLocationIcon);

        //load title
        TextView tvTitle = (TextView) layout.findViewById(R.id.tvName);
        tvTitle.setText(title);

        if (layout.getMeasuredHeight() <= 0) {
            layout.measure(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        }

        Bitmap bitmap = Bitmap.createBitmap(layout.getMeasuredWidth(), layout.getMeasuredHeight(), Bitmap.Config.ARGB_8888);

        Canvas canvas = new Canvas();
        canvas.setBitmap(bitmap);
        layout.layout(0, 0, layout.getMeasuredWidth(), layout.getMeasuredHeight());
        layout.draw(canvas);
        return BitmapDescriptorFactory.fromBitmap(bitmap);
    }

    private BitmapDescriptor getMarkerIconFromDrawable(Drawable drawable) {
        Canvas canvas = new Canvas();
        Bitmap bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        canvas.setBitmap(bitmap);
        drawable.setBounds(0, 0, drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight());
        drawable.draw(canvas);
        return BitmapDescriptorFactory.fromBitmap(bitmap);
    }

    private void updateLocationUI() {
        if (mMap == null) {
            return;
        }
        BaseActivity.requestPermission(getContext(), new AndroidPermissionUtils.OnCallbackRequestPermission() {
            @SuppressLint("MissingPermission")
            @Override
            public void onSuccess() {
                mMap.setMyLocationEnabled(true);
                mMap.getUiSettings().setMyLocationButtonEnabled(true);
            }

            @SuppressLint("MissingPermission")
            @Override
            public void onFailed() {
                mMap.setMyLocationEnabled(false);
                mMap.getUiSettings().setMyLocationButtonEnabled(false);
                mLastKnownLocation = null;
            }
        }, AndroidPermissionUtils.TypePermission.PERMISSION_ACCESS_COARSE_LOCATION, AndroidPermissionUtils.TypePermission.PERMISSION_ACCESS_FINE_LOCATION);
    }

    @OnClick(R.id.btnBack)
    void menuClick() {
        Intent intent = new Intent(Constants.ACTION_LOCATION_BACK_TO_MAIN_APP);
        getActivity().sendBroadcast(intent);
        getActivity().finish();
    }


    @BindView(R.id.btnAddLocation)
    FloatingActionButton btnAddLocation;

    @OnClick(R.id.btnAddLocation)
    void addLocation() {
        if (UserInfo.isLogin()) {
//            AddLocationActivity.openChannel(getContext());
            AddNewLocationActivity.open(getContext());
        } else {
            LoginActivity.open(getContext());
        }
    }

    @OnClick({R.id.txtSearch})
    void search() {
        getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.lnFragment, SearchByNameFragment.getInstants(), "searchFriend").addToBackStack(null).commit();
    }

    @BindView(R.id.coordinatorlayout)
    CoordinatorLayout coordinatorLayout;


    @Override
    public void onDestroy() {
        try {
            ((BaseActivity) getContext()).getSupportActionBar().show();
        } catch (Exception e) {
            e.printStackTrace();
        }
        Window w = getActivity().getWindow(); // in Activity's onCreate() for instance
//        w.setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_ATTACHED_IN_DECOR, WindowManager.LayoutParams.FLAG_LAYOUT_ATTACHED_IN_DECOR);
        super.onDestroy();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    public static Fragment getInstants() {
        return new LocationSearchFragment();
    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        try {
            JsonObject jsonObject = (JsonObject) marker.getTag();
            DetailLocationActivity.open(getContext(), jsonObject);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case REQUEST_CHECK_SETTINGS:
                switch (resultCode) {
                    case Activity.RESULT_OK:
                        getDeviceLocation();
                        break;
                }
        }
    }

    @BindView(R.id.recyclerListTypeLocation)
    RecyclerView recyclerViewTypeLocation;

    public class ViewHolderType extends RecyclerView.ViewHolder {
        @BindView(R.id.tvName)
        TextView tvName;
        @BindView(R.id.imageView)
        ImageView imageView;
        @BindView(R.id.btnAdd)
        View btnAdd;

        public ViewHolderType(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
