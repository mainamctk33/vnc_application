package com.team500ae.vncapplication.activities.translator;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.speech.tts.TextToSpeech;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.team500ae.library.adapters.BaseRecyclerAdapter;
import com.team500ae.library.layoutmanager.MyLinearLayoutManager;
import com.team500ae.library.utils.ConvertUtils;
import com.team500ae.library.utils.RecyclerViewUtils;
import com.team500ae.vncapplication.R;
import com.team500ae.vncapplication.activities.login.LoginActivity;
import com.team500ae.vncapplication.base.BaseActivity;
import com.team500ae.vncapplication.base.BaseViewHolder;
import com.team500ae.vncapplication.constants.Constants;
import com.team500ae.vncapplication.data_access.BookmarkInfo;
import com.team500ae.vncapplication.data_access.translator.TranslatorInfo;
import com.team500ae.vncapplication.data_access.user.UserInfo;
import com.team500ae.vncapplication.models.BaseReturnFunctionEntity;
import com.team500ae.vncapplication.models.ServiceResponseEntity;
import com.team500ae.vncapplication.services.ServiceTranslator;
import com.team500ae.vncapplication.utils.SharedPrefUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 * Created by TamPC on 3/24/2018.
 */

public class TranslatorDetailActivity extends BaseActivity implements com.team500ae.library.activities.BaseActivity.OnCallBackBroadcastListener {

    @BindView(R.id.fragDetailTranslator_rvList)
    RecyclerView rvList;
    @BindView(R.id.loading)
    LinearLayout loading;
    private int type;
    View view;
    int typeID = 0;
    TextToSpeech textToSpeechUS, textToSpeechGerman;
    int result;
    boolean isLoading = false;
    HashMap<Integer, JsonObject> mapData = new HashMap<>();
    private BaseRecyclerAdapter<ViewHolder, JsonObject> adapter;
    ArrayList<JsonObject> data = new ArrayList<>();
    private int selectPos = -1;

    public static void open(Context context, int id, String title) {
        Intent intent = new Intent(context, TranslatorDetailActivity.class);
        intent.putExtra("ID", id);
        intent.putExtra("TITLE", title);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        setContentView(R.layout.fragment_detail_translator);
        ButterKnife.bind(this);
        super.onCreate(savedInstanceState);
        init();
        typeID = getIntent().getIntExtra("ID", 0);
        getDataTranslatorDetailOffline();
        showBackButton();
        String title = getIntent().getStringExtra("TITLE");
        setTitle(title);
        //getData();
        initTextToSpeech();
        registerBroadcastReceiver(new ReceiverItem(Constants.ACTION_LOGIN, this));

    }

    private void getDataTranslatorDetailOffline() {
        String jsonOffline = SharedPrefUtils.getString(getActivity(), Constants.JSON_DATA_TRANSLATOR_DETAIL_LIST);
        if (!android.text.TextUtils.isEmpty(jsonOffline)) {
            bindData(ConvertUtils.toJsonArray(jsonOffline));
        }
    }

    private void initTextToSpeech() {
        textToSpeechUS = new TextToSpeech(this, new TextToSpeech.OnInitListener() {
            @Override
            public void onInit(int status) {
                if (status == TextToSpeech.SUCCESS) {
                    textToSpeechUS.setLanguage(Locale.US);
                    textToSpeechUS.setSpeechRate(0.6f);
                } else {
                    Toast.makeText(TranslatorDetailActivity.this, "Feature not supported in your device", Toast.LENGTH_SHORT).show();
                }
            }
        }, "com.google.android.tts");
        textToSpeechGerman = new TextToSpeech(this, new TextToSpeech.OnInitListener() {
            @Override
            public void onInit(int status) {
                if (status == TextToSpeech.SUCCESS) {
                    textToSpeechGerman.setLanguage(Locale.GERMAN);
                    textToSpeechGerman.setSpeechRate(0.6f);
                } else {
                    Toast.makeText(TranslatorDetailActivity.this, "Feature not supported in your device", Toast.LENGTH_SHORT).show();
                }
            }
        }, "com.google.android.tts");

    }

    private void init() {
        rvList.setLayoutManager(new MyLinearLayoutManager(this));
        //ho tro load next page
        RecyclerViewUtils rvListUtils = new RecyclerViewUtils(new RecyclerViewUtils.RecyclerViewUtilsListener() {
            @Override
            public void onScrollToEnd() {
                if (!isLoading) {
                    //getData(page + 1, size, type, getListener());
                }
            }
        });
        rvListUtils.setUp(rvList);
        rvList.setLayoutManager(new LinearLayoutManager(this));
        adapter = new BaseRecyclerAdapter<ViewHolder, JsonObject>(R.layout.item_translator_detail, new BaseRecyclerAdapter.BaseViewHolder<ViewHolder, JsonObject>() {


            @Override
            public ViewHolder getViewHolder(View v) {
                return new ViewHolder(v);
            }

            @Override
            public void bindData(RecyclerView recyclerView, ViewHolder viewHolder, JsonObject data, int position) {
                try {
                    int categoryId = ConvertUtils.toInt(data.get("Category"));
                    JSONObject jsonObject = ConvertUtils.toJSONObject(data);
                    JSONObject translator = jsonObject.getJSONObject("Translator");
                    viewHolder.tvTitle.setText(translator.getString("ContentVN"));
                    viewHolder.tvDescription.setText(translator.getString("ContentEN"));
                    if (selectPos == position) {
                        viewHolder.llMain.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.colorPrimary));
                    } else {
                        viewHolder.llMain.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.white));
                    }
                    BaseReturnFunctionEntity<Boolean> bookmark = new BookmarkInfo().isBookmark(getActivity(), getRealm(), translator.getInt("ID"), UserInfo.getCurrentUserId(), BookmarkInfo.BookmarkType.Translator);
                    viewHolder.imageView.setSelected(bookmark.isTrue() ? bookmark.data : false);
                    viewHolder.llDescription.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (categoryId == 1)
                                textToSpeechUS.speak(viewHolder.tvDescription.getText().toString(), TextToSpeech.QUEUE_FLUSH, null);
                            else if (categoryId == 2)
                                textToSpeechGerman.speak(viewHolder.tvDescription.getText().toString(), TextToSpeech.QUEUE_FLUSH, null);
                        }
                    });
                    viewHolder.imageView.setOnClickListener(v -> {
                        if (!UserInfo.isLogin()) {
                            LoginActivity.open(getActivity());
                            return;
                        } else {
                            viewHolder.imageView.setSelected(!viewHolder.imageView.isSelected());
                            try {
                                ServiceTranslator.startActionBookmark(getActivity(), translator.getInt("ID"), viewHolder.imageView.isSelected());
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    });
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, data, new BaseRecyclerAdapter.OnClickListener() {
            @Override
            public void onClick(View v, int position, Object o) {
                if (textToSpeechUS != null) {
                    textToSpeechUS.stop();
                }
                if (textToSpeechGerman != null) {
                    textToSpeechGerman.stop();
                }
                if (view != null && view.getVisibility() == View.VISIBLE)
                    view.setVisibility(View.GONE);
                view = v.findViewById(R.id.llDescription);
                if (view.getVisibility() == View.VISIBLE) {
                    view.setVisibility(View.GONE);
                } else {
                    selectPos = position;
                    view.setVisibility(View.VISIBLE);
                }
                adapter.notifyDataSetChanged();
            }
        });

        adapter.bindData(rvList);
        //getData(page, size, type, getListener());
    }

    public class ViewHolder extends BaseViewHolder {
        @BindView(R.id.imgFavorite)
        ImageView imageView;
        @BindView(R.id.tvTitle)
        TextView tvTitle;
        @BindView(R.id.tvDescription)
        TextView tvDescription;
        @BindView(R.id.llDescription)
        LinearLayout llDescription;
        @BindView(R.id.llMain)
        LinearLayout llMain;

        public ViewHolder(View itemView) {
            super(itemView);
        }
    }

    @Override
    public void onCallBackBroadcastListener(Context context, String action, Intent intent) {

    }

    public void getData() {
        new AsyncTask<Void, Void, ServiceResponseEntity<JsonArray>>() {
            ProgressDialog progDialog = new ProgressDialog(TranslatorDetailActivity.this);

            @Override
            protected void onPreExecute() {
                // showProgress();
                super.onPreExecute();
                progDialog.setMessage(getString(R.string.loading));
                progDialog.setIndeterminate(false);
                progDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                progDialog.setCancelable(false);
                progDialog.show();

            }

            @Override
            protected ServiceResponseEntity<JsonArray> doInBackground(Void... voids) {
                ServiceResponseEntity<JsonArray> data;
                try {
                    data = new TranslatorInfo().getAll(0, 20, typeID);
                } catch (Exception ex) {
                    return null;
                }
                return data;
            }


            @Override
            protected void onPostExecute(ServiceResponseEntity<JsonArray> data) {
                //super.onPostExecute(data);
                if (progDialog.isShowing()) {
                    progDialog.dismiss();
                }
                if (data != null) {

                    bindData(data.getData());

//                    ImageUtils.loadImageByGlide(getApplicationContext(), true, 300, 300, ConvertUtils.toString(o2.get("Thumbnail")), R.drawable.img_no_image, R.drawable.img_no_image, imgThumbnail, true);
                }


                //hideProgress();
            }


        }.execute();
    }

    void bindData(JsonArray jsonArray) {
        for (int i = 0; i < jsonArray.size(); i++) {
            try {
                JsonObject jsonObject = ConvertUtils.toJsonObject(jsonArray.get(i));
                JSONObject dataJson = ConvertUtils.toJSONObject(jsonObject);
                JSONObject translator = dataJson.getJSONObject("Translator");
                int id = translator.getInt("ID");
                if (typeID == translator.getInt("Type")) {
                    if (mapData.containsKey(id)) {
                        int index = data.indexOf(mapData.get(id));
                        mapData.put(id, jsonObject);
                        if (index != -1) {
                            data.set(index, jsonObject);
                            adapter.notifyItemChanged(index);
                        }
                    } else {
                        mapData.put(id, jsonObject);
                        data.add(jsonObject);
                        adapter.notifyItemInserted(data.size() - 1);
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (textToSpeechUS != null) {
            textToSpeechUS.stop();
        }
        if (textToSpeechGerman != null) {
            textToSpeechGerman.stop();
        }
    }
}
