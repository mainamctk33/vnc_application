package com.team500ae.vncapplication.utils;

import android.app.AlertDialog;
import android.content.Context;
import android.content.res.Resources;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.Patterns;

import com.team500ae.vncapplication.R;
import com.team500ae.vncapplication.activities.job.interfaces.DialogListener;

import java.util.regex.Pattern;

/**
 * Created by thanh.tran on 5/31/2018.
 */
public class AppUtils {
    public static boolean isNetworkAvailable(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnectedOrConnecting();
    }

    public static int getScreenWidth() {
        return Resources.getSystem().getDisplayMetrics().widthPixels;
    }

    public static int getScreenHeight() {
        return Resources.getSystem().getDisplayMetrics().heightPixels;
    }

    public static void showAlertConfirm(final Context context, String title, String content, @Nullable final DialogListener listener) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(context);
        if (!TextUtils.isEmpty(title))
            builder.setTitle(title);
        builder.setMessage(content);
        builder.setNegativeButton(R.string.ok, (dialogInterface, i) -> {
            if (listener != null) {
                listener.onConfirmClicked();
            }
        });
        builder.setPositiveButton(R.string.cancel, (dialogInterface, i) -> builder.create().dismiss());
        builder.create().show();
    }

    public static boolean validEmail(String email) {
        Pattern pattern = Patterns.EMAIL_ADDRESS;
        return pattern.matcher(email).matches();
    }
}
