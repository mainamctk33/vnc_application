package com.team500ae.vncapplication.data_access.job;

import android.content.Context;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.team500ae.library.utils.ConvertUtils;
import com.team500ae.vncapplication.constants.Constants;
import com.team500ae.vncapplication.data_access.ClientInfo;
import com.team500ae.vncapplication.models.BaseReturnFunctionEntity;
import com.team500ae.vncapplication.models.ServiceResponseEntity;
import com.team500ae.vncapplication.models.realm_models.job.LevelJobEntity;

import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.util.ArrayList;

import io.realm.Realm;
import io.realm.RealmResults;
import io.realm.exceptions.RealmError;

/**
 * Created by SVIP on 12/14/2017.
 */

public class LevelJobInfo {
    public ServiceResponseEntity<JsonArray> getAll(){
        try {
            ClientInfo.DataResponse dataResponse = new ClientInfo().requestApi(ClientInfo.MethodType.GET, Constants.API_LEVEL_JOB_GETALL);
            if (dataResponse.isOK()) {
                ServiceResponseEntity<JsonArray> result = dataResponse.getData(new TypeToken<ServiceResponseEntity<JsonArray>>() {
                }.getType());
                return result;
            }
        } catch (UnknownHostException e) {
            e.printStackTrace();
        } catch (SocketTimeoutException e) {
            e.printStackTrace();
        }
        return null;
    }
    public ServiceResponseEntity<JsonArray> sync(Context context, long lastSyncDate) {
        try {
            ClientInfo.DataResponse dataResponse = new ClientInfo().requestApi(ClientInfo.MethodType.GET, Constants.API_LEVEL_JOB_SYNC + "/" + lastSyncDate);
            if (dataResponse.isOK()) {

                ServiceResponseEntity<JsonArray> result = dataResponse.getData(new TypeToken<ServiceResponseEntity<JsonArray>>(){}.getType());
                return  result;
            }
        } catch (UnknownHostException e) {
            e.printStackTrace();
        } catch (SocketTimeoutException e) {
            e.printStackTrace();
        }
        return null;
    }
    public BaseReturnFunctionEntity<ArrayList<LevelJobEntity>> getList(Context context, Realm realm) {
        try {
            RealmResults<LevelJobEntity> realmList = realm.where(LevelJobEntity.class).findAll();
            ArrayList<LevelJobEntity> result = new ArrayList<>();
            result.addAll(realmList);
            return new BaseReturnFunctionEntity<>(BaseReturnFunctionEntity.StatusFunction.TRUE, result);
        } catch (RealmError e) {
            return new BaseReturnFunctionEntity<>(BaseReturnFunctionEntity.StatusFunction.EXCEPTION, e.getMessage());
        } catch (Exception e) {
            return new BaseReturnFunctionEntity<>(BaseReturnFunctionEntity.StatusFunction.EXCEPTION, e.getMessage());
        } catch (OutOfMemoryError e) {
            return new BaseReturnFunctionEntity<>(BaseReturnFunctionEntity.StatusFunction.EXCEPTION, e.getMessage());
        }
    }
    public BaseReturnFunctionEntity<Boolean> update(Realm realm, JsonArray data) {
        try {
            realm.executeTransaction(realm1 -> {
                for (int i = 0; i < data.size(); i++) {
                    JsonObject jsonObject = ConvertUtils.toJsonObject(data.get(i));
                    update(realm, jsonObject, false);
                }
            });
            return new BaseReturnFunctionEntity<>(BaseReturnFunctionEntity.StatusFunction.TRUE, true);
        } catch (RealmError e) {
            return new BaseReturnFunctionEntity<>(BaseReturnFunctionEntity.StatusFunction.EXCEPTION, e.getMessage());
        } catch (Exception e) {
            return new BaseReturnFunctionEntity<>(BaseReturnFunctionEntity.StatusFunction.EXCEPTION, e.getMessage());
        } catch (OutOfMemoryError e) {
            return new BaseReturnFunctionEntity<>(BaseReturnFunctionEntity.StatusFunction.EXCEPTION, e.getMessage());
        }
    }
    public BaseReturnFunctionEntity<Boolean> update(Realm realm, final JsonObject data, boolean needTransacsion) {
        try {
            if (needTransacsion) {
                realm.executeTransaction(realm1 -> {
                    update(realm, data, false);
                });
            } else {
                int id = ConvertUtils.toInt(data.get("ID"));
                LevelJobEntity typeJobEntity = new LevelJobEntity();
                typeJobEntity.setName(ConvertUtils.toString(data.get("Name")));
                typeJobEntity.setName2(ConvertUtils.toString(data.get("Name2")));
                typeJobEntity.setId(id);
                realm.copyToRealmOrUpdate(typeJobEntity);
            }
            return new BaseReturnFunctionEntity<>(BaseReturnFunctionEntity.StatusFunction.EXCEPTION, true);
        } catch (RealmError e) {
            return new BaseReturnFunctionEntity<>(BaseReturnFunctionEntity.StatusFunction.EXCEPTION, e.getMessage());
        } catch (Exception e) {
            return new BaseReturnFunctionEntity<>(BaseReturnFunctionEntity.StatusFunction.EXCEPTION, e.getMessage());
        } catch (OutOfMemoryError e) {
            return new BaseReturnFunctionEntity<>(BaseReturnFunctionEntity.StatusFunction.EXCEPTION, e.getMessage());
        }
    }
}
