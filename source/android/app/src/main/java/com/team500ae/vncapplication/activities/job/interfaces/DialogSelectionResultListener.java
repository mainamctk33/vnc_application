package com.team500ae.vncapplication.activities.job.interfaces;


import com.team500ae.vncapplication.activities.job.SelectionModel;

public interface DialogSelectionResultListener {
    void onDialogForResult(int code, SelectionModel model);
}
