package com.team500ae.vncapplication.models;

/**
 * Created by Phuong D. Nguyen on 4/18/2018.
 */

public class QuestionObject {
    private String type, title, question, answer;

    public QuestionObject() {
    }

    public QuestionObject(String type, String title, String question, String answer) {
        this.type = type;
        this.title = title;
        this.question = question;
        this.answer = answer;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public String getType() {

        return type;
    }

    public String getTitle() {
        return title;
    }

    public String getQuestion() {
        return question;
    }

    public String getAnswer() {
        return answer;
    }
}
