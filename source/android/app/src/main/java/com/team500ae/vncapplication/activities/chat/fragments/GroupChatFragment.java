package com.team500ae.vncapplication.activities.chat.fragments;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.etiennelawlor.imagegallery.library.adapters.FullScreenImageGalleryAdapter;
import com.google.gson.JsonArray;
import com.onurciner.toastox.ToastOXDialog;
import com.sendbird.android.AdminMessage;
import com.sendbird.android.BaseChannel;
import com.sendbird.android.BaseMessage;
import com.sendbird.android.FileMessage;
import com.sendbird.android.GroupChannel;
import com.sendbird.android.Member;
import com.sendbird.android.SendBird;
import com.sendbird.android.SendBirdException;
import com.sendbird.android.User;
import com.sendbird.android.UserMessage;
import com.team500ae.library.activities.image.ImageViewerActivity;
import com.team500ae.library.utils.AndroidPermissionUtils;
import com.team500ae.library.utils.ConvertUtils;
import com.team500ae.library.utils.ImageUtils;
import com.team500ae.library.utils.ViewUtils;
import com.team500ae.vncapplication.R;
import com.team500ae.vncapplication.activities.chat.GroupChannelActivity;
import com.team500ae.vncapplication.activities.chat.InviteMemberActivity;
import com.team500ae.vncapplication.activities.chat.MediaPlayerActivity;
import com.team500ae.vncapplication.activities.chat.MemberListActivity;
import com.team500ae.vncapplication.activities.chat.UpdateGroupDiaglog;
import com.team500ae.vncapplication.activities.chat.adapter.GroupChatAdapter;
import com.team500ae.vncapplication.activities.chat.utils.FileUtils;
import com.team500ae.vncapplication.activities.chat.utils.PreferenceUtils;
import com.team500ae.vncapplication.activities.chat.utils.UrlPreviewInfo;
import com.team500ae.vncapplication.activities.chat.utils.WebUtils;
import com.team500ae.vncapplication.base.BaseActivity;
import com.team500ae.vncapplication.constants.Constants;
import com.team500ae.vncapplication.data_access.upload.BlobUploadProvider;
import com.team500ae.vncapplication.data_access.upload.UploadInfo;
import com.team500ae.vncapplication.data_access.user.UserInfo;
import com.team500ae.vncapplication.models.ServiceResponseEntity;
import com.team500ae.vncapplication.others.DialogListener;
import com.team500ae.vncapplication.services.ServiceChat;

import org.json.JSONException;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class GroupChatFragment extends Fragment {

    private static final String CONNECTION_HANDLER_ID = "CONNECTION_HANDLER_GROUP_CHAT";

    private static final String LOG_TAG = GroupChatFragment.class.getSimpleName();

    private static final int STATE_NORMAL = 0;
    private static final int STATE_EDIT = 1;

    private static final String CHANNEL_HANDLER_ID = "CHANNEL_HANDLER_GROUP_CHANNEL_CHAT";
    private static final String STATE_CHANNEL_URL = "STATE_CHANNEL_URL";
    private static final int INTENT_REQUEST_CHOOSE_MEDIA = 301;
    private static final int PERMISSION_WRITE_EXTERNAL_STORAGE = 13;
    public static final String EXTRA_CHANNEL_URL = "EXTRA_CHANNEL_URL";
    private static final int CHECK_TYPING = 0;
    private static final int REQ_SELECT_FILE = 10001;

    private InputMethodManager mIMM;
    private HashMap<BaseChannel.SendFileMessageWithProgressHandler, FileMessage> mFileProgressHandlerMap;
    private GroupChatAdapter mChatAdapter;
    private LinearLayoutManager mLayoutManager;

    private GroupChannel mChannel;
    private String mChannelUrl;
    private int mCurrentState = STATE_NORMAL;
    private BaseMessage mEditingMessage = null;
    private BaseMessage message;
    private int position;
    private UpdateGroupDiaglog dialogGroupInfo;
    private Uri uri;
    private Uri tempUri;

    /**
     * To create an instance of this fragment, a Channel URL should be required.
     */
    public static GroupChatFragment newInstance(@NonNull String channelUrl) {
        GroupChatFragment fragment = new GroupChatFragment();

        Bundle args = new Bundle();
        args.putString(GroupChannelListFragment.EXTRA_GROUP_CHANNEL_URL, channelUrl);
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mIMM = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        mFileProgressHandlerMap = new HashMap<>();

        if (savedInstanceState != null) {
            // Get channel URL from saved state.
            mChannelUrl = savedInstanceState.getString(STATE_CHANNEL_URL);
        } else {
            // Get channel URL from GroupChannelListFragment.
            mChannelUrl = getArguments().getString(GroupChannelListFragment.EXTRA_GROUP_CHANNEL_URL);
        }

        Log.d(LOG_TAG, mChannelUrl);

        mChatAdapter = new GroupChatAdapter(getActivity());
        setUpChatListAdapter();

        // Load messages from cache.
        mChatAdapter.load(mChannelUrl);
        thread.start();
    }

    Handler handler = new Handler();

    Thread thread = new Thread(new Runnable() {
        @Override
        public void run() {
            try {
                while (handler != null) {
                    Thread.sleep(3000);
                    try {

                        handler.postDelayed((Runnable) () -> {
                            try {
                                if (mMessageEditText.getText().length() == 0) {
                                    setTypingStatus(false);
                                } else {
                                    setTypingStatus(true);
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }, 1000);
                    } catch (Exception e) {

                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    });
    @BindView(R.id.chat_menu)
    View menuChat;

    @OnClick(R.id.chat_menu)
    void hideMenu() {
        menuChat.setVisibility(View.GONE);
    }

    @BindView(R.id.chat_item_menu)
    View menuChatItem;

    @OnClick(R.id.chat_item_menu)
    void hideMenuChatItem() {
        menuChatItem.setVisibility(View.GONE);
    }

    @OnClick(R.id.btnUpdate)
    void update() {
        menuChat.setVisibility(View.GONE);
        uri = null;
        BaseActivity.requestPermission(getContext(), new AndroidPermissionUtils.OnCallbackRequestPermission() {
            @Override
            public void onSuccess() {
                dialogGroupInfo = new UpdateGroupDiaglog(getActivity(), mChannel.getCoverUrl(), mChannel.getName());
                dialogGroupInfo.setOnItemClickListener(new DialogListener() {
                    @Override
                    public void onConfirmClicked() {
                        String groupName = dialogGroupInfo.getEditText();
                        if (uri != null)
                            new UploadInfo(new BlobUploadProvider() {
                            }, new UploadInfo.UploadListener() {
                                ProgressDialog progDialog = new ProgressDialog(getActivity());
                                ProgressDialog dialogUpload;

                                @Override
                                public void beginUpload() {
                                    progDialog.setMessage(getString(R.string.loading));
                                    progDialog.setIndeterminate(false);
                                    progDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                                    progDialog.setCancelable(true);
                                    progDialog.show();

                                }

                                @Override
                                public void endUpload() {
                                    if (progDialog.isShowing()) {
                                        progDialog.dismiss();
                                    }

                                }

                                @Override
                                public void onSuccess(JsonArray response) {
                                    if (response == null || response.size() == 0) {
                                        updateGroup("");
                                    } else {
                                        updateGroup(ConvertUtils.toString(response.get(0)));
                                    }

                                }

                                @Override
                                public void onError(ServiceResponseEntity<JsonArray> response) {
                                    updateGroup(mChannel.getCoverUrl());
                                }
                            }).upload(getActivity(), Constants.UPLOAD_FILE, ConvertUtils.toArrayList(Uri.class, uri));
                        else
                            updateGroup(mChannel.getCoverUrl());
                    }

                    @Override
                    public void onButtonChooseImage() {

                        tempUri = com.team500ae.vncapplication.utils.FileUtils.createTempFile(getContext());
                        Intent chooseImageIntent = new com.team500ae.vncapplication.utils.FileUtils().selectOrTakePicture(getActivity(), tempUri);
                        startActivityForResult(chooseImageIntent, REQ_SELECT_FILE);
                    }
                });
                dialogGroupInfo.show();
            }

            @Override
            public void onFailed() {

            }
        }, AndroidPermissionUtils.TypePermission.PERMISSION_READ_EXTERNAL_STORAGE, AndroidPermissionUtils.TypePermission.PERMISSION_WRIRE_EXTERNAL_STORAGE, AndroidPermissionUtils.TypePermission.PERMISSION_CAMERA);
    }

    private void updateGroup(String url) {
        mChannel.updateChannel(dialogGroupInfo.getEditText(), url, UserInfo.getCurrentUserId(), new GroupChannel.GroupChannelUpdateHandler() {
            @Override
            public void onResult(GroupChannel groupChannel, SendBirdException e) {
                if (e != null) {
                    // Error!
                    return;
                }
                if (getActivity() != null) {
                    ((GroupChannelActivity) getActivity()).setActionBarTitle(groupChannel.getName());
                }
                BaseActivity.showSnackBar(getContext(), "Update success");
                ServiceChat.startActionUpdateGroup(getActivity(), UserInfo.getCurrentUserId(), UserInfo.getCurrentUserFullName(), groupChannel.getUrl());
            }
        });
    }

    @OnClick(R.id.btnListMembers)
    void viewListMember() {
        if (mChannel != null && mChannel.getMemberCount() == 2) {
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            builder.setIcon(R.drawable.ic_info);
            builder.setTitle(getResources().getString(R.string.notice));
            builder.setMessage(getResources().getString(R.string.option_for_group_chat));
            builder.setCancelable(false);
            builder.setPositiveButton(getString(R.string.select_language_ok), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                }
            });
            builder.show();
            return;
        }

        menuChat.setVisibility(View.GONE);
        Intent intent = new Intent(getActivity(), MemberListActivity.class);
        intent.putExtra(EXTRA_CHANNEL_URL, mChannelUrl);
        startActivity(intent);
    }

    @OnClick(R.id.btnInviteMember)
    void inviteMember() {
        menuChat.setVisibility(View.GONE);
        Intent intent = new Intent(getActivity(), InviteMemberActivity.class);
        intent.putExtra(EXTRA_CHANNEL_URL, mChannelUrl);
        intent.putExtra("LIST_MEMBERS", ConvertUtils.toJson(mChannel.getMembers()));
        startActivity(intent);
    }

    @BindView(R.id.btnTurnOffNotification)
    View btnTurnOffNotification;
    @BindView(R.id.btnTurnOnNotification)
    View btnTurnOnNotification;

    @OnClick({R.id.btnTurnOffNotification, R.id.btnTurnOnNotification})
    void turnOnOffNotification() {
        menuChat.setVisibility(View.GONE);
        mChannel.setPushPreference(mChannel.isPushEnabled() ? false : true, new GroupChannel.GroupChannelSetPushPreferenceHandler() {
            @Override
            public void onResult(SendBirdException e) {
            }
        });
    }

    @OnClick(R.id.btnLeaveGroup)
    void leaveGroup() {
        menuChat.setVisibility(View.GONE);
        new ToastOXDialog.Build(getActivity())
                .setTitle(R.string.confirm)
                .setContent(R.string.msg_do_you_want_to_leave_this_group)
                .setPositiveText(R.string.yes)
                .setPositiveBackgroundColorResource(R.color.btnyes)
                .setPositiveTextColorResource(R.color.black)
                .onPositive(new ToastOXDialog.ButtonCallback() {
                    @Override
                    public void onClick(@NonNull ToastOXDialog toastOXDialog) {
                        mChannel.leave(new GroupChannel.GroupChannelLeaveHandler() {
                            @Override
                            public void onResult(SendBirdException e) {
                                if (e != null) {
                                }
                                ServiceChat.startActionLeaveGroup(getContext(), UserInfo.getCurrentUserId(), UserInfo.getCurrentUserFullName(), mChannel.getUrl());
                                getActivity().finish();
                            }
                        });
                    }
                })
                .setNegativeText(R.string.cancel)
                .setNegativeBackgroundColorResource(R.color.dim)
                .setNegativeTextColorResource(R.color.white).show();
    }

    private void setClipboard(Context context, String text) {
        ClipboardManager clipboard = (ClipboardManager) context.getSystemService(Context.CLIPBOARD_SERVICE);
        ClipData clip = ClipData.newPlainText("Copied Text", text);
        clipboard.setPrimaryClip(clip);
    }

    @OnClick(R.id.btnCopy)
    void copyChatItem() {
        menuChatItem.setVisibility(View.GONE);
        setClipboard(getContext(), ((UserMessage) message).getMessage());
        BaseActivity.showSnackBar(getContext(), R.string.copy_message_to_clipboard);
    }

    @OnClick(R.id.btnDelete)
    void deleteChatItem() {
        menuChatItem.setVisibility(View.GONE);
        new ToastOXDialog.Build(getActivity())
                .setTitle(R.string.confirm)
                .setContent(R.string.msg_do_you_want_to_remove_this_message)
                .setPositiveText(R.string.yes)
                .setPositiveBackgroundColorResource(R.color.btnyes)
                .setPositiveTextColorResource(R.color.black)
                .onPositive(new ToastOXDialog.ButtonCallback() {
                    @Override
                    public void onClick(@NonNull ToastOXDialog toastOXDialog) {
                        mChannel.deleteMessage(message, new BaseChannel.DeleteMessageHandler() {
                            @Override
                            public void onResult(SendBirdException e) {
                                if (e != null) {
                                    // Error!
                                    Toast.makeText(getActivity(), "Error " + e.getCode() + ": " + e.getMessage(), Toast.LENGTH_SHORT).show();
                                    return;
                                }

                                mChatAdapter.loadLatestMessages(30, new BaseChannel.GetMessagesHandler() {
                                    @Override
                                    public void onResult(List<BaseMessage> list, SendBirdException e) {
                                        mChatAdapter.markAllMessagesAsRead();
                                    }
                                });
                            }
                        });
                    }
                })
                .setNegativeText(R.string.cancel)
                .setNegativeBackgroundColorResource(R.color.dim)
                .setNegativeTextColorResource(R.color.white).show();
    }

    @OnClick(R.id.btnEdit)
    void editChatItem() {
        menuChatItem.setVisibility(View.GONE);
        setState(STATE_EDIT, message, position);
    }

    @BindView(R.id.layout_group_chat_root)
    RelativeLayout mRootLayout;
    @BindView(R.id.recycler_group_chat)
    RecyclerView mRecyclerView;
    @BindView(R.id.layout_group_chat_current_event)
    View mCurrentEventLayout;
    @BindView(R.id.text_group_chat_current_event)
    TextView mCurrentEventText;
    @BindView(R.id.edittext_group_chat_message)
    EditText mMessageEditText;
    @BindView(R.id.button_group_chat_send)
    View mMessageSendButton;
    @BindView(R.id.lnOptionMyMessage)
    View lnOptionMyMessage;

    @OnClick(R.id.button_group_chat_send)
    void sendMessage() {
        if (mCurrentState == STATE_EDIT) {
            String userInput = mMessageEditText.getText().toString();
            if (userInput.length() > 0) {
                if (mEditingMessage != null) {
                    editMessage(mEditingMessage, userInput);
                }
            }
            setState(STATE_NORMAL, null, -1);
        } else {
            String userInput = mMessageEditText.getText().toString();
            if (userInput.length() > 0) {
                sendUserMessage(userInput);
                mMessageEditText.setText("");
            }
        }
    }

    @BindView(R.id.button_group_chat_upload)
    View mUploadFileButton;

    @OnClick(R.id.button_group_chat_upload)
    void uploadFile() {
        requestMedia();
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_group_chat, container, false);
        ButterKnife.bind(this, rootView);
        menuChat.setVisibility(View.GONE);
        menuChatItem.setVisibility(View.GONE);

        setRetainInstance(true);


        mMessageEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() > 0) {
                    mMessageSendButton.setEnabled(true);
                } else {
                    mMessageSendButton.setEnabled(false);
                }
            }
        });

        mMessageSendButton.setEnabled(false);

        mMessageEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() == 0) {
                    setTypingStatus(false);
                } else {
                    setTypingStatus(true);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        setUpRecyclerView();

        setHasOptionsMenu(true);

        return rootView;
    }


    private void refresh() {
        if (mChannel == null) {
            GroupChannel.getChannel(mChannelUrl, new GroupChannel.GroupChannelGetHandler() {
                @Override
                public void onResult(GroupChannel groupChannel, SendBirdException e) {
                    if (e != null) {
                        // Error!
                        e.printStackTrace();
                        return;
                    }

                    mChannel = groupChannel;
                    mChatAdapter.setChannel(mChannel);
                    mChatAdapter.loadLatestMessages(100, new BaseChannel.GetMessagesHandler() {
                        @Override
                        public void onResult(List<BaseMessage> list, SendBirdException e) {
                            mChatAdapter.markAllMessagesAsRead();
                        }
                    });
                    updateActionBarTitle();
                }
            });
        } else {
            mChannel.refresh(new GroupChannel.GroupChannelRefreshHandler() {
                @Override
                public void onResult(SendBirdException e) {
                    if (e != null) {
                        // Error!
                        e.printStackTrace();
                        return;
                    }

                    mChatAdapter.loadLatestMessages(30, new BaseChannel.GetMessagesHandler() {
                        @Override
                        public void onResult(List<BaseMessage> list, SendBirdException e) {
                            mChatAdapter.markAllMessagesAsRead();
                        }
                    });
                    updateActionBarTitle();
                }
            });
        }
    }

    @Override
    public void onResume() {
        super.onResume();

        mChatAdapter.setContext(getActivity()); // Glide bug fix (java.lang.IllegalArgumentException: You cannot start a load for a destroyed activity)

        // Gets channel from URL user requested

        Log.d(LOG_TAG, mChannelUrl);

        SendBird.addChannelHandler(CHANNEL_HANDLER_ID, new SendBird.ChannelHandler() {
            @Override
            public void onMessageReceived(BaseChannel baseChannel, BaseMessage baseMessage) {
                if (baseChannel.getUrl().equals(mChannelUrl)) {
                    mChatAdapter.markAllMessagesAsRead();
                    // Add new message to view
                    mChatAdapter.addFirst(baseMessage);
                }
            }

            @Override
            public void onMessageDeleted(BaseChannel baseChannel, long msgId) {
                super.onMessageDeleted(baseChannel, msgId);
                if (baseChannel.getUrl().equals(mChannelUrl)) {
                    mChatAdapter.delete(msgId);
                }
            }

            @Override
            public void onMessageUpdated(BaseChannel channel, BaseMessage message) {
                super.onMessageUpdated(channel, message);
                if (channel.getUrl().equals(mChannelUrl)) {
                    mChatAdapter.update(message);
                }
            }

            @Override
            public void onReadReceiptUpdated(GroupChannel channel) {
                if (channel.getUrl().equals(mChannelUrl)) {
                    mChatAdapter.notifyDataSetChanged();
                }
            }

            @Override
            public void onTypingStatusUpdated(GroupChannel channel) {
                if (channel.getUrl().equals(mChannelUrl)) {
                    List<Member> typingUsers = channel.getTypingMembers();
                    displayTyping(typingUsers);
                }
            }

        });

        SendBird.addConnectionHandler(CONNECTION_HANDLER_ID, new SendBird.ConnectionHandler() {
            @Override
            public void onReconnectStarted() {
            }

            @Override
            public void onReconnectSucceeded() {
                refresh();
            }

            @Override
            public void onReconnectFailed() {
            }
        });

        if (SendBird.getConnectionState() == SendBird.ConnectionState.OPEN) {
            refresh();
        } else {
            if (SendBird.reconnect()) {
                // Will call onReconnectSucceeded()
            } else {
                String userId = PreferenceUtils.getUserId(getActivity());
                if (userId == null) {
                    Toast.makeText(getActivity(), "Require user ID to connect to SendBird.", Toast.LENGTH_LONG).show();
                    return;
                }

                SendBird.connect(userId, new SendBird.ConnectHandler() {
                    @Override
                    public void onConnected(User user, SendBirdException e) {
                        if (e != null) {
                            e.printStackTrace();
                            return;
                        }

                        refresh();
                    }
                });
            }
        }
    }

    @Override
    public void onPause() {
        SendBird.removeChannelHandler(CHANNEL_HANDLER_ID);
        SendBird.removeConnectionHandler(CONNECTION_HANDLER_ID);
        super.onPause();
    }

    @Override
    public void onDestroy() {
        setTypingStatus(false);
        // Save messages to cache.
        mChatAdapter.save();

        super.onDestroy();

        if (handler != null)
            handler = null;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putString(STATE_CHANNEL_URL, mChannelUrl);

        super.onSaveInstanceState(outState);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, final Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            switch (requestCode) {
                case REQ_SELECT_FILE:
                    Bitmap bitmap = com.team500ae.vncapplication.utils.FileUtils.getImageFromResult(getActivity(), resultCode, data, tempUri);
                    dialogGroupInfo.setBitmap(bitmap);
                    File output = com.team500ae.vncapplication.utils.FileUtils.saveBitmap(getActivity(), bitmap);
                    uri = Uri.fromFile(output);
                    //uri = com.team500ae.vncapplication.utils.FileUtils.getSelectedImageFromResult(getActivity(), resultCode, data, tempUri);
                    break;
                case INTENT_REQUEST_CHOOSE_MEDIA:
                    if (data == null) {
                        Log.d(LOG_TAG, "data is null!");
                        return;
                    }
                    sendFileWithThumbnail(data.getData());
            }
        }
        SendBird.setAutoBackgroundDetection(true);
    }

    private void setUpRecyclerView() {
        mLayoutManager = new LinearLayoutManager(getActivity());
        mLayoutManager.setReverseLayout(true);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setAdapter(mChatAdapter);
        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                if (mLayoutManager.findLastVisibleItemPosition() == mChatAdapter.getItemCount() - 1) {
                    mChatAdapter.loadPreviousMessages(30, null);
                }
            }
        });
    }

    private void setUpChatListAdapter() {
        mChatAdapter.setItemClickListener(new GroupChatAdapter.OnItemClickListener() {
            @Override
            public void onUserMessageItemClick(UserMessage message) {
                // Restore failed message and remove the failed message from list.
                if (mChatAdapter.isFailedMessage(message)) {
                    retryFailedMessage(message);
                    return;
                }

                // Message is sending. Do nothing on click event.
                if (mChatAdapter.isTempMessage(message)) {
                    return;
                }


                if (message.getCustomType().equals(GroupChatAdapter.URL_PREVIEW_CUSTOM_TYPE)) {
                    try {
                        UrlPreviewInfo info = new UrlPreviewInfo(message.getData());
                        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(info.getUrl()));
                        startActivity(browserIntent);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFileMessageItemClick(FileMessage message) {
                // Load media chooser and remove the failed message from list.
                if (mChatAdapter.isFailedMessage(message)) {
                    retryFailedMessage(message);
                    return;
                }

                // Message is sending. Do nothing on click event.
                if (mChatAdapter.isTempMessage(message)) {

                    return;
                }


                onFileMessageClicked(message);
            }
        });

        mChatAdapter.setItemLongClickListener(new GroupChatAdapter.OnItemLongClickListener() {
            @Override
            public void onUserMessageItemLongClick(UserMessage message, int position) {
                showMessageOptionsDialog(message, position);
            }

            @Override
            public void onFileMessageItemLongClick(FileMessage message, int position) {
                showMessageOptionsDialog(message, position);
            }

            @Override
            public void onAdminMessageItemLongClick(AdminMessage message, int position) {
            }
        });
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_group_chat, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_option) {
            if (menuChat.getVisibility() == View.VISIBLE)
                menuChat.setVisibility(View.GONE);
            else
                menuChat.setVisibility(View.VISIBLE);

            btnTurnOffNotification.setVisibility(View.GONE);
            btnTurnOnNotification.setVisibility(View.GONE);

            if (mChannel != null)
                if (mChannel.isPushEnabled())
                    btnTurnOffNotification.setVisibility(View.VISIBLE);
                else
                    btnTurnOnNotification.setVisibility(View.VISIBLE);
            ViewUtils.hideKeyboard(getActivity());
        }
        return true;
    }

    private void showMessageOptionsDialog(final BaseMessage message, final int position) {
        this.message = message;
        this.position = position;
        if (message instanceof UserMessage || message instanceof FileMessage) {
            if (message instanceof UserMessage) {
                UserMessage userMessage = (UserMessage) message;
                if (!userMessage.getSender().getUserId().equals(SendBird.getCurrentUser().getUserId())) {
                    lnOptionMyMessage.setVisibility(View.GONE);
                } else {
                    lnOptionMyMessage.setVisibility(View.VISIBLE);
                }
            }
            if (message instanceof FileMessage) {
                FileMessage userMessage = (FileMessage) message;
                if (!userMessage.getSender().getUserId().equals(SendBird.getCurrentUser().getUserId())) {
                    return;
                }
            }
            menuChatItem.setVisibility(View.VISIBLE);
        }
    }

    private void setState(int state, BaseMessage editingMessage, final int position) {
        switch (state) {
            case STATE_NORMAL:
                mCurrentState = STATE_NORMAL;
                mEditingMessage = null;

                mUploadFileButton.setVisibility(View.VISIBLE);
                mMessageEditText.setText("");

//                mIMM.hideSoftInputFromWindow(mMessageEditText.getWindowToken(), 0);
                break;

            case STATE_EDIT:
                mCurrentState = STATE_EDIT;
                mEditingMessage = editingMessage;

                mUploadFileButton.setVisibility(View.GONE);
                String messageString = ((UserMessage) editingMessage).getMessage();
                if (messageString == null) {
                    messageString = "";
                }
                mMessageEditText.setText(messageString);
                if (messageString.length() > 0) {
                    mMessageEditText.setSelection(0, messageString.length());
                }

                mMessageEditText.requestFocus();
                mIMM.toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY);

                mRecyclerView.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        mRecyclerView.scrollToPosition(position);
                    }
                }, 500);
                break;
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        ((GroupChannelActivity) context).setOnBackPressedListener(new GroupChannelActivity.onBackPressedListener() {
            @Override
            public boolean onBack() {
                if (mCurrentState == STATE_EDIT) {
                    setState(STATE_NORMAL, null, -1);
                    return true;
                }
                return false;
            }
        });
    }

    private void retryFailedMessage(final BaseMessage message) {
        new AlertDialog.Builder(getActivity())
                .setMessage("Retry?")
                .setPositiveButton(R.string.resend_message, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (which == DialogInterface.BUTTON_POSITIVE) {
                            if (message instanceof UserMessage) {
                                String userInput = ((UserMessage) message).getMessage();
                                sendUserMessage(userInput);
                            } else if (message instanceof FileMessage) {
                                Uri uri = mChatAdapter.getTempFileMessageUri(message);
                                sendFileWithThumbnail(uri);
                            }
                            mChatAdapter.removeFailedMessage(message);
                        }
                    }
                })
                .setNegativeButton(R.string.delete_message, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (which == DialogInterface.BUTTON_NEGATIVE) {
                            mChatAdapter.removeFailedMessage(message);
                        }
                    }
                }).show();
    }

    /**
     * Display which users are typing.
     * If more than two users are currently typing, this will state that "multiple users" are typing.
     *
     * @param typingUsers The list of currently typing users.
     */
    private void displayTyping(List<Member> typingUsers) {

        if (typingUsers.size() > 0) {
            mCurrentEventLayout.setVisibility(View.VISIBLE);
            String string;

            if (typingUsers.size() == 1) {
                string = typingUsers.get(0).getNickname() + " " + getResources().getString(R.string.typing);
            } else if (typingUsers.size() == 2) {
                string = typingUsers.get(0).getNickname() + " " + typingUsers.get(1).getNickname() + " " + getResources().getString(R.string.typing);
            } else {
                string = typingUsers.size() + " " + getResources().getString(R.string.people_are_typing);
            }
            mCurrentEventText.setText(string);
        } else {
            mCurrentEventLayout.setVisibility(View.GONE);
        }
    }

    private void requestMedia() {
        if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            // If storage permissions are not granted, request permissions at run-time,
            // as per < API 23 guidelines.
            requestStoragePermissions();
        } else {

            tempUri = com.team500ae.vncapplication.utils.FileUtils.createTempFile(getContext());
            Intent chooseImageIntent = new com.team500ae.vncapplication.utils.FileUtils().selectOrTakePicture(getActivity(), tempUri);
            startActivityForResult(chooseImageIntent, INTENT_REQUEST_CHOOSE_MEDIA);

            // Set this as false to maintain connection
            // even when an external Activity is started.
            SendBird.setAutoBackgroundDetection(false);
        }
    }

    private void requestStoragePermissions() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(),
                Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            // Provide an additional rationale to the user if the permission was not granted
            // and the user would benefit from additional context for the use of the permission.
            // For example if the user has previously denied the permission.
            Snackbar.make(mRootLayout, "Storage access permissions are required to upload/download files.",
                    Snackbar.LENGTH_LONG)
                    .setAction("Okay", new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                                    PERMISSION_WRITE_EXTERNAL_STORAGE);
                        }
                    })
                    .show();
        } else {
            // Permission has not been granted yet. Request it directly.
            requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    PERMISSION_WRITE_EXTERNAL_STORAGE);
        }
    }

    private void onFileMessageClicked(FileMessage message) {
        String type = message.getType().toLowerCase();
        if (type.startsWith("image")) {
            ImageViewerActivity.imageDownload = new ImageViewerActivity.IImageDownloadListener() {
                @Override
                public void download(String url) {
                    BaseActivity.requestPermission(getContext(), new AndroidPermissionUtils.OnCallbackRequestPermission() {
                        @Override
                        public void onSuccess() {
                            FileUtils.downloadFile(getContext(), url, message.getName());
                        }

                        @Override
                        public void onFailed() {

                        }
                    }, AndroidPermissionUtils.TypePermission.PERMISSION_WRIRE_EXTERNAL_STORAGE);
                }
            };

            Intent intent = new Intent();
            intent.setAction(android.content.Intent.ACTION_VIEW);
            intent.setDataAndType(Uri.parse(message.getUrl()), "image/*");
            getContext().startActivity(intent);

//            ImageUtils.showImage(getContext(), new FullScreenImageGalleryAdapter.FullScreenImageLoader() {
//                @Override
//                public void loadFullScreenImage(ImageView iv, String imageUrl, int width, LinearLayout bglinearLayout) {
//                    com.team500ae.vncapplication.activities.chat.utils.ImageUtils.displayImageFromUrl(getContext(),imageUrl,iv,null, null);
//                    }
//            }, message.getUrl(), 0);
        } else if (type.startsWith("video")) {
            Intent intent = new Intent(getActivity(), MediaPlayerActivity.class);
            intent.putExtra("url", message.getUrl());
            startActivity(intent);
        } else {
            showDownloadConfirmDialog(message);
        }
    }

    private void showDownloadConfirmDialog(final FileMessage message) {
        if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            // If storage permissions are not granted, request permissions at run-time,
            // as per < API 23 guidelines.
            requestStoragePermissions();
        } else {
            new AlertDialog.Builder(getActivity())
                    .setMessage("Download file?")
                    .setPositiveButton(R.string.download, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            if (which == DialogInterface.BUTTON_POSITIVE) {
                                FileUtils.downloadFile(getActivity(), message.getUrl(), message.getName());
                            }
                        }
                    })
                    .setNegativeButton(R.string.cancel, null).show();
        }

    }

    private void updateActionBarTitle() {
        String title = "";

        if (mChannel != null) {
            int memberCount = mChannel.getMemberCount();
            List<Member> mems = mChannel.getMembers();

            if (memberCount == 1) {
                if (!mems.get(0).getUserId().equals(UserInfo.getCurrentUserId())) {
                    title = mems.get(0).getNickname();
                } else {
                    title = getResources().getString(R.string.no_members);
                }
            } else if (memberCount == 2) {
                if (!mems.get(0).getUserId().equals(UserInfo.getCurrentUserId())) {
                    title = mems.get(0).getNickname();
                } else if (!mems.get(1).getUserId().equals(UserInfo.getCurrentUserId())) {
                    title = mems.get(1).getNickname();
                } else {
                    title = mChannel.getName();
                }
            } else if (memberCount > 2) {
                title = mChannel.getName();
            }
        }

        // Set action bar title to name of channel
        if (getActivity() != null) {
            ((GroupChannelActivity) getActivity()).setActionBarTitle(title);
        }
    }

    private void sendUserMessageWithUrl(final String text, String url) {
        new WebUtils.UrlPreviewAsyncTask() {
            @Override
            protected void onPostExecute(UrlPreviewInfo info) {
                UserMessage tempUserMessage = null;
                BaseChannel.SendUserMessageHandler handler = new BaseChannel.SendUserMessageHandler() {
                    @Override
                    public void onSent(UserMessage userMessage, SendBirdException e) {
                        if (e != null) {
                            // Error!
                            Log.e(LOG_TAG, e.toString());
                            Toast.makeText(
                                    getActivity(), getResources().getString(R.string.send_message_error) + ": " + e.getMessage(), Toast.LENGTH_SHORT)
                                    .show();
                            mChatAdapter.markMessageFailed(userMessage.getRequestId());
                            return;
                        }

                        // Update a sent message to RecyclerView
                        mChatAdapter.markMessageSent(userMessage);
                    }
                };

                try {
                    // Sending a message with URL preview information and custom type.
                    String jsonString = info.toJsonString();
                    tempUserMessage = mChannel.sendUserMessage(text, jsonString, GroupChatAdapter.URL_PREVIEW_CUSTOM_TYPE, handler);
                } catch (Exception e) {
                    // Sending a message without URL preview information.
                    tempUserMessage = mChannel.sendUserMessage(text, handler);
                }


                // Display a user message to RecyclerView
                mChatAdapter.addFirst(tempUserMessage);
            }
        }.execute(url);
    }

    private void sendUserMessage(String text) {
        List<String> urls = WebUtils.extractUrls(text);
        if (urls.size() > 0) {
            sendUserMessageWithUrl(text, urls.get(0));
            return;
        }

        UserMessage tempUserMessage = mChannel.sendUserMessage(text, new BaseChannel.SendUserMessageHandler() {
            @Override
            public void onSent(UserMessage userMessage, SendBirdException e) {
                if (e != null) {
                    // Error!
                    Log.e(LOG_TAG, e.toString());
                    Toast.makeText(
                            getActivity(), getResources().getString(R.string.send_message_error) + ": " + e.getMessage(), Toast.LENGTH_SHORT)
                            .show();
                    mChatAdapter.markMessageFailed(userMessage.getRequestId());
                    return;
                }

                // Update a sent message to RecyclerView
                mChatAdapter.markMessageSent(userMessage);
            }
        });

        // Display a user message to RecyclerView
        mChatAdapter.addFirst(tempUserMessage);
    }

    /**
     * Notify other users whether the current user is typing.
     *
     * @param typing Whether the user is currently typing.
     */
    private void setTypingStatus(boolean typing) {
        if (mChannel == null) {
            return;
        }

        if (typing) {
            mChannel.startTyping();
        } else {
            mChannel.endTyping();
        }
    }

    /**
     * Sends a File Message containing an image file.
     * Also requests thumbnails to be generated in specified sizes.
     *
     * @param uri The URI of the image, which in this case is received through an Intent request.
     */
    private void sendFileWithThumbnail(Uri uri) {
        // Specify two dimensions of thumbnails to generate
        List<FileMessage.ThumbnailSize> thumbnailSizes = new ArrayList<>();
        thumbnailSizes.add(new FileMessage.ThumbnailSize(240, 240));
        thumbnailSizes.add(new FileMessage.ThumbnailSize(320, 320));

        Hashtable<String, Object> info = FileUtils.getFileInfo(getActivity(), uri);

        if (info == null) {
            Toast.makeText(getActivity(), "Extracting file information failed.", Toast.LENGTH_LONG).show();
            return;
        }

        final String path = (String) info.get("path");
        Bitmap bitmap = BitmapFactory.decodeFile(path);
        int rotation = com.team500ae.vncapplication.utils.FileUtils.getRotation(getActivity(), Uri.fromFile(new File(path)), true);
        bitmap = com.team500ae.vncapplication.utils.FileUtils.rotate(bitmap, rotation);
        final File file = new File(path);
        try {
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, new BufferedOutputStream(new FileOutputStream(file)));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        final String name = file.getName();
        final String mime = (String) info.get("mime");
        final int size = (Integer) info.get("size");

        if (path.equals("")) {
            Toast.makeText(getActivity(), "File must be located in local storage.", Toast.LENGTH_LONG).show();
        } else {
            BaseChannel.SendFileMessageWithProgressHandler progressHandler = new BaseChannel.SendFileMessageWithProgressHandler() {
                @Override
                public void onProgress(int bytesSent, int totalBytesSent, int totalBytesToSend) {
                    FileMessage fileMessage = mFileProgressHandlerMap.get(this);
                    if (fileMessage != null && totalBytesToSend > 0) {
                        int percent = (totalBytesSent * 100) / totalBytesToSend;
                        mChatAdapter.setFileProgressPercent(fileMessage, percent);
                    }
                }

                @Override
                public void onSent(FileMessage fileMessage, SendBirdException e) {
                    if (e != null) {
                        Toast.makeText(getActivity(), "" + e.getCode() + ":" + e.getMessage(), Toast.LENGTH_SHORT).show();
                        mChatAdapter.markMessageFailed(fileMessage.getRequestId());
                        return;
                    }

                    mChatAdapter.markMessageSent(fileMessage);
                }
            };

            // Send image with thumbnails in the specified dimensions
            FileMessage tempFileMessage = mChannel.sendFileMessage(file, name, mime, size, "", null, thumbnailSizes, progressHandler);

            mFileProgressHandlerMap.put(progressHandler, tempFileMessage);

            mChatAdapter.addTempFileMessageInfo(tempFileMessage, uri);
            mChatAdapter.addFirst(tempFileMessage);
        }
    }

    private void editMessage(final BaseMessage message, String editedMessage) {
        mChannel.updateUserMessage(message.getMessageId(), editedMessage, null, null, new BaseChannel.UpdateUserMessageHandler() {
            @Override
            public void onUpdated(UserMessage userMessage, SendBirdException e) {
                if (e != null) {
                    // Error!
                    Toast.makeText(getActivity(), "Error " + e.getCode() + ": " + e.getMessage(), Toast.LENGTH_SHORT).show();
                    return;
                }

                mChatAdapter.loadLatestMessages(30, new BaseChannel.GetMessagesHandler() {
                    @Override
                    public void onResult(List<BaseMessage> list, SendBirdException e) {
                        mChatAdapter.markAllMessagesAsRead();
                    }
                });
            }
        });
    }

//    private void getChannelFromUrl(String url) {
//        GroupChannel.getChannel(url, new GroupChannel.GroupChannelGetHandler() {
//            @Override
//            public void onResult(GroupChannel groupChannel, SendBirdException e) {
//                if (e != null) {
//                    // Error!
//                    return;
//                }
//
//                mChannel = groupChannel;
//            }
//        });
//    }
}
