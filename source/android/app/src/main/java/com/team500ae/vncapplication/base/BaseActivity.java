package com.team500ae.vncapplication.base;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.team500ae.vncapplication.R;
import com.team500ae.vncapplication.activities.MainActivity;
import com.team500ae.vncapplication.data_access.RealmInfo;
import com.team500ae.vncapplication.utils.LanguageUtils;

import io.realm.Realm;

/**
 * Created by MaiNam on 11/2/2017.
 */

public class BaseActivity extends com.team500ae.library.activities.BaseActivity {
    public static String TAG = BaseActivity.class.getSimpleName();
    Realm realm;
    private Menu mMenu;

    public Realm getRealm() {
        return RealmInfo.getRealm(getActivity());
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void setContentView(View view) {
        super.setContentView(view);
        this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
    }

    public void showHomeButton(Menu menu) {
        showHomeButton(menu, MainActivity.class);
    }

    public void showHomeButton(Menu menu, Class<?> activity) {
        showHomeButton(menu, activity, null);
    }

    private final int HOME_BUTTON_ID = 1001;
    Intent goHome = null;

    public void showHomeButton(Menu menu, Class<?> activity, Bundle bundle) {
        try {
            if (menu != null && activity != null) {
                MenuItem menuItem = menu.add(0, HOME_BUTTON_ID, 0, "Trang chủ");
                menuItem.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
                menuItem.setIcon(getResources().getDrawable(R.drawable.ic_home_w));

                goHome = new Intent(this, activity);
                goHome.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                if (bundle != null)
                    goHome.putExtras(bundle);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void showBackButton() {
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            // Show the Up button in the action bar.
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setDisplayShowHomeEnabled(true);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        try {

            switch (item.getItemId()) {
                case android.R.id.home:
                    this.finish();
                    slideOut();
                    return true;
                case HOME_BUTTON_ID:
                    if (goHome != null) {
                        startActivity(goHome, TypeSlideIn.in_bottom_to_up);
                    }
                    return true;
                default:
                    return super.onOptionsItemSelected(item);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        mMenu = menu;
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    protected void onDestroy() {
        RealmInfo.closeRealm(realm);
        super.onDestroy();
    }
}
