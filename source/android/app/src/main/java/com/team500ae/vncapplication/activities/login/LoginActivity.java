package com.team500ae.vncapplication.activities.login;

import android.accounts.Account;
import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.InputFilter;
import android.text.InputType;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;
import com.google.gson.JsonObject;
import com.team500ae.library.utils.ConvertUtils;
import com.team500ae.vncapplication.R;
import com.team500ae.vncapplication.base.BaseActivity;
import com.team500ae.vncapplication.constants.Constants;
import com.team500ae.vncapplication.constants.TimeOutConstants;
import com.team500ae.vncapplication.data_access.user.UserInfo;
import com.team500ae.vncapplication.models.BaseReturnFunctionEntity;
import com.team500ae.vncapplication.utils.KeyboardUtils;
import com.team500ae.vncapplication.utils.SecurityUtils;

import org.json.JSONObject;

import java.net.SocketTimeoutException;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnCheckedChanged;
import butterknife.OnClick;

/**
 * Created by Administrator on 22/12/2017.
 */

public class LoginActivity extends BaseActivity implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {

    private static final int RC_SIGN_IN = 9001;
    private String TAG = LoginActivity.class.toString();
    private boolean doubleBackToExit = false;
    private String password, personPhotoUrl;

    private static View view;
    private Animation shakeAnimation;
    private FragmentManager fragmentManager;

    private FirebaseAuth mAuth;
    private CallbackManager callbackManager;
    private GoogleApiClient mGoogleApiClient;

    private EditText edtEmailId, edtPassword;

    //    @BindView(R.id.login_email)
//    EditText edtEmailId;
//    @BindView(R.id.login_password)
//    EditText edtPassword;
    @BindView(R.id.btn_login)
    Button btnLogin;
    @BindView(R.id.forgot_password)
    TextView tvForgotPassword;
    @BindView(R.id.create_account)
    TextView tvCreateAccount;
    @BindView(R.id.show_hide_password)
    CheckBox cbShowHidePassword;
    @BindView(R.id.login_layout)
    LinearLayout loginLayout;
    private GoogleSignInClient mGoogleSignInClient;

    public static void open(Context activity) {
        Intent intent = new Intent(activity, LoginActivity.class);
        activity.startActivity(intent);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        //hide status bar
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_login2);
        ButterKnife.bind(this);
        KeyboardUtils.setupUI(loginLayout, getActivity());
        super.onCreate(savedInstanceState);

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) actionBar.hide();
        //LanguageUtils.changeLanguage(this, LanguageUtils.getAppLanguageSetting(this));

        initLayout();

        logOut();
        FacebookSdk.sdkInitialize(getActivity());
        callbackManager = CallbackManager.Factory.create();

        mAuth = FirebaseAuth.getInstance();
        LoginManager.getInstance().logOut();

        btnLoginFbComponent.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                String accessToken = loginResult.getAccessToken().getToken();

                GraphRequest request = GraphRequest.newMeRequest(loginResult.getAccessToken(), new GraphRequest.GraphJSONObjectCallback() {
                    @Override
                    public void onCompleted(JSONObject object, GraphResponse response) {
                        getHandler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                try {
                                    String strRawResponse = response.getRawResponse();
                                    JsonObject data = ConvertUtils.toJsonObject(strRawResponse);
                                    String fbId = ConvertUtils.toString(data.get("id"));
                                    String name = ConvertUtils.toString(data.get("name"));
                                    String gender = ConvertUtils.toString(data.get("gender"));

                                    loginViaFacebook(fbId, name, gender.equals("male") ? 1 : 0, "https://graph.facebook.com/" + fbId + "/picture?type=large");
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        }, Constants.DELAY_LOGIN_FACEBOOK);
                    }
                });

                Bundle parameters = new Bundle();
                parameters.putString("fields", "id, name, email");
                request.setParameters(parameters);
                request.executeAsync();
            }

            @Override
            public void onCancel() {
                Log.i(TAG, "btnLoginFbComponent>onCancel");
            }

            @Override
            public void onError(FacebookException error) {
                Log.i(TAG, "btnLoginFbComponent>onError" + error.getMessage().toString());
            }
        });

        //login with google
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this, this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();

        FirebaseAuth.getInstance().signOut();
    }

    private void initLayout() {
        View vEmailInclude = findViewById(R.id.layout_email);
        ImageView ivEmailLeftIcon = (ImageView) vEmailInclude.findViewById(R.id.image_left);
        ivEmailLeftIcon.setImageResource(R.drawable.ic_email);
        TextView tvEmailTitle = (TextView) vEmailInclude.findViewById(R.id.text_title);
        tvEmailTitle.setText(getResources().getString(R.string.email));
        edtEmailId = (EditText) vEmailInclude.findViewById(R.id.text_content);
        edtEmailId.setHint(getResources().getString(R.string.only_input_50_characters));
        edtEmailId.setFilters(new InputFilter[]{new InputFilter.LengthFilter(Constants.MAX_FULL_NAME_LENGTH)});
        edtEmailId.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS);
        edtEmailId.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                if (s.length() >= Constants.MAX_FULL_NAME_LENGTH) {
                    new AlertDialog.Builder(LoginActivity.this).setIcon(R.drawable.ic_email).setTitle(getResources().getString(R.string.character_limit_exceeded)).setMessage(getResources().getString(R.string.notice_number_limit_character) + " " + Constants.MAX_FULL_NAME_LENGTH).setPositiveButton(android.R.string.ok, null).show();
                    edtEmailId.requestFocus();
                }
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        ImageView ivEmailRightIcon = (ImageView) vEmailInclude.findViewById(R.id.image_right);
        ivEmailRightIcon.setVisibility(View.GONE);

        View vPasswordInclude = findViewById(R.id.layout_password);
        ImageView ivPasswordLeftIcon = (ImageView) vPasswordInclude.findViewById(R.id.image_left);
        ivPasswordLeftIcon.setImageResource(R.drawable.ic_password);
        TextView tvPasswordTitle = (TextView) vPasswordInclude.findViewById(R.id.text_title);
        tvPasswordTitle.setText(getResources().getString(R.string.passowrd));
        edtPassword = (EditText) vPasswordInclude.findViewById(R.id.text_content);
        edtPassword.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
        edtPassword.setFilters(new InputFilter[]{new InputFilter.LengthFilter(Constants.MAX_FULL_NAME_LENGTH)});

        ImageView ivPasswordRightIcon = (ImageView) vPasswordInclude.findViewById(R.id.image_right);
        ivPasswordRightIcon.setVisibility(View.GONE);
    }

    private void initViews() {
//        fragmentManager = getSupportFragmentManager();

        //load shake animation
//        shakeAnimation = AnimationUtils.loadAnimation(getActivity(), R.anim.shake_animation);
    }

    private void logOut() {
        if (UserInfo.isLogin()) {
            UserInfo.logOut(getBaseContext());
            Intent intent = new Intent(Constants.ACTION_LOGOUT);
            sendBroadcast(intent);
        }
    }

    @OnCheckedChanged(R.id.show_hide_password)
    void showHidePassword(CompoundButton buttonView, boolean isChecked) {
        //if it is checked > show password else hide password
        if (isChecked) {
            //change checkbox text
            cbShowHidePassword.setText(R.string.hide_pwd);

            edtPassword.setInputType(InputType.TYPE_CLASS_TEXT);
            //show password
            edtPassword.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
            edtPassword.requestFocus();
        } else {
            //change checkbox text
            cbShowHidePassword.setText(R.string.show_pwd);

            edtPassword.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
            //show password
            edtPassword.setTransformationMethod(PasswordTransformationMethod.getInstance());
            edtPassword.requestFocus();
        }
    }

    private void loginViaFacebook(String fbId, String name, int gender, String avatar) {
        new AsyncTask<Void, Void, Boolean>() {
            ProgressDialog progDialog = new ProgressDialog(LoginActivity.this);

            @Override
            protected void onPreExecute() {
//                showProgress();
                super.onPreExecute();
                progDialog.setMessage(getString(R.string.activity_login_logging_in));
                progDialog.setIndeterminate(false);
                progDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                progDialog.setCancelable(true);
                progDialog.show();
            }

            @Override
            protected Boolean doInBackground(Void... voids) {

                BaseReturnFunctionEntity<Boolean> result = new UserInfo().loginSocial(getActivity(), UserInfo.SocialType.fb, fbId, name, gender, avatar);
                if (result.isTrue()) {
                    sendBroadcast(new Intent(Constants.ACTION_LOGIN));
                    return result.getData();
                }
                Log.i(TAG, "loginViaFacebook>doInBackground");
                return null;
            }

            @Override
            protected void onPostExecute(Boolean aVoid) {
                try {
                    if (progDialog.isShowing()) {
                        progDialog.dismiss();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                if (aVoid) {
                    finish();
                } else {
//                    showSnackBar(R.string.login_failed);
                    Toast.makeText(getBaseContext(), R.string.login_failed, Toast.LENGTH_SHORT).show();
                }
//                hideProgress();
//                if (aVoid) {
//                    finish();
//                } else {
//                    showSnackBar(R.string.login_failed);
//                }
                super.onPostExecute(aVoid);
            }
        }.execute();
    }

    @OnClick(R.id.create_account)
    public void createAccount() {
        SignUpActivity.open(this);
    }

    @OnClick(R.id.forgot_password)
    void forgotPassword() {
        ForgotPasswordActivity.open(this);
    }

    @BindView(R.id.btn_login_facebook)
    LoginButton btnLoginFbComponent;

    @OnClick(R.id.btn_login_via_fb)
    public void loginViaFacebook() {
        btnLoginFbComponent.performClick();
    }

    @OnClick(R.id.btn_login_via_google)
    public void loginViaGoogle() {
//        Intent signInIntent = mGoogleSignInClient.getSignInIntent();
//        startActivityForResult(signInIntent, RC_SIGN_IN);

        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    private void revokeAccess() {
        Auth.GoogleSignInApi.revokeAccess(mGoogleApiClient).setResultCallback(
                new ResultCallback<Status>() {
                    @Override
                    public void onResult(Status status) {
//                        updateUI(false);
                    }
                });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            handleSignInResult(result);
        } else {
            Log.i(TAG, "onActivityResult: (requestCode != RC_SIGN_IN)");
            callbackManager.onActivityResult(requestCode, resultCode, data);
        }
//        if (requestCode == RC_SIGN_IN) {
//            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
//            handleSignInResult(task);
//        } else {
//            Log.i(TAG, "onActivityResult: (requestCode != RC_SIGN_IN)");
//            callbackManager.onActivityResult(requestCode, resultCode, data);
//        }
    }

    //---
    private void handleSignInResult(GoogleSignInResult result) {
        Log.d(TAG, "handleSignInResult:" + result.isSuccess());
        if (result.isSuccess()) {
            showSnackBar("Login via Google+ is successful");
            // Signed in successfully, show authenticated UI.
            GoogleSignInAccount acct = result.getSignInAccount();

            String personName = acct.getDisplayName();
            personPhotoUrl = Constants.SERVER + "/static/img/default_avatar.png";
            try {
                personPhotoUrl = acct.getPhotoUrl().toString();
            } catch (Exception e) {
                Log.e(TAG, e.getMessage().toString());
            }

            String email = acct.getEmail();
            Account socialId = acct.getAccount();

            Log.e(TAG, "Name: " + personName + ", email: " + email
                    + ", Image: " + personPhotoUrl);

//            showSnackBar("Name: " + personName + " > email: " + email);

            new AsyncTask<Void, Void, Boolean>() {
                ProgressDialog progDialog = new ProgressDialog(LoginActivity.this);

                @Override
                protected void onPreExecute() {
//                showProgress();
                    super.onPreExecute();
                    progDialog.setMessage(getString(R.string.activity_login_logging_in));
                    progDialog.setIndeterminate(false);
                    progDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                    progDialog.setCancelable(true);
                    progDialog.show();
                }

                @Override
                protected Boolean doInBackground(Void... voids) {

                    BaseReturnFunctionEntity<Boolean> result = new UserInfo().loginSocial(getActivity(), UserInfo.SocialType.gg, email, personName, 1, personPhotoUrl);
                    if (result.isTrue()) {
                        sendBroadcast(new Intent(Constants.ACTION_LOGIN));
                        return result.getData();
                    }
                    Log.i(TAG, "loginViaGoogle+>doInBackground");
                    return null;
                }

                @Override
                protected void onPostExecute(Boolean aVoid) {
                    try {
                        if (progDialog.isShowing()) {
                            progDialog.dismiss();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    if (aVoid) {
                        finish();
                    } else {
//                        showSnackBar(R.string.login_failed);
                        Toast.makeText(getBaseContext(), R.string.login_failed, Toast.LENGTH_SHORT).show();
                    }
                    super.onPostExecute(aVoid);
                }
            }.execute();

//        txtName.setText(personName);
//        txtEmail.setText(email);
//        Glide.with(getApplicationContext()).load(personPhotoUrl)
//                .thumbnail(0.5f)
//                .crossFade()
//                .diskCacheStrategy(DiskCacheStrategy.ALL)
//                .into(imgProfilePic);

//        updateUI(true);
        } else {
//            showSnackBar("Login via Google+ is failed");
            Toast.makeText(getBaseContext(), "Login via Google+ is failed", Toast.LENGTH_SHORT).show();
            // Signed out, show unauthenticated UI.
//        updateUI(false);
        }
    }

    //---
    private void handleSignInResult(Task<GoogleSignInAccount> completedTask) {
        try {
            GoogleSignInAccount account = completedTask.getResult(ApiException.class);

            // Signed in successfully, show authenticated UI.
            updateUI(account);
        } catch (ApiException e) {
            // The ApiException status code indicates the detailed failure reason.
            // Please refer to the GoogleSignInStatusCodes class reference for more information.
            Log.w(TAG, "signInResult:failed code=" + e.getStatusCode());
            updateUI(null);
        }
    }

    private void updateUI(GoogleSignInAccount account) {
        Log.d(TAG, "updateUI: ");
    }

    private void firebaseAuthWithGoogle(GoogleSignInAccount account) {
        Log.d(TAG, "firebaseAuthWithGoogle:" + account.getId());

        AuthCredential credential = GoogleAuthProvider.getCredential(account.getIdToken(), null);
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            Log.d(TAG, "signInWithCredential:success");
                            FirebaseUser user = mAuth.getCurrentUser();
                        } else {
                            // If sign in fails, display a message to the user.
                            Log.w(TAG, "signInWithCredential:failure", task.getException());
                        }
                        // ...
                    }
                });
    }

//    @Override
//    public void onBackPressed() {
//        if (doubleBackToExit) {
//            moveTaskToBack(true);
//        }
//
//        this.doubleBackToExit = true;
//        Toast.makeText(this, getString(R.string.please_press_back_to_exit), Toast.LENGTH_SHORT).show();
//        new Handler().postDelayed(new Runnable() {
//            @Override
//            public void run() {
//                doubleBackToExit = false;
//            }
//        }, 2000);
//    }

    @SuppressLint("StaticFieldLeak")
    @OnClick(R.id.btn_login)
    void loginViaEmail() {
        attemptLogin();
    }

    private synchronized void attemptLogin() {
        //todo: check internet connection

        String email = edtEmailId.getText().toString();
        String rawPassword = edtPassword.getText().toString();

        boolean cancel = false;
        View focusView = null;

        // Check for a valid password, if the user entered one.
        if (TextUtils.isEmpty(rawPassword)) {
            edtPassword.setError(getString(R.string.activity_login_error_invalid_password));
            focusView = edtPassword;
            cancel = true;
        }

        // Check for a valid email address.
        if (TextUtils.isEmpty(email)) {
            edtEmailId.setError(getString(R.string.activity_login_error_field_required));
            focusView = edtEmailId;
            cancel = true;
        }

        //check this account is existed or not
//        try {
//            if (!UserInfo.isExistedAccount(email, TimeOutConstants.TIMEOUT_ACCOUNT_CHECKEXIST)) {
//                edtEmailId.setError("Tài khoản này không tồn tại");
//                focusView = edtEmailId;
//                cancel = true;
//            }
//        } catch (Exception e) {
//            showSnackBar("Xảy ra lỗi khi kiểm tra tài khoản '" + email + "' tồn tại hay không: " + e.getMessage());
//            return;
//        }

        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            password = SecurityUtils.encryptMD5(rawPassword);

            new AsyncTask<Void, Void, BaseReturnFunctionEntity<Boolean>>() {
                ProgressDialog progDialog = new ProgressDialog(LoginActivity.this);

                @Override
                protected void onPreExecute() {
                    super.onPreExecute();
                    progDialog.setMessage(getString(R.string.activity_login_logging_in));
                    progDialog.setIndeterminate(false);
                    progDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                    progDialog.setCancelable(true);
                    progDialog.show();
                }

                @Override
                protected BaseReturnFunctionEntity<Boolean> doInBackground(Void... voids) {
                    try {
                        BaseReturnFunctionEntity<Boolean> result = UserInfo.loginEmail(getActivity(), email, password, TimeOutConstants.TIMEOUT_ACCOUNT_LOGIN);
                        if (result.isTrue()) {
                            sendBroadcast(new Intent(Constants.ACTION_LOGIN));
                        }
                        return result;
                    } catch (SocketTimeoutException e) {
                        e.printStackTrace();
                        return new BaseReturnFunctionEntity<>(BaseReturnFunctionEntity.StatusFunction.EXCEPTION, e.getMessage());
                    }
                }

                @Override
                protected void onPostExecute(BaseReturnFunctionEntity<Boolean> result) {
                    try {
                        if (progDialog.isShowing()) {
                            progDialog.dismiss();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    if (result.isTrue()) {
                        finish();
                    } else {
//                        showSnackBar(R.string.login_failed);
                        Toast.makeText(getBaseContext(), result.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                    super.onPostExecute(result);
                }
            }.execute();
        }
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }
}
