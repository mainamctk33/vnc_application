package com.team500ae.vncapplication.activities.location;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.InputFilter;
import android.text.InputType;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.esafirm.imagepicker.features.ImagePicker;
import com.esafirm.imagepicker.model.Image;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStates;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.google.android.gms.maps.model.LatLng;
import com.google.gson.JsonArray;
import com.team500ae.library.adapters.BaseRecyclerAdapter;
import com.team500ae.library.utils.AndroidPermissionUtils;
import com.team500ae.library.utils.ConvertUtils;
import com.team500ae.library.utils.ImageUtils;
import com.team500ae.library.utils.ViewUtils;
import com.team500ae.vncapplication.R;
import com.team500ae.vncapplication.activities.job.interfaces.DialogListener;
import com.team500ae.vncapplication.base.BaseActivity;
import com.team500ae.vncapplication.constants.Constants;
import com.team500ae.vncapplication.data_access.location.LocationInfo;
import com.team500ae.vncapplication.data_access.location.TypeLocationInfo;
import com.team500ae.vncapplication.data_access.upload.BlobUploadProvider;
import com.team500ae.vncapplication.data_access.upload.UploadInfo;
import com.team500ae.vncapplication.models.BaseReturnFunctionEntity;
import com.team500ae.vncapplication.models.ServiceResponseEntity;
import com.team500ae.vncapplication.models.realm_models.location.TypeLocationEntity;
import com.team500ae.vncapplication.others.GridSpacingItemDecoration;
import com.team500ae.vncapplication.utils.AppUtils;
import com.team500ae.vncapplication.utils.GPSTrackerUtils;
import com.team500ae.vncapplication.utils.KeyboardUtils;
import com.team500ae.vncapplication.utils.LanguageUtils;
import com.team500ae.vncapplication.utils.StringUtils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Phuong D. Nguyen on 5/29/2018.
 */

public class AddNewLocationActivity extends BaseActivity implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {

    @BindView(R.id.addLocation_layout)
    LinearLayout addLocationLayout;
    private EditText edtLocationNameContent, edtLocationLocationContent, edtAddressContent, edtIntroductionContent, edtContactContent;
    private TextView tvLocationTypeContent;
    private ImageView ivLocationTypeRightIcon;
    private int locationType;
    protected static final int REQUEST_CHECK_SETTINGS = 0x1;
    protected static final int REQUEST_PICKER_IMAGE = 553;
    private static final int RQ_SELECT_IMAGE = 0x2;
    String urlHeader = "http://maps.google.com/maps/api/staticmap?zoom=16&size=600x600&markers=";
    private static final int REQ_CODE_PLACE_PICKER = 10006;
    private Place place;
    String urlFooter = "&sensor=false";
    private String urlImage;
    private GoogleApiClient mGoogleApiClient;
    private Uri uri;
    private LatLng latlon;
    int type = 3;
    private ArrayList<Integer> typeID;
    private ArrayList<String> typeName;
    List<UriImage> listImages;
    @BindView(R.id.recyclerView1)
    RecyclerView recyclerView1;
    int index = 0;
    private BaseRecyclerAdapter<ViewHolder, UriImage> adapter;
    private Uri tempUri;
    private ArrayList<Uri> listUri = new ArrayList<>();

    class UriImage {
        Uri uri;

        public UriImage(Uri uri) {
            this.uri = uri;
        }

        public UriImage() {

        }
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        setContentView(R.layout.activity_add_location_2);
        ButterKnife.bind(this);
        KeyboardUtils.setupUI(addLocationLayout, getActivity());
        super.onCreate(savedInstanceState);
        showBackButton();
        getSupportActionBar().setTitle(R.string.add_location);

        initLayout();

        showListType();
        tvLocationTypeContent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showLocationTypeDialog();
            }
        });
        ivLocationTypeRightIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showLocationTypeDialog();
            }
        });

        listImages = new ArrayList<>();
        listImages.add(new UriImage());
        adapter = new BaseRecyclerAdapter<>(R.layout.item_image_selected, new BaseRecyclerAdapter.BaseViewHolder<ViewHolder, UriImage>() {
            @Override
            public ViewHolder getViewHolder(View v) {
                return new ViewHolder(v);
            }

            @Override
            public void bindData(RecyclerView recyclerView, ViewHolder viewHolder, UriImage data, int position) {
                if (data.uri == null) {
                    viewHolder.imageView.setVisibility(View.GONE);
                    viewHolder.imgClose.setVisibility(View.GONE);
                    viewHolder.imgCamera.setVisibility(View.VISIBLE);
                } else {
                    ImageUtils.loadImageByGlide(getActivity(), false, 0, 0, data.uri, R.drawable.bg_map_vn, R.drawable.bg_map_vn, viewHolder.imageView, true);
                    viewHolder.imageView.setVisibility(View.VISIBLE);
                    viewHolder.imgClose.setVisibility(View.VISIBLE);
                    viewHolder.imgCamera.setVisibility(View.GONE);
                }
            }
        }, listImages, new BaseRecyclerAdapter.OnClickListener() {
            @Override
            public void onClick(View v, int position, Object o) {

            }
        });
        //recyclerView1.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));
        recyclerView1.setLayoutManager(new GridLayoutManager(getActivity(), 3));
        recyclerView1.addItemDecoration(new GridSpacingItemDecoration(3, getResources().getDimensionPixelSize(R.dimen._5dp), false));
        adapter.bindData(recyclerView1);

        ViewUtils.hideKeyboard(getActivity());

        GPSTrackerUtils gpsTrackerUtils = new GPSTrackerUtils(getActivity());
        if (gpsTrackerUtils.canGetLocation()) {
            Log.d(TAG, "canGetLocation");
            latlon = new LatLng(gpsTrackerUtils.getLatitude(), gpsTrackerUtils.getLongitude());
            GPSTrackerUtils.saveCurrentLocation(getActivity(), gpsTrackerUtils.getLongitude(), gpsTrackerUtils.getLatitude());
            bindData(true);
            return;
        } else {
            settingsrequest();
        }
    }

    private void initLayout() {
        View vLocationNameInclude = findViewById(R.id.layout_location_name);
        ImageView ivLocationNameLeftIcon = (ImageView) vLocationNameInclude.findViewById(R.id.image_left);
        ivLocationNameLeftIcon.setImageResource(R.drawable.ic_job_title);
        TextView tvLocationNameTitle = (TextView) vLocationNameInclude.findViewById(R.id.text_title);
        tvLocationNameTitle.setText(getResources().getString(R.string.location_name_2));
        tvLocationNameTitle.setTextColor(getResources().getColor(R.color.colorPrimary));
        edtLocationNameContent = (EditText) vLocationNameInclude.findViewById(R.id.text_content);
        ImageView ivLocationNameRightIcon = (ImageView) vLocationNameInclude.findViewById(R.id.image_right);
        ivLocationNameRightIcon.setVisibility(View.GONE);

        View vLocationTypeInclude = findViewById(R.id.layout_location_type);
        ImageView ivLocationTypeLeftIcon = (ImageView) vLocationTypeInclude.findViewById(R.id.image_left);
        ivLocationTypeLeftIcon.setImageResource(R.drawable.ic_type_question);
        TextView tvLocationTypeTitle = (TextView) vLocationTypeInclude.findViewById(R.id.text_title);
        tvLocationTypeTitle.setText(getResources().getString(R.string.location_type_2));
        tvLocationTypeContent = (TextView) vLocationTypeInclude.findViewById(R.id.text_content);
        ivLocationTypeRightIcon = (ImageView) vLocationTypeInclude.findViewById(R.id.image_right);
        ivLocationTypeRightIcon.setImageResource(R.drawable.ic_next);

        View vLocationLocationInclude = findViewById(R.id.layout_location_location);
        ImageView ivLocationLocationLeftIcon = (ImageView) vLocationLocationInclude.findViewById(R.id.image_left);
        ivLocationLocationLeftIcon.setImageResource(R.drawable.ic_address);
        TextView tvLocationLocationTitle = (TextView) vLocationLocationInclude.findViewById(R.id.text_title);
        tvLocationLocationTitle.setText(getResources().getString(R.string.location_location));
        edtLocationLocationContent = (EditText) vLocationLocationInclude.findViewById(R.id.text_content);
        edtLocationLocationContent.setVisibility(View.GONE);
        ImageView ivLocationLocationRightIcon = (ImageView) vLocationLocationInclude.findViewById(R.id.image_right);
        ivLocationLocationRightIcon.setVisibility(View.GONE);

        View vAddressInclude = findViewById(R.id.layout_address);
        ImageView ivAddressLeftIcon = (ImageView) vAddressInclude.findViewById(R.id.image_left);
        ivAddressLeftIcon.setImageResource(R.drawable.ic_company);
        TextView tvAddressTitle = (TextView) vAddressInclude.findViewById(R.id.text_title);
        tvAddressTitle.setText(getResources().getString(R.string.address));
        edtAddressContent = (EditText) vAddressInclude.findViewById(R.id.text_content);
        ImageView ivAddressRightIcon = (ImageView) vAddressInclude.findViewById(R.id.image_right);
        ivAddressRightIcon.setVisibility(View.GONE);

        View vIntroductionInclude = findViewById(R.id.layout_introduction);
        ImageView ivIntroductionLeftIcon = (ImageView) vIntroductionInclude.findViewById(R.id.image_left);
        ivIntroductionLeftIcon.setImageResource(R.drawable.ic_job_description);
        TextView tvIntroductionTitle = (TextView) vIntroductionInclude.findViewById(R.id.text_title);
        tvIntroductionTitle.setText(getResources().getString(R.string.introduction));
        tvIntroductionTitle.setVisibility(View.GONE);
        edtIntroductionContent = (EditText) vIntroductionInclude.findViewById(R.id.text_content);
        edtIntroductionContent.setGravity(Gravity.LEFT);
        ImageView ivIntroductionRightIcon = (ImageView) vIntroductionInclude.findViewById(R.id.image_right);
        ivIntroductionRightIcon.setVisibility(View.GONE);

        View vContactInclude = findViewById(R.id.layout_contact);
        ImageView ivContactLeftIcon = (ImageView) vContactInclude.findViewById(R.id.image_left);
        ivContactLeftIcon.setImageResource(R.drawable.ic_phone_number);
        TextView tvContactTitle = (TextView) vContactInclude.findViewById(R.id.text_title);
        tvContactTitle.setText(getResources().getString(R.string.job_contact));
        edtContactContent = (EditText) vContactInclude.findViewById(R.id.text_content);
        edtContactContent.setFilters(new InputFilter[]{new InputFilter.LengthFilter(Constants.MAX_PHONE_NUMBER_LENGTH)});
        edtContactContent.setInputType(InputType.TYPE_CLASS_NUMBER);
        edtContactContent.setFilters(new InputFilter[]{new InputFilter.LengthFilter(Constants.MAX_PHONE_NUMBER_LENGTH)});
        ImageView ivContactRightIcon = (ImageView) vContactInclude.findViewById(R.id.image_right);
        ivContactRightIcon.setVisibility(View.GONE);
    }

    @BindView(R.id.imgLocation)
    ImageView imgLocation;

    @OnClick(R.id.imgLocation)
    void getLocation() {
        PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();
        try {
            startActivityForResult(builder.build(getActivity()), REQ_CODE_PLACE_PICKER);
        } catch (GooglePlayServicesRepairableException e) {
            showSnackBar("Bạn cần cài đặt google playservice để sử dụng tính năng này");
            e.printStackTrace();
        } catch (GooglePlayServicesNotAvailableException e) {
            showSnackBar("Bạn cần cài đặt google playservice để sử dụng tính năng này");
            e.printStackTrace();
        }
    }

//    @BindView(R.id.edtAddressContent)
//    EditText edtAddressContent;
//
//    @BindView(R.id.edtLocationNameContent)
//    EditText edtLocationNameContent;
//
//    @BindView(R.id.edtIntroductionContent)
//    EditText edtIntroductionContent;
//
//    @BindView(R.id.edtContactContent)
//    EditText edtContactContent;

    @OnClick(R.id.btn_add_location)
    void add() {
        if (validateAddLocation()) {
            new UploadInfo(new BlobUploadProvider() {
            }, new UploadInfo.UploadListener() {
                @Override
                public void beginUpload() {
                    try {
                        dialog = ProgressDialog.show(getActivity(), "",
                                getString(R.string.please_wait), true);

                        dialog.show();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }

                @Override
                public void endUpload() {

                }

                @Override
                public void onSuccess(JsonArray response) {
                    if (response.size() == listUri.size()) {
                        addLocation(edtLocationNameContent.getText().toString(), locationType, edtIntroductionContent.getText().toString(), latlon.latitude, latlon.longitude, edtContactContent.getText().toString(), ConvertUtils.toJson(response), edtAddressContent.getText().toString());
                    } else {
                        showSnackBar(R.string.msg_upload_image_error);
                    }
                }

                @Override
                public void onError(ServiceResponseEntity<JsonArray> response) {
                    try {
                        dialog.hide();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }).upload(getActivity(), Constants.UPLOAD_FILE, listUri);
        }
    }

//    @BindView(R.id.txtLocationType)
//    Spinner txtLocationType;

    private void showLocationTypeDialog() {
        final String[] arrayLocationType = typeName.toArray(new String[0]);
        String currentJobCategory = tvLocationTypeContent.getText().toString();
        int itemSelected = StringUtils.getStringIndex(typeName, currentJobCategory);

        new AlertDialog.Builder(this).setTitle(getResources().getString(R.string.location_type_2)).setSingleChoiceItems(arrayLocationType, itemSelected, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int selectedIndex) {
                tvLocationTypeContent.setText(arrayLocationType[selectedIndex]);
            }
        }).setIcon(R.drawable.ic_type_question).setNegativeButton(getResources().getString(R.string.close), null).show();
    }

    void showListType() {
        try {
            BaseReturnFunctionEntity<ArrayList<TypeLocationEntity>> typeLocationEntities = new TypeLocationInfo().getListTypeLocation(getActivity(), getRealm());
            typeID = new ArrayList<>();
            typeName = new ArrayList<>();
            String appLanguageSetting = LanguageUtils.getAppLanguageSetting(this);
            if (typeLocationEntities.isTrue())
                for (TypeLocationEntity type : typeLocationEntities.getData()) {
                    typeID.add(type.getId());
                    typeName.add(appLanguageSetting.equals("en") ? type.getName() : type.getEnName());
                }

//            txtLocationType.setAdapter(new ArrayAdapter<String>(getApplicationContext(), R.layout.item_spinner, typeName));
        } catch (Exception ex) {
        }
    }

    private void addLocation(String name, int type, String content, double lat, double lon, String contact, String thumbnail, String address) {
        new AsyncTask<Void, Void, Boolean>() {
            @Override
            protected void onPreExecute() {
                try {
                    dialog.show();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                super.onPreExecute();
            }

            @Override
            protected Boolean doInBackground(Void... voids) {
                LocationInfo locationInfo = new LocationInfo();
                BaseReturnFunctionEntity<Boolean> result = locationInfo.addLocation(name, type, content, lat, lon, contact, thumbnail, address);
                if (result.isTrue())
                    return result.getData();
                return false;
            }

            @Override
            protected void onPostExecute(Boolean aVoid) {
                if (aVoid != null && aVoid) {
                    BaseActivity.showSnackBar(getActivity(), R.string.add_location_success);
                    getHandler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            finish();
                        }
                    }, 1000);
                } else
                    BaseActivity.showSnackBar(getActivity(), R.string.add_location_error);

                try {
                    dialog.hide();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                super.onPostExecute(aVoid);
            }
        }.execute();
    }

    public void settingsrequest() {

        mGoogleApiClient = new GoogleApiClient.Builder(getActivity())
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();
        mGoogleApiClient.connect();

        LocationRequest locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(30 * 1000);
        locationRequest.setFastestInterval(5 * 1000);
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(locationRequest);
        builder.setAlwaysShow(true); //this is the key ingredient

        PendingResult<LocationSettingsResult> result =
                LocationServices.SettingsApi.checkLocationSettings(mGoogleApiClient, builder.build());
        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(LocationSettingsResult result) {
                final Status status = result.getStatus();
                final LocationSettingsStates state = result.getLocationSettingsStates();
                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.SUCCESS:
                        Log.d(TAG, "SUCCESS");
                        try {
                            status.startResolutionForResult(getActivity(), REQUEST_CHECK_SETTINGS);
                        } catch (IntentSender.SendIntentException e) {
                            finish();
                        }
                        break;
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        Log.d(TAG, "RESOLUTION_REQUIRED");
                        try {
                            status.startResolutionForResult(getActivity(), REQUEST_CHECK_SETTINGS);
                        } catch (IntentSender.SendIntentException e) {
                            e.printStackTrace();
                            finish();
                        }
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        finish();
                        break;
                }
            }
        });
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {

            case REQUEST_PICKER_IMAGE:
                if (ImagePicker.shouldHandle(requestCode, resultCode, data)) {
                    List<Image> images = ImagePicker.getImages(data);
                    if (listImages.size() + images.size() > 5)
                        Toast.makeText(this, "Please select up to 4 images", Toast.LENGTH_SHORT).show();
                    else {
                        for (int i = 0; i < images.size(); i++) {
                            Image image = images.get(i);
                            listImages.add(index, new UriImage(Uri.parse(image.getPath())));
                        }
                        adapter.notifyDataSetChanged();
                    }
                }
                break;
            case REQUEST_CHECK_SETTINGS:
                switch (resultCode) {
                    case Activity.RESULT_OK:
                        getHandler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                GPSTrackerUtils gpsTrackerUtils = new GPSTrackerUtils(getActivity());
                                if (gpsTrackerUtils.canGetLocation()) {
                                    GPSTrackerUtils.saveCurrentLocation(getActivity(), gpsTrackerUtils.getLongitude(), gpsTrackerUtils.getLatitude());
                                }
                                bindData(false);
                            }
                        }, 1000);
                        break;
                    case Activity.RESULT_CANCELED:
                        bindData(true);
                        break;
                    default:
                        bindData(true);
                        break;
                }
                break;
            case REQ_CODE_PLACE_PICKER:
                if (resultCode == RESULT_OK) {
                    place = PlacePicker.getPlace(getActivity(), data);
                    if (edtLocationNameContent.getText().toString().trim().equals("") && place.getName() != null) {
                        edtLocationNameContent.setText(place.getName());
                    }
//                    if (edtAddressContent.getText().toString().trim().equals("") && place.getAddress() != null) {
                    edtAddressContent.setText(place.getAddress());
//                    }
                    if (edtContactContent.getText().toString().trim().equals("") && place.getWebsiteUri() != null) {
                        String contact = "";
                        if (TextUtils.isEmpty(place.getPhoneNumber()))
                            contact += place.getPhoneNumber() + "\n";
                        if (TextUtils.isEmpty(place.getWebsiteUri().toString()))
                            contact += place.getWebsiteUri().toString();
                        edtContactContent.setText(contact);
                    }
                    latlon = place.getLatLng();
                    urlImage = urlHeader + place.getLatLng().latitude + "," + place.getLatLng().longitude + urlFooter;
                    ImageUtils.loadImageByGlide(getActivity(), false, 0, 150, urlImage, R.drawable.bg_map_vn, R.drawable.bg_map_vn, imgLocation, true);
                }
                break;
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void bindData(boolean withLonLat) {
//        getControl();

        double latitude = GPSTrackerUtils.getCurrentLocationLatitude(getActivity());
        double longitude = GPSTrackerUtils.getCurrentLocationLongitude(getActivity());
        if (!withLonLat) {
            latitude = GPSTrackerUtils.getCurrentLocationLatitude(getActivity());
            longitude = GPSTrackerUtils.getCurrentLocationLongitude(getActivity());
        } else
//            tvPlace.setText(xa + ", " + huyen + ", " + tinh);
            if (latitude != 0 | longitude != 0) {
                urlImage = urlHeader + latitude + "," + longitude + urlFooter;
                ImageUtils.loadImageByGlide(getActivity(), false, 0, 150, urlImage, R.drawable.bg_map_vn, R.drawable.bg_map_vn, imgLocation, true);
            }
    }

    public static void open(Context context) {
        Intent intent = new Intent(context, AddNewLocationActivity.class);
        context.startActivity(intent);
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                if (checkValueAddLocation()) {
                    AppUtils.showAlertConfirm(this, getString(R.string.notice), getString(R.string.str_add_new_location_confirm_exit), new DialogListener() {
                        @Override
                        public void onConfirmClicked() {
                            AddNewLocationActivity.this.finish();
                            slideOut();
                        }
                    });
                } else {
                    this.finish();
                    slideOut();
                }

                break;
        }
        return false;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.imageView)
        ImageView imageView;
        @BindView(R.id.imgCamera)
        View imgCamera;
        @BindView(R.id.imgClose)
        View imgClose;

        @OnClick(R.id.imgClose)
        void remove() {
            int index = getAdapterPosition();
            listImages.remove(index);
            adapter.notifyDataSetChanged();
            if (listImages.size() > 0) {
                if (listImages.get(listImages.size() - 1).uri != null) {
                    listImages.add(new UriImage());
                }
            }
        }

        @OnClick(R.id.lnImage)
        void selectImage() {
            index = getAdapterPosition();
            requestPermission(new AndroidPermissionUtils.OnCallbackRequestPermission() {
                @Override
                public void onSuccess() {
                    if (listImages.size() >= 5) {
                        Toast.makeText(getActivity(), "Please select up to 4 images", Toast.LENGTH_SHORT).show();
                    } else {
                        ImagePicker.create(getActivity())
                                .showCamera(true)
                                .theme(R.style.ImagePickerTheme)
                                .start();
                    }
                }

                @Override
                public void onFailed() {

                }
            }, AndroidPermissionUtils.TypePermission.PERMISSION_READ_EXTERNAL_STORAGE);
        }

        public ViewHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);
        }
    }

    public boolean validateAddLocation() {
        String str = "";
        listUri = new ArrayList<>();
        for (UriImage uriImage : listImages) {
            if (uriImage.uri != null)
                listUri.add(uriImage.uri);
        }
        if (listUri.size() == 0) {
            str = getString(R.string.please_select_image_for_location);
        } else if (place == null && latlon == null) {
            str = getString(R.string.please_select_place);
        } else if (edtLocationNameContent.getText().toString().trim().equals("")) {
            str = getString(R.string.please_input_location_name);
        } else if (edtLocationNameContent.getText().toString().trim().equals("")) {
            str = getString(R.string.please_input_location_name);
        } else if (edtAddressContent.getText().toString().trim().equals("")) {
            str = getString(R.string.please_input_location_address);
        } else if (edtContactContent.getText().toString().trim().equals("")) {
            str = getString(R.string.please_input_location_contact);
        } else if (edtIntroductionContent.getText().toString().trim().equals("")) {
            str = getString(R.string.please_input_location_content);
        } else if (tvLocationTypeContent.getText().toString().trim().equals("")) {
            str = getString(R.string.please_select_location_type);
        }
        String locationTypeValue = tvLocationTypeContent.getText().toString().trim();
        locationType = StringUtils.getIdFromValue(typeName, typeID, locationTypeValue);
        if (!TextUtils.isEmpty(str)) {
            showSnackBar(str);
            return false;
        }
        return true;
    }

    public boolean checkValueAddLocation() {
        listUri = new ArrayList<>();
        for (UriImage uriImage : listImages) {
            if (uriImage.uri != null)
                listUri.add(uriImage.uri);
        }
        return listUri.size() != 0 || place != null && latlon != null || !TextUtils.isEmpty(edtLocationNameContent.getText()) || !TextUtils.isEmpty(edtLocationNameContent.getText()) || !TextUtils.isEmpty(edtAddressContent.getText()) || !TextUtils.isEmpty(edtContactContent.getText()) || !TextUtils.isEmpty(edtIntroductionContent.getText()) || !TextUtils.isEmpty(tvLocationTypeContent.getText());
    }

    @Override
    public void onBackPressed() {
        if (checkValueAddLocation()) {
            AppUtils.showAlertConfirm(this, getString(R.string.notice), getString(R.string.str_add_new_location_confirm_exit), new DialogListener() {
                @Override
                public void onConfirmClicked() {
                    AddNewLocationActivity.super.onBackPressed();
                }
            });
        } else
            super.onBackPressed();
    }

}
