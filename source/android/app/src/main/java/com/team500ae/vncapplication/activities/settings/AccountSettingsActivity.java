package com.team500ae.vncapplication.activities.settings;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.team500ae.library.utils.ConvertUtils;
import com.team500ae.vncapplication.R;
import com.team500ae.vncapplication.activities.login.ChangePasswordActivity;
import com.team500ae.vncapplication.activities.login.EditAccountActivity;
import com.team500ae.vncapplication.activities.login.LoginActivity;
import com.team500ae.vncapplication.base.BaseActivity;
import com.team500ae.vncapplication.constants.Constants;
import com.team500ae.vncapplication.data_access.user.UserInfo;

import butterknife.BindView;
import butterknife.OnClick;

public class AccountSettingsActivity extends BaseActivity {

    @BindView(R.id.layout_edit_account)
    View vEditAccount;
    @BindView(R.id.layout_change_password)
    View vChangePassword;
    @BindView(R.id.layout_log_out)
    View vLogOut;

    public static void open(Context activity) {
        Intent intent = new Intent(activity, AccountSettingsActivity.class);
        activity.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setContentView(R.layout.activity_account_settings);
        super.onCreate(savedInstanceState);
        showBackButton();

        initLayout();
    }

    private void initLayout() {
        ImageView ivEditAccountLeftIcon = (ImageView) vEditAccount.findViewById(R.id.iv_left_icon);
        ivEditAccountLeftIcon.setImageResource(R.drawable.ic_account);
        TextView tvEditAccountTitle = (TextView) vEditAccount.findViewById(R.id.tv_title);
        tvEditAccountTitle.setText(getResources().getString(R.string.pref_account_edit_profile_title));
        TextView tvEditAccountSubTitle = (TextView) vEditAccount.findViewById(R.id.tv_sub_title);
        tvEditAccountSubTitle.setText(getResources().getString(R.string.settings_edit_account_sub_title));

        ImageView ivChangePasswordLeftIcon = (ImageView) vChangePassword.findViewById(R.id.iv_left_icon);
        ivChangePasswordLeftIcon.setImageResource(R.drawable.ic_password);
        TextView tvChangePasswordTitle = (TextView) vChangePassword.findViewById(R.id.tv_title);
        tvChangePasswordTitle.setText(getResources().getString(R.string.pref_account_change_password_title));
        TextView tvChangePasswordSubTitle = (TextView) vChangePassword.findViewById(R.id.tv_sub_title);
        tvChangePasswordSubTitle.setText(getResources().getString(R.string.settings_change_password_sub_title));

        ImageView ivLogOutLeftIcon = (ImageView) vLogOut.findViewById(R.id.iv_left_icon);
        ivLogOutLeftIcon.setImageResource(R.drawable.ic_log_out);
        TextView tvLogOutTitle = (TextView) vLogOut.findViewById(R.id.tv_title);
        tvLogOutTitle.setText(getResources().getString(R.string.pref_account_log_out_title));
        TextView tvLogOutSubTitle = (TextView) vLogOut.findViewById(R.id.tv_sub_title);
        tvLogOutSubTitle.setText(getResources().getString(R.string.settings_log_out_sub_title));
    }

    @OnClick(R.id.layout_edit_account)
    void editAccount() {
        if (!UserInfo.isLogin()) {
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            builder.setTitle(getResources().getString(R.string.pref_account_edit_profile_title));
            builder.setMessage(getResources().getString(R.string.msg_have_not_login));
            builder.setIcon(R.drawable.ic_message);
            builder.setPositiveButton(getString(android.R.string.ok),
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            // do the work to update the preference ---
                        }
                    });
            builder.create().show();
        } else {
            EditAccountActivity.open(getActivity());
        }
    }

    @OnClick(R.id.layout_change_password)
    void changePassword() {
        if (!UserInfo.isLogin()) {
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            builder.setTitle(getResources().getString(R.string.pref_account_change_password_title));
            builder.setMessage(getResources().getString(R.string.msg_have_not_login));
            builder.setIcon(R.drawable.ic_message);
            builder.setPositiveButton(getString(android.R.string.ok),
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            // do the work to update the preference ---
                        }
                    });
            builder.create().show();
        } else {
            String socialId=ConvertUtils.toString(UserInfo.getCurrentUser().get("SocialId"));
            if(socialId!=null && !socialId.isEmpty()){
                Toast.makeText(getBaseContext(), R.string.can_not_change_password_with_social_account, Toast.LENGTH_SHORT).show();
                return;
            }

            //only allow changing password with non-social account
            ChangePasswordActivity.open(getActivity());
        }
    }

    @OnClick(R.id.layout_log_out)
    void logOut() {
        if (!UserInfo.isLogin()) {
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            builder.setTitle(getResources().getString(R.string.pref_account_log_out_title));
            builder.setMessage(getResources().getString(R.string.msg_have_not_login));
            builder.setIcon(R.drawable.ic_message);
            builder.setPositiveButton(getString(android.R.string.ok),
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            // do the work to update the preference ---
                        }
                    });
            builder.create().show();
        } else {
            UserInfo.logOut(getActivity());
            Intent intent = new Intent(Constants.ACTION_LOGOUT);
            sendBroadcast(intent);

            LoginActivity.open(getActivity());
        }
    }
}
