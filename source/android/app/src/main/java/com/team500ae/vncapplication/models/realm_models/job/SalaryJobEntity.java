package com.team500ae.vncapplication.models.realm_models.job;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by SVIP on 12/18/2017.
 */

public class SalaryJobEntity extends RealmObject {
    @PrimaryKey
    private int id;
    private String name;
    private int from;
    private int to;
    public int getId(){
        return  id;
    }
    public void setId(int id){
        this.id = id;
    }
    public String getName(){
        return name;
    }
    public void setName(String name){
        this.name = name;
    }
    public int getFrom(){
        return  from;
    }
    public void setFrom(int from){
        this.from = from;
    }
    public int getTo(){
        return  to;
    }
    public void setTo(int to){
        this.to = to;
    }
}
