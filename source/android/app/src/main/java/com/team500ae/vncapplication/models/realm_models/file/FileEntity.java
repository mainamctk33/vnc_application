package com.team500ae.vncapplication.models.realm_models.file;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class FileEntity extends RealmObject {
    @PrimaryKey
    private String url;
    private long downloadDate;
    private boolean finished;
    private long id;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public long getDownloadDate() {
        return downloadDate;
    }

    public void setDownloadDate(long downloadDate) {
        this.downloadDate = downloadDate;
    }

    public boolean isFinished() {
        return finished;
    }

    public void setFinished(boolean finished) {
        this.finished = finished;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }
}
