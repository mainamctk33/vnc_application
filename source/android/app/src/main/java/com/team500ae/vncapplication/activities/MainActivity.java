package com.team500ae.vncapplication.activities;

import android.annotation.SuppressLint;
import android.app.DownloadManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Configuration;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.StringRes;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.JsonArray;
import com.team500ae.library.utils.ImageUtils;
import com.team500ae.vncapplication.Connectivity;
import com.team500ae.vncapplication.R;
import com.team500ae.vncapplication.activities.chatcontact.ChatContactActivity;
import com.team500ae.vncapplication.activities.friend.SearchFriendActivity;
import com.team500ae.vncapplication.activities.job.DetailJobActivity;
import com.team500ae.vncapplication.activities.job.fragments.JobFragment;
import com.team500ae.vncapplication.activities.job.fragments.JobTabFragment;
import com.team500ae.vncapplication.activities.location.LocationSearchActivity;
import com.team500ae.vncapplication.activities.location.fragments.LocationSearchFragment;
import com.team500ae.vncapplication.activities.login.LoginActivity;
import com.team500ae.vncapplication.activities.news.fragments.NewsFragment;
import com.team500ae.vncapplication.activities.news.fragments.NewsTabFragment;
import com.team500ae.vncapplication.activities.settings.HelpAndFeedbackActivity;
import com.team500ae.vncapplication.activities.settings.Settings2Activity;
import com.team500ae.vncapplication.activities.tips.fragments.TipsFragment;
import com.team500ae.vncapplication.activities.tips.fragments.TipsTabFragment;
import com.team500ae.vncapplication.activities.translator.fragments.TranslatorCommonFragment;
import com.team500ae.vncapplication.activities.translator.fragments.TranslatorFragment;
import com.team500ae.vncapplication.activities.translator.fragments.TranslatorMyFavouriteFragment;
import com.team500ae.vncapplication.activities.translator.fragments.TranslatorTabFragment;
import com.team500ae.vncapplication.base.BaseActivity;
import com.team500ae.vncapplication.constants.Constants;
import com.team500ae.vncapplication.data_access.file.FileInfo;
import com.team500ae.vncapplication.data_access.job.JobInfo;
import com.team500ae.vncapplication.data_access.news.NewsInfo;
import com.team500ae.vncapplication.data_access.tips.TipsInfo;
import com.team500ae.vncapplication.data_access.translator.TranslatorInfo;
import com.team500ae.vncapplication.data_access.translator.TypeTranslatorInfo;
import com.team500ae.vncapplication.data_access.user.UserInfo;
import com.team500ae.vncapplication.models.ServiceResponseEntity;
import com.team500ae.vncapplication.services.ServiceJob;
import com.team500ae.vncapplication.services.ServiceLocation;
import com.team500ae.vncapplication.services.ServiceNews;
import com.team500ae.vncapplication.services.ServiceTips;
import com.team500ae.vncapplication.services.ServiceTranslator;
import com.team500ae.vncapplication.utils.AppUtils;
import com.team500ae.vncapplication.utils.LanguageUtils;
import com.team500ae.vncapplication.utils.SharedPrefUtils;

import java.util.Objects;

import butterknife.BindView;
import de.hdodenhof.circleimageview.CircleImageView;

public class MainActivity extends BaseActivity
        implements NavigationView.OnNavigationItemSelectedListener, NewsTabFragment.LoadNewsListener, JobTabFragment.LoadListener, TranslatorCommonFragment.LoadTranslatorListener, TranslatorMyFavouriteFragment.LoadTranslatorListener, TranslatorTabFragment.LoadTranslatorListener, TipsTabFragment.LoadTipsListener, com.team500ae.library.activities.BaseActivity.OnCallBackBroadcastListener {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.drawer_layout)
    DrawerLayout drawer;
    @BindView(R.id.nav_view)
    NavigationView navigationView;
    @BindView(R.id.mainLayout)
    LinearLayout mainLayout;
    //    @Nullable
//    @BindView(R.id.nav_full_name)
    TextView tvFullName;
    //    @Nullable
//    @BindView(R.id.nav_email)
    TextView tvEmail;

    private NewsFragment newsFragment;
    private JobFragment jobFragment;
    private TipsFragment tipsFragment;
    private TranslatorFragment translatorFragment;
    private NavigationView mNavigationView;
    private Fragment locationFragment;
    Menu menuNavigate;
    private MenuItem btnLogout;
    private View lnLogin;
    private View lnLoginInformation;
    private CircleImageView ivAvatar;
    private boolean isFirstNetWorkAvailable;
    private DownloadManager mDownloadManager;
//    private LoginFragment loginFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        if(!Connectivity.isConnected(getActivity()))
        {
            Toast.makeText(this, R.string.please_check_internet_connection, Toast.LENGTH_SHORT).show();
            this.finish();
            return;
        }

        //Tam Tran check app is first run
        boolean firstrun = getSharedPreferences("PREFERENCE", MODE_PRIVATE).getBoolean("firstrun", true);
        if (firstrun) {
            getSharedPreferences("PREFERENCE", MODE_PRIVATE)
                    .edit()
                    .putBoolean("firstrun", false)
                    .commit();
            LanguageUtils.changeLanguage(this, "en");
        } else {
            LanguageUtils.changeLanguage(this, LanguageUtils.getAppLanguageSetting(this));
        }
        setContentView(R.layout.activity_main);
        super.onCreate(savedInstanceState);
        setSupportActionBar(toolbar);
        mDownloadManager = (DownloadManager) getSystemService(Context.DOWNLOAD_SERVICE);
        //add header view to navigation. Header contains: full name and email
        View headerView = navigationView.inflateHeaderView(R.layout.nav_header_main2);
        tvFullName = (TextView) headerView.findViewById(R.id.nav_full_name);
        tvEmail = (TextView) headerView.findViewById(R.id.nav_email);
        ivAvatar = headerView.findViewById(R.id.ivAvatar);
        lnLogin = headerView.findViewById(R.id.lnLogin);
        lnLoginInformation = headerView.findViewById(R.id.lnLoginInformation);
        headerView.findViewById(R.id.btnLogin).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LoginActivity.open(getActivity());
            }
        });

        menuNavigate = navigationView.getMenu();
        btnLogout = menuNavigate.findItem(R.id.nav_log_out);

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        navigationView.setNavigationItemSelectedListener(this);
        registerBroadcastReceiver(
                new ReceiverItem(DownloadManager.ACTION_DOWNLOAD_COMPLETE, this),
                new ReceiverItem(Constants.ACTION_LOGIN, this),
                new ReceiverItem(Constants.ACTION_LOGOUT, this),
                new ReceiverItem(Constants.ACTION_UPDATE_ACCOUNT, this),
                new ReceiverItem(Constants.ACTION_TYPE_NEWS_FOR_USER_CHANGED, this),
                new ReceiverItem(Constants.ACTION_NEWS_VIEW_MODE_CHANGE, this),
                new ReceiverItem(Constants.ACTION_JOB_VIEW_MODE_CHANGE, this),
                new ReceiverItem(Constants.ACTION_NEWS_BOOKMARK_SUCCESS, this),
                new ReceiverItem(Constants.ACTION_TYPE_JOB_FOR_USER_CHANGED, this),
                new ReceiverItem(Constants.ACTION_TYPE_LOCATION_FOR_USER_CHANGED, this),
                new ReceiverItem(Constants.ACTION_JOB_BOOKMARK_SUCCESS, this),
                new ReceiverItem(Constants.ACTION_JOB_ADD_SUCCESS, this),

                new ReceiverItem(Constants.ACTION_TYPE_TIPS_FOR_USER_CHANGED, this),
                new ReceiverItem(Constants.ACTION_TIPS_VIEW_MODE_CHANGE, this),
                new ReceiverItem(Constants.ACTION_TIPS_BOOKMARK_SUCCESS, this),

                new ReceiverItem(Constants.ACTION_LOCATION_BACK_TO_MAIN_APP, this)
        );
        //thanhtam.tr
        getSupportActionBar().setTitle(R.string.title_news);
        getSupportFragmentManager().beginTransaction().replace(R.id.mainLayout, newsFragment = NewsFragment.getInstants(), "fragment").commit();
        updateUsernameOnNavigation();
        getDataTranslator();
        getDataTranslatorDetail();
//        SignUpActivity.openChannel(getActivity());

        registerReceiver(networkChangeReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        intentOpenShareJob(intent);
    }

    private void intentOpenShareJob(Intent intent) {
        try {
            if (intent.getDataString() != null) {
                int idJob = Integer.parseInt(Objects.requireNonNull(intent.getData()).getPathSegments().get(2));
                if (idJob > 0)
                    DetailJobActivity.open(this, idJob);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            if (getSupportFragmentManager().getBackStackEntryCount() == 1) {
                if (doubleBackToExit) {
                    super.onBackPressed();
                    return;
                }
                this.doubleBackToExit = true;
                Toast.makeText(this, getString(R.string.please_press_back_to_exit), Toast.LENGTH_SHORT).show();
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        doubleBackToExit = false;
                    }
                }, 2000);
            } else {
                super.onBackPressed();
            }
        }
    }

    //thanhtam.tr
    boolean doubleBackToExit = false;

    private void updateUsernameOnNavigation() {
        drawer.closeDrawers();
        navigationView.getMenu().getItem(0).setChecked(true);
        lnLogin.setVisibility(View.GONE);
        lnLoginInformation.setVisibility(View.GONE);
        if (UserInfo.isLogin()) {
            tvFullName.setText(UserInfo.getCurrentUserFullName());
            tvEmail.setText(
                    getResources().getString(R.string.app_name));
            ImageUtils.loadImageByGlide(getActivity(), false, 0, 0, UserInfo.getCurrentUserAvartar(), R.drawable.default_avatar, R.drawable.default_avatar, ivAvatar, true);
            btnLogout.setVisible(true);
            lnLoginInformation.setVisibility(View.VISIBLE);
        } else {
            btnLogout.setVisible(false);
            lnLogin.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        try {
            getSupportActionBar().show();
        } catch (Exception e) {

        }
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        switch (id) {
            case R.id.nav_news:
                getSupportFragmentManager().beginTransaction().replace(R.id.mainLayout, newsFragment = NewsFragment.getInstants(), "fragment").commit();
                setActionBarTitle(R.string.title_news);
                break;
            case R.id.nav_job:
                if (!Connectivity.isConnected(getActivity())) {
                    showSnackBar(R.string.please_check_internet_connection);
                    break;
                }
                setActionBarTitle(R.string.title_job);
                getSupportFragmentManager().beginTransaction().replace(R.id.mainLayout, jobFragment = JobFragment.getInstants(), "fragment").commit();
                break;
            case R.id.nav_tips:
                setActionBarTitle(R.string.title_tips);
                getSupportFragmentManager().beginTransaction().replace(R.id.mainLayout, tipsFragment = TipsFragment.getInstants(), "fragment").commit();
                break;
            case R.id.nav_friend_around:
                if (!Connectivity.isConnected(getActivity())) {
                    showSnackBar(R.string.please_check_internet_connection);
                    break;
                }
                SearchFriendActivity.open(getActivity());
                break;
            case R.id.nav_location:
                if (!Connectivity.isConnected(getActivity())) {
                    showSnackBar(R.string.please_check_internet_connection);
                    break;
                }
                LocationSearchActivity.open(getActivity());
                break;
            case R.id.nav_chat:
                if (!Connectivity.isConnected(getActivity())) {
                    showSnackBar(R.string.please_check_internet_connection);
                    break;
                }
                if (UserInfo.isLogin()) {
                    ChatContactActivity.open(getActivity());
                } else {
//                    getSupportFragmentManager().beginTransaction().replace(R.id.mainLayout, contactMessage = ContactAndMessageFragment.getInstants(), "fragment").commit();
                    LoginActivity.open(getActivity());
                }
                break;
            case R.id.nav_log_out:
                if (UserInfo.isLogin()) {
                    UserInfo.logOut(getBaseContext());
                    Intent intent = new Intent(Constants.ACTION_LOGOUT);
                    sendBroadcast(intent);

                } else {
                    showSnackBar(R.string.msg_not_log_in);
                }
                break;
            case R.id.nav_setting:
                Settings2Activity.open(getActivity());
//                Intent modifySettings = new Intent(MainActivity.this, SettingsActivity.class);
//                startActivity(modifySettings);
                break;
            case R.id.nav_translator:
                getSupportActionBar().setTitle(R.string.title_translator);
                getSupportFragmentManager().beginTransaction().replace(R.id.mainLayout, translatorFragment = TranslatorFragment.getInstants(), "fragment").commit();
                break;
            case R.id.nav_about_us:
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://iduhoc.vn/ws/main/ve-chung-toi/"));
                startActivity(browserIntent);
                break;
            case R.id.nav_feedback:
                HelpAndFeedbackActivity.open(this);
                break;
            default:
                showSnackBar(R.string.msg_function_not_available);
        }
        getHandler().postDelayed(new Runnable() {
            @Override
            public void run() {
                drawer.closeDrawer(GravityCompat.START);
            }
        }, 500);
        return true;
    }

    void setActionBarTitle(@StringRes int res) {
        try {
            getSupportActionBar().setTitle(getString(res));

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onBeginLoad(Fragment fragment) {
// do something
    }

    @Override
    public void onEndLoad(Fragment fragment) {
// do something
    }

    @Override
    public JsonArray getData(int page, int size, int type, Fragment fragment) {
        try {
            if (fragment instanceof NewsTabFragment) {

                ServiceResponseEntity<JsonArray> result = new NewsInfo().getAll(getApplicationContext(), page, size, type, "", "");
                if (result != null && result.getStatus() == 0) {
                    return result.getData();
                }
            } else if (fragment instanceof JobTabFragment) {
                ServiceResponseEntity<JsonArray> result = new JobInfo().getAllWithCity(getApplicationContext(), page, size, type);
                if (result != null && result.getStatus() == 0) {
                    return result.getData();
                }
                // call api get list job
            } else if (fragment instanceof TipsTabFragment) {
                ServiceResponseEntity<JsonArray> result = new TipsInfo().getAll(getApplicationContext(), page, size, type, "");
                if (result != null && result.getStatus() == 0) {
                    Log.d(TAG, "Tips size: " + result.getData().size());
                    return result.getData();
                }
                // call api get list tab
            }
            // call api get list translator
            else if (fragment instanceof TranslatorTabFragment) {
                ServiceResponseEntity<JsonArray> result = new TypeTranslatorInfo().getAll();
                if (result != null && result.getStatus() == 0) {
                    SharedPrefUtils.setString(getActivity(), Constants.JSON_DATA_TYPE_TRANSLATOR_LIST, result.getData().toString());
                    return result.getData();
                }
                // call api get list tab
            } else if (fragment instanceof TranslatorMyFavouriteFragment) {
                ServiceResponseEntity<JsonArray> result = new TranslatorInfo().getAll(page, size, type);
                if (result != null && result.getStatus() == 0) {
                    return result.getData();
                }
            } else if (fragment instanceof TranslatorCommonFragment) {
                ServiceResponseEntity<JsonArray> result = new TranslatorInfo().getAll(page, size, type);
                if (result != null && result.getStatus() == 0) {
                    return result.getData();
                }
                // call api get list tab
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @SuppressLint("MissingSuperCall")
    @Override
    public void onSaveInstanceState(Bundle outState) {
    }

    @Override
    public void onCallBackBroadcastListener(Context context, String action, Intent intent) {
        switch (action) {
            case DownloadManager.ACTION_DOWNLOAD_COMPLETE:
                long extraDownloadId = intent.getLongExtra(DownloadManager.EXTRA_DOWNLOAD_ID, -1);
                new FileInfo().addFile(getRealm(), "", true, extraDownloadId);
                break;
            case Constants.ACTION_TYPE_NEWS_FOR_USER_CHANGED:
                getSupportFragmentManager().beginTransaction().replace(R.id.mainLayout, newsFragment = NewsFragment.getInstants(), "fragment").commitAllowingStateLoss();
                getSupportActionBar().setTitle(R.string.title_news);
                break;
            case Constants.ACTION_LOGIN:
                getSupportFragmentManager().beginTransaction().replace(R.id.mainLayout, newsFragment = NewsFragment.getInstants(), "fragment").commitAllowingStateLoss();
                getSupportActionBar().setTitle(R.string.title_news);
                updateUsernameOnNavigation();
                break;
            case Constants.ACTION_UPDATE_ACCOUNT:
                updateUsernameOnNavigation();
                break;
            case Constants.ACTION_LOGOUT:
                getSupportFragmentManager().beginTransaction().replace(R.id.mainLayout, newsFragment = NewsFragment.getInstants(), "fragment").commitAllowingStateLoss();
                getSupportActionBar().setTitle(R.string.title_news);
                updateUsernameOnNavigation();
                break;
            case Constants.ACTION_NEWS_VIEW_MODE_CHANGE:
                getSupportFragmentManager().beginTransaction().replace(R.id.mainLayout, newsFragment = NewsFragment.getInstants(), "fragment").commitAllowingStateLoss();
                getSupportActionBar().setTitle(R.string.title_news);
                break;
            case Constants.ACTION_JOB_VIEW_MODE_CHANGE:
                getSupportFragmentManager().beginTransaction().replace(R.id.mainLayout, jobFragment = JobFragment.getInstants(), "fragment").commitAllowingStateLoss();
                getSupportActionBar().setTitle(R.string.title_job);
                break;
            case Constants.ACTION_NEWS_BOOKMARK_SUCCESS:
                if (newsFragment != null)
                    newsFragment.notifyBookmarkChange();
                break;
            case Constants.ACTION_JOB_BOOKMARK_SUCCESS:
                if (jobFragment != null)
                    jobFragment.notifyBookmarkChange();
                break;
            case Constants.ACTION_JOB_ADD_SUCCESS:
                if (jobFragment != null)
                    jobFragment.notifyAddChange();
                break;
            case Constants.ACTION_TYPE_LOCATION_FOR_USER_CHANGED:
                getSupportFragmentManager().beginTransaction().replace(R.id.mainLayout, locationFragment = LocationSearchFragment.getInstants(), "fragment").commitAllowingStateLoss();
                break;
            case Constants.ACTION_TYPE_JOB_FOR_USER_CHANGED:
                getSupportFragmentManager().beginTransaction().replace(R.id.mainLayout, jobFragment = JobFragment.getInstants(), "fragment").commitAllowingStateLoss();
                getSupportActionBar().setTitle(R.string.title_job);
                break;
            case Constants.ACTION_TYPE_TIPS_FOR_USER_CHANGED:
                getSupportFragmentManager().beginTransaction().replace(R.id.mainLayout, tipsFragment = TipsFragment.getInstants(), "fragment").commitAllowingStateLoss();
                getSupportActionBar().setTitle(R.string.title_tips);
                break;
            case Constants.ACTION_TIPS_VIEW_MODE_CHANGE:
                getSupportFragmentManager().beginTransaction().replace(R.id.mainLayout, tipsFragment = TipsFragment.getInstants(), "fragment").commitAllowingStateLoss();
                getSupportActionBar().setTitle(R.string.title_tips);
                break;
            case Constants.ACTION_TIPS_BOOKMARK_SUCCESS:
                if (tipsFragment != null)
                    tipsFragment.notifyBookmarkChange();
                break;
            case Constants.ACTION_LOCATION_BACK_TO_MAIN_APP:
                navigationView.getMenu().getItem(4).setChecked(true);
                setActionBarTitle(R.string.title_tips);
                getSupportFragmentManager().beginTransaction().replace(R.id.mainLayout, tipsFragment = TipsFragment.getInstants(), "fragment").commit();
                break;
        }
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    public void showMenu() {
        drawer.openDrawer(Gravity.LEFT);
    }

    public void showFragment(Fragment fragment) {
        hideKeyboard();
        getSupportFragmentManager().beginTransaction().replace(R.id.mainLayout, fragment, "fragment").addToBackStack(null).commit();
    }

    private void getDataTranslator() {
        new AsyncTask<Void, Void, ServiceResponseEntity<JsonArray>>() {
            @Override
            protected void onPreExecute() {
                // showProgress();
                super.onPreExecute();

            }

            @Override
            protected ServiceResponseEntity<JsonArray> doInBackground(Void... voids) {
                ServiceResponseEntity<JsonArray> data;
                try {
                    data = new TypeTranslatorInfo().getAll();
                } catch (Exception ex) {
                    return null;
                }
                return data;
            }


            @Override
            protected void onPostExecute(ServiceResponseEntity<JsonArray> data) {

                if (data != null) {
                    SharedPrefUtils.setString(getActivity(), Constants.JSON_DATA_TYPE_TRANSLATOR_LIST, data.getData().toString());
                }

            }


        }.execute();
    }

    private void getDataTranslatorDetail() {
        new AsyncTask<Void, Void, ServiceResponseEntity<JsonArray>>() {
            @Override
            protected void onPreExecute() {
                // showProgress();
                super.onPreExecute();

            }

            @Override
            protected ServiceResponseEntity<JsonArray> doInBackground(Void... voids) {
                ServiceResponseEntity<JsonArray> data;
                try {
                    data = new TranslatorInfo().getAllNew();
                } catch (Exception ex) {
                    return null;
                }
                return data;
            }


            @Override
            protected void onPostExecute(ServiceResponseEntity<JsonArray> data) {

                if (data != null) {
                    SharedPrefUtils.setString(getActivity(), Constants.JSON_DATA_TRANSLATOR_DETAIL_LIST, data.getData().toString());
                }

            }


        }.execute();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(networkChangeReceiver);
    }

    private BroadcastReceiver networkChangeReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (isFirstNetWorkAvailable) {
                if (AppUtils.isNetworkAvailable(MainActivity.this)) {
                    ServiceNews.startActionSyncTypeNews(getApplicationContext());
                    ServiceNews.startActionSyncTag(getApplicationContext());
                    ServiceLocation.startActionSyncTypeLocation(getApplicationContext());
                    ServiceJob.startActionSyncType(getApplicationContext());
                    ServiceJob.startActionSyncCompany(getApplicationContext());
                    ServiceJob.startActionSyncSalary(getApplicationContext());
                    ServiceJob.startActionSyncLevel(getApplicationContext());
                    ServiceJob.startActionSyncCountry(getApplicationContext());
                    ServiceJob.startActionSyncCity(getApplicationContext());
                    ServiceTranslator.startActionSyncCategory(getApplicationContext());
                    ServiceTranslator.startActionSyncType(getApplicationContext());
                    ServiceTips.startActionSyncTypeTips(getApplicationContext());
                    UserInfo.getCurrentUserHaveSaved(getApplicationContext());
                    getDataTranslator();
                    getDataTranslatorDetail();
                }
            }
            isFirstNetWorkAvailable = true;
        }
    };
}
