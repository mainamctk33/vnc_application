package com.team500ae.vncapplication.activities.chat.fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.makeramen.roundedimageview.RoundedImageView;
import com.sendbird.android.AdminMessage;
import com.sendbird.android.BaseMessage;
import com.sendbird.android.FileMessage;
import com.sendbird.android.GroupChannel;
import com.sendbird.android.GroupChannelListQuery;
import com.sendbird.android.SendBirdException;
import com.sendbird.android.UserMessage;
import com.team500ae.library.adapters.BaseRecyclerAdapter;
import com.team500ae.library.layoutmanager.MyLinearLayoutManager;
import com.team500ae.vncapplication.R;
import com.team500ae.vncapplication.activities.chat.CreateGroupChannelActivity;
import com.team500ae.vncapplication.activities.chat.GroupChannelActivity;
import com.team500ae.vncapplication.activities.chat.adapter.GroupChannelListAdapter;
import com.team500ae.vncapplication.activities.chat.utils.DateUtils;
import com.team500ae.vncapplication.activities.chat.utils.TypingIndicator;
import com.team500ae.vncapplication.base.BaseActivity;
import com.team500ae.vncapplication.base.BaseViewHolder;
import com.team500ae.vncapplication.data_access.user.UserInfo;
import com.team500ae.vncapplication.utils.KeyboardUtils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SearchGroupChatFragment extends Fragment implements Filterable {
    private static final String CHANNEL_HANDLER_ID = "CHANNEL_HANDLER_GROUP_CHANNEL_LIST";
    public static final String EXTRA_GROUP_CHANNEL_URL = "GROUP_CHANNEL_URL";
    private GroupChannelListQuery mChannelListQuery;
    ArrayList<GroupChannel> lists;
    private ProgressDialog dialogProgress;
    private ArrayList<GroupChannel> listData;
    BaseRecyclerAdapter<ChannelHolder, GroupChannel> adapter;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        listData = new ArrayList<>();
        lists = new ArrayList<>();
        View view = inflater.inflate(R.layout.fragment_search_group_chat, container, false);
        KeyboardUtils.setupUI(view, getActivity());
        ButterKnife.bind(this, view);
        dialogProgress = new ProgressDialog(getContext());

        dialogProgress.setMessage(getString(R.string.loading));
        dialogProgress.setIndeterminate(false);
        dialogProgress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        dialogProgress.setCancelable(false);
        dialogProgress.show();
        recyclerView.setLayoutManager(new MyLinearLayoutManager(getContext()));
        adapter = new BaseRecyclerAdapter<ChannelHolder, GroupChannel>(R.layout.list_item_group_channel, new BaseRecyclerAdapter.BaseViewHolder<ChannelHolder, GroupChannel>() {
            @Override
            public ChannelHolder getViewHolder(View v) {
                return new ChannelHolder(v);
            }

            @Override
            public void bindData(RecyclerView recyclerView, ChannelHolder viewHolder, GroupChannel data, int position) throws IOException {
                viewHolder.bind(getContext(), data, new GroupChannelListAdapter.OnItemClickListener() {
                    @Override
                    public void onItemClick(GroupChannel channel) {
                        enterGroupChannel(channel);
                    }
                }, new GroupChannelListAdapter.OnItemLongClickListener() {
                    @Override
                    public void onItemLongClick(GroupChannel channel) {
                        BaseActivity.showSnackBar(getContext(), "Test");
                    }
                });
            }
        }, listData, new BaseRecyclerAdapter.OnClickListener() {
            @Override
            public void onClick(View v, int position, Object o) {

            }
        });
        adapter.bindData(recyclerView);
        txtSearch.requestFocus();
        
        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.showSoftInput(txtSearch, InputMethodManager.SHOW_IMPLICIT);
        txtSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (!s.toString().equals("")) {
                    btnClear.setVisibility(View.VISIBLE);
                } else {
                    btnClear.setVisibility(View.GONE);
                }
                getFilter().filter(s.toString());
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        btnClear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                txtSearch.setText("");
                getFilter().filter("");
                btnClear.setVisibility(View.GONE);
            }
        });

        refreshChannelList();

        return view;
    }

    void enterGroupChannel(GroupChannel channel) {
        final String channelUrl = channel.getUrl();

        enterGroupChannel(channelUrl);
    }

    void enterGroupChannel(String channelUrl) {
        GroupChannelActivity.openChannel((AppCompatActivity) getContext(), channelUrl);
//        GroupChatFragment fragment = GroupChatFragment.newInstance(channelUrl);
//        getFragmentManager().beginTransaction()
//                .replace(R.id.container_group_channel, fragment)
//                .addToBackStack(null)
//                .commit();
    }

    @BindView(R.id.txtSearch)
    EditText txtSearch;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.btnClear)
    ImageView btnClear;

    private void refreshChannelList() {
        mChannelListQuery = GroupChannel.createMyGroupChannelListQuery();
        mChannelListQuery.setLimit(100);

        mChannelListQuery.next(new GroupChannelListQuery.GroupChannelListQueryResultHandler() {
            @Override
            public void onResult(List<GroupChannel> list, SendBirdException e) {
                if (list != null)
                    lists.addAll(list);
                dialogProgress.dismiss();
                getFilter().filter("");
                txtSearch.focusSearch(View.FOCUS_UP);
            }
        });
    }

    public static SearchGroupChatFragment getInstance(Context context) {
        SearchGroupChatFragment fragment = new SearchGroupChatFragment();
        return fragment;
    }

    boolean checkGroupChat(GroupChannel item, String keyword) {
        if (item.getName() != null && item.getName().toLowerCase().contains(keyword))
            return true;
        for (com.sendbird.android.Member mem : item.getMembers()) {
            if (mem.getNickname().toLowerCase().contains(keyword))
                return true;
            if (mem.getUserId().toLowerCase().contains(keyword))
                return true;
        }
        if (item.getLastMessage() != null) {
            if (item.getLastMessage() instanceof UserMessage) {
                String message = ((UserMessage) item.getLastMessage()).getMessage();
                if (message != null) {
                    if (message.toLowerCase().contains(keyword))
                        return true;
                }
            }
        }
        return false;

    }

    @OnClick(R.id.fab_group_channel_list)
    void createGroup() {
        Intent intent = new Intent(getContext(), CreateGroupChannelActivity.class);
        startActivity(intent);
    }

    @Override
    public Filter getFilter() {
        Filter filter = new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                FilterResults results = new FilterResults();
                // Skip the autocomplete query if no constraints are given.
                if (constraint == null || constraint.toString().trim().equals("")) {
                    results.values = lists;
                    results.count = lists.size();
                } else {
                    String keyword = constraint.toString().toLowerCase().trim();
                    ArrayList<GroupChannel> list2 = new ArrayList<>();
                    for (GroupChannel item : lists) {
                        if (checkGroupChat(item, keyword))
                            list2.add(item);

                    }
                    results.values = list2;
                    results.count = list2.size();
                }
                return results;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                listData.clear();
                listData.addAll((ArrayList<GroupChannel>) results.values);
                adapter.notifyDataSetChanged();
            }
        };
        return filter;
    }

    public class ChannelHolder extends BaseViewHolder {

        @BindView(R.id.image_group_channel_list_cover)
        RoundedImageView coverImage;
        @BindView(R.id.text_group_channel_list_message)
        TextView lastMessageText;
        @BindView(R.id.text_group_channel_list_topic)
        TextView topicText;
        @BindView(R.id.text_group_channel_list_unread_count)
        TextView unreadCountText;
        @BindView(R.id.text_group_channel_list_date)
        TextView dateText;

        @BindView(R.id.container_group_channel_list_typing_indicator)
        LinearLayout typingIndicatorContainer;

        ChannelHolder(View itemView) {
            super(itemView);
        }

        /**
         * Binds views in the ViewHolder to information contained within the Group Channel.
         *
         * @param context
         * @param channel
         * @param clickListener     A listener that handles simple clicks.
         * @param longClickListener A listener that handles long clicks.
         */
        void bind(final Context context, final GroupChannel channel,
                  @Nullable final GroupChannelListAdapter.OnItemClickListener clickListener,
                  @Nullable final GroupChannelListAdapter.OnItemLongClickListener longClickListener) {
            String title = "";
            String avatar = "";
            int memberCount = channel.getMemberCount();
            List<com.sendbird.android.Member> mems = channel.getMembers();

            if (memberCount == 1) {
                if (!mems.get(0).getUserId().equals(UserInfo.getCurrentUserId())) {
                    title = mems.get(0).getNickname();
                    avatar = mems.get(0).getProfileUrl();
                } else {
                    title = context.getResources().getString(R.string.no_members);
                    avatar = channel.getCoverUrl();
                }
            } else if (memberCount == 2) {
                if (!mems.get(0).getUserId().equals(UserInfo.getCurrentUserId())) {
                    title = mems.get(0).getNickname();
                    avatar = mems.get(0).getProfileUrl();
                } else if (!mems.get(1).getUserId().equals(UserInfo.getCurrentUserId())) {
                    title = mems.get(1).getNickname();
                    avatar = mems.get(01).getProfileUrl();
                } else {
                    title = channel.getName();
                    avatar = channel.getCoverUrl();
                }

            } else if (memberCount > 2) {
                title = channel.getName();
                avatar = channel.getCoverUrl();
            }

            topicText.setText(title);
            com.team500ae.library.utils.ImageUtils.loadImageByGlide(context, false, 0, 0, avatar, R.drawable.img_no_image, R.drawable.img_no_image, coverImage, true);

            int unreadCount = channel.getUnreadMessageCount();
            // If there are no unread messages, hide the unread count badge.
            if (unreadCount == 0) {
                unreadCountText.setVisibility(View.INVISIBLE);
            } else {
                unreadCountText.setVisibility(View.VISIBLE);
                unreadCountText.setText(String.valueOf(channel.getUnreadMessageCount()));
            }

            BaseMessage lastMessage = channel.getLastMessage();
            if (lastMessage != null) {
                // Display information about the most recently sent message in the channel.
                dateText.setText(String.valueOf(DateUtils.formatDateTime(lastMessage.getCreatedAt())));

                // Bind last message text according to the type of message. Specifically, if
                // the last message is a File Message, there must be special formatting.
                if (lastMessage instanceof UserMessage) {
                    lastMessageText.setText(((UserMessage) lastMessage).getMessage());
                } else if (lastMessage instanceof AdminMessage) {
                    lastMessageText.setText(((AdminMessage) lastMessage).getMessage());
                } else {
                    String lastMessageString = String.format(
                            context.getString(R.string.group_channel_list_file_message_text),
                            ((FileMessage) lastMessage).getSender().getNickname());
                    lastMessageText.setText(lastMessageString);
                }
            }

            /*
             * Set up the typing indicator.
             * A typing indicator is basically just three dots contained within the layout
             * that animates. The animation is implemented in the {@link TypingIndicator#animate() class}
             */
            ArrayList<ImageView> indicatorImages = new ArrayList<>();
            indicatorImages.add((ImageView) typingIndicatorContainer.findViewById(R.id.typing_indicator_dot_1));
            indicatorImages.add((ImageView) typingIndicatorContainer.findViewById(R.id.typing_indicator_dot_2));
            indicatorImages.add((ImageView) typingIndicatorContainer.findViewById(R.id.typing_indicator_dot_3));

            TypingIndicator indicator = new TypingIndicator(indicatorImages, 600);
            indicator.animate();

            // debug
//            typingIndicatorContainer.setVisibility(View.VISIBLE);
//            lastMessageText.setText(("Someone is typing"));

            // If someone in the channel is typing, display the typing indicator.
            if (channel.isTyping()) {
                typingIndicatorContainer.setVisibility(View.VISIBLE);
                lastMessageText.setText(R.string.some_one_is_typing);
            } else {
                // Display typing indicator only when someone is typing
                typingIndicatorContainer.setVisibility(View.GONE);
            }

            // Set an OnClickListener to this item.
            if (clickListener != null) {
                itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        clickListener.onItemClick(channel);
                    }
                });
            }

            // Set an OnLongClickListener to this item.
            if (longClickListener != null) {
                itemView.setOnLongClickListener(new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View v) {
                        longClickListener.onItemLongClick(channel);

                        // return true if the callback consumed the long click
                        return true;
                    }
                });
            }
        }
    }
}
