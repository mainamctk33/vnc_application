package com.team500ae.vncapplication.models.realm_models.tips;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by phuong.ndp on 11/27/2017.
 * done
 */

public class MyTypeTipsEntity extends RealmObject {
    @PrimaryKey
    String id;

    String userId;
    TypeTipsEntity typeTips;
    long updatedDate;

    public long getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(long updatedDate) {
        this.updatedDate = updatedDate;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public TypeTipsEntity getTypeTips() {
        return typeTips;
    }

    public void setTypeTips(TypeTipsEntity typeTips) {
        this.typeTips = typeTips;
    }
}
