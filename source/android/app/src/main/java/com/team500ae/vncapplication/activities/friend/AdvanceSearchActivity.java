package com.team500ae.vncapplication.activities.friend;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.team500ae.library.utils.ConvertUtils;
import com.team500ae.library.utils.SharedPreferencesUtil;
import com.team500ae.library.utils.StringUtils;
import com.team500ae.vncapplication.R;
import com.team500ae.vncapplication.base.BaseActivity;
import com.team500ae.vncapplication.constants.Constants;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class AdvanceSearchActivity extends BaseActivity {

    @BindView(R.id.layout_gender)
    View vGenderLayout;
    @BindView(R.id.layout_age)
    View vAgeLayout;
    @BindView(R.id.layout_max_distance)
    View vMaxDistanceLayout;
    @BindView(R.id.layout_search_option)
    View vSearchOptionLayout;

    private TextView tvGenderContent, tvAgeContent, tvDistanceContent, tvSearchOptionContent;

    private int genderIdx = 0, ageIdx = 0, searchIdx = 0;
    private String[] listGenderOptions;
    private String[] listAgeOptions;
    private String[] listSearchOptions;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setContentView(R.layout.activity_advance_search);
        ButterKnife.bind(this);
        super.onCreate(savedInstanceState);
        showBackButton();

        getSupportActionBar().setTitle(R.string.advance_search);

        listGenderOptions = getResources().getStringArray(R.array.genderOptions);
        listAgeOptions = getResources().getStringArray(R.array.ageOptions);
        listSearchOptions = getResources().getStringArray(R.array.search_mode);

        initLayout();
    }

    private void initLayout() {
        genderIdx = SharedPreferencesUtil.getInt(getActivity(), Constants.FILTER_GENDER, 0);
        ageIdx = SharedPreferencesUtil.getInt(getActivity(), Constants.FILTER_AGE, 0);
        searchIdx = SharedPreferencesUtil.getBoolean(getActivity(), Constants.ONLY_FRIEND, false) ? 1 : 0;
        ImageView ivGenderLeftIcon = (ImageView) vGenderLayout.findViewById(R.id.image_left);
        ivGenderLeftIcon.setImageResource(R.drawable.ic_gender);
        TextView tvGenderTitle = (TextView) vGenderLayout.findViewById(R.id.text_title);
        tvGenderTitle.setText(getResources().getString(R.string.gender));
        tvGenderContent = (TextView) vGenderLayout.findViewById(R.id.text_content);
        ImageView ivGenderRightIcon = (ImageView) vGenderLayout.findViewById(R.id.image_right);
        ivGenderRightIcon.setImageResource(R.drawable.ic_next);

        ImageView ivAgeLeftIcon = (ImageView) vAgeLayout.findViewById(R.id.image_left);
        ivAgeLeftIcon.setImageResource(R.drawable.ic_account);
        TextView tvAgeTitle = (TextView) vAgeLayout.findViewById(R.id.text_title);
        tvAgeTitle.setText(getResources().getString(R.string.range_age));
        tvAgeContent = (TextView) vAgeLayout.findViewById(R.id.text_content);
        ImageView ivAgeRightIcon = (ImageView) vAgeLayout.findViewById(R.id.image_right);
        ivAgeRightIcon.setImageResource(R.drawable.ic_next);

        ImageView ivDistanceLeftIcon = (ImageView) vMaxDistanceLayout.findViewById(R.id.image_left);
        ivDistanceLeftIcon.setImageResource(R.drawable.ic_address);
        TextView tvDistanceTitle = (TextView) vMaxDistanceLayout.findViewById(R.id.text_title);
        tvDistanceTitle.setText(getResources().getString(R.string.max_distance));
        tvDistanceContent = (TextView) vMaxDistanceLayout.findViewById(R.id.text_content);
        tvDistanceContent.setText(SharedPreferencesUtil.getString(getActivity(), Constants.MAX_DISTANCE, ""));
        ImageView ivDistanceRightIcon = (ImageView) vMaxDistanceLayout.findViewById(R.id.image_right);
        ivDistanceRightIcon.setVisibility(View.GONE);

        ImageView ivSearchOptionLeftIcon = (ImageView) vSearchOptionLayout.findViewById(R.id.image_left);
        ivSearchOptionLeftIcon.setImageResource(R.drawable.ic_notification);
        TextView tvSearchOptionTitle = (TextView) vSearchOptionLayout.findViewById(R.id.text_title);
        tvSearchOptionTitle.setText(getResources().getString(R.string.search_option));
        tvSearchOptionContent = (TextView) vSearchOptionLayout.findViewById(R.id.text_content);
        ImageView ivSearchOptionRightIcon = (ImageView) vSearchOptionLayout.findViewById(R.id.image_right);
        ivSearchOptionRightIcon.setImageResource(R.drawable.ic_next);


        tvGenderContent.setText(listGenderOptions[genderIdx]);
        tvAgeContent.setText(listAgeOptions[ageIdx]);
        tvSearchOptionContent.setText(listSearchOptions[searchIdx]);
    }

    @OnClick(R.id.layout_gender)
    void showGenderTypeDialog() {
        new AlertDialog.Builder(this).setTitle(getResources().getString(R.string.gender)).setSingleChoiceItems(listGenderOptions, genderIdx, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int selectedIndex) {
                genderIdx = selectedIndex;
                tvGenderContent.setText(listGenderOptions[selectedIndex]);
            }
        }).setIcon(R.drawable.ic_gender).setNegativeButton(getResources().getString(R.string.close), null).show();
    }

    @OnClick(R.id.layout_age)
    void showAgeDialog() {
        new AlertDialog.Builder(this).setTitle(getResources().getString(R.string.range_age)).setSingleChoiceItems(listAgeOptions, ageIdx, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int selectedIndex) {
                ageIdx = selectedIndex;
                tvAgeContent.setText(listAgeOptions[selectedIndex]);
            }
        }).setIcon(R.drawable.ic_account).setNegativeButton(getResources().getString(R.string.close), null).show();
    }

    @OnClick(R.id.layout_search_option)
    void showDistanceDialog() {
        new AlertDialog.Builder(this).setTitle(getResources().getString(R.string.search_option)).setSingleChoiceItems(listSearchOptions, searchIdx, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int selectedIndex) {
                searchIdx = selectedIndex;
                tvSearchOptionContent.setText(listSearchOptions[selectedIndex]);
            }
        }).setIcon(R.drawable.ic_notification).setNegativeButton(getResources().getString(R.string.close), null).show();
    }

    @OnClick(R.id.btn_search)
    void save() {
        SharedPreferencesUtil.setSharedPreferences(getActivity(), SharedPreferencesUtil.EnumType.Int, Constants.FILTER_GENDER, genderIdx);
        SharedPreferencesUtil.setSharedPreferences(getActivity(), SharedPreferencesUtil.EnumType.Int, Constants.FILTER_AGE, ageIdx);
        SharedPreferencesUtil.setSharedPreferences(getActivity(), SharedPreferencesUtil.EnumType.Bool, Constants.ONLY_FRIEND, searchIdx == 1);
        String distance = tvDistanceContent.getText().toString();
        if (!StringUtils.isEmpty(distance))
            distance = String.valueOf(ConvertUtils.toInt(distance));
        SharedPreferencesUtil.setSharedPreferences(getActivity(), SharedPreferencesUtil.EnumType.String, Constants.MAX_DISTANCE, distance.equals("0") ? "" : distance);
        finish();
    }
}
