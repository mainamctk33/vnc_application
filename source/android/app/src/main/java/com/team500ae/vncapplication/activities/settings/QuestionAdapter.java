package com.team500ae.vncapplication.activities.settings;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.team500ae.vncapplication.R;
import com.team500ae.vncapplication.models.QuestionObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Phuong D. Nguyen on 4/18/2018.
 */

public class QuestionAdapter extends RecyclerView.Adapter<QuestionAdapter.MyViewHolder> implements Filterable {

    private Context mContext;
    private ArrayList<QuestionObject> listQuestion;
    private ArrayList<QuestionObject> filterListQuestion;

    public QuestionAdapter(Context mContext, ArrayList<QuestionObject> listQuestion) {
        this.mContext = mContext;
        this.listQuestion = listQuestion;
        this.filterListQuestion = listQuestion;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.question_view_row, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        QuestionObject questionObject = filterListQuestion.get(position);
        holder.tvType.setText(questionObject.getType());
        holder.tvTitle.setText(questionObject.getTitle());
        holder.tvQuestion.setText(questionObject.getQuestion());
        holder.tvAnswer.setText(questionObject.getAnswer());
    }

    @Override
    public int getItemCount() {
        return filterListQuestion.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        public TextView tvType, tvTitle, tvQuestion, tvAnswer;

        public MyViewHolder(View itemView) {
            super(itemView);
            tvType = (TextView) itemView.findViewById(R.id.type_text_content);
            tvTitle = (TextView) itemView.findViewById(R.id.title_text_content);
            tvQuestion = (TextView) itemView.findViewById(R.id.question_text_content);
            tvAnswer = (TextView) itemView.findViewById(R.id.answer_text_content);
        }
    }

    @Override
    public Filter getFilter() {

        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String keyword = charSequence.toString();

                if (keyword.isEmpty()) {
                    filterListQuestion = listQuestion;
                } else {
                    keyword = keyword.toLowerCase();
                    ArrayList<QuestionObject> filteredList = new ArrayList<>();
                    for (QuestionObject obj : listQuestion) {
                        if (obj.getTitle().toLowerCase().contains(keyword) || obj.getQuestion().toLowerCase().contains(keyword) || obj.getAnswer().toLowerCase().contains(keyword)) {
                            filteredList.add(obj);
                        }
                    }

                    filterListQuestion = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = filterListQuestion;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults filterResults) {
                filterListQuestion = (ArrayList<QuestionObject>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }
}
