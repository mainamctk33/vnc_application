package com.team500ae.vncapplication.activities.user;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.JsonObject;
import com.team500ae.library.utils.ConvertUtils;
import com.team500ae.library.utils.ImageUtils;
import com.team500ae.vncapplication.R;
import com.team500ae.vncapplication.activities.chat.GroupChannelActivity;
import com.team500ae.vncapplication.activities.login.EditAccountActivity;
import com.team500ae.vncapplication.base.BaseActivity;
import com.team500ae.vncapplication.constants.Constants;
import com.team500ae.vncapplication.data_access.RealmInfo;
import com.team500ae.vncapplication.data_access.friend.FriendInfo;
import com.team500ae.vncapplication.data_access.job.TypeJobInfo;
import com.team500ae.vncapplication.data_access.user.UserInfo;
import com.team500ae.vncapplication.models.BaseReturnFunctionEntity;
import com.team500ae.vncapplication.models.realm_models.job.TypeJobEntity;
import com.team500ae.vncapplication.utils.AppUtils;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;
import io.realm.Realm;

/**
 * Created by maina on 3/16/2018.
 */

public class UserProfileActivity extends BaseActivity {
    @BindView(R.id.imgLocation)
    ImageView imgLocation;

    private String userId = null;
    private boolean isFriend = false;
    private String friendStatus = "";

    TextView tvFullnameContent, tvEmailContent, tvPhoneNumberContent, tvAddressContent, tvJobContent, tvIntroductionContent;
    TextView tvBirthdayContent, tvGenderContent, tvJobCategoryContent, tvNationContent;
    ImageView ivGenderRightIcon;
    CircleImageView civUserAvatar;

    private String fullname, birthday, username, gender, address, job, jobCategory, phonenumber, introduction, avatarUrl, latAddress, lonAddress;

    //    @BindView(R.id.tv_full_name)
//    TextView tvFullname;
//    @BindView(R.id.tv_email)
//    TextView tvEmail;
//    @BindView(R.id.tv_phone_number)
//    TextView tvPhonenumber;
//    @BindView(R.id.tv_gender)
//    TextView tvGender;
//    @BindView(R.id.tv_birthday)
//    TextView tvBirthday;
//    @BindView(R.id.tv_job)
//    TextView tvJob;
//    @BindView(R.id.tv_job_category)
//    TextView tvJobCategory;
//    @BindView(R.id.tv_address)
//    TextView tvAddress;
    @BindView(R.id.btn_add_friend)
    Button btnAddFriend;

    private Realm realm;
    private ArrayList<Integer> listTypeJobID;
    private ArrayList<String> listTypeJobName;

    public static void open(Context context, String userId) {
        if (UserInfo.isLogin()) {
            if (UserInfo.getCurrentUserId().equals((userId))) {
                EditAccountActivity.open(context);
                return;
            }
        }
        Intent intent = new Intent(context, UserProfileActivity.class);
        intent.putExtra("USER_ID", userId);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
//        setContentView(R.layout.activity_user_profile);
        setContentView(R.layout.activity_detail_profile);
        ButterKnife.bind(this);
        super.onCreate(savedInstanceState);
        realm = RealmInfo.getRealm(getApplicationContext());
        userId = getIntent().getStringExtra("USER_ID");
        showBackButton();

//        View photoHeader = findViewById(R.id.photoHeader);
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//            /* For devices equal or higher than lollipop set the translation above everything else */
//            photoHeader.setTranslationZ(6);
//            /* Redraw the view to show the translation */
//            photoHeader.invalidate();
//        }

        initLayout();
        loadJobCategory();
        getFriendState();
        loadUserInfo();
    }

    private void initLayout() {
        View vFullnameInclude = findViewById(R.id.layout_full_name);
        ImageView ivFullnameLeftIcon = (ImageView) vFullnameInclude.findViewById(R.id.image_left);
        ivFullnameLeftIcon.setImageResource(R.drawable.ic_full_name);
        TextView tvFullnameTitle = (TextView) vFullnameInclude.findViewById(R.id.text_title);
        tvFullnameTitle.setText(getResources().getString(R.string.fullName));
        tvFullnameTitle.setTextColor(getResources().getColor(R.color.colorPrimary));
        tvFullnameContent = (TextView) vFullnameInclude.findViewById(R.id.text_content);
        ImageView ivFullnameRightIcon = (ImageView) vFullnameInclude.findViewById(R.id.image_right);
        ivFullnameRightIcon.setVisibility(View.GONE);

        View vEmailInclude = findViewById(R.id.layout_email);
        ImageView ivEmailLeftIcon = (ImageView) vEmailInclude.findViewById(R.id.image_left);
        ivEmailLeftIcon.setImageResource(R.drawable.ic_email);
        TextView tvEmailTitle = (TextView) vEmailInclude.findViewById(R.id.text_title);
        tvEmailTitle.setText(getResources().getString(R.string.email));
        tvEmailTitle.setTextColor(getResources().getColor(R.color.colorPrimary));
        tvEmailContent = (TextView) vEmailInclude.findViewById(R.id.text_content);
        ImageView ivEmailRightIcon = (ImageView) vEmailInclude.findViewById(R.id.image_right);
        ivEmailRightIcon.setVisibility(View.GONE);

        View vBirthdayInclude = findViewById(R.id.layout_birthday);
        ImageView ivBirthdayLeftIcon = (ImageView) vBirthdayInclude.findViewById(R.id.image_left);
        ivBirthdayLeftIcon.setImageResource(R.drawable.ic_birthday);
        TextView tvBirthdayTitle = (TextView) vBirthdayInclude.findViewById(R.id.text_title);
        tvBirthdayTitle.setText(getResources().getString(R.string.birthday));
        tvBirthdayContent = (TextView) vBirthdayInclude.findViewById(R.id.text_content);
        tvBirthdayContent.setText("19/11/1991");
        ImageView ivBirthdayRightIcon = (ImageView) vBirthdayInclude.findViewById(R.id.image_right);
        ivBirthdayRightIcon.setVisibility(View.GONE);

        View vGenderInclude = findViewById(R.id.layout_gender);
        ImageView ivGenderLeftIcon = (ImageView) vGenderInclude.findViewById(R.id.image_left);
        ivGenderLeftIcon.setImageResource(R.drawable.ic_gender);
        TextView tvGenderTitle = (TextView) vGenderInclude.findViewById(R.id.text_title);
        tvGenderTitle.setText(getResources().getString(R.string.gender));
        tvGenderContent = (TextView) vGenderInclude.findViewById(R.id.text_content);
        tvGenderContent.setText(getResources().getString(R.string.common_gender_male));
        ivGenderRightIcon = (ImageView) vGenderInclude.findViewById(R.id.image_right);
        ivGenderRightIcon.setVisibility(View.GONE);

        View vPhoneNumberInclude = findViewById(R.id.layout_phone_number);
        ImageView ivPhoneNumberLeftIcon = (ImageView) vPhoneNumberInclude.findViewById(R.id.image_left);
        ivPhoneNumberLeftIcon.setImageResource(R.drawable.ic_phone_number);
        TextView tvPhoneNumberTitle = (TextView) vPhoneNumberInclude.findViewById(R.id.text_title);
        tvPhoneNumberTitle.setText(getResources().getString(R.string.phone_number));
        tvPhoneNumberContent = (TextView) vPhoneNumberInclude.findViewById(R.id.text_content);
        ImageView ivPhoneNumberRightIcon = (ImageView) vPhoneNumberInclude.findViewById(R.id.image_right);
        ivPhoneNumberRightIcon.setVisibility(View.GONE);

        View vLocationLocationInclude = findViewById(R.id.layout_location_location);
        ImageView ivLocationLocationLeftIcon = (ImageView) vLocationLocationInclude.findViewById(R.id.image_left);
        ivLocationLocationLeftIcon.setImageResource(R.drawable.ic_address);
        TextView tvLocationLocationTitle = (TextView) vLocationLocationInclude.findViewById(R.id.text_title);
        tvLocationLocationTitle.setText(getResources().getString(R.string.location_location));
        TextView edtLocationLocationContent = (TextView) vLocationLocationInclude.findViewById(R.id.text_content);
        edtLocationLocationContent.setVisibility(View.GONE);
        ImageView ivLocationLocationRightIcon = (ImageView) vLocationLocationInclude.findViewById(R.id.image_right);
        ivLocationLocationRightIcon.setVisibility(View.GONE);

        View vAddressInclude = findViewById(R.id.layout_address);
        ImageView ivAddressLeftIcon = (ImageView) vAddressInclude.findViewById(R.id.image_left);
        ivAddressLeftIcon.setImageResource(R.drawable.ic_company);
        TextView tvAddressTitle = (TextView) vAddressInclude.findViewById(R.id.text_title);
        tvAddressTitle.setText(getResources().getString(R.string.address));
        tvAddressContent = (TextView) vAddressInclude.findViewById(R.id.text_content);
        ImageView ivAddressRightIcon = (ImageView) vAddressInclude.findViewById(R.id.image_right);
        ivAddressRightIcon.setVisibility(View.GONE);

        View vNationInclude = findViewById(R.id.layout_nation);
        ImageView ivNationLeftIcon = (ImageView) vNationInclude.findViewById(R.id.image_left);
        ivNationLeftIcon.setImageResource(R.drawable.ic_nation);
        TextView tvNationTitle = (TextView) vNationInclude.findViewById(R.id.text_title);
        tvNationTitle.setText(getResources().getString(R.string.nation));
        tvNationContent = (TextView) vNationInclude.findViewById(R.id.text_content);
        tvNationContent.setText("Vietnam");
        ImageView ivNationRightIcon = (ImageView) vNationInclude.findViewById(R.id.image_right);
        ivNationRightIcon.setVisibility(View.GONE);

        View vJobCategoryInclude = findViewById(R.id.layout_job_category);
        ImageView ivJobCategoryLeftIcon = (ImageView) vJobCategoryInclude.findViewById(R.id.image_left);
        ivJobCategoryLeftIcon.setImageResource(R.drawable.ic_job_category);
        TextView tvJobCategoryTitle = (TextView) vJobCategoryInclude.findViewById(R.id.text_title);
        tvJobCategoryTitle.setText(getResources().getString(R.string.job_category));
        tvJobCategoryContent = (TextView) vJobCategoryInclude.findViewById(R.id.text_content);
        ImageView ivJobCategoryRightIcon = (ImageView) vJobCategoryInclude.findViewById(R.id.image_right);
        ivJobCategoryRightIcon.setVisibility(View.GONE);

        View vJobInclude = findViewById(R.id.layout_job);
        ImageView ivJobLeftIcon = (ImageView) vJobInclude.findViewById(R.id.image_left);
        ivJobLeftIcon.setImageResource(R.drawable.ic_job);
        TextView tvJobTitle = (TextView) vJobInclude.findViewById(R.id.text_title);
        tvJobTitle.setText(getResources().getString(R.string.job));
        tvJobContent = (TextView) vJobInclude.findViewById(R.id.text_content);
        ImageView ivJobRightIcon = (ImageView) vJobInclude.findViewById(R.id.image_right);
        ivJobRightIcon.setVisibility(View.GONE);

        View vIntroductionInclude = findViewById(R.id.layout_introduction);
        ImageView ivIntroductionLeftIcon = (ImageView) vIntroductionInclude.findViewById(R.id.image_left);
        ivIntroductionLeftIcon.setImageResource(R.drawable.ic_introduction);
        TextView tvIntroductionTitle = (TextView) vIntroductionInclude.findViewById(R.id.text_title);
        tvIntroductionTitle.setText(getResources().getString(R.string.introduction));
        tvIntroductionTitle.setVisibility(View.GONE);
        tvIntroductionContent = (TextView) vIntroductionInclude.findViewById(R.id.text_content);
        tvIntroductionContent.setGravity(Gravity.LEFT);
        ImageView ivIntroductionRightIcon = (ImageView) vIntroductionInclude.findViewById(R.id.image_right);
        ivIntroductionRightIcon.setVisibility(View.GONE);

        civUserAvatar = (CircleImageView) findViewById(R.id.civ_user_avatar);
    }

    private void loadJobCategory() {
        BaseReturnFunctionEntity<ArrayList<TypeJobEntity>> myList = new TypeJobInfo().getListTypeJob(getApplicationContext(), realm);
        listTypeJobID = new ArrayList<>();
        listTypeJobName = new ArrayList<>();
        if (myList.isTrue()) {
            for (TypeJobEntity type : myList.getData()) {
                listTypeJobID.add(type.getId());
                listTypeJobName.add(type.getName());
            }
        }
    }

    private String getJobCategory(int categoryId) {
        if (listTypeJobID == null || listTypeJobID.isEmpty() || listTypeJobName == null || listTypeJobName.isEmpty())
            return "";

        for (int i = 0; i < listTypeJobID.size(); i++)
            if (listTypeJobID.get(i) == categoryId)
                return listTypeJobName.get(i);

        return "";
    }

    private void getFriendState() {
        new AsyncTask<Void, Void, String>() {

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
            }

            @Override
            protected String doInBackground(Void... voids) {
                try {
//                    isFriend = UserInfo.isFriend(userId);
                    friendStatus = FriendInfo.getFriendStatus(userId);
                    return friendStatus;
                } catch (Exception e) {
                    Log.d(TAG, "Exception: " + e.getMessage().toString());
                    return null;
                }
            }

            @Override
            protected void onPostExecute(String friendStatus) {
                super.onPostExecute(friendStatus);
            }
        }.execute();
    }

    private void loadUserInfo() {
        new AsyncTask<Void, Void, JsonObject>() {
            ProgressDialog progDialog = new ProgressDialog(UserProfileActivity.this);

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                progDialog.setMessage(getString(R.string.loading));
                progDialog.setIndeterminate(false);
                progDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                progDialog.setCancelable(true);
                progDialog.show();
            }

            @Override
            protected JsonObject doInBackground(Void... voids) {
                //call api load user
                JsonObject userInfo = UserInfo.getAccountInfo(userId);
                return userInfo;
            }

            @Override
            protected void onPostExecute(JsonObject userInfo) {
                if (userInfo != null) {
                    tvFullnameContent.setText(ConvertUtils.toString(userInfo.get("FullName")));
                    String birthday = ConvertUtils.toDateString(ConvertUtils.toLong(userInfo.get("Birthday")), "dd/mm/yyyy");
                    tvBirthdayContent.setText(birthday);

                    String email = ConvertUtils.toString(userInfo.get("UserName")).toLowerCase();
                    if (email.contains(UserInfo.SocialType.fb.toString())) {
                        tvEmailContent.setText(getResources().getString(R.string.hidden_content));
                        tvEmailContent.setTextColor(getResources().getColor(R.color.grey_600));
                        tvBirthdayContent.setText(getResources().getString(R.string.hidden_content));
                        tvBirthdayContent.setTextColor(getResources().getColor(R.color.grey_600));
                    } else if (email.contains(UserInfo.SocialType.gg.toString())) {
                        tvEmailContent.setText(email.substring(2));//cut prefix: gg
                        tvBirthdayContent.setText(getResources().getString(R.string.hidden_content));
                        tvBirthdayContent.setTextColor(getResources().getColor(R.color.grey_600));
                    }else
                        tvEmailContent.setText(email);

                    if (ConvertUtils.toInt(userInfo.get("Gender")) == 1)
                        tvGenderContent.setText(getString(R.string.common_gender_male));
                    else
                        tvGenderContent.setText(getString(R.string.common_gender_female));

                    tvAddressContent.setText(ConvertUtils.toString(userInfo.get("CurrentLivingCity")));
                    tvNationContent.setText(ConvertUtils.toString(userInfo.get("Nationality")));
                    tvJobContent.setText(ConvertUtils.toString(userInfo.get("Job")));
                    tvJobCategoryContent.setText(getJobCategory(ConvertUtils.toInt(userInfo.get("TypeJob"))));

                    if (ConvertUtils.toBoolean(userInfo.get("ShowPhoneNumber")))
                        tvPhoneNumberContent.setText(ConvertUtils.toString(userInfo.get("PhoneNumber")));
                    else
                        tvPhoneNumberContent.setText(getResources().getString(R.string.hidden_content));

                    tvIntroductionContent.setText(ConvertUtils.toString(userInfo.get("Introduction")));
                    updateFriendButton();

//        Glide.with(this).load(ConvertUtils.toString(currentUser.get("Avartar"))).into(civUserProfileDetail);
                    if (ConvertUtils.toBoolean(userInfo.get("ShowAvatar")))
                        ImageUtils.loadImageByGlide(getApplicationContext(), true, 140, 140, ConvertUtils.toString(userInfo.get("Avartar")), R.drawable.default_avatar, R.drawable.default_avatar, civUserAvatar, true);

                    //load location on map
                    String latAddress = ConvertUtils.toString(userInfo.get("LatAddress"));
                    String lonAddress = ConvertUtils.toString(userInfo.get("LonAddress"));
                    if (latAddress == null || latAddress.isEmpty() || lonAddress == null || lonAddress.isEmpty()) {
                        ImageUtils.loadImageByGlide(getActivity(), false, 0, 150, "", R.drawable.bg_map_vn, R.drawable.bg_map_vn, imgLocation, true);
                    } else {
                        String urlLocationImage = Constants.MAP_URL_HEADER + latAddress + "," + lonAddress + Constants.MAP_URL_FOOTER;
                        ImageUtils.loadImageByGlide(getActivity(), false, 0, 150, urlLocationImage, R.drawable.bg_map_vn, R.drawable.bg_map_vn, imgLocation, true);
                    }

                }

                super.onPostExecute(userInfo);
                if (progDialog.isShowing()) {
                    progDialog.dismiss();
                }
            }
        }.execute();
    }

    @OnClick(R.id.btn_send_message)
    void sendMessage() {
        if (!AppUtils.isNetworkAvailable(this)) {
            showSnackBar(this, R.string.please_check_internet_connection);
            return;
        }

        if (!UserInfo.isLogin()) {
            showSnackBar(getActivity(), R.string.please_login_to_chat_with_this_user);
            return;
        }

        if (!android.text.TextUtils.isEmpty(friendStatus) && !friendStatus.equals(Constants.STATUS_FRIEND)) {
            showSnackBar(getActivity(), R.string.add_friend_before_chat);
            return;
        }

        ArrayList<String> mSelectedIds = new ArrayList<>();
        mSelectedIds.add(userId);
        mSelectedIds.add(UserInfo.getCurrentUserId());
        GroupChannelActivity.open(getActivity(),userId);
    }

    private void updateFriendButton() {
        if (friendStatus.equals(Constants.STATUS_FRIEND)) {
            btnAddFriend.setText(getResources().getString(R.string.un_friend));
        } else if (friendStatus.equals(Constants.STATUS_SENT_REQUEST)) {
            btnAddFriend.setText(getResources().getString(R.string.cancel_request));
        } else if (friendStatus.equals(Constants.STATUS_RECEIVED_REQUEST)) {
            btnAddFriend.setText(getResources().getString(R.string.confirm_request));
        } else if (friendStatus.equals(Constants.STATUS_NOT_FRIEND)) {
            btnAddFriend.setText(getResources().getString(R.string.add_friend));
        }
    }

    @OnClick(R.id.btn_add_friend)
    void addFriend() {
        if (!AppUtils.isNetworkAvailable(this)) {
            showSnackBar(this, R.string.please_check_internet_connection);
            return;
        }

        if (!UserInfo.isLogin()) {
            showSnackBar(getActivity(), R.string.please_login_to_add_friend);
            return;
        }

        if (!TextUtils.isEmpty(friendStatus) && friendStatus.equals(Constants.STATUS_FRIEND)) {
            //friend -click-> do: unfriend | next action: add friend
            new AsyncTask<Void, Void, BaseReturnFunctionEntity<Boolean>>() {

                @Override
                protected void onPreExecute() {
                    super.onPreExecute();
                }

                @Override
                protected BaseReturnFunctionEntity<Boolean> doInBackground(Void... voids) {
                    return FriendInfo.unFriend(userId);
                }

                @Override
                protected void onPostExecute(BaseReturnFunctionEntity<Boolean> result) {
                    super.onPostExecute(result);
                    if (result.isTrue()) {
                        friendStatus = Constants.STATUS_NOT_FRIEND;
                        btnAddFriend.setText(getResources().getString(R.string.add_friend));
                        showSnackBar(R.string.activity_un_friend_successfully);
                    } else {
                        showSnackBar(R.string.activity_un_friend_failed);
                    }
                }
            }.execute();
        } else if (friendStatus.equals(Constants.STATUS_SENT_REQUEST)) {
            //sent request -click-> do: cancel request | next action: add friend
            new AsyncTask<Void, Void, BaseReturnFunctionEntity<Boolean>>() {

                @Override
                protected void onPreExecute() {
                    super.onPreExecute();
                }

                @Override
                protected BaseReturnFunctionEntity<Boolean> doInBackground(Void... voids) {
                    return FriendInfo.cancelFriendRequest(userId);
                }

                @Override
                protected void onPostExecute(BaseReturnFunctionEntity<Boolean> result) {
                    super.onPostExecute(result);
                    if (result.isTrue()) {
                        friendStatus = Constants.STATUS_NOT_FRIEND;
                        btnAddFriend.setText(getResources().getString(R.string.add_friend));
                        showSnackBar(R.string.activity_cancel_friend_request_successfully);
                    } else {
                        showSnackBar(R.string.activity_cancel_friend_request_failed);
                    }
                }
            }.execute();
        } else if (friendStatus.equals(Constants.STATUS_RECEIVED_REQUEST)) {
            //recieved request -click-> do: confirm request | next action: unfriend
            new AsyncTask<Void, Void, BaseReturnFunctionEntity<Boolean>>() {

                @Override
                protected void onPreExecute() {
                    super.onPreExecute();
                }

                @Override
                protected BaseReturnFunctionEntity<Boolean> doInBackground(Void... voids) {
                    return FriendInfo.acceptFriendRequest(userId);
                }

                @Override
                protected void onPostExecute(BaseReturnFunctionEntity<Boolean> result) {
                    super.onPostExecute(result);
                    if (result.isTrue()) {
                        friendStatus = Constants.STATUS_FRIEND;
                        btnAddFriend.setText(getResources().getString(R.string.un_friend));
                        showSnackBar(R.string.activity_add_friend_successfully);
                    } else {
                        showSnackBar(R.string.activity_add_friend_failed);
                    }
                }
            }.execute();
        } else if (friendStatus.equals(Constants.STATUS_NOT_FRIEND)) {
            //not friend -click-> do: sent request | next action: cancel request
            new AsyncTask<Void, Void, BaseReturnFunctionEntity<Boolean>>() {

                @Override
                protected void onPreExecute() {
                    super.onPreExecute();
                }

                @Override
                protected BaseReturnFunctionEntity<Boolean> doInBackground(Void... voids) {
                    return FriendInfo.sendFriendRequest(userId);
                }

                @Override
                protected void onPostExecute(BaseReturnFunctionEntity<Boolean> result) {
                    super.onPostExecute(result);
                    if (result.isTrue()) {
                        friendStatus = Constants.STATUS_SENT_REQUEST;
                        btnAddFriend.setText(getResources().getString(R.string.cancel_request));
                        showSnackBar(R.string.activity_send_friend_request_successfully);
                    } else {
                        showSnackBar(R.string.activity_send_friend_request_failed);
                    }
                }
            }.execute();
        }

    }
}
