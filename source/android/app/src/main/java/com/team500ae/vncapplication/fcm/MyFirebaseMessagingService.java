package com.team500ae.vncapplication.fcm;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.team500ae.library.utils.ConvertUtils;
import com.team500ae.vncapplication.R;
import com.team500ae.vncapplication.activities.MainActivity;
import com.team500ae.vncapplication.activities.chat.GroupChannelActivity;
import com.team500ae.vncapplication.activities.chat.utils.PreferenceUtils;
import com.team500ae.vncapplication.activities.chatcontact.ChatContactActivity;
import com.team500ae.vncapplication.activities.job.JobDetailActivity;
import com.team500ae.vncapplication.activities.news.NewsDetailActivity;
import com.team500ae.vncapplication.activities.tips.TipsDetailActivity;
import com.team500ae.vncapplication.data_access.user.UserInfo;
import com.team500ae.vncapplication.utils.FlagAttributeUtils;
import com.team500ae.vncapplication.utils.StringUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Random;

public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private static final String TAG = "MyFirebaseMsgService";

    /**
     * Called when message is received.
     *
     * @param remoteMessage Object representing the message received from Firebase Cloud Messaging.
     */
    // [START receive_message]
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        // [START_EXCLUDE]
        // There are two types of messages data messages and notification messages. Data messages are handled
        // here in onMessageReceived whether the app is in the foreground or background. Data messages are the type
        // traditionally used with GCM. Notification messages are only received here in onMessageReceived when the app
        // is in the foreground. When the app is in the background an automatically generated notification is displayed.
        // When the user taps on the notification they are returned to the app. Messages containing both notification
        // and data payloads are treated as notification messages. The Firebase console always sends notification
        // messages. For more see: https://firebase.google.com/docs/cloud-messaging/concept-options
        // [END_EXCLUDE]

        // TODO(developer): Handle FCM messages here.
        // Not getting messages here? See why this may be: https://goo.gl/39bRNJ
        Log.d(TAG, "From: " + remoteMessage.getFrom());

        // Check if message contains a data payload.
        if (remoteMessage.getData().size() > 0) {
            Log.d(TAG, "Message data payload: " + remoteMessage.getData());
        }

        // Check if message contains a notification payload.
        if (remoteMessage.getNotification() != null) {
            Log.d(TAG, "Message Notification Body: " + remoteMessage.getNotification().getBody());
        }

        String channelUrl = null;
        try {
            String _sendbird = remoteMessage.getData().get("sendbird");
            if (_sendbird != null) {
                JSONObject sendBird = new JSONObject(remoteMessage.getData().get("sendbird"));
                JSONObject channel = (JSONObject) sendBird.get("channel");
                channelUrl = (String) channel.get("channel_url");
                sendNotification(this, remoteMessage.getData().get("message"), channelUrl);
            } else {
                JsonArray data = ConvertUtils.toJsonArray(remoteMessage.getData().get("data"));

                sendNotification(this, remoteMessage, data);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void sendNotification(Context context, RemoteMessage remoteMessage, JsonArray data) {
        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        String CHANNEL_ID = "iDuhoc_Channel";// The id of the channel.
        CharSequence name = getString(R.string.channel_name);// The user-visible name of the channel.
        int notificationId =new Random(1000).nextInt() /* ID of notification */;
        int importance = NotificationManager.IMPORTANCE_HIGH;
        NotificationChannel mChannel = null;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            mChannel = new NotificationChannel(CHANNEL_ID, name, importance);
        }

        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(context)
                .setSmallIcon(R.drawable.ic_app_logo)
                .setLargeIcon(BitmapFactory.decodeResource(context.getResources(), R.drawable.ic_app_logo))
                .setContentTitle("Thông báo")
                .setContentText("")
                .setAutoCancel(true)
                .setChannelId(CHANNEL_ID)
                .setSound(defaultSoundUri)
                .setPriority(Notification.PRIORITY_MAX)
                .setDefaults(Notification.DEFAULT_ALL);

        if(data.size()==4)
        {
            notificationBuilder.setContentTitle(ConvertUtils.toString(ConvertUtils.toJsonObject(data.get(2)).get("Value")));
            notificationBuilder.setContentText(ConvertUtils.toString(ConvertUtils.toJsonObject(data.get(3)).get("Value")));

            int type = ConvertUtils.toInt(ConvertUtils.toJsonObject(data.get(0)).get("Value"));
            if(type==1)
            {
                notificationBuilder.setContentTitle(context.getResources().getString(R.string.friend_request));
            }else
            {
                if(type==2)
                {
                    notificationBuilder.setContentTitle(context.getResources().getString(R.string.new_news));
                }else
                {
                    if(type==3)
                    {
                        notificationBuilder.setContentTitle(context.getResources().getString(R.string.new_job));
                    }else
                    {
                        if(type==4)
                        {
                            notificationBuilder.setContentTitle(context.getResources().getString(R.string.new_tips));
                        }else
                        {
                            notificationBuilder.setContentTitle(context.getResources().getString(R.string.new_job_approved));
                        }
                    }
                }
            }
        }
        if(!setPendingIntent(notificationBuilder, data))
            return;

        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            notificationManager.createNotificationChannel(mChannel);
        }

        notificationManager.notify(notificationId, notificationBuilder.build());
    }

    private boolean setPendingIntent(NotificationCompat.Builder notificationBuilder, JsonArray data) {
        Log.d(TAG, "setPendingIntent: ");
        JsonObject jsonObject = new JsonObject();
        if (data.size() == 4) {
            jsonObject = ConvertUtils.toJsonObject(data.get(0));
        }
        int type = ConvertUtils.toInt(jsonObject.get("Value"));
        Intent intent = null;
        int settingValue;
        switch (type) {
            case 0:
                break;
            case 1:
                if (!UserInfo.isLogin())
                    return false;
                settingValue = ConvertUtils.toInt(UserInfo.getCurrentUser().get("SettingAttribute"));
                if (!FlagAttributeUtils.getAttribute(settingValue, FlagAttributeUtils.SHOW_FRIEND_NOTIFICATION))
                    return false;
                intent = new Intent(getApplicationContext(), ChatContactActivity.class);
                intent.putExtra("TAB", 2);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                break;
            case 2:
                if (UserInfo.isLogin()) {
                    settingValue = ConvertUtils.toInt(UserInfo.getCurrentUser().get("SettingAttribute"));
                    if (!FlagAttributeUtils.getAttribute(settingValue, FlagAttributeUtils.SHOW_NEWS_NOTIFICATION))
                        return false;
                }

                intent = new Intent(getApplicationContext(), NewsDetailActivity.class);
                intent.putExtra("NEWS_ID", ConvertUtils.toInt(ConvertUtils.toJsonObject(data.get(1)).get("Value")));
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                break;
            case 3:
                if (UserInfo.isLogin()) {
                    settingValue = ConvertUtils.toInt(UserInfo.getCurrentUser().get("SettingAttribute"));
                    if (!FlagAttributeUtils.getAttribute(settingValue, FlagAttributeUtils.SHOW_JOB_NOTIFICATION))
                        return false;
                }
                intent = new Intent(getApplicationContext(), JobDetailActivity.class);
                intent.putExtra("ID", ConvertUtils.toInt(ConvertUtils.toJsonObject(data.get(1)).get("Value")));
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                break;
            case 4:
                if (UserInfo.isLogin()) {
                    settingValue = ConvertUtils.toInt(UserInfo.getCurrentUser().get("SettingAttribute"));
                    if (!FlagAttributeUtils.getAttribute(settingValue, FlagAttributeUtils.SHOW_TIPS_NOTIFICATION))
                        return false;
                }
                intent = new Intent(getApplicationContext(), TipsDetailActivity.class);
                intent.putExtra("TIPS_ID", ConvertUtils.toInt(ConvertUtils.toJsonObject(data.get(1)).get("Value")));
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                break;
            case 5:
                if (UserInfo.isLogin()) {
                    intent = new Intent(getApplicationContext(), MainActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                }
                break;
        }

        if (intent != null) {
            PendingIntent pendingIntent = PendingIntent.getActivity(getApplicationContext(), 0 /* Request code */, intent,
                    PendingIntent.FLAG_UPDATE_CURRENT);

            notificationBuilder.setContentIntent(pendingIntent);

//        if (PreferenceUtils.getNotificationsShowPreviews(context)) {
//            notificationBuilder.setContentText(messageBody);
//        } else {
//            notificationBuilder.setContentText("Somebody sent you a message.");
//        }
        }
        return true;

    }

    public static void sendNotification(Context context, String messageBody, String channelUrl) {
        Intent intent = new Intent(context, GroupChannelActivity.class);
        intent.putExtra("groupChannelUrl", channelUrl);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

        PendingIntent pendingIntent = PendingIntent.getActivity(context, 0 /* Request code */, intent,
                PendingIntent.FLAG_UPDATE_CURRENT);

        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(context)
                .setSmallIcon(R.drawable.ic_launcher)
                .setContentTitle(context.getResources().getString(R.string.app_name))
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setPriority(Notification.PRIORITY_MAX)
                .setDefaults(Notification.DEFAULT_ALL)
                .setContentIntent(pendingIntent);

        if (PreferenceUtils.getNotificationsShowPreviews(context)) {
            notificationBuilder.setContentText(messageBody);
        } else {
            notificationBuilder.setContentText("Somebody sent you a message.");
        }

        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify(0 /* ID of notification */, notificationBuilder.build());
    }
}