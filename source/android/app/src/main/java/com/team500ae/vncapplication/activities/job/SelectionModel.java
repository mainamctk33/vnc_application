package com.team500ae.vncapplication.activities.job;

/**
 * Created by thanh.tran on 5/30/2018.
 */
public class SelectionModel {
    public int id;
    private String value;
    public int salaryForm;
    public int salaryTo;

    public SelectionModel(int id, String value) {
        this.id = id;
        this.value = value;
    }

    public SelectionModel(int id, String value, int salaryForm, int salaryTo) {
        this.id = id;
        this.value = value;
        this.salaryForm = salaryForm;
        this.salaryTo = salaryTo;
    }

    public String getValue() {
        return value;
    }

}
