package com.team500ae.vncapplication.models.realm_models.translator;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.Required;

/**
 * Created by TamPC on 3/24/2018.
 */

public class TypeTranslatorEntity extends RealmObject {
    @PrimaryKey
    int id;

    @Required
    String name;


    int category;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getCategory() {
        return category;
    }

    public void setCategory(int category) {
        this.category = category;
    }
}
