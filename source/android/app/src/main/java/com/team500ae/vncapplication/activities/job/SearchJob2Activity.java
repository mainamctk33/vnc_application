package com.team500ae.vncapplication.activities.job;

import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AppCompatActivity;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.google.gson.JsonArray;
import com.team500ae.vncapplication.R;
import com.team500ae.vncapplication.activities.job.fragments.JobTabFragment;
import com.team500ae.vncapplication.activities.job.interfaces.DialogSelectionResultListener;
import com.team500ae.vncapplication.activities.job.selectiondialog.SelectionFragmentDialog;
import com.team500ae.vncapplication.data_access.RealmInfo;
import com.team500ae.vncapplication.data_access.job.CityJobInfo;
import com.team500ae.vncapplication.data_access.job.CountryJobInfo;
import com.team500ae.vncapplication.data_access.job.JobInfo;
import com.team500ae.vncapplication.data_access.job.LevelJobInfo;
import com.team500ae.vncapplication.data_access.job.SalaryJobInfo;
import com.team500ae.vncapplication.data_access.job.TypeJobInfo;
import com.team500ae.vncapplication.models.BaseReturnFunctionEntity;
import com.team500ae.vncapplication.models.ServiceResponseEntity;
import com.team500ae.vncapplication.models.realm_models.job.CityJobEntity;
import com.team500ae.vncapplication.models.realm_models.job.CountryJobEntity;
import com.team500ae.vncapplication.models.realm_models.job.LevelJobEntity;
import com.team500ae.vncapplication.models.realm_models.job.SalaryJobEntity;
import com.team500ae.vncapplication.models.realm_models.job.TypeJobEntity;
import com.team500ae.vncapplication.utils.KeyboardUtils;

import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.realm.Realm;

/**
 * Created by Phuong D. Nguyen on 6/12/2018.
 */

public class SearchJob2Activity extends AppCompatActivity implements JobTabFragment.LoadListener {

    @BindView(R.id.layout_key_word)
    View vKeyWordLayout;
    @BindView(R.id.layout_nation)
    View vNationLayout;
    @BindView(R.id.layout_city)
    View vCityLayout;
    @BindView(R.id.layout_job_category)
    View vJobCategoryLayout;
    @BindView(R.id.layout_job_position)
    View vJobPositionLayout;
    @BindView(R.id.layout_salary)
    View vSalaryLayout;
    @BindView(R.id.layout_company)
    View vCompanyLayout;
    @BindView(R.id.layout_sorted_by)
    View vSortedByLayout;

    @BindView(R.id.layout_search_main)
    ScrollView layout_search_main;
    @BindView(R.id.layoutFragment)
    LinearLayout layoutFragment;
    @BindView(R.id.searchJob_layout)
    RelativeLayout searchJobLayout;

    private EditText edtKeyWordContent, edtCompanyContent;
    private TextView tvNationContent, tvCityContent, tvJobCategoryContent, tvJobPositionContent, tvSalaryContent, tvSortedByContent;
    private int idCity = -1, idCountry = -1, idJobCategory = -1, idPosition = -1, idSortBy = -1, idSalaryForm = 0, idSalaryTo = 999999999;

    Realm realm;
    List<SelectionModel> mCountryModel = new ArrayList<>();
    List<SelectionModel> mCityModel = new ArrayList<>();
    List<SelectionModel> mJobCategoryModel = new ArrayList<>();
    List<SelectionModel> mPositionModel = new ArrayList<>();
    List<SelectionModel> mSalaryModel = new ArrayList<>();
    List<SelectionModel> mSortByModel = new ArrayList<>();

    public static void open(FragmentActivity activity) {
        Intent intent = new Intent(activity, SearchJob2Activity.class);
        activity.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setContentView(R.layout.activity_search_job2);
        ButterKnife.bind(this);
        KeyboardUtils.setupUI(searchJobLayout, this);
        realm = RealmInfo.getRealm(getApplicationContext());
        getSupportActionBar().setTitle(R.string.search_job);
        initLayout();
        showBackButton();
        showData();
        super.onCreate(savedInstanceState);
    }


    private void initLayout() {
        ImageView ivKeyWordLeftIcon = (ImageView) vKeyWordLayout.findViewById(R.id.image_left);
        ivKeyWordLeftIcon.setImageResource(R.drawable.ic_job_title);
        TextView tvKeyWordTitle = (TextView) vKeyWordLayout.findViewById(R.id.text_title);
        tvKeyWordTitle.setVisibility(View.GONE);
        edtKeyWordContent = (EditText) vKeyWordLayout.findViewById(R.id.text_content);
        edtKeyWordContent.setGravity(Gravity.LEFT);
//        edtKeyWordContent.setFilters(new InputFilter[]{new InputFilter.LengthFilter(Constants.MAX_FULL_NAME_LENGTH)});
        ImageView ivKeyWordRightIcon = (ImageView) vKeyWordLayout.findViewById(R.id.image_right);
        ivKeyWordRightIcon.setVisibility(View.GONE);

        ImageView ivNationLeftIcon = (ImageView) vNationLayout.findViewById(R.id.image_left);
        ivNationLeftIcon.setImageResource(R.drawable.ic_nation);
        TextView tvNationTitle = (TextView) vNationLayout.findViewById(R.id.text_title);
        tvNationTitle.setText(getResources().getString(R.string.nation));
        tvNationContent = (TextView) vNationLayout.findViewById(R.id.text_content);
        ImageView ivNationRightIcon = (ImageView) vNationLayout.findViewById(R.id.image_right);
        ivNationRightIcon.setImageResource(R.drawable.ic_next);

        ImageView ivCityLeftIcon = (ImageView) vCityLayout.findViewById(R.id.image_left);
        ivCityLeftIcon.setImageResource(R.drawable.ic_address);
        TextView tvCityTitle = (TextView) vCityLayout.findViewById(R.id.text_title);
        tvCityTitle.setText(getResources().getString(R.string.city));
        tvCityContent = (TextView) vCityLayout.findViewById(R.id.text_content);
        ImageView ivCityRightIcon = (ImageView) vCityLayout.findViewById(R.id.image_right);
        ivCityRightIcon.setImageResource(R.drawable.ic_next);

        ImageView ivJobCategoryLeftIcon = (ImageView) vJobCategoryLayout.findViewById(R.id.image_left);
        ivJobCategoryLeftIcon.setImageResource(R.drawable.ic_job_category);
        TextView tvJobCategoryTitle = (TextView) vJobCategoryLayout.findViewById(R.id.text_title);
        tvJobCategoryTitle.setText(getResources().getString(R.string.job_category));
        tvJobCategoryContent = (TextView) vJobCategoryLayout.findViewById(R.id.text_content);
        ImageView ivJobCategoryRightIcon = (ImageView) vJobCategoryLayout.findViewById(R.id.image_right);
        ivJobCategoryRightIcon.setImageResource(R.drawable.ic_next);

        ImageView ivJobPositionLeftIcon = (ImageView) vJobPositionLayout.findViewById(R.id.image_left);
        ivJobPositionLeftIcon.setImageResource(R.drawable.ic_job);
        TextView tvJobPositionTitle = (TextView) vJobPositionLayout.findViewById(R.id.text_title);
        tvJobPositionTitle.setText(getResources().getString(R.string.job_position));
        tvJobPositionContent = (TextView) vJobPositionLayout.findViewById(R.id.text_content);
        ImageView ivJobPositionRightIcon = (ImageView) vJobPositionLayout.findViewById(R.id.image_right);
        ivJobPositionRightIcon.setImageResource(R.drawable.ic_next);

        ImageView ivSalaryLeftIcon = (ImageView) vSalaryLayout.findViewById(R.id.image_left);
        ivSalaryLeftIcon.setImageResource(R.drawable.ic_salary);
        TextView tvSalaryTitle = (TextView) vSalaryLayout.findViewById(R.id.text_title);
        tvSalaryTitle.setText(getResources().getString(R.string.salary));
        tvSalaryContent = (TextView) vSalaryLayout.findViewById(R.id.text_content);
        ImageView ivSalaryRightIcon = (ImageView) vSalaryLayout.findViewById(R.id.image_right);
        ivSalaryRightIcon.setImageResource(R.drawable.ic_next);

        ImageView ivCompanyLeftIcon = (ImageView) vCompanyLayout.findViewById(R.id.image_left);
        ivCompanyLeftIcon.setImageResource(R.drawable.ic_company);
        TextView tvCompanyTitle = (TextView) vCompanyLayout.findViewById(R.id.text_title);
        tvCompanyTitle.setText(getResources().getString(R.string.company));
        edtCompanyContent = (EditText) vCompanyLayout.findViewById(R.id.text_content);
        ImageView ivCompanyRightIcon = (ImageView) vCompanyLayout.findViewById(R.id.image_right);
        ivCompanyRightIcon.setVisibility(View.GONE);

        ImageView ivSortedByLeftIcon = (ImageView) vSortedByLayout.findViewById(R.id.image_left);
        ivSortedByLeftIcon.setImageResource(R.drawable.ic_gender);
        TextView tvSortedByTitle = (TextView) vSortedByLayout.findViewById(R.id.text_title);
        tvSortedByTitle.setText(getResources().getString(R.string.job_search_sort_by));
        tvSortedByContent = (TextView) vSortedByLayout.findViewById(R.id.text_content);
        ImageView ivSortedByRightIcon = (ImageView) vSortedByLayout.findViewById(R.id.image_right);
        ivSortedByRightIcon.setImageResource(R.drawable.ic_next);
    }

    public void showBackButton() {
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }

    private void showData() {
        //country
        BaseReturnFunctionEntity<ArrayList<CountryJobEntity>> myListCountry = new CountryJobInfo().getList(getApplicationContext(), realm);
        if (myListCountry.isTrue()) {
            for (CountryJobEntity type : myListCountry.getData()) {
                mCountryModel.add(new SelectionModel(type.getId(), type.getName()));
            }
        }
        vNationLayout.setOnClickListener(v -> showSelectionDialog(mCountryModel, (code, model) -> {
            tvNationContent.setText(model.getValue());
            idCountry = model.id;
        }));
        //city
        BaseReturnFunctionEntity<ArrayList<CityJobEntity>> myListCity = new CityJobInfo().getList(getApplicationContext(), realm);
        if (myListCity.isTrue()) {

            for (CityJobEntity type : myListCity.getData()) {
                mCityModel.add(new SelectionModel(type.getId(), type.getName()));
            }
        }
        vCityLayout.setOnClickListener(v -> showSelectionDialog(mCityModel, (code, model) -> {
            tvCityContent.setText(model.getValue());
            idCity = model.id;
        }));
        //Salary
        BaseReturnFunctionEntity<ArrayList<SalaryJobEntity>> myListSalary = new SalaryJobInfo().getList(getApplicationContext(), realm);
        if (myListSalary.isTrue()) {

            for (SalaryJobEntity type : myListSalary.getData()) {
                mSalaryModel.add(new SelectionModel(type.getId(), type.getName(), type.getFrom(), type.getTo()));
            }
        }
        vSalaryLayout.setOnClickListener(v -> showSelectionDialog(mSalaryModel, (code, model) -> {
            tvSalaryContent.setText(model.getValue());
            idSalaryForm = model.salaryForm;
            idSalaryTo = model.salaryTo;
        }));
        //job category
        BaseReturnFunctionEntity<ArrayList<TypeJobEntity>> myListJobCategory = new TypeJobInfo().getListTypeJob(getApplicationContext(), realm);
        if (myListJobCategory.isTrue()) {

            for (TypeJobEntity type : myListJobCategory.getData()) {
                mJobCategoryModel.add(new SelectionModel(type.getId(), type.getName()));
            }
        }
        vJobCategoryLayout.setOnClickListener(v -> showSelectionDialog(mJobCategoryModel, (code, model) -> {
            tvJobCategoryContent.setText(model.getValue());
            idJobCategory = model.id;
        }));
        //Position
        BaseReturnFunctionEntity<ArrayList<LevelJobEntity>> myListPosition = new LevelJobInfo().getList(getApplicationContext(), realm);

        if (myListPosition.isTrue()) {

            for (LevelJobEntity type : myListPosition.getData()) {
                mPositionModel.add(new SelectionModel(type.getId(), type.getName()));
            }
        }
        vJobPositionLayout.setOnClickListener(v -> showSelectionDialog(mPositionModel, (code, model) -> {
            tvJobPositionContent.setText(model.getValue());
            idPosition = model.id;
        }));
        //SortBy
        mSortByModel.add(new SelectionModel(-1, getString(R.string.job_search_none)));
        mSortByModel.add(new SelectionModel(0, getString(R.string.job_search_recent)));
        mSortByModel.add(new SelectionModel(1, getString(R.string.job_search_highest_salary)));
        vSortedByLayout.setOnClickListener(v -> showSelectionDialog(mSortByModel, (code, model) -> {
            tvSortedByContent.setText(model.getValue());
            idSortBy = model.id;
        }));
    }

    @Override
    public void onDestroy() {
        RealmInfo.closeRealm(realm);
        super.onDestroy();
    }

    JobTabFragment tabFragment;

    @OnClick(R.id.tvApply)
    void tvApply() {
        try {
            layout_search_main.setVisibility(View.GONE);
            layoutFragment.setVisibility(View.VISIBLE);
            tabFragment = (JobTabFragment) getSupportFragmentManager().findFragmentByTag("job");
            if (tabFragment == null) {
                tabFragment = JobTabFragment.getInstance(0);
                getSupportFragmentManager().beginTransaction().replace(R.id.layoutFragment, tabFragment, "job").addToBackStack(null).commit();
            } else {
                tabFragment.getData();
            }
        } catch (Exception e) {
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home) {
            onBackPressed();


            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onBackPressed() {
        if (layout_search_main.getVisibility() == View.VISIBLE) {
            layoutFragment.setVisibility(View.VISIBLE);
            layout_search_main.setVisibility(View.GONE);
        } else {
            layoutFragment.setVisibility(View.GONE);
            layout_search_main.setVisibility(View.VISIBLE);
        }
        super.onBackPressed();

    }

    public void showSelectionDialog(List<SelectionModel> mData, DialogSelectionResultListener listener) {
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        Fragment prev = getFragmentManager().findFragmentByTag(SelectionFragmentDialog.class.getSimpleName());
        if (prev != null) {
            ft.remove(prev);
        }
        ft.addToBackStack(null);
        SelectionFragmentDialog dialog;
        dialog = SelectionFragmentDialog.newInstance(mData);
        dialog.setOnItemListener(listener);
        dialog.show(getSupportFragmentManager(), SelectionFragmentDialog.class.getSimpleName());
    }

    @Override
    public void onBeginLoad(android.support.v4.app.Fragment fragment) {

    }

    @Override
    public void onEndLoad(android.support.v4.app.Fragment fragment) {

    }

    @Override
    public JsonArray getData(int page, int size, int type, android.support.v4.app.Fragment fragment) {
        try {
            ServiceResponseEntity<JsonArray> result = new JobInfo().searchWithCity(this, page, size, URLEncoder.encode(edtKeyWordContent.getText().toString(), "UTF-8"), idCountry, idCity, idJobCategory, idPosition, idSalaryForm, idSalaryTo, edtCompanyContent.getText().toString(), idSortBy);
            if (result != null && result.getStatus() == 0)
                return result.getData();
            return new JsonArray();
        } catch (Exception ex) {
            return new JsonArray();
        }
    }

    @OnClick(R.id.tvClear)
    void tvClear() {
        tvJobPositionContent.setText("");
        tvJobCategoryContent.setText("");
        tvSalaryContent.setText("");
        tvNationContent.setText("");
        tvCityContent.setText("");
        tvSortedByContent.setText("");
        edtCompanyContent.setText("");
        edtKeyWordContent.setText("");
        idSalaryForm = 0;
        idSalaryTo = 999999999;
        idSortBy = idPosition = idCity = idJobCategory = idCountry = -1;
    }
}
