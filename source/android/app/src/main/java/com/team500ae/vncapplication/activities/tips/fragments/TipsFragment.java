package com.team500ae.vncapplication.activities.tips.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;

import com.team500ae.library.adapters.BaseViewPagerAdapter;
import com.team500ae.vncapplication.R;
import com.team500ae.vncapplication.activities.tips.MgrCategoryActivity;
import com.team500ae.vncapplication.activities.tips.SearchTipsActivity;
import com.team500ae.vncapplication.data_access.RealmInfo;
import com.team500ae.vncapplication.data_access.tips.TypeTipsInfo;
import com.team500ae.vncapplication.data_access.user.UserInfo;
import com.team500ae.vncapplication.models.BaseReturnFunctionEntity;
import com.team500ae.vncapplication.models.realm_models.tips.TypeTipsEntity;
import com.team500ae.vncapplication.services.ServiceTips;
import com.team500ae.vncapplication.utils.AppUtils;
import com.team500ae.vncapplication.utils.LanguageUtils;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.realm.Realm;

/**
 * Created by phuong.ndp on 11/27/2017.
 * 171204-1106pm
 */

public class TipsFragment extends Fragment {
    Realm realm;
    private BaseReturnFunctionEntity<ArrayList<TypeTipsEntity>> myListTypeTips;
    private ArrayList<TypeTipsEntity> myTypeTips;
    private ArrayList<Fragment> tipsTabFragments;
    private ArrayList<String> listTitle;
    private BaseViewPagerAdapter adapter;
    private TypeTipsInfo typeTips;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_tips, container, false);
        ButterKnife.bind(this, view);
        realm = RealmInfo.getRealm(getContext());

        myTypeTips = new ArrayList<>();
        listTitle = new ArrayList<>();
        tipsTabFragments = new ArrayList<>();

        showFragment();

        tabLayout.setupWithViewPager(viewPager);
        viewPager.setOffscreenPageLimit(4);

        adapter = new BaseViewPagerAdapter(getContext(), getChildFragmentManager(), tipsTabFragments, listTitle);
        viewPager.setAdapter(adapter);

        return view;
    }

    public void showFragment() {
        try {
            typeTips = new TypeTipsInfo();
            myListTypeTips = typeTips.getTypeTipsHasSelected(getContext(), realm, UserInfo.getCurrentUserId());
            if (myListTypeTips.isTrue())
                myTypeTips.addAll(myListTypeTips.getData());

            if (myTypeTips.size() == 0) {
                if (AppUtils.isNetworkAvailable(getActivity())) {
                    ServiceTips.startSetDefaultCategory(getContext());
                }
                BaseReturnFunctionEntity<ArrayList<TypeTipsEntity>> temp = typeTips.getListTypeTips(getContext(), realm);
                if (temp.isTrue()) {
                    for (int i = 0; i < 5 && i < temp.getData().size(); i++) {
                        myTypeTips.add(temp.getData().get(i));
                    }
                }
            }

            //add 'ALL' category. ID = -2 => API will get all tips when type category is -2
            TypeTipsEntity allCategory = new TypeTipsEntity();
            allCategory.setId(-2);
            allCategory.setName(getString(R.string.category_all));
            allCategory.setName2(getString(R.string.category_all));
            myTypeTips.add(0, allCategory);

            if (UserInfo.isLogin()) {
                TypeTipsEntity bookmark = new TypeTipsEntity();
                bookmark.setId(-1);
                bookmark.setName(getString(R.string.has_bookmark));
                bookmark.setName2(getString(R.string.has_bookmark));
                myTypeTips.add(0, bookmark);
            }
            String appLanguageSetting = LanguageUtils.getAppLanguageSetting(getActivity());
            for (TypeTipsEntity typeTips : myTypeTips) {
                listTitle.add(appLanguageSetting.equals("en") ? typeTips.getName() : typeTips.getName2());
                tipsTabFragments.add(TipsTabFragment.getInstance(typeTips.getId()));
            }
//            adapter.notifyDataSetChanged();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @BindView(R.id.viewPager)
    ViewPager viewPager;
    @BindView(R.id.tabs)
    TabLayout tabLayout;

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.clear();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDestroy() {
        RealmInfo.closeRealm(realm);
        super.onDestroy();
    }

    public static TipsFragment getInstants() {
        TipsFragment tipsFragment = new TipsFragment();
        return tipsFragment;
    }

    @OnClick(R.id.btnAddCat)
    void btnAddCatClick() {
        MgrCategoryActivity.open(getContext());
    }

    @OnClick(R.id.btnSearch)
    void btnSearchClick() {
        SearchTipsActivity.open(getActivity());
    }

    public void notifyOnChangeTypeTips() {
        tabLayout.removeAllTabs();
        showFragment();
    }

    public void notifyBookmarkChange() {
        if (myTypeTips != null && myTypeTips.size() > 0 && myTypeTips.get(0).getId() == -1) {
            ((TipsTabFragment) tipsTabFragments.get(0)).getData();
        }
    }
}
