package com.team500ae.vncapplication.models.realm_models.translator;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.Required;

/**
 * Created by TamPC on 3/24/2018.
 */

public class CategoryTranslatorEntity extends RealmObject {
    @PrimaryKey
    int id;

    @Required
    String name;

    String name2;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName2() {
        return name2 != null ? name2 : "";
    }

    public void setName2(String name2) {
        this.name2 = name2;
    }
}
