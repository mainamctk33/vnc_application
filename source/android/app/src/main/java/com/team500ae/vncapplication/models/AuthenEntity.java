package com.team500ae.vncapplication.models;

import com.google.gson.JsonObject;
import com.team500ae.library.utils.ConvertUtils;
import com.team500ae.library.utils.StringUtils;
import com.team500ae.vncapplication.data_access.user.UserInfo;


import java.util.Date;

/**
 * Created by MaiNam on 11/15/2017.
 */

public class AuthenEntity {
    private String PRIVATE_KEY = "123i@123team500ae";
    private String key = "";
    private String token = "";
    private String loginToken = "";


    public String getKey() {
        return key;
    }
    public String getToken() {
        return token;
    }
    public String getLoginToken() {
        return loginToken;
    }

    public AuthenEntity() {
        String time = String.valueOf(new Date().getTime() / 1000);
        String key = "";
        if (UserInfo.isLogin()) {
            key = UserInfo.getCurrentUserId();
            loginToken = UserInfo.getLoginToken();
        } else {
            key = time;
        }
        this.key = key;
        this.token = StringUtils.getMD5(key + PRIVATE_KEY);
    }

    public static String getAuthen() {
        AuthenEntity authenEntity = new AuthenEntity();
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("Key", authenEntity.getKey());
        jsonObject.addProperty("LoginToken", authenEntity.getLoginToken());
        jsonObject.addProperty("Token", authenEntity.getToken());
        return ConvertUtils.toJson(jsonObject);
    }
}
