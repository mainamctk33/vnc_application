package com.team500ae.vncapplication.data_access;

import android.content.Context;
import android.net.Uri;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.reflect.TypeToken;
import com.team500ae.library.constants.ServerConstants;
import com.team500ae.library.model.DataDictionary;
import com.team500ae.library.utils.ConvertUtils;
import com.team500ae.library.utils.FileInfo;
import com.team500ae.vncapplication.models.AuthenEntity;
import com.team500ae.vncapplication.models.ServiceResponseEntity;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Type;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * Created by MaiNam on 11/15/2017.
 */

public class ClientInfo {
    private static final String TAG = ClientInfo.class.getSimpleName();
    String serverUrl = "http://api.vnc.iduhoc.vn/api/";


    public enum MethodType {
        PUT,
        POST,
        GET,
        DELETE
    }

    private static OkHttpClient client;
    private static int timeOut = 100;

    private static OkHttpClient getClient() {
        if (client == null)
            client = new OkHttpClient.Builder()
                    .connectTimeout(timeOut, TimeUnit.SECONDS)
                    .readTimeout(timeOut, TimeUnit.SECONDS)
                    .writeTimeout(timeOut, TimeUnit.SECONDS)
                    .build();
        return client;
    }

    private Request.Builder getRequestBuilder(String url) {
        Log.d(TAG, "getRequestBuilder: "+url);
        return new Request.Builder().addHeader("Auth", AuthenEntity.getAuthen()).url(url);
    }


    public DataResponse postFile(Context context, String serverUrl, String api, ArrayList<Uri> files) throws Exception {
        MultipartBody.Builder formBody = new MultipartBody.Builder()
                .setType(MultipartBody.FORM);
        File _file;
        for (Uri uri : files) {
            _file = FileInfo.getFromUri(context, uri);
            formBody.addFormDataPart("file", _file.getName(),
                    RequestBody.create(MediaType.parse("image/png"), _file));
        }
        return requestApi(MethodType.POST,serverUrl,api,formBody.build(),0);
    }

    public DataResponse postFile(Context context, String api, ArrayList<Uri> files) throws Exception {
        return postFile(context,serverUrl,api,files);
    }


    public DataResponse requestApi(MethodType methodType, String api, JsonObject dataBody) throws UnknownHostException, SocketTimeoutException {
        RequestBody requestBody = RequestBody.create(ServerConstants.JSON, ConvertUtils.toJson(dataBody));
        return requestApi(methodType, serverUrl, api, requestBody, timeOut);
    }

    public <T> DataResponse requestApi(MethodType methodType, String api) throws UnknownHostException, SocketTimeoutException {
        return requestApi(methodType, serverUrl, api);
    }

    public <T> DataResponse requestApi(MethodType methodType, String server, String api) throws UnknownHostException, SocketTimeoutException {
        Request.Builder builder = getRequestBuilder(server + api);
        Request request = builder.build();
        return execute(request, timeOut);
    }

    public <T> DataResponse requestApi(MethodType methodType, String api, JsonObject dataBody, int timeOut) throws UnknownHostException, SocketTimeoutException {
        RequestBody requestBody = RequestBody.create(ServerConstants.JSON, ConvertUtils.toJson(dataBody));
        return requestApi(methodType, serverUrl, api, requestBody, timeOut);
    }

    public <T> DataResponse requestApi(MethodType methodType, String server, String api, JsonObject dataBody, int timeOut) throws UnknownHostException, SocketTimeoutException {
        RequestBody requestBody = RequestBody.create(ServerConstants.JSON, ConvertUtils.toJson(dataBody));
        return requestApi(methodType, server, api, requestBody, timeOut);
    }

    public <T> DataResponse requestApi(MethodType methodType, String server, String api, RequestBody requestBody, int timeOut) throws UnknownHostException, SocketTimeoutException {
        return requestApi(methodType, server + api, requestBody, timeOut);
    }

    public <T> DataResponse requestApi(MethodType methodType, String url, RequestBody requestBody, int timeOut) throws UnknownHostException, SocketTimeoutException {
        Request.Builder builder = getRequestBuilder(url);
        if (requestBody != null) {
            switch (methodType) {
                case POST:
                    builder.post(requestBody);
                    break;
                case PUT:
                    builder.put(requestBody);
                    break;
                case DELETE:
                    builder.delete(requestBody);
            }
        }
        Request request = builder.build();
        return execute(request, timeOut);
    }

    <T> DataResponse execute(Request request, int timeOut) throws SocketTimeoutException, UnknownHostException {
        try {
            OkHttpClient client = null;
            if (timeOut != 0) {
                client = new OkHttpClient.Builder()
                        .connectTimeout(timeOut, TimeUnit.SECONDS)
                        .readTimeout(timeOut, TimeUnit.SECONDS)
                        .writeTimeout(timeOut, TimeUnit.SECONDS)
                        .build();
            } else {
                client = getClient();
            }
            Response response = client.newCall(request).execute();
            String bodyString = response.body().string();//getFromStream(response.body().byteStream(),"UTF-16");
            Log.d(TAG, "execute: "+bodyString);
            return new DataResponse(response.code(), bodyString);
        } catch (SocketTimeoutException e) {
            throw e;
        } catch (UnknownHostException e) {
            throw e;
        } catch (Exception e) {
            e.printStackTrace();
        } catch (OutOfMemoryError e) {
            e.printStackTrace();
        }
        return new DataResponse(500, "{Message: Internal server error}");
    }


    public static class DataResponse {
        private int status;
        private String body;

        public <T> T getData(Type type) {
            T t = ConvertUtils.toObject(body, type);
            return t;
        }
        public <T> T getData(Class<T> c) {
            T t = ConvertUtils.toObject(body, c);
            return t;
        }

        public boolean isOK() {
            return status == ServerConstants.RESPONSE_OK;
        }

        public boolean isTimeOut() {
            return status == ServerConstants.REQUEST_TIMEOUT_ERROR;
        }

        public DataResponse(int status, String body) {
            this.status = status;
            this.body = body;
        }

        public DataResponse(Response response) {
            this.status = response.code();
            try {
                this.body = response.body().string();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        public int getStatus() {
            return status;
        }

        public void setStatus(int status) {
            this.status = status;
        }

        public String getBody() {
            return body;
        }

        public void setBody(String body) {
            this.body = body;
        }
    }
}
