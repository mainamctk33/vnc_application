package com.team500ae.vncapplication.activities.translator.fragments;


import android.annotation.SuppressLint;
import android.os.AsyncTask;
import android.os.Bundle;
import android.speech.tts.TextToSpeech;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.team500ae.library.adapters.BaseRecyclerAdapter;
import com.team500ae.library.layoutmanager.MyLinearLayoutManager;
import com.team500ae.library.utils.ConvertUtils;
import com.team500ae.library.utils.RecyclerViewUtils;
import com.team500ae.vncapplication.R;
import com.team500ae.vncapplication.activities.login.LoginActivity;
import com.team500ae.vncapplication.activities.translator.TranslatorAdapter;
import com.team500ae.vncapplication.activities.translator.TranslatorModel;
import com.team500ae.vncapplication.base.BaseActivity;
import com.team500ae.vncapplication.base.BaseViewHolder;
import com.team500ae.vncapplication.data_access.BookmarkInfo;
import com.team500ae.vncapplication.data_access.user.UserInfo;
import com.team500ae.vncapplication.models.BaseReturnFunctionEntity;
import com.team500ae.vncapplication.others.GridSpacingItemDecoration;
import com.team500ae.vncapplication.services.ServiceTranslator;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class TranslatorCommonFragment extends Fragment {
    @BindView(R.id.loading)
    LinearLayout loading;
    @BindView(R.id.fragTranslatorMyFavourite_rvList)
    RecyclerView rvList;
    Unbinder unbinder;
    View view;
    TextToSpeech textToSpeech;
    int result;
    boolean isLoading = false;
    @BindView(R.id.swipeRefreshLayout)
    SwipeRefreshLayout swipeRefreshLayout;
    private int selectPos = -1;
    int page = 1;
    int size = 10;
    private boolean isViewShown = false;
    HashMap<Integer, JsonObject> mapData = new HashMap<>();
    private TranslatorAdapter adapter;
    private List<TranslatorModel> mData;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_translator_my_favourite, container, false);
        unbinder = ButterKnife.bind(this, view);
        init();
        if (!isViewShown) {
            getData(page, size, -1, getListener());
        }
        return view;
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (textToSpeech != null) {
            textToSpeech.stop();
            textToSpeech.shutdown();
        }
    }

    private void initTextToSpeech(Locale locale, String text) {
        textToSpeech = new TextToSpeech(getActivity(), new TextToSpeech.OnInitListener() {
            @Override
            public void onInit(int status) {
                if (status == TextToSpeech.SUCCESS) {
                    result = textToSpeech.setLanguage(locale);
                    textToSpeech.speak(text, TextToSpeech.QUEUE_FLUSH, null);
                } else {
                    Toast.makeText(getActivity(), "Feature not supported in your device", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void init() {
        mData = new ArrayList<>();
        adapter = new TranslatorAdapter(mData);
        rvList.setLayoutManager(new GridLayoutManager(getActivity(), 3));
        rvList.setClipToPadding(false);
        GridSpacingItemDecoration decoration = new GridSpacingItemDecoration(3, getActivity().getResources().getDimensionPixelSize(R.dimen._5dp), false);
        rvList.addItemDecoration(decoration);
        rvList.setAdapter(adapter);
    }

    public interface LoadTranslatorListener {
        public void onBeginLoad(Fragment fragment);

        public void onEndLoad(Fragment fragment);

        public JsonArray getData(int page, int size, int type, Fragment translatorTabFragment);
    }

    public TranslatorCommonFragment.LoadTranslatorListener getListener() {
        if (getContext() instanceof TranslatorCommonFragment.LoadTranslatorListener)
            return (TranslatorCommonFragment.LoadTranslatorListener) getContext();
        return null;
    }

    @SuppressLint("StaticFieldLeak")
    public void getData(final int page, final int size, final int type, final LoadTranslatorListener listener) {
        new AsyncTask<Void, JsonArray, Void>() {

            @Override
            protected void onPreExecute() {
                swipeRefreshLayout.setEnabled(false);
                if (page == 1)
                    swipeRefreshLayout.setRefreshing(true);
                else
                    loading.setVisibility(View.VISIBLE);
                if (listener != null)
                    listener.onBeginLoad(TranslatorCommonFragment.this);
                isLoading = true;
                super.onPreExecute();
            }

            @Override
            protected Void doInBackground(Void... voids) {

                if (listener != null) {
                    publishProgress(listener.getData(page, size, type, TranslatorCommonFragment.this));
                } else
                    publishProgress(new JsonArray());
                return null;
            }

            @Override
            protected void onProgressUpdate(JsonArray... values) {
                if (values.length > 0 && values[0] != null) {
                    if (page == 1) {
                        mapData.clear();
                        mData.clear();
                        adapter.notifyDataSetChanged();
                    }

                    bindData(values[0]);
                }
                super.onProgressUpdate(values);
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                swipeRefreshLayout.setEnabled(true);
                if (page == 1)
                    swipeRefreshLayout.setRefreshing(false);
                else
                    loading.setVisibility(View.GONE);
                if (listener != null)
                    listener.onEndLoad(TranslatorCommonFragment.this);
                isLoading = false;
                super.onPostExecute(aVoid);
            }
        }.execute();
    }

    void bindData(JsonArray jsonArray) {
        for (int i = 0; i < jsonArray.size(); i++) {
            JsonObject jsonObject = ConvertUtils.toJsonObject(jsonArray.get(i));
            int id = ConvertUtils.toInt(jsonObject.get("ID"));
            if (mapData.containsKey(id)) {
                int index = mData.indexOf(mapData.get(id));
                mapData.put(id, jsonObject);
                if (index != -1) {
                    mData.set(index, new TranslatorModel(jsonObject));
                    adapter.notifyItemChanged(index);
                }
            } else {
                mapData.put(id, jsonObject);
                mData.add(new TranslatorModel(jsonObject));
                adapter.notifyItemInserted(mData.size() - 1);
            }
        }
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (getView() != null) {
            isViewShown = true;
            getData(page, size, -1, getListener());
        } else {
            isViewShown = false;
        }
    }
}
