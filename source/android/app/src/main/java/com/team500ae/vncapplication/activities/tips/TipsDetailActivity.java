package com.team500ae.vncapplication.activities.tips;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.webkit.WebView;
import android.widget.ImageView;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.team500ae.library.utils.ConvertUtils;
import com.team500ae.vncapplication.R;
import com.team500ae.vncapplication.activities.login.LoginActivity;
import com.team500ae.vncapplication.base.BaseActivity;
import com.team500ae.vncapplication.constants.Constants;
import com.team500ae.vncapplication.data_access.BookmarkInfo;
import com.team500ae.vncapplication.data_access.job.JobInfo;
import com.team500ae.vncapplication.data_access.tips.TipsInfo;
import com.team500ae.vncapplication.data_access.user.UserInfo;
import com.team500ae.vncapplication.models.BaseReturnFunctionEntity;
import com.team500ae.vncapplication.models.ServiceResponseEntity;
import com.team500ae.vncapplication.services.ServiceTips;
import com.team500ae.vncapplication.utils.ShareUtils;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by phuong.ndp on 11/27/2017.
 * done
 */

public class TipsDetailActivity extends BaseActivity implements com.team500ae.library.activities.BaseActivity.OnCallBackBroadcastListener {

    private int id;
    private boolean hasBookmark;
    @BindView(R.id.webView)
    WebView webView;

    @BindView(R.id.btnBookmark)
    ImageView imgBookmark;
    ArrayList<Integer> listBookmarkID;

    public static void open(Context context, int id) {
        Intent intent = new Intent(context, TipsDetailActivity.class);
        intent.putExtra("TIPS_ID", id);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        setContentView(R.layout.activity_detail_tips);
        ButterKnife.bind(this);
        super.onCreate(savedInstanceState);
        showBackButton();
        setTitle(R.string.title_tips);
        id = getIntent().getIntExtra("TIPS_ID", 0);
        //webView.loadUrl("http://onlineapi.site/loi-khuyen/" + id);
        webView.loadUrl("http://vnc.iduhoc.vn/loi-khuyen/" + id);

        listBookmarkID = new ArrayList<>();
        if (!UserInfo.isLogin()) {
            imgBookmark.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.ic_like));
        }else {
            new AsyncTask<Void, Void, ServiceResponseEntity<JsonArray>>() {
                @Override
                protected ServiceResponseEntity<JsonArray> doInBackground(Void... voids) {
                    ServiceResponseEntity<JsonArray> data;
                    try {
                        data = new TipsInfo().getAll(getApplicationContext(), 1, 99999999, -1,"");
                    } catch (Exception ex) {
                        return null;
                    }
                    return data;
                }

                @Override
                protected void onPostExecute(ServiceResponseEntity<JsonArray> data) {
                    if (data != null && data.getStatus() == 0) {
                        for (int i = 0; i < data.getData().size(); i++) {
                            JsonObject jsonObject = ConvertUtils.toJsonObject(data.getData().get(i));
                            listBookmarkID.add(ConvertUtils.toInt(jsonObject.get("ID")));
                        }
                    }
                    if (listBookmarkID.contains(id))
                        hasBookmark = true;
                    else
                        hasBookmark = false;
                    imgBookmark.setImageDrawable(ContextCompat.getDrawable(getActivity(), hasBookmark ? R.drawable.ic_like_check : R.drawable.ic_like));
                }
            }.execute();
        }

        //BaseReturnFunctionEntity<Boolean> bookmark = new BookmarkInfo().isBookmark(getActivity(), getRealm(), id, UserInfo.getCurrentUserId(), BookmarkInfo.BookmarkType.Tip);
        //hasBookmark = bookmark.isTrue() ? bookmark.data : false;
        //imgBookmark.setImageDrawable(ContextCompat.getDrawable(getActivity(), hasBookmark ? R.drawable.ic_like_check : R.drawable.ic_like));
    }

    @OnClick(R.id.btnBookmark)
    void bookmark() {
        if (!UserInfo.isLogin()) {
            LoginActivity.open(getActivity());
        } else {
            hasBookmark = !hasBookmark;
            ServiceTips.startActionBookmark(getActivity(), id, hasBookmark);
            imgBookmark.setImageDrawable(ContextCompat.getDrawable(getActivity(), hasBookmark ? R.drawable.ic_like_check : R.drawable.ic_like));
        }
    }

    @OnClick(R.id.llShare)
    void share() {
        ShareUtils.shareFacebook(getActivity(), Constants.SERVER + "/applink/tips/" + id, "vnc");
    }

    @Override
    public void onCallBackBroadcastListener(Context context, String action, Intent intent) {
        switch (action) {
            case Constants.ACTION_LOGIN:
                //BaseReturnFunctionEntity<Boolean> bookmark = new BookmarkInfo().isBookmark(getActivity(), getRealm(), id, UserInfo.getCurrentUserId(), BookmarkInfo.BookmarkType.Tip);
                //hasBookmark = bookmark.isTrue() ? bookmark.data : false;
                //imgBookmark.setImageDrawable(ContextCompat.getDrawable(getActivity(), hasBookmark ? R.drawable.ic_like_check : R.drawable.ic_like));
                new AsyncTask<Void, Void, ServiceResponseEntity<JsonArray>>() {
                    @Override
                    protected ServiceResponseEntity<JsonArray> doInBackground(Void... voids) {
                        ServiceResponseEntity<JsonArray> data;
                        try {
                            data = new TipsInfo().getAll(getApplicationContext(), 1, 99999999, -1,"");
                        } catch (Exception ex) {
                            return null;
                        }
                        return data;
                    }

                    @Override
                    protected void onPostExecute(ServiceResponseEntity<JsonArray> data) {
                        if (data != null && data.getStatus() == 0) {
                            for (int i = 0; i < data.getData().size(); i++) {
                                JsonObject jsonObject = ConvertUtils.toJsonObject(data.getData().get(i));
                                listBookmarkID.add(ConvertUtils.toInt(jsonObject.get("ID")));
                            }
                        }
                        if (listBookmarkID.contains(id))
                            hasBookmark = true;
                        else
                            hasBookmark = false;
                        imgBookmark.setImageDrawable(ContextCompat.getDrawable(getActivity(), hasBookmark ? R.drawable.ic_like_check : R.drawable.ic_like));
                    }
                }.execute();
                break;
        }
    }
}
