package com.team500ae.vncapplication.activities.job.selectiondialog;


import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.team500ae.vncapplication.R;
import com.team500ae.vncapplication.activities.job.SelectionModel;
import com.team500ae.vncapplication.activities.job.interfaces.DialogSelectionResultListener;
import com.team500ae.vncapplication.activities.job.interfaces.ItemListener;
import com.team500ae.vncapplication.constants.Constants;
import com.team500ae.vncapplication.utils.AppUtils;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SelectionFragmentDialog extends DialogFragment implements ItemListener {

    @BindView(R.id.dialogSelection_loadingCircular)
    ProgressBar loadingCircular;
    @BindView(R.id.dialogSelection_tvSelect)
    TextView tvSelect;
    @BindView(R.id.dialogSelection_rvSelection)
    RecyclerView rvSelection;
    private List<SelectionModel> mData = new ArrayList<>();
    private DialogSelectionResultListener listener;
    private boolean fullBorder = false;
    private int mCode;

    public static SelectionFragmentDialog newInstance(List<SelectionModel> mData) {
        Bundle args = new Bundle();
        args.putSerializable(Constants.LIST_MODEL, (Serializable) mData);
        SelectionFragmentDialog fragment = new SelectionFragmentDialog();
        fragment.setArguments(args);
        return fragment;
    }

    public void setOnItemListener(DialogSelectionResultListener listener) {
        this.listener = listener;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (getDialog() != null && getDialog().getWindow() != null) {
            getDialog().requestWindowFeature(Window.FEATURE_NO_TITLE);
            getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        }
        View view = inflater.inflate(R.layout.dialog_selection, container, false);
        ButterKnife.bind(this, view);
        getBundle();
        init();
        return view;
    }

    private void getBundle() {
        if (getArguments() != null) {
            mData = (List<SelectionModel>) getArguments().getSerializable(Constants.LIST_MODEL);
        }
    }

    private void init() {
        loadingCircular.getIndeterminateDrawable().setColorFilter(ContextCompat.getColor(getContext(), R.color.white), PorterDuff.Mode.SRC_IN);
        SelectionAdapter adapter = new SelectionAdapter(mData);
        adapter.setOnItemListener(this);
        rvSelection.setLayoutManager(new LinearLayoutManager(getActivity()));
        rvSelection.setAdapter(adapter);
    }

    @Override
    public void onStart() {
        super.onStart();
        if (getDialog() != null && getDialog().getWindow() != null) {
            int marginWidth = (int) getResources().getDimension(R.dimen._44dp);
            int marginHeight = (int) getResources().getDimension(R.dimen._200dp);
            getDialog().getWindow()
                    .setLayout(AppUtils.getScreenWidth() - marginWidth, AppUtils.getScreenHeight() - marginHeight);
        }

    }

    @Override
    public void setStyle(int style, int theme) {
        super.setStyle(style, android.R.style.Theme_Holo_Dialog);
    }


    @Override
    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);
    }

    @Override
    public void onItemClicked(int position) {
        if (listener != null) {
            dismiss();
            listener.onDialogForResult(mCode, mData.get(position));
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }
}
