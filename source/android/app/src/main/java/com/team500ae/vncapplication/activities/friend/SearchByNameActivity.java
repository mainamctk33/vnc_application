package com.team500ae.vncapplication.activities.friend;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.ExclusionStrategy;
import com.google.gson.FieldAttributes;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.google.gson.annotations.SerializedName;
import com.google.gson.reflect.TypeToken;
import com.team500ae.library.adapters.BaseRecyclerAdapter;
import com.team500ae.library.layoutmanager.MyLinearLayoutManager;
import com.team500ae.library.utils.AndroidPermissionUtils;
import com.team500ae.library.utils.ConvertUtils;
import com.team500ae.library.utils.ImageUtils;
import com.team500ae.library.utils.StringUtils;
import com.team500ae.vncapplication.R;
import com.team500ae.vncapplication.activities.chat.GroupChannelActivity;
import com.team500ae.vncapplication.activities.user.UserProfileActivity;
import com.team500ae.vncapplication.base.BaseActivity;
import com.team500ae.vncapplication.base.BaseViewHolder;
import com.team500ae.vncapplication.constants.Constants;
import com.team500ae.vncapplication.data_access.DataCacheInfo;
import com.team500ae.vncapplication.data_access.user.UserInfo;
import com.team500ae.vncapplication.models.BaseReturnFunctionEntity;
import com.team500ae.vncapplication.utils.KeyboardUtils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SearchByNameActivity extends BaseActivity implements Filterable {
    @BindView(R.id.parent)
    RelativeLayout parent;
    private ArrayList<JsonObject> listObjects;
    private ArrayList<JsonObject> listObjectsHistory;
    private BaseRecyclerAdapter<ViewHolder, JsonObject> adapter;
    private long time = 0;
    private BaseRecyclerAdapter<ViewHolder, JsonObject> adapterHistory;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        setContentView(R.layout.fragment_search_friend_by_name);
        ButterKnife.bind(this);
        KeyboardUtils.setupUI(parent, getActivity());
        super.onCreate(savedInstanceState);

        listObjects = new ArrayList<>();
        listObjectsHistory = new ArrayList<>();
        try {
            String text = DataCacheInfo.getData(getActivity(), DataCacheInfo.EnumCacheType.History_User);
            listObjectsHistory = new Gson().fromJson(text, new TypeToken<ArrayList<JsonObject>>() {
            }.getType());
            if (listObjectsHistory == null)
                listObjectsHistory = new ArrayList<>();
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (listObjectsHistory == null)
            listObjectsHistory = new ArrayList<>();
        recyclerView.setLayoutManager(new MyLinearLayoutManager(getActivity()));
        recyclerView.setHasFixedSize(true);
        txtSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                time = new Date().getTime();
                if (!s.toString().equals("")) {
                    mClear.setVisibility(View.VISIBLE);
                    adapter.bindData(recyclerView);

                    if (listObjects != null && listObjects.size() > 0)
                        resultSearch.setVisibility(View.VISIBLE);
                    else
                        resultSearch.setVisibility(View.GONE);
                } else {
                    mClear.setVisibility(View.GONE);
                    adapterHistory.bindData(recyclerView);
                    if (listObjectsHistory != null && listObjectsHistory.size() > 0)
                        resultSearch.setVisibility(View.VISIBLE);
                    else
                        resultSearch.setVisibility(View.GONE);
                }
                getFilter().filter(time + "><" + s.toString());
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        adapter = new BaseRecyclerAdapter<ViewHolder, JsonObject>(R.layout.item_user_search, new BaseRecyclerAdapter.BaseViewHolder<ViewHolder, JsonObject>() {
            @Override
            public ViewHolder getViewHolder(View v) {
                return new ViewHolder(v);
            }

            @Override
            public void bindData(RecyclerView recyclerView, ViewHolder viewHolder, JsonObject jsonObject, int position) throws IOException {
                if (ConvertUtils.toBoolean(jsonObject.get("ShowAvatar"))) {
                    ImageUtils.loadImageByGlide(getActivity(), false, 0, 0, ConvertUtils.toString(jsonObject.get("Avartar")), R.drawable.img_no_image, R.drawable.img_no_image, viewHolder.imageView, true);
                } else {
                    viewHolder.imageView.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.default_avatar));
                }
                viewHolder.tvName.setText(ConvertUtils.toString(jsonObject.get("FullName")));
                String userName = ConvertUtils.toString(jsonObject.get("UserName"));
                viewHolder.btnSendMessage.setTag(userName);
//                String bod = ConvertUtils.toString(jsonObject.get("Birthday"));
//                if (com.team500ae.vncapplication.utils.StringUtils.isNullOrWhiteSpace(bod)) {
//                    viewHolder.lnAge.setVisibility(View.GONE);
//                } else {
//                    viewHolder.lnAge.setVisibility(View.VISIBLE);
//                    long dou = ConvertUtils.toLong(bod);
//                    viewHolder.tvAge.setText(String.valueOf(TimeUtils.getAge(dou)));
//                }
                String phone = ConvertUtils.toString(jsonObject.get("PhoneNumber"));
                if (!com.team500ae.vncapplication.utils.StringUtils.isNullOrWhiteSpace(phone)) {
                    viewHolder.btnCall.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.ic_call));
                } else {
                    viewHolder.btnCall.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.ic_call_dim));
                }
                viewHolder.btnCall.setTag(phone);
                int gender = ConvertUtils.toInt(jsonObject.get("Gender"));
                if (gender == 0) {
                    viewHolder.imgGender.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.ic_female));
                } else {
                    viewHolder.imgGender.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.ic_male));
                }

                String address = ConvertUtils.toString(jsonObject.get("Address"));
                if ("".equals(address)) {
                    Geocoder geocoder;

                    geocoder = new Geocoder(getActivity(), Locale.getDefault());

                    List<Address> addresses = geocoder.getFromLocation(ConvertUtils.toDouble(jsonObject.get("Lat")), ConvertUtils.toDouble(jsonObject.get("Lon")), 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5
                    address = "";
                    if (addresses.size() != 0) {
                        Address _address = addresses.get(0);
                        if (!com.team500ae.vncapplication.utils.StringUtils.isNullOrWhiteSpace(_address.getLocality())) {
                            address += _address.getLocality();
                        }
                        if (!com.team500ae.vncapplication.utils.StringUtils.isNullOrWhiteSpace(_address.getSubAdminArea())) {
                            if (!address.isEmpty()) {
                                address += ", ";
                            }
                            address += _address.getSubAdminArea();
                        }
                        if (!com.team500ae.vncapplication.utils.StringUtils.isNullOrWhiteSpace(_address.getAdminArea())) {
                            if (!address.isEmpty()) {
                                address += ", ";
                            }
                            address += _address.getAdminArea();
                        }
                        if (!com.team500ae.vncapplication.utils.StringUtils.isNullOrWhiteSpace(_address.getCountryName())) {
                            if (!address.isEmpty()) {
                                address += ", ";
                            }
                            address += _address.getCountryName();
                        }
                        jsonObject.addProperty("Address", address);
                    }
                }
                viewHolder.tvAddress.setText(address);
                if (StringUtils.isEmpty(address)) {
                    viewHolder.tvAddress.setVisibility(View.GONE);
                } else
                    viewHolder.tvAddress.setVisibility(View.VISIBLE);
            }
        }, listObjects, new BaseRecyclerAdapter.OnClickListener() {
            @Override
            public void onClick(View v, int position, Object o) {
                String userName = addContactToSearchHistory(o);
                Gson gson = new GsonBuilder()
                        .setExclusionStrategies(new SerializedNameOnlyStrategy())
                        .create();
                String json = gson.toJson(listObjectsHistory);
                DataCacheInfo.setData(getActivity(), DataCacheInfo.EnumCacheType.History_User, json);
                UserProfileActivity.open(getActivity(), userName);
            }
        });


        adapterHistory = new BaseRecyclerAdapter<ViewHolder, JsonObject>(R.layout.item_user_search, new BaseRecyclerAdapter.BaseViewHolder<ViewHolder, JsonObject>() {
            @Override
            public ViewHolder getViewHolder(View v) {
                return new ViewHolder(v);
            }

            @Override
            public void bindData(RecyclerView recyclerView, ViewHolder viewHolder, JsonObject jsonObject, int position) throws IOException {
                if (ConvertUtils.toBoolean(jsonObject.get("ShowAvatar"))) {
                    ImageUtils.loadImageByGlide(getActivity(), false, 0, 0, ConvertUtils.toString(jsonObject.get("Avartar")), R.drawable.img_no_image, R.drawable.img_no_image, viewHolder.imageView, true);
                } else {
                    viewHolder.imageView.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.default_avatar));
                }
                viewHolder.tvName.setText(ConvertUtils.toString(jsonObject.get("FullName")));
                String userName = ConvertUtils.toString(jsonObject.get("UserName"));
                viewHolder.btnSendMessage.setTag(userName);
//                String bod = ConvertUtils.toString(jsonObject.get("Birthday"));
//                if (com.team500ae.vncapplication.utils.StringUtils.isNullOrWhiteSpace(bod)) {
//                    viewHolder.lnAge.setVisibility(View.GONE);
//                } else {
//                    viewHolder.lnAge.setVisibility(View.VISIBLE);
//                    long dou = ConvertUtils.toLong(bod);
//                    viewHolder.tvAge.setText(String.valueOf(TimeUtils.getAge(dou)));
//                }
                String phone = ConvertUtils.toString(jsonObject.get("PhoneNumber"));
                if (!com.team500ae.vncapplication.utils.StringUtils.isNullOrWhiteSpace(phone)) {
                    viewHolder.btnCall.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.ic_call));
                } else {
                    viewHolder.btnCall.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.ic_call_dim));
                }
                viewHolder.btnCall.setTag(phone);
                int gender = ConvertUtils.toInt(jsonObject.get("Gender"));
                if (gender == 0) {
                    viewHolder.imgGender.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.ic_female));
                } else {
                    viewHolder.imgGender.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.ic_male));
                }

                String address = ConvertUtils.toString(jsonObject.get("Address"));
                if ("".equals(address)) {
                    Geocoder geocoder;

                    geocoder = new Geocoder(getActivity(), Locale.getDefault());

                    List<Address> addresses = geocoder.getFromLocation(ConvertUtils.toDouble(jsonObject.get("Lat")), ConvertUtils.toDouble(jsonObject.get("Lon")), 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5
                    address = "";
                    if (addresses.size() != 0) {
                        Address _address = addresses.get(0);
                        if (!com.team500ae.vncapplication.utils.StringUtils.isNullOrWhiteSpace(_address.getLocality())) {
                            address += _address.getLocality();
                        }
                        if (!com.team500ae.vncapplication.utils.StringUtils.isNullOrWhiteSpace(_address.getSubAdminArea())) {
                            if (!address.isEmpty()) {
                                address += ", ";
                            }
                            address += _address.getSubAdminArea();
                        }
                        if (!com.team500ae.vncapplication.utils.StringUtils.isNullOrWhiteSpace(_address.getAdminArea())) {
                            if (!address.isEmpty()) {
                                address += ", ";
                            }
                            address += _address.getAdminArea();
                        }
                        if (!com.team500ae.vncapplication.utils.StringUtils.isNullOrWhiteSpace(_address.getCountryName())) {
                            if (!address.isEmpty()) {
                                address += ", ";
                            }
                            address += _address.getCountryName();
                        }
                        jsonObject.addProperty("Address", address);
                    }
                }
                viewHolder.tvAddress.setText(address);
                if (StringUtils.isEmpty(address)) {
                    viewHolder.tvAddress.setVisibility(View.GONE);
                } else
                    viewHolder.tvAddress.setVisibility(View.VISIBLE);
            }
        }, listObjectsHistory, new BaseRecyclerAdapter.OnClickListener() {
            @Override
            public void onClick(View v, int position, Object o) {
                JsonObject jsonObject = (JsonObject) o;
                UserProfileActivity.open(getActivity(), ConvertUtils.toString(jsonObject.get("UserName")));
            }
        });
        if (listObjectsHistory != null && listObjectsHistory.size() > 0)
            resultSearch.setVisibility(View.VISIBLE);
        else
            resultSearch.setVisibility(View.GONE);
        adapterHistory.bindData(recyclerView);
    }

    private String addContactToSearchHistory(Object obj) {
        JsonObject jsonObject = (JsonObject) obj;
        String inputUsername = ConvertUtils.toString(jsonObject.get("UserName")).toLowerCase();
        String userName;
        for (int i = 0; i < listObjectsHistory.size(); i++) {
            userName = ConvertUtils.toString(listObjectsHistory.get(i).get("UserName")).toLowerCase();
            if (userName.equals(inputUsername)){
                listObjectsHistory.remove(i);
                break;
            }
        }

        listObjectsHistory.add(0, jsonObject);
        if (listObjectsHistory.size() > Constants.MAX_HISTORY_SEARCH_ITEM)
            listObjectsHistory.remove(listObjectsHistory.size() - 1);

        return inputUsername;
    }

    public class SerializedNameOnlyStrategy implements ExclusionStrategy {

        @Override
        public boolean shouldSkipField(FieldAttributes f) {
            return f.getAnnotation(SerializedName.class) == null;
        }

        @Override
        public boolean shouldSkipClass(Class<?> clazz) {
            return false;
        }
    }

    @OnClick({R.id.btnBack})
    void back() {
        this.finish();
    }

    @BindView(R.id.btnClear)
    View mClear;

    @OnClick(R.id.btnClear)
    void clear() {
        txtSearch.setText("");
    }

    @BindView(R.id.txtSearch)
    EditText txtSearch;
    @BindView(R.id.resultSearch)
    View resultSearch;
    @BindView(R.id.list_search)
    RecyclerView recyclerView;

    private JsonObject getAutocomplete(CharSequence constraint, double time) {
        BaseReturnFunctionEntity<JsonObject> result = new UserInfo().search(1, 50, constraint.toString(), time);
        if (result.isTrue()) {
            return result.getData();
        }
        return new JsonObject();
    }

    @Override
    public Filter getFilter() {
        Filter filter = new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint1) {
                if (constraint1 == null)
                    constraint1 = "";
                String constraint = constraint1.toString();
                String qr = constraint.toString();
                String[] arr = qr.split("><");
                FilterResults results = new FilterResults();
                if (arr.length <= 1) {
                    return null;
                } else {
                    JsonObject req = getAutocomplete(arr[1], ConvertUtils.toDouble(arr[0]));
                    if (ConvertUtils.toDouble(req.get("Time")) == time) {
                        ArrayList<JsonObject> mResultList = ConvertUtils.toArrayList(ConvertUtils.toJsonArray(req.get("Users")));
                        results.values = mResultList;
                        results.count = mResultList.size();
                        return results;
                    } else
                        return null;
                }
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                if (results != null) {
                    listObjects.clear();
                    if (results != null && results.count > 0) {
                        listObjects.addAll((ArrayList<JsonObject>) results.values);
                        // The API returned at least one result, update the data.
                    }
                    adapter.notifyDataSetChanged();
                    if (listObjects != null && listObjects.size() > 0) {
                        resultSearch.setVisibility(View.VISIBLE);
                    } else
                        resultSearch.setVisibility(View.GONE);
                }
            }
        };
        return filter;
    }

    public class ViewHolder extends BaseViewHolder {
        @BindView(R.id.imageView)
        ImageView imageView;
        @BindView(R.id.tvUserName)
        TextView tvName;
        @BindView(R.id.btnSendMessage)
        View btnSendMessage;
        @BindView(R.id.tvAddress)
        TextView tvAddress;
        @BindView(R.id.btnCall)
        ImageView btnCall;
        @BindView(R.id.imgGender)
        ImageView imgGender;
        @BindView(R.id.lnAge)
        View lnAge;
        @BindView(R.id.tvAge)
        TextView tvAge;


        @OnClick(R.id.btnCall)
        void call(View view) {
            requestPermission(getActivity(), new AndroidPermissionUtils.OnCallbackRequestPermission() {
                @SuppressLint("MissingPermission")
                @Override
                public void onSuccess() {
                    String phone = (String) view.getTag();
                    if (phone != null && !phone.trim().isEmpty()) {
                        try {
                            Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + phone));
                            startActivity(intent);
                            return;
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                    showSnackBar(R.string.not_found_phone_number_in_profile);
                }

                @Override
                public void onFailed() {
                    showSnackBar(getActivity(), R.string.you_much_allow_this_app_make_a_call);
                }
            }, AndroidPermissionUtils.TypePermission.CALL_PHONE);
        }


        @OnClick(R.id.btnSendMessage)
        void chat(View view) {
            if ("".equals(view.getTag())) {
                showSnackBar(getActivity(), R.string.add_friend_before_chat);
                return;
            }

            String userId = (String) view.getTag();
            GroupChannelActivity.open(getActivity(), userId);
        }

        public ViewHolder(View itemView) {
            super(itemView);
        }
    }

    public static void open(Context context) {
        Intent intent = new Intent(context, SearchByNameActivity.class);
        context.startActivity(intent);
    }
}
