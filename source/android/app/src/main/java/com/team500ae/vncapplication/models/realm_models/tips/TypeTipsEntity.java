package com.team500ae.vncapplication.models.realm_models.tips;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.Required;

/**
 * Created by phuong.ndp on 11/27/2017.
 * done
 */

public class TypeTipsEntity extends RealmObject {
    @PrimaryKey
    int id;

    @Required
    String name;
    String name2;
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName2() {
        return name2 != null ? name2 : "";
    }

    public void setName2(String name2) {
        this.name2 = name2;
    }
}
