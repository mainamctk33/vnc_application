package com.team500ae.vncapplication.models;

import com.google.gson.JsonElement;

/**
 * Created by maina on 11/16/2017.
 */

public class ServiceResponseEntity<T> {
    public int Status;
    public T Data;
    public String Message;

    public int getStatus() {
        return Status;
    }

    public T getData() {
        return Data;
    }
}
