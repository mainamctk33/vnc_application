package com.team500ae.vncapplication.activities.tips;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.reflect.TypeToken;
import com.team500ae.library.adapters.BaseRecyclerAdapter;
import com.team500ae.library.layoutmanager.MyLinearLayoutManager;
import com.team500ae.library.utils.ConvertUtils;
import com.team500ae.vncapplication.R;
import com.team500ae.vncapplication.activities.tips.fragments.TipsTabFragment;
import com.team500ae.vncapplication.base.BaseActivity;
import com.team500ae.vncapplication.base.BaseViewHolder;
import com.team500ae.vncapplication.constants.Constants;
import com.team500ae.vncapplication.data_access.DataCacheInfo;
import com.team500ae.vncapplication.data_access.tips.TipsInfo;
import com.team500ae.vncapplication.models.ServiceResponseEntity;
import com.team500ae.vncapplication.utils.KeyboardUtils;
import com.team500ae.vncapplication.utils.StringUtils;

import java.util.ArrayList;

import javax.annotation.Nullable;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by phuong.ndp on 05/12/2017.
 */

public class SearchTipsActivity extends BaseActivity implements TipsTabFragment.LoadTipsListener {
    TipsTabFragment TipsTabFragment;
    String tagSearch = null;
    String keyword = null;
    @BindView(R.id.searchTip_layout)
    RelativeLayout searchTipLayout;
    private ArrayList<String> listData = new ArrayList<>();
    private BaseRecyclerAdapter<ViewHolder, String> adapter;

    public static void open(FragmentActivity activity) {
        Intent intent = new Intent(activity, SearchTipsActivity.class);
        activity.startActivity(intent);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        setContentView(R.layout.activity_search_tips);
        ButterKnife.bind(this);
        KeyboardUtils.setupUI(searchTipLayout, getActivity());
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();
        txtSearch.setImeActionLabel(getString(R.string.keyboard_search), KeyEvent.KEYCODE_ENTER);
        txtSearch.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                if (i == EditorInfo.IME_ACTION_DONE) {
                    tagSearch = null;
                    keyword = textView.getText().toString();
                    if (!keyword.equals("")) {
                        listData.remove(keyword);
                        listData.add(0, keyword);
                    }
                    listData = StringUtils.getLimitedListString(listData, Constants.MAX_HISTORY_SEARCH_ITEM);
                    adapter.notifyDataSetChanged();
                    DataCacheInfo.setData(getRealm(), DataCacheInfo.EnumCacheType.History_Tips, ConvertUtils.toJson(listData), true);
                    cardHistory.setVisibility(listData.size() > 0 ? View.VISIBLE : View.GONE);
                    getTips();
                }
                return true;
            }
        });
        txtSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
//                if (charSequence.toString().isEmpty()) {
//                    btnSearch.setImageDrawable(getResources().getDrawable(R.drawable.ic_search_b));
//                } else {
//                    btnSearch.setImageDrawable(getResources().getDrawable(R.drawable.ic_close));
//                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        recyclerView.setLayoutManager(new MyLinearLayoutManager(getActivity()));
        adapter = new BaseRecyclerAdapter<ViewHolder, String>(R.layout.item_tag, new BaseRecyclerAdapter.BaseViewHolder<ViewHolder, String>() {
            @Override
            public ViewHolder getViewHolder(View v) {
                return new ViewHolder(v);
            }

            @Override
            public void bindData(RecyclerView recyclerView, ViewHolder viewHolder, String data, int position) {
                viewHolder.tvName.setText(data);
            }
        }, listData, new BaseRecyclerAdapter.OnClickListener() {
            @Override
            public void onClick(View v, int position, Object o) {
                txtSearch.setText(String.valueOf(o));
                search();
            }
        });
        adapter.bindData(recyclerView);
        getHistory();
    }

    void getHistory() {
        String dataHistory = DataCacheInfo.getData(getRealm(), DataCacheInfo.EnumCacheType.History_Tips);
        if (!TextUtils.isEmpty(dataHistory)) {
            ArrayList<String> listHistory = new Gson().fromJson(DataCacheInfo.getData(getRealm(), DataCacheInfo.EnumCacheType.History_Tips), new TypeToken<ArrayList<String>>() {
            }.getType());
            // ArrayList<String> listHistory = ConvertUtils.toArrayList(DataCacheInfo.getData(getRealm(), DataCacheInfo.EnumCacheType.History_Tips));
            listData.addAll(listHistory);
            listData = StringUtils.getLimitedListString(listData, Constants.MAX_HISTORY_SEARCH_ITEM);
            adapter.notifyDataSetChanged();
            cardHistory.setVisibility(listData.size() > 0 ? View.VISIBLE : View.GONE);
        }
    }

    @OnClick(R.id.btnBack)
    void back() {
        this.onBackPressed();
        txtSearch.setText("");
    }

    @BindView(R.id.btnSearch)
    ImageView btnSearch;
    @BindView(R.id.txtSearch)
    TextView txtSearch;
    @BindView(R.id.layoutFragment)
    LinearLayout layoutFragment;
    @BindView(R.id.cardHistory)
    View cardHistory;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;

    @OnClick(R.id.btnSearch)
    void search() {
        keyword = txtSearch.getText().toString();
        if (!keyword.equals("")) {
            listData.remove(keyword);
            listData.add(0, keyword);
        }
        adapter.notifyDataSetChanged();
        DataCacheInfo.setData(getRealm(), DataCacheInfo.EnumCacheType.History_Tips, ConvertUtils.toJson(listData), true);
        cardHistory.setVisibility(listData.size() > 0 ? View.VISIBLE : View.GONE);
        getTips();
    }

    void getTips() {
        TipsTabFragment = (TipsTabFragment) getSupportFragmentManager().findFragmentByTag("Tips");
        if (TipsTabFragment == null) {
            TipsTabFragment = TipsTabFragment.getInstance(0);
            getSupportFragmentManager().beginTransaction().replace(R.id.layoutFragment, TipsTabFragment, "Tips").addToBackStack(null).commit();
        } else {
            TipsTabFragment.getData();
        }

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
//        showHomeButton(menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public void onBeginLoad(Fragment fragment) {

    }

    @Override
    public void onEndLoad(Fragment fragment) {

    }

    @Override
    public JsonArray getData(int page, int size, int type, Fragment TipsTabFragment) {
        ServiceResponseEntity<JsonArray> result = new TipsInfo().getAll(getActivity(), page, size, type, keyword);
        if (result != null && result.getStatus() == 0)
            return result.getData();
        return new JsonArray();
    }

    public class ViewHolder extends BaseViewHolder {
        @BindView(R.id.tvName)
        TextView tvName;

        public ViewHolder(View itemView) {
            super(itemView);
        }
    }
}
