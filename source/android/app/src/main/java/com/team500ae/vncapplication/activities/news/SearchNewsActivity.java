package com.team500ae.vncapplication.activities.news;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.cunoraz.tagview.Tag;
import com.cunoraz.tagview.TagView;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.team500ae.library.adapters.BaseRecyclerAdapter;
import com.team500ae.library.layoutmanager.MyLinearLayoutManager;
import com.team500ae.library.utils.ConvertUtils;
import com.team500ae.vncapplication.R;
import com.team500ae.vncapplication.activities.news.fragments.NewsTabFragment;
import com.team500ae.vncapplication.base.BaseActivity;
import com.team500ae.vncapplication.base.BaseViewHolder;
import com.team500ae.vncapplication.constants.Constants;
import com.team500ae.vncapplication.data_access.DataCacheInfo;
import com.team500ae.vncapplication.data_access.news.NewsInfo;
import com.team500ae.vncapplication.models.ServiceResponseEntity;
import com.team500ae.vncapplication.utils.KeyboardUtils;
import com.team500ae.vncapplication.utils.StringUtils;

import java.util.ArrayList;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by maina on 12/1/2017.
 */

public class SearchNewsActivity extends BaseActivity implements NewsTabFragment.LoadNewsListener {
    @BindView(R.id.searchNew_layout)
    RelativeLayout searchNewLayout;
    private long currentTime;
    NewsTabFragment newsTabFragment;
    String tagSearch = null;
    String keyword = null;
    private ArrayList<String> listData = new ArrayList<>();
    private BaseRecyclerAdapter<ViewHolder, String> adapter;

    public static void open(FragmentActivity activity) {
        Intent intent = new Intent(activity, SearchNewsActivity.class);
        activity.startActivity(intent);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        setContentView(R.layout.activity_search_news);
        ButterKnife.bind(this);
        KeyboardUtils.setupUI(searchNewLayout, getActivity());
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();
        txtSearch.setImeActionLabel(getString(R.string.keyboard_search), KeyEvent.KEYCODE_ENTER);
        txtSearch.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                if (i == EditorInfo.IME_ACTION_DONE) {
                    tagSearch = null;
                    keyword = textView.getText().toString();
                    if (!keyword.equals("")) {
                        listData.remove(keyword);
                        listData.add(0, keyword);
                    }
                    listData = StringUtils.getLimitedListString(listData, Constants.MAX_HISTORY_SEARCH_ITEM);
                    adapter.notifyDataSetChanged();
                    DataCacheInfo.setData(getRealm(), DataCacheInfo.EnumCacheType.History, ConvertUtils.toJson(listData), true);
                    cardHistory.setVisibility(listData.size() > 0 ? View.VISIBLE : View.GONE);
                    getNews();
                }
                return true;
            }
        });
        JsonArray tags = ConvertUtils.toJsonArray(DataCacheInfo.getData(getRealm(), DataCacheInfo.EnumCacheType.Tag));
        for (int i = 0; i < tags.size(); i++) {
            JsonObject jsonObject = ConvertUtils.toJsonObject(tags.get(i));
            tagView.addTag(new Tag(ConvertUtils.toString(jsonObject.get("TagName"))));
        }
        tagView.setOnTagClickListener(new TagView.OnTagClickListener() {
            @Override
            public void onTagClick(Tag tag, int i) {
                txtSearch.setText("");
                keyword = "";
                tagSearch = tag.text;
                getNews();
            }
        });
        currentTime = new Date().getTime();
        txtSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }


            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
//                if (charSequence.toString().isEmpty()) {
//                    btnSearch.setImageDrawable(getResources().getDrawable(R.drawable.ic_search_b));
//                } else {
//                    btnSearch.setImageDrawable(getResources().getDrawable(R.drawable.ic_close));
//                }

            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        recyclerView.setLayoutManager(new MyLinearLayoutManager(getActivity()));
        adapter = new BaseRecyclerAdapter<ViewHolder, String>(R.layout.item_tag, new BaseRecyclerAdapter.BaseViewHolder<ViewHolder, String>() {
            @Override
            public ViewHolder getViewHolder(View v) {
                return new ViewHolder(v);
            }

            @Override
            public void bindData(RecyclerView recyclerView, ViewHolder viewHolder, String data, int position) {
                viewHolder.tvName.setText(data);
            }
        }, listData, new BaseRecyclerAdapter.OnClickListener() {
            @Override
            public void onClick(View v, int position, Object o) {
                txtSearch.setText(String.valueOf(o));
                search();
            }
        });
        adapter.bindData(recyclerView);
        getHistory();
    }

    void getHistory() {
        String dataHistory = DataCacheInfo.getData(getRealm(), DataCacheInfo.EnumCacheType.History);
        if (!TextUtils.isEmpty(dataHistory)) {
            ArrayList<String> listHistory = new Gson().fromJson(DataCacheInfo.getData(getRealm(), DataCacheInfo.EnumCacheType.History), new TypeToken<ArrayList<String>>() {
            }.getType());
            //ArrayList<String> listHistory = ConvertUtils.toArrayList(DataCacheInfo.getData(getRealm(), DataCacheInfo.EnumCacheType.History));
            listData.addAll(listHistory);
            listData = StringUtils.getLimitedListString(listData, Constants.MAX_HISTORY_SEARCH_ITEM);
            adapter.notifyDataSetChanged();
            cardHistory.setVisibility(listData.size() > 0 ? View.VISIBLE : View.GONE);
        }
    }

    @OnClick(R.id.btnBack)
    void back() {
        this.onBackPressed();
        txtSearch.setText("");
    }

    @BindView(R.id.btnSearch)
    ImageView btnSearch;
    @BindView(R.id.txtSearch)
    TextView txtSearch;
    @BindView(R.id.layoutFragment)
    LinearLayout layoutFragment;
    @BindView(R.id.cardHistory)
    View cardHistory;
    @BindView(R.id.cardTag)
    View cardTag;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;

    @OnClick(R.id.btnSearch)
    void search() {
        tagView = null;
        keyword = txtSearch.getText().toString();
        if (!keyword.equals("")) {
            listData.remove(keyword);
            listData.add(0, keyword);
        }
        adapter.notifyDataSetChanged();
        DataCacheInfo.setData(getRealm(), DataCacheInfo.EnumCacheType.History, ConvertUtils.toJson(listData), true);
        cardHistory.setVisibility(listData.size() > 0 ? View.VISIBLE : View.GONE);
        getNews();
    }

    void getNews() {
        newsTabFragment = (NewsTabFragment) getSupportFragmentManager().findFragmentByTag("news");
        if (newsTabFragment == null) {
            newsTabFragment = NewsTabFragment.getInstance(0);
            getSupportFragmentManager().beginTransaction().replace(R.id.layoutFragment, newsTabFragment, "news").addToBackStack(null).commit();
        } else {
            newsTabFragment.getData();
        }

    }


    @BindView(R.id.tag_group)
    TagView tagView;

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
//        showHomeButton(menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public void onBeginLoad(Fragment fragment) {

    }

    @Override
    public void onEndLoad(Fragment fragment) {

    }

    @Override
    public JsonArray getData(int page, int size, int type, Fragment newsTabFragment) {
        try {
            ServiceResponseEntity<JsonArray> result = new NewsInfo().getAll(getActivity(), page, size, type, keyword, tagSearch);
            if (result.getStatus() == 0)
                return result.getData();
        }catch (Exception e)
        {
            e.printStackTrace();
        }
        return new JsonArray();
    }

    public class ViewHolder extends BaseViewHolder {
        @BindView(R.id.tvName)
        TextView tvName;

        public ViewHolder(View itemView) {
            super(itemView);
        }
    }
}
