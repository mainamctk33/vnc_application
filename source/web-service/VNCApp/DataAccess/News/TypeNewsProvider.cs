﻿using System;
using System.Collections.Generic;
using System.Linq;
using VNCApp.Database.News.TypeNews;
using VNCApp.Models;

namespace VNCApp.DataAccess
{
    public class TypeNewsProvider : IProvider<TypeNew, int>
    {
        public BaseReturnFunction<List<TypeNew>> GetAll()
        {
            try
            {
                using (var context = new TypeNewsDbDataContext())
                {
                    var list = context.TypeNews;
                    return new BaseReturnFunction<List<TypeNew>>(StatusFunction.TRUE, list.ToList(), list.Count());
                }
            }
            catch (Exception e)
            {
                return new BaseReturnFunction<List<TypeNew>>(StatusFunction.EXCEPTION, e.Message);
            }
        }

        public BaseReturnFunction<List<TypeNew>> Search(int page, int size, string keyword)
        {
            try
            {
                using (var context = new TypeNewsDbDataContext())
                {
                    IQueryable<TypeNew> list;
                    if (String.IsNullOrWhiteSpace(keyword))
                        list = context.TypeNews;
                    else
                    {
                        keyword = keyword.Trim().ToLower();
                        list = context.TypeNews.Where(x => x.Name.ToLower().Contains(keyword) || x.Name2.ToLower().Contains(keyword));
                    }
                    return new BaseReturnFunction<List<TypeNew>>(StatusFunction.TRUE, list.Skip((page - 1) * size).Take(size).ToList(), list.Count());
                }
            }
            catch (Exception e)
            {
                return new BaseReturnFunction<List<TypeNew>>(StatusFunction.EXCEPTION, e.Message);
            }
        }
        public BaseReturnFunction<Boolean> Delete(int id)
        {
            try
            {
                using (var context = new TypeNewsDbDataContext())
                {
                    var cat = context.TypeNews.SingleOrDefault(x => x.ID == id);
                    if (cat != null)
                    {
                        context.TypeNews.DeleteOnSubmit(cat);
                        context.SubmitChanges();
                    }
                }
                return new BaseReturnFunction<bool>(StatusFunction.TRUE, true);
            }
            catch (Exception e)
            {
                return new BaseReturnFunction<bool>(StatusFunction.EXCEPTION, e.Message);
            }
        }

        public BaseReturnFunction<TypeNew> GetById(int id)
        {
            try
            {
                using (var context = new TypeNewsDbDataContext())
                {
                    var cat = context.TypeNews.SingleOrDefault(x => x.ID == id);
                    if (cat != null)
                        return new BaseReturnFunction<TypeNew>(StatusFunction.TRUE, cat);
                    return new BaseReturnFunction<TypeNew>(StatusFunction.TRUE, "Không tồn tại chuyên mục với id = " + id);
                }
            }
            catch (Exception e)
            {
                return new BaseReturnFunction<TypeNew>(StatusFunction.EXCEPTION, e.Message);
            }
        }

        public BaseReturnFunction<TypeNew> Create(TypeNew model)
        {
            try
            {
                using (var context = new TypeNewsDbDataContext())
                {
                    context.TypeNews.InsertOnSubmit(model);
                    context.SubmitChanges();
                }
                return new BaseReturnFunction<TypeNew>(StatusFunction.TRUE, model);
            }
            catch (Exception e)
            {
                return new BaseReturnFunction<TypeNew>(StatusFunction.EXCEPTION, e.Message);
            }
        }

        public BaseReturnFunction<TypeNew> Update(int id, TypeNew model)
        {
            try
            {
                using (var context = new TypeNewsDbDataContext())
                {
                    TypeNew typeNew = context.TypeNews.SingleOrDefault(x => x.ID == id);
                    if (typeNew != null)
                    {
                        typeNew.Name = model.Name;
                        typeNew.Name2 = model.Name2;
                        context.SubmitChanges();
                        return new BaseReturnFunction<TypeNew>(StatusFunction.TRUE, typeNew);
                    }
                }
                return new BaseReturnFunction<TypeNew>(StatusFunction.FALSE, "Không tồn tại chuyên mục với id = " + id);
            }
            catch (Exception e)
            {
                return new BaseReturnFunction<TypeNew>(StatusFunction.EXCEPTION, e.Message);
            }
        }

        public BaseReturnFunction<List<TypeNew>> Sync(double time)
        {
            try
            {
                using (var context = new TypeNewsDbDataContext())
                {
                    return new BaseReturnFunction<List<TypeNew>>(StatusFunction.TRUE, context.TypeNews.ToList());
                }
            }
            catch (Exception e)
            {
                return new BaseReturnFunction<List<TypeNew>>(StatusFunction.EXCEPTION, e.Message);
            }
        }

    }
}