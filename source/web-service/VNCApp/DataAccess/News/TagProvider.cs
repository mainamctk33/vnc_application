﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using VNCApp.Database.News.Tag;
using VNCApp.Models;

namespace VNCApp.DataAccess.News
{
    public class TagProvider : IProvider<Tag, String>
    {
        public BaseReturnFunction<List<Tag>> GetAll()
        {
            throw new NotImplementedException();
        }

        public BaseReturnFunction<bool> Delete(string id)
        {
            throw new NotImplementedException();
        }

        public BaseReturnFunction<Tag> GetById(string id)
        {
            throw new NotImplementedException();
        }

        public BaseReturnFunction<Tag> Create(Tag model)
        {
            try
            {
                using (var context = new TagDbDataContext())
                {
                    var tag = context.Tags.SingleOrDefault(x => x.TagName == model.TagName);
                    if (tag == null)
                    {
                        context.Tags.InsertOnSubmit(model);
                        context.SubmitChanges();
                    }
                    return new BaseReturnFunction<Tag>(StatusFunction.TRUE,tag);
                }
            }
            catch (Exception e)
            {
                return new BaseReturnFunction<Tag>(StatusFunction.EXCEPTION, e.Message);
            }
        }

        public BaseReturnFunction<Boolean> AssignNews(String tagName, int newsId)
        {
            try
            {
                using (var context = new TagDbDataContext())
                {
                    context.NewsTags.InsertOnSubmit(new NewsTag()
                    {
                        ID = newsId,
                        TagName = tagName
                    });
                    context.SubmitChanges();
                    return new BaseReturnFunction<Boolean>(StatusFunction.TRUE,true);
                }
            }
            catch (Exception e)
            {
                return new BaseReturnFunction<Boolean>(StatusFunction.EXCEPTION, e.Message);
            }
        }

        public BaseReturnFunction<Tag> Update(string id, Tag model)
        {
            throw new NotImplementedException();
        }

        public BaseReturnFunction<List<Tag>> Sync(double time)
        {
            try
            {
                using (var context = new TagDbDataContext())
                {
                    var list = context.Tags.OrderByDescending(x => x.NewsTags.Count).Take(10).ToList();
                    return new BaseReturnFunction<List<Tag>>(StatusFunction.TRUE, list, list.Count);
                }
            }
            catch (Exception e)
            {
                return new BaseReturnFunction<List<Tag>>(StatusFunction.EXCEPTION, e.Message);
            }
        }

        public BaseReturnFunction<Boolean> ClearAllTagOfNews(int modelId)
        {
            try
            {
                using (var context = new TagDbDataContext())
                {
                    var list = context.NewsTags.Where(x=>x.ID==modelId);
                    context.NewsTags.DeleteAllOnSubmit(list);
                    context.SubmitChanges();
                    return new BaseReturnFunction<Boolean>(StatusFunction.TRUE,true);
                }
            }
            catch (Exception e)
            {
                return new BaseReturnFunction<Boolean>(StatusFunction.EXCEPTION, false);
            }
        }

        public BaseReturnFunction<List<Tag>> GetTagByNews(int id)
        {
            try
            {
                using (var context = new TagDbDataContext())
                {
                    var list = context.NewsTags.Where(x => x.ID == id);
                    return new BaseReturnFunction<List<Tag>>(StatusFunction.TRUE, list.Select(x=>x.Tag).ToList());
                }

            }
            catch (Exception e)
            {
                return new BaseReturnFunction<List<Tag>>(StatusFunction.EXCEPTION, e.Message);
            }
        }
    }
}