﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BaseLibrary.Common;
using VNCApp.Database.News.News;
using VNCApp.Models;

namespace VNCApp.DataAccess.News
{
    public class NewsProvider : IProvider<New, int>
    {

        public BaseReturnFunction<List<New>> GetAll(int page, int size, string tag, int type, String keyword, String userId)
        {
            try
            {
                using (var context = new NewsDbDataContext())
                {
                    IQueryable<New> list = null;
                    //type = -2 ~ get tips for tab 'All'
                    if (type == -2)
                        list = context.News;
                    else if (type == -1)
                    {
                        if (!String.IsNullOrWhiteSpace(userId))
                            list = context.BookmarkNews.Where(x => x.BookmarkBy == userId).OrderBy(x => x.BookmarkDate).Select(x => x.New);
                    }
                    else
                        list = context.News.Where(x => type == 0 || x.Type == type);

                    if (list == null)
                        return new BaseReturnFunction<List<New>>(StatusFunction.FALSE, "");

                    list = list.Where(x =>
                        //(String.IsNullOrWhiteSpace(keyword) || (x.Title.Contains(keyword) || x.Content.Contains(keyword)))
                        (String.IsNullOrWhiteSpace(keyword) || (x.Title.Contains(keyword)))
                        && (String.IsNullOrWhiteSpace(tag) || x.NewsTags.Any(y => y.TagName == tag))).OrderByDescending(x => x.CreatedDate);
                    int count = list.Count();
                    return new BaseReturnFunction<List<New>>(StatusFunction.TRUE,
                        list.Skip((page - 1) * size).Take(size).ToList(), count);
                }
            }
            catch (Exception e)
            {
                return new BaseReturnFunction<List<New>>(StatusFunction.EXCEPTION, e.Message);
            }
        }


        public BaseReturnFunction<List<New>> GetAll()
        {
            throw new NotImplementedException();
        }

        public BaseReturnFunction<bool> Delete(int id)
        {
            try
            {
                using (var context = new NewsDbDataContext())
                {
                    var news = context.News.SingleOrDefault(x => x.ID == id);
                    if (news != null)
                    {
                        context.News.DeleteOnSubmit(news);
                        context.SubmitChanges();
                    }
                }
                return new BaseReturnFunction<bool>(StatusFunction.TRUE, true);
            }
            catch (Exception e)
            {
                return new BaseReturnFunction<bool>(StatusFunction.EXCEPTION, e.Message);
            }
        }

        public BaseReturnFunction<New> GetById(int id)
        {
            try
            {
                using (var context = new NewsDbDataContext())
                {
                    var news = context.News.SingleOrDefault(x => x.ID == id);
                    if (news != null)
                    {
                        return new BaseReturnFunction<New>(StatusFunction.TRUE, news);
                    }
                    return new BaseReturnFunction<New>(StatusFunction.FALSE, "Không tồn tại tin tức với id = " + id);
                }

            }
            catch (Exception e)
            {
                return new BaseReturnFunction<New>(StatusFunction.EXCEPTION, e.Message);
            }
        }

        public BaseReturnFunction<New> Create(New model)
        {
            try
            {
                using (var context = new NewsDbDataContext())
                {
                    context.News.InsertOnSubmit(model);
                    context.SubmitChanges();
                }
                return new BaseReturnFunction<New>(StatusFunction.TRUE, model);
            }
            catch (Exception e)
            {
                return new BaseReturnFunction<New>(StatusFunction.EXCEPTION, e.Message);
            }
        }

        public BaseReturnFunction<New> Update(int id, New model)
        {
            try
            {
                using (var context = new NewsDbDataContext())
                {
                    New news = context.News.SingleOrDefault(x => x.ID == id);
                    if (news != null)
                    {
                        news.Content = model.Content;
                        news.CreatedBy = model.CreatedBy;
                        news.CreatedDate = model.CreatedDate;
                        news.SubContent = model.SubContent;
                        news.Thumbnail = model.Thumbnail;
                        news.Title = model.Title;
                        news.Type = model.Type;
                        news.UpdatedDate = DateTime.Now.GetUnixTimeStamp();
                        context.SubmitChanges();
                        return new BaseReturnFunction<New>(StatusFunction.TRUE, news);
                    }
                }
                return new BaseReturnFunction<New>(StatusFunction.FALSE, "Không tồn tại tin tức với id = " + id);
            }
            catch (Exception e)
            {
                return new BaseReturnFunction<New>(StatusFunction.EXCEPTION, e.Message);
            }

        }

        public BaseReturnFunction<List<New>> Sync(double time)
        {
            throw new NotImplementedException();
        }

        public BaseReturnFunction<Boolean> Bookmark(int id, bool bookmark, string requestUserId)
        {
            try
            {
                using (var context = new NewsDbDataContext())
                {
                    if (!bookmark)
                    {
                        var bm = context.BookmarkNews.SingleOrDefault(x => x.BookmarkBy == requestUserId && x.ID == id);
                        if (bm != null)
                        {
                            context.BookmarkNews.DeleteOnSubmit(bm);
                            context.SubmitChanges();
                        }
                        return new BaseReturnFunction<bool>(StatusFunction.TRUE, true);
                    }
                    else
                    {
                        context.BookmarkNews.InsertOnSubmit(new BookmarkNew()
                        {
                            ID = id,
                            BookmarkBy = requestUserId,
                            BookmarkDate = DateTime.Now.GetUnixTimeStamp()
                        });
                        context.SubmitChanges();
                        return new BaseReturnFunction<bool>(StatusFunction.TRUE, true);
                    }
                }
            }
            catch (Exception e)
            {
                return new BaseReturnFunction<bool>(StatusFunction.EXCEPTION, e.Message);
            }
        }


    }
}