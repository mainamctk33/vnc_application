﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BaseLibrary.Common;
using System.Dynamic;

using VNCApp.Models;
using System.Web.Script.Serialization;

namespace VNCApp.DataAccess.SendBird
{
    public class SendBirdProvider
    {
        String sendBirdServer = "https://api.sendbird.com/v3/";
        String ApiToken = "1d69d24ba6bc6e70acf68aeedc5528365c5ef981";
        public BaseReturnFunction<Object> GetBlockList(String userId, String token, int limit)
        {
            String _token = !string.IsNullOrWhiteSpace(token) ? "&token=" + token : "";
            Dictionary<String, String> header = new Dictionary<string, string>();
            header.Add("Content-Type", "application/json, charset=utf8");
            header.Add("Api-Token", ApiToken);
            var data = ClientUtils.GetData<Object>(sendBirdServer, $"users/{userId}/block?limit={limit}{_token}", header);
           return new BaseReturnFunction<Object>(StatusFunction.TRUE,data.Data.Content,0);
        }

        internal object LeaveGroup(string username, string fullname, string channelUrl)
        {
            Dictionary<String, String> header = new Dictionary<string, string>();
            header.Add("Content-Type", "application/json, charset=utf8");
            header.Add("Api-Token", ApiToken);
            dynamic obj = new ExpandoObject();
            dynamic _type = new ExpandoObject();
            _type.type = "leave_group";
            dynamic users = new ExpandoObject();
            users.user_id = username;
            users.nickname = fullname;
            _type.users = new List<dynamic> { users };
            obj.message = "user leave group";
            obj.data = Newtonsoft.Json.JsonConvert.SerializeObject(_type);
            obj.message_type = "ADMM";
            obj.is_silent = true;
            var data = ClientUtils.PostData<Object, dynamic>(sendBirdServer, $"group_channels/{channelUrl}/messages", obj, header);
            return new BaseReturnFunction<Object>(StatusFunction.TRUE, data.Data);
        }

        internal object UpdateGroup(string username, string fullname, string channelUrl)
        {
            Dictionary<String, String> header = new Dictionary<string, string>();
            header.Add("Content-Type", "application/json, charset=utf8");
            header.Add("Api-Token", ApiToken);
            dynamic obj = new ExpandoObject();
            dynamic _type = new ExpandoObject();
            _type.type = "update_group";
            dynamic users = new ExpandoObject();
            users.user_id = username;
            users.nickname = fullname;
            _type.users = new List<dynamic> { users };
            obj.message = "user update group";
            obj.data = Newtonsoft.Json.JsonConvert.SerializeObject(_type);
            obj.message_type = "ADMM";
            obj.is_silent = true;
            var data = ClientUtils.PostData<Object, dynamic>(sendBirdServer, $"group_channels/{channelUrl}/messages", obj, header);
            return new BaseReturnFunction<Object>(StatusFunction.TRUE, data.Data);
        }

        internal object InviteMember(string username, string fullname, List<MemberSendBird> members, string channelUrl)
        {
            Dictionary<String, String> header = new Dictionary<string, string>();
            header.Add("Content-Type", "application/json, charset=utf8");
            header.Add("Api-Token", ApiToken);
            dynamic obj = new ExpandoObject();
            dynamic _type = new ExpandoObject();
            _type.type = "invite_member_group";
            dynamic user = new ExpandoObject();
            user.user_id = username;
            user.nickname = fullname;
            _type.user = user;
            _type.members = members;
            obj.message = "user invite member";
            obj.data = Newtonsoft.Json.JsonConvert.SerializeObject(_type);
            obj.message_type = "ADMM";
            obj.is_silent = true;
            var data = ClientUtils.PostData<Object, dynamic>(sendBirdServer, $"group_channels/{channelUrl}/messages", obj, header);
            return new BaseReturnFunction<Object>(StatusFunction.TRUE, data.Data);
        }
        public BaseReturnFunction<Object> CreateAccount(String userId, String fullname, String avatar )
        {
            Dictionary<String, String> header = new Dictionary<string, string>();
            header.Add("Content-Type", "application/json, charset=utf8");
            header.Add("Api-Token", ApiToken);
            dynamic obj = new ExpandoObject();
            obj.user_id = userId;
            obj.nickname = fullname;
            obj.profile_url = avatar;
            var data = ClientUtils.PostData<Object, dynamic>(sendBirdServer, $"users", obj,header);
            return new BaseReturnFunction<Object>(StatusFunction.TRUE, data.Data);
        }
    }
}