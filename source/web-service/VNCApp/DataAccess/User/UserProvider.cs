﻿using System;
using System.Collections.Generic;
using System.Linq;
using VNCApp.Database.User;
using VNCApp.Models;
using System.Device.Location;
using BaseLibrary.Common;
using BaseLibrary.Helper;
using VNCApp.Database.User.FriendDb;
using System.Web.Hosting;

namespace VNCApp.DataAccess.User
{
    public class UserProvider : IProvider<Database.User.User, String>
    {
        public enum SocialType
        {
            Fb,
            Gg
        }
        public BaseReturnFunction<List<Database.User.User>> GetAll()
        {
            try
            {
                //id~u
                using (var context = new UserDbDataContext())
                {
                    IQueryable<Database.User.User> listUsers;
                    listUsers = context.Users.Where(x => x.UserName != null);
                    if (listUsers != null)
                    {
                        return new BaseReturnFunction<List<Database.User.User>>(StatusFunction.TRUE, listUsers.ToList(), listUsers.Count());
                    }
                    return new BaseReturnFunction<List<Database.User.User>>(StatusFunction.FALSE, "Danh sách tài khoản là trống.");
                }
            }
            catch (Exception e)
            {
                return new BaseReturnFunction<List<Database.User.User>>(StatusFunction.EXCEPTION, e.Message);
            }
        }

        public BaseReturnFunction<object> Search(int page, int size, String query, String Time, String requestUserId)
        {
            try
            {
                using (var context = new FriendDbDataContext())
                {
                    IQueryable<Database.User.FriendDb.User> listUsers;
                    if (String.IsNullOrWhiteSpace(query))
                        listUsers = context.Users;
                    else
                    {
                        query = query.ToLower().Trim();
                        listUsers = context.Users.Where(x => x.FullName.ToLower().Contains(query));
                    }
                    if (listUsers != null)
                    {
                        return new BaseReturnFunction<object>(StatusFunction.TRUE, new
                        {
                            Time,
                            Users = listUsers.Skip((page - 1) * size).Take(size).Select(x => new
                            {
                                x.Gender,
                                x.UserName,
                                x.FullName,
                                x.Birthday,
                                x.Avartar,
                                x.PhoneNumber,
                                x.Lat,
                                x.Lon,
                                x.Introduction,
                                x.SettingAttribute,
                                x.ShowAvatar,
                                x.ReceiveStrangeMessage,
                                x.ShowPhoneNumber,
                                isFriend = (x.Friends.SingleOrDefault(y => y.IsAccepted && (y.FriendId == requestUserId || y.UserId == requestUserId)) != null || x.Friends1.SingleOrDefault(y => y.IsAccepted && (y.FriendId == requestUserId || y.UserId == requestUserId)) != null)
                            }).ToList()
                        }, listUsers.Count());
                    }
                    return new BaseReturnFunction<object>(StatusFunction.FALSE, "Danh sách tài khoản là trống.");
                }
            }
            catch (Exception e)
            {
                return new BaseReturnFunction<object>(StatusFunction.EXCEPTION, e.Message);
            }
        }


        public BaseReturnFunction<bool> Delete(string id)
        {
            throw new NotImplementedException();
        }

        public BaseReturnFunction<Database.User.User> GetById(string id)
        {
            try
            {
                //id~username
                if (String.IsNullOrWhiteSpace(id))
                    return new BaseReturnFunction<Database.User.User>(StatusFunction.FALSE, "Tài khoản hoặc mật khẩu là null hoặc trống/ Username or password is null or empty");

                using (var context = new UserDbDataContext())
                {
                    var user = context.Users.SingleOrDefault(x => x.UserName != null && x.UserName.ToLower().Equals(id.ToLower()));
                    if (user != null)
                    {
                        return new BaseReturnFunction<Database.User.User>(StatusFunction.TRUE, user);
                    }
                    return new BaseReturnFunction<Database.User.User>(StatusFunction.FALSE, "Tài khoản này không tồn tại/ Your account does not exist: " + id);
                }
            }
            catch (Exception e)
            {
                return new BaseReturnFunction<Database.User.User>(StatusFunction.EXCEPTION, e.Message);
            }
        }

        public BaseReturnFunction<bool> IsExisted(string username)
        {
            try
            {
                if (String.IsNullOrWhiteSpace(username))
                    return new BaseReturnFunction<bool>(StatusFunction.EXCEPTION, "Tài khoản cần kiểm tra là trống hoặc null/ Username is null or empty.");

                using (var context = new UserDbDataContext())
                {
                    var user = context.Users.SingleOrDefault(x => x.UserName != null && x.UserName.ToLower().Equals(username.ToLower()));
                    if (user != null)
                        return new BaseReturnFunction<bool>(StatusFunction.TRUE, true);
                }
                return new BaseReturnFunction<bool>(StatusFunction.FALSE, false);
            }
            catch (Exception e)
            {
                return new BaseReturnFunction<bool>(StatusFunction.EXCEPTION, e.Message);
            }
        }

        public BaseReturnFunction<Database.User.User> LoginEmail(string username, String password)
        {
            try
            {
                if (String.IsNullOrWhiteSpace(username) || String.IsNullOrWhiteSpace(password))
                    return new BaseReturnFunction<Database.User.User>(StatusFunction.FALSE, "Tài khoản hoặc mật khẩu là null hoặc trống/ Username or password is null or empty");

                using (var context = new UserDbDataContext())
                {
                    var user = context.Users.SingleOrDefault(x => x.UserName != null && x.UserName.ToLower().Equals(username.ToLower()) && x.Password != null && x.Password.Equals(password));
                    if (user != null)
                    {
                        if(user.IsActived==null || user.IsActived.Value==0)
                        {
                            return new BaseReturnFunction<Database.User.User>(StatusFunction.FALSE, "Tài khoản chưa được kích hoạt/ Your account has not been activated");
                        }
                        context.SubmitChanges();
                        user.LoginToken = TokenHelper.GenerateToken(user.UserName);
                        return new BaseReturnFunction<Database.User.User>(StatusFunction.TRUE, user);
                    }

                    //check account is existed or not
                    user = context.Users.SingleOrDefault(x => x.UserName != null && x.UserName.ToLower().Equals(username.ToLower()));
                    if (user == null)
                        return new BaseReturnFunction<Database.User.User>(StatusFunction.FALSE, "Tài khoản này không tồn tại/ Your account does not exist: " + username);

                    return new BaseReturnFunction<Database.User.User>(StatusFunction.FALSE, "Tài khoản hoặc mật khẩu không chính xác/ Username or password is not correct.");
                }
            }
            catch (Exception e)
            {
                return new BaseReturnFunction<Database.User.User>(StatusFunction.EXCEPTION, e.Message);
            }
        }

        public BaseReturnFunction<Boolean> UpdateLocation(string username, double lat, double lon)
        {
            //username ~ email
            try
            {
                if (String.IsNullOrWhiteSpace(username))
                    return new BaseReturnFunction<Boolean>(StatusFunction.FALSE, "Tài khoản hoặc mật khẩu là null hoặc trống/ Username or password is null or empty");

                using (var context = new UserDbDataContext())
                {
                    Database.User.User user = context.Users.SingleOrDefault(x => x.UserName != null && x.UserName.ToLower().Equals(username.ToLower()));
                    if (user != null)
                    {
                        user.Lat = lat;
                        user.Lon = lon;

                        context.SubmitChanges();
                        return new BaseReturnFunction<Boolean>(StatusFunction.TRUE, true);
                    }
                }
                return new BaseReturnFunction<Boolean>(StatusFunction.FALSE, "Tài khoản này không tồn tại/ Your account does not exist: " + username);
            }
            catch (Exception e)
            {
                return new BaseReturnFunction<Boolean>(StatusFunction.FALSE, e.Message);
            }
        }

        public BaseReturnFunction<Boolean> ShareOnlineStatusSetting(string username, bool isShared)
        {
            //username ~ email
            try
            {
                if (String.IsNullOrWhiteSpace(username))
                    return new BaseReturnFunction<Boolean>(StatusFunction.FALSE, "Tài khoản hoặc mật khẩu là null hoặc trống/ Username or password is null or empty");

                using (var context = new UserDbDataContext())
                {
                    Database.User.User user = context.Users.SingleOrDefault(x => x.UserName != null && x.UserName.ToLower().Equals(username.ToLower()));
                    if (user != null)
                    {
                        user.ShareOnlineStatus = isShared;
                        context.SubmitChanges();
                        return new BaseReturnFunction<Boolean>(StatusFunction.TRUE, true);
                    }
                }
                return new BaseReturnFunction<Boolean>(StatusFunction.FALSE, "Tài khoản này không tồn tại/ Your account does not exist: " + username);
            }
            catch (Exception e)
            {
                return new BaseReturnFunction<Boolean>(StatusFunction.EXCEPTION, e.Message);
            }
        }

        public BaseReturnFunction<Boolean> ShowAvatarSetting(string username, bool isShown)
        {
            //username ~ email
            try
            {
                if (String.IsNullOrWhiteSpace(username))
                    return new BaseReturnFunction<Boolean>(StatusFunction.FALSE, "Tài khoản hoặc mật khẩu là null hoặc trống/ Username or password is null or empty");

                using (var context = new UserDbDataContext())
                {
                    Database.User.User user = context.Users.SingleOrDefault(x => x.UserName != null && x.UserName.ToLower().Equals(username.ToLower()));
                    if (user != null)
                    {
                        user.ShowAvatar = isShown;
                        context.SubmitChanges();
                        return new BaseReturnFunction<Boolean>(StatusFunction.TRUE, true);
                    }
                }
                return new BaseReturnFunction<Boolean>(StatusFunction.FALSE, "Tài khoản này không tồn tại/ Your account does not exist: " + username);
            }
            catch (Exception e)
            {
                return new BaseReturnFunction<Boolean>(StatusFunction.EXCEPTION, e.Message);
            }
        }

        public BaseReturnFunction<Boolean> ReceiveStrangeMessageSetting(string username, bool isReceived)
        {
            //username ~ email
            try
            {
                if (String.IsNullOrWhiteSpace(username))
                    return new BaseReturnFunction<Boolean>(StatusFunction.FALSE, "Tài khoản hoặc mật khẩu là null hoặc trống/ Username or password is null or empty");

                using (var context = new UserDbDataContext())
                {
                    Database.User.User user = context.Users.SingleOrDefault(x => x.UserName != null && x.UserName.ToLower().Equals(username.ToLower()));
                    if (user != null)
                    {
                        user.ReceiveStrangeMessage = isReceived;
                        context.SubmitChanges();
                        return new BaseReturnFunction<Boolean>(StatusFunction.TRUE, true);
                    }
                }
                return new BaseReturnFunction<Boolean>(StatusFunction.FALSE, "Tài khoản này không tồn tại/ Your account does not exist: " + username);
            }
            catch (Exception e)
            {
                return new BaseReturnFunction<Boolean>(StatusFunction.EXCEPTION, e.Message);
            }
        }

        public BaseReturnFunction<Boolean> ShowYourPhoneNumberSetting(string username, bool isShown)
        {
            //username ~ email
            try
            {
                if (String.IsNullOrWhiteSpace(username))
                    return new BaseReturnFunction<Boolean>(StatusFunction.FALSE, "Tài khoản hoặc mật khẩu là null hoặc trống/ Username or password is null or empty");

                using (var context = new UserDbDataContext())
                {
                    Database.User.User user = context.Users.SingleOrDefault(x => x.UserName != null && x.UserName.ToLower().Equals(username.ToLower()));
                    if (user != null)
                    {
                        user.ShowPhoneNumber = isShown;
                        context.SubmitChanges();
                        return new BaseReturnFunction<Boolean>(StatusFunction.TRUE, true);
                    }
                }
                return new BaseReturnFunction<Boolean>(StatusFunction.FALSE, "Tài khoản này không tồn tại/ Your account does not exist: " + username);
            }
            catch (Exception e)
            {
                return new BaseReturnFunction<Boolean>(StatusFunction.EXCEPTION, e.Message);
            }
        }

        public BaseReturnFunction<Boolean> UpdateSettingAttribute(string username, int settingValue)
        {
            //username ~ email
            try
            {
                if (String.IsNullOrWhiteSpace(username))
                    return new BaseReturnFunction<Boolean>(StatusFunction.FALSE, "Tài khoản hoặc mật khẩu là null hoặc trống/ Username or password is null or empty");

                using (var context = new UserDbDataContext())
                {
                    Database.User.User user = context.Users.SingleOrDefault(x => x.UserName != null && x.UserName.ToLower().Equals(username.ToLower()));
                    if (user != null)
                    {
                        user.SettingAttribute = settingValue;
                        context.SubmitChanges();
                        return new BaseReturnFunction<Boolean>(StatusFunction.TRUE, true);
                    }
                }
                return new BaseReturnFunction<Boolean>(StatusFunction.FALSE, "Tài khoản này không tồn tại/ Your account does not exist: " + username);
            }
            catch (Exception e)
            {
                return new BaseReturnFunction<Boolean>(StatusFunction.EXCEPTION, e.Message);
            }
        }

        public BaseReturnFunction<Database.User.User> Create(Database.User.User model)
        {
            try
            {
                using (var context = new UserDbDataContext())
                {
                    var user = context.Users.SingleOrDefault(x => x.UserName == model.UserName);
                    if(user!=null)
                    {
                        return new BaseReturnFunction<Database.User.User>(StatusFunction.FALSE, "Tài khoản đã tồn tại/ Account already exists");
                    }
                    if (String.IsNullOrWhiteSpace(model.Avartar))
                        //model.Avartar = "http://onlineapi.site/static/img/default_avatar.png";
                        model.Avartar = "http://vnc.iduhoc.vn/static/img/default_avatar.png";

                    //show all by default
                    model.ShareOnlineStatus = true;
                    model.ShowAvatar = true;
                    model.ShowPhoneNumber = true;
                    model.ReceiveStrangeMessage = true;

                    //turn on getting new post (news, job, tips, friend, share your locatin) by default
                    model.SettingAttribute = 4 | 8 | 16 | 32 | 64;//download media file via 4G and wifi is turned off. See FlagAttributeUtils class in Android Studio

                    context.Users.InsertOnSubmit(model);

                    if (IsValidEmail(model.UserName))
                    {
                        string subject = @"Thông báo kích hoạt tài khoản iDuhoc";

                        var content = System.IO.File.ReadAllText(HostingEnvironment.MapPath(@"~/App_Data/template/activate_email_vi.txt"));
                        String token = (DateTime.Now.ToString() + model.UserName).GenerateStringToMD5();
                        model.ActivateToken = token;
                        if (UtilitiesHelper.SendMail2(model.UserName, subject, String.Format(content, model.FullName, token)))
                        {
                            model.IsActived = 1;
                            context.SubmitChanges();
                        }
                    }
                    else
                    {
                        context.SubmitChanges();
                    }
                }
                return new BaseReturnFunction<Database.User.User>(StatusFunction.TRUE, model);
            }
            catch (Exception e)
            {
                return new BaseReturnFunction<Database.User.User>(StatusFunction.EXCEPTION, e.Message);
            }
        }
        bool IsValidEmail(string email)
        {
            try
            {
                var addr = new System.Net.Mail.MailAddress(email);
                return addr.Address == email;
            }
            catch
            {
                return false;
            }
        }
        public BaseReturnFunction<Database.User.User> Update(string username, Database.User.User model)
        {
            //username ~ email
            try
            {
                if (String.IsNullOrWhiteSpace(username) || String.IsNullOrEmpty(model.UserName))
                    return new BaseReturnFunction<Database.User.User>(StatusFunction.FALSE, "Tài khoản hoặc mật khẩu là null hoặc trống/ Username or password is null or empty");

                if (!username.ToLower().Equals(model.UserName.ToLower()))
                    return new BaseReturnFunction<Database.User.User>(StatusFunction.FALSE, "Chỉ chủ tài khoản mới có quyền thay đổi thông tin/ Only account holders have the right to change information.");

                using (var context = new UserDbDataContext())
                {
                    Database.User.User user = context.Users.SingleOrDefault(x => x.UserName != null && x.UserName.ToLower().Equals(username.ToLower()));
                    if (user != null)
                    {
                        user.Nationality = model.Nationality;
                        user.Birthday = model.Birthday;
                        user.Gender = model.Gender;
                        user.CurrentLivingCity = model.CurrentLivingCity;
                        user.TypeJob = model.TypeJob;
                        user.Job = model.Job;
                        user.PhoneNumber = model.PhoneNumber;
                        user.Introduction = model.Introduction;

                        user.FullName = model.FullName;
                        user.Avartar = model.Avartar;

                        user.LatAddress = model.LatAddress;
                        user.LonAddress = model.LonAddress;

                        context.SubmitChanges();
                        user.LoginToken = TokenHelper.GenerateToken(user.UserName);
                        return new BaseReturnFunction<Database.User.User>(StatusFunction.TRUE, user);
                    }
                }
                return new BaseReturnFunction<Database.User.User>(StatusFunction.FALSE, "Tài khoản này không tồn tại/ Your account does not exist: " + username);
            }
            catch (Exception e)
            {
                return new BaseReturnFunction<Database.User.User>(StatusFunction.FALSE, e.Message);
            }
        }

        public BaseReturnFunction<Database.User.User> ChangePassword(string username, string password)
        {
            //username ~ email
            try
            {
                if (String.IsNullOrWhiteSpace(username))
                    return new BaseReturnFunction<Database.User.User>(StatusFunction.FALSE, "Tài khoản hoặc mật khẩu là null hoặc trống/ Username or password is null or empty");

                using (var context = new UserDbDataContext())
                {
                    Database.User.User user = context.Users.SingleOrDefault(x => x.UserName != null && x.UserName.ToLower().Equals(username.ToLower()));
                    if (user != null)
                    {
                        user.Password = password;

                        context.SubmitChanges();
                        user.LoginToken = TokenHelper.GenerateToken(user.UserName);
                        return new BaseReturnFunction<Database.User.User>(StatusFunction.TRUE, user);
                    }
                }
                return new BaseReturnFunction<Database.User.User>(StatusFunction.FALSE, "Tài khoản này không tồn tại/ Your account does not exist: " + username);
            }
            catch (Exception e)
            {
                return new BaseReturnFunction<Database.User.User>(StatusFunction.FALSE, e.Message);
            }
        }

        public BaseReturnFunction<Object> FindFriendAround(String userId, double latitude, double longitude, int page, int size, int range_age, int gender, int only_friend, int max_distance)
        {
            try
            {
                GeoCoordinate location = new GeoCoordinate(latitude, longitude);
                using (var context = new FriendDbDataContext())
                {
                    IQueryable<Database.User.FriendDb.User> users = null;
                    if (only_friend == 1)
                        users = context.Users.Where(x => x.UserName != userId && x.Lat.HasValue && x.Lon.HasValue && x.Lat.Value != 0 && x.Lon.Value != 0 && x.Friends.FirstOrDefault(y => (y.UserId == userId || y.FriendId == userId) && y.IsAccepted) != null);
                    else
                        users = context.Users.Where(x => x.UserName != userId && x.Lat.HasValue && x.Lon.HasValue && x.Lat.Value != 0 && x.Lon.Value != 0);
                    if (gender != 0)
                    {
                        if (gender == 2)
                            gender = 0;
                        users = users.Where(x => x.Gender == gender);
                    }
                    var user2 = users.ToList();
                    if (range_age != 0)
                    {
                        int year = DateTime.Now.Year;
                        user2 = user2.FindAll(x => x.Birthday.HasValue && inRangeAge(range_age, x.Birthday.Value, year));
                    }
                    if (max_distance == 0)
                    {
                        var users2 = user2.Select(x => new
                        {
                            User = new
                            {
                                x.Gender,
                                x.UserName,
                                x.FullName,
                                x.Birthday,
                                x.Avartar,
                                x.PhoneNumber,
                                x.Lat,
                                x.Lon,
                                x.Introduction,
                                x.SettingAttribute,
                                x.ReceiveStrangeMessage,
                                x.ShowAvatar,
                                x.ShowPhoneNumber,
                                isFriend = (x.Friends.SingleOrDefault(y => y.IsAccepted && (y.FriendId == userId || y.UserId == userId)) != null || x.Friends1.SingleOrDefault(y => y.IsAccepted && (y.FriendId == userId || y.UserId == userId)) != null)

                            },
                            Distance = (x.Lon == null || x.Lat == null) ? double.MaxValue : location.GetDistanceTo(new GeoCoordinate(x.Lat.Value, x.Lon.Value)),
                        }).OrderBy(k => k.Distance).Skip((page - 1) * size).Take(size);
                        return new BaseReturnFunction<Object>(StatusFunction.TRUE, users2.ToList());
                    }
                    else
                    {
                        max_distance *= 1000;
                        var users2 = user2.Select(x => new
                        {
                            User = new
                            {
                                x.Gender,
                                x.UserName,
                                x.FullName,
                                x.Birthday,
                                x.Avartar,
                                x.PhoneNumber,
                                x.Lat,
                                x.Lon,
                                x.Introduction,
                                x.SettingAttribute,
                                x.ShowAvatar,
                                x.ReceiveStrangeMessage,
                                x.ShowPhoneNumber,
                                isFriend = (x.Friends.SingleOrDefault(y => y.IsAccepted && (y.FriendId == userId || y.UserId == userId)) != null || x.Friends1.SingleOrDefault(y => y.IsAccepted && (y.FriendId == userId || y.UserId == userId)) != null)
                            },
                            Distance = (x.Lon == null || x.Lat == null) ? double.MaxValue : location.GetDistanceTo(new GeoCoordinate(x.Lat.Value, x.Lon.Value)),
                        }).Where(x => x.Distance <= max_distance).OrderBy(k => k.Distance).Skip((page - 1) * size).Take(size);
                        return new BaseReturnFunction<Object>(StatusFunction.TRUE, users2.ToList());
                    }
                }
            }
            catch (Exception e)
            {
                return new BaseReturnFunction<object>(StatusFunction.EXCEPTION, e.Message);
            }
        }
        bool inRangeAge(int range, double bod, int year)
        {
            int age = year - bod.UnixTimeStampToDateTime().Year;
            if (age == 0)
                age = 1;
            switch (range)
            {
                case 1:
                    return age <= 20;
                case 2:
                    return 20 <= age && age <= 40;
                case 3:
                    return age >= 40;
            }
            return true;
        }

        public BaseReturnFunction<List<Database.User.User>> Sync(double time)
        {
            throw new NotImplementedException();
        }

        public BaseReturnFunction<Database.User.User> LoginSocial(SocialType socialType, string socialId, string fullName, int gender, string avatar, long lat, long lon)
        {
            try
            {
                using (var context = new UserDbDataContext())
                {
                    var user = context.Users.SingleOrDefault(x => x.SocialId == socialId);
                    if (user == null)
                    {
                        user = new Database.User.User()
                        {
                            SocialId = socialId,
                            Avartar = avatar,
                            FullName = fullName,
                            UserName = socialType.ToString() + socialId,
                            LatestLogin = DateTime.Now.GetUnixTimeStamp(),
                            SettingAttribute = 4 | 8 | 16
                        };
                        if (lat != 0 && lon != 0)
                        {
                            user.Lat = lat;
                            user.Lon = lon;
                        }
                        //user.LoginToken = TokenHelper.GenerateToken(user.UserName);
                        context.Users.InsertOnSubmit(user);
                        context.SubmitChanges();
                    }
                    else
                    {
                        if (lat != 0 && lon != 0)
                        {
                            user.Lat = lat;
                            user.Lon = lon;
                        }
                        //user.LoginToken = TokenHelper.GenerateToken(user.UserName);
                        context.SubmitChanges();
                    }
                    user.LoginToken = TokenHelper.GenerateToken(user.UserName);
                    return new BaseReturnFunction<Database.User.User>(StatusFunction.TRUE, user);
                }
            }
            catch (Exception e)
            {
                return new BaseReturnFunction<Database.User.User>(StatusFunction.EXCEPTION, e.Message);
            }
        }

    }
}