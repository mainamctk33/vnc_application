﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using VNCApp.Models;

namespace VNCApp.DataAccess
{
    /// <summary>
    /// Inteface cung cấp mô hình provider truy cập data
    /// </summary>
    /// <typeparam name="T">Bảng trong db</typeparam>
    /// <typeparam name="E">E kiểu khóa chính</typeparam>
    public interface IProvider<T, E> where T: new()
    {
        BaseReturnFunction<List<T>> GetAll();
        BaseReturnFunction<Boolean> Delete(E id);
        BaseReturnFunction<T> GetById(E id);
        BaseReturnFunction<T> Create(T model);
        BaseReturnFunction<T> Update(E id, T model);
        BaseReturnFunction<List<T>> Sync(double time);
    }
}