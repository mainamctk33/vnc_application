﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using VNCApp.Database.Tips.TypeTips;
using VNCApp.Models;

namespace VNCApp.DataAccess.Tips
{
    public class TypeTipsProvider : IProvider<TypeTip, int>
    {

        public BaseReturnFunction<List<TypeTip>> GetAll()
        {
            try
            {
                using (var context = new TypeTipsDbDataContext())
                {
                    var list = context.TypeTips;
                    return new BaseReturnFunction<List<TypeTip>>(StatusFunction.TRUE, list.ToList(), list.Count());
                }
            }
            catch (Exception e)
            {
                return new BaseReturnFunction<List<TypeTip>>(StatusFunction.EXCEPTION, e.Message);
            }
        }

        public BaseReturnFunction<Boolean> Delete(int id)
        {
            try
            {
                using (var context = new TypeTipsDbDataContext())
                {
                    var cat = context.TypeTips.SingleOrDefault(x => x.ID == id);
                    if (cat != null)
                    {
                        context.TypeTips.DeleteOnSubmit(cat);
                        context.SubmitChanges();
                    }
                }
                return new BaseReturnFunction<bool>(StatusFunction.TRUE, true);
            }
            catch (Exception e)
            {
                return new BaseReturnFunction<bool>(StatusFunction.EXCEPTION, e.Message);
            }
        }

        public BaseReturnFunction<TypeTip> GetById(int id)
        {
            try
            {
                using (var context = new TypeTipsDbDataContext())
                {
                    var cat = context.TypeTips.SingleOrDefault(x => x.ID == id);
                    if (cat != null)
                        return new BaseReturnFunction<TypeTip>(StatusFunction.TRUE, cat);
                    return new BaseReturnFunction<TypeTip>(StatusFunction.TRUE, "Không tồn tại chuyên mục với id = " + id);
                }
            }
            catch (Exception e)
            {
                return new BaseReturnFunction<TypeTip>(StatusFunction.EXCEPTION, e.Message);
            }
        }

        public BaseReturnFunction<TypeTip> Create(TypeTip model)
        {
            try
            {
                using (var context = new TypeTipsDbDataContext())
                {
                    context.TypeTips.InsertOnSubmit(model);
                    context.SubmitChanges();
                }
                return new BaseReturnFunction<TypeTip>(StatusFunction.TRUE, model);
            }
            catch (Exception e)
            {
                return new BaseReturnFunction<TypeTip>(StatusFunction.EXCEPTION, e.Message);
            }
        }

        public BaseReturnFunction<TypeTip> Update(int id, TypeTip model)
        {
            try
            {
                using (var context = new TypeTipsDbDataContext())
                {
                    TypeTip TypeTip = context.TypeTips.SingleOrDefault(x => x.ID == id);
                    if (TypeTip != null)
                    {
                        TypeTip.Name = model.Name;
                        TypeTip.Name2 = model.Name2;
                        context.SubmitChanges();
                        return new BaseReturnFunction<TypeTip>(StatusFunction.TRUE, TypeTip);
                    }
                }
                return new BaseReturnFunction<TypeTip>(StatusFunction.FALSE, "Không tồn tại chuyên mục với id = " + id);
            }
            catch (Exception e)
            {
                return new BaseReturnFunction<TypeTip>(StatusFunction.EXCEPTION, e.Message);
            }
        }

        public BaseReturnFunction<List<TypeTip>> Sync(double time)
        {
            try
            {
                using (var context = new TypeTipsDbDataContext())
                {
                    return new BaseReturnFunction<List<TypeTip>>(StatusFunction.TRUE, context.TypeTips.ToList());
                }
            }
            catch (Exception e)
            {
                return new BaseReturnFunction<List<TypeTip>>(StatusFunction.EXCEPTION, e.Message);
            }
        }
        public BaseReturnFunction<List<TypeTip>> Search(int page, int size, string keyword)
        {
            try
            {
                using (var context = new TypeTipsDbDataContext())
                {
                    IQueryable<TypeTip> list;
                    if (String.IsNullOrWhiteSpace(keyword))
                        list = context.TypeTips;
                    else
                    {
                        
                        keyword = keyword.Trim().ToLower();
                        list = context.TypeTips.Where(x => x.Name.ToLower().Contains(keyword) || x.Name2.ToLower().Contains(keyword));
                    }
                    return new BaseReturnFunction<List<TypeTip>>(StatusFunction.TRUE, list.Skip((page - 1) * size).Take(size).ToList(), list.Count());
                }
            }
            catch (Exception e)
            {
                return new BaseReturnFunction<List<TypeTip>>(StatusFunction.EXCEPTION, e.Message);
            }
        }

    }
}