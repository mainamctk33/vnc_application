﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BaseLibrary.Common;
using VNCApp.Database.Tips.Tips;
using VNCApp.Models;

namespace VNCApp.DataAccess.Tips
{
    public class TipsProvider : IProvider<Tip, int>
    {
        public BaseReturnFunction<List<Tip>> GetAll(int page, int size, int type, String keyword, String userId)
        {
            try
            {
                using (var context = new TipsDbDataContext())
                {
                    IQueryable<Tip> list;
                    //type = -2 ~ get tips for tab 'All'
                    if (type == -2)
                        list = context.Tips;
                    else if (type == -1)
                        if (!String.IsNullOrWhiteSpace(userId))
                            list = context.BookmarkTips.Where(x => x.BookmarkBy == userId).OrderBy(x => x.BookmarkDate).Select(x => x.Tip);
                        else
                            list = context.Tips;
                    else
                        list = context.Tips.Where(x => type == 0 || x.Type == type);

                    list = list.Where(x => String.IsNullOrWhiteSpace(keyword) ||
                        (x.Title != null && x.Title.ToLower().Contains(keyword.ToLower())) ||
                        (x.Content != null && x.Content.ToLower().Contains(keyword.ToLower()))
                        ).OrderByDescending(x => x.CreatedDate);

                    int count = list.Count();
                    return new BaseReturnFunction<List<Tip>>(StatusFunction.TRUE,
                        list.Skip((page - 1) * size).Take(size).ToList(), count);
                }
            }
            catch (Exception e)
            {
                return new BaseReturnFunction<List<Tip>>(StatusFunction.EXCEPTION, e.Message);
            }
        }

        public BaseReturnFunction<List<Tip>> GetAll()
        {
            throw new NotImplementedException();
        }

        public BaseReturnFunction<bool> Delete(int id)
        {
            try
            {
                using (var context = new TipsDbDataContext())
                {
                    var tips = context.Tips.SingleOrDefault(x => x.ID == id);
                    if (tips != null)
                    {
                        context.Tips.DeleteOnSubmit(tips);
                        context.SubmitChanges();
                    }
                }
                return new BaseReturnFunction<bool>(StatusFunction.TRUE, true);
            }
            catch (Exception e)
            {
                return new BaseReturnFunction<bool>(StatusFunction.EXCEPTION, e.Message);
            }
        }

        public BaseReturnFunction<Tip> GetById(int id)
        {
            try
            {
                using (var context = new TipsDbDataContext())
                {
                    var tips = context.Tips.SingleOrDefault(x => x.ID == id);
                    if (tips != null)
                    {
                        return new BaseReturnFunction<Tip>(StatusFunction.TRUE, tips);
                    }
                    return new BaseReturnFunction<Tip>(StatusFunction.FALSE, "Không tồn tại lời khuyên với id = " + id);
                }
            }
            catch (Exception e)
            {
                return new BaseReturnFunction<Tip>(StatusFunction.EXCEPTION, e.Message);
            }
        }

        public BaseReturnFunction<Tip> Create(Tip model)
        {
            try
            {
                if (model.Title == null || model.Content == null || model.Thumbnail == null)
                    return new BaseReturnFunction<Tip>(StatusFunction.FALSE, "Some information is missing / Một số thông tin đang bị thiếu");

                using (var context = new TipsDbDataContext())
                {
                    IQueryable<Tip> listTips;
                    listTips = context.Tips.Where(x => x.Title.ToLower().Equals(model.Title.ToLower()) && x.Content.ToLower().Equals(model.Content.ToLower()));
                    if (listTips.Any())
                    {
                        return new BaseReturnFunction<Tip>(StatusFunction.FALSE, "This tips already exists / Lời khuyên này đã tồn tại");
                    }
                    else
                    {
                        context.Tips.InsertOnSubmit(model);
                        context.SubmitChanges();
                        return new BaseReturnFunction<Tip>(StatusFunction.TRUE, model);
                    }
                }
            }
            catch (Exception e)
            {
                return new BaseReturnFunction<Tip>(StatusFunction.EXCEPTION, e.Message);
            }
        }

        public BaseReturnFunction<Tip> Update(int id, Tip model)
        {
            try
            {
                using (var context = new TipsDbDataContext())
                {
                    Tip tips = context.Tips.SingleOrDefault(x => x.ID == id);
                    if (tips != null)
                    {
                        tips.Content = model.Content;
                        tips.CreatedBy = model.CreatedBy;
                        tips.CreatedDate = model.CreatedDate;
                        tips.SubContent = model.SubContent;
                        tips.Thumbnail = model.Thumbnail;
                        tips.Title = model.Title;
                        tips.Type = model.Type;
                        tips.UpdatedDate = DateTime.Now.GetUnixTimeStamp();
                        context.SubmitChanges();
                        return new BaseReturnFunction<Tip>(StatusFunction.TRUE, tips);
                    }
                }

                return new BaseReturnFunction<Tip>(StatusFunction.FALSE, "Không tồn tại lời khuyên với id = " + id);
            }
            catch (Exception e)
            {
                return new BaseReturnFunction<Tip>(StatusFunction.EXCEPTION, e.Message);
            }
        }

        public BaseReturnFunction<List<Tip>> Sync(double time)
        {
            throw new NotImplementedException();
        }

        public BaseReturnFunction<Boolean> Bookmark(int id, bool bookmark, string requestUserId)
        {
            try
            {

                using (var context = new TipsDbDataContext())
                {
                    //bookmark=false ~ user don't want to bookmark exited tips > delete this bookmark
                    if (!bookmark)
                    {
                        var bm = context.BookmarkTips.SingleOrDefault(x => x.BookmarkBy == requestUserId && x.ID == id);
                        if (bm != null)
                        {
                            context.BookmarkTips.DeleteOnSubmit(bm);
                            context.SubmitChanges();
                        }
                        return new BaseReturnFunction<bool>(StatusFunction.TRUE, true);
                    }
                    else
                    //bookmark=true ~ user want to bookmark > create new bookmark
                    {
                        context.BookmarkTips.InsertOnSubmit(new BookmarkTip()
                        {
                            ID = id,
                            BookmarkBy = requestUserId,
                            BookmarkDate = DateTime.Now.GetUnixTimeStamp()
                        });
                        context.SubmitChanges();
                        return new BaseReturnFunction<bool>(StatusFunction.TRUE, true);
                    }
                }
            }
            catch (Exception e)
            {
                return new BaseReturnFunction<bool>(StatusFunction.EXCEPTION, e.Message);
            }
        }
    }
}