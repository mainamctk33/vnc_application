﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using VNCApp.Database.Location.LocationDb;
using VNCApp.Models;
using BaseLibrary.Common;
using VNCApp.Database.Location.LocationBookmark;
using System.Dynamic;

namespace VNCApp.DataAccess.Location
{
    public class LocationProvider : IProvider<Database.Location.LocationDb.Location, int>
    {
        public BaseReturnFunction<Boolean> Create2(Database.Location.LocationDb.Location model)
        {
            try
            {
                if (model == null || model.Contact == null || model.Title == null || model.Address == null)
                {
                    return new BaseReturnFunction<Boolean>(StatusFunction.FALSE, "Miss some information of this location/Thông tin về địa điểm này bị thiếu");
                }

                using (var context = new LocationDbDataContext())
                {
                    //context.Locations.InsertOnSubmit(model);
                    //context.SubmitChanges();
                    //return new BaseReturnFunction<Boolean>(StatusFunction.TRUE, true);

                    IQueryable<Database.Location.LocationDb.Location> listLocations;
                    //listLocations = context.Locations.Where(x => x.Title != null && x.Contact != null && x.Address != null && x.Title.Equals(model.Title) && x.Contact.Equals(model.Contact) && x.Address.Equals(model.Address));
                    listLocations = context.Locations.Where(x => x.Title != null && x.Title.Equals(model.Title) && x.Type == model.Type && x.Lat == model.Lat && x.Lon == model.Lon);
                    if (!listLocations.Any())
                    {
                        context.Locations.InsertOnSubmit(model);
                        context.SubmitChanges();
                        return new BaseReturnFunction<Boolean>(StatusFunction.TRUE, true);
                    }
                    else
                    {
                        return new BaseReturnFunction<Boolean>(StatusFunction.FALSE, "This location already exists/Địa điểm này đã tồn tại");
                    }
                }
            }
            catch (Exception e)
            {
                return new BaseReturnFunction<Boolean>(StatusFunction.EXCEPTION, e.Message);
            }
        }

        public BaseReturnFunction<Database.Location.LocationDb.Location> Create(Database.Location.LocationDb.Location model)
        {
            try
            {
                using (var context = new LocationDbDataContext())
                {
                    context.Locations.InsertOnSubmit(model);
                    context.SubmitChanges();
                    return new BaseReturnFunction<Database.Location.LocationDb.Location>(StatusFunction.TRUE, model);
                }
            }
            catch (Exception e)
            {
                return new BaseReturnFunction<Database.Location.LocationDb.Location>(StatusFunction.EXCEPTION, e.Message);
            }
        }

        public BaseReturnFunction<Boolean> Approve(string requestUserId, int id, bool approve)
        {
            try
            {
                using (var context = new LocationDbDataContext())
                {
                    var location = context.Locations.FirstOrDefault(x => x.ID == id);
                    if (location != null)
                    {
                        if (approve)
                        {
                            location.ApproveBy = "mainam";
                            location.ApproveDate = DateTime.Now.GetUnixTimeStamp();
                        }
                        else
                        {
                            location.ApproveBy = null;
                            location.ApproveDate = null;
                        }
                        location.UpdatedDate = DateTime.Now.GetUnixTimeStamp();
                        context.SubmitChanges();
                        return new BaseReturnFunction<Boolean>(StatusFunction.TRUE, true);
                    }
                    return new BaseReturnFunction<bool>(StatusFunction.FALSE, false);
                }

            }
            catch (Exception e)
            {
                return new BaseReturnFunction<bool>(StatusFunction.EXCEPTION, e.Message);

            }
        }


        public BaseReturnFunction<bool> Delete(int id)
        {
            try
            {
                using (var context = new LocationDbDataContext())
                {
                    var location = context.Locations.FirstOrDefault(x => x.ID == id);
                    if (location != null)
                    {
                        context.Locations.DeleteOnSubmit(location);
                        context.SubmitChanges();
                        return new BaseReturnFunction<bool>(StatusFunction.TRUE, true);
                    }
                    return new BaseReturnFunction<bool>(StatusFunction.FALSE, false);
                }
            }
            catch (Exception e)
            {
                return new BaseReturnFunction<bool>(StatusFunction.EXCEPTION, e.Message);
                throw;
            }
        }


        public BaseReturnFunction<Boolean> Bookmark(int id, Boolean bookmark, String requestUserId)
        {
            try
            {
                using (var context = new LocationBookmarkDataContext())
                {
                    if (!bookmark)
                    {
                        var bm = context.BookmarkLocations.SingleOrDefault(x => x.BookmarkBy == requestUserId && x.ID == id);
                        if (bm != null)
                        {
                            context.BookmarkLocations.DeleteOnSubmit(bm);
                            context.SubmitChanges();
                        }
                        return new BaseReturnFunction<bool>(StatusFunction.TRUE, true);
                    }
                    else
                    {
                        context.BookmarkLocations.InsertOnSubmit(new BookmarkLocation()
                        {
                            ID = id,
                            BookmarkBy = requestUserId,
                            BookmarkDate = DateTime.Now.GetUnixTimeStamp()
                        });
                        context.SubmitChanges();
                        return new BaseReturnFunction<bool>(StatusFunction.TRUE, true);
                    }
                }
            }
            catch (Exception e)
            {
                return new BaseReturnFunction<bool>(StatusFunction.EXCEPTION, e.Message);
            }
        }

        public BaseReturnFunction<dynamic> MyRating(int id, String requestUserId)
        {
            try
            {
                using (var context = new VNCApp.Database.Location.Location.LocationDbDataContext())
                {
                    var myRate = context.UserRateLocations.SingleOrDefault(x => x.UserName == requestUserId && x.LocationId == id);
                    if (myRate != null)
                        return new BaseReturnFunction<dynamic>(StatusFunction.TRUE, new { myRate.Value, myRate.Comment });
                    return new BaseReturnFunction<dynamic>(StatusFunction.FALSE, new ExpandoObject());
                }
            }
            catch (Exception e)
            {
                return new BaseReturnFunction<dynamic>(StatusFunction.FALSE, e.Message);
            }
        }


        public BaseReturnFunction<Boolean> IsBookmark(int id, String requestUserId)
        {
            try
            {
                using (var context = new LocationBookmarkDataContext())
                {
                    var bm = context.BookmarkLocations.SingleOrDefault(x => x.BookmarkBy == requestUserId && x.ID == id);
                    if (bm != null)
                    {
                        return new BaseReturnFunction<bool>(StatusFunction.TRUE, true);
                    }
                    return new BaseReturnFunction<bool>(StatusFunction.TRUE, false);
                }
            }
            catch (Exception e)
            {
                return new BaseReturnFunction<bool>(StatusFunction.EXCEPTION, e.Message);
            }
        }



        public BaseReturnFunction<Database.Location.LocationDb.Location> GetById(int id)
        {
            try
            {
                using (var context = new LocationDbDataContext())
                {
                    var list = context.Locations.SingleOrDefault(x => x.ID == id);
                    if (list != null)
                    {
                        return new BaseReturnFunction<Database.Location.LocationDb.Location>(StatusFunction.TRUE, list);
                    }
                    return new BaseReturnFunction<Database.Location.LocationDb.Location>(StatusFunction.FALSE, "Không tồn tại lời khuyên với id = " + id);
                }
            }
            catch (Exception e)
            {
                return new BaseReturnFunction<Database.Location.LocationDb.Location>(StatusFunction.EXCEPTION, e.Message);
            }
        }

        public BaseReturnFunction<List<Database.Location.LocationDb.Location>> Sync(double time)
        {
            throw new NotImplementedException();
        }

        public BaseReturnFunction<Database.Location.LocationDb.Location> Update(int id, Database.Location.LocationDb.Location model)
        {
            try
            {
                using (var context = new LocationDbDataContext())
                {
                    Database.Location.LocationDb.Location data = context.Locations.SingleOrDefault(x => x.ID == id);
                    if (data != null)
                    {
                        data.Address = model.Address;
                        data.ApproveBy = data.ApproveBy;
                        data.ApproveDate = model.ApproveDate;
                        data.Contact = data.Contact;
                        data.Content = model.Content;
                        data.CreatedBy = model.CreatedBy;
                        data.CreatedDate = model.CreatedDate;
                        data.Lat = model.Lat;
                        data.Lon = model.Lon;
                        data.Rate = model.Rate;
                        data.Thumbnail = model.Thumbnail;
                        data.Title = model.Title;
                        data.TotalMemberRate = model.TotalMemberRate;
                        data.Type = model.Type;
                        data.TypeLocation = model.TypeLocation;
                        data.UpdatedDate = DateTime.Now.GetUnixTimeStamp();
                        context.SubmitChanges();
                        return new BaseReturnFunction<Database.Location.LocationDb.Location>(StatusFunction.TRUE, data);
                    }
                }

                return new BaseReturnFunction<Database.Location.LocationDb.Location>(StatusFunction.FALSE, "Không tồn tại lời khuyên với id = " + id);
            }
            catch (Exception e)
            {
                return new BaseReturnFunction<Database.Location.LocationDb.Location>(StatusFunction.EXCEPTION, e.Message);
            }
        }

        public BaseReturnFunction<List<Database.Location.LocationDb.Location>> GetAll()
        {
            try
            {
                using (var context = new LocationDbDataContext())
                {
                    IQueryable<Database.Location.LocationDb.Location> list = context.Locations;
                    return new BaseReturnFunction<List<Database.Location.LocationDb.Location>>(StatusFunction.TRUE, list.OrderByDescending(x => x.CreatedDate).OrderByDescending(x => x.ApproveDate).ToList(), list.Count());
                }
            }
            catch (Exception e)
            {
                return new BaseReturnFunction<List<Database.Location.LocationDb.Location>>(StatusFunction.EXCEPTION, e.Message);
            }
        }

        public int GetPending()
        {
            try
            {
                using (var context = new LocationDbDataContext())
                {

                    return context.Locations.Where(x => x.ApproveDate == null).ToList().Count();
                }
            }
            catch (Exception e)
            {
                return 0;
            }
        }
    }
}