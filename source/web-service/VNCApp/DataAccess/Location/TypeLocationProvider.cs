﻿using System;
using System.Collections.Generic;
using System.Linq;
using VNCApp.Models;
using VNCApp.Database.Location.TypeLocation;

namespace VNCApp.DataAccess
{
    public class TypeLocationProvider : IProvider<TypeLocation, int>
    {
        public BaseReturnFunction<List<TypeLocation>> GetAll()
        {
            try
            {
                using (var context = new TypeLocationDbDataContext())
                {
                    var list = context.TypeLocations;
                    return new BaseReturnFunction<List<TypeLocation>>(StatusFunction.TRUE, list.ToList(), list.Count());
                }
            }
            catch (Exception e)
            {
                return new BaseReturnFunction<List<TypeLocation>>(StatusFunction.EXCEPTION, e.Message);
            }
        }

        public BaseReturnFunction<List<TypeLocation>> Search(int page, int size, string keyword)
        {
            try
            {
                using (var context = new TypeLocationDbDataContext())
                {
                    IQueryable<TypeLocation> list;
                    if (String.IsNullOrWhiteSpace(keyword))
                        list = context.TypeLocations;
                    else
                    {
                        keyword = keyword.Trim().ToLower();
                        list = context.TypeLocations.Where(x => x.Name.ToLower().Contains(keyword));
                    }
                    return new BaseReturnFunction<List<TypeLocation>>(StatusFunction.TRUE, list.Skip((page - 1) * size).Take(size).ToList(), list.Count());
                }
            }
            catch (Exception e)
            {
                return new BaseReturnFunction<List<TypeLocation>>(StatusFunction.EXCEPTION, e.Message);
            }
        }
        public BaseReturnFunction<Boolean> Delete(int id)
        {
            try
            {
                using (var context = new TypeLocationDbDataContext())
                {
                    var cat = context.TypeLocations.SingleOrDefault(x => x.ID == id);
                    if (cat != null)
                    {
                        context.TypeLocations.DeleteOnSubmit(cat);
                        context.SubmitChanges();
                    }
                }
                return new BaseReturnFunction<bool>(StatusFunction.TRUE, true);
            }
            catch (Exception e)
            {
                return new BaseReturnFunction<bool>(StatusFunction.EXCEPTION, e.Message);
            }
        }

        public BaseReturnFunction<TypeLocation> GetById(int id)
        {
            try
            {
                using (var context = new TypeLocationDbDataContext())
                {
                    var cat = context.TypeLocations.SingleOrDefault(x => x.ID == id);
                    if (cat != null)
                        return new BaseReturnFunction<TypeLocation>(StatusFunction.TRUE, cat);
                    return new BaseReturnFunction<TypeLocation>(StatusFunction.TRUE, "Không tồn tại chuyên mục với id = " + id);
                }
            }
            catch (Exception e)
            {
                return new BaseReturnFunction<TypeLocation>(StatusFunction.EXCEPTION, e.Message);
            }
        }

        public BaseReturnFunction<TypeLocation> Create(TypeLocation model)
        {
            try
            {
                using (var context = new TypeLocationDbDataContext())
                {
                    context.TypeLocations.InsertOnSubmit(model);
                    context.SubmitChanges();
                }
                return new BaseReturnFunction<TypeLocation>(StatusFunction.TRUE, model);
            }
            catch (Exception e)
            {
                return new BaseReturnFunction<TypeLocation>(StatusFunction.EXCEPTION, e.Message);
            }
        }

        public BaseReturnFunction<TypeLocation> Update(int id, TypeLocation model)
        {
            try
            {
                using (var context = new TypeLocationDbDataContext())
                {
                    TypeLocation TypeLocation = context.TypeLocations.SingleOrDefault(x => x.ID == id);
                    if (TypeLocation != null)
                    {
                        TypeLocation.Name = model.Name;
                        TypeLocation.EnName = model.EnName;
                        TypeLocation.Logo = model.Logo;
                        context.SubmitChanges();
                        return new BaseReturnFunction<TypeLocation>(StatusFunction.TRUE, TypeLocation);
                    }
                }
                return new BaseReturnFunction<TypeLocation>(StatusFunction.FALSE, "Không tồn tại chuyên mục với id = " + id);
            }
            catch (Exception e)
            {
                return new BaseReturnFunction<TypeLocation>(StatusFunction.EXCEPTION, e.Message);
            }
        }

        public BaseReturnFunction<List<TypeLocation>> Sync(double time)
        {
            try
            {
                using (var context = new TypeLocationDbDataContext())
                {
                    return new BaseReturnFunction<List<TypeLocation>>(StatusFunction.TRUE, context.TypeLocations.ToList());
                }
            }
            catch (Exception e)
            {
                return new BaseReturnFunction<List<TypeLocation>>(StatusFunction.EXCEPTION, e.Message);
            }
        }

    }
}