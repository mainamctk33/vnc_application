﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using VNCApp.Database.Jobs.CompanyJob;
using VNCApp.Models;

namespace VNCApp.DataAccess.Jobs
{
    public class CompanyJobProvider : IProvider<CompanyJob, int>
    {
        public BaseReturnFunction<CompanyJob> Create(CompanyJob model)
        {
            try
            {
                using (var context = new CompanyJobDbDataContext())
                {
                    context.CompanyJobs.InsertOnSubmit(model);
                    context.SubmitChanges();
                }
                return new BaseReturnFunction<CompanyJob>(StatusFunction.TRUE, model);
            }
            catch (Exception e)
            {
                return new BaseReturnFunction<CompanyJob>(StatusFunction.EXCEPTION, e.Message);
            }
        }
        public BaseReturnFunction<bool> Delete(int id)
        {
            try
            {
                using (var context = new CompanyJobDbDataContext())
                {
                    var data = context.CompanyJobs.SingleOrDefault(x => x.ID == id);
                    if (data != null)
                    {
                        context.CompanyJobs.DeleteOnSubmit(data);
                        context.SubmitChanges();
                    }
                    else
                        return new BaseReturnFunction<bool>(StatusFunction.FALSE, "Không tồn tại id = " + id);
                }
                return new BaseReturnFunction<bool>(StatusFunction.TRUE, true);
            }
            catch (Exception e)
            {

                return new BaseReturnFunction<bool>(StatusFunction.EXCEPTION, e.Message);
            }
        }
        public BaseReturnFunction<List<CompanyJob>> GetAll()
        {
            try
            {
                using (var context = new CompanyJobDbDataContext())
                {
                    var list = context.CompanyJobs;
                    int count = list.Count();
                    return new BaseReturnFunction<List<CompanyJob>>(StatusFunction.TRUE, list.OrderBy(x=>x.Name).ToList(), count);
                }
            }
            catch (Exception)
            {

                throw;
            }
        }
        public BaseReturnFunction<List<CompanyJob>> GetAll(int page, int size)
        {
            try
            {
                using (var context = new CompanyJobDbDataContext())
                {
                    var list = context.CompanyJobs;
                    int count = list.Count();
                    return new BaseReturnFunction<List<CompanyJob>>(StatusFunction.TRUE,
                        list.OrderBy(x => x.Name).Skip((page - 1) * size).Take(size).ToList(), count);
                }
            }
            catch (Exception)
            {

                throw;
            }
        }
        public BaseReturnFunction<CompanyJob> GetById(int id)
        {
            try
            {
                using (var context = new CompanyJobDbDataContext())
                {
                    var data = context.CompanyJobs.SingleOrDefault(x => x.ID == id);

                    if (data != null)
                    {
                        return new BaseReturnFunction<CompanyJob>(StatusFunction.TRUE, data);
                    }
                    return new BaseReturnFunction<CompanyJob>(StatusFunction.FALSE, "Không tồn tại id = " + id);
                }
            }
            catch (Exception e)
            {

                return new BaseReturnFunction<CompanyJob>(StatusFunction.EXCEPTION, e.Message);
            }
        }
        public BaseReturnFunction<List<CompanyJob>> Sync(double time)
        {
            try
            {
                using (var context = new CompanyJobDbDataContext())
                {
                    return new BaseReturnFunction<List<CompanyJob>>(StatusFunction.TRUE, context.CompanyJobs.OrderBy(x => x.Name).ToList());
                }
            }
            catch (Exception e)
            {
                return new BaseReturnFunction<List<CompanyJob>>(StatusFunction.EXCEPTION, e.Message);
            }
        }
        public BaseReturnFunction<CompanyJob> Update(int id, CompanyJob model)
        {
            try
            {
                using (var context = new CompanyJobDbDataContext())
                {
                    CompanyJob data = context.CompanyJobs.SingleOrDefault(x => x.ID == id);
                    if (data != null)
                    {
                        data.Name = model.Name;
                        context.SubmitChanges();
                        return new BaseReturnFunction<CompanyJob>(StatusFunction.TRUE, data);
                    }
                    return new BaseReturnFunction<CompanyJob>(StatusFunction.FALSE, "Không tồn tại id = " + id);
                }
            }
            catch (Exception e)
            {
                return new BaseReturnFunction<CompanyJob>(StatusFunction.EXCEPTION, e.Message);
            }
        }
        public BaseReturnFunction<List<CompanyJob>> Search(int page, int size, string keyword)
        {
            try
            {
                using (var context = new CompanyJobDbDataContext())
                {
                    IQueryable<CompanyJob> list;
                    if (String.IsNullOrWhiteSpace(keyword))
                        list = context.CompanyJobs;
                    else
                    {
                        keyword = keyword.Trim().ToLower();
                        list = context.CompanyJobs.Where(x => x.Name.ToLower().Contains(keyword));
                    }
                    return new BaseReturnFunction<List<CompanyJob>>(StatusFunction.TRUE, list.OrderBy(x => x.Name).Skip((page - 1) * size).Take(size).ToList(), list.Count());
                }
            }
            catch (Exception e)
            {
                return new BaseReturnFunction<List<CompanyJob>>(StatusFunction.EXCEPTION, e.Message);
            }
        }
    }
}