﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using VNCApp.Database.Jobs.SalaryJob;
using VNCApp.Models;

namespace VNCApp.DataAccess.Jobs
{
    public class SalaryJobProvider : IProvider<SalaryJob, int>
    {
        public BaseReturnFunction<SalaryJob> Create(SalaryJob model)
        {
            try
            {
                using (var context = new SalaryJobDbDataContext())
                {
                    context.SalaryJobs.InsertOnSubmit(model);
                    context.SubmitChanges();
                }
                return new BaseReturnFunction<SalaryJob>(StatusFunction.TRUE, model);
            }
            catch (Exception e)
            {
                return new BaseReturnFunction<SalaryJob>(StatusFunction.EXCEPTION, e.Message);
            }
        }
        public BaseReturnFunction<bool> Delete(int id)
        {
            try
            {
                using (var context = new SalaryJobDbDataContext())
                {
                    var data = context.SalaryJobs.SingleOrDefault(x => x.ID == id);
                    if (data != null)
                    {
                        context.SalaryJobs.DeleteOnSubmit(data);
                        context.SubmitChanges();
                    }
                    else
                        return new BaseReturnFunction<bool>(StatusFunction.FALSE, "Không tồn tại id = " + id);
                }
                return new BaseReturnFunction<bool>(StatusFunction.TRUE, true);
            }
            catch (Exception e)
            {

                return new BaseReturnFunction<bool>(StatusFunction.EXCEPTION, e.Message);
            }
        }
        public BaseReturnFunction<List<SalaryJob>> GetAll()
        {
            try
            {
                using (var context = new SalaryJobDbDataContext())
                {
                    var list = context.SalaryJobs;
                    int count = list.Count();
                    return new BaseReturnFunction<List<SalaryJob>>(StatusFunction.TRUE, list.ToList(), count);
                }
            }
            catch (Exception)
            {

                throw;
            }
        }
        public BaseReturnFunction<List<SalaryJob>> GetAll(int page, int size)
        {
            try
            {
                using (var context = new SalaryJobDbDataContext())
                {
                    var list = context.SalaryJobs;
                    int count = list.Count();
                    return new BaseReturnFunction<List<SalaryJob>>(StatusFunction.TRUE,
                        list.Skip((page - 1) * size).Take(size).ToList(), count);
                }
            }
            catch (Exception)
            {

                throw;
            }
        }
        public BaseReturnFunction<SalaryJob> GetById(int id)
        {
            try
            {
                using (var context = new SalaryJobDbDataContext())
                {
                    var data = context.SalaryJobs.SingleOrDefault(x => x.ID == id);

                    if (data != null)
                    {
                        return new BaseReturnFunction<SalaryJob>(StatusFunction.TRUE, data);
                    }
                    return new BaseReturnFunction<SalaryJob>(StatusFunction.FALSE, "Không tồn tại id = " + id);
                }
            }
            catch (Exception e)
            {

                return new BaseReturnFunction<SalaryJob>(StatusFunction.EXCEPTION, e.Message);
            }
        }
        public BaseReturnFunction<List<SalaryJob>> Sync(double time)
        {
                try
                {
                    using (var context = new SalaryJobDbDataContext())
                    {
                        return new BaseReturnFunction<List<SalaryJob>>(StatusFunction.TRUE, context.SalaryJobs.ToList());
                    }
                }
                catch (Exception e)
                {
                    return new BaseReturnFunction<List<SalaryJob>>(StatusFunction.EXCEPTION, e.Message);
                }
        }
        public BaseReturnFunction<SalaryJob> Update(int id, SalaryJob model)
        {
            try
            {
                using (var context = new SalaryJobDbDataContext())
                {
                    SalaryJob data = context.SalaryJobs.SingleOrDefault(x => x.ID == id);
                    if (data != null)
                    {
                        data.Name = model.Name;
                        context.SubmitChanges();
                        return new BaseReturnFunction<SalaryJob>(StatusFunction.TRUE, data);
                    }
                    return new BaseReturnFunction<SalaryJob>(StatusFunction.FALSE, "Không tồn tại id = " + id);
                }
            }
            catch (Exception e)
            {
                return new BaseReturnFunction<SalaryJob>(StatusFunction.EXCEPTION, e.Message);
            }
        }
        public BaseReturnFunction<List<SalaryJob>> Search(int page, int size, string keyword)
        {
            try
            {
                using (var context = new SalaryJobDbDataContext())
                {
                    IQueryable<SalaryJob> list;
                    if (String.IsNullOrWhiteSpace(keyword))
                        list = context.SalaryJobs;
                    else
                    {
                        keyword = keyword.Trim().ToLower();
                        list = context.SalaryJobs.Where(x => x.Name.ToLower().Contains(keyword));
                    }
                    return new BaseReturnFunction<List<SalaryJob>>(StatusFunction.TRUE, list.Skip((page - 1) * size).Take(size).ToList(), list.Count());
                }
            }
            catch (Exception e)
            {
                return new BaseReturnFunction<List<SalaryJob>>(StatusFunction.EXCEPTION, e.Message);
            }
        }
    }
}