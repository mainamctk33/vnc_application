﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using VNCApp.Database.Jobs.CountryJob;
using VNCApp.Models;

namespace VNCApp.DataAccess.Jobs
{
    public class CountryJobProvider : IProvider<CountryJob, int>
    {
        public BaseReturnFunction<CountryJob> Create(CountryJob model)
        {
            try
            {
                using (var context = new CountryJobDbDataContext())
                {
                    context.CountryJobs.InsertOnSubmit(model);
                    context.SubmitChanges();
                }
                return new BaseReturnFunction<CountryJob>(StatusFunction.TRUE, model);
            }
            catch (Exception e)
            {
                return new BaseReturnFunction<CountryJob>(StatusFunction.EXCEPTION, e.Message);
            }
        }
        public BaseReturnFunction<bool> Delete(int id)
        {
            try
            {
                using (var context = new CountryJobDbDataContext())
                {
                    var data = context.CountryJobs.SingleOrDefault(x => x.ID == id);
                    if (data != null)
                    {
                        context.CountryJobs.DeleteOnSubmit(data);
                        context.SubmitChanges();
                    }
                    else
                        return new BaseReturnFunction<bool>(StatusFunction.FALSE, "Không tồn tại id = " + id);
                }
                return new BaseReturnFunction<bool>(StatusFunction.TRUE, true);
            }
            catch (Exception e)
            {

                return new BaseReturnFunction<bool>(StatusFunction.EXCEPTION, e.Message);
            }
        }
        public BaseReturnFunction<List<CountryJob>> GetAll()
        {
            try
            {
                using (var context = new CountryJobDbDataContext())
                {
                    var list = context.CountryJobs;
                    int count = list.Count();
                    return new BaseReturnFunction<List<CountryJob>>(StatusFunction.TRUE, list.OrderBy(x => x.Name).ToList(), count);
                }
            }
            catch (Exception)
            {

                throw;
            }
        }
        public BaseReturnFunction<List<CountryJob>> GetAll(int page, int size)
        {
            try
            {
                using (var context = new CountryJobDbDataContext())
                {
                    var list = context.CountryJobs;
                    int count = list.Count();
                    return new BaseReturnFunction<List<CountryJob>>(StatusFunction.TRUE,
                        list.OrderBy(x => x.Name).Skip((page - 1) * size).Take(size).ToList(), count);
                }
            }
            catch (Exception)
            {

                throw;
            }
        }
        public BaseReturnFunction<CountryJob> GetById(int id)
        {
            try
            {
                using (var context = new CountryJobDbDataContext())
                {
                    var data = context.CountryJobs.SingleOrDefault(x => x.ID == id);

                    if (data != null)
                    {
                        return new BaseReturnFunction<CountryJob>(StatusFunction.TRUE, data);
                    }
                    return new BaseReturnFunction<CountryJob>(StatusFunction.FALSE, "Không tồn tại id = " + id);
                }
            }
            catch (Exception e)
            {

                return new BaseReturnFunction<CountryJob>(StatusFunction.EXCEPTION, e.Message);
            }
        }
        public BaseReturnFunction<List<CountryJob>> Sync(double time)
        {
            try
            {
                using (var context = new CountryJobDbDataContext())
                {
                    return new BaseReturnFunction<List<CountryJob>>(StatusFunction.TRUE, context.CountryJobs.OrderBy(x => x.Name).ToList());
                }
            }
            catch (Exception e)
            {
                return new BaseReturnFunction<List<CountryJob>>(StatusFunction.EXCEPTION, e.Message);
            }
        }
        public BaseReturnFunction<CountryJob> Update(int id, CountryJob model)
        {
            try
            {
                using (var context = new CountryJobDbDataContext())
                {
                    CountryJob data = context.CountryJobs.SingleOrDefault(x => x.ID == id);
                    if (data != null)
                    {
                        data.Name = model.Name;                        
                        context.SubmitChanges();
                        return new BaseReturnFunction<CountryJob>(StatusFunction.TRUE, data);
                    }
                    return new BaseReturnFunction<CountryJob>(StatusFunction.FALSE, "Không tồn tại id = " + id);
                }
            }
            catch (Exception e)
            {
                return new BaseReturnFunction<CountryJob>(StatusFunction.EXCEPTION, e.Message);
            }
        }
        public BaseReturnFunction<List<CountryJob>> Search(int page, int size, string keyword)
        {
            try
            {
                using (var context = new CountryJobDbDataContext())
                {
                    IQueryable<CountryJob> list;
                    if (String.IsNullOrWhiteSpace(keyword))
                        list = context.CountryJobs;
                    else
                    {
                        keyword = keyword.Trim().ToLower();
                        list = context.CountryJobs.Where(x => x.Name.ToLower().Contains(keyword));
                    }
                    return new BaseReturnFunction<List<CountryJob>>(StatusFunction.TRUE, list.OrderBy(x => x.Name).Skip((page - 1) * size).Take(size).ToList(), list.Count());
                }
            }
            catch (Exception e)
            {
                return new BaseReturnFunction<List<CountryJob>>(StatusFunction.EXCEPTION, e.Message);
            }
        }
    }
}