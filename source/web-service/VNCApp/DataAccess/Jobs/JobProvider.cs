﻿using BaseLibrary.Common;
using System;
using System.Collections.Generic;
using System.Data.Linq;
using System.Linq;
using System.Web;
using VNCApp.Database.Jobs.Jobs;
using VNCApp.Models;


namespace VNCApp.DataAccess.Jobs
{

    public class JobProvider : IProvider<Job, int>
    {
        public BaseReturnFunction<Job> Create(Job model)
        {
            try
            {
                //using (var context = new JobsDbDataContext())
                //{
                //    context.Jobs.InsertOnSubmit(model);
                //    context.SubmitChanges();
                //}
                //return new BaseReturnFunction<Job>(StatusFunction.TRUE, model);

                if (String.IsNullOrEmpty(model.Title) || String.IsNullOrEmpty(model.Description) || String.IsNullOrEmpty(model.Requirement) || String.IsNullOrEmpty(model.Phone))
                    return new BaseReturnFunction<Job>(StatusFunction.FALSE, "Some information is missing / Một số thông tin đang bị thiếu");

                if (model.Email == null)
                    model.Email = "";
                if (model.Company == null)
                    model.Company = "";

                using (var context = new JobsDbDataContext())
                {
                    //IQueryable<Job> listJobs;
                    //listJobs = context.Jobs.Where(x => x.Title.ToLower().Equals(model.Title.ToLower()) && x.Description.ToLower().Equals(model.Description.ToLower()) && x.Requirement.ToLower().Equals(model.Requirement.ToLower()) 
                    //    && x.Phone.ToLower().Equals(model.Phone.ToLower()) && x.Address.ToLower().Equals(model.Address.ToLower()) 
                    //    && x.City == model.City && x.Country == model.Country && x.Level == model.Level && x.Type == model.Type && x.Salary == model.Salary 
                    //    && x.Email != null && model.Email != null && x.Email.ToLower().Equals(model.Email.ToLower()) 
                    //    && x.Company != null && model.Company != null && x.Company.ToLower().Equals(model.Company.ToLower()));
                    //if (listJobs.Count() > 0)
                    //{
                    //    return new BaseReturnFunction<Job>(StatusFunction.FALSE, "This job already exists / Công việc này đã tồn tại");
                    //}
                    //else
                    //{
                    //    context.Jobs.InsertOnSubmit(model);
                    //    context.SubmitChanges();
                    //    return new BaseReturnFunction<Job>(StatusFunction.TRUE, model);
                    //}

                    var job = context.Jobs.Where(x => x.Title.ToLower().Equals(model.Title.ToLower()) && x.Description.ToLower().Equals(model.Description.ToLower()) && x.Requirement.ToLower().Equals(model.Requirement.ToLower())
                        && x.Phone.ToLower().Equals(model.Phone.ToLower())
                        && x.City == model.City && x.Country == model.Country && x.Level == model.Level && x.Type == model.Type && x.Salary == model.Salary
                        && x.Email != null && model.Email != null && x.Email.ToLower().Equals(model.Email.ToLower())
                        && x.Company.ToLower().Equals(model.Company.ToLower())).FirstOrDefault();
                    if (job != null)
                    {
                        return new BaseReturnFunction<Job>(StatusFunction.FALSE, "This job already exists / Công việc này đã tồn tại");
                    }
                    else
                    {
                        context.Jobs.InsertOnSubmit(model);
                        context.SubmitChanges();
                        return new BaseReturnFunction<Job>(StatusFunction.TRUE, model);
                    }
                }
            }
            catch (Exception e)
            {
                return new BaseReturnFunction<Job>(StatusFunction.EXCEPTION, e.Message);
            }
        }

        public BaseReturnFunction<bool> Delete(int id)
        {
            try
            {
                using (var context = new JobsDbDataContext())
                {
                    var data = context.Jobs.SingleOrDefault(x => x.ID == id);
                    if (data != null)
                    {
                        context.Jobs.DeleteOnSubmit(data);
                        context.SubmitChanges();
                    }
                    else
                        return new BaseReturnFunction<bool>(StatusFunction.FALSE, "Không tồn tại id = " + id);
                }
                return new BaseReturnFunction<bool>(StatusFunction.TRUE, true);
            }
            catch (Exception e)
            {

                return new BaseReturnFunction<bool>(StatusFunction.EXCEPTION, e.Message);
            }
        }
        public BaseReturnFunction<List<Job>> GetAll()
        {
            throw new Exception();
        }


        public BaseReturnFunction<object> GetAllWithCityMine(int page, int size, int type, string userId)
        {
            throw new Exception();
            //try
            //{
            //    using (var context = new JobsDbDataContext())
            //    {

            //        //if (type == -1)
            //        //{
            //        //    var list = context.Jobs.Select(x => new { Job = x, City = x.CityJob.Name }).ToList();
            //        //    int count = list.Count();
            //        //    return new BaseReturnFunction<object>(StatusFunction.TRUE,
            //        //        list.Skip((page - 1) * size).Take(size).ToList(), count);
            //        //}
            //        //else
            //        //{
            //        //    var list = context.Jobs.Where(x => (type == 0 || x.Type == type)).Select(x => new { Job = x, City = x.CityJob.Name }).ToList();
            //        //    int count = list.Count();
            //        //    return new BaseReturnFunction<object>(StatusFunction.TRUE,
            //        //        list.Skip((page - 1) * size).Take(size).ToList(), count);

            //        //}
            //        IQueryable<Job> list;
            //        if (type == -2)
            //            if (!String.IsNullOrWhiteSpace(userId))
            //                list = context.BookmarkJobs.Where(x => x.BookmarkBy == userId).OrderBy(x => x.BookmarkDate).Select(x => x.Job);
            //            else
            //                list = context.Jobs;
            //        else
            //            list = context.Jobs.Where(x => type==-1|| type == 0 || x.Type == type);
            //        list = list.Where(x => x.ApproveDate != null && x.ApproveDate > 0).OrderByDescending(x => x.ApproveDate);
            //        int count = list.Count();
            //        return new BaseReturnFunction<object>(StatusFunction.TRUE,
            //            list.Skip((page - 1) * size).Take(size).Select(x => new { Job = x, City = x.CityJob.Name }).ToList(), count);
            //    }
            //}
            //catch (Exception e)
            //{

            //    return new BaseReturnFunction<object>(StatusFunction.EXCEPTION, e.Message);
            //}
        }

        public BaseReturnFunction<object> GetAllWithCity(int page, int size, int type, string userId)
        {
            try
            {
                using (var context = new JobsDbDataContext())
                {

                    //if (type == -1)
                    //{
                    //    var list = context.Jobs.Select(x => new { Job = x, City = x.CityJob.Name }).ToList();
                    //    int count = list.Count();
                    //    return new BaseReturnFunction<object>(StatusFunction.TRUE,
                    //        list.Skip((page - 1) * size).Take(size).ToList(), count);
                    //}
                    //else
                    //{
                    //    var list = context.Jobs.Where(x => (type == 0 || x.Type == type)).Select(x => new { Job = x, City = x.CityJob.Name }).ToList();
                    //    int count = list.Count();
                    //    return new BaseReturnFunction<object>(StatusFunction.TRUE,
                    //        list.Skip((page - 1) * size).Take(size).ToList(), count);

                    //}
                    IQueryable<Job> list;
                    if (type == -3)
                    {
                        if (!String.IsNullOrWhiteSpace(userId))
                            list = context.Jobs.Where(x => x.CreatedBy == userId).OrderBy(x => x.CreatedDate);
                        else
                            list = context.Jobs.Where(x => x.ID == -9999);
                        //list = new Table<Job>;
                    }
                    else
                    {
                        if (type == -2)
                        {
                            if (!String.IsNullOrWhiteSpace(userId))
                                list = context.BookmarkJobs.Where(x => x.BookmarkBy == userId).OrderBy(x => x.BookmarkDate).Select(x => x.Job);
                            else
                                list = context.Jobs;
                        }
                        else
                            list = context.Jobs.Where(x => type == -1 || type == 0 || x.Type == type);
                        list = list.Where(x => x.ApproveDate != null && x.ApproveDate > 0).OrderByDescending(x => x.ApproveDate);
                    }
                    int count = list.Count();
                    return new BaseReturnFunction<object>(StatusFunction.TRUE,
                        list.OrderByDescending(x => x.CreatedDate).OrderByDescending(x => x.ApproveDate).Skip((page - 1) * size).Take(size).Select(x => new { Job = x, City = x.CityJob.Name, Country = x.CityJob.CountryJob.Name }).ToList(), count);
                }
            }
            catch (Exception e)
            {

                return new BaseReturnFunction<object>(StatusFunction.EXCEPTION, e.Message);
            }
        }
        public BaseReturnFunction<object> SearchWithCity(int page, int size, string title, int country, int city, int type, int level, int salaryFrom, int salaryTo, string company, int sort)
        {
            try
            {
                using (var context = new JobsDbDataContext())
                {

                    var list = context.Jobs.Where(x => (type == -1 || x.Type == type)
                    && x.ApproveDate != null && x.ApproveDate > 0
                    && (title == null || title.Equals("") || (x.Title != null && x.Title.ToLower().Contains(title.ToLower())))
                        && (country == -1 || country == 0 || (x.CityJob != null && x.CityJob.Country != null && x.CityJob.Country == country))
                        && (city == -1 || city == 0 || (x.City != null && x.City == city))
                        && (level == -1 || level == 0 || (x.Level != null && x.Level == level))
                        && (company == null || company.Equals("") || (x.Company != null && x.Company.Contains(company)))
                        && (salaryFrom == -1 || salaryFrom == 0 || salaryTo == -1 || salaryTo == 0 || (x.Salary != null && Convert.ToInt32(x.Salary) >= salaryFrom && Convert.ToInt32(x.Salary) < salaryTo))
                        );//.Select(x => new { Job = x, City = x.CityJob.Name }).OrderByDescending(x => (x.Job.ApproveDate));//.ToList();
                    if (sort != 1)
                        list = list.OrderByDescending(x => x.ApproveDate);
                    if (sort == 1)
                        list = list.OrderByDescending(x => x.Salary);
                    int count = list.Count();
                    return new BaseReturnFunction<object>(StatusFunction.TRUE,
                        list.OrderByDescending(x => x.CreatedDate).OrderByDescending(x => x.ApproveDate).Skip((page - 1) * size).Take(size).Select(x => new { Job = x, City = x.CityJob.Name, Country = x.CityJob.CountryJob.Name }).ToList(), count);

                }
            }
            catch (Exception e)
            {

                return new BaseReturnFunction<object>(StatusFunction.EXCEPTION, e.Message);
            }
        }
        //public BaseReturnFunction<List<Job>> GetAll(int page, int size, int type)
        //{
        //    try
        //    {
        //        using (var context = new JobsDbDataContext())
        //        {

        //            if (type == -1)
        //            {
        //                var list = context.Jobs.ToList();
        //                int count = list.Count();
        //                return new BaseReturnFunction<List<Job>>(StatusFunction.TRUE,
        //                    list.Skip((page - 1) * size).Take(size).ToList(), count);
        //            }
        //            else
        //            {
        //                var list = context.Jobs.Where(x => (type == 0 || x.Type == type));
        //                int count = list.Count();
        //                return new BaseReturnFunction<List<Job>>(StatusFunction.TRUE,
        //                    list.OrderByDescending(x => x.CreatedDate).OrderByDescending(x => x.ApproveDate).Skip((page - 1) * size).Take(size).ToList(), count);
        //            }
        //        }
        //    }
        //    catch (Exception e)
        //    {
        //        return new BaseReturnFunction<List<Job>>(StatusFunction.EXCEPTION, e.Message);
        //    }
        //}
        public BaseReturnFunction<object> GetAll(int page, int size, int type, string keyword)
        {
            try
            {
                using (var context = new JobsDbDataContext())
                {

                    if (type == -1)
                    {
                        IQueryable<Job> list;
                        if (String.IsNullOrWhiteSpace(keyword))
                             list = context.Jobs;
                        else
                            list = context.Jobs.Where(x=>x.Title.Contains(keyword));
                        int count = list.Count();
                        return new BaseReturnFunction<object>(StatusFunction.TRUE,
                            list.Skip((page - 1) * size).Take(size).Select(x => new { Job = x, CreatedByName = x.User.FullName }).ToList(), count);
                    }
                    else
                    {
                        IQueryable<Job> list;
                        if (String.IsNullOrWhiteSpace(keyword))
                            list = context.Jobs.Where(x => (type == 0 || x.Type == type));
                        else
                            list = context.Jobs.Where(x => (type == 0 || x.Type == type) && x.Title.Contains(keyword));
                        int count = list.Count();
                        return new BaseReturnFunction<object>(StatusFunction.TRUE,
                            list.OrderByDescending(x => x.CreatedDate).OrderByDescending(x => x.ApproveDate).Skip((page - 1) * size).Take(size).Select(x => new { Job = x, CreatedByName = x.User.FullName }).ToList(), count);
                    }
                }
            }
            catch (Exception e)
            {
                return new BaseReturnFunction<object>(StatusFunction.EXCEPTION, e.Message);
            }
        }


        public BaseReturnFunction<Job> GetById(int id)
        {
            try
            {
                using (var context = new JobsDbDataContext())
                {

                    var data = context.Jobs.SingleOrDefault(x => x.ID == id);
                    if (data != null)
                    {
                        return new BaseReturnFunction<Job>(StatusFunction.TRUE, data);
                    }
                    return new BaseReturnFunction<Job>(StatusFunction.FALSE, "Không tồn tại id = " + id);
                }
            }
            catch (Exception e)
            {

                return new BaseReturnFunction<Job>(StatusFunction.EXCEPTION, e.Message);
            }
        }
        public BaseReturnFunction<object> GetByIdMoreInfo(int id)
        {
            try
            {
                using (var context = new JobsDbDataContext())
                {
                    var data = context.Jobs.Where(x => x.ID == id).Select(x => new
                    {
                        Job = x,
                        City = x.CityJob.Name,
                        Country = x.CountryJob.Name,
                        Type = x.TypeJob.Name,
                        Level = x.LevelJob.Name,
                    }).First();
                    if (data != null)
                    {
                        return new BaseReturnFunction<object>(StatusFunction.TRUE, data);
                    }
                    return new BaseReturnFunction<object>(StatusFunction.FALSE, "Không tồn tại id = " + id);
                }
            }
            catch (Exception e)
            {

                return new BaseReturnFunction<object>(StatusFunction.EXCEPTION, e.Message);
            }
        }


        public int GetPending()
        {
            try
            {
                using (var context = new JobsDbDataContext())
                {

                        return context.Jobs.Where(x=>x.ApproveDate == null).ToList().Count();
                }
            }
            catch (Exception e)
            {
                return 0;
            }
        }
        







        public BaseReturnFunction<List<Job>> Sync(double time)
        {
            throw new NotImplementedException();
        }
        public BaseReturnFunction<Job> Update(int id, Job model)
        {
            try
            {
                using (var context = new JobsDbDataContext())
                {
                    Job data = context.Jobs.SingleOrDefault(x => x.ID == id);
                    if (data != null)
                    {
                        data.Address = model.Address;
                        data.ApproveBy = model.ApproveBy;
                        data.ApproveDate = model.ApproveDate;
                        data.City = model.City;
                        data.Company = model.Company;
                        data.Country = model.Country;
                        data.CreatedBy = model.CreatedBy;
                        data.CreatedDate = model.CreatedDate;
                        data.Description = model.Description;
                        data.Email = model.Email;
                        data.Lat = model.Lat;
                        data.Level = model.Level;
                        data.Long = model.Long;
                        data.Phone = model.Phone;
                        data.Requirement = model.Requirement;
                        data.Salary = model.Salary;
                        data.Status = model.Status;
                        data.Title = model.Title;
                        data.Type = model.Type;
                        data.Thumbnail = model.Thumbnail;
                        context.SubmitChanges();
                        return new BaseReturnFunction<Job>(StatusFunction.TRUE, data);
                    }
                    return new BaseReturnFunction<Job>(StatusFunction.FALSE, "Không tồn tại id = " + id);
                }
            }
            catch (Exception e)
            {
                return new BaseReturnFunction<Job>(StatusFunction.EXCEPTION, e.Message);
            }
        }
        public BaseReturnFunction<Job> Approve(int id, string approveBy)
        {
            try
            {
                using (var context = new JobsDbDataContext())
                {
                    Job data = context.Jobs.SingleOrDefault(x => x.ID == id);
                    if (data != null)
                    {
                        if (data.ApproveDate != null)
                            return new BaseReturnFunction<Job>(StatusFunction.FALSE, "Đã được duyệt trước đó rồi");
                        data.ApproveDate = DateTime.Now.GetUnixTimeStamp();
                        //thanhtam.tr TODO
                        // data.ApproveBy = approveBy;
                        context.SubmitChanges();
                        return new BaseReturnFunction<Job>(StatusFunction.TRUE, data);
                    }
                    return new BaseReturnFunction<Job>(StatusFunction.FALSE, "Không tồn tại id = " + id);
                }
            }
            catch (Exception e)
            {
                return new BaseReturnFunction<Job>(StatusFunction.EXCEPTION, e.Message);
            }
        }
        //thanhtam.tr - need Implement
        public BaseReturnFunction<Boolean> Bookmark(int id, bool bookmark, string requestUserId)
        {
            try
            {
                using (var context = new JobsDbDataContext())
                {
                    if (!bookmark)
                    {
                        var bm = context.BookmarkJobs.SingleOrDefault(x => x.BookmarkBy == requestUserId && x.ID == id);
                        if (bm != null)
                        {
                            context.BookmarkJobs.DeleteOnSubmit(bm);
                            context.SubmitChanges();
                        }
                        return new BaseReturnFunction<bool>(StatusFunction.TRUE, true);
                    }
                    else
                    {
                        context.BookmarkJobs.InsertOnSubmit(new BookmarkJob()
                        {
                            ID = id,
                            BookmarkBy = requestUserId,
                            BookmarkDate = DateTime.Now.GetUnixTimeStamp()
                        });
                        context.SubmitChanges();
                        return new BaseReturnFunction<bool>(StatusFunction.TRUE, true);
                    }
                }
                return new BaseReturnFunction<bool>(StatusFunction.TRUE, true);
            }
            catch (Exception e)
            {
                return new BaseReturnFunction<bool>(StatusFunction.EXCEPTION, e.Message);
            }
        }

    }
}