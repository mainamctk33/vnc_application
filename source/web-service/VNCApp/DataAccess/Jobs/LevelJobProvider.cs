﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using VNCApp.Database.Jobs.LevelJob;
using VNCApp.Models;

namespace VNCApp.DataAccess.Jobs
{
    public class LevelJobProvider : IProvider<LevelJob, int>
    {
        public BaseReturnFunction<LevelJob> Create(LevelJob model)
        {
            try
            {
                using (var context = new LevelJobDbDataContext())
                {
                    context.LevelJobs.InsertOnSubmit(model);
                    context.SubmitChanges();
                }
                return new BaseReturnFunction<LevelJob>(StatusFunction.TRUE, model);
            }
            catch (Exception e)
            {
                return new BaseReturnFunction<LevelJob>(StatusFunction.EXCEPTION, e.Message);
            }
        }
        public BaseReturnFunction<bool> Delete(int id)
        {
            try
            {
                using (var context = new LevelJobDbDataContext())
                {
                    var data = context.LevelJobs.SingleOrDefault(x => x.ID == id);
                    if (data != null)
                    {
                        context.LevelJobs.DeleteOnSubmit(data);
                        context.SubmitChanges();
                    }
                    else
                        return new BaseReturnFunction<bool>(StatusFunction.FALSE, "Không tồn tại id = " + id);
                }
                return new BaseReturnFunction<bool>(StatusFunction.TRUE, true);
            }
            catch (Exception e)
            {

                return new BaseReturnFunction<bool>(StatusFunction.EXCEPTION, e.Message);
            }
        }
        public BaseReturnFunction<List<LevelJob>> GetAll()
        {
            try
            {
                using (var context = new LevelJobDbDataContext())
                {
                    var list = context.LevelJobs;
                    int count = list.Count();
                    return new BaseReturnFunction<List<LevelJob>>(StatusFunction.TRUE, list.OrderBy(x => x.Name).ToList(), count);
                }
            }
            catch (Exception)
            {

                throw;
            }
        }
        public BaseReturnFunction<List<LevelJob>> GetAll(int page, int size)
        {
            try
            {
                using (var context = new LevelJobDbDataContext())
                {
                    var list = context.LevelJobs;
                    int count = list.Count();
                    return new BaseReturnFunction<List<LevelJob>>(StatusFunction.TRUE,
                        list.OrderBy(x => x.Name).Skip((page - 1) * size).Take(size).ToList(), count);
                }
            }
            catch (Exception)
            {

                throw;
            }
        }
        public BaseReturnFunction<LevelJob> GetById(int id)
        {
            try
            {
                using (var context = new LevelJobDbDataContext())
                {
                    var data = context.LevelJobs.SingleOrDefault(x => x.ID == id);

                    if (data != null)
                    {
                        return new BaseReturnFunction<LevelJob>(StatusFunction.TRUE, data);
                    }
                    return new BaseReturnFunction<LevelJob>(StatusFunction.FALSE, "Không tồn tại id = " + id);
                }
            }
            catch (Exception e)
            {

                return new BaseReturnFunction<LevelJob>(StatusFunction.EXCEPTION, e.Message);
            }
        }
        public BaseReturnFunction<List<LevelJob>> Sync(double time)
        {
            try
            {
                using (var context = new LevelJobDbDataContext())
                {
                    return new BaseReturnFunction<List<LevelJob>>(StatusFunction.TRUE, context.LevelJobs.OrderBy(x => x.Name).ToList());
                }
            }
            catch (Exception e)
            {
                return new BaseReturnFunction<List<LevelJob>>(StatusFunction.EXCEPTION, e.Message);
            }
        }
        public BaseReturnFunction<LevelJob> Update(int id, LevelJob model)
        {
            try
            {
                using (var context = new LevelJobDbDataContext())
                {
                    LevelJob data = context.LevelJobs.SingleOrDefault(x => x.ID == id);
                    if (data != null)
                    {
                        data.Name = model.Name;
                        data.Name2 = model.Name2;
                        context.SubmitChanges();
                        return new BaseReturnFunction<LevelJob>(StatusFunction.TRUE, data);
                    }
                    return new BaseReturnFunction<LevelJob>(StatusFunction.FALSE, "Không tồn tại id = " + id);
                }
            }
            catch (Exception e)
            {
                return new BaseReturnFunction<LevelJob>(StatusFunction.EXCEPTION, e.Message);
            }
        }
        public BaseReturnFunction<List<LevelJob>> Search(int page, int size, string keyword)
        {
            try
            {
                using (var context = new LevelJobDbDataContext())
                {
                    IQueryable<LevelJob> list;
                    if (String.IsNullOrWhiteSpace(keyword))
                        list = context.LevelJobs;
                    else
                    {
                        keyword = keyword.Trim().ToLower();
                        list = context.LevelJobs.Where(x => x.Name.ToLower().Contains(keyword) || x.Name2.ToLower().Contains(keyword));
                    }
                    return new BaseReturnFunction<List<LevelJob>>(StatusFunction.TRUE, list.OrderBy(x => x.Name).Skip((page - 1) * size).Take(size).ToList(), list.Count());
                }
            }
            catch (Exception e)
            {
                return new BaseReturnFunction<List<LevelJob>>(StatusFunction.EXCEPTION, e.Message);
            }
        }
    }
}