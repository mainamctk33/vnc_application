﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using VNCApp.Database.Jobs.TypeJob;
using VNCApp.Models;

namespace VNCApp.DataAccess.Jobs
{
    public class TypeJobProvider : IProvider<TypeJob, int>
    {
        public BaseReturnFunction<TypeJob> Create(TypeJob model)
        {
            try
            {
                using (var context = new TypeJobDbDataContext())
                {
                    context.TypeJobs.InsertOnSubmit(model);
                    context.SubmitChanges();
                }
                return new BaseReturnFunction<TypeJob>(StatusFunction.TRUE, model);
            }
            catch (Exception e)
            {
                return new BaseReturnFunction<TypeJob>(StatusFunction.EXCEPTION, e.Message);
            }
        }
        public BaseReturnFunction<bool> Delete(int id)
        {
            try
            {
                using (var context = new TypeJobDbDataContext())
                {
                    var data = context.TypeJobs.SingleOrDefault(x => x.ID == id);
                    if (data != null)
                    {
                        context.TypeJobs.DeleteOnSubmit(data);
                        context.SubmitChanges();
                    }
                    else
                        return new BaseReturnFunction<bool>(StatusFunction.FALSE, "Không tồn tại id = " + id);
                }
                return new BaseReturnFunction<bool>(StatusFunction.TRUE, true);
            }
            catch (Exception e)
            {

                return new BaseReturnFunction<bool>(StatusFunction.EXCEPTION, e.Message);
            }
        }
        public BaseReturnFunction<List<TypeJob>> Search(int page, int size, string keyword)
        {
            try
            {
                using (var context = new TypeJobDbDataContext())
                {
                    IQueryable<TypeJob> list;
                    if (String.IsNullOrWhiteSpace(keyword))
                        list = context.TypeJobs;
                    else
                    {
                        keyword = keyword.Trim().ToLower();
                        list = context.TypeJobs.Where(x => x.Name.ToLower().Contains(keyword) || x.Name2.ToLower().Contains(keyword));
                    }
                    return new BaseReturnFunction<List<TypeJob>>(StatusFunction.TRUE, list.OrderBy(x => x.Name).Skip((page - 1) * size).Take(size).ToList(), list.Count());
                }
            }
            catch (Exception e)
            {
                return new BaseReturnFunction<List<TypeJob>>(StatusFunction.EXCEPTION, e.Message);
            }
        }
        public BaseReturnFunction<List<TypeJob>> GetAll()
        {
            try
            {
                using (var context = new TypeJobDbDataContext())
                {
                    var list = context.TypeJobs;
                    int count = list.Count();
                    return new BaseReturnFunction<List<TypeJob>>(StatusFunction.TRUE, list.OrderBy(x => x.Name).ToList(), count);
                }
            }
            catch (Exception)
            {

                throw;
            }
        }

        public BaseReturnFunction<List<TypeJob>> GetAll(int page, int size)
        {
            try
            {
                using (var context = new TypeJobDbDataContext())
                {
                    var list = context.TypeJobs;
                    int count = list.Count();
                    return new BaseReturnFunction<List<TypeJob>>(StatusFunction.TRUE,
                        list.OrderBy(x => x.Name).Skip((page - 1) * size).Take(size).ToList(), count);
                }
            }
            catch (Exception)
            {

                throw;
            }
        }
        public BaseReturnFunction<TypeJob> GetById(int id)
        {
            try
            {
                using (var context = new TypeJobDbDataContext())
                {
                    var data = context.TypeJobs.SingleOrDefault(x => x.ID == id);

                    if (data != null)
                    {
                        return new BaseReturnFunction<TypeJob>(StatusFunction.TRUE, data);
                    }
                    return new BaseReturnFunction<TypeJob>(StatusFunction.FALSE, "Không tồn tại id = " + id);
                }
            }
            catch (Exception e)
            {

                return new BaseReturnFunction<TypeJob>(StatusFunction.EXCEPTION, e.Message);
            }
        }
        public BaseReturnFunction<List<TypeJob>> Sync(double time)
        {
            try
            {
                using (var context = new TypeJobDbDataContext())
                {
                    return new BaseReturnFunction<List<TypeJob>>(StatusFunction.TRUE, context.TypeJobs.OrderBy(x => x.Name).ToList());
                }
            }
            catch (Exception e)
            {
                return new BaseReturnFunction<List<TypeJob>>(StatusFunction.EXCEPTION, e.Message);
            }
        }
        public BaseReturnFunction<TypeJob> Update(int id, TypeJob model)
        {
            try
            {
                using (var context = new TypeJobDbDataContext())
                {
                    TypeJob data = context.TypeJobs.SingleOrDefault(x => x.ID == id);
                    if (data != null)
                    {
                        data.Name = model.Name;
                        data.Name2 = model.Name2;
                        context.SubmitChanges();
                        return new BaseReturnFunction<TypeJob>(StatusFunction.TRUE, data);
                    }
                    return new BaseReturnFunction<TypeJob>(StatusFunction.FALSE, "Không tồn tại id = " + id);
                }
            }
            catch (Exception e)
            {
                return new BaseReturnFunction<TypeJob>(StatusFunction.EXCEPTION, e.Message);
            }
        }
    }
}