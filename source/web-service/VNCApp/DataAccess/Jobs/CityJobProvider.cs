﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using VNCApp.Database.Jobs.CityJob;
using VNCApp.Models;

namespace VNCApp.DataAccess.Jobs
{
    public class CityJobProvider : IProvider<CityJob, int>
    {
        public BaseReturnFunction<CityJob> Create(CityJob model)
        {
            try
            {
                using (var context = new CityJobDbDataContext())
                {
                    context.CityJobs.InsertOnSubmit(model);
                    context.SubmitChanges();
                }
                return new BaseReturnFunction<CityJob>(StatusFunction.TRUE, model);
            }
            catch (Exception e)
            {
                return new BaseReturnFunction<CityJob>(StatusFunction.EXCEPTION, e.Message);
            }
        }
        public BaseReturnFunction<bool> Delete(int id)
        {
            try
            {
                using (var context = new CityJobDbDataContext())
                {
                    var data = context.CityJobs.SingleOrDefault(x => x.ID == id);
                    if (data != null)
                    {
                        context.CityJobs.DeleteOnSubmit(data);
                        context.SubmitChanges();
                    }
                    else
                        return new BaseReturnFunction<bool>(StatusFunction.FALSE, "Không tồn tại id = " + id);
                }
                return new BaseReturnFunction<bool>(StatusFunction.TRUE, true);
            }
            catch (Exception e)
            {

                return new BaseReturnFunction<bool>(StatusFunction.EXCEPTION, e.Message);
            }
        }
        public BaseReturnFunction<List<CityJob>> GetAll()
        {
            try
            {
                using (var context = new CityJobDbDataContext())
                {
                    var list = context.CityJobs;
                    int count = list.Count();
                    return new BaseReturnFunction<List<CityJob>>(StatusFunction.TRUE, list.OrderBy(x=>x.Name).ToList(), count);
                }
            }
            catch (Exception)
            {

                throw;
            }
        }
        public BaseReturnFunction<List<CityJob>> GetAll(int country)
        {
            try
            {
                using (var context = new CityJobDbDataContext())
                {
                    if (country == -1)
                    {
                        var list = context.CityJobs;
                        int count = list.Count();
                        return new BaseReturnFunction<List<CityJob>>(StatusFunction.TRUE, list.OrderBy(x => x.Name).ToList(), count);
                    }
                    else
                    {
                        var list = context.CityJobs.Where(x=>x.Country == country);
                        int count = list.Count();
                        return new BaseReturnFunction<List<CityJob>>(StatusFunction.TRUE, list.OrderBy(x => x.Name).ToList(), count);
                    }
                }
            }
            catch (Exception)
            {

                throw;
            }
        }
        public BaseReturnFunction<List<CityJob>> GetAll(int page, int size)
        {
            try
            {
                using (var context = new CityJobDbDataContext())
                {
                    var list = context.CityJobs;
                    int count = list.Count();
                    return new BaseReturnFunction<List<CityJob>>(StatusFunction.TRUE,
                        list.OrderBy(x => x.Name).Skip((page - 1) * size).Take(size).ToList(), count);
                }
            }
            catch (Exception)
            {

                throw;
            }
        }
        public BaseReturnFunction<CityJob> GetById(int id)
        {
            try
            {
                using (var context = new CityJobDbDataContext())
                {
                    var data = context.CityJobs.SingleOrDefault(x => x.ID == id);

                    if (data != null)
                    {
                        return new BaseReturnFunction<CityJob>(StatusFunction.TRUE, data);
                    }
                    return new BaseReturnFunction<CityJob>(StatusFunction.FALSE, "Không tồn tại id = " + id);
                }
            }
            catch (Exception e)
            {

                return new BaseReturnFunction<CityJob>(StatusFunction.EXCEPTION, e.Message);
            }
        }
        public BaseReturnFunction<List<CityJob>> Sync(double time)
        {
            try
            {
                using (var context = new CityJobDbDataContext())
                {
                    return new BaseReturnFunction<List<CityJob>>(StatusFunction.TRUE, context.CityJobs.OrderBy(x => x.Name).ToList());
                }
            }
            catch (Exception e)
            {
                return new BaseReturnFunction<List<CityJob>>(StatusFunction.EXCEPTION, e.Message);
            }
        }
        public BaseReturnFunction<CityJob> Update(int id, CityJob model)
        {
            try
            {
                using (var context = new CityJobDbDataContext())
                {
                    CityJob data = context.CityJobs.SingleOrDefault(x => x.ID == id);
                    if (data != null)
                    {
                        data.Name = model.Name;
                        data.Country = model.Country;
                        context.SubmitChanges();
                        return new BaseReturnFunction<CityJob>(StatusFunction.TRUE, data);
                    }
                    return new BaseReturnFunction<CityJob>(StatusFunction.FALSE, "Không tồn tại id = " + id);
                }
            }
            catch (Exception e)
            {
                return new BaseReturnFunction<CityJob>(StatusFunction.EXCEPTION, e.Message);
            }
        }
        public BaseReturnFunction<List<CityJob>> Search(int page, int size, string keyword, int country)
        {
            try
            {
                using (var context = new CityJobDbDataContext())
                {
                    if (country == -1)
                    {
                        IQueryable<CityJob> list;
                        if (String.IsNullOrWhiteSpace(keyword))
                            list = context.CityJobs;
                        else
                        {
                            keyword = keyword.Trim().ToLower();
                            list = context.CityJobs.Where(x => x.Name.ToLower().Contains(keyword));
                        }
                        return new BaseReturnFunction<List<CityJob>>(StatusFunction.TRUE, list.Skip((page - 1) * size).Take(size).ToList(), list.Count());
                    }
                    else
                    {
                        var list = context.CityJobs.Where(x => x.Country == country);
                        int count = list.Count();
                        return new BaseReturnFunction<List<CityJob>>(StatusFunction.TRUE, list.OrderBy(x => x.Name).ToList(), count);
                    }
                }
            }
            catch (Exception e)
            {
                return new BaseReturnFunction<List<CityJob>>(StatusFunction.EXCEPTION, e.Message);
            }
        }
    }
}