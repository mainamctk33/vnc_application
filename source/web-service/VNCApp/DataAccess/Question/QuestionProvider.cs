﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using VNCApp.Database.Question;
using VNCApp.Models;

namespace VNCApp.DataAccess.Question
{
    public class QuestionProvider : IProvider<Database.Question.Question, int>
    {
        public BaseReturnFunction<Database.Question.Question> Create(Database.Question.Question model)
        {
            try
            {
                if (model.Title == null || model.Content == null)
                    return new BaseReturnFunction<Database.Question.Question>(StatusFunction.FALSE, "Some information is missing / Một số thông tin đang bị thiếu");

                using (var context = new QuestionDbDataContext())
                {
                    IQueryable<Database.Question.Question> listQuestions;
                    //listQuestions = context.Questions.Where(x => !String.IsNullOrWhiteSpace(x.Title) && !String.IsNullOrWhiteSpace(x.Content) && ((!String.IsNullOrWhiteSpace(model.Title) && x.Title.ToLower().Equals(model.Title.ToLower())) || (!String.IsNullOrWhiteSpace(model.Content) && x.Content.ToLower().Equals(model.Content.ToLower()))));
                    listQuestions = context.Questions.Where(x => x.Title != null && x.Content != null && (model.Title != null && x.Title.ToLower().Equals(model.Title.ToLower())) || (model.Content != null && x.Content.ToLower().Equals(model.Content.ToLower())));
                    if (!listQuestions.Any())
                    {
                        //add new question
                        if (String.IsNullOrEmpty(model.Type))
                            model.Type = "Public";
                        else if (model.Type.ToLower().Equals("public") || model.Type.ToLower().Equals("công khai"))
                            model.Type = "Public";
                        else if (model.Type.ToLower().Equals("private") || model.Type.ToLower().Equals("riêng tư"))
                            model.Type = "Private";
                        else
                            model.Type = "Public";

                        context.Questions.InsertOnSubmit(model);
                        context.SubmitChanges();

                        return new BaseReturnFunction<Database.Question.Question>(StatusFunction.TRUE, model);
                    }
                    else
                    {
                        return new BaseReturnFunction<Database.Question.Question>(StatusFunction.FALSE, "This question already exists / Câu hỏi này đã tồn tại");
                    }

                }
            }
            catch (Exception e)
            {
                return new BaseReturnFunction<Database.Question.Question>(StatusFunction.EXCEPTION, e.Message);
            }
        }

        public BaseReturnFunction<bool> Delete(int id)
        {
            try
            {
                using (var context = new QuestionDbDataContext())
                {
                    var question = context.Questions.SingleOrDefault(x => x.ID == id);
                    if (question != null)
                    {
                        context.Questions.DeleteOnSubmit(question);
                        context.SubmitChanges();
                    }
                }
                return new BaseReturnFunction<bool>(StatusFunction.TRUE, true);
            }
            catch (Exception e)
            {
                return new BaseReturnFunction<bool>(StatusFunction.EXCEPTION, e.Message);
            }
        }

        public BaseReturnFunction<List<Database.Question.Question>> GetAll()
        {
            try
            {
                using (var context = new QuestionDbDataContext())
                {
                    IQueryable<Database.Question.Question> listQuestions;
                    listQuestions = context.Questions.Where(x => x.ID > 0);
                    if (listQuestions != null)
                    {
                        return new BaseReturnFunction<List<Database.Question.Question>>(StatusFunction.TRUE, listQuestions.ToList(), listQuestions.Count());
                    }
                    return new BaseReturnFunction<List<Database.Question.Question>>(StatusFunction.FALSE, "List of questions is empty/Danh sách câu hỏi là trống.");
                }
            }
            catch (Exception e)
            {
                return new BaseReturnFunction<List<Database.Question.Question>>(StatusFunction.EXCEPTION, e.Message);
            }
        }

        //get list of questions by user
        public BaseReturnFunction<List<Database.Question.Question>> GetMyQuestion(string username)
        {
            try
            {
                if (String.IsNullOrWhiteSpace(username))
                    return new BaseReturnFunction<List<Database.Question.Question>>(StatusFunction.FALSE, "Tài khoản là null hoặc trống.");

                using (var context = new QuestionDbDataContext())
                {
                    IQueryable<Database.Question.Question> listQuestions;
                    listQuestions = context.Questions.Where(x => x.CreatedBy != null && x.CreatedBy.ToLower().Equals(username.ToLower())).OrderByDescending(x => x.CreatedDate);
                    if (listQuestions != null)
                    {
                        return new BaseReturnFunction<List<Database.Question.Question>>(StatusFunction.TRUE, listQuestions.ToList(), listQuestions.Count());
                    }
                    return new BaseReturnFunction<List<Database.Question.Question>>(StatusFunction.FALSE, "List of questions is empty/Danh sách câu hỏi là trống.");
                }
            }
            catch (Exception e)
            {
                return new BaseReturnFunction<List<Database.Question.Question>>(StatusFunction.EXCEPTION, e.Message);
            }
        }

        //get list public questions
        public BaseReturnFunction<List<Database.Question.Question>> GetCommonQuestion()
        {
            try
            {
                using (var context = new QuestionDbDataContext())
                {
                    IQueryable<Database.Question.Question> listQuestions;
                    listQuestions = context.Questions.Where(x => x.Type != null && x.Type.ToLower().Equals("public")).OrderByDescending(x => x.CreatedDate);
                    if (listQuestions != null)
                    {
                        return new BaseReturnFunction<List<Database.Question.Question>>(StatusFunction.TRUE, listQuestions.ToList(), listQuestions.Count());
                    }
                    return new BaseReturnFunction<List<Database.Question.Question>>(StatusFunction.FALSE, "List of questions is empty/Danh sách câu hỏi là trống.");
                }
            }
            catch (Exception e)
            {
                return new BaseReturnFunction<List<Database.Question.Question>>(StatusFunction.EXCEPTION, e.Message);
            }
        }

        public BaseReturnFunction<Database.Question.Question> GetById(int id)
        {
            try
            {
                using (var context = new QuestionDbDataContext())
                {
                    var question = context.Questions.SingleOrDefault(x => x.ID == id);
                    if (question != null)
                    {
                        return new BaseReturnFunction<Database.Question.Question>(StatusFunction.TRUE, question);
                    }
                    return new BaseReturnFunction<Database.Question.Question>(StatusFunction.FALSE, "Does not exist question with ID is/Không tồn tại câu hỏi với ID là: " + id);
                }
            }
            catch (Exception e)
            {
                return new BaseReturnFunction<Database.Question.Question>(StatusFunction.EXCEPTION, e.Message);
            }
        }

        public BaseReturnFunction<List<Database.Question.Question>> Sync(double time)
        {
            throw new NotImplementedException();
        }

        public BaseReturnFunction<Database.Question.Question> Update(int id, Database.Question.Question model)
        {
            try
            {
                using (var context = new QuestionDbDataContext())
                {
                    Database.Question.Question question = context.Questions.SingleOrDefault(x => x.ID == id);
                    if (question != null)
                    {
                        question.Title = model.Title;

                        question.Content = model.Content;
                        question.Answer = model.Answer;
                        question.CreatedBy = model.CreatedBy;
                        question.CreatedDate = model.CreatedDate;


                        if (!String.IsNullOrEmpty(model.Type))
                        {
                            if (model.Type.ToLower().Equals("public") || model.Type.ToLower().Equals("công khai"))
                                question.Type = "Public";
                            else if (model.Type.ToLower().Equals("private") || model.Type.ToLower().Equals("riêng tư"))
                                question.Type = "Private";
                        }


                        context.SubmitChanges();
                        return new BaseReturnFunction<Database.Question.Question>(StatusFunction.TRUE, question);
                    }
                }

                return new BaseReturnFunction<Database.Question.Question>(StatusFunction.FALSE, "Không tồn tại câu hỏi với id = " + id);
            }
            catch (Exception e)
            {
                return new BaseReturnFunction<Database.Question.Question>(StatusFunction.EXCEPTION, e.Message);
            }
        }

        public BaseReturnFunction<List<Database.Question.Question>> Search(int page, int size, string keyword)
        {
            try
            {
                using (var context = new QuestionDbDataContext())
                {
                    IQueryable<Database.Question.Question> list;
                    if (String.IsNullOrWhiteSpace(keyword))
                        list = context.Questions.OrderByDescending(x => x.CreatedDate);
                    else
                    {
                        keyword = keyword.Trim().ToLower();
                        list = context.Questions.Where(x => (x.Title != null && x.Title.ToLower().Contains(keyword.ToLower())) || (x.Content != null && x.Content.ToLower().Contains(keyword.ToLower())) || (x.Answer != null && x.Answer.ToLower().Contains(keyword.ToLower()))).OrderByDescending(x => x.CreatedDate);
                    }
                    return new BaseReturnFunction<List<Database.Question.Question>>(StatusFunction.TRUE, list.Skip((page - 1) * size).Take(size).ToList(), list.Count());
                }
            }
            catch (Exception e)
            {
                return new BaseReturnFunction<List<Database.Question.Question>>(StatusFunction.EXCEPTION, e.Message);
            }
        }

        public BaseReturnFunction<bool> IsExisted(string username, string title, string content)
        {
            try
            {
                if (String.IsNullOrWhiteSpace(title) && String.IsNullOrWhiteSpace(content))
                    return new BaseReturnFunction<bool>(StatusFunction.FALSE, false);

                using (var context = new QuestionDbDataContext())
                {
                    IQueryable<Database.Question.Question> listQuestions;
                    listQuestions = context.Questions.Where(x => !String.IsNullOrWhiteSpace(x.CreatedBy) && x.CreatedBy.ToLower().Equals(username.ToLower()) && ((!String.IsNullOrWhiteSpace(title) && x.Title.Equals(title.ToLower())) || (!String.IsNullOrWhiteSpace(content) && x.Content.Equals(content.ToLower()))));
                    if (listQuestions != null)
                    {
                        return new BaseReturnFunction<bool>(StatusFunction.TRUE, true);
                    }
                }
                return new BaseReturnFunction<bool>(StatusFunction.FALSE, false);
            }
            catch (Exception e)
            {
                return new BaseReturnFunction<bool>(StatusFunction.EXCEPTION, e.Message);
            }
        }

        public int GetPending()
        {
            try
            {
                using (var context = new QuestionDbDataContext())
                {

                    return context.Questions.Where(x=> x.Answer==null).ToList().Count();
                }
            }
            catch (Exception e)
            {
                return 0;
            }
        }
    }
}