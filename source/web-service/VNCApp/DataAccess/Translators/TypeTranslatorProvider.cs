﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using VNCApp.Database.Translators.TypeTranslator;
using VNCApp.Models;

namespace VNCApp.DataAccess.Translators
{
    public class TypeTranslatorProvider : IProvider<TypeTranslator, int>
    {
        public BaseReturnFunction<List<TypeTranslator>> GetAll(int page, int size, int type, string keyword)
        {
            try
            {
                using (var context = new TypeTranslatorDbDataContext())
                {
                    var list = context.TypeTranslators.Where(x => (type == 0 || x.Category == type));
                    if (!String.IsNullOrWhiteSpace(keyword))
                    {
                        keyword = keyword.ToLower();
                        list = list.Where(x => x.Name.ToLower().Contains(keyword) || x.Name2.ToLower().Contains(keyword));
                    }
                    return new BaseReturnFunction<List<TypeTranslator>>(StatusFunction.TRUE, list.Skip((page - 1) * size).Take(size).ToList(), list.Count());
                }
            }
            catch (Exception e)
            {
                return new BaseReturnFunction<List<TypeTranslator>>(StatusFunction.EXCEPTION, e.Message);
            }
        }
        public BaseReturnFunction<List<TypeTranslator>> GetAllByCategory(int id)
        {
            try
            {
                using (var context = new TypeTranslatorDbDataContext())
                {
                    var list = context.TypeTranslators.Where(x=> x.Category == id);
                    return new BaseReturnFunction<List<TypeTranslator>>(StatusFunction.TRUE, list.ToList(), list.Count());
                }
            }
            catch (Exception e)
            {
                return new BaseReturnFunction<List<TypeTranslator>>(StatusFunction.EXCEPTION, e.Message);
            }
        }

        public BaseReturnFunction<List<TypeTranslator>> Search(int page, int size, string keyword)
        {
            try
            {
                using (var context = new TypeTranslatorDbDataContext())
                {
                    IQueryable<TypeTranslator> list;
                    if (String.IsNullOrWhiteSpace(keyword))
                        list = context.TypeTranslators;
                    else
                    {
                        keyword = keyword.Trim().ToLower();
                        list = context.TypeTranslators.Where(x => x.Name.ToLower().Contains(keyword) || x.Name2.ToLower().Contains(keyword));
                    }
                    return new BaseReturnFunction<List<TypeTranslator>>(StatusFunction.TRUE, list.Skip((page - 1) * size).Take(size).ToList(), list.Count());
                }
            }
            catch (Exception e)
            {
                return new BaseReturnFunction<List<TypeTranslator>>(StatusFunction.EXCEPTION, e.Message);
            }
        }
        public BaseReturnFunction<Boolean> Delete(int id)
        {
            try
            {
                using (var context = new TypeTranslatorDbDataContext())
                {
                    var cat = context.TypeTranslators.SingleOrDefault(x => x.ID == id);
                    if (cat != null)
                    {
                        context.TypeTranslators.DeleteOnSubmit(cat);
                        context.SubmitChanges();
                    }
                }
                return new BaseReturnFunction<bool>(StatusFunction.TRUE, true);
            }
            catch (Exception e)
            {
                return new BaseReturnFunction<bool>(StatusFunction.EXCEPTION, e.Message);
            }
        }

        public BaseReturnFunction<TypeTranslator> GetById(int id)
        {
            try
            {
                using (var context = new TypeTranslatorDbDataContext())
                {
                    var cat = context.TypeTranslators.SingleOrDefault(x => x.ID == id);
                    if (cat != null)
                        return new BaseReturnFunction<TypeTranslator>(StatusFunction.TRUE, cat);
                    return new BaseReturnFunction<TypeTranslator>(StatusFunction.TRUE, "Không tồn tại id = " + id);
                }
            }
            catch (Exception e)
            {
                return new BaseReturnFunction<TypeTranslator>(StatusFunction.EXCEPTION, e.Message);
            }
        }

        public BaseReturnFunction<TypeTranslator> Create(TypeTranslator model)
        {
            try
            {
                using (var context = new TypeTranslatorDbDataContext())
                {
                    context.TypeTranslators.InsertOnSubmit(model);
                    context.SubmitChanges();
                }
                return new BaseReturnFunction<TypeTranslator>(StatusFunction.TRUE, model);
            }
            catch (Exception e)
            {
                return new BaseReturnFunction<TypeTranslator>(StatusFunction.EXCEPTION, e.Message);
            }
        }

        public BaseReturnFunction<TypeTranslator> Update(int id, TypeTranslator model)
        {
            try
            {
                using (var context = new TypeTranslatorDbDataContext())
                {
                    TypeTranslator TypeTranslator = context.TypeTranslators.SingleOrDefault(x => x.ID == id);
                    if (TypeTranslator != null)
                    {
                        TypeTranslator.Name = model.Name;
                        TypeTranslator.Name2 = model.Name2;
                        TypeTranslator.Category = model.Category;
                        context.SubmitChanges();
                        return new BaseReturnFunction<TypeTranslator>(StatusFunction.TRUE, TypeTranslator);
                    }
                }
                return new BaseReturnFunction<TypeTranslator>(StatusFunction.FALSE, "Không tồn tại id = " + id);
            }
            catch (Exception e)
            {
                return new BaseReturnFunction<TypeTranslator>(StatusFunction.EXCEPTION, e.Message);
            }
        }

        public BaseReturnFunction<List<TypeTranslator>> Sync(double time)
        {
            try
            {
                using (var context = new TypeTranslatorDbDataContext())
                {
                    return new BaseReturnFunction<List<TypeTranslator>>(StatusFunction.TRUE, context.TypeTranslators.ToList());
                }
            }
            catch (Exception e)
            {
                return new BaseReturnFunction<List<TypeTranslator>>(StatusFunction.EXCEPTION, e.Message);
            }
        }

        public BaseReturnFunction<List<TypeTranslator>> GetAll()
        {
            throw new NotImplementedException();
        }
    }
}