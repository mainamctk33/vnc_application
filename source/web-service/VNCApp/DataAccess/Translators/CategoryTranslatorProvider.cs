﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using VNCApp.Database.Translators.CategoryTranslator;
using VNCApp.Models;

namespace VNCApp.DataAccess.Translators
{
    public class CategoryTranslatorProvider : IProvider<CategoryTranslator, int>
    {
        public BaseReturnFunction<List<CategoryTranslator>> GetAll()
        {
            try
            {
                using (var context = new CategoryTranslatorDbDataContext())
                {
                    var list = context.CategoryTranslators;
                    return new BaseReturnFunction<List<CategoryTranslator>>(StatusFunction.TRUE, list.ToList(), list.Count());
                }
            }
            catch (Exception e)
            {
                return new BaseReturnFunction<List<CategoryTranslator>>(StatusFunction.EXCEPTION, e.Message);
            }
        }

        public BaseReturnFunction<List<CategoryTranslator>> Search(int page, int size, string keyword)
        {
            try
            {
                using (var context = new CategoryTranslatorDbDataContext())
                {
                    IQueryable<CategoryTranslator> list;
                    if (String.IsNullOrWhiteSpace(keyword))
                        list = context.CategoryTranslators;
                    else
                    {
                        keyword = keyword.Trim().ToLower();
                        list = context.CategoryTranslators.Where(x => x.Name.ToLower().Contains(keyword)|| x.Name2.ToLower().Contains(keyword));
                    }
                    return new BaseReturnFunction<List<CategoryTranslator>>(StatusFunction.TRUE, list.Skip((page - 1) * size).Take(size).ToList(), list.Count());
                }
            }
            catch (Exception e)
            {
                return new BaseReturnFunction<List<CategoryTranslator>>(StatusFunction.EXCEPTION, e.Message);
            }
        }
        public BaseReturnFunction<Boolean> Delete(int id)
        {
            try
            {
                using (var context = new CategoryTranslatorDbDataContext())
                {
                    var cat = context.CategoryTranslators.SingleOrDefault(x => x.ID == id);
                    if (cat != null)
                    {
                        context.CategoryTranslators.DeleteOnSubmit(cat);
                        context.SubmitChanges();
                    }
                }
                return new BaseReturnFunction<bool>(StatusFunction.TRUE, true);
            }
            catch (Exception e)
            {
                return new BaseReturnFunction<bool>(StatusFunction.EXCEPTION, e.Message);
            }
        }

        public BaseReturnFunction<CategoryTranslator> GetById(int id)
        {
            try
            {
                using (var context = new CategoryTranslatorDbDataContext())
                {
                    var cat = context.CategoryTranslators.SingleOrDefault(x => x.ID == id);
                    if (cat != null)
                        return new BaseReturnFunction<CategoryTranslator>(StatusFunction.TRUE, cat);
                    return new BaseReturnFunction<CategoryTranslator>(StatusFunction.TRUE, "Không tồn tại id = " + id);
                }
            }
            catch (Exception e)
            {
                return new BaseReturnFunction<CategoryTranslator>(StatusFunction.EXCEPTION, e.Message);
            }
        }

        public BaseReturnFunction<CategoryTranslator> Create(CategoryTranslator model)
        {
            try
            {
                using (var context = new CategoryTranslatorDbDataContext())
                {
                    context.CategoryTranslators.InsertOnSubmit(model);
                    context.SubmitChanges();
                }
                return new BaseReturnFunction<CategoryTranslator>(StatusFunction.TRUE, model);
            }
            catch (Exception e)
            {
                return new BaseReturnFunction<CategoryTranslator>(StatusFunction.EXCEPTION, e.Message);
            }
        }

        public BaseReturnFunction<CategoryTranslator> Update(int id, CategoryTranslator model)
        {
            try
            {
                using (var context = new CategoryTranslatorDbDataContext())
                {
                    CategoryTranslator CategoryTranslator = context.CategoryTranslators.SingleOrDefault(x => x.ID == id);
                    if (CategoryTranslator != null)
                    {
                        CategoryTranslator.Name = model.Name;
                        CategoryTranslator.Name2 = model.Name2;
                        context.SubmitChanges();
                        return new BaseReturnFunction<CategoryTranslator>(StatusFunction.TRUE, CategoryTranslator);
                    }
                }
                return new BaseReturnFunction<CategoryTranslator>(StatusFunction.FALSE, "Không tồn tại id = " + id);
            }
            catch (Exception e)
            {
                return new BaseReturnFunction<CategoryTranslator>(StatusFunction.EXCEPTION, e.Message);
            }
        }

        public BaseReturnFunction<List<CategoryTranslator>> Sync(double time)
        {
            try
            {
                using (var context = new CategoryTranslatorDbDataContext())
                {
                    return new BaseReturnFunction<List<CategoryTranslator>>(StatusFunction.TRUE, context.CategoryTranslators.ToList());
                }
            }
            catch (Exception e)
            {
                return new BaseReturnFunction<List<CategoryTranslator>>(StatusFunction.EXCEPTION, e.Message);
            }
        }

    }
}