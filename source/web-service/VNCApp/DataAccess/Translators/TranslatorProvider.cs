﻿using BaseLibrary.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using VNCApp.Database.Translators.Translator;
using VNCApp.Models;

namespace VNCApp.DataAccess.Translators
{

    public class TranslatorProvider : IProvider<Translator, int>
    {
        public BaseReturnFunction<Translator> Create(Translator model)
        {
            try
            {
                using (var context = new TranslatorDbDataContext())
                {
                    context.Translators.InsertOnSubmit(model);
                    context.SubmitChanges();
                }
                return new BaseReturnFunction<Translator>(StatusFunction.TRUE, model);
            }
            catch (Exception e)
            {
                return new BaseReturnFunction<Translator>(StatusFunction.EXCEPTION, e.Message);
            }
        }
        public BaseReturnFunction<bool> Delete(int id)
        {
            try
            {
                using (var context = new TranslatorDbDataContext())
                {
                    var data = context.Translators.SingleOrDefault(x => x.ID == id);
                    if (data != null)
                    {
                        context.Translators.DeleteOnSubmit(data);
                        context.SubmitChanges();
                    }
                    else
                        return new BaseReturnFunction<bool>(StatusFunction.FALSE, "Không tồn tại id = " + id);
                }
                return new BaseReturnFunction<bool>(StatusFunction.TRUE, true);
            }
            catch (Exception e)
            {

                return new BaseReturnFunction<bool>(StatusFunction.EXCEPTION, e.Message);
            }
        }

        public BaseReturnFunction<List<Translator>> GetAll()
        {
            throw new NotImplementedException();
        }
        public BaseReturnFunction<List<Translator>> GetAll(int page, int size, int type, int category, string keyword, string userId)
        {
            try
            {
                using (var context = new TranslatorDbDataContext())
                {
                    IQueryable<Translator> list;
                    if (category > 0)
                    {
                        list = context.Translators.Where(x => x.TypeTranslator.Category == category);
                    }
                    else
                    {
                        if (type == -1)
                            if (!String.IsNullOrWhiteSpace(userId))
                                list = context.BookmarkTranslators.Where(x => x.BookmarkBy == userId).OrderBy(x => x.BookmarkDate).Select(x => x.Translator);
                            else
                                list = context.Translators;
                        else
                            list = context.Translators.Where(x => type == 0 || x.Type == type);
                    }
                    // list = list.Where(x => String.IsNullOrWhiteSpace(keyword) || (x.ContentEN.Contains(keyword) || x.ContentVN.Contains(keyword) || x.ContentGE.Contains(keyword)));
                    if (!string.IsNullOrEmpty(keyword))
                        list = list.Where(x=> x.ContentVN.ToLower().Contains(keyword) || x.ContentEN.ToLower().Contains(keyword));
                    int count = list.Count();
                    return new BaseReturnFunction<List<Translator>>(StatusFunction.TRUE,
                        list.OrderByDescending(x => x.CreatedDate).Skip((page - 1) * size).Take(size).ToList(), count);
                }
            }
            catch (Exception e)
            {
                return new BaseReturnFunction<List<Translator>>(StatusFunction.EXCEPTION, e.Message);
            }
        }
        public BaseReturnFunction<object> GetAllNew()
        {
            try
            {
                using (var context = new TranslatorDbDataContext())
                {
                    IQueryable<Translator> list;
                        list = context.Translators.OrderByDescending(x=>x.CreatedDate);
                    int count = list.Count();
                    return new BaseReturnFunction<object>(StatusFunction.TRUE,
                        list.Select(x => new { Translator = x, Category = x.TypeTranslator.Category.Value }).ToList(), count);
                }
            }
            catch (Exception e)
            {
                return new BaseReturnFunction<object>(StatusFunction.EXCEPTION, e.Message);
            }
        }
        public BaseReturnFunction<Translator> GetById(int id)
        {
            try
            {
                using (var context = new TranslatorDbDataContext())
                {

                    var data = context.Translators.SingleOrDefault(x => x.ID == id);
                    if (data != null)
                    {
                        return new BaseReturnFunction<Translator>(StatusFunction.TRUE, data);
                    }
                    return new BaseReturnFunction<Translator>(StatusFunction.FALSE, "Không tồn tại id = " + id);
                }
            }
            catch (Exception e)
            {

                return new BaseReturnFunction<Translator>(StatusFunction.EXCEPTION, e.Message);
            }
        }

        public BaseReturnFunction<List<Translator>> Sync(double time)
        {
            throw new NotImplementedException();
        }
        public BaseReturnFunction<Translator> Update(int id, Translator model)
        {
            try
            {
                using (var context = new TranslatorDbDataContext())
                {
                    Translator data = context.Translators.SingleOrDefault(x => x.ID == id);
                    if (data != null)
                    {
                        data.ContentEN = model.ContentEN;
                        data.ContentGE = model.ContentGE;
                        data.ContentVN = model.ContentVN;
                        data.CreatedDate = model.CreatedDate;
                        data.IsDeleted = model.IsDeleted;
                        data.UpdatedDate = DateTime.Now.GetUnixTimeStamp();
                        data.Type = model.Type;
                        context.SubmitChanges();
                        return new BaseReturnFunction<Translator>(StatusFunction.TRUE, data);
                    }
                    return new BaseReturnFunction<Translator>(StatusFunction.FALSE, "Không tồn tại id = " + id);
                }
            }
            catch (Exception e)
            {
                return new BaseReturnFunction<Translator>(StatusFunction.EXCEPTION, e.Message);
            }
        }
        public BaseReturnFunction<Boolean> Bookmark(int id, bool bookmark, string requestUserId)
        {
            try
            {
                using (var context = new TranslatorDbDataContext())
                {
                    if (!bookmark)
                    {
                        var bm = context.BookmarkTranslators.SingleOrDefault(x => x.BookmarkBy == requestUserId && x.ID == id);
                        if (bm != null)
                        {
                            context.BookmarkTranslators.DeleteOnSubmit(bm);
                            context.SubmitChanges();
                        }
                        return new BaseReturnFunction<bool>(StatusFunction.TRUE, true);
                    }
                    else
                    {
                        context.BookmarkTranslators.InsertOnSubmit(new BookmarkTranslator()
                        {
                            ID = id,
                            BookmarkBy = requestUserId,
                            BookmarkDate = DateTime.Now.GetUnixTimeStamp()
                        });
                        context.SubmitChanges();
                        return new BaseReturnFunction<bool>(StatusFunction.TRUE, true);
                    }
                }
            }
            catch (Exception e)
            {
                return new BaseReturnFunction<bool>(StatusFunction.EXCEPTION, e.Message);
            }
        }

      
    }
}