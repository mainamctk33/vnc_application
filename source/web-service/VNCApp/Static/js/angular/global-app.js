﻿var ngApp = angular.module('vncApp', ['ngSanitize', 'ui.select2', 'ngValidate'])
    .directive('ckeditor', function ($rootScope) {
        return {
            require: 'ngModel',
            link: function (scope, element, attr, ngModel) {
                // enable ckeditor
                setTimeout(function () {
                    var ckeditor = CKEditorSetup.Show(attr.id);

                    ckeditor.on('change', function () {
                        ngModel.$setViewValue(this.getData());
                    });
                }, 1000);
            }
        };
    })
    .directive('ngRepeatDone', function () {
        return function (scope, element, attrs) {
            if (scope.$last) { // all are rendered
                scope.$eval(attrs.ngRepeatDone);
            }
        }
    }).directive('ngEnter', function () {
        return function (scope, element, attrs) {
            element.bind("keydown keypress", function (event) {
                if (event.which === 13) {
                    scope.$apply(function () {
                        scope.$eval(attrs.ngEnter);
                    });

                    event.preventDefault();
                }
            });
        };
    })
    .directive('ngFileChange', function () {
        return {
            restrict: 'A',
            link: function ($scope, element, attrs) {
                var onChangeFunc = $scope.$eval(attrs.ngFileChange);
                element.bind('change', function (event) {
                    onChangeFunc();
                    $scope.$apply();
                });
            }
        };
    }).directive('compile',
    function ($compile, $parse) {
        return {
            restrict: 'A',
            link: function (scope, element, attr) {
                var parsed = $parse(attr.ngBindHtml);

                function getStringValue() {
                    return (parsed(scope) || '').toString();
                }

                // Recompile if the template changes
                scope.$watch(getStringValue,
                    function () {
                        $compile(element,
                            null,
                            -9999)(
                            scope); // The -9999 makes it skip directives so that we do not recompile ourselves
                    });

                //$timeout(function () {
                //    debugger;
                //    $compile(elem.contents())(scope);
                //});
            }
        };
    });


ngApp.controller('vncController',
	function ($scope, $http, $sce) {
        $scope.pageView = "";
        $scope.view = function (a) {
            drawBreadCrumb();
            $scope.pageView =
                $sce.trustAsHtml(
                    '<h1 class="ajax-loading-animation"><i class="fa fa-cog fa-spin"></i> Loading...</h1>');
            $http({
                url: a,
                method: "get"
            }).then(function successCallback(response) {
                $scope.viewPage = $sce.trustAsHtml(response.data);
            },
                function errorCallback(response) {
                    $scope.viewPage = "Lỗi";
                });
		}
    });


ngApp.controller('typeNewsController',
    function ($scope, $http, $sce) {
        $scope.totalPage = 10;
        $scope.keyword = "";
        $scope.total = 0;
        $scope.listPage = [];
        $scope.page = 0;
        $scope.size = 10;
        $scope.data = [];
        $scope.updatemode = false;
        $scope.changeToUpdateMode = function (item) {
            item.updating = true;
			item.tempName = item.Name;
			item.tempName2 = item.Name2;
        }

        $scope.changeToCreatedMode = function () {
            $scope.createdmode = true;
            $scope.tempCreatedItem = {};
        }
        $scope.cancelCreate = function (item) {
            $scope.createdmode = false;
        }

        $scope.createNew = function () {
            confirmOKCancel("Xác nhận", "Bạn có muốn lưu chuyên mục mới", "Có", "Không", function () {
                $http({
                    method: 'POST',
                    url: "/typenews/create",
                    data: $scope.tempCreatedItem
                }).then(function successCallback(response) {
                    if (response.data.IsTrue) {
                        $scope.createdmode = false;
                        $scope.data.splice(0, 0, response.data.Data);
						item.Name2 = item.tempName2;
						item.Name = item.tempName;
						$scope.updatemode = false;
						item.updating = false;
                        $scope.$apply();
						alertSmallBox("lưu thành công", success);
                    } else {
                        alertSmallBox("lưu không thành công", error);
                    }
                }, function errorCallback(response) {
                    alertSmallBox("lưu không thành công", error);
                });
            });
        }

        $scope.cancel = function (item) {
            $scope.createdmode = false;
            item.updating = false;
        }

        $scope.save = function (item) {
            confirmOKCancel("Xác nhận", "Bạn có muốn lưu thay đổi", "Có", "Không", function () {
                $http({
                    method: 'PUT',
                    url: "/typenews/update/" + item.ID,
                    data: {
                        ID: item.ID,
						Name: item.tempName,
						Name2: item.tempName2
                    }
                }).then(function successCallback(response) {
                    if (response.data.IsTrue) {
                        alertSmallBox("Cập nhật thành công", success);
                        $scope.updatemode = false;
                        item.updating = false;
						item.Name = item.tempName;
						item.Name2 = item.tempName2;
                        $scope.$apply();
                    } else {
                        alertSmallBox("Cập nhật không thành công", error);
                    }
                }, function errorCallback(response) {
                    alertSmallBox("Cập nhật không thành công", error);
                });
            });
        }

        $scope.loadPage = function (page) {
            $scope.page = page;
            $http({
                method: 'GET',
                url: "/typenews/search?keyword=" + $scope.keyword + "&page=" + $scope.page + "&size=" + $scope.size
            }).then(function successCallback(response) {
                if (response.data.IsTrue) {
                    $scope.listPage = [];
                    $scope.total = response.data.Total;
                    $scope.data = response.data.Data;
                    var temp = ($scope.total) / $scope.size;
                    var totalPage = Math.round(temp, 0);
                    $scope.totalPage = temp > totalPage ? totalPage + 1 : totalPage;
                    for (var i = $scope.page - 10; i < $scope.page + 10; i++) {
                        if (i > 0 && i <= $scope.totalPage)
                            $scope.listPage.push(i);
                    }
                }
            }, function errorCallback(response) {

            });
        }
        $scope.remove = function (item) {
            confirmOKCancel("Xác nhận!", "Bạn có muốn xóa chuyên mục này?", "Có", "Không", function () {
                $http({
                    method: 'delete',
                    url: "/typenews/delete/" + item.ID
                }).then(function successCallback(response) {
                    if (response.data.IsTrue) {
                        var index = $scope.data.indexOf(item);
                        if (index !== -1) {
                            $scope.data.splice(index, 1);
                        }
                        alertSmallBox("Xóa thành công", success);
                        $scope.$apply();
                    } else {
                        alertSmallBox("Xóa không thành công", error);
                    }
                }, function errorCallback(response) {

                });
            });
        }
    });

ngApp.controller('typeLocationController',
    function ($scope, $http, $sce) {
        $scope.totalPage = 10;
        $scope.keyword = "";
        $scope.total = 0;
        $scope.listPage = [];
        $scope.page = 0;
        $scope.size = 10;
        $scope.data = [];
        $scope.updatemode = false;
        $scope.fileSelected = "";
        $scope.locationType = {};
        $scope.selectImage = function () {
            var selector = $(event.target);
            var fileName = selector[0].value.replace("C:\\fakepath\\", "").toLocaleLowerCase();
            if (common.endWith(fileName, ".jpg") ||
                common.endWith(fileName, ".png") ||
                common.endWith(fileName, ".gif")) {
                $scope.fileSelected = selector[0].value.replace("C:\\fakepath\\", "");
                var filePath = URL.createObjectURL(selector[0].files[0]);
                $scope.locationType.Logo = filePath;
            } else {
                alertSmallBox("Vui lòng chọn hình ảnh có định dạng png, jpg, gif", "error");
            }
            $scope.$apply();
        }

        $scope.showCreateModal = function () {
            $scope.locationType = {};
            $scope.currentItem = null;
            $("#modal-edit-location-type").modal("show")
        }

        $scope.changeToUpdateMode = function (item) {
            $scope.locationType = JSON.parse(JSON.stringify(item));
            $scope.currentItem = item;
            $("#modal-edit-location-type").modal("show")
        }

        $scope.loadPage = function (page) {
            $scope.page = page;
            $http({
                method: 'GET',
                url: "/typelocation/search?keyword=" + $scope.keyword + "&page=" + $scope.page + "&size=" + $scope.size
            }).then(function successCallback(response) {
                if (response.data.IsTrue) {
                    $scope.listPage = [];
                    $scope.total = response.data.Total;
                    $scope.data = response.data.Data;
                    var temp = ($scope.total) / $scope.size;
                    var totalPage = Math.round(temp, 0);
                    $scope.totalPage = temp > totalPage ? totalPage + 1 : totalPage;
                    for (var i = $scope.page - 10; i < $scope.page + 10; i++) {
                        if (i > 0 && i <= $scope.totalPage)
                            $scope.listPage.push(i);
                    }
                }
            }, function errorCallback(response) {

            });
        }
        $scope.remove = function (item) {
            confirmOKCancel("Xác nhận!", "Bạn có muốn xóa loại địa điểm này?", "Có", "Không", function () {
                $http({
                    method: 'delete',
                    url: "/typelocation/delete/" + item.ID
                }).then(function successCallback(response) {
                    if (response.data.IsTrue) {
                        var index = $scope.data.indexOf(item);
                        if (index !== -1) {
                            $scope.data.splice(index, 1);
                        }
                        alertSmallBox("Xóa thành công", success);
                        $scope.$apply();
                    } else {
                        alertSmallBox("Xóa không thành công", error);
                    }
                }, function errorCallback(response) {

                });
            });
        }
        $scope.save = function () {
            if ($scope.locationType.Name.trim() == "") {
                alertSmallBox("Vui lòng nhập tên loại địa điểm", "error")
                return;
            }
            if (!$scope.locationType.Logo) {
                alertSmallBox("Vui lòng chọn icon cho loại địa điểm", "error")
                return;
            }

            if ($("#select-file-image").val() !== "") {
                var formData = new FormData();
                formData.append('file', $("#select-file-image")[0].files[0]);
                $("body").addClass("loading");
                $.ajax({
                    type: 'POST',
                    url: "/api/FileManager/Upload",
                    data: formData,
                    processData: false, // tell jQuery not to process the data
                    contentType: false, // tell jQuery not to set contentType
                    success: function (data) {
                        if (data.IsTrue) {
                            $("body").removeClass("loading");
                            $scope.locationType.Logo = data.Data[0];
                            $scope.submit();
                        } else {
                            alertSmallBox(data.message, "error");
                        }
                    },
                    beforeSend: function () {
                        $("body").addClass("loading");
                    },
                    error: function (data) {
                        alertSmallBox(data.message, "error");
                        $("body").removeClass("loading");
                    },
                    failure: function (data) {
                        $("body").removeClass("loading");
                    },
                    async: true
                });
            } else {
                $scope.submit();
            }
        }
        $scope.submit = function () {
            var item = $scope.locationType;
            $("body").addClass("loading");

            if (item.ID) {
                $http({
                    method: 'PUT',
                    url: "/typelocation/update/" + item.ID,
                    data: item
                }).then(function successCallback(response) {
                    $("body").removeClass("loading");
                    if (response.data.IsTrue) {
                        alertSmallBox("Cập nhật thành công", success);
                        $scope.currentItem.Name = item.Name;
                        $scope.currentItem.Logo = item.Logo;
                        $("#modal-edit-location-type").modal("hide");
                        $("body").removeClass("loading");
                    } else {
                        alertSmallBox("Cập nhật không thành công", error);
                    }
                }, function errorCallback(response) {
                    alertSmallBox("Cập nhật không thành công", error);
                });
            }
            else {
                $http({
                    method: 'POST',
                    url: "/typelocation/create",
                    data: item
                }).then(function successCallback(response) {
                    $("body").removeClass("loading");
                    if (response.data.IsTrue) {
                        $scope.data.splice(0, 0, response.data.Data);
                        alertSmallBox("lưu thành công", success);
                        $("#modal-edit-location-type").modal("hide");
                    } else {
                        alertSmallBox("lưu không thành công", error);
                    }
                }, function errorCallback(response) {
                    alertSmallBox("lưu không thành công", error);
                    $("body").removeClass("loading");
                });
            }
        }
    });
ngApp.controller('newsController',
    function ($scope, $http, $sce) {
        $scope.tag = "";
        $scope.totalPage = 10;
        $scope.keyword = "";
        $scope.total = 0;
        $scope.listPage = [];
        $scope.page = 0;
        $scope.size = 10;
        $scope.data = [];
        $scope.listType = [];
        $scope.loadListType = function () {
            $http({
                method: 'GET',
                url: "/typenews/getall"
            }).then(function successCallback(response) {
                if (response.data.IsTrue) {
                    $scope.listType = response.data.Data;
                }
            }, function errorCallback(response) {

            });
        }
        $scope.loadPage = function (page) {
            $scope.page = page;
            $http({
                method: 'GET',
                url: "/news/getall?keyword=" + $scope.keyword + "&page=" + $scope.page + "&size=" + $scope.size
            }).then(function successCallback(response) {
                if (response.data.IsTrue) {
                    $scope.listPage = [];
                    $scope.total = response.data.Total;
                    $scope.data = response.data.Data;
                    var temp = ($scope.total) / $scope.size;
                    var totalPage = Math.round(temp, 0);
                    $scope.totalPage = temp > totalPage ? totalPage + 1 : totalPage;
                    for (var i = $scope.page - 10; i < $scope.page + 10; i++) {
                        if (i > 0 && i <= $scope.totalPage)
                            $scope.listPage.push(i);
                    }
                }
            }, function errorCallback(response) {

            });
        }
        $scope.view = function (item) {

        }
        $scope.edit = function (item) {
            location.hash = "/admin/mgrnews/create/" + item.ID;
        }
        $scope.remove = function (item) {
            confirmOKCancel("Xác nhận!", "Bạn có muốn xóa bài viết này?", "Có", "Không", function () {
                $http({
                    method: 'delete',
                    url: "/news/delete/" + item.ID
                }).then(function successCallback(response) {
                    if (response.data.IsTrue) {
                        var index = $scope.data.indexOf(item);
                        if (index !== -1) {
                            $scope.data.splice(index, 1);
                        }
                        alertSmallBox("Xóa thành công", success);
                        $scope.$apply();
                    } else {
                        alertSmallBox("Xóa không thành công", error);
                    }
                }, function errorCallback(response) {

                });
            });
        }
    });
ngApp.controller('locationController',
    function ($scope, $http, $sce) {
        $scope.totalPage = 10;
        $scope.keyword = "";
        $scope.total = 0;
        $scope.listPage = [];
        $scope.page = 0;
        $scope.size = 10;
        $scope.data = [];
        $scope.searchName = "";
        $scope.searchType = "0";
        $scope.searchStatus = "-1";
        $scope.loadPage = function (page) {
            $scope.page = page;
            $http({
                method: 'GET',
                url: "/location/listlocation?name=" + $scope.searchName + "&status=" + $scope.searchStatus + "&type=" + $scope.searchType + "&page=" + $scope.page + "&size=" + $scope.size
            }).then(function successCallback(response) {
                if (response.data.IsTrue) {
                    $scope.listPage = [];
                    $scope.total = response.data.Total;
                    $scope.data = response.data.Data;
                    var temp = ($scope.total) / $scope.size;
                    var totalPage = Math.round(temp, 0);
                    $scope.totalPage = temp > totalPage ? totalPage + 1 : totalPage;
                    for (var i = $scope.page - 10; i < $scope.page + 10; i++) {
                        if (i > 0 && i <= $scope.totalPage)
                            $scope.listPage.push(i);
                    }
                }
            }, function errorCallback(response) {

            });
        }
        $scope.view = function (item) {

        }
        $scope.edit = function (item) {
            location.hash = "/admin/mgrlocation/create/" + item.ID;
        }
        $scope.viewLocation = function (lat, lon) {
            window.open("https://www.google.com/maps/search/?api=1&query=" + lat + "," + lon);
        }
        $scope.remove = function (item) {
            confirmOKCancel("Xác nhận!", "Bạn có muốn xóa địa điểm này?", "Có", "Không", function () {
                $http({
                    method: 'delete',
                    url: "/location/delete/" + item.ID
                }).then(function successCallback(response) {
                    if (response.data.IsTrue) {
                        var index = $scope.data.indexOf(item);
                        if (index !== -1) {
                            $scope.data.splice(index, 1);
                        }
                        alertSmallBox("Xóa thành công", success);
                        $scope.$apply();
                    } else {
                        alertSmallBox("Xóa không thành công", error);
                    }
                }, function errorCallback(response) {

                });
            });
        },
            $scope.approve = function (item) {
                debugger;
                confirmOKCancel("Xác nhận!", "Bạn có muốn phê duyệt địa điểm này?", "Có", "Không", function () {
                    $http({
                        method: 'put',
                        url: "/location/approve/" + item.ID,
                        data: {
                            approve: true
                        }
                    }).then(function successCallback(response) {
                        if (data.data.IsTrue) {
                            item.Approve = true;
                            item.ApproveDate = new Date().getTime();
                            alertSmallBox("Phê duyệt thành công", success);
                            $scope.$apply();
                        } else {
                            alertSmallBox("Phê duyệt không thành công", error);
                        }
                    }, function errorCallback(response) {

                    });
                });
            }
    });

ngApp.controller('createLocationController',
    function ($scope, $http, $sce) {
        $.validator.addMethod(
            "input_content",
            function (value, element) {
                debugger;
                return CKEditorSetup.GetData(element.id).trim() !== "";
            },
            "Vui lòng nhập nội dung"
        );

        $scope.validationOptions = {
            rules: {
                type:
                { required: true },
                title: {
                    required: true
                },
                content:
                {
                    input_content: true
                }
            },

            messages: {
                type:
                { required: "Vui lòng chọn chuyên mục" }
                ,
                title: {
                    required: "Vui lòng nhập tiêu đề"
                }
            }
        }
        $scope.fileSelected = "";
        $scope.selectImage = function () {
            var selector = $(event.target);
            var fileName = selector[0].value.replace("C:\\fakepath\\", "").toLocaleLowerCase();
            if (common.endWith(fileName, ".jpg") ||
                common.endWith(fileName, ".png") ||
                common.endWith(fileName, ".gif")) {
                $scope.fileSelected = selector[0].value.replace("C:\\fakepath\\", "");
                var filePath = URL.createObjectURL(selector[0].files[0]);
                $scope.location.Thumbnail = filePath;
            } else {
                alertSmallBox("Vui lòng chọn hình ảnh có định dạng png, jpg, gif", "error");
            }
            $scope.$apply();
        }
        $scope.submit = function () {
            if ($scope.location.ID != undefined) {
                $http({
                    method: 'PUT',
                    url: "/location/update/" + $scope.location.ID,
                    data:
                    {
                        model: $scope.location,
                    }
                }).then(function successCallback(response) {
                    if (response.data.IsTrue) {
                        alertSmallBox("Sửa bài viết thành công", success);
                        location.hash = "/admin/mgrlocation";
                    } else {
                        alertSmallBox("Sửa bài viết không thành công", error);
                    }
                },
                    function errorCallback(response) {
                        alertSmallBox("Sửa bài viết không thành công", error);
                    });
            } else {
                $http({
                    method: 'POST',
                    url: "/location/create",
                    data:
                    {
                        model: $scope.location,
                    }
                }).then(function successCallback(response) {
                    if (response.data.IsTrue) {
                        alertSmallBox("Đăng vài viết thành công", success);
                        location.hash = "/admin/mgrlocation";
                    } else {
                        alertSmallBox("Đăng vài viết không thành công", error);
                    }
                },
                    function errorCallback(response) {
                        alertSmallBox("Đăng vài viết không thành công", error);
                    });

            }
        }
        $scope.uploadImage = function () {
            if ($("#add-location-form").valid()) {
                if (!$scope.location.Content) {
                    alertSmallBox("Vui lòng nhập nội dung", error);
                    return;
                }
                if ($("#select-file-image-cover").val() === "" && !$scope.location.Thumbnail) {
                    alertSmallBox("Vui lòng chọn ảnh cần upload", error);
                    return;
                }
                if ($("#select-file-image-cover").val() !== "") {
                    var formData = new FormData();
                    formData.append('file', $("#select-file-image-cover")[0].files[0]);
                    $.ajax({
                        type: 'POST',
                        url: "/api/FileManager/Upload",
                        data: formData,
                        processData: false, // tell jQuery not to process the data
                        contentType: false, // tell jQuery not to set contentType
                        success: function (data) {
                            if (data.IsTrue) {
                                $("body").removeClass("loading");
                                $scope.location.Thumbnail = data.Data[0];
                                $scope.submit();
                            } else {
                                alertSmallBox("uploadImage>location.Thumbnail: " + data.message, error);
                            }
                        },
                        beforeSend: function () {
                            $("body").addClass("loading");
                            $("#modal-upload-image").modal("hide");
                        },
                        error: function (data) {
                            alertSmallBox("uploadImage>error: function: " + data.message, error);
                            $("body").removeClass("loading");
                        },
                        failure: function (data) {
                            $("body").removeClass("loading");
                        },
                        async: true
                    });
                } else {
                    $scope.submit();
                }
            }
        }
        $scope.loadListType = function () {
            $http({
                method: 'GET',
                url: "/typelocation/getall"
            }).then(function successCallback(response) {
                if (response.data.IsTrue) {
                    $scope.listType = response.data.Data;
                }
            }, function errorCallback(response) {

            });
        }
        $scope.loadListType();
    });

ngApp.controller('createNewsController',
    function ($scope, $http, $sce) {
        $.validator.addMethod(
            "input_content",
            function (value, element) {
                return CKEditorSetup.GetData(element.id).trim() !== "";
            },
            "Vui lòng nhập nội dung tin tức"
        );

        $scope.validationOptions = {
            rules: {
                type:
                { required: true },
                title: {
                    required: true
                },
                content:
                {
                    input_content: true
                },
                contentpreview: {
                    required: true
                }
            },

            messages: {
                type:
                { required: "Vui lòng chọn chuyên mục" }
                ,
                title: {
                    required: "Vui lòng nhập tiêu đề tin tức"
                },
                contentpreview: {
                    required: "Vui lòng nhập nội dung xem trước của tin tức"
                }
            }
        }
        $scope.fileSelected = "";
        $scope.selectImage = function () {
            var selector = $(event.target);
            var fileName = selector[0].value.replace("C:\\fakepath\\", "").toLocaleLowerCase();
            if (common.endWith(fileName, ".jpg") ||
                common.endWith(fileName, ".png") ||
                common.endWith(fileName, ".gif")) {
                $scope.fileSelected = selector[0].value.replace("C:\\fakepath\\", "");
                var filePath = URL.createObjectURL(selector[0].files[0]);
                $scope.news.Thumbnail = filePath;
            } else {
                alertSmallBox("Vui lòng chọn hình ảnh có định dạng png, jpg, gif", "error");
            }
            $scope.$apply();
        }
        $scope.submit = function () {
            if ($scope.news.ID != undefined) {
                $http({
                    method: 'PUT',
                    url: "/news/updateWithTag/" + $scope.news.ID,
                    data:
                    {
                        model: $scope.news,
                        tag: $scope.tag
                    }
                }).then(function successCallback(response) {
                    if (response.data.IsTrue) {
                        alertSmallBox("Sửa tin tức thành công", success);
                        location.hash = "/admin/mgrnews";
                    } else {
                        alertSmallBox("Sửa tin tức không thành công", error);
                    }
                },
                    function errorCallback(response) {
                        alertSmallBox("lưu không thành công", error);
                    });
            } else {
                $http({
                    method: 'POST',
                    url: "/news/createWithTag",
                    data:
                    {
                        model: $scope.news,
                        tag: $scope.tag
                    }
                }).then(function successCallback(response) {
                    if (response.data.IsTrue) {
                        alertSmallBox("Đăng bài thành công", success);
                        location.hash = "/admin/mgrnews";
                    } else {
                        alertSmallBox("Đăng bài không thành công", error);
                    }
                },
                    function errorCallback(response) {
                        alertSmallBox("Đăng bài không thành công", error);
                    });

            }
        }
        $scope.uploadImage = function () {
            if ($("#add-news-form").valid()) {
                if (!$scope.news.Content) {
                    alertSmallBox("Vui lòng nhập nội dung tin tức", error);
                    return;
                }
                if ($("#select-file-image-cover").val() === "" && !$scope.news.Thumbnail) {
                    alertSmallBox("Vui lòng chọn ảnh cần upload", "error");
                    return;
                }
                if ($("#select-file-image-cover").val() !== "") {


                    var formData = new FormData();
                    formData.append('file', $("#select-file-image-cover")[0].files[0]);
                    $.ajax({
                        type: 'POST',
                        url: "/api/FileManager/Upload",
                        data: formData,
                        processData: false, // tell jQuery not to process the data
                        contentType: false, // tell jQuery not to set contentType
                        success: function (data) {
                            if (data.IsTrue) {
                                $("body").removeClass("loading");
                                $scope.news.Thumbnail = data.Data[0];
                                $scope.submit();
                                //news.submit(data.data[0].ImageUrl);
                            } else {
                                alertSmallBox(data.message, "error");
                            }
                        },
                        beforeSend: function () {
                            $("body").addClass("loading");
                            $("#modal-upload-image").modal("hide");
                        },
                        error: function (data) {
                            alertSmallBox(data.message, "error");
                            $("body").removeClass("loading");
                        },
                        failure: function (data) {
                            $("body").removeClass("loading");
                        },
                        async: true
                    });
                } else {
                    $scope.submit();
                }
            }
        }
        $scope.loadListType = function () {
            $http({
                method: 'GET',
                url: "/typenews/getall"
            }).then(function successCallback(response) {
                if (response.data.IsTrue) {
                    $scope.listType = response.data.Data;
                }
            }, function errorCallback(response) {

            });
        }
        $scope.loadListType();
    });

ngApp.controller('jobController',
    function ($scope, $http, $sce) {
        $scope.tag = "";
        $scope.totalPage = 10;
        $scope.keyword = "";
        $scope.total = 0;
        $scope.listPage = [];
        $scope.page = 0;
        $scope.size = 10;
        $scope.data = [];
        $scope.listType = [];
        $scope.loadListType = function () {
            $http({
                method: 'GET',
                url: "/typejob/getall"
            }).then(function successCallback(response) {
                if (response.data.IsTrue) {
                    $scope.listType = response.data.Data;
                }
            }, function errorCallback(response) {

            });
        }

        $scope.loadPage = function (page) {
            $scope.page = page;
            $http({
                method: 'GET',
                url: "/job/getall?keyword=" + $scope.keyword + "&page=" + $scope.page + "&size=" + $scope.size
            }).then(function successCallback(response) {
                if (response.data.IsTrue) {
                    $scope.listPage = [];
                    $scope.total = response.data.Total;
                    $scope.data = response.data.Data;
                    var temp = ($scope.total) / $scope.size;
                    var totalPage = Math.round(temp, 0);
                    $scope.totalPage = temp > totalPage ? totalPage + 1 : totalPage;
                    for (var i = $scope.page - 10; i < $scope.page + 10; i++) {
                        if (i > 0 && i <= $scope.totalPage)
                            $scope.listPage.push(i);
                    }
                }
            }, function errorCallback(response) {

            });
        }
        $scope.view = function (item) {
            alertSmallBox("Chức năng đang phát triển", error);
        }
        $scope.edit = function (item) {
            location.hash = "/admin/mgrjob/create/" + item.ID;
        }
        $scope.approve = function (item) {
            // location.hash = "/admin/mgrjob/create/" + item.ID;
            confirmOKCancel("Xác nhận!", "Bạn có muốn duyệt công việc này?", "Có", "Không", function () {
                $http({
                    method: 'put',
                    url: "/job/approve/" + item.ID
                }).then(function successCallback(response) {
                    if (response.data.IsTrue) {
                        alertSmallBox("Duyệt thành công", success);
                        item.ApproveDate = new Date().getTime();
                    } else {
                        alertSmallBox("Duyệt không thành công " + response.data.Message, error);
                    }
                }, function errorCallback(response) {

                });
            });
        }
        $scope.remove = function (item) {
            confirmOKCancel("Xác nhận!", "Bạn có muốn xóa công việc này?", "Có", "Không", function () {
                $http({
                    method: 'delete',
                    url: "/job/delete/" + item.Job.ID
                }).then(function successCallback(response) {
                    if (response.data.IsTrue) {
                        var index = $scope.data.indexOf(item);
                        if (index !== -1) {
                            $scope.data.splice(index, 1);
                        }
                        alertSmallBox("Xóa thành công", success);
                        $scope.$apply();
                    } else {
                        alertSmallBox("Xóa không thành công", error);
                    }
                }, function errorCallback(response) {

                });
            });
        }
    });


ngApp.controller('createJobController',
    function ($scope, $http, $sce) {
        $scope.listCityTemp = [];
        $scope.validationOptions = {
            rules: {
                title: { required: true },
                country: { required: true },
                city: { required: true },
                type: { required: true },
                company: { required: true },
                level: { required: true },
                salary: { required: true },
                description: { required: true },
                requirement: { required: true },
                phone: { required: true },
                email: { required: true }

            },

            messages: {
                //title:{ required: "Vui lòng nhập tiêu đề công việc" },
                //country:{ required: "Vui lòng chọn quốc gia" },
                //city:{ required: "Vui lòng chọn thành phố" },
                //type:{ required: "Vui lòng chọn chuyên mục" },
                //company: { required: "Vui lòng chọn công ty" },
                //level: { required: "Vui lòng chọn chức vụ" },
                //salary: { required: "Vui lòng nhập mức lương" },
                //description: { required: "Vui lòng nhập mô tả công việc" },
                //requirement: { required: "Vui lòng nhập yêu cầu công việc" },
                //phone: { required: "Vui lòng nhập số điện thoại" },
                //email: { required: "Vui lòng nhập email" }

            }
        }
        $scope.fileSelected = "";
        $scope.selectImage = function () {
            var selector = $(event.target);
            var fileName = selector[0].value.replace("C:\\fakepath\\", "").toLocaleLowerCase();
            if (common.endWith(fileName, ".jpg") ||
                common.endWith(fileName, ".png") ||
                common.endWith(fileName, ".gif")) {
                $scope.fileSelected = selector[0].value.replace("C:\\fakepath\\", "");
                var filePath = URL.createObjectURL(selector[0].files[0]);
                $scope.job.Thumbnail = filePath;
            } else {
                alertSmallBox("Vui lòng chọn hình ảnh có định dạng png, jpg, gif", "error");
            }
            $scope.$apply();
        }
        $scope.selectCountry = function () {
            if ($scope.job && $scope.job.Country) {
                $scope.listCityTemp = $scope.listCity.filter(function (item) {
                    return item.Country == $scope.job.Country;
                });
            } else {
                $scope.listCityTemp = [];
            }
        }
        $scope.loadListCityTemp = function (cityIndex) {
            $http({
                method: 'GET',
                url: "/cityjob/search?country=" + cityIndex
            }).then(function successCallback(response) {
                if (response.data.IsTrue) {
                    $scope.listCityTemp = response.data.Data;
                }
            }, function errorCallback(response) {

            });
        }

        $scope.submit = function () {
            if ($scope.job.ID != undefined) {
                $http({
                    method: 'PUT',
                    url: "/job/update/" + $scope.job.ID,
                    data:
                    {
                        model: $scope.job,

                    }
                }).then(function successCallback(response) {
                    if (response.data.IsTrue) {
                        alertSmallBox("Sửa công việc thành công", success);
                        location.hash = "/admin/mgrjob";
                    } else {
                        alertSmallBox("Sửa công việc không thành công", error);
                    }
                },
                    function errorCallback(response) {
                        alertSmallBox("lưu không thành công", error);
                    });
            } else {
                $http({
                    method: 'POST',
                    url: "/job/create",
                    data:
                    {
                        model: $scope.job,

                    }
                }).then(function successCallback(response) {
                    if (response.data.IsTrue) {
                        alertSmallBox("Đăng bài thành công", success);
                        location.hash = "/admin/mgrjob";
                    } else {
                        alertSmallBox("Đăng bài không thành công", error);
                    }
                },
                    function errorCallback(response) {
                        alertSmallBox("Đăng bài không thành công", error);
                    });

            }
        }
        //$scope.uploadImage = function () {
        //    if ($("#add-job-form").valid()) {
        //        //if (!$scope.job.Content) {
        //        //    alertSmallBox("Vui lòng nhập nội dung công việc", error);
        //        //    return;
        //        //}
        //        if ($("#select-file-image-cover").val() === "" && !$scope.job.Thumbnail) {
        //            alertSmallBox("Vui lòng chọn ảnh cần upload", "error");
        //            return;
        //        }
        //        if ($("#select-file-image-cover").val() !== "") {


        //            var formData = new FormData();
        //            formData.append('file', $("#select-file-image-cover")[0].files[0]);
        //            $.ajax({
        //                type: 'POST',
        //                url: "/api/FileManager/Upload",
        //                data: formData,
        //                processData: false, // tell jQuery not to process the data
        //                contentType: false, // tell jQuery not to set contentType
        //                success: function (data) {
        //                    if (data.IsTrue) {
        //                        $("body").removeClass("loading");
        //                        $scope.job.Thumbnail = data.Data[0];
        //                        $scope.submit();
        //                        //job.submit(data.data[0].ImageUrl);
        //                    } else {
        //                        alertSmallBox(data.message, "error");
        //                    }
        //                },
        //                beforeSend: function () {
        //                    $("body").addClass("loading");
        //                    $("#modal-upload-image").modal("hide");
        //                },
        //                error: function (data) {
        //                    alertSmallBox(data.message, "error");
        //                    $("body").removeClass("loading");
        //                },
        //                failure: function (data) {
        //                    $("body").removeClass("loading");
        //                },
        //                async: true
        //            });
        //        } else {
        //            $scope.submit();
        //        }
        //    }
        //}
        $scope.uploadImage = function () {
            if ($("#add-job-form").valid()) {
                //if (!$scope.job.Content) {
                //    alertSmallBox("Vui lòng nhập nội dung tin tức", error);
                //    return;
                //}
                if ($("#select-file-image-cover").val() === "" && !$scope.job.Thumbnail) {
                    alertSmallBox("Vui lòng chọn ảnh cần upload", "error");
                    return;
                }
                if ($("#select-file-image-cover").val() !== "") {


                    var formData = new FormData();
                    formData.append('file', $("#select-file-image-cover")[0].files[0]);
                    $.ajax({
                        type: 'POST',
                        url: "/api/FileManager/Upload",
                        data: formData,
                        processData: false, // tell jQuery not to process the data
                        contentType: false, // tell jQuery not to set contentType
                        success: function (data) {
                            if (data.IsTrue) {
                                $("body").removeClass("loading");
                                $scope.job.Thumbnail = data.Data[0];
                                $scope.submit();
                                //job.submit(data.data[0].ImageUrl);
                            } else {
                                alertSmallBox(data.message, "error");
                            }
                        },
                        beforeSend: function () {
                            $("body").addClass("loading");
                            $("#modal-upload-image").modal("hide");
                        },
                        error: function (data) {
                            alertSmallBox(data.message, "error");
                            $("body").removeClass("loading");
                        },
                        failure: function (data) {
                            $("body").removeClass("loading");
                        },
                        async: true
                    });
                } else {
                    $scope.submit();
                }
            }
        }
        $scope.loadListCountry = function () {
            $http({
                method: 'GET',
                url: "/countryjob/getall"
            }).then(function successCallback(response) {
                if (response.data.IsTrue) {
                    $scope.listCountry = response.data.Data;
                }
            }, function errorCallback(response) {

            });
        }
        $scope.loadListCountry();

        $scope.loadListCity = function () {
            $http({
                method: 'GET',
                url: "/cityjob/getall"
            }).then(function successCallback(response) {
                if (response.data.IsTrue) {
                    $scope.listCity = response.data.Data;
                }
            }, function errorCallback(response) {

            });
        }
        $scope.loadListCity();

        $scope.loadListType = function () {
            $http({
                method: 'GET',
                url: "/typejob/getall"
            }).then(function successCallback(response) {
                if (response.data.IsTrue) {
                    $scope.listType = response.data.Data;
                }
            }, function errorCallback(response) {

            });
        }
        $scope.loadListType();

        $scope.loadListCompany = function () {
            $http({
                method: 'GET',
                url: "/companyjob/getall"
            }).then(function successCallback(response) {
                if (response.data.IsTrue) {
                    $scope.listCompany = response.data.Data;
                }
            }, function errorCallback(response) {

            });
        }
        $scope.loadListCompany();

        $scope.loadListLevel = function () {
            $http({
                method: 'GET',
                url: "/leveljob/getall"
            }).then(function successCallback(response) {
                if (response.data.IsTrue) {
                    $scope.listLevel = response.data.Data;
                }
            }, function errorCallback(response) {

            });
        }
        $scope.loadListLevel();
        $scope.loadListCityTemp();

    });



ngApp.controller('typeJobController',
    function ($scope, $http, $sce) {
        $scope.totalPage = 10;
        $scope.keyword = "";
        $scope.total = 0;
        $scope.listPage = [];
        $scope.page = 0;
        $scope.size = 10;
        $scope.data = [];
        $scope.updatemode = false;
        $scope.changeToUpdateMode = function (item) {
            item.updating = true;
			item.tempName = item.Name;
			item.tempName2 = item.Name2;
        }

        $scope.changeToCreatedMode = function () {
            $scope.createdmode = true;
            $scope.tempCreatedItem = {};
        }
        $scope.cancelCreate = function (item) {
            $scope.createdmode = false;
        }

        $scope.createNew = function () {
            confirmOKCancel("Xác nhận", "Bạn có muốn lưu chuyên mục mới", "Có", "Không", function () {
                $http({
                    method: 'POST',
                    url: "/typejob/create",
                    data: $scope.tempCreatedItem
                }).then(function successCallback(response) {
                    if (response.data.IsTrue) {
                        $scope.createdmode = false;
                        $scope.data.splice(0, 0, response.data.Data);
						item.Name2 = item.tempName2;
						item.Name = item.tempName;
                        $scope.updatemode = false;
                        item.updating = false;
                        $scope.$apply();
                        alertSmallBox("lưu thành công", success);
                    } else {
                        alertSmallBox("lưu không thành công", error);
                    }
                }, function errorCallback(response) {
                    alertSmallBox("lưu không thành công", error);
                });
            });
        }

        $scope.cancel = function (item) {
            $scope.createdmode = false;
            item.updating = false;
        }

        $scope.save = function (item) {
            confirmOKCancel("Xác nhận", "Bạn có muốn lưu thay đổi", "Có", "Không", function () {
                $http({
                    method: 'PUT',
                    url: "/typejob/update/" + item.ID,
                    data: {
                        ID: item.ID,
						Name: item.tempName,
						Name2: item.tempName2
                    }
                }).then(function successCallback(response) {
                    if (response.data.IsTrue) {
                        alertSmallBox("Cập nhật thành công", success);
                        $scope.updatemode = false;
                        item.updating = false;
						item.Name = item.tempName;
						item.Name2 = item.tempName2;
                        $scope.$apply();
                    } else {
                        alertSmallBox("Cập nhật không thành công", error);
                    }
                }, function errorCallback(response) {
                    alertSmallBox("Cập nhật không thành công", error);
                });
            });
        }

        $scope.loadPage = function (page) {
            $scope.page = page;
            $http({
                method: 'GET',
                url: "/typejob/search?keyword=" + $scope.keyword + "&page=" + $scope.page + "&size=" + $scope.size
                //url: "/typejob/getall"
            }).then(function successCallback(response) {
                if (response.data.IsTrue) {
                    $scope.listPage = [];
                    $scope.total = response.data.Total;
                    $scope.data = response.data.Data;
                    var temp = ($scope.total) / $scope.size;
                    var totalPage = Math.round(temp, 0);
                    $scope.totalPage = temp > totalPage ? totalPage + 1 : totalPage;
                    for (var i = $scope.page - 10; i < $scope.page + 10; i++) {
                        if (i > 0 && i <= $scope.totalPage)
                            $scope.listPage.push(i);
                    }
                }
            }, function errorCallback(response) {

            });
        }
        $scope.remove = function (item) {
            confirmOKCancel("Xác nhận!", "Bạn có muốn xóa chuyên mục này?", "Có", "Không", function () {
                $http({
                    method: 'delete',
                    url: "/typejob/delete/" + item.ID
                }).then(function successCallback(response) {
                    if (response.data.IsTrue) {
                        var index = $scope.data.indexOf(item);
                        if (index !== -1) {
                            $scope.data.splice(index, 1);
                        }
                        alertSmallBox("Xóa thành công", success);
                        $scope.$apply();
                    } else {
                        alertSmallBox("Xóa không thành công", error);
                    }
                }, function errorCallback(response) {

                });
            });
        }
    });

ngApp.controller('companyJobController',
    function ($scope, $http, $sce) {

        $scope.totalPage = 10;
        $scope.keyword = "";
        $scope.total = 0;
        $scope.listPage = [];
        $scope.page = 0;
        $scope.size = 10;
        $scope.data = [];
        $scope.updatemode = false;
        $scope.changeToUpdateMode = function (item) {
            item.updating = true;
            item.tempName = item.Name;
        }

        $scope.changeToCreatedMode = function () {
            $scope.createdmode = true;
            $scope.tempCreatedItem = {};
        }
        $scope.cancelCreate = function (item) {
            $scope.createdmode = false;
        }

        $scope.createNew = function () {
            confirmOKCancel("Xác nhận", "Bạn có muốn tạo mới ?", "Có", "Không", function () {
                $http({
                    method: 'POST',
                    url: "/companyjob/create",
                    data: $scope.tempCreatedItem
                }).then(function successCallback(response) {
                    if (response.data.IsTrue) {
                        $scope.createdmode = false;
                        $scope.data.splice(0, 0, response.data.Data);
                        alertSmallBox("lưu thành công", success);
                        $scope.updatemode = false;
                        item.updating = false;
                        item.Name = item.tempName;
                        $scope.$apply();
                    } else {
                        alertSmallBox("lưu không thành công", error);
                    }
                }, function errorCallback(response) {
                    alertSmallBox("lưu không thành công", error);
                });
            });
        }

        $scope.cancel = function (item) {
            $scope.createdmode = false;
            item.updating = false;
        }

        $scope.save = function (item) {
            confirmOKCancel("Xác nhận", "Bạn có muốn lưu thay đổi", "Có", "Không", function () {
                $http({
                    method: 'PUT',
                    url: "/companyjob/update/" + item.ID,
                    data: {
                        ID: item.ID,
                        Name: item.tempName
                    }
                }).then(function successCallback(response) {
                    if (response.data.IsTrue) {
                        alertSmallBox("Cập nhật thành công", success);
                        $scope.updatemode = false;
                        item.updating = false;
                        item.Name = item.tempName;
                        $scope.$apply();
                    } else {
                        alertSmallBox("Cập nhật không thành công", error);
                    }
                }, function errorCallback(response) {
                    alertSmallBox("Cập nhật không thành công", error);
                });
            });
        }

        $scope.loadPage = function (page) {
            $scope.page = page;
            $http({
                method: 'GET',
                url: "/companyjob/search?keyword=" + $scope.keyword + "&page=" + $scope.page + "&size=" + $scope.size
                //url: "/companyjob/getall"
            }).then(function successCallback(response) {
                if (response.data.IsTrue) {
                    $scope.listPage = [];
                    $scope.total = response.data.Total;
                    $scope.data = response.data.Data;
                    var temp = ($scope.total) / $scope.size;
                    var totalPage = Math.round(temp, 0);
                    $scope.totalPage = temp > totalPage ? totalPage + 1 : totalPage;
                    for (var i = $scope.page - 10; i < $scope.page + 10; i++) {
                        if (i > 0 && i <= $scope.totalPage)
                            $scope.listPage.push(i);
                    }
                }
            }, function errorCallback(response) {

            });
        }
        $scope.remove = function (item) {
            confirmOKCancel("Xác nhận!", "Bạn có muốn xóa ?", "Có", "Không", function () {
                $http({
                    method: 'delete',
                    url: "/companyjob/delete/" + item.ID
                }).then(function successCallback(response) {
                    if (response.data.IsTrue) {
                        var index = $scope.data.indexOf(item);
                        if (index !== -1) {
                            $scope.data.splice(index, 1);
                        }
                        alertSmallBox("Xóa thành công", success);
                        $scope.$apply();
                    } else {
                        alertSmallBox("Xóa không thành công", error);
                    }
                }, function errorCallback(response) {

                });
            });
        }
    });


ngApp.controller('levelJobController',
    function ($scope, $http, $sce) {

        $scope.totalPage = 10;
        $scope.keyword = "";
        $scope.total = 0;
        $scope.listPage = [];
        $scope.page = 0;
        $scope.size = 10;
        $scope.data = [];
        $scope.updatemode = false;
        $scope.changeToUpdateMode = function (item) {
            item.updating = true;
			item.tempName = item.Name;
			item.tempName2 = item.Name2;
        }

        $scope.changeToCreatedMode = function () {
            $scope.createdmode = true;
            $scope.tempCreatedItem = {};
        }
        $scope.cancelCreate = function (item) {
            $scope.createdmode = false;
        }

        $scope.createNew = function () {
            confirmOKCancel("Xác nhận", "Bạn có muốn tạo mới ?", "Có", "Không", function () {
                $http({
                    method: 'POST',
                    url: "/leveljob/create",
                    data: $scope.tempCreatedItem
                }).then(function successCallback(response) {
                    if (response.data.IsTrue) {
                        $scope.createdmode = false;
                        $scope.data.splice(0, 0, response.data.Data);
                        alertSmallBox("lưu thành công", success);
                        $scope.updatemode = false;
                        item.updating = false;
						item.Name = item.tempName;
						item.Name2 = item.tempName2;
                        $scope.$apply();
                    } else {
                        alertSmallBox("lưu không thành công", error);
                    }
                }, function errorCallback(response) {
                    alertSmallBox("lưu không thành công", error);
                });
            });
        }

        $scope.cancel = function (item) {
            $scope.createdmode = false;
            item.updating = false;
        }

        $scope.save = function (item) {
            confirmOKCancel("Xác nhận", "Bạn có muốn lưu thay đổi", "Có", "Không", function () {
                $http({
                    method: 'PUT',
                    url: "/leveljob/update/" + item.ID,
                    data: {
                        ID: item.ID,
						Name: item.tempName,
						Name2: item.tempName2
                    }
                }).then(function successCallback(response) {
                    if (response.data.IsTrue) {
                        alertSmallBox("Cập nhật thành công", success);
                        $scope.updatemode = false;
                        item.updating = false;
						item.Name = item.tempName;
						item.Name2 = item.tempName2;
                        $scope.$apply();
                    } else {
                        alertSmallBox("Cập nhật không thành công", error);
                    }
                }, function errorCallback(response) {
                    alertSmallBox("Cập nhật không thành công", error);
                });
            });
        }

        $scope.loadPage = function (page) {
            $scope.page = page;
            $http({
                method: 'GET',
                url: "/leveljob/search?keyword=" + $scope.keyword + "&page=" + $scope.page + "&size=" + $scope.size
                //url: "/leveljob/getall"
            }).then(function successCallback(response) {
                if (response.data.IsTrue) {
                    $scope.listPage = [];
                    $scope.total = response.data.Total;
                    $scope.data = response.data.Data;
                    var temp = ($scope.total) / $scope.size;
                    var totalPage = Math.round(temp, 0);
                    $scope.totalPage = temp > totalPage ? totalPage + 1 : totalPage;
                    for (var i = $scope.page - 10; i < $scope.page + 10; i++) {
                        if (i > 0 && i <= $scope.totalPage)
                            $scope.listPage.push(i);
                    }
                }
            }, function errorCallback(response) {

            });
        }
        $scope.remove = function (item) {
            confirmOKCancel("Xác nhận!", "Bạn có muốn xóa ?", "Có", "Không", function () {
                $http({
                    method: 'delete',
                    url: "/leveljob/delete/" + item.ID
                }).then(function successCallback(response) {
                    if (response.data.IsTrue) {
                        var index = $scope.data.indexOf(item);
                        if (index !== -1) {
                            $scope.data.splice(index, 1);
                        }
                        alertSmallBox("Xóa thành công", success);
                        $scope.$apply();
                    } else {
                        alertSmallBox("Xóa không thành công", error);
                    }
                }, function errorCallback(response) {

                });
            });
        }
    });

ngApp.controller('countryJobController',
    function ($scope, $http, $sce) {

        $scope.totalPage = 10;
        $scope.keyword = "";
        $scope.total = 0;
        $scope.listPage = [];
        $scope.page = 0;
        $scope.size = 10;
        $scope.data = [];
        $scope.updatemode = false;
        $scope.changeToUpdateMode = function (item) {
            item.updating = true;
            item.tempName = item.Name;
        }

        $scope.changeToCreatedMode = function () {
            $scope.createdmode = true;
            $scope.tempCreatedItem = {};
        }
        $scope.cancelCreate = function (item) {
            $scope.createdmode = false;
        }

        $scope.createNew = function () {
            confirmOKCancel("Xác nhận", "Bạn có muốn tạo mới ?", "Có", "Không", function () {
                $http({
                    method: 'POST',
                    url: "/countryjob/create",
                    data: $scope.tempCreatedItem
                }).then(function successCallback(response) {
                    if (response.data.IsTrue) {
                        $scope.createdmode = false;
                        $scope.data.splice(0, 0, response.data.Data);
                        alertSmallBox("lưu thành công", success);
                        $scope.updatemode = false;
                        item.updating = false;
                        item.Name = item.tempName;
                        $scope.$apply();
                    } else {
                        alertSmallBox("lưu không thành công", error);
                    }
                }, function errorCallback(response) {
                    alertSmallBox("lưu không thành công", error);
                });
            });
        }

        $scope.cancel = function (item) {
            $scope.createdmode = false;
            item.updating = false;
        }

        $scope.save = function (item) {
            confirmOKCancel("Xác nhận", "Bạn có muốn lưu thay đổi", "Có", "Không", function () {
                $http({
                    method: 'PUT',
                    url: "/countryjob/update/" + item.ID,
                    data: {
                        ID: item.ID,
                        Name: item.tempName
                    }
                }).then(function successCallback(response) {
                    if (response.data.IsTrue) {
                        alertSmallBox("Cập nhật thành công", success);
                        $scope.updatemode = false;
                        item.updating = false;
                        item.Name = item.tempName;
                        $scope.$apply();
                    } else {
                        alertSmallBox("Cập nhật không thành công", error);
                    }
                }, function errorCallback(response) {
                    alertSmallBox("Cập nhật không thành công", error);
                });
            });
        }

        $scope.loadPage = function (page) {
            $scope.page = page;
            $http({
                method: 'GET',
                url: "/countryjob/search?keyword=" + $scope.keyword + "&page=" + $scope.page + "&size=" + $scope.size
                //url: "/countryjob/getall"
            }).then(function successCallback(response) {
                if (response.data.IsTrue) {
                    $scope.listPage = [];
                    $scope.total = response.data.Total;
                    $scope.data = response.data.Data;
                    var temp = ($scope.total) / $scope.size;
                    var totalPage = Math.round(temp, 0);
                    $scope.totalPage = temp > totalPage ? totalPage + 1 : totalPage;
                    for (var i = $scope.page - 10; i < $scope.page + 10; i++) {
                        if (i > 0 && i <= $scope.totalPage)
                            $scope.listPage.push(i);
                    }
                }
            }, function errorCallback(response) {

            });
        }
        $scope.remove = function (item) {
            confirmOKCancel("Xác nhận!", "Bạn có muốn xóa ?", "Có", "Không", function () {
                $http({
                    method: 'delete',
                    url: "/countryjob/delete/" + item.ID
                }).then(function successCallback(response) {
                    if (response.data.IsTrue) {
                        var index = $scope.data.indexOf(item);
                        if (index !== -1) {
                            $scope.data.splice(index, 1);
                        }
                        alertSmallBox("Xóa thành công", success);
                        $scope.$apply();
                    } else {
                        alertSmallBox("Xóa không thành công", error);
                    }
                }, function errorCallback(response) {

                });
            });
        }
    });

ngApp.controller('cityJobController',
    function ($scope, $http, $sce) {

        $scope.totalPage = 10;
        $scope.keyword = "";
        $scope.total = 0;
        $scope.listPage = [];
        $scope.page = 0;
        $scope.size = 10;
        $scope.data = [];
        $scope.updatemode = false;
        $scope.listType = [];
        $scope.loadListType = function () {
            $http({
                method: 'GET',
                url: "/countryjob/getall"
            }).then(function successCallback(response) {
                if (response.data.IsTrue) {
                    $scope.listType = response.data.Data;
                }
            }, function errorCallback(response) {

            });
        }
        $scope.changeToUpdateMode = function (item) {
            item.updating = true;
            item.tempName = item.Name;
            item.tempCountry = item.Country;
        }

        $scope.changeToCreatedMode = function () {
            $scope.createdmode = true;
            $scope.tempCreatedItem = {};
        }
        $scope.cancelCreate = function (item) {
            $scope.createdmode = false;
        }



        $scope.createNew = function () {
            confirmOKCancel("Xác nhận", "Bạn có muốn tạo mới ?", "Có", "Không", function () {
                $http({
                    method: 'POST',
                    url: "/cityjob/create",
                    data: $scope.tempCreatedItem
                }).then(function successCallback(response) {
                    if (response.data.IsTrue) {
                        $scope.createdmode = false;
                        $scope.data.splice(0, 0, response.data.Data);
                        alertSmallBox("lưu thành công", success);
                        $scope.updatemode = false;
                        item.updating = false;
                        item.Name = item.tempName;
                        $scope.$apply();
                    } else {
                        alertSmallBox("lưu không thành công", error);
                    }
                }, function errorCallback(response) {
                    alertSmallBox("lưu không thành công", error);
                });
            });
        }

        $scope.cancel = function (item) {
            $scope.createdmode = false;
            item.updating = false;
        }

        $scope.save = function (item) {
            confirmOKCancel("Xác nhận", "Bạn có muốn lưu thay đổi", "Có", "Không", function () {
                $http({
                    method: 'PUT',
                    url: "/cityjob/update/" + item.ID,
                    data: {
                        ID: item.ID,
                        Name: item.tempName,
                        Country: item.Country
                    }
                }).then(function successCallback(response) {
                    if (response.data.IsTrue) {
                        alertSmallBox("Cập nhật thành công", success);
                        $scope.updatemode = false;
                        item.updating = false;
                        item.Name = item.tempName;
                        item.Country = item.tempCountry;
                        $scope.$apply();
                    } else {
                        alertSmallBox("Cập nhật không thành công", error);
                    }
                }, function errorCallback(response) {
                    alertSmallBox("Cập nhật không thành công", error);
                });
            });
        }

        $scope.loadPage = function (page) {
            $scope.page = page;
            $http({
                method: 'GET',
                url: "/cityjob/search?keyword=" + $scope.keyword + "&page=" + $scope.page + "&size=" + $scope.size
                //url: "/cityjob/getall"
            }).then(function successCallback(response) {
                if (response.data.IsTrue) {
                    $scope.listPage = [];
                    $scope.total = response.data.Total;
                    $scope.data = response.data.Data;
                    var temp = ($scope.total) / $scope.size;
                    var totalPage = Math.round(temp, 0);
                    $scope.totalPage = temp > totalPage ? totalPage + 1 : totalPage;
                    for (var i = $scope.page - 10; i < $scope.page + 10; i++) {
                        if (i > 0 && i <= $scope.totalPage)
                            $scope.listPage.push(i);
                    }
                }
            }, function errorCallback(response) {

            });
        }
        $scope.remove = function (item) {
            confirmOKCancel("Xác nhận!", "Bạn có muốn xóa ?", "Có", "Không", function () {
                $http({
                    method: 'delete',
                    url: "/cityjob/delete/" + item.ID
                }).then(function successCallback(response) {
                    if (response.data.IsTrue) {
                        var index = $scope.data.indexOf(item);
                        if (index !== -1) {
                            $scope.data.splice(index, 1);
                        }
                        alertSmallBox("Xóa thành công", success);
                        $scope.$apply();
                    } else {
                        alertSmallBox("Xóa không thành công", error);
                    }
                }, function errorCallback(response) {

                });
            });
        }
    });

ngApp.controller('typeTipsController',
	function ($scope, $http, $sce) {
		$scope.totalPage = 10;
		$scope.keyword = "";
		$scope.total = 0;
		$scope.listPage = [];
		$scope.page = 0;
		$scope.size = 10;
		$scope.data = [];
		$scope.updatemode = false;
		$scope.changeToUpdateMode = function (item) {
			item.updating = true;
			item.tempName = item.Name;
			item.tempName2 = item.Name2;
		}

		$scope.changeToCreatedMode = function () {
			$scope.createdmode = true;
			$scope.tempCreatedItem = {};
		}
		$scope.cancelCreate = function (item) {
			$scope.createdmode = false;
		}

		$scope.createNew = function () {
			confirmOKCancel("Xác nhận", "[Type tips] Bạn có muốn thêm chuyên mục mới", "Có", "Không", function () {
				$http({
					method: 'POST',
					url: "/typetips/create",
					data: $scope.tempCreatedItem
				}).then(function successCallback(response) {
					if (response.data.IsTrue) {
						$scope.createdmode = false;
						$scope.data.splice(0, 0, response.data.Data);
						item.Name2 = item.tempName2;
						item.Name = item.tempName;
						$scope.updatemode = false;
						item.updating = false;
						$scope.$apply();
						alertSmallBox("[Type tips] Thêm chuyên mục mới thành công", success);
					} else {
						alertSmallBox("[Type tips] Thêm chuyên mục mới không thành công", error);
					}
				}, function errorCallback(response) {
					alertSmallBox("[Type tips] Thêm chuyên mục mới không thành công", error);
				});
			});
		}

		$scope.cancel = function (item) {
			$scope.createdmode = false;
			item.updating = false;
		}

		$scope.save = function (item) {
			confirmOKCancel("Xác nhận", "[Type tips] Bạn có muốn lưu thay đổi", "Có", "Không", function () {
				$http({
					method: 'PUT',
					url: "/typetips/update/" + item.ID,
					data: {
						ID: item.ID,
						Name: item.tempName,
						Name2: item.tempName2
					}
				}).then(function successCallback(response) {
					if (response.data.IsTrue) {
						alertSmallBox("[Type tips] Cập nhật chuyên mục thành công", success);
						$scope.updatemode = false;
						item.updating = false;
						item.Name = item.tempName;
						item.Name2 = item.tempName2;
						$scope.$apply();
					} else {
						alertSmallBox("[Type tips] Cập nhật chuyên mục không thành công", error);
					}
				}, function errorCallback(response) {
					alertSmallBox("[Type tips] Cập nhật chuyên mục không thành công", error);
				});
			});
		}

		$scope.loadPage = function (page) {
			$scope.page = page;
			$http({
				method: 'GET',
				url: "/typetips/search?keyword=" + $scope.keyword + "&page=" + $scope.page + "&size=" + $scope.size
			}).then(function successCallback(response) {
				if (response.data.IsTrue) {
					$scope.listPage = [];
					$scope.total = response.data.Total;
					$scope.data = response.data.Data;
					var temp = ($scope.total) / $scope.size;
					var totalPage = Math.round(temp, 0);
					$scope.totalPage = temp > totalPage ? totalPage + 1 : totalPage;
					for (var i = $scope.page - 10; i < $scope.page + 10; i++) {
						if (i > 0 && i <= $scope.totalPage)
							$scope.listPage.push(i);
					}
				}
			}, function errorCallback(response) {

			});
		}
		$scope.remove = function (item) {
			confirmOKCancel("Xác nhận!", "[Type tips] Bạn có muốn xóa chuyên mục này?", "Có", "Không", function () {
				$http({
					method: 'delete',
					url: "/typetips/delete/" + item.ID
				}).then(function successCallback(response) {
					if (response.data.IsTrue) {
						var index = $scope.data.indexOf(item);
						if (index !== -1) {
							$scope.data.splice(index, 1);
						}
						alertSmallBox("[Type tips] Xóa chuyên mục thành công", success);
						$scope.$apply();
					} else {
						alertSmallBox("[Type tips] Xóa chuyên mục không thành công", error);
					}
				}, function errorCallback(response) {

				});
			});
		}
	});


ngApp.controller('tipsController',
    function ($scope, $http, $sce) {
        $scope.totalPage = 10;
        $scope.keyword = "";
        $scope.total = 0;
        $scope.listPage = [];
        $scope.page = 0;
        $scope.size = 10;
        $scope.data = [];
        $scope.listType = [];
        $scope.loadListType = function () {
            $http({
                method: 'GET',
                url: "/typetips/getall"
            }).then(function successCallback(response) {
                if (response.data.IsTrue) {
                    $scope.listType = response.data.Data;
                }
            }, function errorCallback(response) {

            });
        }
        $scope.loadPage = function (page) {
            $scope.page = page;
            $http({
                method: 'GET',
                url: "/tips/getall?keyword=" + $scope.keyword + "&page=" + $scope.page + "&size=" + $scope.size
            }).then(function successCallback(response) {
                if (response.data.IsTrue) {
                    $scope.listPage = [];
                    $scope.total = response.data.Total;
                    $scope.data = response.data.Data;
                    var temp = ($scope.total) / $scope.size;
                    var totalPage = Math.round(temp, 0);
                    $scope.totalPage = temp > totalPage ? totalPage + 1 : totalPage;
                    for (var i = $scope.page - 10; i < $scope.page + 10; i++) {
                        if (i > 0 && i <= $scope.totalPage)
                            $scope.listPage.push(i);
                    }
                }
            }, function errorCallback(response) {

            });
        }
        $scope.view = function (item) {

        }
        $scope.edit = function (item) {
            location.hash = "/admin/mgrtips/create/" + item.ID;
        }
        $scope.remove = function (item) {
            confirmOKCancel("Xác nhận!", "[Tips] Bạn có muốn xóa bài viết '" + item.Title + "' không? ID: " + item.ID, "Có", "Không", function () {
                $http({
                    method: 'delete',
                    url: "/tips/delete/" + item.ID
                }).then(function successCallback(response) {
                    if (response.data.IsTrue) {
                        var index = $scope.data.indexOf(item);
                        if (index !== -1) {
                            $scope.data.splice(index, 1);
                        }
                        alertSmallBox("[Tips] Xóa bài viết thành công", success);
                        $scope.$apply();
                    } else {
                        alertSmallBox("[Tips] Xóa bài viết không thành công: " + response.Data.getStringValue, error);
                    }
                }, function errorCallback(response) {

                });
            });
        }
    });

ngApp.controller('createTipsController',
    function ($scope, $http, $sce) {
        $.validator.addMethod(
            "input_content",
            function (value, element) {
                return CKEditorSetup.GetData(element.id).trim() !== "";
            },
            "Vui lòng nhập nội dung lời khuyên"
        );

        $scope.validationOptions = {
            rules: {
                type:
                { required: true },
                title: {
                    required: true
                },
                content:
                {
                    input_content: true
                },
                contentpreview: {
                    required: true
                }
            },

            messages: {
                type:
                { required: "Vui lòng chọn chuyên mục cho lời khuyên" }
                ,
                title: {
                    required: "Vui lòng nhập tiêu đề của lời khuyên"
                },
                contentpreview: {
                    required: "Vui lòng nhập nội dung xem trước của lời khuyên"
                }
            }
        }
        $scope.fileSelected = "";
        $scope.selectImage = function () {
            var selector = $(event.target);
            var fileName = selector[0].value.replace("C:\\fakepath\\", "").toLocaleLowerCase();
            if (common.endWith(fileName, ".jpg") ||
                common.endWith(fileName, ".png") ||
                common.endWith(fileName, ".gif")) {
                $scope.fileSelected = selector[0].value.replace("C:\\fakepath\\", "");
                var filePath = URL.createObjectURL(selector[0].files[0]);
                $scope.tips.Thumbnail = filePath;
            } else {
                alertSmallBox("[Tips] Vui lòng chọn hình ảnh có định dạng png, jpg, gif", "error");
            }
            $scope.$apply();
        }
        $scope.submit = function () {
            if ($scope.tips.ID != undefined) {
                $http({
                    method: 'PUT',
                    url: "/tips/update/" + $scope.tips.ID,
                    data:
                    {
                        model: $scope.tips,
                    }
                }).then(function successCallback(response) {
                    if (response.data.IsTrue) {
                        alertSmallBox("[Tips] Sửa bài viết thành công", success);
                        location.hash = "/admin/mgrtips";
                    } else {
                        alertSmallBox("[Tips] Sửa bài viết không thành công", error);
                    }
                },
                    function errorCallback(response) {
                        alertSmallBox("[Tips] Sửa bài viết không thành công", error);
                    });
            } else {
                $http({
                    method: 'POST',
                    url: "/tips/create",
                    data:
                    {
                        model: $scope.tips,
                    }
                }).then(function successCallback(response) {
                    if (response.data.IsTrue) {
                        alertSmallBox("[Tips] Đăng vài viết thành công", success);
                        location.hash = "/admin/mgrtips";
                    } else {
                        alertSmallBox("[Tips] Đăng vài viết không thành công", error);
                    }
                },
                    function errorCallback(response) {
                        alertSmallBox("[Tips] Đăng vài viết không thành công", error);
                    });

            }
        }
        $scope.uploadImage = function () {
            if ($("#add-tips-form").valid()) {
                if (!$scope.tips.Content) {
                    alertSmallBox("[Tips] Vui lòng nhập nội dung bài viết", error);
                    return;
                }
                if ($("#select-file-image-cover").val() === "" && !$scope.tips.Thumbnail) {
                    alertSmallBox("[Tips] Vui lòng chọn ảnh cần upload", error);
                    return;
                }
                if ($("#select-file-image-cover").val() !== "") {
                    var formData = new FormData();
                    formData.append('file', $("#select-file-image-cover")[0].files[0]);
                    $.ajax({
                        type: 'POST',
                        url: "/api/FileManager/Upload",
                        data: formData,
                        processData: false, // tell jQuery not to process the data
                        contentType: false, // tell jQuery not to set contentType
                        success: function (data) {
                            if (data.IsTrue) {
                                $("body").removeClass("loading");
                                $scope.tips.Thumbnail = data.Data[0];
                                $scope.submit();
                                //tips.submit(data.data[0].ImageUrl);
                            } else {
                                alertSmallBox("uploadImage>tips.Thumbnail: " + data.message, error);
                            }
                        },
                        beforeSend: function () {
                            $("body").addClass("loading");
                            $("#modal-upload-image").modal("hide");
                        },
                        error: function (data) {
                            alertSmallBox("uploadImage>error: function: " + data.message, error);
                            $("body").removeClass("loading");
                        },
                        failure: function (data) {
                            $("body").removeClass("loading");
                        },
                        async: true
                    });
                } else {
                    $scope.submit();
                }
            }
        }
        $scope.loadListType = function () {
            $http({
                method: 'GET',
                url: "/typetips/getall"
            }).then(function successCallback(response) {
                if (response.data.IsTrue) {
                    $scope.listType = response.data.Data;
                }
            }, function errorCallback(response) {

            });
        }
        $scope.loadListType();
    });

ngApp.controller('questionController',
    function ($scope, $http, $sce) {
        $scope.tag = "";
        $scope.totalPage = 10;
        $scope.keyword = "";
        $scope.total = 0;
        $scope.listPage = [];
        $scope.page = 0;
        $scope.size = 10;
        $scope.data = [];
        $scope.listType = [];

        $scope.loadListQuestionPage = function (page) {
            $scope.page = page;
            $http({
                method: 'GET',
                url: "/question/search?keyword=" + $scope.keyword + "&page=" + $scope.page + "&size=" + $scope.size
            }).then(function successCallback(response) {
                if (response.data.IsTrue) {
                    $scope.listPage = [];
                    $scope.total = response.data.Total;
                    $scope.data = response.data.Data;
                    var temp = ($scope.total) / $scope.size;
                    var totalPage = Math.round(temp, 0);
                    $scope.totalPage = temp > totalPage ? totalPage + 1 : totalPage;
                    for (var i = $scope.page - 10; i < $scope.page + 10; i++) {
                        if (i > 0 && i <= $scope.totalPage)
                            $scope.listPage.push(i);
                    }
                }
            }, function errorCallback(response) {

            });
        }
        $scope.view = function (item) {

        }
        $scope.editQuestion = function (item) {
            location.hash = "/admin/mgrquestion/create/" + item.ID;
        }
        $scope.deleteQuestion = function (item) {
            confirmOKCancel("Xác nhận!", "Bạn có muốn xóa câu hỏi '" + item.Title + "' không?", "Có", "Không", function () {
                $http({
                    method: 'delete',
                    url: "/question/delete/" + item.ID
                }).then(function successCallback(response) {
                    if (response.data.IsTrue) {
                        var index = $scope.data.indexOf(item);
                        if (index !== -1) {
                            $scope.data.splice(index, 1);
                        }
                        alertSmallBox("Xóa thành công", success);
                        $scope.$apply();
                    } else {
                        alertSmallBox("Xóa không thành công", error);
                    }
                }, function errorCallback(response) {

                });
            });
        }
    });

ngApp.controller('createQuestionController',
    function ($scope, $http, $sce) {

        $scope.validationOptions = {
            rules: {
                title: { required: true },
                content: { required: true },
                answer: { required: true }
            },

            messages: {
                title: {
                    required: "Vui lòng nhập tiêu đề của câu hỏi"
                },
                content: {
                    required: "Vui lòng nhập nội dung chi tiết của câu hỏi"
                },
                answer: {
                    required: "Vui lòng nhập câu trả lời cho câu hỏi"
                }
            }
        }

        $scope.submit = function () {
            if ($("#add-question-form").valid()) {
                if ($scope.question.ID != undefined) {
                    $http({
                        method: 'PUT',
                        url: "/question/update/" + $scope.question.ID,
                        data:
                        {
                            model: $scope.question,
                        }
                    }).then(function successCallback(response) {
                        if (response.data.IsTrue) {
                            alertSmallBox("[Question] Sửa câu hỏi thành công", success);
                            location.hash = "/admin/mgrquestion";
                        } else {
                            alertSmallBox("[Question] Sửa câu hỏi không thành công", error);
                        }
                    },
                        function errorCallback(response) {
                            alertSmallBox("[Question] Sửa câu hỏi không thành công", error);
                        });
                } else {
                    $http({
                        method: 'POST',
                        url: "/question/create",
                        data:
                        {
                            model: $scope.question,
                        }
                    }).then(function successCallback(response) {
                        if (response.data.IsTrue) {
                            alertSmallBox("[Question] Tạo câu hỏi thành công", success);
                            location.hash = "/admin/mgrquestion";
                        } else {
                            alertSmallBox("[Question] Tạo câu hỏi không thành công", error);
                        }
                    },
                        function errorCallback(response) {
                            alertSmallBox("[Question] Tạo câu hỏi không thành công", error);
                        });
                }
            }
        }

    });



ngApp.controller('typeTranslatorController',
    function ($scope, $http, $sce) {

        $scope.totalPage = 10;
        $scope.keyword = "";
        $scope.total = 0;
        $scope.listPage = [];
        $scope.page = 0;
        $scope.size = 10;
        $scope.data = [];
        $scope.updatemode = false;
        $scope.listType = [];
        $scope.changeToUpdateMode = function (item) {
            item.updating = true;
			item.tempName = item.Name;
			item.tempName2 = item.Name2;
            item.tempCategory = $scope.language
        }

        $scope.changeToCreatedMode = function () {
            $scope.createdmode = true;
            $scope.tempCreatedItem = {};
            $scope.tempCreatedItem.Category = $scope.language;
        }
        $scope.cancelCreate = function (item) {
            $scope.createdmode = false;
        }
        $scope.createNew = function () {
            confirmOKCancel("Xác nhận", "Bạn có muốn thêm ?", "Có", "Không", function () {
                $http({
                    method: 'POST',
                    url: "/typetranslator/create",
                    data: $scope.tempCreatedItem
                }).then(function successCallback(response) {
                    if (response.data.IsTrue) {
                        $scope.createdmode = false;
                        $scope.data.splice(0, 0, response.data.Data);
                        
                        $scope.updatemode = false;
                        item.updating = false;
						item.Name = item.tempName;
						item.Name2 = item.tempName2;
                        item.Category = item.tempCategory;
						$scope.$apply();
						alertSmallBox("Thêm thành công", success);
                    } else {
                        alertSmallBox("Thêm không thành công", error);
                    }
                }, function errorCallback(response) {
                    alertSmallBox("Thêm không thành công", error);
                });
            });
        }

        $scope.cancel = function (item) {
            $scope.createdmode = false;
            item.updating = false;
        }

        $scope.save = function (item) {
            confirmOKCancel("Xác nhận", "Bạn có muốn sửa", "Có", "Không", function () {
                $http({
                    method: 'PUT',
                    url: "/typetranslator/update/" + item.ID,
                    data: {
                        ID: item.ID,
						Name: item.tempName,
						Name2: item.tempName2,
                        Category: item.tempCategory
                    }
                }).then(function successCallback(response) {
                    if (response.data.IsTrue) {
                        alertSmallBox("Sửa thành công", success);
                        $scope.updatemode = false;
                        item.updating = false;
						item.Name = item.tempName;
						item.Name2 = item.tempName2;
                        item.Category = item.tempCategory;
                        $scope.$apply();
                    } else {
                        alertSmallBox("Sửa không thành công", error);
                    }
                }, function errorCallback(response) {
                    alertSmallBox("Sửa không thành công", error);
                });
            });
        }

        $scope.loadPage = function (page, language) {
            $scope.page = page;
            $scope.language = language;
            $http({
				method: 'GET',
				url: "/typetranslator/getall?keyword=" + $scope.keyword + "&type=" + $scope.language + "&page=" + $scope.page + "&size=" + $scope.size
            }).then(function successCallback(response) {
                if (response.data.IsTrue) {
                    $scope.listPage = [];
                    $scope.total = response.data.Total;
                    $scope.data = response.data.Data;
                    var temp = ($scope.total) / $scope.size;
                    var totalPage = Math.round(temp, 0);
                    $scope.totalPage = temp > totalPage ? totalPage + 1 : totalPage;
                    for (var i = $scope.page - 10; i < $scope.page + 10; i++) {
                        if (i > 0 && i <= $scope.totalPage)
                            $scope.listPage.push(i);
                    }
                }
            }, function errorCallback(response) {

            });
        }
        $scope.remove = function (item) {
            confirmOKCancel("Xác nhận!", "Bạn có muốn xóa ?", "Có", "Không", function () {
                $http({
                    method: 'delete',
                    url: "/typetranslator/delete/" + item.ID
                }).then(function successCallback(response) {
                    if (response.data.IsTrue) {
                        var index = $scope.data.indexOf(item);
                        if (index !== -1) {
                            $scope.data.splice(index, 1);
                        }
                        alertSmallBox("Xóa thành công", success);
                        $scope.$apply();
                    } else {
                        alertSmallBox("Xóa không thành công", error);
                    }
                }, function errorCallback(response) {

                });
            });
        }
    });

ngApp.controller('translatorController',
    function ($scope, $http, $sce) {
        $scope.language = "";
        $scope.totalPage = 10;
        $scope.keyword = "";
        $scope.total = 0;
        $scope.listPage = [];
        $scope.page = 0;
        $scope.size = 10;
        $scope.data = [];
        $scope.listType = [];


        $scope.loadListType = function (language) {
            $scope.language = language;
            $http({
                method: 'GET',
                url: "/typetranslator/getall?type=" + language
            }).then(function successCallback(response) {
                if (response.data.IsTrue) {
                    $scope.listType = response.data.Data;
                }
            }, function errorCallback(response) {

            });
        }
        $scope.loadPage = function (page) {
            $scope.page = page;
            $http({
                method: 'GET',
                url: "/translator/getall?keyword=" + $scope.keyword + "&page=" + $scope.page + "&size=" + $scope.size + "&category=" + $scope.language
            }).then(function successCallback(response) {
                if (response.data.IsTrue) {
                    $scope.listPage = [];
                    $scope.total = response.data.Total;
                    $scope.data = response.data.Data;
                    var temp = ($scope.total) / $scope.size;
                    var totalPage = Math.round(temp, 0);
                    $scope.totalPage = temp > totalPage ? totalPage + 1 : totalPage;
                    for (var i = $scope.page - 10; i < $scope.page + 10; i++) {
                        if (i > 0 && i <= $scope.totalPage)
                            $scope.listPage.push(i);
                    }
                }
            }, function errorCallback(response) {

            });
        }
        $scope.view = function (item) {

        }
        $scope.edit = function (item) {
            if ($scope.language == 2)
                location.hash = "/admin/mgrtranslatorgermany/create/" + item.ID;
            else
                location.hash = "/admin/mgrtranslatorenglish/create/" + item.ID;
        }
        $scope.remove = function (item) {
            confirmOKCancel("Xác nhận!", "[Translator] Bạn có muốn xóa '" + item.ContentVN + "' không? ID: " + item.ID, "Có", "Không", function () {
                $http({
                    method: 'delete',
                    url: "/translator/delete/" + item.ID
                }).then(function successCallback(response) {
                    if (response.data.IsTrue) {
                        var index = $scope.data.indexOf(item);
                        if (index !== -1) {
                            $scope.data.splice(index, 1);
                        }
                        alertSmallBox("[Translator] Xóa thành công", success);
                        $scope.$apply();
                    } else {
                        alertSmallBox("[Translator] Xóa không thành công: " + response.Data.getStringValue, error);
                    }
                }, function errorCallback(response) {

                });
            });
        }
    });

ngApp.controller('createTranslatorController',

 function ($scope, $http, $sce) {

     $scope.setData = function (language) {
         $scope.language = language;
         //$scope.translator.type = language;
     }
     $scope.validationOptions = {
         rules: {
             title: { required: true },
             title2: { required: true },
             type: { required: true }
         },

         messages: {
             title: {
                 required: "Vui lòng nhập nội dung"
             },
             title2: {
                 required: "Vui lòng nhập nội dung"
             },
             type: {
                 required: "Vui lòng chọn chuyên mục"
             }
         }
     }

     $scope.submit = function () {
         if ($("#add-translator-form").valid()) {
             if ($scope.translator.ID != undefined) {

                 $http({
                     method: 'PUT',
                     url: "/translator/update/" + $scope.translator.ID,
                     data:
                     {
                         model: $scope.translator,
                     }
                 }).then(function successCallback(response) {
                     if (response.data.IsTrue) {
                         alertSmallBox("[Translator] Sửa thành công", success);
                         if ($scope.language == 2)
                             location.hash = "/admin/mgrtranslatorgermany";
                         else
                             location.hash = "/admin/mgrtranslatorenglish";
                     } else {
                         alertSmallBox("[Translator] Sửa không thành công", error);
                     }
                 },
                     function errorCallback(response) {
                         alertSmallBox("[Translator] Sửa không thành công", error);
                     });
             } else {

                 $http({
                     method: 'POST',
                     url: "/translator/create",
                     data:
                     {
                         model: $scope.translator,
                     }
                 }).then(function successCallback(response) {
                     if (response.data.IsTrue) {
                         alertSmallBox("[Translator] Thêm thành công", success);
                         if ($scope.language == 2)
                             location.hash = "/admin/mgrtranslatorgermany";
                         else
                             location.hash = "/admin/mgrtranslatorenglish";
                     } else {
                         alertSmallBox("[Translator] Thêm không thành công", error);
                     }
                 },
                     function errorCallback(response) {
                         alertSmallBox("[Translator] Thêm không thành công", error);
                     });
             }
         }
     }
     $scope.loadListType = function (language) {
         $scope.language = language;
         $http({
             method: 'GET',
             url: "/typetranslator/getall?type=" + language
         }).then(function successCallback(response) {
             if (response.data.IsTrue) {
                 $scope.listType = response.data.Data;
             }
         }, function errorCallback(response) {

         });
     }
     $scope.loadListType();
 });
