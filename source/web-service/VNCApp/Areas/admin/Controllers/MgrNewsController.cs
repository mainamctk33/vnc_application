﻿using System;
using System.Linq;
using System.Web.Mvc;
using BaseLibrary.Controllers;
using VNCApp.DataAccess.News;

namespace VNCApp.Areas.admin.Controllers
{
    public class MgrNewsController : BaseController
    {
        // GET: admin/MgrNews
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Create(int id = 0)
        {
            var news = new NewsProvider().GetById(id);
            var tag = new TagProvider().GetTagByNews(id);
            if (tag.IsTrue)
                ViewBag.Tag = String.Join(",", tag.Data.Select(x => x.TagName));
            if (news.IsTrue && news.Data != null)
                return View(news.Data);
            return View();
        }
    }
}