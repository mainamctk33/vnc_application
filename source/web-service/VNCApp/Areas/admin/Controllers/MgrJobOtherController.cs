﻿using BaseLibrary.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using VNCApp.DataAccess.Jobs;

namespace VNCApp.Areas.admin.Controllers
{
    public class MgrJobOtherController : BaseController
    {
        // GET: admin/MgrJobOther
        public ActionResult Level()
        {
            return View();
        }
        public ActionResult Company()
        {
            return View();
        }
        public ActionResult Country()
        {
            return View();
        }
        public ActionResult City()
        {
            return View();
        }
    }
}