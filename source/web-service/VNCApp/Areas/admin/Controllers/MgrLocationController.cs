﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using VNCApp.DataAccess;
using VNCApp.DataAccess.Location;
using VNCApp.Database.Location.Location;

namespace VNCApp.Areas.admin.Controllers
{
    public class MgrLocationController : Controller
    {
        // GET: admin/MgrLocation
        public ActionResult Index()
        {
            var typeLocation = new TypeLocationProvider().GetAll();
            if (typeLocation.IsTrue)
                return View(typeLocation.Data);
            return View(new List<TypeLocation>());
        }
        public ActionResult Create(int id = 0)
        {
            if (id == 0) return null;
            var data = new LocationProvider().GetById(id);
            if (data.IsTrue && data.Data != null)
                return View(data.Data);
            return View();
        }
    }
}