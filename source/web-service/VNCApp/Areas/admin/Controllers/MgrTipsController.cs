﻿using BaseLibrary.Controllers;
using System.Web.Mvc;
using VNCApp.DataAccess.Tips;

namespace VNCApp.Areas.admin.Controllers
{
    public class MgrTipsController : BaseController
    {
        //GET: admin/MgrTips
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Create(int id = 0)
        {
            var tips = new TipsProvider().GetById(id);
            if (tips.IsTrue && tips.Data != null)
                return View(tips.Data);
            return View();
        }
    }
}