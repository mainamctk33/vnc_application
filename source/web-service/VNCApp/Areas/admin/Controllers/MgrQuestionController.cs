﻿using BaseLibrary.Controllers;
using System;
using System.Linq;
using System.Web.Mvc;
using VNCApp.DataAccess.Question;

namespace VNCApp.Areas.admin.Controllers
{
    public class MgrQuestionController : BaseController
    {
        // GET: admin/MgrQuestion
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Create(int id = 0)
        {
            var question = new QuestionProvider().GetById(id);
            if (question.IsTrue && question.Data != null)
                return View(question.Data);
            return View();
        }
    }
}