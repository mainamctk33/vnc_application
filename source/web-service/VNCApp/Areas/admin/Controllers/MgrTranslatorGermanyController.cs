﻿using BaseLibrary.Controllers;
using System.Web.Mvc;
using VNCApp.DataAccess.Translators;

namespace VNCApp.Areas.admin.Controllers
{
    public class MgrTranslatorGermanyController : BaseController
    {
        //GET: admin/MgrTranslator
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Create(int id = 0)
        {
            var data = new TranslatorProvider().GetById(id);
            if (data.IsTrue && data.Data != null)
                return View(data.Data);
            return View();
        }
        public ActionResult Type()
        {
            return View();
        }
    }
}