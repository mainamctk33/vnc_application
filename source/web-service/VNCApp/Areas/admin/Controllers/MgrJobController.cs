﻿using BaseLibrary.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using VNCApp.DataAccess.Jobs;

namespace VNCApp.Areas.admin.Controllers
{
    public class MgrJobController : BaseController
    {
        // GET: admin/MgrJob
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult Create(int id = 0)
        {
            var job = new JobProvider().GetById(id);
            if (job.IsTrue && job.Data != null)
                return View(job.Data);
            return View();
        }
    }
}