﻿using BaseLibrary.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using VNCApp.Database.Question;
using BaseLibrary.Utils;

namespace VNCApp.Areas.api.Controllers.Question
{
    public class QuestionController : BaseService<DataAccess.Question.QuestionProvider, int, VNCApp.Database.Question.Question>
    {
        public override ActionResult Create(Database.Question.Question model)
        {
            model.CreatedDate = DateTime.Now.GetUnixTimeStamp();
            String createdBy = requestUserId();
            if (String.IsNullOrEmpty(createdBy))
                createdBy = "ndphuong@live.com";

            model.CreatedBy = createdBy;
            var result = dataProvider.Create(model);
            return ServiceResponse(result);
        }

        public override ActionResult Delete(int id)
        {
            var result = dataProvider.Delete(id);
            return ServiceResponse(result);
        }

        public override ActionResult Get(int id)
        {
            var result = dataProvider.GetById(id);
            return ServiceResponse(result);
        }

        public override ActionResult GetAll()
        {
            var result = dataProvider.GetAll();
            return ServiceResponse(result);
        }

        public ActionResult GetMyQuestion()
        {
            var result = dataProvider.GetMyQuestion(requestUserId());
            return ServiceResponse(result);
        }

        public ActionResult GetCommonQuestion()
        {
            var result = dataProvider.GetCommonQuestion();
            return ServiceResponse(result);
        }

        public override ActionResult Sync(double lastSyncTime = 0)
        {
            throw new NotImplementedException();
        }

        public override ActionResult Update(int id, Database.Question.Question model)
        {
            var result = dataProvider.Update(id, model);
            return ServiceResponse(result);
        }

        [HttpGet]
        public ActionResult Search()
        {
            String _page = Request.QueryString["page"];
            String _size = Request.QueryString["size"];
            String keyword = Request.QueryString["keyword"];
            int page = ConvertUtils.ToInt(_page, 1);
            int size = ConvertUtils.ToInt(_size, 15);
            var result = dataProvider.Search(page, size, keyword);
            return ServiceResponse(result);
        }

        public ActionResult IsExisted(string title, string content)
        {
            var result = dataProvider.IsExisted(requestUserId(), title, content);
            return ServiceResponse(result);
        }
        public ActionResult GetPending()
        {
            var result = dataProvider.GetPending();
            return ServiceResponse(result);
        }

    }
}