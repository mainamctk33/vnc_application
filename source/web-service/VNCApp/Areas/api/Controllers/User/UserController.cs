﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using BaseLibrary.Utils;
using VNCApp.DataAccess.User;
using VNCApp.Database.User;
using BaseLibrary.Common;
using System.Text;
using System.Dynamic;
using VNCApp.Models;
using BaseLibrary.Helper;
using System.Web.Hosting;

namespace VNCApp.Areas.api.Controllers.User
{
    public class UserController : BaseService<UserProvider, String, Database.User.User>
    {
        public override ActionResult GetAll()
        {
            throw new NotImplementedException();
        }

        public override ActionResult Delete(string id)
        {
            throw new NotImplementedException();
        }

        [HttpPut]
        public ActionResult UpdateAccount(Database.User.User model)
        {
            var result = dataProvider.Update(requestUserId(), model);
            return ServiceResponse(result);
        }

        [HttpPut]
        public ActionResult ChangePassword(string password)
        {
            var result = dataProvider.ChangePassword(requestUserId(), password);
            return ServiceResponse(result);
        }

        public override ActionResult Get(string id)
        {
            //throw new NotImplementedException();
            var result = dataProvider.GetById(id);
            return ServiceResponse(result);
        }

        public ActionResult GetUserInfo(string username)
        {
            //throw new NotImplementedException();
            var result = dataProvider.GetById(username);
            return ServiceResponse(result);
        }

        //create user account ~ register
        public override ActionResult Create(Database.User.User model)
        {
            var result = dataProvider.Create(model);
            return ServiceResponse(result);
        }

        public ActionResult IsExisted(string username)
        {
            var result = dataProvider.IsExisted(username);
            return ServiceResponse(result);
        }

        public ActionResult ShareOnlineStatusSetting(bool isShared)
        {
            var result = dataProvider.ShareOnlineStatusSetting(requestUserId(), isShared);
            return ServiceResponse(result);
        }

        public ActionResult ShowAvatarSetting(bool isShown)
        {
            var result = dataProvider.ShowAvatarSetting(requestUserId(), isShown);
            return ServiceResponse(result);
        }

        public ActionResult ReceiveStrangeMessageSetting(bool isReceived)
        {
            var result = dataProvider.ReceiveStrangeMessageSetting(requestUserId(), isReceived);
            return ServiceResponse(result);
        }

        public ActionResult ShowYourPhoneNumberSetting(bool isShown)
        {
            var result = dataProvider.ShowYourPhoneNumberSetting(requestUserId(), isShown);
            return ServiceResponse(result);
        }

        public ActionResult UpdateSettingAttribute(int settingValue)
        {
            var result = dataProvider.UpdateSettingAttribute(requestUserId(), settingValue);
            return ServiceResponse(result);
        }

        [HttpPost]
        public ActionResult LoginEmail(String username, String password)
        {
            var result = dataProvider.LoginEmail(username, password);
            return ServiceResponse(result);
        }

        [HttpPost]
        public ActionResult LoginSocial(UserProvider.SocialType socialType, String socialId, String fullName, String avatar, int gender = 1, long lat = 0, long lon = 0)
        {
            var result = dataProvider.LoginSocial(socialType, socialId, fullName, gender, avatar, lat, lon);
            return ServiceResponse(result);
        }

        [HttpPut]
        public ActionResult UpdateLocation(double latitude, double longitude)
        {
            String username = requestUserId();
            var result = dataProvider.UpdateLocation(username, latitude, longitude);
            return ServiceResponse(result);
            //throw new NotImplementedException();
            //String userName = GetRequestedUserIdFromRequest();
            //if (String.IsNullOrWhiteSpace())
        }

        public ActionResult FindFriendAround()
        {
            int page = ConvertUtils.ToInt(Request.QueryString["page"]);
            int size = ConvertUtils.ToInt(Request.QueryString["size"]);
            double latitude = ConvertUtils.ToDouble(Request.QueryString["latitude"]);
            double longitude = ConvertUtils.ToDouble(Request.QueryString["longitude"]);
            int range_age = ConvertUtils.ToInt(Request.QueryString["range_age"]);
            int gender = ConvertUtils.ToInt(Request.QueryString["gender"]);
            int max_distance = ConvertUtils.ToInt(Request.QueryString["max_distance"]);
            int only_friend = ConvertUtils.ToInt(Request.QueryString["only_friend"]);
            String userId = requestUserId();
            if (page == 1 && !String.IsNullOrWhiteSpace(userId))
                Task.Run(() =>
                {
                    new UserProvider().UpdateLocation(userId, latitude, longitude);
                });

            var result = dataProvider.FindFriendAround(userId, latitude, longitude, page, size, range_age, gender, only_friend, max_distance);
            return ServiceResponse(result);
        }

        public override ActionResult Sync(double lastSyncTime)
        {
            throw new NotImplementedException();
        }

        public ActionResult BlockList()
        {
            int limit = ConvertUtils.ToInt(Request.QueryString["limit"], 10);
            String token = Request.QueryString["token"];
            String userId = requestUserId();
            var data = new DataAccess.SendBird.SendBirdProvider().GetBlockList(userId, token, limit);
            return ServiceResponse(data);
        }


        [HttpGet]
        public ActionResult Search()
        {
            String _size = Request.QueryString["size"];
            String _page = Request.QueryString["page"];
            String query = Request.QueryString["query"];
            String time = Request.QueryString["time"];
            int size = ConvertUtils.ToInt(_size, 10);
            int page = ConvertUtils.ToInt(_page, 1);
            return ServiceResponse(dataProvider.Search(page, size, query, time, requestUserId()));
        }

        public override ActionResult Update(String id, Database.User.User model)
        {
            //String username = Request.QueryString["username"];
            var result = dataProvider.Update(id, model);
            return ServiceResponse(result);
        }
        public ActionResult Change_Password_Token(String id, String newPassword, String token)
        {
            using (var context = new UserDbDataContext())
            {
                var user = context.Users.SingleOrDefault(x => x.UserName == id && x.ResetPasswordToken == token);
                if (user != null)
                {
                    user.Password = newPassword.GenerateStringToMD5();
                    user.ResetPasswordToken = null;
                    user.ResetPassExpired = null;
                    context.SubmitChanges();
                    return ServiceResponse(new BaseReturnFunction<Boolean>(StatusFunction.TRUE, true));
                }
            }
            return ServiceResponse(new BaseReturnFunction<Boolean>(StatusFunction.FALSE, "Đổi mật khẩu không thành công"));
        }


        [HttpPost]
        public ActionResult ForgotPassword(String email)
        {
            try
            {
                String token = (DateTime.Now.ToString() + email).GenerateStringToMD5();
                using (var context = new UserDbDataContext())
                {
                    var account = context.Users.SingleOrDefault(x => x.UserName.ToLower().Equals(email.ToLower()));
                    if (account != null)
                    {
                        account.ResetPasswordToken = token;
                        account.ResetPassExpired = DateTime.Now.AddMinutes(60);

                        string subject = @"Thông báo tài khoản - Từ Ứng dụng [iDuhoc]";

                        var content = System.IO.File.ReadAllText(HostingEnvironment.MapPath(@"~/App_Data/template/forgot_password_vi.txt"));

                        if (UtilitiesHelper.SendMail2(email, subject, String.Format(content, account.FullName, token)))
                        {
                            context.SubmitChanges();
                            return ServiceResponse(new BaseReturnFunction<Boolean>(StatusFunction.TRUE, true));
                        }
                    }
                    else {
                        return ServiceResponse(new BaseReturnFunction<Boolean>(StatusFunction.FALSE, "Tài khoản này không tồn tại/ Your account does not exist: " + email));
                    }
                }
            }
            catch (Exception e)
            {
                return ServiceResponse(new BaseReturnFunction<Boolean>(StatusFunction.FALSE, e.Message));

            }
            return ServiceResponse(new BaseReturnFunction<Boolean>(StatusFunction.FALSE, "hehe"));

        }

    }
}