﻿using BaseLibrary.Utils;
using BaseLibrary.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using VNCApp.Database.Tips.Tips;
using VNCApp.Areas.api.Controllers.Notification;

namespace VNCApp.Areas.api.Controllers.Tips
{
    public class TipsController : BaseService<DataAccess.Tips.TipsProvider, int, Tip>
    {
        public override ActionResult GetAll()
        {
            String _page = Request.QueryString["page"];
            String _size = Request.QueryString["size"];
            String _type = Request.QueryString["type"];
            String keyword = Request.QueryString["keyword"];

            int page = ConvertUtils.ToInt(_page, 1);
            int size = ConvertUtils.ToInt(_size, 20);
            int type = ConvertUtils.ToInt(_type, 0);
            if (keyword==null)
                keyword = "";
            var result = dataProvider.GetAll(page, size, type, keyword, requestUserId());
            return ServiceResponse(result);
        }

        public override ActionResult Delete(int id)
        {
            var result = dataProvider.Delete(id);
            return ServiceResponse(result);
        }

        public override ActionResult Update(int id, Tip model)
        {
            var result = dataProvider.Update(id, model);
            return ServiceResponse(result);
        }

        public override ActionResult Get(int id)
        {
            var result = dataProvider.GetById(id);
            return ServiceResponse(result);
        }

        public override ActionResult Create(Tip model)
        {
            model.CreatedDate = DateTime.Now.GetUnixTimeStamp();
            model.UpdatedDate = DateTime.Now.GetUnixTimeStamp();
            var result = dataProvider.Create(model);
            if (result.IsTrue)
                new NotificationController().SendNotificationNewTip(model.Title, model.ID);
            return ServiceResponse(result);
        }

        public override ActionResult Sync(double lastSyncTime)
        {
            throw new NotImplementedException();
        }

        public ActionResult Bookmark(int id, bool bookmark)
        {
            var result = dataProvider.Bookmark(id, bookmark, requestUserId());
            return ServiceResponse(result);
        }

        //// GET: api/Tips
        //public ActionResult Index()
        //{
        //    return View();
        //}

    }
}