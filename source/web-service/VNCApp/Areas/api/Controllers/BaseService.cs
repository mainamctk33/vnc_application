﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json.Linq;
using System.Web.Mvc;
using BaseLibrary.DataAccess;
using BaseLibrary.Helper;
using BaseLibrary.Utils;
using Newtonsoft.Json.Linq;
using VNCApp.DataAccess;

namespace VNCApp.Areas.api.Controllers
{
    /// <summary>
    /// Lớp cung cấp mô hình chuẩn cho controller
    /// </summary>
    /// <typeparam name="T">Provider truy cập databse</typeparam>
    /// <typeparam name="E">Kiểu khóa chính</typeparam>
    /// <typeparam name="F">Bảng trong csdl</typeparam>
    public abstract class BaseService<T, E, F> : Controller where T : IProvider<F, E>, new() where F : new()
    {
        public T dataProvider;

        public BaseService()
        {
            dataProvider = new T();
        }
        /// <summary>
        /// Lay thong tin dang nhap User dua vao thong tin LoginToken tren Auth cua Header
        /// </summary>
        /// <returns></returns>
        public string requestUserId()
        {
            string requestUserId = string.Empty;
            try
            {
                string auth = Request.Headers.GetValues("Auth").FirstOrDefault();
                var authObj = ConvertUtils.toObject<dynamic>(auth);
                if (authObj.LoginToken != null && !string.IsNullOrWhiteSpace(authObj.LoginToken.ToString()))
                {
                    string loginTokenId = authObj.LoginToken.ToString();
                    dynamic validateResult = TokenHelper.ValidateToken(loginTokenId);
                    if (validateResult.Code == (int)TokenHelper.ValidateTokenResult.Success)
                    {
                        // Check LastResetTime
                        var payload = ConvertUtils.toObject<dynamic>(validateResult.Data.ToString());
                        requestUserId = payload.userId.ToString();
                    }
                }
            }
            catch(Exception e)
            {
                return requestUserId;
            }
            return requestUserId;
        }


        public JsonResult ServiceResponse(Object data)
        {
            return new ResponseInfo().ServiceResponse(data);
        }

        [HttpGet]
        public abstract ActionResult GetAll();

        [HttpDelete]
        public abstract ActionResult Delete(E id);

        [HttpPut]
        public abstract ActionResult Update(E id, F model);

        [HttpGet]
        public abstract ActionResult Get(E id);
        [HttpPost]
        public abstract ActionResult Create(F model);

        [HttpGet]
        public abstract ActionResult Sync(double lastSyncTime = 0);
    }
}