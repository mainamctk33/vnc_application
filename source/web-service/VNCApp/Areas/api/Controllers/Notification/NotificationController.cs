﻿using BaseLibrary.Controllers;
using BaseLibrary.Utils;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using VNCApp.Database.Notification;
using VNCApp.Models;

namespace VNCApp.Areas.api.Controllers.Notification
{
    public class NotificationController : BaseController
    {
       public static string applicationID = "AAAAfYYNKeo:APA91bHrtHCB7xd6qMLd30-3e5KhzBU2tctmyJUxnshTb3V-qKzkwW1N-k_Ai3Cz9JX-MJihkaWRaVbguJEmDrhHr-VMvetP8jdks8HSE50K5OAD-G4PZvPkvM7iH-TpltvS0pTeC9Td";
        public static string senderId = "539119921642";

        public enum NotificationType
        {
            NewMessage = 0,
            NewRequestAddFriend=  1,
            NewPost = 2,
            NewJob = 3,
            NewTip = 4,
            ApproveJob = 5

        }
        // GET: api/Notification
        public ActionResult Index()
        {
            return View();
        }


        public ActionResult RegisterToken(String id, String token, String deviceId)
        {
            try
            {
                using (var context = new NotificationDbDataContext())
                {
                    var device = context.Devices.SingleOrDefault(x => x.ID == deviceId);
                    if (device != null)
                    {
                        device.UserName = id;
                        device.Token = token;
                        context.SubmitChanges();
                    }
                    else
                    {
                        device = new Device()
                        {
                            Token = token,
                            ID = deviceId,
                            UserName = id
                        };
                        context.Devices.InsertOnSubmit(device);
                        context.SubmitChanges();
                    }
                    return ServiceResponse(new BaseReturnFunction<Boolean>(StatusFunction.TRUE, true));
                }
            }
            catch (Exception e)
            {
                return ServiceResponse(new BaseReturnFunction<Boolean>(StatusFunction.EXCEPTION, e.Message));
            }
        }


        public ActionResult RemoveToken(String id)
        {
            try
            {
                using (var context = new NotificationDbDataContext())
                {
                    var device = context.Devices.SingleOrDefault(x => x.ID == id);
                    if (device != null)
                    {
                        context.Devices.DeleteOnSubmit(device);
                        context.SubmitChanges();
                    }
                    return ServiceResponse(new BaseReturnFunction<Boolean>(StatusFunction.TRUE, true));
                }
            }
            catch (Exception e)
            {
                return ServiceResponse(new BaseReturnFunction<Boolean>(StatusFunction.EXCEPTION, e.Message));
            }
        }

        public ActionResult SendNotification(String username, String body, String title, String data)
        {
            SendPushNotification(username, body, title, data);
            return ServiceResponse(new BaseReturnFunction<Boolean>(StatusFunction.TRUE, true));
        }

        public ActionResult SendNotificationNewPost(String body, int newsId)
        {
            dynamic data = new ExpandoObject();
            data.type = (int)NotificationType.NewPost;
            data.id = newsId;
            data.title = "Tin tức mới";
            data.content = body;
            SendPushNotificationToTopic("news", data);
            return ServiceResponse(new BaseReturnFunction<Boolean>(StatusFunction.TRUE, true));
        }

        public ActionResult SendNotificationNewTip(string body, int iD)
        {
            dynamic data = new ExpandoObject();
            data.type = (int)NotificationType.NewTip;
            data.id = iD;
            data.title = "Lời khuyên mới";
            data.content = body;

            SendPushNotificationToTopic("tip", data);
            return ServiceResponse(new BaseReturnFunction<Boolean>(StatusFunction.TRUE, true));

        }

        public ActionResult SendNotificationNewJob(String body, int jobId)
        {
            dynamic data = new ExpandoObject();
            data.type = (int)NotificationType.NewJob;
            data.id = jobId;
            data.title = "Công việc mới";
            data.content = body;
            SendPushNotificationToTopic("job", data);
            return ServiceResponse(new BaseReturnFunction<Boolean>(StatusFunction.TRUE, true));
        }

        public ActionResult SendNotificationApproveJob(String username, String body, int jobId)
        {
            dynamic data = new ExpandoObject();
            data.type = (int)NotificationType.ApproveJob;
            data.id = jobId;
            data.from = "Admin";
            //SendPushNotificationToTopic("job", body, "Công việc đã được duyệt", data);
            
            SendPushNotification(username, body, "Công việc đã được duyệt", ConvertUtils.Serialize(data));
            return ServiceResponse(new BaseReturnFunction<Boolean>(StatusFunction.TRUE, true));
        }

        public ActionResult SendNotificationNewRequestAddFriend(String username, String requestFromName, String requestFromUserName)
        {
            dynamic data = new ExpandoObject();
            data.type = (int)NotificationType.NewRequestAddFriend;
            data.from = requestFromUserName;
            SendPushNotification(username,"Bạn có một yêu cầu từ " + requestFromName, "Yêu cầu kết bạn", ConvertUtils.Serialize(data));
            return ServiceResponse(new BaseReturnFunction<Boolean>(StatusFunction.TRUE, true));
        }


        public ActionResult Send(String username, String body, String title, String data)
        {
            SendPushNotification(username, body, title, data);
            return ServiceResponse(new BaseReturnFunction<Boolean>(StatusFunction.TRUE, true));
        }

        public static void SendPushNotificationToTopic(String topic, Object _data)
        {
            try
            {
                new Task(() => {
                    try
                    {
                        WebRequest tRequest = WebRequest.Create("https://fcm.googleapis.com/fcm/send");
                        tRequest.Method = "post";
                        tRequest.ContentType = "application/json";
                        var data = new
                        {
                            condition = "'"+topic+"' in topics",
                            data = new
                            {
                                data = _data
                            }
                            //,
                            //notification = new
                            //{
                            //    body = body,
                            //    title = title,
                            //    sound = "Enabled"
                            //}
                        };

                        var serializer = new JavaScriptSerializer();
                        var json = serializer.Serialize(data);
                        Byte[] byteArray = Encoding.UTF8.GetBytes(json);
                        tRequest.Headers.Add(string.Format("Authorization: key={0}", applicationID));
                        tRequest.Headers.Add(string.Format("Sender: id={0}", senderId));
                        tRequest.ContentLength = byteArray.Length;

                        using (Stream dataStream = tRequest.GetRequestStream())
                        {
                            dataStream.Write(byteArray, 0, byteArray.Length);
                            using (WebResponse tResponse = tRequest.GetResponse())
                            {
                                using (Stream dataStreamResponse = tResponse.GetResponseStream())
                                {
                                    using (StreamReader tReader = new StreamReader(dataStreamResponse))
                                    {
                                        String sResponseFromServer = tReader.ReadToEnd();
                                        string str = sResponseFromServer;
                                    }
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        string str = ex.Message;
                    }
                }).Start();
            }
            catch (Exception)
            {
            }
        }


        public static void SendPushNotification(String userName, String body, String title, Object data)
        {
            try
            {
                using (var context = new NotificationDbDataContext())
                {
                    var devices = context.Devices.Where(x => x.UserName == userName).Select(x => x.Token).ToList();
                    new Task(() =>
                    {
                        devices.ForEach(x =>
                        {
                            SendPushNotificationToDevice(x, body, title, data);
                        });
                    }).Start();
                }
            }
            catch (Exception)
            {
            }
        }

        public static void SendPushNotificationToDevice(String deviceId, String body, String title, dynamic _data)
        {
            try
            {
                WebRequest tRequest = WebRequest.Create("https://fcm.googleapis.com/fcm/send");
                tRequest.Method = "post";
                tRequest.ContentType = "application/json";
                var data = new
                {
                    to = deviceId,
                    data = new {
                        data= _data
                    },
                    notification = new
                    {
                        body = body,
                        title = title,
                        sound = "Enabled"
                    }
                };

                var serializer = new JavaScriptSerializer();
                var json = serializer.Serialize(data);
                Byte[] byteArray = Encoding.UTF8.GetBytes(json);
                tRequest.Headers.Add(string.Format("Authorization: key={0}", applicationID));
                tRequest.Headers.Add(string.Format("Sender: id={0}", senderId));
                tRequest.ContentLength = byteArray.Length;

                using (Stream dataStream = tRequest.GetRequestStream())
                {
                    dataStream.Write(byteArray, 0, byteArray.Length);
                    using (WebResponse tResponse = tRequest.GetResponse())
                    {
                        using (Stream dataStreamResponse = tResponse.GetResponseStream())
                        {
                            using (StreamReader tReader = new StreamReader(dataStreamResponse))
                            {
                                String sResponseFromServer = tReader.ReadToEnd();
                                string str = sResponseFromServer;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string str = ex.Message;
            }
        }
    }
}