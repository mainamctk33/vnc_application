﻿using BaseLibrary.Controllers;
using BaseLibrary.Helper;
using BaseLibrary.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using VNCApp.Database.User.FriendDb;
using BaseLibrary.Common;
using VNCApp.Models;
using System.Dynamic;
using VNCApp.Areas.api.Controllers.Notification;

namespace VNCApp.Areas.api.Controllers.Friend
{
    public class FriendController : BaseController
    {
        public string requestUserId()
        {
            string requestUserId = string.Empty;
            try
            {
                string auth = Request.Headers.GetValues("Auth").FirstOrDefault();
                var authObj = ConvertUtils.toObject<dynamic>(auth);
                if (authObj.LoginToken != null && !string.IsNullOrWhiteSpace(authObj.LoginToken.ToString()))
                {
                    string loginTokenId = authObj.LoginToken.ToString();
                    dynamic validateResult = TokenHelper.ValidateToken(loginTokenId);
                    if (validateResult.Code == (int)TokenHelper.ValidateTokenResult.Success)
                    {
                        // Check LastResetTime
                        var payload = ConvertUtils.toObject<dynamic>(validateResult.Data.ToString());
                        requestUserId = payload.userId.ToString();
                    }
                }
            }
            catch (Exception e)
            {
                return requestUserId;
            }
            return requestUserId;
        }

        public ActionResult SendFriendRequest(String toUsername)
        {
            using (var context = new FriendDbDataContext())
            {
                //UserId =send_request=> FriendId thi trang thai: IsAccepted=false
                var fromUsername = requestUserId();
                var friend = context.Friends.SingleOrDefault(x => x.UserId == fromUsername && x.FriendId == toUsername);
                if (friend == null)
                {
                    friend = new VNCApp.Database.User.FriendDb.Friend()
                    {
                        UserId = fromUsername,
                        FriendId = toUsername,
                        FriendDate = DateTime.Now.GetUnixTimeStamp(),
                        IsAccepted = false
                    };
                    context.Friends.InsertOnSubmit(friend);
                    context.SubmitChanges();
                    new NotificationController().SendNotificationNewRequestAddFriend(friend.FriendId, friend.User.FullName, friend.UserId);
                    return ServiceResponse(new BaseReturnFunction<Boolean>(StatusFunction.TRUE, true));
                }
                return ServiceResponse(new BaseReturnFunction<Boolean>(StatusFunction.FALSE, false));
            }
        }

        public ActionResult CancelFriendRequest(String toUsername)
        {
            try
            {
                using (var context = new FriendDbDataContext())
                {
                    //UserId =cancel_request=> FriendId > do: delete
                    var fromUsername = requestUserId();
                    var friend = context.Friends.SingleOrDefault(x => x.UserId == fromUsername && x.FriendId == toUsername && x.IsAccepted == false);
                    if (friend != null)
                    {
                        context.Friends.DeleteOnSubmit(friend);
                        context.SubmitChanges();
                        return ServiceResponse(new BaseReturnFunction<Boolean>(StatusFunction.TRUE, true));
                    }
                    return ServiceResponse(new BaseReturnFunction<Boolean>(StatusFunction.FALSE, true));
                }
            }
            catch (Exception e)
            {
                return ServiceResponse(new BaseReturnFunction<Boolean>(StatusFunction.EXCEPTION, e.Message));
            }
        }

        public ActionResult DeleteFriendRequest(String requester)
        {
            try
            {
                using (var context = new FriendDbDataContext())
                {
                    //UserId:requester, FriendId: recevier
                    var recevier = requestUserId();//get infor of user who calls API
                    var friend = context.Friends.SingleOrDefault(x => x.UserId == requester && x.FriendId == recevier && x.IsAccepted == false);
                    if (friend != null)
                    {
                        context.Friends.DeleteOnSubmit(friend);
                        context.SubmitChanges();
                        return ServiceResponse(new BaseReturnFunction<Boolean>(StatusFunction.TRUE, true));
                    }
                    return ServiceResponse(new BaseReturnFunction<Boolean>(StatusFunction.FALSE, true));
                }
            }
            catch (Exception e)
            {
                return ServiceResponse(new BaseReturnFunction<Boolean>(StatusFunction.EXCEPTION, e.Message));
            }
        }

        public ActionResult AcceptFriendRequest(String toUsername)
        {
            using (var context = new FriendDbDataContext())
            {
                //UserId =send_request=> FriendId thi trang thai: IsAccepted=false
                //FriendId =accept_request=> UserId >> doi trang thai: IsAccepted=true
                //Vi FriendId nhan request tu UserId -> FriendId la nguoi dong y ket ban
                var acceptor = requestUserId();
                var friend = context.Friends.SingleOrDefault(x => x.FriendId == acceptor && x.UserId == toUsername && x.IsAccepted == false);
                if (friend != null)
                {
                    friend.IsAccepted = true;
                    context.SubmitChanges();
                    return ServiceResponse(new BaseReturnFunction<Boolean>(StatusFunction.TRUE, true));
                }
                return ServiceResponse(new BaseReturnFunction<Boolean>(StatusFunction.FALSE, false));
            }
        }

        public ActionResult IsFriend(String username)
        {
            using (var context = new FriendDbDataContext())
            {

                var userId = requestUserId();
                var friend = context.Friends.SingleOrDefault(x => ((x.UserId == userId && x.FriendId == username) || (x.UserId == username && x.FriendId == userId)) && x.IsAccepted == true);
                if (friend != null)
                {
                    return ServiceResponse(new BaseReturnFunction<Boolean>(StatusFunction.TRUE, true));
                }
                return ServiceResponse(new BaseReturnFunction<Boolean>(StatusFunction.FALSE, false));
            }
        }

        public ActionResult Unfriend(String toUsername)
        {
            try
            {
                using (var context = new FriendDbDataContext())
                {

                    var userId = requestUserId();
                    var friend = context.Friends.SingleOrDefault(x => ((x.UserId == userId && x.FriendId == toUsername) || (x.UserId == toUsername && x.FriendId == userId)) && x.IsAccepted == true);
                    if (friend != null)
                    {
                        context.Friends.DeleteOnSubmit(friend);
                        context.SubmitChanges();
                        return ServiceResponse(new BaseReturnFunction<Boolean>(StatusFunction.TRUE, true));
                    }
                    return ServiceResponse(new BaseReturnFunction<Boolean>(StatusFunction.FALSE, false));
                }

            }
            catch (Exception e)
            {
                return ServiceResponse(new BaseReturnFunction<Boolean>(StatusFunction.EXCEPTION, e.Message));
            }
        }

        public ActionResult GetListFriendRequest()
        {
            int page = ConvertUtils.ToInt(Request.QueryString["page"]);
            int size = ConvertUtils.ToInt(Request.QueryString["size"]);
            String keyword = Request.QueryString["keyword"];

            try
            {
                var userId = requestUserId();
                using (var context = new FriendDbDataContext())
                {

                    IQueryable<VNCApp.Database.User.FriendDb.User> listFriends = context.Friends.Where(x => x.FriendId == userId && x.IsAccepted == false).Select(x=>x.User);
                    if (listFriends != null)
                    {
                        if (!String.IsNullOrWhiteSpace(keyword))
                        {
                            keyword = keyword.ToLower().Trim();

                            listFriends = listFriends.Where(x => x.FullName.ToLower().Contains(keyword) || x.UserName.ToLower().Contains(keyword));
                        }
                        int count = listFriends.Count();
                        return ServiceResponse(new BaseReturnFunction<Object>(StatusFunction.TRUE, listFriends.Select(x => 
                        new {
                            x.Gender,
                            x.UserName,
                            x.FullName,
                            x.Birthday,
                            x.Avartar,
                            x.PhoneNumber,
                            x.Lat,
                            x.Lon,
                            x.Introduction,
                            x.SettingAttribute,
                            x.ShowAvatar,
                            x.ReceiveStrangeMessage,
                            x.ShowPhoneNumber,
                            isFriend = (x.Friends.SingleOrDefault(y => y.IsAccepted && (y.FriendId == userId || y.UserId == userId)) != null || x.Friends1.SingleOrDefault(y => y.IsAccepted && (y.FriendId == userId || y.UserId == userId)) != null)
                        }).ToList(), count));
                    }
                    return ServiceResponse(new BaseReturnFunction<Object>(StatusFunction.FALSE, ""));
                }
            }
            catch (Exception e)
            {
                return ServiceResponse(new BaseReturnFunction<Boolean>(StatusFunction.EXCEPTION, e.Message));
            }
        }
        
        public ActionResult GetListFriend()
        {
            try
            {
                var userId = requestUserId();
                using (var context = new FriendDbDataContext())
                {
                    var listFriends = context.Users.Where(x => x.UserName != userId).Where(x => x.Friends.FirstOrDefault(y => (y.UserId == userId || y.FriendId == userId) && y.IsAccepted) != null);
                    if (listFriends != null)
                    {
                        int count = listFriends.Count();
                        return ServiceResponse(new BaseReturnFunction<Object>(StatusFunction.TRUE, listFriends.Select(x => new {
                            x.Gender,
                            x.UserName,
                            x.FullName,
                            x.Birthday,
                            x.Avartar,
                            x.PhoneNumber,
                            x.Lat,
                            x.Lon,
                            x.Introduction,
                            x.SettingAttribute,
                            x.ShowAvatar,
                            x.ReceiveStrangeMessage,
                            x.ShowPhoneNumber,
                            isFriend = true
                        }).ToList(), listFriends.Count()));
                    }
                    return ServiceResponse(new BaseReturnFunction<Object>(StatusFunction.FALSE, ""));
                }
            }
            catch (Exception e)
            {
                return ServiceResponse(new BaseReturnFunction<Boolean>(StatusFunction.EXCEPTION, e.Message));
            }
        }

        public ActionResult GetFriendStatus(String toUsername)
        {
            try
            {
                //chua viet xong ham nay
                var userId = requestUserId();
                using (var context = new FriendDbDataContext())
                {
                    var friend = context.Friends.SingleOrDefault(x => ((x.UserId == userId && x.FriendId == toUsername) || (x.UserId == toUsername && x.FriendId == userId)) && x.IsAccepted == true);
                    if (friend != null)
                        return ServiceResponse(new BaseReturnFunction<Object>(StatusFunction.TRUE, "Friend"));

                    friend = context.Friends.SingleOrDefault(x => x.UserId == userId && x.FriendId == toUsername && x.IsAccepted == false);
                    if (friend != null)
                        return ServiceResponse(new BaseReturnFunction<Object>(StatusFunction.TRUE, "Sent Request"));

                    friend = context.Friends.SingleOrDefault(x => x.UserId == toUsername && x.FriendId == userId && x.IsAccepted == false);
                    if (friend != null)
                        return ServiceResponse(new BaseReturnFunction<Object>(StatusFunction.TRUE, "Received Request"));

                    return ServiceResponse(new BaseReturnFunction<Object>(StatusFunction.TRUE, "Not Friend"));
                }
            }
            catch (Exception e)
            {
                return ServiceResponse(new BaseReturnFunction<Boolean>(StatusFunction.EXCEPTION, e.Message));
            }
        }

        public ActionResult search()
        {
            int page = ConvertUtils.ToInt(Request.QueryString["page"]);
            int size = ConvertUtils.ToInt(Request.QueryString["size"]);
            String keyword = Request.QueryString["keyword"];
            try
            {
                var userId = requestUserId();
                using (var context = new FriendDbDataContext())
                {
                    var listFriends = context.Friends.Where(x => (x.UserId == userId || x.FriendId == userId)&&x.IsAccepted);
                    if (!String.IsNullOrWhiteSpace(keyword))
                    {
                        keyword = keyword.ToLower().Trim();
                        listFriends = listFriends.Where(x => (x.User.UserName==userId && x.User1.FullName!=null && x.User1.FullName.ToLower().Contains(keyword)) || (x.User1.UserName == userId && x.User.FullName != null && x.User.FullName.ToLower().Contains(keyword)));
                    }
                    var list1 = listFriends.Where(x => x.User.UserName == userId).Select(x => x.User1).ToList();
                    var list2 = listFriends.Where(x => x.User1.UserName == userId).Select(x => x.User).ToList();
                    list2.AddRange(list1);
                    var total = list2.Count();
                    var result = list2.Skip((page - 1) * size).Take(size).ToList().Select(x => 
                    new {
                        x.Gender,
                        x.UserName,
                        x.FullName,
                        x.Birthday,
                        x.Avartar,
                        x.PhoneNumber,
                        x.Lat,
                        x.Lon,
                        x.Introduction,
                        x.SettingAttribute,
                        x.ShowAvatar,
                        x.ReceiveStrangeMessage,
                        x.ShowPhoneNumber,
                        isFriend = true
                    }
                    );
                    return ServiceResponse(new BaseReturnFunction<Object>(StatusFunction.TRUE, result, total));
                }
            }
            catch (Exception e)
            {
                return ServiceResponse(new BaseReturnFunction<Boolean>(StatusFunction.EXCEPTION, e.Message));
            }
        }

    }
}