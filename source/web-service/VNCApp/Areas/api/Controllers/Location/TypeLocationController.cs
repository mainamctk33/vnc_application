﻿using System;
using System.Web.Mvc;
using BaseLibrary.Utils;
using VNCApp.DataAccess;
using VNCApp.Database.Location.TypeLocation;

namespace VNCApp.Areas.api.Controllers.Location
{
    public class TypeLocationController : BaseService<TypeLocationProvider, int, TypeLocation>
    {
        public override ActionResult GetAll()
        {
            var result = dataProvider.GetAll();
            return ServiceResponse(result);
        }
        [HttpGet]
        public ActionResult Search()
        {
            String _page = Request.QueryString["page"];
            String _size = Request.QueryString["size"];
            String keyword = Request.QueryString["keyword"];
            int page = ConvertUtils.ToInt(_page, 1);
            int size = ConvertUtils.ToInt(_size, 20);
            var result = dataProvider.Search(page, size, keyword);
            return ServiceResponse(result);
        }

        public override ActionResult Delete(int id)
        {
            var result = dataProvider.Delete(id);
            return ServiceResponse(result);
        }

        public override ActionResult Update(int id, TypeLocation model)
        {
            var result = dataProvider.Update(id, model);
            return ServiceResponse(result);
        }

        public override ActionResult Get(int id)
        {
            var result = dataProvider.GetById(id);
            return ServiceResponse(result);
        }

        public override ActionResult Create(TypeLocation model)
        {
            var result = dataProvider.Create(model);
            return ServiceResponse(result);
        }

        public override ActionResult Sync(double lastSyncTime = 0)
        {
            var result = dataProvider.Sync(lastSyncTime);
            return ServiceResponse(result);
        }
    }
}