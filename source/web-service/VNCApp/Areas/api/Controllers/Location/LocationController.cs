﻿using BaseLibrary.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using VNCApp.Database.Location.LocationDb;
using BaseLibrary.Common;
using VNCApp.DataAccess.Location;
using VNCApp.Models;
using BaseLibrary.Utils;
using System.Threading.Tasks;
using VNCApp.DataAccess.User;

namespace VNCApp.Areas.api.Controllers.Location
{
    public class LocationController : BaseService<LocationProvider, int, Database.Location.LocationDb.Location>
    {

        public override ActionResult Create(Database.Location.LocationDb.Location model)
        {
            model.CreatedBy = requestUserId();
            model.CreatedDate = model.UpdatedDate = DateTime.Now.GetUnixTimeStamp();
            model.Rate = 0;
            model.TotalMemberRate = 0;
            if (model.Address == null)
                model.Address = "";
            var result = dataProvider.Create2(model);
            return ServiceResponse(result);
        }

        [HttpPut]
        public ActionResult Approve(int id, bool approve)
        {
            var result = dataProvider.Approve(requestUserId(), id, approve);
            return ServiceResponse(result);
        }


        public ActionResult Approve(int id)
        {
            var result = dataProvider.Approve(requestUserId(), id, true);
            return ServiceResponse(result);
        }


        public override ActionResult Delete(int id)
        {
            var result = dataProvider.Delete(id);
            return ServiceResponse(result);
        }

        public override ActionResult Get(int id)
        {
            var result = dataProvider.GetById(id);
            return ServiceResponse(result);
        }

        public override ActionResult GetAll()
        {
            var result = dataProvider.GetAll();
            return ServiceResponse(result);
        }

        public override ActionResult Sync(double lastSyncTime = 0)
        {
            throw new NotImplementedException();
        }

        public override ActionResult Update(int id, Database.Location.LocationDb.Location model)
        {
            var result = dataProvider.Update(id, model);
            return ServiceResponse(result);
        }

        public ActionResult ListLocation()
        {
            int page = ConvertUtils.ToInt(Request.QueryString["page"]);
            int size = ConvertUtils.ToInt(Request.QueryString["size"]);
            String name = Request.QueryString["name"];
            int status = ConvertUtils.ToInt(Request.QueryString["status"]);
            int type = ConvertUtils.ToInt(Request.QueryString["type"]);
            try
            {
                var userId = requestUserId();
                if (name == null)
                    name = "";
                name = name.ToLower().Trim();
                using (var context = new VNCApp.Database.Location.Location.LocationDbDataContext())
                {
                    IQueryable<VNCApp.Database.Location.Location.Location> listLocation =
                        context.Locations.Where(x => (String.IsNullOrWhiteSpace(name) || x.Title.ToLower().Contains(name)) && (status == -1 || (status == 1 && x.ApproveBy != null) || (status == 0 && x.ApproveBy == null)) && (type == 0 || x.Type == type));
                    var total = listLocation.Count();
                    var result = listLocation.OrderByDescending(x => x.CreatedDate).OrderByDescending(x=>x.ApproveDate).Skip((page - 1) * size).Take(size).ToList().Select(x => new { CreatedBy = x.User.FullName, x.CreatedDate, x.Title, x.Address, x.Contact, x.Thumbnail, x.Lat, x.Lon, x.ID, x.Content, x.Rate, x.TotalMemberRate, Type = x.Type, TypeName = x.TypeLocation.Name, TypeLogo = x.TypeLocation.Logo, ApproveDate = x.ApproveDate.HasValue ? x.ApproveDate.Value + "" : "" }).ToList();
                    return ServiceResponse(new BaseReturnFunction<Object>(StatusFunction.TRUE, result, total));
                }
            }
            catch (Exception e)
            {
                return ServiceResponse(new BaseReturnFunction<Boolean>(StatusFunction.EXCEPTION, e.Message));
            }
        }

        public ActionResult Search_By_Name()
        {
            int page = ConvertUtils.ToInt(Request.QueryString["page"]);
            int size = ConvertUtils.ToInt(Request.QueryString["size"]);
            String keyword = Request.QueryString["keyword"];
            try
            {
                var userId = requestUserId();
                using (var context = new LocationDbDataContext())
                {
                    IQueryable<VNCApp.Database.Location.LocationDb.Location> listLocation = context.Locations.Where(x=>x.ApproveBy!=null);
                    if (!String.IsNullOrWhiteSpace(keyword))
                    {
                        keyword = keyword.ToLower().Trim();
                        listLocation = listLocation.Where(x => x.Title != null && x.Title.ToLower().Contains(keyword));
                    }
                    var total = listLocation.Count();
                    var result = listLocation.OrderByDescending(x => x.CreatedDate).OrderByDescending(x => x.ApproveDate).Skip((page - 1) * size).Take(size).ToList().Select(x => new { x.Title, x.Address, x.Contact, x.Thumbnail, x.Lat, x.Lon, x.ID, x.Content, x.Rate, x.TotalMemberRate, Type = x.Type, TypeName = x.TypeLocation.Name, TypeLogo = x.TypeLocation.Logo }).ToList();
                    return ServiceResponse(new BaseReturnFunction<Object>(StatusFunction.TRUE, result, total));
                }
            }
            catch (Exception e)
            {
                return ServiceResponse(new BaseReturnFunction<Boolean>(StatusFunction.EXCEPTION, e.Message));
            }
        }


        public ActionResult Search_My_Location()
        {
            int page = ConvertUtils.ToInt(Request.QueryString["page"]);
            int size = ConvertUtils.ToInt(Request.QueryString["size"]);
            String keyword = Request.QueryString["keyword"];
            try
            {
                var userId = requestUserId();
                using (var context = new LocationDbDataContext())
                {
                    IQueryable<VNCApp.Database.Location.LocationDb.Location> listLocation = context.Locations.Where(x => x.CreatedBy == userId);
                    if (!String.IsNullOrWhiteSpace(keyword))
                    {
                        keyword = keyword.ToLower().Trim();
                        listLocation = listLocation.Where(x => x.Title != null && x.Title.ToLower().Contains(keyword));
                    }
                    var total = listLocation.Count();
                    var result = listLocation.OrderByDescending(x => x.CreatedDate).OrderByDescending(x => x.ApproveDate).Skip((page - 1) * size).Take(size).ToList().Select(x => new { x.Title, x.Address, x.CreatedDate, x.Contact, x.Thumbnail, x.Lat, x.Lon, x.ID, x.Content, x.Rate, x.TotalMemberRate, Type = x.Type, TypeName = x.TypeLocation.Name, TypeLogo = x.TypeLocation.Logo, x.ApproveBy, ApproveDate = x.ApproveDate.HasValue ? (x.ApproveDate + "") : "" }).ToList();
                    return ServiceResponse(new BaseReturnFunction<Object>(StatusFunction.TRUE, result, total));
                }
            }
            catch (Exception e)
            {
                return ServiceResponse(new BaseReturnFunction<Boolean>(StatusFunction.EXCEPTION, e.Message));
            }
        }

        public ActionResult Search_Location_Bookmark()
        {
            int page = ConvertUtils.ToInt(Request.QueryString["page"]);
            int size = ConvertUtils.ToInt(Request.QueryString["size"]);
            String keyword = Request.QueryString["keyword"];
            try
            {
                var userId = requestUserId();
                using (var context = new VNCApp.Database.Location.LocationBookmark.LocationBookmarkDataContext())
                {
                    IQueryable<VNCApp.Database.Location.LocationBookmark.Location> listLocation = context.BookmarkLocations.Where(x => x.BookmarkBy == userId && (x.Location.ApproveBy != null || x.Location.CreatedBy==userId)).Select(x => x.Location);
                    if (!String.IsNullOrWhiteSpace(keyword))
                    {
                        keyword = keyword.ToLower().Trim();
                        listLocation = listLocation.Where(x => x.Title != null && x.Title.ToLower().Contains(keyword));
                    }
                    var total = listLocation.Count();
                    var result = listLocation.OrderByDescending(x => x.CreatedDate).OrderByDescending(x => x.ApproveDate).Skip((page - 1) * size).Take(size).ToList().Select(x => new { x.Title, x.Address, x.Contact, x.Thumbnail, x.Lat, x.Lon, x.ID, x.Content, x.Rate, x.TotalMemberRate, Type = x.Type, TypeName = x.TypeLocation.Name, TypeLogo = x.TypeLocation.Logo }).ToList();
                    return ServiceResponse(new BaseReturnFunction<Object>(StatusFunction.TRUE, result, total));
                }
            }
            catch (Exception e)
            {
                return ServiceResponse(new BaseReturnFunction<Boolean>(StatusFunction.EXCEPTION, e.Message));
            }
        }

        public ActionResult Near()
        {
            double lat = ConvertUtils.ToDouble(Request.QueryString["lat"]);
            double lon = ConvertUtils.ToDouble(Request.QueryString["lon"]);
            int type = ConvertUtils.ToInt(Request.QueryString["type"], 0);
            int page = ConvertUtils.ToInt(Request.QueryString["page"]);
            int size = ConvertUtils.ToInt(Request.QueryString["size"]);

            String userId = requestUserId();
            if (page == 1)
            {
                if (!String.IsNullOrWhiteSpace(userId))
                    Task.Run(() =>
                    {
                        new UserProvider().UpdateLocation(userId, lat, lon);
                    });
            }

            try
            {
                using (var context = new LocationDbDataContext())
                {
                    IQueryable<VNCApp.Database.Location.LocationDb.Location> listLocation = context.Locations.Where(x=>x.ApproveBy!=null);
                    if (type != 0)
                        listLocation = listLocation.Where(x => x.Type == type);
                    var temp = listLocation.ToList();
                    var list = temp.Select(x => new { Location = x, Distance = DistanceUtils.getDistance(lat, lon, x.Lat, x.Lon) }).OrderBy(x => x.Distance);
                    var total = list.Count();
                    var result = list.Skip((page - 1) * size).Take(size).ToList().Select(x => new { x.Location.Title, x.Location.Address, x.Location.Contact, x.Location.Thumbnail, x.Location.Lat, x.Location.Lon, x.Location.ID, x.Location.Content, x.Location.Rate, x.Location.TotalMemberRate, Type = x.Location.Type, TypeName = x.Location.TypeLocation.Name, TypeLogo = x.Location.TypeLocation.Logo, x.Distance }).ToList();
                    return ServiceResponse(new BaseReturnFunction<Object>(StatusFunction.TRUE, result, total));
                }
            }
            catch (Exception e)
            {
                return ServiceResponse(new BaseReturnFunction<Boolean>(StatusFunction.EXCEPTION, e.Message));
            }
        }

        [HttpPut]
        public ActionResult Rate(int id, float value, String comment)
        {
            String userId = requestUserId();
            if (value != 0)
            {
                using (var context = new VNCApp.Database.Location.Location.LocationDbDataContext())
                {
                    var myRate = context.UserRateLocations.SingleOrDefault(x => x.UserName == userId && x.LocationId == id);
                    if (myRate != null)
                    {
                        myRate.Value = value;
                        myRate.Comment = comment;
                        myRate.UpdatedDate = DateTime.Now.GetUnixTimeStamp();

                        var location = context.Locations.SingleOrDefault(x => x.ID == id);
                        if (location != null)
                        {
                            var listTemp = location.UserRateLocations.ToList();
                            listTemp.Add(myRate);
                            var rate = Math.Round(listTemp.Average(x => x.Value), 2);
                            location.Rate = rate;
                            location.TotalMemberRate = location.UserRateLocations.Count();
                        }
                        context.SubmitChanges();
                    }
                    else
                    {
                        myRate = new Database.Location.Location.UserRateLocation()
                        {
                            LocationId = id,
                            Comment = "",
                            UserName = userId,
                            Value = value,
                            CreatedDate = DateTime.Now.GetUnixTimeStamp(),
                            UpdatedDate = DateTime.Now.GetUnixTimeStamp()
                        };
                        context.UserRateLocations.InsertOnSubmit(myRate);

                        var location = context.Locations.SingleOrDefault(x => x.ID == id);
                        if (location != null)
                        {
                            var listTemp = location.UserRateLocations.ToList();
                            listTemp.Add(myRate);
                            var rate = Math.Round(listTemp.Average(x => x.Value), 2);
                            location.Rate = rate;
                            location.TotalMemberRate = location.UserRateLocations.Count();
                        }
                        context.SubmitChanges();
                    }
                    return ServiceResponse(new BaseReturnFunction<Boolean>(StatusFunction.TRUE, true));
                }
            }
            return ServiceResponse(new BaseReturnFunction<Boolean>(StatusFunction.FALSE, false));
        }
        [HttpPut]
        public ActionResult Bookmark(int id, bool bookmark)
        {
            var result = dataProvider.Bookmark(id, bookmark, requestUserId());
            return ServiceResponse(result);
        }
        [HttpGet]
        public ActionResult IsBookmark(int id)
        {
            var result = dataProvider.IsBookmark(id, requestUserId());
            return ServiceResponse(result);
        }
        [HttpGet]
        public ActionResult MyRate(int id)
        {
            var result = dataProvider.MyRating(id, requestUserId());
            return ServiceResponse(result);
        }

        public ActionResult GetPending()
        {
            var result = dataProvider.GetPending();
            return ServiceResponse(result);
        }


    }
}