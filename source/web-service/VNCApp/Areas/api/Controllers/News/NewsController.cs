﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BaseLibrary.Common;
using BaseLibrary.Helper;
using BaseLibrary.Utils;
using VNCApp.DataAccess.News;
using VNCApp.Database.News.News;
using VNCApp.Database.News.Tag;
using VNCApp.Filter;
using VNCApp.Areas.api.Controllers.Notification;

namespace VNCApp.Areas.api.Controllers.News
{
    public class NewsController : BaseService<NewsProvider, int, New>
    {
        public override ActionResult GetAll()
        {
            String _page = Request.QueryString["page"];
            String _size = Request.QueryString["size"];
            String _type = Request.QueryString["type"];
            String tag = Request.QueryString["tag"];
            String keyword = Request.QueryString["keyword"];


            int page = ConvertUtils.ToInt(_page, 1);
            int size = ConvertUtils.ToInt(_size, 20);
            int type = ConvertUtils.ToInt(_type, 0);

            var result = dataProvider.GetAll(page, size, tag, type, keyword, requestUserId());
            return ServiceResponse(result);
        }

        public override ActionResult Delete(int id)
        {
            var result = dataProvider.Delete(id);
            return ServiceResponse(result);
        }

        public override ActionResult Update(int id, New model)
        {
            var result = dataProvider.Update(id, model);
            return ServiceResponse(result);
        }
        [HttpPut]
        public ActionResult UpdateWithTag(int id, New model, String tag)
        {
            try
            {
                model.UpdatedDate = DateTime.Now.GetUnixTimeStamp();
                var tags = tag.Split(',');
                TagProvider tagProvider = new TagProvider();
                NewsProvider newProvider = new NewsProvider();
                var result = newProvider.Update(model.ID, model);
                if (result.IsTrue)
                {
                    if (!String.IsNullOrWhiteSpace(tag))
                    {
                        tagProvider.ClearAllTagOfNews(model.ID);
                        foreach (var item in tags)
                        {
                            var result2 = tagProvider.Create(new Tag()
                            {
                                TagName = item,
                                IsHashTag = false
                            });
                            if (result2.IsTrue)
                            {
                                tagProvider.AssignNews(item, model.ID);
                            }                        
                        }
                    }
                }
                return ServiceResponse(result);
            }
            catch (Exception e)
            {
                return ServiceResponse(e.Message);
            }
        }


        public override ActionResult Get(int id)
        {
            var result = dataProvider.GetById(id);
            return ServiceResponse(result);
        }

        public override ActionResult Create(New model)
        {
            model.CreatedDate = DateTime.Now.GetUnixTimeStamp();
            model.UpdatedDate = DateTime.Now.GetUnixTimeStamp();
            model.CreatedBy = "mainam";
            var result = dataProvider.Create(model);
            if(result.IsTrue)
            new NotificationController().SendNotificationNewPost(model.Title, model.ID);
            return ServiceResponse(result);
        }

        [HttpPost]
        public ActionResult CreateWithTag(New model, String tag)
        {
            try
            {
                model.CreatedDate = DateTime.Now.GetUnixTimeStamp();
                model.UpdatedDate = DateTime.Now.GetUnixTimeStamp();
                model.CreatedBy = "mainam";
                var tags = tag.Split(',');
                TagProvider tagProvider = new TagProvider();
                NewsProvider newProvider = new NewsProvider();
                var result = newProvider.Create(model);
                if (result.IsTrue)
                {
                    if (!String.IsNullOrWhiteSpace(tag))
                    {
                        foreach (var item in tags)
                        {
                            var result2 = tagProvider.Create(new Tag()
                            {
                                TagName = item,
                                IsHashTag = false
                            });
                            if (result2.IsTrue)
                            {
                                tagProvider.AssignNews(item, result.Data.ID);
                            }                        
                        }
                    }
                }
                if(result.IsTrue)
                new NotificationController().SendNotificationNewPost(model.Title, model.ID);
                return ServiceResponse(result);

            }
            catch (Exception e)
            {
                return ServiceResponse(e.Message);
            }
        }

        public override ActionResult Sync(double lastSyncTime)
        {
            throw new NotImplementedException();
        }

        [HttpPut]
        public ActionResult Bookmark(int id, bool bookmark)
        {
            var result = dataProvider.Bookmark(id, bookmark, requestUserId());
            return ServiceResponse(result);
        }


    }
}