﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using VNCApp.DataAccess.News;
using VNCApp.Database.News.Tag;

namespace VNCApp.Areas.api.Controllers.News
{
    public class TagController : BaseService<TagProvider, String, Tag>
    {
        public override ActionResult GetAll()
        {
            return ServiceResponse(dataProvider.GetAll());
        }

        public override ActionResult Delete(string id)
        {
            return ServiceResponse(dataProvider.Delete(id));
        }

        public override ActionResult Update(string id, Tag model)
        {
            return ServiceResponse(dataProvider.Update(id, model));
        }

        public override ActionResult Get(string id)
        {
            return ServiceResponse(dataProvider.GetById(id));
        }

        public override ActionResult Create(Tag model)
        {
            return ServiceResponse(dataProvider.Create(model));
        }

        public override ActionResult Sync(double lastSyncTime = 0)
        {            
            return ServiceResponse(dataProvider.Sync(lastSyncTime));
        }
    }
}