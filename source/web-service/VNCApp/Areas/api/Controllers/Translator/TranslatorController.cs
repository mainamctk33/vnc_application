﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BaseLibrary.Common;
using BaseLibrary.Helper;
using BaseLibrary.Utils;
using VNCApp.DataAccess.Translators;
using VNCApp.Filter;
using VNCApp.Database.Translators.Translator;

namespace VNCApp.Areas.api.Controllers.Translators
{
    public class TranslatorController : BaseService<TranslatorProvider, int, Translator>
    {
        public override ActionResult GetAll()
        {
            String _page = Request.QueryString["page"];
            String _size = Request.QueryString["size"];
            String _type = Request.QueryString["type"];
            String _category = Request.QueryString["category"];
            String _keyword = Request.QueryString["keyword"];


            int page = ConvertUtils.ToInt(_page, 1);
            int size = ConvertUtils.ToInt(_size, 20);
            int type = ConvertUtils.ToInt(_type, 0);
            int category = ConvertUtils.ToInt(_category, 0);

            var result = dataProvider.GetAll(page, size, type, category, _keyword, requestUserId());
            return ServiceResponse(result);
        }
        public ActionResult GetAllNew()
        {
            var result = dataProvider.GetAllNew();
            return ServiceResponse(result);
        }
        public override ActionResult Delete(int id)
        {
            var result = dataProvider.Delete(id);
            return ServiceResponse(result);
        }

        public override ActionResult Update(int id, Translator model)
        {
            var result = dataProvider.Update(id, model);
            return ServiceResponse(result);
        }

        public override ActionResult Get(int id)
        {
            var result = dataProvider.GetById(id);
            return ServiceResponse(result);
        }

        public override ActionResult Create(Translator model)
        {
            model.CreatedDate = DateTime.Now.GetUnixTimeStamp();
            model.UpdatedDate = DateTime.Now.GetUnixTimeStamp();
            var result = dataProvider.Create(model);
            return ServiceResponse(result);
        }

       

        public override ActionResult Sync(double lastSyncTime)
        {
            throw new NotImplementedException();
        }

        [HttpPut]
        public ActionResult Bookmark(int id, bool bookmark)
        {
            var result = dataProvider.Bookmark(id, bookmark, requestUserId());
            return ServiceResponse(result);
        }


    }
}