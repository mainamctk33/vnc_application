﻿using BaseLibrary.Common;
using BaseLibrary.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using VNCApp.Areas.api.Controllers.Notification;
using VNCApp.DataAccess.Jobs;
using VNCApp.Database.Jobs.Jobs;

namespace VNCApp.Areas.api.Controllers.Jobs
{
    public class JobController : BaseService<JobProvider, int, Job>
    {
        public override ActionResult Create(Job model)
        {
            model.CreatedDate = DateTime.Now.GetUnixTimeStamp();
            string createdBy = requestUserId();
            if (string.IsNullOrEmpty(createdBy))
                createdBy = "Admin";
            model.CreatedBy = createdBy;
            if (model.Address == null)
                model.Address = "";
            var result = dataProvider.Create(model);
            if (result.IsTrue)
            {
                new NotificationController().SendNotificationNewJob(model.Title, model.ID);
            }
            return ServiceResponse(result);
        }

        public override ActionResult Delete(int id)
        {
            var result = dataProvider.Delete(id);
            return ServiceResponse(result);
        }

        public override ActionResult Get(int id)
        {
            var result = dataProvider.GetByIdMoreInfo(id);
            return ServiceResponse(result);
        }


        public override ActionResult GetAll()
        {
            String _page = Request.QueryString["page"];
            String _size = Request.QueryString["size"];
            String _type = Request.QueryString["type"];
            String _keyword = Request.QueryString["keyword"];

            int page = ConvertUtils.ToInt(_page, 1);
            int size = ConvertUtils.ToInt(_size, 20);
            int type = ConvertUtils.ToInt(_type, 0);

            var result = dataProvider.GetAll(page, size, type, _keyword);
            return ServiceResponse(result);
        }

        public ActionResult GetAllWithCity()
        {
            String _page = Request.QueryString["page"];
            String _size = Request.QueryString["size"];
            String _type = Request.QueryString["type"];


            int page = ConvertUtils.ToInt(_page, 1);
            int size = ConvertUtils.ToInt(_size, 20);
            int type = ConvertUtils.ToInt(_type, 0);

            var result = dataProvider.GetAllWithCity(page, size, type, requestUserId());
            return ServiceResponse(result);
        }

        public ActionResult GetAllWithCityMine()
        {
            String _page = Request.QueryString["page"];
            String _size = Request.QueryString["size"];
            String _type = Request.QueryString["type"];


            int page = ConvertUtils.ToInt(_page, 1);
            int size = ConvertUtils.ToInt(_size, 20);
            int type = ConvertUtils.ToInt(_type, 0);

            var result = dataProvider.GetAllWithCityMine(page, size, type, requestUserId());
            return ServiceResponse(result);
        }

        public ActionResult SearchWithCity()
        {
            //(int page, int size, int country, int city, int type, int level, int salary, int company)
            String _page = Request.QueryString["page"];
            String _size = Request.QueryString["size"];
            String _title = Request.QueryString["title"];
            String _country = Request.QueryString["country"];
            String _city = Request.QueryString["city"];
            String _type = Request.QueryString["type"];
            String _level = Request.QueryString["level"];
            String _salaryFrom = Request.QueryString["salaryFrom"];
            String _salaryTo = Request.QueryString["salaryTo"];
            String _company = Request.QueryString["company"];
            String _sort = Request.QueryString["sort"];


            int page = ConvertUtils.ToInt(_page, 1);
            int size = ConvertUtils.ToInt(_size, 20);
            string title = _title != null ? _title : "";
            int country = ConvertUtils.ToInt(_country, -1);
            int city = ConvertUtils.ToInt(_city, -1);
            int type = ConvertUtils.ToInt(_type, -1);
            int level = ConvertUtils.ToInt(_level, -1);
            int salaryFrom = ConvertUtils.ToInt(_salaryFrom, -1);
            int salaryTo = ConvertUtils.ToInt(_salaryTo, -1);
            string company = _company != null ? _company : "";
            int sort = ConvertUtils.ToInt(_sort, -1);
            

            var result = dataProvider.SearchWithCity(page, size, title, country, city, type, level, salaryFrom, salaryTo, company,sort);
            return ServiceResponse(result);
        }


        public override ActionResult Sync(double lastSyncTime = 0)
        {
            var result = dataProvider.Sync(lastSyncTime);
            return ServiceResponse(result);
        }

        public override ActionResult Update(int id, Job model)
        {
            if (String.IsNullOrWhiteSpace(model?.Address))
                model.Address = "";
            var result = dataProvider.Update(id, model);
            return ServiceResponse(result);
        }
        [HttpPut]
        public  ActionResult Approve(int id)
        {
            string approveBy = "";
            var result = dataProvider.Approve(id, approveBy);
            if (result.IsTrue)
            {
                var temp = dataProvider.GetById(id);
                if (temp.IsTrue)
                {
                    var jobTemp = temp.Data;
                    new NotificationController().SendNotificationApproveJob(jobTemp.CreatedBy, jobTemp.Title, jobTemp.ID);
                }
            }
            return ServiceResponse(result);
        }
        [HttpPut]
        public ActionResult Bookmark(int id, bool bookmark)
        {
            var result = dataProvider.Bookmark(id, bookmark, requestUserId());
            return ServiceResponse(result);
        }

        public ActionResult GetPending()
        {
            var result = dataProvider.GetPending();
            return ServiceResponse(result);
        }
    }
}