﻿using BaseLibrary.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using VNCApp.DataAccess.Jobs;
using VNCApp.Database.Jobs.SalaryJob;

namespace VNCApp.Areas.api.Controllers.Jobs
{
    public class SalaryJobController : BaseService<SalaryJobProvider, int, SalaryJob>
    {
        public override ActionResult Create(SalaryJob model)
        {
            return ServiceResponse(dataProvider.Create(model));
        }

        public override ActionResult Delete(int id)
        {
            return ServiceResponse(dataProvider.Delete(id));
        }

        public override ActionResult Get(int id)
        {
            return ServiceResponse(dataProvider.GetById(id));
        }

        public override ActionResult GetAll()
        {
            return ServiceResponse(dataProvider.GetAll());
        }

        public override ActionResult Sync(double lastSyncTime = 0)
        {
            return ServiceResponse(dataProvider.Sync(lastSyncTime));
        }

        public override ActionResult Update(int id, SalaryJob model)
        {
            return ServiceResponse(dataProvider.Update(id, model));
        }
        [HttpGet]
        public ActionResult Search()
        {
            String _page = Request.QueryString["page"];
            String _size = Request.QueryString["size"];
            String keyword = Request.QueryString["keyword"];
            int page = ConvertUtils.ToInt(_page, 1);
            int size = ConvertUtils.ToInt(_size, 20);
            var result = dataProvider.Search(page, size, keyword);
            return ServiceResponse(result);
        }
    }
}
