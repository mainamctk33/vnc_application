﻿using BaseLibrary.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using VNCApp.DataAccess.Jobs;
using VNCApp.Database.Jobs.CityJob;

namespace VNCApp.Areas.api.Controllers.Jobs
{
    public class CityJobController : BaseService<CityJobProvider, int, CityJob>
    {
        public override ActionResult Create(CityJob model)
        {
            return ServiceResponse(dataProvider.Create(model));
        }

        public override ActionResult Delete(int id)
        {
            return ServiceResponse(dataProvider.Delete(id));
        }

        public override ActionResult Get(int id)
        {
            return ServiceResponse(dataProvider.GetById(id));
        }

        public override ActionResult GetAll()
        {
            return ServiceResponse(dataProvider.GetAll());
        }

       

        public override ActionResult Sync(double lastSyncTime = 0)
        {
            return ServiceResponse(dataProvider.Sync(lastSyncTime));
        }

        public override ActionResult Update(int id, CityJob model)
        {
            return ServiceResponse(dataProvider.Update(id, model));
        }
        [HttpGet]
        public ActionResult Search()
        {
            String _page = Request.QueryString["page"];
            String _size = Request.QueryString["size"];
            String keyword = Request.QueryString["keyword"];
            String _country = Request.QueryString["country"];
            int page = ConvertUtils.ToInt(_page, 1);
            int size = ConvertUtils.ToInt(_size, 20);
            int country = ConvertUtils.ToInt(_country, -1);
            var result = dataProvider.Search(page, size, keyword,country);
            return ServiceResponse(result);
        }
    }
}