﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BaseLibrary.DataAccess;
using BaseLibrary.Utils;
using VNCApp.Models;

namespace VNCApp.Areas.api.Controllers.Chat
{
    public class ChatController :Controller
    {
        // GET: api/ChatController
        public ActionResult CreateAccount(String username, String fullname, String avatar)
        {
            var data = new DataAccess.SendBird.SendBirdProvider().CreateAccount(username, fullname,avatar);
            return new ResponseInfo().ServiceResponse(data);
        }
        public ActionResult LeaveGroup(String username, String fullname, String channelUrl)
        {
            var data = new DataAccess.SendBird.SendBirdProvider().LeaveGroup(username, fullname, channelUrl);
            return new ResponseInfo().ServiceResponse(data);
        }
        public ActionResult UpdateGroup(String username, String fullname, String channelUrl)
        {
            var data = new DataAccess.SendBird.SendBirdProvider().UpdateGroup(username, fullname, channelUrl);
            return new ResponseInfo().ServiceResponse(data);
        }
        public ActionResult InviteMember(String username, String fullname, List<MemberSendBird> members, String channelUrl)
        {
            var data = new DataAccess.SendBird.SendBirdProvider().InviteMember(username, fullname, members,channelUrl);
            return new ResponseInfo().ServiceResponse(data);
        }
    }
}