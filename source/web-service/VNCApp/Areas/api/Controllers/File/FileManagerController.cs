﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BaseLibrary.Common;
using BaseLibrary.Controllers;
using VNCApp.Models;

namespace VNCApp.Areas.api.Controllers.File
{
    public class FileManagerController : BaseController
    {
        public ActionResult Upload()
        {
            return execute((a, b, c, d) =>
            {
                if (Request.Files == null || Request.Files.Count == 0)
                    throw new Exception("Chọn file để upload");
                if (!Directory.Exists(Server.MapPath("~/Upload/Images/")))
                {
                    Directory.CreateDirectory(Server.MapPath("~/Upload/Images/"));
                }
                List<String> list = new List<string>();
                for(int i=0;i < Request.Files.Count;i++)
                {
                    var item = Request.Files[i];
                    String fileName = DateTime.Now.GetUnixTimeStamp() + item.FileName;
                    item.SaveAs(Server.MapPath("~/Upload/Images/")+fileName);
                    //list.Add("http://onlineapi.site/"+"Upload/Images/"+fileName);
                    list.Add("http://vnc.iduhoc.vn/" + "Upload/Images/" + fileName);
                }
                var temp = new BaseReturnFunction<List<String>>(StatusFunction.TRUE, list);
                return ServiceResponse(temp);
            });
        }
    }
}