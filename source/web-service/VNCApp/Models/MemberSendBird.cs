﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VNCApp.Models
{
    public class MemberSendBird
    {
        public String userId { get; set; }
        public String nickname { get; set; }
        public MemberSendBird(String userId, String nickname)
        {
            this.userId = userId;
            this.nickname = nickname;
        }
        public MemberSendBird()
        {

        }
    }
}