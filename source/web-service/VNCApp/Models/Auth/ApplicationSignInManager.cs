﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using Microsoft.Owin.Security;

namespace VNCApp.Models.Auth
{
    // Configure the application sign-in manager which is used in this application.
    public class ApplicationSignInManager : SignInManager<ApplicationUser, string>
    {
        public ApplicationSignInManager(ApplicationUserManager userManager, IAuthenticationManager authenticationManager)
            : base(userManager, authenticationManager)
        {
        }

        public override Task<ClaimsIdentity> CreateUserIdentityAsync(ApplicationUser user)
        {
            return user.GenerateUserIdentityAsync((ApplicationUserManager)UserManager);
        }

        public static ApplicationSignInManager Create(IdentityFactoryOptions<ApplicationSignInManager> options, IOwinContext context)
        {
            return new ApplicationSignInManager(context.GetUserManager<ApplicationUserManager>(), context.Authentication);
        }

        //public override async Task<SignInStatus> PasswordSignInAsync(string userName, string password,
        //    bool isPersistent, bool shouldLockout)
        //{
        //    SignInStatus signInStatu;
        //    if (this.UserManager != null)
        //    {
        //        ///// changed to use email address instead of username
        //        Task<ApplicationUser> userAwaiter = this.UserManager.FindAsync(userName,password);
        //        return SignInStatus.Success;
        //        //ApplicationUser tUser = await userAwaiter;
        //        //if (tUser != null)
        //        //{
        //        //    Task<bool> cultureAwaiter1 = this.UserManager.IsLockedOutAsync(tUser.Id);
        //        //    if (!await cultureAwaiter1)
        //        //    {
        //        //        Task<bool> cultureAwaiter2 = this.UserManager.CheckPasswordAsync(tUser, password);
        //        //        if (!await cultureAwaiter2)
        //        //        {
        //        //            if (shouldLockout)
        //        //            {
        //        //                Task<IdentityResult> cultureAwaiter3 = this.UserManager.AccessFailedAsync(tUser.Id);
        //        //                await cultureAwaiter3;
        //        //                Task<bool> cultureAwaiter4 = this.UserManager.IsLockedOutAsync(tUser.Id);
        //        //                if (await cultureAwaiter4)
        //        //                {
        //        //                    signInStatu = SignInStatus.LockedOut;
        //        //                    return signInStatu;
        //        //                }
        //        //            }
        //        //            signInStatu = SignInStatus.Failure;
        //        //        }
        //        //        else
        //        //        {
        //        //            Task<IdentityResult> cultureAwaiter5 = this.UserManager.ResetAccessFailedCountAsync(tUser.Id);
        //        //            await cultureAwaiter5;
        //        //            Task<SignInStatus> cultureAwaiter6 = this.SignInOrTwoFactor(tUser, isPersistent);
        //        //            signInStatu = await cultureAwaiter6;
        //        //        }
        //        //    }
        //        //    else
        //        //    {
        //        //        signInStatu = SignInStatus.LockedOut;
        //        //    }
        //        //}
        //        //else
        //        //{
        //        //    signInStatu = SignInStatus.Failure;
        //        //}
        //    }
        //    else
        //    {
        //        signInStatu = SignInStatus.Failure;
        //    }
        //    return signInStatu;
        //}
    }
}