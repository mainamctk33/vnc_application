﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VNCApp.Models
{
    public class ResponseEntity<T> where T : new()
    {
        public int Code { get; set; }
        public String Message { get; set; }
        public T Data { get; set; }

    }
}