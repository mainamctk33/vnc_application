﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VNCApp.Constants
{
    public class Constants
    {
        public static readonly string LOGIN_IS_REQUIRED = "Vui lòng đăng nhập để thực hiện";
        public static readonly string NOT_FOUND_GROUP="Không tìm thấy nhóm nào";
        public static readonly string ACCESS_DENY = "Bạn không có quyền truy cập vào nhóm này";
    }
}