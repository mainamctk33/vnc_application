﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace VNCApp.App_Start
{
    public class MyControllerFactory : DefaultControllerFactory
    {
        public override IController CreateController(RequestContext requestContext, string controllerName)

        {

            var controller = base.CreateController(requestContext, controllerName);

            HttpContext.Current.Items["controllerInstance"] = controller;

            return controller;

        }
    }
}