﻿using System.Web;
using System.Web.Optimization;

namespace VNCApp
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {

            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery/jquery-2.0.2.min.js",
                        "~/Scripts/jquery/jquery-ui.min.js"
                        ));


            bundles.Add(new ScriptBundle("~/bundles/smartadmin").Include(
                        "~/Scripts/smart-admin/app.config.js",
                        "~/Scripts/smart-admin/bootstrap.min.js",
                        "~/Scripts/notification/SmartNotification.min.js",
                        "~/Scripts/smart-admin/jarvis.widget.min.js",
                        "~/Scripts/select2/select2.min.js",
                        "~/Scripts/select2/select2-tab-fix.js",
                        "~/Scripts/smart-admin/demo.min.js",
                        "~/Scripts/smart-admin/app.min.js"
                        ));

            bundles.Add(new ScriptBundle("~/bundles/jqueryvalidate").Include(
                        "~/Scripts/jquery/jquery.validate.min.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
          "~/Content/site.css",
          "~/Content/font-awesome.min.css",
          "~/Content/loading.css",
          "~/Content/style.css",
          "~/Content/header.css"
          ));

            bundles.Add(new StyleBundle("~/Content/smartadmin").Include(
    "~/Content/smart-admin/bootstrap.min.css",
    "~/Content/smart-admin/smartadmin-production.min.css",
    "~/Content/smart-admin/smartadmin-production-plugins.min.css",
    "~/Content/select2/select2.min.css"
    ));
        }
    }
}
