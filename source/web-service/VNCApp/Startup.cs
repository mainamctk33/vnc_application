﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(VNCApp.Startup))]
namespace VNCApp
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
