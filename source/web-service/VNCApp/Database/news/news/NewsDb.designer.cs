﻿using System.Web.Script.Serialization;

#pragma warning disable 1591
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace VNCApp.Database.News.News
{
	using System.Data.Linq;
	using System.Data.Linq.Mapping;
	using System.Data;
	using System.Collections.Generic;
	using System.Reflection;
	using System.Linq;
	using System.Linq.Expressions;
	using System.ComponentModel;
	using System;
	
	
	[global::System.Data.Linq.Mapping.DatabaseAttribute(Name="VNC")]
	public partial class NewsDbDataContext : System.Data.Linq.DataContext
	{
		
		private static System.Data.Linq.Mapping.MappingSource mappingSource = new AttributeMappingSource();
		
    #region Extensibility Method Definitions
    partial void OnCreated();
    partial void InsertTypeNew(TypeNew instance);
    partial void UpdateTypeNew(TypeNew instance);
    partial void DeleteTypeNew(TypeNew instance);
    partial void InsertNew(New instance);
    partial void UpdateNew(New instance);
    partial void DeleteNew(New instance);
    partial void InsertBookmarkNew(BookmarkNew instance);
    partial void UpdateBookmarkNew(BookmarkNew instance);
    partial void DeleteBookmarkNew(BookmarkNew instance);
    partial void InsertNewsTag(NewsTag instance);
    partial void UpdateNewsTag(NewsTag instance);
    partial void DeleteNewsTag(NewsTag instance);
    #endregion
		
		public NewsDbDataContext() : 
				base(global::System.Configuration.ConfigurationManager.ConnectionStrings["VNCConnectionString"].ConnectionString, mappingSource)
		{
			OnCreated();
		}
		
		public NewsDbDataContext(string connection) : 
				base(connection, mappingSource)
		{
			OnCreated();
		}
		
		public NewsDbDataContext(System.Data.IDbConnection connection) : 
				base(connection, mappingSource)
		{
			OnCreated();
		}
		
		public NewsDbDataContext(string connection, System.Data.Linq.Mapping.MappingSource mappingSource) : 
				base(connection, mappingSource)
		{
			OnCreated();
		}
		
		public NewsDbDataContext(System.Data.IDbConnection connection, System.Data.Linq.Mapping.MappingSource mappingSource) : 
				base(connection, mappingSource)
		{
			OnCreated();
		}

	    [ScriptIgnore]
        public System.Data.Linq.Table<TypeNew> TypeNews
		{
			get
			{
				return this.GetTable<TypeNew>();
			}
		}
		
		public System.Data.Linq.Table<New> News
		{
			get
			{
				return this.GetTable<New>();
			}
		}
	    [ScriptIgnore]

        public System.Data.Linq.Table<BookmarkNew> BookmarkNews
		{
			get
			{
				return this.GetTable<BookmarkNew>();
			}
		}
	    [ScriptIgnore]

        public System.Data.Linq.Table<NewsTag> NewsTags
		{
			get
			{
				return this.GetTable<NewsTag>();
			}
		}
	}
	
	[global::System.Data.Linq.Mapping.TableAttribute(Name="dbo.TypeNews")]
	public partial class TypeNew : INotifyPropertyChanging, INotifyPropertyChanged
	{
		
		private static PropertyChangingEventArgs emptyChangingEventArgs = new PropertyChangingEventArgs(String.Empty);
		
		private int _ID;
		
		private string _Name;
		
		private EntitySet<New> _News;
		
    #region Extensibility Method Definitions
    partial void OnLoaded();
    partial void OnValidate(System.Data.Linq.ChangeAction action);
    partial void OnCreated();
    partial void OnIDChanging(int value);
    partial void OnIDChanged();
    partial void OnNameChanging(string value);
    partial void OnNameChanged();
    #endregion
		
		public TypeNew()
		{
			this._News = new EntitySet<New>(new Action<New>(this.attach_News), new Action<New>(this.detach_News));
			OnCreated();
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_ID", AutoSync=AutoSync.OnInsert, DbType="Int NOT NULL IDENTITY", IsPrimaryKey=true, IsDbGenerated=true)]
		public int ID
		{
			get
			{
				return this._ID;
			}
			set
			{
				if ((this._ID != value))
				{
					this.OnIDChanging(value);
					this.SendPropertyChanging();
					this._ID = value;
					this.SendPropertyChanged("ID");
					this.OnIDChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_Name", DbType="NVarChar(50) NOT NULL", CanBeNull=false)]
		public string Name
		{
			get
			{
				return this._Name;
			}
			set
			{
				if ((this._Name != value))
				{
					this.OnNameChanging(value);
					this.SendPropertyChanging();
					this._Name = value;
					this.SendPropertyChanged("Name");
					this.OnNameChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.AssociationAttribute(Name="TypeNew_New", Storage="_News", ThisKey="ID", OtherKey="Type")]
		public EntitySet<New> News
		{
			get
			{
				return this._News;
			}
			set
			{
				this._News.Assign(value);
			}
		}
		
		public event PropertyChangingEventHandler PropertyChanging;
		
		public event PropertyChangedEventHandler PropertyChanged;
		
		protected virtual void SendPropertyChanging()
		{
			if ((this.PropertyChanging != null))
			{
				this.PropertyChanging(this, emptyChangingEventArgs);
			}
		}
		
		protected virtual void SendPropertyChanged(String propertyName)
		{
			if ((this.PropertyChanged != null))
			{
				this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
			}
		}
		
		private void attach_News(New entity)
		{
			this.SendPropertyChanging();
			entity.TypeNew = this;
		}
		
		private void detach_News(New entity)
		{
			this.SendPropertyChanging();
			entity.TypeNew = null;
		}
	}
	
	[global::System.Data.Linq.Mapping.TableAttribute(Name="dbo.News")]
	public partial class New : INotifyPropertyChanging, INotifyPropertyChanged
	{
		
		private static PropertyChangingEventArgs emptyChangingEventArgs = new PropertyChangingEventArgs(String.Empty);
		
		private int _ID;
		
		private int _Type;
		
		private string _Title;
		
		private string _Content;
		
		private string _Thumbnail;
		
		private string _CreatedBy;
		
		private long _CreatedDate;
		
		private string _SubContent;
		
		private long _UpdatedDate;
		
		private EntitySet<BookmarkNew> _BookmarkNews;
		
		private EntitySet<NewsTag> _NewsTags;
		
		private EntityRef<TypeNew> _TypeNew;
		
    #region Extensibility Method Definitions
    partial void OnLoaded();
    partial void OnValidate(System.Data.Linq.ChangeAction action);
    partial void OnCreated();
    partial void OnIDChanging(int value);
    partial void OnIDChanged();
    partial void OnTypeChanging(int value);
    partial void OnTypeChanged();
    partial void OnTitleChanging(string value);
    partial void OnTitleChanged();
    partial void OnContentChanging(string value);
    partial void OnContentChanged();
    partial void OnThumbnailChanging(string value);
    partial void OnThumbnailChanged();
    partial void OnCreatedByChanging(string value);
    partial void OnCreatedByChanged();
    partial void OnCreatedDateChanging(long value);
    partial void OnCreatedDateChanged();
    partial void OnSubContentChanging(string value);
    partial void OnSubContentChanged();
    partial void OnUpdatedDateChanging(long value);
    partial void OnUpdatedDateChanged();
    #endregion
		
		public New()
		{
			this._BookmarkNews = new EntitySet<BookmarkNew>(new Action<BookmarkNew>(this.attach_BookmarkNews), new Action<BookmarkNew>(this.detach_BookmarkNews));
			this._NewsTags = new EntitySet<NewsTag>(new Action<NewsTag>(this.attach_NewsTags), new Action<NewsTag>(this.detach_NewsTags));
			this._TypeNew = default(EntityRef<TypeNew>);
			OnCreated();
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_ID", AutoSync=AutoSync.OnInsert, DbType="Int NOT NULL IDENTITY", IsPrimaryKey=true, IsDbGenerated=true)]
		public int ID
		{
			get
			{
				return this._ID;
			}
			set
			{
				if ((this._ID != value))
				{
					this.OnIDChanging(value);
					this.SendPropertyChanging();
					this._ID = value;
					this.SendPropertyChanged("ID");
					this.OnIDChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_Type", DbType="Int NOT NULL")]
		public int Type
		{
			get
			{
				return this._Type;
			}
			set
			{
				if ((this._Type != value))
				{
					if (this._TypeNew.HasLoadedOrAssignedValue)
					{
						throw new System.Data.Linq.ForeignKeyReferenceAlreadyHasValueException();
					}
					this.OnTypeChanging(value);
					this.SendPropertyChanging();
					this._Type = value;
					this.SendPropertyChanged("Type");
					this.OnTypeChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_Title", DbType="NVarChar(255) NOT NULL", CanBeNull=false)]
		public string Title
		{
			get
			{
				return this._Title;
			}
			set
			{
				if ((this._Title != value))
				{
					this.OnTitleChanging(value);
					this.SendPropertyChanging();
					this._Title = value;
					this.SendPropertyChanged("Title");
					this.OnTitleChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_Content", DbType="NVarChar(MAX) NOT NULL", CanBeNull=false)]
		public string Content
		{
			get
			{
				return this._Content;
			}
			set
			{
				if ((this._Content != value))
				{
					this.OnContentChanging(value);
					this.SendPropertyChanging();
					this._Content = value;
					this.SendPropertyChanged("Content");
					this.OnContentChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_Thumbnail", DbType="NVarChar(255)")]
		public string Thumbnail
		{
			get
			{
				return this._Thumbnail;
			}
			set
			{
				if ((this._Thumbnail != value))
				{
					this.OnThumbnailChanging(value);
					this.SendPropertyChanging();
					this._Thumbnail = value;
					this.SendPropertyChanged("Thumbnail");
					this.OnThumbnailChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_CreatedBy", DbType="NVarChar(50) NOT NULL", CanBeNull=false)]
		public string CreatedBy
		{
			get
			{
				return this._CreatedBy;
			}
			set
			{
				if ((this._CreatedBy != value))
				{
					this.OnCreatedByChanging(value);
					this.SendPropertyChanging();
					this._CreatedBy = value;
					this.SendPropertyChanged("CreatedBy");
					this.OnCreatedByChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_CreatedDate", DbType="BigInt NOT NULL")]
		public long CreatedDate
		{
			get
			{
				return this._CreatedDate;
			}
			set
			{
				if ((this._CreatedDate != value))
				{
					this.OnCreatedDateChanging(value);
					this.SendPropertyChanging();
					this._CreatedDate = value;
					this.SendPropertyChanged("CreatedDate");
					this.OnCreatedDateChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_SubContent", DbType="NVarChar(500) NOT NULL", CanBeNull=false)]
		public string SubContent
		{
			get
			{
				return this._SubContent;
			}
			set
			{
				if ((this._SubContent != value))
				{
					this.OnSubContentChanging(value);
					this.SendPropertyChanging();
					this._SubContent = value;
					this.SendPropertyChanged("SubContent");
					this.OnSubContentChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_UpdatedDate", DbType="BigInt NOT NULL")]
		public long UpdatedDate
		{
			get
			{
				return this._UpdatedDate;
			}
			set
			{
				if ((this._UpdatedDate != value))
				{
					this.OnUpdatedDateChanging(value);
					this.SendPropertyChanging();
					this._UpdatedDate = value;
					this.SendPropertyChanged("UpdatedDate");
					this.OnUpdatedDateChanged();
				}
			}
		}
		
        [ScriptIgnore]
		[global::System.Data.Linq.Mapping.AssociationAttribute(Name="New_BookmarkNew", Storage="_BookmarkNews", ThisKey="ID", OtherKey="ID")]
		public EntitySet<BookmarkNew> BookmarkNews
		{
			get
			{
				return this._BookmarkNews;
			}
			set
			{
				this._BookmarkNews.Assign(value);
			}
		}

	    [ScriptIgnore]
        [global::System.Data.Linq.Mapping.AssociationAttribute(Name="New_NewsTag", Storage="_NewsTags", ThisKey="ID", OtherKey="ID")]
		public EntitySet<NewsTag> NewsTags
		{
			get
			{
				return this._NewsTags;
			}
			set
			{
				this._NewsTags.Assign(value);
			}
		}
	    [ScriptIgnore]

        [global::System.Data.Linq.Mapping.AssociationAttribute(Name="TypeNew_New", Storage="_TypeNew", ThisKey="Type", OtherKey="ID", IsForeignKey=true, DeleteOnNull=true, DeleteRule="CASCADE")]
		public TypeNew TypeNew
		{
			get
			{
				return this._TypeNew.Entity;
			}
			set
			{
				TypeNew previousValue = this._TypeNew.Entity;
				if (((previousValue != value) 
							|| (this._TypeNew.HasLoadedOrAssignedValue == false)))
				{
					this.SendPropertyChanging();
					if ((previousValue != null))
					{
						this._TypeNew.Entity = null;
						previousValue.News.Remove(this);
					}
					this._TypeNew.Entity = value;
					if ((value != null))
					{
						value.News.Add(this);
						this._Type = value.ID;
					}
					else
					{
						this._Type = default(int);
					}
					this.SendPropertyChanged("TypeNew");
				}
			}
		}
		
		public event PropertyChangingEventHandler PropertyChanging;
		
		public event PropertyChangedEventHandler PropertyChanged;
		
		protected virtual void SendPropertyChanging()
		{
			if ((this.PropertyChanging != null))
			{
				this.PropertyChanging(this, emptyChangingEventArgs);
			}
		}
		
		protected virtual void SendPropertyChanged(String propertyName)
		{
			if ((this.PropertyChanged != null))
			{
				this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
			}
		}
		
		private void attach_BookmarkNews(BookmarkNew entity)
		{
			this.SendPropertyChanging();
			entity.New = this;
		}
		
		private void detach_BookmarkNews(BookmarkNew entity)
		{
			this.SendPropertyChanging();
			entity.New = null;
		}
		
		private void attach_NewsTags(NewsTag entity)
		{
			this.SendPropertyChanging();
			entity.New = this;
		}
		
		private void detach_NewsTags(NewsTag entity)
		{
			this.SendPropertyChanging();
			entity.New = null;
		}
	}
	
	[global::System.Data.Linq.Mapping.TableAttribute(Name="dbo.BookmarkNews")]
	public partial class BookmarkNew : INotifyPropertyChanging, INotifyPropertyChanged
	{
		
		private static PropertyChangingEventArgs emptyChangingEventArgs = new PropertyChangingEventArgs(String.Empty);
		
		private int _ID;
		
		private string _BookmarkBy;
		
		private long _BookmarkDate;
		
		private EntityRef<New> _New;
		
    #region Extensibility Method Definitions
    partial void OnLoaded();
    partial void OnValidate(System.Data.Linq.ChangeAction action);
    partial void OnCreated();
    partial void OnIDChanging(int value);
    partial void OnIDChanged();
    partial void OnBookmarkByChanging(string value);
    partial void OnBookmarkByChanged();
    partial void OnBookmarkDateChanging(long value);
    partial void OnBookmarkDateChanged();
    #endregion
		
		public BookmarkNew()
		{
			this._New = default(EntityRef<New>);
			OnCreated();
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_ID", DbType="Int NOT NULL", IsPrimaryKey=true)]
		public int ID
		{
			get
			{
				return this._ID;
			}
			set
			{
				if ((this._ID != value))
				{
					if (this._New.HasLoadedOrAssignedValue)
					{
						throw new System.Data.Linq.ForeignKeyReferenceAlreadyHasValueException();
					}
					this.OnIDChanging(value);
					this.SendPropertyChanging();
					this._ID = value;
					this.SendPropertyChanged("ID");
					this.OnIDChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_BookmarkBy", DbType="NVarChar(50) NOT NULL", CanBeNull=false, IsPrimaryKey=true)]
		public string BookmarkBy
		{
			get
			{
				return this._BookmarkBy;
			}
			set
			{
				if ((this._BookmarkBy != value))
				{
					this.OnBookmarkByChanging(value);
					this.SendPropertyChanging();
					this._BookmarkBy = value;
					this.SendPropertyChanged("BookmarkBy");
					this.OnBookmarkByChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_BookmarkDate", DbType="BigInt NOT NULL")]
		public long BookmarkDate
		{
			get
			{
				return this._BookmarkDate;
			}
			set
			{
				if ((this._BookmarkDate != value))
				{
					this.OnBookmarkDateChanging(value);
					this.SendPropertyChanging();
					this._BookmarkDate = value;
					this.SendPropertyChanged("BookmarkDate");
					this.OnBookmarkDateChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.AssociationAttribute(Name="New_BookmarkNew", Storage="_New", ThisKey="ID", OtherKey="ID", IsForeignKey=true, DeleteOnNull=true, DeleteRule="CASCADE")]
		public New New
		{
			get
			{
				return this._New.Entity;
			}
			set
			{
				New previousValue = this._New.Entity;
				if (((previousValue != value) 
							|| (this._New.HasLoadedOrAssignedValue == false)))
				{
					this.SendPropertyChanging();
					if ((previousValue != null))
					{
						this._New.Entity = null;
						previousValue.BookmarkNews.Remove(this);
					}
					this._New.Entity = value;
					if ((value != null))
					{
						value.BookmarkNews.Add(this);
						this._ID = value.ID;
					}
					else
					{
						this._ID = default(int);
					}
					this.SendPropertyChanged("New");
				}
			}
		}
		
		public event PropertyChangingEventHandler PropertyChanging;
		
		public event PropertyChangedEventHandler PropertyChanged;
		
		protected virtual void SendPropertyChanging()
		{
			if ((this.PropertyChanging != null))
			{
				this.PropertyChanging(this, emptyChangingEventArgs);
			}
		}
		
		protected virtual void SendPropertyChanged(String propertyName)
		{
			if ((this.PropertyChanged != null))
			{
				this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
			}
		}
	}
	
	[global::System.Data.Linq.Mapping.TableAttribute(Name="dbo.NewsTag")]
	public partial class NewsTag : INotifyPropertyChanging, INotifyPropertyChanged
	{
		
		private static PropertyChangingEventArgs emptyChangingEventArgs = new PropertyChangingEventArgs(String.Empty);
		
		private int _ID;
		
		private string _TagName;
		
		private EntityRef<New> _New;
		
    #region Extensibility Method Definitions
    partial void OnLoaded();
    partial void OnValidate(System.Data.Linq.ChangeAction action);
    partial void OnCreated();
    partial void OnIDChanging(int value);
    partial void OnIDChanged();
    partial void OnTagNameChanging(string value);
    partial void OnTagNameChanged();
    #endregion
		
		public NewsTag()
		{
			this._New = default(EntityRef<New>);
			OnCreated();
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_ID", DbType="Int NOT NULL", IsPrimaryKey=true)]
		public int ID
		{
			get
			{
				return this._ID;
			}
			set
			{
				if ((this._ID != value))
				{
					if (this._New.HasLoadedOrAssignedValue)
					{
						throw new System.Data.Linq.ForeignKeyReferenceAlreadyHasValueException();
					}
					this.OnIDChanging(value);
					this.SendPropertyChanging();
					this._ID = value;
					this.SendPropertyChanged("ID");
					this.OnIDChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_TagName", DbType="NVarChar(50) NOT NULL", CanBeNull=false, IsPrimaryKey=true)]
		public string TagName
		{
			get
			{
				return this._TagName;
			}
			set
			{
				if ((this._TagName != value))
				{
					this.OnTagNameChanging(value);
					this.SendPropertyChanging();
					this._TagName = value;
					this.SendPropertyChanged("TagName");
					this.OnTagNameChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.AssociationAttribute(Name="New_NewsTag", Storage="_New", ThisKey="ID", OtherKey="ID", IsForeignKey=true, DeleteOnNull=true, DeleteRule="CASCADE")]
		public New New
		{
			get
			{
				return this._New.Entity;
			}
			set
			{
				New previousValue = this._New.Entity;
				if (((previousValue != value) 
							|| (this._New.HasLoadedOrAssignedValue == false)))
				{
					this.SendPropertyChanging();
					if ((previousValue != null))
					{
						this._New.Entity = null;
						previousValue.NewsTags.Remove(this);
					}
					this._New.Entity = value;
					if ((value != null))
					{
						value.NewsTags.Add(this);
						this._ID = value.ID;
					}
					else
					{
						this._ID = default(int);
					}
					this.SendPropertyChanged("New");
				}
			}
		}
		
		public event PropertyChangingEventHandler PropertyChanging;
		
		public event PropertyChangedEventHandler PropertyChanged;
		
		protected virtual void SendPropertyChanging()
		{
			if ((this.PropertyChanging != null))
			{
				this.PropertyChanging(this, emptyChangingEventArgs);
			}
		}
		
		protected virtual void SendPropertyChanged(String propertyName)
		{
			if ((this.PropertyChanged != null))
			{
				this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
			}
		}
	}
}
#pragma warning restore 1591
