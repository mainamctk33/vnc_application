﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BaseLibrary.Controllers;
using VNCApp.DataAccess.News;

namespace VNCApp.Controllers
{
    public class WebNewsController : BaseController
    {
        public ActionResult Detail(int id = 0)
        {
            NewsProvider newsProvider = new NewsProvider();
            var news = newsProvider.GetById(id);
            if (news.IsTrue)
                return View(news.Data);
            return View();
        }

        public ActionResult AppLink(String id = "")
        {
            return View();
        }
    }
}