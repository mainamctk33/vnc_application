﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BaseLibrary.Controllers;
using BaseLibrary.Models.Entity;
using VNCApp.Database.User;
using BaseLibrary.Helper;
using VNCApp.Models;

namespace VNCApp.Controllers
{
    public class HomeController : BaseController
    {
        // GET: Default
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult ForgotPassword(String id)
        {
            using (var context = new UserDbDataContext())
            {
                var user = context.Users.FirstOrDefault(x => x.ResetPasswordToken == id);
                if (user == null || user.ResetPassExpired != null && user.ResetPassExpired.Value < DateTime.Now)
                {
                    return View("TokenExprired");
                }
                return View("ChangePassword", user);
            }

        }
        public ActionResult Activate(String id)
        {
            using (var context = new UserDbDataContext())
            {
                var user = context.Users.FirstOrDefault(x => x.ActivateToken  == id);
                if (user == null)
                {
                    return View("TokenExprired");
                }
                user.IsActived = 1;
                context.SubmitChanges();
                return View("ActivateAccountSuccess", user);
            }

        }
    }
}