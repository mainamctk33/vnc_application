﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BaseLibrary.Controllers;
using System.Web.Mvc;
using VNCApp.DataAccess.Tips;

namespace VNCApp.Controllers
{
    public class WebTipsController : BaseController
    {
        public ActionResult Detail(int id = 0)
        {
            TipsProvider tipsProvider = new TipsProvider();
            var tips = tipsProvider.GetById(id);
            if (tips.IsTrue)
                return View(tips.Data);
            return View();
        }

        public ActionResult AppLink(String id = "")
        {
            return View();
        }
    }
}