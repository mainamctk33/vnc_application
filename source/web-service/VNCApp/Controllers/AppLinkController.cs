﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using VNCApp.DataAccess.Jobs;
using VNCApp.DataAccess.News;
using VNCApp.DataAccess.Tips;

namespace VNCApp.Controllers
{
    public class AppLinkController : Controller
    {
        // GET: AppLink
        public ActionResult Index(String id)
        {
            return View();
        }
        public ActionResult News(int id = 0)
        {
            NewsProvider newsProvider = new NewsProvider();
            var result = newsProvider.GetById(id);
            if (result.IsTrue)
            {
                var news = result.Data;
                return View(news);
            }
            return View();
        }
        public ActionResult Job(int id = 0)
        {
            JobProvider newsProvider = new JobProvider();
            var result = newsProvider.GetById(id);
            if (result.IsTrue)
            {
                var news = result.Data;
                return View(news);
            }
            return View();
        }
        public ActionResult Tips(int id = 0)
        {
            TipsProvider newsProvider = new TipsProvider();
            var result = newsProvider.GetById(id);
            if (result.IsTrue)
            {
                var news = result.Data;
                return View(news);
            }
            return View();
        }

    }
}