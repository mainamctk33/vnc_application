﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BaseLibrary.Controllers;
using VNCApp.DataAccess.Jobs;

namespace VNCApp.Controllers
{
    public class WebJobController : BaseController
    {
        public ActionResult Detail(int id = 0)
        {
            JobProvider jobProvider = new JobProvider();
            var job = jobProvider.GetById(id);
            if (job.IsTrue)
                return View(job.Data);
            return View();
        }

        public ActionResult AppLink(String id = "")
        {
            return View();
        }
    }
}