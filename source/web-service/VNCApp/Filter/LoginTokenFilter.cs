﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http.Filters;
using System.Web;
using System.Web.Http.Controllers;
using BaseLibrary.Helper;
using BaseLibrary.Models.Entity;
using BaseLibrary.Utils;

namespace VNCApp.Filter
{
    public class LoginTokenFilter : AuthorizationFilterAttribute
    {
        public override void OnAuthorization(HttpActionContext filterContext)
        {
            try
            {
                if (!filterContext.Request.Headers.Contains("Auth"))
                {
                    throw new Exception("Header does not contains 'Auth'");
                }
                string auth = filterContext.Request.Headers.GetValues("Auth").FirstOrDefault();
                if (!string.IsNullOrWhiteSpace(auth))
                {
                    if (!GetAuthenCode(auth))
                    {
                        ChangeGoneRequest(filterContext);
                        return;
                    }
                }
            }
            catch
            {
                ChangeGoneRequest(filterContext);
                return;
            }
            base.OnAuthorization(filterContext);
        }


        public static bool GetAuthenCode(string jsonData)
        {
            var auth = ConvertUtils.toObject<dynamic>(jsonData);
            if (auth.LoginToken != null && !string.IsNullOrWhiteSpace(auth.LoginToken.ToString()))
            {
                string loginTokenId = auth.LoginToken.ToString();
                dynamic validateResult = TokenHelper.ValidateToken(loginTokenId);
                if (validateResult.Code == (int)TokenHelper.ValidateTokenResult.Expirated || validateResult.Code == (int)TokenHelper.ValidateTokenResult.Invalid)
                {
                    return false;
                }
                else if (validateResult.Code == (int)TokenHelper.ValidateTokenResult.Success)
                {
                    var payload = ConvertUtils.toObject<dynamic>(validateResult.Data);
                    string userId = payload.userId.ToString();
                    return !String.IsNullOrWhiteSpace(userId);
                }
            }
            return false;
        }

        private static void ChangeGoneRequest(HttpActionContext filterContext)
        {
            filterContext.Response = filterContext.Request.CreateResponse(HttpStatusCode.NotAcceptable);
        }
    }
}