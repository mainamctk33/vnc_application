﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Security.Claims;
using System.Web;
using System.Web.Mvc;
using BaseLibrary.Common;
using BaseLibrary.DataAccess;
using BaseLibrary.Models.Entity;
using BaseLibrary.Utils;
using Microsoft.AspNet.Identity;
using Microsoft.Owin.Security;

namespace BaseLibrary.Controllers
{
    public class BaseController : Controller
    {
        public JsonResult ServiceResponse(Object data)
        {
            return new ResponseInfo().ServiceResponse(data);
        }
        public bool IsOk(dynamic result)
        {
            try
            {
                return result.StatusCode == HttpStatusCode.OK;
            }
            catch (Exception)
            {
                return false;
            }
        }


        public JsonResult Ok(int statusCode)
        {
            return Ok("", statusCode);
        }
        public JsonResult Ok(Object data)
        {
            return Ok(data, 200);
        }
        public JsonResult Ok(Object obj, int statusCode)
        {
            return Ok(obj, statusCode, new ResponseParam(ConstantsBase.Response.STATUS_CODE, statusCode));
        }
        public JsonResult Ok(Object obj, int statusCode, params ResponseParam[] param)
        {
            var dic = new Dictionary<String, Object>();
            dic.Add(ConstantsBase.Response.STATUS_CODE, statusCode);
            if (param != null && param.Length > 0)
            {
                foreach (var item in param)
                {
                    if (item.Name == ConstantsBase.Response.DATA)
                        obj = item.Value;
                    else
                    {
                        if (dic.ContainsKey(item.Name))
                            dic[item.Name] = item.Value;
                        else
                        {
                            dic.Add(item.Name, item.Value);
                        }
                    }
                }
                return new ResponseInfo().ResponseOK(obj, dic);
            }
            return Ok(obj, statusCode);
        }
        public JsonResult Ok(Object obj, int statusCode, Dictionary<String, Object> dic)
        {
            try
            {
                return Ok(obj, statusCode, dic.Select(x => new ResponseParam(x.Key, x.Value)).ToArray());

            }
            catch (Exception e)
            {
                return Ok(obj, statusCode);
            }
        }

        public JsonResult NotOk(int statusCode)
        {
            return NotOk("", statusCode);
        }
        public JsonResult NotOk(Object obj, int statusCode)
        {
            return NotOk(obj, statusCode, new ResponseParam(ConstantsBase.Response.STATUS_CODE, statusCode));
        }
        public JsonResult NotOk(Object obj, int statusCode, params ResponseParam[] param)
        {
            var dic = new Dictionary<String, Object>();
            dic.Add(ConstantsBase.Response.STATUS_CODE, statusCode);
            if (param != null && param.Length > 0)
            {
                foreach (var item in param)
                {
                    if (item.Name == ConstantsBase.Response.DATA)
                        obj = item.Value;
                    else
                    {
                        if (dic.ContainsKey(item.Name))
                            dic[item.Name] = item.Value;
                        else
                        {
                            dic.Add(item.Name, item.Value);
                        }
                    }
                }
                return new ResponseInfo().ResponseNotOK(obj, dic);
            }
            return new ResponseInfo().ResponseNotOK(obj, new Dictionary<string, object>());
        }
        public JsonResult NotOk(Object obj, int statusCode, Dictionary<String, Object> dic)
        {
            return NotOk(obj, statusCode, dic.Select(x => new ResponseParam(x.Key, x.Value)).ToArray());
        }

        public JsonResult NotOKException(Object obj, Exception e)
        {
            return NotOk(obj, ConstantsBase.StatusCode.EXEPTION, new ResponseParam(ConstantsBase.Response.EXCEPTION, e.Message));
        }



        public JsonResult RecheckParame()
        {
            return NotOk(ConstantsBase.Error.RECHECK_PARAME, ConstantsBase.StatusCode.RE_CHECK_PARAME);
        }
        public JsonResult RequireLogin()
        {
            return NotOk(ConstantsBase.Error.PLEASE_LOGIN_TO_PROCESS, ConstantsBase.StatusCode.NO_LOGIN);
        }
        public JsonResult RequireAdmin()
        {
            return NotOk(ConstantsBase.Error.DO_NOT_HAVE_PERMISSION, ConstantsBase.StatusCode.REQUIRE_PERMISSION);
        }
        public JsonResult RequirePermission()
        {
            return NotOk(ConstantsBase.Error.DO_NOT_HAVE_PERMISSION, ConstantsBase.StatusCode.REQUIRE_PERMISSION);
        }


        public T UserLogin<T>()
        {

            try
            {
                return ConvertUtils.toObject<T>(User.Identity.Name);
            }
            catch (Exception e)
            {
                return default(T);
            }

        }
        

        public AccountEntity CurrentUser
        {
            get { return UserLogin<AccountEntity>(); }
            set
            {
                var claims = new List<Claim>();
                try
                {
                    var ctx = Request.GetOwinContext();
                    var authenticationManager = ctx.Authentication;
                    if (value == null)
                    {
                        // Sign Out.    
                        authenticationManager.SignOut();
                    }
                    else
                    {
                        // Setting    
                        claims.Add(new Claim(ClaimTypes.Role, value.getRoleId()));
                        claims.Add(new Claim(ClaimTypes.Name, value.Serialize()));
                        var claimIdenties = new ClaimsIdentity(claims, DefaultAuthenticationTypes.ApplicationCookie);
                        // Sign In.    
                        authenticationManager.SignIn(new AuthenticationProperties() { IsPersistent = false },
                            claimIdenties);
                    }
                }
                catch (Exception ex)
                {
                    // Info    
                    throw ex;
                }
            }
        }

        public String CurrentUserId
        {
            get
            {
                try
                {
                    return CurrentUser?.Id;
                }
                catch (Exception e)
                {
                    return null;
                }
            }
        }

        public String CurrentLoginToken
        {
            get
            {
                try
                {
                    return CurrentUser?.LoginToken;
                }
                catch (Exception e)
                {
                    return null;
                }
            }
        }

        public AuthEntity AuthEntity
        {
            get
            {
                var currentUser = CurrentUser;
                return new AuthEntity(CurrentLoginToken, currentUser?.Id);
            }   
        }
        public bool IsLogin => CurrentUser != null;


        public String RenderPartialToString(String view)
        {
            return ControllerContext.RenderPartialToString(view);
        }

        public string RenderPartialToString<T>(string partialViewName, T model)
            where T : new()
        {
            return ControllerContext.RenderPartialToString(partialViewName, model);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="authEntity">Authen Object</param>
        /// <param name="currentAccount">Current account login</param>
        /// <param name="currentAccountId">Current id of account login</param>
        /// <param name="parameter">all parame of method</param>
        /// <returns></returns>
        public delegate ActionResult delExecute(AuthEntity authEntity, AccountEntity currentAccount, String currentAccountId, params ParameterInfo[] parameter);

        public virtual ActionResult execute(delExecute executeMethod, params ParameterInfo[] parameter)
        {
            try
            {
                var result = executeMethod(AuthEntity, CurrentUser, CurrentUserId, parameter);
                return result;
            }
            catch (Exception e)
            {
                return NotOKException(ConstantsBase.Error.ERROR_OCCUR_TRY_AGAIN, e);
            }
        }
    }

    public class ResponseParam
    {
        public String Name { get; set; }
        public object Value { get; set; }

        public ResponseParam()
        { }

        public ResponseParam(String Name, Object Value)
        {
            this.Name = Name;
            this.Value = Value;
        }
    }
}