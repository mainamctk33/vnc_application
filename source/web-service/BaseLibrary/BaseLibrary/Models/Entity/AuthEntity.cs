﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BaseLibrary.Common;

namespace BaseLibrary.Models.Entity
{
    public class AuthEntity
    {
        public string LoginToken { get; set; }
        public String UserId;
        public string DeviceType = "web";

        public AuthEntity() { }
        public AuthEntity(String loginToken, String userId)
        {
            this.LoginToken = loginToken;
            this.UserId = userId;
        }
    }
}