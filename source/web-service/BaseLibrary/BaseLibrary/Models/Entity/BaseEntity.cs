﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BaseLibrary.Models.Entity
{
    public class BaseEntity<T>
    {
        public T Data { get; set; }
        public int Total { get; set; }
        public int Code { get; set; }
    }
}