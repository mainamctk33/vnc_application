﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BaseLibrary.Models.Entity
{
    public abstract class AccountEntity
    {
        public String Id { get; set; }
        public String FullName { get; set; }
        public  String RoleId { get; set; }

        public String LoginToken { get; set; }

        public abstract String getRoleId();
    }
    public class AccountEntityInstance:AccountEntity
    {
        public override string getRoleId()
        {
            return "";
        }
    }
}