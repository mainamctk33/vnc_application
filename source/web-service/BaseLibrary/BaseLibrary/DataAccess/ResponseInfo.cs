﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace BaseLibrary.DataAccess
{
    public class ResponseInfo
    {
        public JsonResult ServiceResponse(Object data)
        {
            return new JsonResult()
            {
                Data = data,
                JsonRequestBehavior = JsonRequestBehavior.AllowGet,
                ContentType = "application/json",
                ContentEncoding = Encoding.UTF8,
                MaxJsonLength = int.MaxValue,
               
            };
        }


        public JsonResult ResponseNotOK(object message, Dictionary<String, Object> dic)
        {
            dic.Add("success", false);
            dic.Add("message", message);
            return new JsonResult()
            {
                Data = dic,
                JsonRequestBehavior = JsonRequestBehavior.AllowGet,
                ContentType = "application/json",
                ContentEncoding = Encoding.UTF8,
                MaxJsonLength = int.MaxValue
            };
        }
        public  JsonResult ResponseOK(object data, Dictionary<String, Object> dic)
        {
            dic.Add("success", true);
            dic.Add("data", data);
            return new JsonResult()
            {
                Data = dic,
                JsonRequestBehavior = JsonRequestBehavior.AllowGet,
                ContentType = "application/json",
                ContentEncoding = Encoding.UTF8,
                MaxJsonLength = int.MaxValue
            };
        }

    }
}