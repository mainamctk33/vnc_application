﻿using System;
using System.Collections.Generic;
using BaseLibrary.Common;

namespace BaseLibrary.Utils
{
    public class StringUtils
    {
        public static String SubString(String content, int countCharactor)

        {
            string[] list = content.Split(' ');
            List<string> listString = new List<string>();
            foreach (var item in list)
            {
                listString.Add(item);
                if (listString.Count == countCharactor)
                {
                    break;
                }
            }
            return StringHelper.ToString(listString, " ");

        }

        internal static string SubString(string content, int countCharactor, char splitBy, string readMore)
        {
            string[] list = content.Split(splitBy);
            String result = "";
            foreach (var item in list)
            {
                result += item;
                if (result.Length >= countCharactor)
                    return result + readMore;
                result += splitBy;
            }
            return result;
        }
    }
}