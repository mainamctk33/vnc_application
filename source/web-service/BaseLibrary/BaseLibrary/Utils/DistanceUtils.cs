﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Device.Location;


namespace BaseLibrary.Utils
{
    public class DistanceUtils
    {
        public static double Rad(double x)
        {
            return x * Math.PI / 180;
        }


        public static double getDistance(double lat1, double lon1, double lat2, double lon2)
        {
            GeoCoordinate location = new GeoCoordinate(lat1, lon1);
            return location.GetDistanceTo(new GeoCoordinate(lat2, lon2));


            //var R = 6378137; // Earth’s mean radius in meter
            //var dLat = Rad(lat2 - lat1);
            //var dLong = Rad(lon2 - lon1);
            //var a = Math.Sin(dLat / 2) * Math.Sin(dLat / 2) +
            //  Math.Cos(Rad(lat1)) * Math.Cos(Rad(lat2)) *
            //  Math.Sin(dLong / 2) * Math.Sin(dLong / 2);
            //var c = 2 * Math.Atan2(Math.Sqrt(a), Math.Sqrt(1 - a));
            //var d = R * c;
            //return d; // returns the distance in meter
        }
    }
}
