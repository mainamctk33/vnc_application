﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using BaseLibrary.Models.Entity;
using BaseLibrary.Common;
using RestSharp;

namespace BaseLibrary.Common
{
    public class ClientUtils
    {
        public class DataResponse<T> where T : new()
        {
            public int StatusCode { get; set; }
            public HttpStatusCode StatusCodeEnum { get; set; }
            public ResponseResultModels<T> Data;

            public DataResponse(HttpStatusCode statusCode, ResponseResultModels<T> data)
            {
                this.StatusCodeEnum = statusCode;
                this.StatusCode = (int)statusCode;
                this.Data = data;
            }

            public bool IsOk
            {
                get { return StatusCodeEnum == HttpStatusCode.OK; }
            }
        }

        public static DataResponse<T> GetData<T>(string host, string controller, AuthEntity authEntity) where T : new()
        {
            return GetData<T>(host, controller, new Dictionary<string, string>(), authEntity);
        }

        public static DataResponse<T> GetData<T>(string host, string controller, Dictionary<String, String> header, AuthEntity authEntity) where T : new()
        {
            return GetData<T>(host, controller, null, header, authEntity);
        }

        public static DataResponse<T> GetData<T>(string host, string controller, Dictionary<String, String> queryString, Dictionary<String, String> header, AuthEntity authEntity) where T : new()
        {
            if (header.ContainsKey("Auth"))
                header["Auth"] = authEntity.Serialize();
            else
                header.Add("Auth", authEntity.Serialize());
            return GetData<T>(host, controller, queryString, header);
        }


        public static DataResponse<T> GetData<T>(string host, string controller, Dictionary<String, String> queryString, Dictionary<String, String> header) where T : new()
        {
            if (queryString != null && queryString.Keys.Count > 0)
            {
                var temp = string.Join("&", queryString.Select(x => x.Key + " = " + x.Value).ToArray());
                if (controller.Contains("?"))
                {
                    if (!controller.Contains("&"))
                        controller += temp;
                    else
                    {
                        controller += "&" + temp;
                    }
                }
                else
                {
                    controller += "?" + temp;
                }
            }
            return GetData<T>(host, controller, header);
        }

        public static DataResponse<T> CreateRequest<T>(RestClient client, RestRequest request) where T : new()
        {
            try
            {
                var response = client.Execute(request);
                var result = new DataResponse<T>(response.StatusCode, new ResponseResultModels<T>(response));
                
                return result;
            }
            catch (Exception e)
            {
                var result = new DataResponse<T>(HttpStatusCode.BadRequest, new ResponseResultModels<T>() { Content = ConstantsBase.Error.ERROR_OCCUR_TRY_AGAIN + e.Message });
                return result;
            }
        }
        public static DataResponse<T> GetData<T>(string host, string controller, Dictionary<String, String> header) where T : new()
        {
            try
            {
                var client = new RestClient(host);
                var request = new RestRequest(controller, Method.GET);

                if (header != null)
                {
                    foreach (String item in header.Keys)
                    {
                        request.AddHeader(item, header[item]);
                    }
                }

                return CreateRequest<T>(client, request);
            }
            catch (Exception e)
            {
                return new DataResponse<T>(HttpStatusCode.BadRequest, new ResponseResultModels<T>() { Content = ConstantsBase.Error.ERROR_OCCUR_TRY_AGAIN + e.Message });
            }
        }

        public static DataResponse<T> PostData<T, T2>(string host, string controller, T2 data, Dictionary<String, String> header, AuthEntity authEntity) where T : new()
        {
            if (header.ContainsKey("Auth"))
                header["Auth"] = authEntity.Serialize();
            else
                header.Add("Auth", authEntity.Serialize());


            return PostData<T, T2>(host, controller, data, header);
        }
        public static DataResponse<T> PostData<T, T2>(string host, string controller, T2 data, AuthEntity authEntity) where T : new()
        {
            var header = new Dictionary<string, string>();
            if (header.ContainsKey("Auth"))
                header["Auth"] = authEntity.Serialize();
            else
                header.Add("Auth", authEntity.Serialize());


            return PostData<T, T2>(host, controller, data, header);
        }

        public static DataResponse<T> PostData<T, T2>(string host, string controller, T2 data, Dictionary<String, String> header) where T : new()
        {
            try
            {
                var client = new RestClient(host);
                var request = new RestRequest(controller, Method.POST);

                if (header != null)
                {
                    foreach (String item in header.Keys)
                    {
                        request.AddHeader(item, header[item]);
                    }
                }
                if (data != null)
                {
                    try
                    {
                        //Add dynamic object
                        foreach (var pair in (dynamic)data)
                        {
                            request.AddParameter(pair.Name, pair.Value.Value);
                        }
                    }
                    catch (Exception)
                    {
                        //Add object convert to body json
                        request.AddJsonBody(data);
                    }
                }

                return CreateRequest<T>(client, request);
            }
            catch (Exception e)
            {
                return new DataResponse<T>(HttpStatusCode.BadRequest, new ResponseResultModels<T>() { Content = ConstantsBase.Error.ERROR_OCCUR_TRY_AGAIN + e.Message });
            }
        }

        public static DataResponse<T> PostData<T>(string host, string controller, Dictionary<String, String> header) where T : new()
        {
            return PostData<T, dynamic>(host, controller, null, header);
        }
        
     
        public static DataResponse<T> PostFile<T>(string host, string controller, List<HttpPostedFileBase> files, Dictionary<String, String> header) where T : new()
        {
            try
            {
                var client = new RestClient(host);
                var request = new RestRequest(controller, Method.POST);

                if (header != null)
                {
                    foreach (String item in header.Keys)
                    {
                        request.AddHeader(item, header[item]);
                    }
                }
                if (files != null)
                {
                    foreach (var file in files)
                    {
                        byte[] fileData = null;
                        using (var binaryReader = new BinaryReader(file.InputStream))
                        {
                            fileData = binaryReader.ReadBytes(file.ContentLength);
                        }
                        request.AddFile(file.FileName.ConvertToUnSign().Replace(" ", "").ToLower(), fileData, file.FileName.ConvertToUnSign().Replace(" ", "").ToLower());
                    }
                }

                return CreateRequest<T>(client, request);
            }
            catch (Exception e)
            {
                return new DataResponse<T>(HttpStatusCode.BadRequest, new ResponseResultModels<T>() { Content = ConstantsBase.Error.ERROR_OCCUR_TRY_AGAIN });
            }
        }

        public static DataResponse<T> PostFile<T>(string host, string controller, List<HttpPostedFileBase> files, Dictionary<String, String> header, AuthEntity authEntity) where T : new()
        {
            if (header.ContainsKey("Auth"))
                header["Auth"] = authEntity.Serialize();
            else
                header.Add("Auth", authEntity.Serialize());


            return PostFile<T>(host, controller, files, header);
        }

        public static DataResponse<T> PostFile<T>(string host, string controller, List<HttpPostedFileBase> files, AuthEntity authEntity) where T : new()
        {
            return PostFile<T>(host, controller, files, new Dictionary<string, string>(), authEntity);
        }

        public static DataResponse<T> PostFile<T>(string host, string controller, HttpPostedFileBase files, AuthEntity authEntity) where T : new()
        {
            return PostFile<T>(host, controller, new List<HttpPostedFileBase>() { files },
                new Dictionary<string, string>(), authEntity);
        }

        public static DataResponse<T> Request<T, T2>(Method method, string host, string controller, T2 data, Dictionary<String, String> header, AuthEntity authEntity) where T : new()
        {
            if (header.ContainsKey("Auth"))
                header["Auth"] = authEntity.Serialize();
            else
                header.Add("Auth", authEntity.Serialize());
            return Request<T, T2>(method, host, controller, data, header);
        }

        public static DataResponse<T> Request<T, T2>(Method method, string host, string controller, T2 data, AuthEntity authEntity) where T : new()
        {
            Dictionary<String, String> header = new Dictionary<string, string>();
            return Request<T, T2>(method, host, controller, data, header, authEntity);
        }

        public static DataResponse<T> Request<T, T2>(Method method, string host, string controller, T2 data, Dictionary<String, String> header) where T : new()
        {
            try
            {
                var client = new RestClient(host);
                var request = new RestRequest(controller, method);

                if (header != null)
                {
                    foreach (String item in header.Keys)
                    {
                        request.AddHeader(item, header[item]);
                    }
                }
                if (data != null)
                {
                    try
                    {
                        //Add dynamic object
                        foreach (var pair in (dynamic)data)
                        {
                            request.AddParameter(pair.Name, pair.Value.Value);
                        }
                    }
                    catch (Exception)
                    {
                        //Add object convert to body json
                        request.AddJsonBody(data);
                    }
                }
                return CreateRequest<T>(client, request);
            }
            catch (Exception e)
            {
                return new DataResponse<T>(HttpStatusCode.BadRequest, new ResponseResultModels<T>() { Content = ConstantsBase.Error.ERROR_OCCUR_TRY_AGAIN + e.Message });
            }
        }

    }

}