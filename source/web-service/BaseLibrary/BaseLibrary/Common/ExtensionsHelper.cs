﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Text;
using Newtonsoft.Json;
using RestSharp;
using System.Security.Cryptography;
using System.Text.RegularExpressions;
using System.Dynamic;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using System.Web.UI;
using BaseLibrary.Controllers;
using BaseLibrary.Models.Entity;
using BaseLibrary.Utils;
using HttpUtility = RestSharp.Extensions.MonoHttp.HttpUtility;

namespace BaseLibrary.Common
{
    public static class ExtensionsHelper
    {
        //Function create or update item in list data
        public static List<T> DataListUpdateOrCreateData<T>(this List<T> listData, T itemData, string fieldKey, string fieldSort) where T : new()
        {
            var json = JsonConvert.DeserializeObject<List<dynamic>>(JsonConvert.SerializeObject(listData));
            var itemJson = JsonConvert.DeserializeObject<dynamic>(JsonConvert.SerializeObject(itemData));
            var fieldKeyValue = itemJson[fieldKey];
            object obj = null;
            foreach (var item in json)
            {
                if (item[fieldKey].ToString().Equals(fieldKeyValue.ToString()))
                {
                    obj = item;
                    break;
                }
            }
            if (obj == null)
                json.Add(itemJson);
            else
            {
                json[json.IndexOf(obj)] = itemJson;
            }
            if (!string.IsNullOrWhiteSpace(fieldSort))
            {
                var pos = 1;
                foreach (var item in json)
                {
                    item[fieldSort] = pos;
                    pos++;
                }
            }
            return JsonConvert.DeserializeObject<List<T>>(JsonConvert.SerializeObject(json));
        }

        //Function sort item in list data
        public static List<T> DataListSort<T>(this List<T> listData, string fieldSort) where T : new()
        {
            var json = JsonConvert.DeserializeObject<List<dynamic>>(JsonConvert.SerializeObject(listData));

            if (!string.IsNullOrWhiteSpace(fieldSort))
            {
                var pos = 1;
                foreach (var item in json)
                {
                    item[fieldSort] = pos;
                    pos++;
                }
            }
            return JsonConvert.DeserializeObject<List<T>>(JsonConvert.SerializeObject(json));
        }

        //Function sort item in list data
        public static List<T> DataListSort<T>(this List<T> listData, int startPos, int indexPos, string fieldSort) where T : new()
        {
            var json = JsonConvert.DeserializeObject<List<dynamic>>(JsonConvert.SerializeObject(listData));
            var itemStart = json[startPos];
            var itemIndexPos = json[indexPos];
            JsonArray jsonArray = new JsonArray();
            for (int i = 0; i < listData.Count; i++)
            {
                if (i != startPos && i != indexPos)
                {
                    jsonArray.Add(listData[i]);
                }
                if (i == indexPos)
                {
                    jsonArray.Add(itemStart);
                    jsonArray.Add(listData[i]);
                }
            }
            json = JsonConvert.DeserializeObject<List<dynamic>>(JsonConvert.SerializeObject(jsonArray));

            if (!string.IsNullOrWhiteSpace(fieldSort))
            {
                var pos = 1;
                foreach (var item in json)
                {
                    item[fieldSort] = pos;
                    pos++;
                }
            }
            return JsonConvert.DeserializeObject<List<T>>(JsonConvert.SerializeObject(json));
        }
        

        //Function render partial with model to string html
        public static string RenderPartialToString(this ControllerContext context, string partialViewName)
        {
            ViewDataDictionary viewData = new ViewDataDictionary();
            TempDataDictionary tempData = new TempDataDictionary();

            ViewEngineResult result = ViewEngines.Engines.FindPartialView(context, partialViewName);

            if (result.View != null)
            {
                StringBuilder sb = new StringBuilder();
                using (StringWriter sw = new StringWriter(sb))
                {
                    using (HtmlTextWriter output = new HtmlTextWriter(sw))
                    {
                        ViewContext viewContext = new ViewContext(context, result.View, viewData, tempData, output);
                        result.View.Render(viewContext, output);
                    }
                }

                return sb.ToString();
            }
            return String.Empty;
        }


        //Function render partial with model to string html
        public static string RenderPartialToString<T>(this ControllerContext context, string partialViewName, T model) where T : new()
        {
            ViewDataDictionary viewData = new ViewDataDictionary(model);
            TempDataDictionary tempData = new TempDataDictionary();

            ViewEngineResult result = ViewEngines.Engines.FindPartialView(context, partialViewName);

            if (result.View != null)
            {
                StringBuilder sb = new StringBuilder();
                using (StringWriter sw = new StringWriter(sb))
                {
                    using (HtmlTextWriter output = new HtmlTextWriter(sw))
                    {
                        ViewContext viewContext = new ViewContext(context, result.View, viewData, tempData, output);
                        result.View.Render(viewContext, output);
                    }
                }

                return sb.ToString();
            }
            return String.Empty;
        }

        //Function get unix time stamp for date
        public static long GetUnixTimeStamp(this DateTime dateTime)
        {
            return (long)dateTime.Subtract(new DateTime(1970, 1, 1)).TotalMilliseconds;
        }

        //Function convert unix time stamp to date
        public static DateTime UnixTimeStampToDateTime(this double timeMilisecond)
        {
            return ConvertUtils.TimeMilisecondToDateTime(timeMilisecond);
        }

        //Function get time post status
        public static StringBuilder GetTimePostStatus(this double timeMilisecond)
        {
            //Define date time
            long date10Day = 86400 * 10;
            long dateOneDay = 86400;
            long dateOneHour = 3600;
            long dateOneMinute = 60;
            string dateDayName = " ngày";
            string dateHourName = " giờ";
            string dateMinuteName = " phút";
            string dateHourAgoName = " trước";
            string dateJustNowName = " vừa xong";

            StringBuilder time = new StringBuilder();
            try
            {
                var temp = ConvertUtils.TimeMilisecondToDateTime(timeMilisecond);

                double totalTimeDiff = (DateTime.Now - temp).TotalSeconds;

                if (totalTimeDiff > date10Day)
                {
                    time.Append(temp.ToString("dd-MM-yyyy HH:mm:ss"));
                }
                else
                {
                    if (totalTimeDiff > dateOneDay)
                    {
                        int day = (int)(totalTimeDiff / dateOneDay);
                        time.Append(day).Append(" ").Append(dateDayName).Append(dateHourAgoName);
                    }
                    else if (totalTimeDiff > dateOneHour)
                    {
                        int hour = (int)(totalTimeDiff / dateOneHour);
                        time.Append(hour).Append(" ").Append(dateHourName).Append(dateHourAgoName);
                    }
                    else if (totalTimeDiff > dateOneMinute)
                    {
                        int minute = (int)(totalTimeDiff / dateOneMinute);
                        time.Append(minute).Append(" ").Append(dateMinuteName).Append(dateHourAgoName);
                    }
                    else
                        time.Append(dateJustNowName);
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.Message);
            }
            return time;
        }

        //Function generate string to md5
        public static String GenerateStringToMD5(this string value)
        {
            byte[] byteArray = Encoding.UTF8.GetBytes(value);
            MemoryStream stream = new MemoryStream(byteArray);
            string hexString = String.Empty;
            using (var md5 = MD5.Create())
            {
                hexString = ToHex(md5.ComputeHash(stream), false);
            }
            return hexString;
        }

        private static string ToHex(byte[] bytes, bool upperCase)
        {
            StringBuilder result = new StringBuilder(bytes.Length * 2);

            for (int i = 0; i < bytes.Length; i++)
                result.Append(bytes[i].ToString(upperCase ? "X2" : "x2"));

            return result.ToString();
        }
        //End Function generate string to md5

        //Function convert string to unsign
        public static string ConvertToUnSign(this string value)
        {
            Regex regex = new Regex("\\p{IsCombiningDiacriticalMarks}+");
            string temp = value.Normalize(NormalizationForm.FormD);
            return regex.Replace(temp, String.Empty).Replace('\u0111', 'd').Replace('\u0110', 'D');
        }

        /// <summary>
        /// Function sub string with count charactor
        /// </summary>
        /// <param name="content">String</param>
        /// <param name="countCharactor">Interger</param>
        /// <returns></returns>
        public static string SubString(this string content, int countCharactor)
        {
            return StringUtils.SubString(content, countCharactor);
        }

        /// <summary>
        /// Function sub string with count charactor
        /// </summary>
        /// <param name="content">String</param>
        /// <param name="countCharactor">Interger</param>
        /// <returns></returns>
        public static string SubString(this string content, int countCharactor, char splitBy, String readMore)
        {
            return StringUtils.SubString(content, countCharactor, splitBy, readMore);
        }

        public static string Serialize(this Object _object)
        {
            return new JavaScriptSerializer().Serialize(_object);

        }

        public static ExpandoObject ToExpando(this object anonymousObject)
        {
            IDictionary<string, object> anonymousDictionary = HtmlHelper.AnonymousObjectToHtmlAttributes(anonymousObject);
            IDictionary<string, object> expando = new ExpandoObject();
            foreach (var item in anonymousDictionary)
                expando.Add(item);
            return (ExpandoObject)expando;
        }

        #region price format
        public static String PriceFormat(this long val)
        {
            return String.Format("{0:N2}", val).Replace(".00", "").Replace(",", ".");
        }
        public static String PriceFormat(this double val)
        {
            return String.Format("{0:N2}", val).Replace(".00", "").Replace(",", ".");
        }
        public static String PriceFormat(this float val)
        {
            return String.Format("{0:N2}", val).Replace(".00", "").Replace(",", ".");
        }

        public static String PriceFormat(this int val)
        {
            return String.Format("{0:N2}", val).Replace(".00", "").Replace(",", ".");
        }
        #endregion

        public static String UrlEncode(this String text)
        {
            return HttpUtility.UrlEncode(text);
        }
        public static String UrlDecode(this String text)
        {
            return HttpUtility.UrlDecode(text);
        }
        public static String HtmlDecode(this String text)
        {
            return HttpUtility.HtmlDecode(text);
        }
        public static String HtmlEncode(this String text)
        {
            return HttpUtility.HtmlEncode(text);
        }

        public static AuthEntity GetAuthEntity(this HttpContext context)
        {
            try
            {
                return ((BaseController)HttpContext.Current.Items["controllerInstance"]).AuthEntity;
            }
            catch (Exception e)
            {
                return new AuthEntity();
            }
        }

        public static Models.Entity.AccountEntityInstance GetAccountEntity(this HttpContext context)
        {
            try
            {
                return ((BaseController)HttpContext.Current.Items["controllerInstance"]).UserLogin<AccountEntityInstance>();
            }
            catch (Exception e)
            {
                return new AccountEntityInstance();
            }
        }
        public static T GetAccountEntity<T>(this HttpContext context)
        {
            try
            {
                return ((BaseController)HttpContext.Current.Items["controllerInstance"]).UserLogin<T>();
            }
            catch (Exception e)
            {
                return default(T);
            }
        }
    }
}