﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.Diagnostics;
using System.IO;
using System.Runtime.CompilerServices;
using System.ComponentModel;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.UI;
using Newtonsoft.Json;
using RestSharp;
using System.Web.Configuration;

namespace BaseLibrary.Common
{
    public static class UtilitiesHelper
    {
        public static string mailServer = WebConfigurationManager.AppSettings["MailServer"];
        public static string mailServerPassword = WebConfigurationManager.AppSettings["MailServerPassword"];
        public static string mailServerSTMP = WebConfigurationManager.AppSettings["MailServerSTMP"];
        public static string mailServerPORT = WebConfigurationManager.AppSettings["MailServerPORT"];

        public static bool SendMail2(string emailTo, string subject, string body)
        {
            System.Net.Mail.MailMessage mail = new System.Net.Mail.MailMessage();
            mail.To.Add(emailTo);
            mail.From = new MailAddress(mailServer, "iDuhoc", System.Text.Encoding.UTF8);
            mail.Subject = subject;
            mail.SubjectEncoding = System.Text.Encoding.UTF8;
            mail.Body = body;
            mail.BodyEncoding = System.Text.Encoding.UTF8;
            mail.IsBodyHtml = true;
            mail.Priority = MailPriority.High;
            SmtpClient client = new SmtpClient();
            client.Credentials = new System.Net.NetworkCredential(mailServer, mailServerPassword);
            client.Port = int.Parse(mailServerPORT);
            client.Host = mailServerSTMP;
            client.EnableSsl = true;
            
                client.Send(mail);
                return true;
           
        }

    }
}