﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using Newtonsoft.Json;
using RestSharp;

namespace BaseLibrary.Common
{
    public class ResponseResultModels<T> : IRestResponse where T : new()
    {
        public IRestRequest Request { get; set; }
        public string ContentType { get; set; }
        public long ContentLength { get; set; }
        public string ContentEncoding { get; set; }
        public string Content { get; set; }
        public HttpStatusCode StatusCode { get; set; }
        public string StatusDescription { get; set; }
        public byte[] RawBytes { get; set; }
        public Uri ResponseUri { get; set; }
        public string Server { get; set; }
        public IList<RestResponseCookie> Cookies { get; }
        public IList<Parameter> Headers { get; }
        public ResponseStatus ResponseStatus { get; set; }
        public string ErrorMessage { get; set; }
        public Exception ErrorException { get; set; }
        public T Data
        {
            get
            {
                try
                {
                    return JsonConvert.DeserializeObject<T>(Content);
                }
                catch (Exception ex)
                {
                    return new T();
                }
            }
        }
        public ResponseResultModels(IRestResponse response)
        {
            this.Request = response.Request;
            this.ContentType = response.ContentType;
            this.ContentLength = response.ContentLength;
            this.Content = response.Content;
            this.StatusCode = response.StatusCode;
            this.StatusDescription = response.StatusDescription;
            this.RawBytes = response.RawBytes;
            this.ResponseUri = response.ResponseUri;
            this.Server = response.Server;
            this.Cookies = response.Cookies;
            this.Headers = response.Headers;
            this.ResponseStatus = response.ResponseStatus;
            this.ErrorMessage = response.ErrorMessage;
            this.ErrorException = response.ErrorException;
        }
        public ResponseResultModels()
        {
        }
    }

}