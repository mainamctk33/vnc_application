﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BaseLibrary.Common
{
    public class ConstantsBase
    {
        public class Response
        {
            public static readonly string DATA = "data";
            public static readonly string CONTENT = "content";
            public static readonly string STATUS_CODE = "status_code";
            public static readonly string MESSAGE = "message";
            public static readonly string EXCEPTION = "exception";
            public static readonly string RESPONSE = "response";
            public static readonly string DATA_STATUS = "data_status";
        }
        public class StatusCode
        {
            public static readonly int NO_LOGIN = 100001;
            public static readonly int EXEPTION = 100002;
            public static readonly int NO_REASON = 100003;
            public static readonly int RE_CHECK_PARAME = 100004;
            public static readonly int REQUIRE_PERMISSION = 100005;
            public static readonly int OK = 200;
            public static readonly int NOT_FOUND = 404;
        }
        public class Error
        {
            public static readonly string DO_NOT_HAVE_PERMISSION = "Bạn không có quyền thực hiện chức năng này";
            public static readonly string ERROR_OCCUR_TRY_AGAIN = "Có lỗi xảy ra, vui lòng thử lại sau";
            public static readonly string PLEASE_LOGIN_TO_PROCESS = "Vui lòng đang nhập để thực hiện";
            public static readonly string TOKEN_EXPRIRED = "Token không tồn tại hoặc đã hết hạn";
            public static readonly string RECHECK_PARAME = "Vui lòng kiểm tra lại dữ liệu";
        }
    }
}