﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BaseLibrary.Common;
using BaseLibrary.Utils;
using JWT;
using JWT.Algorithms;
using JWT.Serializers;
using TokenExpiredException = BaseLibrary.Common.TokenExpiredException;

namespace BaseLibrary.Helper
{
    public static class TokenHelper
    {
        private static string secret = "DA1F316D4CEA865044B768CFFCF7A396";

        /// <summary>
        /// Tao Token cho tk User va ktv
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="type">Mac dinh khong truyen, 0->User, 1->KTV</param>
        /// <returns></returns>
        public static string GenerateToken(string userId)
        {
            try
            {
                var payload = new Dictionary<string, object>
                    {
                        { "exp", long.MaxValue},
                        { "iat", DateTime.Now.GetUnixTimeStamp()},
                        { "userId", userId},
                        { "iss", "vnc"}
                    };

                IJwtAlgorithm algorithm = new HMACSHA256Algorithm();
                IJsonSerializer serializer = new JsonNetSerializer();
                IBase64UrlEncoder urlEncoder = new JwtBase64UrlEncoder();
                IJwtEncoder encoder = new JwtEncoder(algorithm, serializer, urlEncoder);

                var token = encoder.Encode(payload, secret);
                return token;
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        public static dynamic ValidateToken(string token)
        {
            try
            {
                IJsonSerializer serializer = new JsonNetSerializer();
                IDateTimeProvider provider = new UtcDateTimeProvider();
                IJwtValidator validator = new JwtValidator(serializer, provider);
                IBase64UrlEncoder urlEncoder = new JwtBase64UrlEncoder();
                IJwtDecoder decoder = new JwtDecoder(serializer, validator, urlEncoder);
                var json = decoder.Decode(token, secret, verify: true);
                return ConvertUtils.toObject<dynamic>(new
                {
                    Code = (int)ValidateTokenResult.Success,
                    Data = json
                }.Serialize());
            }
            catch (TokenExpiredException)
            {
                return new
                {
                    Code = (int)ValidateTokenResult.Expirated,
                    Data = "Token has expired"
                };
            }
            catch (SignatureVerificationException)
            {
                return new
                {
                    Code = (int)ValidateTokenResult.Invalid,
                    Data = "Token has invalid signature"
                };
            }
        }

        public enum ValidateTokenResult
        {
            Success = 0,
            Invalid = 1,
            Expirated = 2
        }
    }
}
